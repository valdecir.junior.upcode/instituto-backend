import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.nepalign.model.Cliente;
import br.com.desenv.nepalign.model.ImagemUsuario;
import br.com.desenv.nepalign.model.TabelaSistema;
import br.com.desenv.nepalign.service.ClienteService;
import br.com.desenv.nepalign.service.ImagemUsuarioService;
import br.com.desenv.nepalign.service.TabelaSistemaService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;
import flex.messaging.io.ArrayList;


public class ConverterFotosCliente {
	public static void main(String[] args) throws Exception
	{

		EntityManager manager =null ;
		EntityTransaction transaction = null;
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			TabelaSistema tbl = new TabelaSistemaService().recuperarPorId(manager, 3);
			String where = " (foto is not null or fotoCpf is not null or fotoInconsistencia is not null or fotoRg is not null or fotoRgVerso is not null )";
			System.out.println("Carregando Cliente...");
			List<Cliente> listaCliente = new ClienteService().listarPaginadoSqlLivreGenerico(manager, Integer.MAX_VALUE, 1, where, null, null).getRecordList();
			System.out.println("Clientes Carregados... \n Iniciando Script...");
			Integer length = listaCliente.size();
			List<ImagemUsuario> listaImagemUsuario = new ArrayList();
			for (int i = 0; i < listaCliente.size(); i++) 
			{
				
				Cliente cliente = listaCliente.get(i);
		
				if(cliente.getFoto()!=null)
				{
					ImagemUsuario imagemUsuarioFoto = new ImagemUsuario();			
					imagemUsuarioFoto.setTabelaSistema(tbl);
					imagemUsuarioFoto.setDescricao("Foto de " + tbl.getDescricao());
					imagemUsuarioFoto.setCampoChave("Foto Cliente");				
					imagemUsuarioFoto.setImagem(cliente.getFoto());	
					imagemUsuarioFoto.setInfoCampoChave(""+cliente.getId());
					listaImagemUsuario.add(imagemUsuarioFoto);
				}
				if(cliente.getFotoCpf()!=null)
				{
					ImagemUsuario imagemUsuarioFoto = new ImagemUsuario();			
					imagemUsuarioFoto.setTabelaSistema(tbl);
					imagemUsuarioFoto.setDescricao("Foto de " + tbl.getDescricao());
					imagemUsuarioFoto.setCampoChave("Foto CPF");				
					imagemUsuarioFoto.setImagem(cliente.getFotoCpf());	
					imagemUsuarioFoto.setInfoCampoChave(""+cliente.getId());
					listaImagemUsuario.add(imagemUsuarioFoto);
				}
				if(cliente.getFotoRg()!=null)
				{
					ImagemUsuario imagemUsuarioFoto = new ImagemUsuario();			
					imagemUsuarioFoto.setTabelaSistema(tbl);
					imagemUsuarioFoto.setDescricao("Foto de " + tbl.getDescricao());
					imagemUsuarioFoto.setCampoChave("Foto RG");				
					imagemUsuarioFoto.setImagem(cliente.getFotoRg());	
					imagemUsuarioFoto.setInfoCampoChave(""+cliente.getId());
					listaImagemUsuario.add(imagemUsuarioFoto);
				}
				if(cliente.getFotoRgVerso()!=null)
				{
					ImagemUsuario imagemUsuarioFoto = new ImagemUsuario();			
					imagemUsuarioFoto.setTabelaSistema(tbl);
					imagemUsuarioFoto.setDescricao("Foto de " + tbl.getDescricao());
					imagemUsuarioFoto.setCampoChave("Foto RG Verso");				
					imagemUsuarioFoto.setImagem(cliente.getFotoRgVerso());
					imagemUsuarioFoto.setInfoCampoChave(""+cliente.getId());
					listaImagemUsuario.add(imagemUsuarioFoto);
				}
				if(cliente.getFotoInconsistencia()!=null)
				{
					ImagemUsuario imagemUsuarioFoto = new ImagemUsuario();			
					imagemUsuarioFoto.setTabelaSistema(tbl);
					imagemUsuarioFoto.setDescricao("Foto de " + tbl.getDescricao());
					imagemUsuarioFoto.setCampoChave("Foto Inconsistencia");				
					imagemUsuarioFoto.setImagem(cliente.getFotoInconsistencia());
					imagemUsuarioFoto.setInfoCampoChave(""+cliente.getId());
					listaImagemUsuario.add(imagemUsuarioFoto);
				}
				System.out.println("Fotos Cliente"+cliente.getId()+" Carregadas! "+(i+1)+" de "+length);
			}	
			Integer len = listaImagemUsuario.size();
			for (int j = 0; j < listaImagemUsuario.size(); j++) 
			{
				System.out.println("Salvando Imagens "+(j+1)+" de "+len);
				new ImagemUsuarioService().salvar(manager, transaction, listaImagemUsuario.get(j));
			}
			if(transacaoIndependente)
			{
				transaction.commit();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
}
