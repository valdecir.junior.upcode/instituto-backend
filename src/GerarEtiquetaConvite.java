import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import br.com.desenv.nepalign.model.Cliente;


public class GerarEtiquetaConvite {

	
	
	public static void main(String[] args) throws JRException {
		gerar();
		}
	public static void gerar() throws JRException
	{
		String arquivoCSV = "C:/Users/Tarcisio/Documents/LISTA EVENTO ATUALIZADA.csv";
	    BufferedReader br = null;
	    String linha = "";
	    String divisor = ";";
	    String relatorio = "etiquetaConvite.jasper";
		byte[] reportBytes = null;
	    try {

	        br = new BufferedReader(new FileReader(arquivoCSV));
	        List<Cliente> lista = new ArrayList<Cliente>();
	        while ((linha = br.readLine()) != null)
	        {
	            String[] l = linha.split(divisor);
	            if(!l[0].equals("CLIENTES EVENTO"))
	            {
	            Cliente c = new Cliente();
	            c.setNome(l[0]);
	            c.setTelefoneCelular01(l[1]);
	            c.setLogradouro(l[2]);
	            c.setBairro(l[3]);
	            c.setCidadeCache(l[4]);
	            lista.add(c);
	            System.out.println(l[0]+" - "+l[1]);
	            }

	        }
			JRBeanCollectionDataSource dS = new JRBeanCollectionDataSource(lista);
         	String reportPath = "C:/Desenv/WorkSpace/nepalign/src/relatorios/"+relatorio;	
			System.out.println(reportPath);
			reportBytes = JasperExportManager.exportReportToPdf(JasperFillManager.fillReport(reportPath, null,dS  ));
			DateFormat formataDataPdf  = new SimpleDateFormat("dd-MM-yyyy");
			
          	String caminhoBoletimPdf = "C:/Users/Tarcisio/Documents/etiquetaConvite.pdf";
        	OutputStream out;
          	if(new File(caminhoBoletimPdf).exists())
			{
				System.out.println("Arquivo já existente...");
          		new File(caminhoBoletimPdf).delete();
          		System.out.println("Arquivo: " + caminhoBoletimPdf + " foi excluído!");
			}
          	// exportacao do relatorio para outro formato, no caso PDF
          	out = new FileOutputStream(caminhoBoletimPdf);
			out.write(reportBytes,0,reportBytes.length);
			out.flush();
			out.close();
			System.out.println("Convite PDF Gerado com sucesso.");
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}
}
