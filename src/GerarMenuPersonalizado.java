
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.nepalign.service.UsuarioService;


public class GerarMenuPersonalizado {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		UsuarioService usuarioService = new UsuarioService();
		Usuario usuario = usuarioService.recuperarPorId(532);
		
		dos.write(usuarioService.recuperarPreferencias(usuario));
		dos.writeUTF("[HOME]="); //usar antes de comear os dados das tiles
		dos.writeUTF("RESUMO DAS VENDAS POR GRUPO"); //titulo da tile
		dos.writeUTF("0xFFC125");// cor da tile
		dos.writeUTF("br.com.desenv.nepalign.view.component.RelatorioVendasGrupo"); //nome da tela
	
		dos.writeUTF("[HOME]="); //usar antes de comear os dados das tiles
		dos.writeUTF("RESUMO DAS VENDAS DO DIA"); //titulo da tile
		dos.writeUTF("0xFFC125");// cor da tile
		dos.writeUTF("br.com.desenv.nepalign.view.component.FormRelatorioVendaPorVendedor"); //nome da tela
	
		dos.writeUTF("[HOME]="); //usar antes de comear os dados das tiles
		dos.writeUTF("RESUMO DAS VENDAS (GRUPO / MARCA)"); //titulo da tile
		dos.writeUTF("0xFFC125");// cor da tile
		dos.writeUTF("br.com.desenv.nepalign.view.component.PedidoVendaComponenteRelatorioMarcaEGrupo"); //nome da tela
	
		dos.writeUTF("[HOME]="); //usar antes de comear os dados das tiles
		dos.writeUTF("RESUMO DAS VENDAS POR PLANO"); //titulo da tile
		dos.writeUTF("0xFFC125");// cor da tile
		dos.writeUTF("br.com.desenv.nepalign.view.component.FormRelatorioVendaPorPlano"); //nome da tela
	
		dos.writeUTF("[HOME]="); //usar antes de comear os dados das tiles
		dos.writeUTF("TOTAIS ESTOQUE/GRUPO"); //titulo da tile
		dos.writeUTF("0x36A8CE");// cor da tile
		dos.writeUTF("br.com.desenv.nepalign.view.component.FormRelatorioEstoqueGeral"); //nome da tela
	
		dos.writeUTF("[HOME]="); //usar antes de comear os dados das tiles
		dos.writeUTF("CONSULTA SALDO"); //titulo da tile
		dos.writeUTF("0x36A8CE");// cor da tile
		dos.writeUTF("br.com.desenv.nepalign.view.component.SaldoEstoqueProdutoComponenteRelatorio"); //nome da tela
	
		dos.writeUTF("[HOME]="); //usar antes de comear os dados das tiles
		dos.writeUTF("RESUMO CONTAS A PAGAR POR DIA/QUINZENA"); //titulo da tile
		dos.writeUTF("0x5FB336");// cor da tile
		dos.writeUTF("br.com.desenv.nepalign.view.component.FormRelatorioContasAPagarResumo"); //nome da tela
	
		dos.writeUTF("[HOME]="); //usar antes de comear os dados das tiles
		dos.writeUTF("PEDIDOS PENDENTES"); //titulo da tile
		dos.writeUTF("0xDA532C");// cor da tile
		dos.writeUTF("br.com.desenv.nepalign.view.component.PedidoCompraVendaPendenteComponenteRelatorio"); //nome da tela
		
		dos.writeUTF("[SHOW_LAST_MOVUPDATE]=");
		dos.writeBoolean(true);
	
		usuarioService.atualizarPreferenciasUsuario(usuario, baos.toByteArray());
		
	
	}

}
