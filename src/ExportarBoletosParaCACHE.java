

import java.sql.Connection;
import java.sql.Statement;
import java.util.List;

import br.com.desenv.nepalign.model.BoletoBancario;
import br.com.desenv.nepalign.service.BoletoBancarioService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ExportarBoletosParaCACHE 
{
	public static void main(String args[]) throws Exception
	{
		BoletoBancarioService boletoService = new BoletoBancarioService();
		List<BoletoBancario> lista = boletoService.listar();
		Connection con = ConexaoUtil.getConexaoPadraoCACHE();
		for (BoletoBancario boletoBancario : lista) {
			try
			{ 
				exportarBoletoParaCache(boletoBancario, con);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}
	}
	
	public static void exportarBoletoParaCache(BoletoBancario boletoBancario, Connection conCache) throws Exception
	{
		boolean conIndependente = true;
		Connection con = null;
		String sql;
		if (conCache== null)
		{
			con = ConexaoUtil.getConexaoPadraoCACHE();
		}
		else
		{
			conIndependente=false;
			con = conCache;
		}
		//verifica se boleto já existe
		sql = "SELECT * FROM CRBoletoDuplicata WHERE nossoNumero = '" + boletoBancario.getNossoNumero() + "'";
		Statement stm = con.createStatement();
		if (stm.executeQuery(sql).next()==true) //Boleto existe, precisamos fazer um UpDate
		{
//			sql = "UPDATE CRBoletoDuplicata SET codigoBarrasDuplicata = '" + (boletoBancario.getCodigoBarras()==null?"":boletoBancario.getCodigoBarras()) + "'" + 
//				 ", idCliente =  " + boletoBancario.getClienteCache() + 
//				 ", idEmpresa =  " + DsvConstante.getParametrosSistema().getInt("EMPRESA_LOCAL") +
//				 ", linhaDigitavelDuplicata = '" + (boletoBancario.getLinhaDigitavel()==null?"":boletoBancario.getLinhaDigitavel()) + "'" +
//				 //", nossoNumero = '" + boletoBancario.getNossoNumero() + "'" + 
//				 ", numeroDuplicata = '" + boletoBancario.getDuplicataCache() + "'" + 
//				 ", numeroParcela = '" + boletoBancario.getParcelaDuplicataCache() + "'" +
//				 ", valorParcela = " + boletoBancario.getValorOriginal() + " " +
//				 ", valorBoleto = " + (boletoBancario.getValorOriginal() +boletoBancario.getValorAcrescimos()) + " ";;
//			
//			sql = sql + " WHERE nossoNumero = '" + boletoBancario.getNossoNumero() + "'";
//			stm.execute(sql);
			System.out.println("Já tem :"+boletoBancario.getNossoNumero());	
		}
		else
		{
			sql = "INSERT INTO CRBoletoDuplicata SET codigoBarrasDuplicata = '" + (boletoBancario.getCodigoBarras()==null?"":boletoBancario.getCodigoBarras()) + "'" + 
					 ", idCliente =  " + boletoBancario.getClienteCache() + 
					 ", idEmpresa =  " + DsvConstante.getParametrosSistema().getInt("EMPRESA_LOCAL") +
					 ", linhaDigitavelDuplicata = '" + (boletoBancario.getLinhaDigitavel()==null?"":boletoBancario.getLinhaDigitavel()) + "'" +
					 ", nossoNumero = '" + boletoBancario.getNossoNumero() + "'" + 
					 ", numeroDuplicata = '" + boletoBancario.getDuplicataCache() + "'" + 
					 ", numeroParcela = '" + boletoBancario.getParcelaDuplicataCache() + "'" +
					 ", valorParcela = " + boletoBancario.getValorOriginal() + " " +
					 ", valorBoleto = " + (boletoBancario.getValorOriginal() +boletoBancario.getValorAcrescimos()) + " ";
			
			try
			{
				stm.execute(sql);
			}
			catch(Exception exp)
			{
				exp.printStackTrace();
				System.out.print("Duplicidade: ");
				System.out.println(boletoBancario.getNossoNumero()+ " " + boletoBancario.getClienteCache()+ " " + boletoBancario.getDuplicataCache() + " " + boletoBancario.getParcelaDuplicataCache());
			}
		}
			
		if(con != null && !con.isClosed() && conIndependente==true)
			con.close();
	}
	
}	
