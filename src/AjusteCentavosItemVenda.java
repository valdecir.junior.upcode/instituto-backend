import java.sql.Connection;
import java.sql.ResultSet;

import br.com.desenv.padraoignorante.conexao.ConexaoUtil;


public class AjusteCentavosItemVenda 
{
	public static void main(String[] args) throws Exception
	{
		Connection con = ConexaoUtil.getConexaoPadrao();
		  
		String str = "";
		str = str + " select v.idTipoMovimentacaoEstoque as tipo, v.idPedidoVenda, v.numeroControle, v.valorTotalPedido, round(sum(i.quantidade * IF(i.entradaSaida = 'S' , i.precoVenda, i.precoVenda * (-1))), 2) as VI, v.valorTotalPedido - (round(sum(i.quantidade * IF(i.entradaSaida = 'S', i.precoVenda, i.precoVenda * -1)), 2)) as diff from itempedidovenda i ";
		str = str + " inner join pedidovenda v on v.idpedidovenda = i.idpedidovenda ";
		str = str + " where v.dataVenda >='2015-12-01 00:00:00' and v.dataVenda <= '2015-12-06 23:59:59' ";
		str = str + " and v.idSituacaoPedidoVenda <> 5 and v.idEmpresaFisica = 4 and v.valorTotalPedido > 0";
		str = str + " group by v.idPedidoVenda ";   
		str = str + " having (sum(i.quantidade * IF(i.entradaSaida = 'S', i.precoVenda, i.precoVenda * -1)) <> v.valorTotalPedido) and v.valorTotalPedido - (round(sum(i.quantidade * IF(i.entradaSaida = 'S', i.precoVenda, i.precoVenda * -1)), 2)) <> 0 ";
		 
		ResultSet rs = con.createStatement().executeQuery(str);
		
		while(rs.next())
		{
			Integer idPedidoVenda = rs.getInt("idPedidoVenda");
			Integer tipoMov = rs.getInt("tipo");
			
			Double diff = rs.getDouble("diff");
			 
			String strItem = "select idItemPedidoVenda, quantidade, precoVenda from itemPedidoVenda where idPedidoVenda = " + idPedidoVenda + " order by quantidade asc ";
			
			if(diff < 0)
			{
				ResultSet rsItem = con.createStatement().executeQuery(strItem);
				 
				Double valorTotal = 0.0;
				
				Integer idItemMenorQuantidade = 0;
				
				while(rsItem.next())
				{
					Double quantidade = rsItem.getDouble("quantidade");
					Double valorVenda = rsItem.getDouble("precoVenda");
					
					if(idItemMenorQuantidade == 0 && quantidade > 1 && !(quantidade % 2 > 0))
					{
						if(idItemMenorQuantidade == 0)
							idItemMenorQuantidade = rsItem.getInt("idItemPedidoVenda");	
					}
					else if(idItemMenorQuantidade == 0 && quantidade == 1)
					{
						if(idItemMenorQuantidade == 0)
							idItemMenorQuantidade = rsItem.getInt("idItemPedidoVenda");	
					}
					
					
					valorTotal += valorVenda * quantidade;
				}
				String sql = "";
				 
				if(tipoMov == 5)
					sql = "UPDATE itemPedidoVenda set precoVenda = precoVenda - (IF(entradaSaida = 'S',  ROUND(" + (diff * -1) + ", 2) * -1, ROUND(" + (diff * -1) + ", 2)) / quantidade) where idItemPedidoVenda = " + idItemMenorQuantidade + ";";
				else
					sql = "UPDATE itemPedidoVenda set precoVenda = precoVenda - (ROUND(" + (diff * -1) + ", 2) / quantidade) where idItemPedidoVenda = " + idItemMenorQuantidade + ";";
				
				if(idItemMenorQuantidade == 0)
				{
					System.out.println("#" + idPedidoVenda);
					System.out.println("#" + sql);
				}
				else
					System.out.println(sql);
			}
			else
			{
				ResultSet rsItem = con.createStatement().executeQuery(strItem);
				
				Double valorTotal = 0.0;
				
				Integer idItemMenorQuantidade = 0;
				
				while(rsItem.next())
				{
					Double quantidade = rsItem.getDouble("quantidade");
					Double valorVenda = rsItem.getDouble("precoVenda");
					
					if(idItemMenorQuantidade == 0 && quantidade > 1 && !(quantidade % 2 > 0))
					{
						if(idItemMenorQuantidade == 0)
							idItemMenorQuantidade = rsItem.getInt("idItemPedidoVenda");	
					}
					else if(idItemMenorQuantidade == 0 && quantidade == 1)
					{
						if(idItemMenorQuantidade == 0)
							idItemMenorQuantidade = rsItem.getInt("idItemPedidoVenda");	
					}
					
					valorTotal += valorVenda * quantidade;
				}

				String sql = "";
				
				if(tipoMov == 5)
					sql = "UPDATE itemPedidoVenda set precoVenda = precoVenda + (IF(entradaSaida = 'S',  ROUND(" + (diff * -1) + ", 2) * -1, ROUND(" + (diff * -1) + ", 2)) / quantidade) where idItemPedidoVenda = " + idItemMenorQuantidade + ";";
				else
					sql = "UPDATE itemPedidoVenda set precoVenda = precoVenda + (ROUND(" + (diff) + ", 2) / quantidade) where idItemPedidoVenda = " + idItemMenorQuantidade + ";";
				
				if(idItemMenorQuantidade == 0)
				{
					System.out.println("#" + idPedidoVenda);
					System.out.println("#" + sql);
				}
				else
					System.out.println(sql);
			}
		}
	}
}
