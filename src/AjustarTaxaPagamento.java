import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.nepalign.model.PagamentoCartao;
import br.com.desenv.nepalign.model.Parcelapagamentocartao;
import br.com.desenv.nepalign.model.TaxaPagamento;
import br.com.desenv.nepalign.service.PagamentoCartaoService;
import br.com.desenv.nepalign.service.ParcelapagamentocartaoService;
import br.com.desenv.nepalign.service.TaxaPagamentoService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;


public class AjustarTaxaPagamento {
	public static void main(String args[]) throws Exception
	{
		EntityManager manager = null;
		EntityTransaction transaction = null;
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			System.out.println("carregando pagamentos....");
			List<PagamentoCartao> listaPagamentoCartao = new PagamentoCartaoService().listar(manager, null, null, null);
			System.out.println("carregamento completo.");
			int size = listaPagamentoCartao.size();
			for (int i = 0; i < size; i++) 
			{
				PagamentoCartao pag = listaPagamentoCartao.get(i);
				System.out.println("ajustando pagamento  "+pag.getId()+" "+(i+1)+" de "+size);
				
				Parcelapagamentocartao parC = new Parcelapagamentocartao();
				parC.setPagamentoCartao(pag);
				List<Parcelapagamentocartao> listaParcelasAntigas = new ParcelapagamentocartaoService().listarPorObjetoFiltro(manager, parC, null, null, null);
				for (int j = 0; j < listaParcelasAntigas.size(); j++) 
				{
					new ParcelapagamentocartaoService().excluir(manager, transaction, listaParcelasAntigas.get(j));
				}
				TaxaPagamento taxa = new TaxaPagamentoService().buscarTaxaPorFormaDePagamento(pag.getCaixaDia().getEmpresaFisica(),pag.getFormaPagamento(), pag.getNumeroParcelas());
				pag.setTaxaPagamento(taxa);
				Double valorTaxado = pag.getValorOriginal() -(pag.getValorOriginal()*(taxa.getPercentual()/100));
				int valRed = (int) ((valorTaxado)/pag.getNumeroParcelas()*100);
				Double  valorParcela =  ((double)valRed / 100D); 
				pag.setValorParcela(valorParcela);
				pag.setValorTaxado(valorTaxado);
				pag.setEmitente(pag.getFormaPagamento().getDescricao());
				pag.setAlterado(true);
				new PagamentoCartaoService().atualizar(manager, transaction, pag);
				List<Parcelapagamentocartao> listaParcelasNovas = gerarNovasParcelas(pag, valorParcela, pag.getNumeroParcelas(), manager, transaction);
				for (int j = 0; j < listaParcelasNovas.size(); j++) 
				{
					new ParcelapagamentocartaoService().atualizar(manager, transaction, listaParcelasNovas.get(j));
				}
			}
		
			if(transacaoIndependente)
			{
				System.out.println("comitando...");
				transaction.commit();
				System.out.println("feito.");
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			if(transacaoIndependente)
				transaction.rollback();


			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	public  static List<Parcelapagamentocartao> gerarNovasParcelas(PagamentoCartao pag,Double valorParcela,Integer vezes,EntityManager manager,EntityTransaction transaction) throws ParseException
	{
		
		Parcelapagamentocartao parcela = new Parcelapagamentocartao();
		List<Parcelapagamentocartao> listaPar = new ArrayList<>();
		Integer mes = new Integer(1);
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
		Date data = new Date(); 
		Calendar cal = null;
		for (int i = 1; i <= vezes; i++) 
		{
			String dataString = formatter.format(new Date());
			data = (java.util.Date) formatter.parse(dataString);
			cal=Calendar.getInstance();
			cal.setTime(data);
			cal.add(Calendar.MONTH, mes);
			parcela = new Parcelapagamentocartao();
			parcela.setDataVencimento(cal.getTime());
			parcela.setNumero(i);
			parcela.setValor(valorParcela);
			parcela.setPagamentoCartao(pag);
			listaPar.add(parcela);
			mes += 1;
		}
		return listaPar;
	}
}
