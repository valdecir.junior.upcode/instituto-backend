import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import br.com.desenv.nepalign.model.ContaGerencial;
import br.com.desenv.nepalign.model.TipoConta;
import br.com.desenv.nepalign.model.TipoContaContaGer;
import br.com.desenv.nepalign.service.ContaGerencialService;
import br.com.desenv.nepalign.service.TipoContaContaGerService;
import br.com.desenv.nepalign.service.TipoContaService;
import br.com.desenv.nepalign.util.DsvUtilText;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;


public class ImportadorPlanoContasNovo 
{
	public static void main(String[] args) throws Exception 
	{
		System.setProperty("file.encoding", "UTF-8");
		ImportadorPlanoContasNovo ipc = new ImportadorPlanoContasNovo();
		ipc.importadorPlanoContas("");
		ipc.geradorPlanoContasDePara("");
	}
	
	private void importadorPlanoContas(String arquivo) throws Exception
	{
		FileReader reader = null;
		BufferedReader bufferedReader = null;
		String linha = null;
		
		try
		{
			
			Connection con = ConexaoUtil.getConexaoPadrao();
			Statement st = con.createStatement();
			st.execute("DELETE FROM contagerencial");
			st.execute("DELETE FROM TipoConta");
			st.execute("DELETE FROM TipoContaContaGer");
			st.execute("ALTER TABLE contagerencial AUTO_INCREMENT = 1");
			st.execute("ALTER TABLE TipoConta AUTO_INCREMENT = 1");
			st.execute("ALTER TABLE TipoContaContaGer AUTO_INCREMENT = 1");
			
			st.execute("INSERT INTO contagerencial (`idContaGerencial`, `codigoContaNivel1`, `descricao`, `contaContabil`, `codigoReduzido`) VALUES ('1', '1', 'ENTRADAS', '1', '1')");
			st.execute("INSERT INTO contagerencial (`idContaGerencial`, `codigoContaNivel1`, `descricao`, `contaContabil`, `codigoReduzido`) VALUES ('2', '2', 'SAIDAS', '2', '2')");
			
			st.close();
			con.close();
		}
		catch(Exception ex)
		{
			throw ex;
		}
		
		ContaGerencial conta;
		ContaGerencialService contaService = new ContaGerencialService();
		TipoConta tipoConta;
		TipoContaService tipoContaService = new TipoContaService();
		TipoContaContaGer tipoDePara = new TipoContaContaGer();
		TipoContaContaGerService tipoDeParaService = new TipoContaContaGerService();
		
		try
		{
			reader = new FileReader("c:/desenv/manamaue/planoDeContas.csv");
		}
		catch(FileNotFoundException fnfe)
		{
			throw new Exception(fnfe);	
		}
		
		int nivel1;
		int nivel2;
		int nivel3;
		int nivel4;
		String descricao;
		String contas[];
		
		bufferedReader = new BufferedReader(reader);
		String valores[];
		int contLinhas = 0; 
		int cont=0;
		while (bufferedReader.ready())
		{
			linha = bufferedReader.readLine();
			contLinhas++;
			if (linha.trim().equals("")==false)
			{
				valores = linha.split(";");
				nivel2 = 0;
				nivel3 = 0;
				if (valores.length>0 && valores[0].trim().equals("")==false)
				{
					cont++;
					//System.out.println(valores[0]);
					conta = new ContaGerencial();
					contas = valores[0].split("\\.");
					if (contas.length==1)
						nivel2 = Integer.parseInt(valores[0]);
					else if (contas.length==2)
					{
						nivel2 = Integer.parseInt(contas[0]);
						nivel3 = Integer.parseInt(contas[1]);
					}
					//if (contas.length>2)
						
					nivel1 = Integer.parseInt(valores[2]);
					descricao = valores[2];
					conta = new ContaGerencial();
					conta.setCodigoContaNivel1(nivel1);
					if (nivel2>0)
						conta.setCodigoContaNivel2(nivel2);
					if (nivel3>0)
						conta.setCodigoContaNivel3(nivel3);
					conta.setCodigoReduzido(cont);
					conta.setContaContabil(nivel1+(nivel2>0?("."+nivel2):"")+(nivel3>0?("."+nivel3):""));
					conta.setDescricao(DsvUtilText.retirarAcento(valores[1]));
					conta = contaService.salvar(conta);
					
					//Tipo conta equivalente
					if (nivel3>0)
					{
						tipoConta = new TipoConta();
						tipoConta.setDescricao(DsvUtilText.retirarAcento(valores[1]));
						tipoConta.setCredito(nivel1==1?1:0);
						tipoConta.setIncideCpmf(0);
						tipoConta = tipoContaService.salvar(tipoConta);
						
						tipoDePara = new TipoContaContaGer();
						tipoDePara.setContaGerencial(conta);
						tipoDePara.setTipoConta(tipoConta);
						tipoDeParaService.salvar(tipoDePara);
						
					}
					
				}
			}
		}
		bufferedReader.close();
		System.out.println("Linhas :" + contLinhas);
	}
	private void geradorPlanoContasDePara(String arquivo) throws Exception
	{
		FileReader reader = null;
		BufferedReader bufferedReader = null;
		String linha = null;
		Statement st = null;
		
		try
		{
			
			Connection con = ConexaoUtil.getConexaoPadrao();
			st = con.createStatement();
			st.execute("DELETE FROM contagerencialDePara");
			//st.execute("INSERT INTO contagerencial (`idContaGerencial`, `codigoContaNivel1`, `descricao`, `contaContabil`, `codigoReduzido`) VALUES ('1', '1', 'ENTRADAS', '1', '1')");
			//st.execute("INSERT INTO contagerencial (`idContaGerencial`, `codigoContaNivel1`, `descricao`, `contaContabil`, `codigoReduzido`) VALUES ('2', '2', 'SAIDAS', '2', '2')");
			//st.close();
			//con.close();
		}
		catch(Exception ex)
		{
			throw ex;
		}
		
		ContaGerencial conta;
		ContaGerencialService contaService = new ContaGerencialService();
		
		try
		{
			reader = new FileReader("c:/desenv/manamaue/PlanoContasDePara.csv");
		}
		catch(FileNotFoundException fnfe)
		{
			throw new Exception(fnfe);	
		}
		
		int nivel1;
		int nivel2;
		int nivel3;
		int nivel4;
		String descricao;
		String contas[];
		
		bufferedReader = new BufferedReader(reader);
		String valores[];
		int contLinhas = 0; 
		int cont=0;
		
		while (bufferedReader.ready())
		{
			linha = bufferedReader.readLine();
			contLinhas++;
			if (linha.trim().equals("")==false)
			{
				valores = linha.split(";");
				nivel2 = 0;
				nivel3 = 0;
				if (valores.length>4 && valores[4].trim().equals("")==false)
				{
					cont++;
					conta = new ContaGerencial();
					//System.out.println(valores[4]);
					contas = valores[4].split("\\.");
					if (contas.length==1)
						nivel2 = Integer.parseInt(valores[0]);
					else if (contas.length==2)
					{
						nivel2 = Integer.parseInt(contas[0]);
						nivel3 = Integer.parseInt(contas[1]);
					}
					//if (contas.length>2)
						
					//nivel1 = Integer.parseInt(valores[2]);
					if (nivel2>0)
						conta.setCodigoContaNivel2(nivel2);
					if (nivel3>0)
						conta.setCodigoContaNivel3(nivel3);
					List<ContaGerencial> lista = contaService.listarPorObjetoFiltro(conta);
					if (lista.size()>0)
					{
						conta = lista.get(0);
						String str = "INSERT INTO ContaGerencialDePara VALUES (0, '" + valores[0]+"', " + conta.getId() + ", '" + valores[4]+"')";					
						st.execute(str);
					}
					else
					{
						System.out.println("Conta não foi encontrada: " + nivel2+"."+nivel3+" "+linha);
					}
				}
			}
		}
		bufferedReader.close();
		System.out.println("Linhas :" + contLinhas);
	}
	public static Date dataCacheParaJava(int dataHora)
	{
		// DATA ZERO DO CACHE - 12/31/1840


		Calendar calendario = Calendar.getInstance();
		calendario.set(Calendar.YEAR, 1840);
		calendario.set(Calendar.MONTH, 11);
		calendario.set(Calendar.DAY_OF_MONTH, 31);

		calendario.set(Calendar.HOUR_OF_DAY, 0);
		calendario.set(Calendar.MINUTE, 0);
		calendario.set(Calendar.SECOND, 0);
		calendario.set(Calendar.MILLISECOND, 0);

		//dataHora = dataHora.replaceAll("[^0-9,]", "");
		
		calendario.add(Calendar.DAY_OF_MONTH, dataHora);


		return calendario.getTime();

	}	
}
