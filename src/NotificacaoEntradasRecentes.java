
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.mail.Address;
import javax.mail.internet.InternetAddress;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.alertas.EnviarEmail;
import br.com.desenv.nepalign.relatorios.RelatorioNotificaoEntradaRecentes;
import br.com.desenv.nepalign.util.DsvConstante;


public class NotificacaoEntradasRecentes 
{
	/**
	 * @param args
	 * @throws Exception 
	 */
	static RelatorioNotificaoEntradaRecentes relatorio=  new RelatorioNotificaoEntradaRecentes();
	public static void main(String[] args) throws Exception 
	{
		//enviarEmailEntradaRecentes(args[0x00], new Date(),new Date());
		//enviarEmailGiroEntradaRecentes(args[0x00], new Date(), 15, 30);
		enviarEmailGiroEntradaRecentes(args[0x00], new Date(), 15, 30);
		//enviarEmailGiroEntradaRecentes(args[0x00], new Date(),30);
	}
	
	public static void enviarEmailEntradaRecentes(String idEmpresas,Date dataInicial,Date dataFinal) throws Exception
	{
		String caminhoArquivo ="";
		try
		{
			final byte[] pdf = relatorio.gerar(idEmpresas,dataInicial,dataFinal);
			final DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			if(DsvConstante.getParametrosSistema().get("caminhoArquivoEmialEntradaRecente")!=null)
				caminhoArquivo = DsvConstante.getParametrosSistema().get("caminhoArquivoEmialEntradaRecente");

			caminhoArquivo = caminhoArquivo+"/Entrada Mercadoria Recente "+df.format(dataInicial)+"_"+df.format(dataFinal)+".pdf";
			if(new File(caminhoArquivo).exists())
			{
				System.out.println("Arquivo já existente...");
				new File(caminhoArquivo).delete();
				System.out.println("Arquivo: " + caminhoArquivo + " foi excluido!");
			}
			
			OutputStream out = new FileOutputStream(caminhoArquivo);
			out.write(pdf,0,pdf.length);
			out.flush();
			out.close();
			
			
			final String subject = "Nepal - Entrada de Mercadorias [" + IgnUtil.dateFormat.format(new Date()) + "]!";
			final String messagem = "Olá, o relatório de mercadorias que chegaram hoje (" + IgnUtil.dateFormat.format(new Date()) + ") está disponível em anexo!\n\n\n\nPor favor não responda essa mensagem. Esse é um e-mail automático do Sistema Nepal!";
			final String from = "noreply@manamaue.com.br";
			final String password = "mana@vitoria7";
			final String smtpServer = "smtp.manamaue.com.br";
			
			final List<String> to = new ArrayList<String>();
			
			for (final String email : DsvConstante.getParametrosSistema().get("EMAIL_ENTRADA_RECENTES").split(";")) 
				to.add(email);

			Address[] addressTo = new Address[to.size()];

			for(int i = 0; i < addressTo.length; i++)
				addressTo[i] = new InternetAddress(to.get(i));

			if(addressTo.length > 0) 
			{
				System.out.println("Enviando E-mail Entrada Mercadoria.");
				new EnviarEmail().enviarEmail(from, password, smtpServer, addressTo, subject, messagem, "Sistema Nepal", caminhoArquivo);
				System.out.println("Enviou Email Entrada Mercadoria.: "+new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
			}

		}
		catch(Exception ex)
		{
			Logger log = Logger.getGlobal();
			log.fine("Erro Enviar E-Mail de log do cliente!");
			ex.printStackTrace();
		}
		finally
		{
			if(new File(caminhoArquivo).exists())
				new File(caminhoArquivo).delete();
		}
	}

	public static void enviarEmailGiroEntradaRecentes(String idEmpresas,Date dataInicial,Integer... dias) throws Exception
	{
		try
		{
			HashMap<String, byte[]> relatorios = new HashMap<>();
			
			for(int dia : dias)
				relatorios.put("Giro Mercadoria Recente de "+dia+" dias "+IgnUtil.dateFormat.format(dataInicial), relatorio.gerarGiro(idEmpresas, dataInicial, dia));
			
			final String subject = "Nepal - Giro de Mercadorias Recentes "+Arrays.toString(dias)+" Dias [" + IgnUtil.dateFormat.format(new Date()) + "]!";
			final String messagem = "Olá, o relatório de giro das mercadorias recentes "+Arrays.toString(dias)+" Dias (" + IgnUtil.dateFormat.format(new Date()) + ") está disponível em anexo!<br><br>Por favor não responda essa mensagem. <br><br>Esse é um e-mail automático do Sistema Nepal!";
			final String from = "noreply@manamaue.com.br";
			final String password = "mana@vitoria7";
			final String smtpServer = "smtp.manamaue.com.br";
			
			final List<String> to = new ArrayList<String>();
			
			for (final String email : DsvConstante.getParametrosSistema().get("EMAIL_ENTRADA_RECENTES").split(";")) 
				to.add(email);

			Address[] addressTo = new Address[to.size()];

			for(int i = 0; i < addressTo.length; i++)
				addressTo[i] = new InternetAddress(to.get(i));
		
			if(addressTo.length > 0) 
			{ 
				System.out.println("Enviando E-mail Giro Mercadoria.");
				new EnviarEmail().enviarEmail(from, password, smtpServer, addressTo, subject, messagem, "Sistema Nepal", relatorios);
				System.out.println("Enviou Email Giro Mercadoria.: "+new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
			}

		}
		catch(Exception ex)
		{
			Logger log = Logger.getGlobal();
			log.fine("Erro Enviar E-Mail de log do cliente!");
			ex.printStackTrace();
		}
	}
}
