import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.mail.Address;
import javax.mail.internet.InternetAddress;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.alertas.EnviarEmail;
import br.com.desenv.nepalign.relatorios.RelatorioNotificacaoRecebimentos;
import br.com.desenv.nepalign.util.DsvConstante;

public class NotificacaoRecebimentos {

	static RelatorioNotificacaoRecebimentos relatorio =  new RelatorioNotificacaoRecebimentos();
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception 
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DAY_OF_MONTH, -4);
		enviarEmailRecebimentos("1,2,3,4,5,6,7,8,9,10,13", new Date());

	}
	public static void enviarEmailRecebimentos(String idEmpresas,Date dataInicial) throws Exception
	{
		String caminhoArquivo ="";
		try
		{
			final byte[] pdf = relatorio.gerarRelatorioPDF(dataInicial,idEmpresas);
			final DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			if(DsvConstante.getParametrosSistema().get("caminhoArquivoEmailRecebimentos")!=null)
				caminhoArquivo = DsvConstante.getParametrosSistema().get("caminhoArquivoEmailRecebimentos");

			caminhoArquivo = caminhoArquivo+"/Recebimento "+df.format(dataInicial)+".pdf";
			if(new File(caminhoArquivo).exists())
			{
				System.out.println("Arquivo já existente...");
				new File(caminhoArquivo).delete();
				System.out.println("Arquivo: " + caminhoArquivo + " foi excluido!");
			}
			
			OutputStream out = new FileOutputStream(caminhoArquivo);
			out.write(pdf,0,pdf.length);
			out.flush();
			out.close();
			
			
			final String subject = "Nepal - Recebimentos  [" + IgnUtil.dateFormat.format(new Date()) + "]!";
			final String messagem = "Olá, o relatório de recebimentos hoje (" + IgnUtil.dateFormat.format(new Date()) + ") está disponível em anexo!\n\n\n\nPor favor não responda essa mensagem. Esse é um e-mail automático do Sistema Nepal!";
			final String from = "noreply@manamaue.com.br";
			final String password = "mana@vitoria7";
			final String smtpServer = "smtp.manamaue.com.br";
			
			final List<String> to = new ArrayList<String>();
			
			for (final String email : DsvConstante.getParametrosSistema().get("EMAIL_RECEBIMENTO").split(";")) 
				to.add(email);

			Address[] addressTo = new Address[to.size()];

			for(int i = 0; i < addressTo.length; i++)
				addressTo[i] = new InternetAddress(to.get(i));

			if(addressTo.length > 0) 
			{
				System.out.println("Enviando E-mail Recebimento.");
				new EnviarEmail().enviarEmail(from, password, smtpServer, addressTo, subject, messagem, "Sistema Nepal", caminhoArquivo);
				System.out.println("Enviou E-mail Recebimento.: "+new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
			}

		}
		catch(Exception ex)
		{
			Logger log = Logger.getGlobal();
			log.fine("Erro Enviar E-Mail de log do cliente!");
			ex.printStackTrace();
		}
		finally
		{
			if(new File(caminhoArquivo).exists())
				new File(caminhoArquivo).delete();
		}
	}
}
