import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;

public class TesteImpressaoArgoxDireto {

	public static void main(String[] args) {
		PrintService impressora;
		StringBuffer sb = new StringBuffer();
		char c = 27;
		char[] comandos = {27};

		sb.append("<STX>n");
		sb.append("<STX>M0500\n");
		sb.append("<STX>O0220\n");
		sb.append("<STX>V0\n");
		sb.append("<STX>f220\n");
		sb.append("<SOH>D\n");
		
		sb.append("<STX>L\n");
		sb.append("A2\n");
		sb.append("1911A1800170016000000\n");
		sb.append("1911A180017016200\n");
		sb.append("Q0001\n");
		sb.append("E\n");
		
		try {
			DocFlavor df = DocFlavor.SERVICE_FORMATTED.PRINTABLE;
			PrintService[] ps = PrintServiceLookup
					.lookupPrintServices(df, null);
			for (PrintService p : ps) {
				if (p.getName() != null && p.getName().contains("Nomda da Impressora")) {
					System.out
							.println("Impressora Selecionada: " + p.getName());
					System.out.println("Impressora encontrada: " + p.getName());
					impressora = p;
					DocPrintJob dpj = impressora.createPrintJob();
					String valor = sb.toString();
					InputStream stream = new ByteArrayInputStream(valor.getBytes());

					DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
					Doc doc = new SimpleDoc(stream, flavor, null);
					dpj.print(doc, null);
					
				}
				

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}


