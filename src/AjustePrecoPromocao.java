import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class AjustePrecoPromocao 
{
	public static void main(String[] args) throws IOException, SQLException 
	{
		new AjustePrecoPromocao().ajustePrecoVenda();
	}
	
	public void ajustePrecoVenda() throws IOException, SQLException
	{
		Connection con = ConexaoUtil.getConexaoPadrao();
		List<String> listaInfo = new ArrayList<String>(); 
		
		System.out.println("Fazendo a leitura do arquivo.");
		
		BufferedReader reader = new BufferedReader(new FileReader("F:/Desenv/BaixaBixo/TENIS_2.txt"));
		BufferedWriter writer = new BufferedWriter(new FileWriter("F:/Desenv/BaixaBixo/TENIS_2.old"));
		
		String readBufferLine = "";
		
		while((readBufferLine = reader.readLine()) != null) 
		{
			listaInfo.add(readBufferLine);
		}
		 
		reader.close();
		
		for(String info : listaInfo)
		{
			String[] infos = info.split(";");
			 
			String referencia = infos[0];
			String novaVenda = infos[1].replace(",", ".").replace(" ", "");
			
			for (String empresaId : "1,2,3,4,6,9,13".split(","))
			{
				String queryVendaAntiga = "SELECT venda, ep.codigoBarras from saldoEstoqueProduto sep inner join estoqueProduto ep on ep.idEstoqueProduto = sep.idEstoqueProduto where sep.idEstoqueProduto in (select idEstoqueProduto from estoqueProduto where idProduto = " + referencia + ") and mes = 7 and ano = 2015 and idEmpresaFisica = " + empresaId + ";";
				
				ResultSet rs = con.createStatement().executeQuery(queryVendaAntiga);
				
				while(rs.next())
				{
					String infoAntiga = empresaId + "," + rs.getString(1) + "," + rs.getString(2);
					
					writer.write(infoAntiga + "\n");
				}	
			}
			
			
			String query = "UPDATE saldoEstoqueProduto set venda = " + novaVenda + " where idEstoqueProduto in (select idEstoqueProduto from estoqueProduto where idProduto = " + referencia + ") and mes = 7 and ano = 2015 and idEmpresaFisica in (1,2,3,4,6,9,13);";
			
			System.out.println(query);
			
		}
		
		writer.close();
		
		System.out.println("Total de " + listaInfo.size() + " referências a serem ajustadas!");
	}
	
	public void voltaPrecoNormal() throws IOException, SQLException
	{
		Connection con = ConexaoUtil.getConexaoPadrao();
		List<String> listaInfo = new ArrayList<String>();
		
		System.out.println("Fazendo a leitura do arquivo.");
		
		BufferedReader reader = new BufferedReader(new FileReader("F:/Desenv/precosAntigos.txt"));
		
		String readBufferLine = "";
		
		while((readBufferLine = reader.readLine()) != null)
		{
			listaInfo.add(readBufferLine);
		}
		 
		reader.close();
		
		for(String info : listaInfo)
		{
			String[] infos = info.split(",");
			
			String emp = infos[0x0];
			String precoVenda = infos[0x1];
			String codigoBarras = infos[0x2];
			
			String query = "UPDATE saldoEstoqueProduto set venda = " + precoVenda + " where idEstoqueProduto = " + codigoBarras + " and mes = 7 and ano = 2015 and idEmpresaFisica = " + emp + ";";
			
			System.out.println(query);
		}
	}
}
