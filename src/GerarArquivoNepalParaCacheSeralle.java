import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.desenv.nepalign.model.Duplicata;
import br.com.desenv.nepalign.model.DuplicataParcela;
import br.com.desenv.nepalign.model.NotaFiscal;
import br.com.desenv.nepalign.service.DuplicataParcelaService;
import br.com.desenv.nepalign.service.NotaFiscalService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;


public class GerarArquivoNepalParaCacheSeralle {



	static String separador = "|";
	static Integer idEmpresaJorrovi;
	static Integer idEmpresaSeralle;
	static Date dataSeralle = null;

	public static void main(String[] args) throws Exception {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			final EntityManager manager = null;
			final EntityTransaction	transaction = null;
			List<Thread> threads = new ArrayList<>();
			dataSeralle = dateFormat.parse(DsvConstante.getParametrosSistema().get("dataSeralle"));
			if(args.length>0){
				idEmpresaSeralle = Integer.parseInt(args[0]);
				idEmpresaJorrovi = Integer.parseInt(args[1]);
			}
			else
			{
				idEmpresaSeralle = 10;
				idEmpresaJorrovi = 910;
			}
			/*class threadDuplicata implements Runnable{
				@Override
				public void run() {
					try {
						gerarArquivoDuplicataCache(manager,transaction);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}
			threads.add(new Thread(new threadDuplicata()));
		*/
			class threadParcelas implements Runnable{

				@Override
				public void run() {
					try {

						gerarArquivoParcelasCache(manager, transaction);

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			threads.add(new Thread(new threadParcelas()));
			/*
			class threadMovimentoAcordo implements Runnable{

				@Override
				public void run() {
					try {
						gerarArquivoMovimentoParcelasAcordoCache( manager, transaction);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			}
			threads.add(new Thread(new threadMovimentoAcordo()));

			class threadMovimento implements Runnable{

				@Override
				public void run() {
					try {
						gerarArquivoMovimentoParcelasCache(manager,transaction);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			}
			threads.add(new Thread(new threadMovimento()));



			class threadAgenda implements Runnable{

				@Override
				public void run() {
					try {
						gerarArquivoAgendaCache(manager,transaction);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			}
			threads.add(new Thread(new threadAgenda()));
			class threadClientes implements Runnable{

				@Override
				public void run() {
					try {
						gerarArquivoClienteCache(manager,transaction);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			}
			threads.add(new Thread(new threadClientes()));


			class threadAcordos implements Runnable{

				@Override
				public void run() {
					try {
						gerarArquivosAcordo(manager,transaction);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			}
			threads.add(new Thread(new threadAcordos()));

			class threadSeprocadosReativados implements Runnable{

				@Override
				public void run() {
					try {
						gerarArquivoSeprocadosReativados(manager,transaction);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			}
			threads.add(new Thread(new threadSeprocadosReativados()));

			class threadHistorico implements Runnable{

				@Override
				public void run() {
					try {
						gerarArquivoHistorico(manager,transaction);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			}
			threads.add(new Thread(new threadHistorico()));


			class threadBiometria implements Runnable{

				@Override
				public void run() {
					try {
						gerarArquivoBiometria(manager,transaction);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			}
			threads.add(new Thread(new threadBiometria()));


			class threadImagemUsuario implements Runnable{

				@Override
				public void run() {
					try {
						gerarArquivoImagemUsuario(manager,transaction);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			}
			threads.add(new Thread(new threadImagemUsuario()));
			*/
			for (Thread thread : threads) {
				thread.start();
			}


		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
		}
	}
	public static void gerarArquivoImagemUsuario(EntityManager manager,
			EntityTransaction transaction) throws Exception {
		Boolean transacaoIndependente = false;
		try {
			if (manager == null || transaction == null) {
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			System.out.println("Gerando Arquivo Imagem Usuario");
			PrintWriter writer = getWriter("ArquivoNepalCache_ImagemUsuario", cabecalhoImagemUsuario());

			String sql = "Select i.id,i.campoChave,i.descricao,i.infoCampoChave from ImagemUsuario i " +
					" where i.tabelaSistema.id = 3 ";
			Query query = manager.createQuery(sql);
			List<Object[]> imagens = query.getResultList();
			int size = imagens.size();
			int x = 1;
			for (Object[] imagem : imagens) {
				imprimirPorcetagem(x, size, " Imagem  ");
				x++;

				StringBuilder linha = new StringBuilder();
				linha.append(imagem[0]);
				linha.append(separador+imagem[1]);
				linha.append(separador+imagem[2]);
				linha.append(separador+imagem[3]);
				writer.println(linha);
			}

			writer.println("FIM");
			writer.close();
			System.out.println("Terminou Arquivo Imagem");

			if (transacaoIndependente)
				transaction.commit();

		} catch (Exception ex) {
			if (transacaoIndependente) {
				transaction.rollback();
				ex.printStackTrace();
				throw ex;
			}
			ex.printStackTrace();
			throw ex;
		} finally {
			if (transacaoIndependente)
				manager.close();
		}

	}
	private static String cabecalhoImagemUsuario() {
		StringBuilder sb = new StringBuilder();
		sb.append("CODIGO");
		sb.append(separador+"CAMPO");
		sb.append(separador+"DESCRICAO");
		sb.append(separador+"CLIENTE");
		return sb.toString();
	}
	public static void gerarArquivoBiometria(EntityManager manager,
			EntityTransaction transaction) throws Exception {
		Boolean transacaoIndependente = false;
		try {
			if (manager == null || transaction == null) {
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}	
			System.out.println("Gerando Arquivo Biometria");
			PrintWriter writer = getWriter("ArquivoNepalCache_Biometria", cabecalhoBiometria());

			String sql = "Select b.biometria,b.cliente.id from BiometriaCliente b ";
			Query query = manager.createQuery(sql);
			List<Object[]> biometrias = query.getResultList();

			int size = biometrias.size();
			int x = 1;
			for (Object[] biometria : biometrias) {
				imprimirPorcetagem(x, size, " Biometria  ");
				x++;
				StringBuilder linha = new StringBuilder();
				linha.append(biometria[0]);
				linha.append(separador+biometria[1]);
				writer.println(linha);
			}
			writer.println("FIM");
			writer.close();
			System.out.println("Terminou Arquivo Biometria");

			if (transacaoIndependente)
				transaction.commit();

		} catch (Exception ex) {
			if (transacaoIndependente) {
				transaction.rollback();
				ex.printStackTrace();
				throw ex;
			}
			ex.printStackTrace();
			throw ex;
		} finally {
			if (transacaoIndependente)
				manager.close();
		}

	}
	private static String cabecalhoBiometria() {
		StringBuilder sb = new StringBuilder();
		sb.append("BIOMETRIA");
		sb.append(separador+"CLIENTE");

		return sb.toString();
	}
	public static void gerarArquivoHistorico(EntityManager manager,
			EntityTransaction transaction) throws Exception {
		Boolean transacaoIndependente = false;
		try {
			if (manager == null || transaction == null) {
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			System.out.println("Gerando Aquivo Historico... ");

			SimpleDateFormat dateFormatComHora = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

			PrintWriter writer = getWriter("ArquivoNepalCache_Historico", cabecalhoHistorico());

			String sql = "Select c.dataCobranca,c.observacao,t.descricao,c.cliente.id,c.alerta " +
					" From HistoricoCobranca c " +
					" left outer join c.tipoContato t ";
			Query query = manager.createQuery(sql);
			List<Object[]> historicos = query.getResultList();

			int size = historicos.size();
			int x = 1;

			for (Object[] historico : historicos) {
				imprimirPorcetagem(x, size, " Historico  ");
				x++;

				StringBuilder linha = new StringBuilder();
				String data = "null";
				if(historico[0] != null){

					data = dateFormatComHora.format((Date)historico[0]);
				}
				linha.append(data);
				linha.append(separador+historico[1]);
				linha.append(separador+historico[2]);
				linha.append(separador+historico[3]);
				linha.append(separador+historico[4]);

				writer.println(linha.toString());
			}
			writer.println("FIM");
			writer.close();
			System.out.println("Terminou Arquivo Historico");

			if (transacaoIndependente)
				transaction.commit();

		} catch (Exception ex) {
			if (transacaoIndependente) {
				transaction.rollback();
				ex.printStackTrace();
				throw ex;
			}
			ex.printStackTrace();
			throw ex;
		} finally {
			if (transacaoIndependente)
				manager.close();
		}

	}
	private static String cabecalhoHistorico() {
		StringBuilder sb = new StringBuilder();
		sb.append("DATA");
		sb.append(separador+"HISTORICO");
		sb.append(separador+"TIPO CONTATO");
		sb.append(separador+"CLIENTE");
		sb.append(separador+"ALERTA(1=SIM 0=não)");

		return sb.toString();
	}
	public static void  gerarArquivoSeprocadosReativados(EntityManager manager,
			EntityTransaction transaction) throws Exception {
		Boolean transacaoIndependente = false;
		try {
			if (manager == null || transaction == null) {
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}

			System.out.println("Gerando Arquivo Seprocados Reativados...");
			SimpleDateFormat dateFormatComHora = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
			DuplicataParcelaService parcelaService = new DuplicataParcelaService();
			PrintWriter writer = getWriter("ArquivoNepalCache_SeprocadosReativados", cabecalhoSeprocadosReativados());

			String sql ="Select s.dataNova,s.seprocadoReativado,c.id, " +
					" p.numeroDuplicata,p.numeroParcela," +
					" d.id,d.dataCompra, " +
					" e.id,s.observacao,s.impresso,s.dataImpressao, " +
					" p.acordo " +
					" From SeprocadoReativados s " +
					" left outer join s.empresaFisica e " +
					" left outer join s.cliente c " +
					" left outer join s.duplicataParcela p " +
					" left outer join p.duplicata d ";

			Query query = manager.createQuery(sql);
			List<Object[]> seprocados = query.getResultList();

			int size = seprocados.size();
			int x = 1;

			for (Object[] seprocado : seprocados) {
				imprimirPorcetagem(x, size, " Seprocados  ");
				x++;
				StringBuilder linha = new StringBuilder();
				linha.append(dateFormatComHora.format((Date)seprocado[0]));
				linha.append(separador+seprocado[1]);
				linha.append(separador+seprocado[2]);
				linha.append(separador+seprocado[3]);
				linha.append(separador+seprocado[4]);
				if(seprocado[5] != null){

					Date dataCompra = (Date)seprocado[6];

					if(seprocado[11] != null && seprocado[11].equals("S")){
						Duplicata dup = new Duplicata();
						dup.setDataCompra(dataCompra);
						dup.setId((Integer) seprocado[5]);
						DuplicataParcela parcela = new DuplicataParcela();
						parcela.setDuplicata(dup);
						//boolean seralle = parcelaService.verificaLojaParcelaAcordo(parcela, dataSeralle, manager, transaction);
						boolean seralle = true;
						linha.append(separador+(seralle?idEmpresaSeralle:idEmpresaJorrovi));
					}
					else{
						linha.append(separador+((dataCompra.getTime() >= dataSeralle.getTime())?idEmpresaSeralle:idEmpresaJorrovi));
					}
				}
				else{
					linha.append(separador+seprocado[7]);
				}

				linha.append(separador+seprocado[8]);
				linha.append(separador+seprocado[9]);
				String dataImpressao = seprocado[10] == null 
						? "null"
								: dateFormatComHora.format((Date)seprocado[10]);

				linha.append(separador+dataImpressao);
				writer.println(linha.toString());
			}
			writer.println("FIM");
			writer.close();
			System.out.println("Terminou Arquivo Seprocados Reativados");
			if (transacaoIndependente)
				transaction.commit();

		} catch (Exception ex) {
			if (transacaoIndependente) {
				transaction.rollback();
				ex.printStackTrace();
				throw ex;
			}
			ex.printStackTrace();
			throw ex;
		} finally {
			if (transacaoIndependente)
				manager.close();
		}

	}
	private static String cabecalhoSeprocadosReativados() {
		StringBuilder sb = new StringBuilder();
		sb.append("DATA");
		sb.append(separador+"REATIVADO(R)/SEPROCADO(S)");
		sb.append(separador+"CLIENTE");
		sb.append(separador+"NUMERO DUPLICATA");
		sb.append(separador+"NUMERO PARCELA");
		sb.append(separador+"LOJA");
		sb.append(separador+"OBSERVACAO");
		sb.append(separador+"IMPRESSO");
		sb.append(separador+"DATA IMPRESSAO");


		return sb.toString();
	}
	private static void gerarArquivosAcordo(EntityManager manager, EntityTransaction transaction) throws Exception {
		Boolean transacaoIndependente = false;
		try {
			if (manager == null || transaction == null) {
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			System.out.println("Gerando arquivo de acordos...");
			PrintWriter writer = getWriter("ArquivoNepalCache_Acordo", cabecalhoDuplicataAcordo());

			String sql ="Select a.empresaFisica.id,a.cliente.id,a.numeroNovaDuplicata," +
					"a.numeroDuplicataAntiga, a.numeroParcelaDuplicataAntiga" +
					" From DuplicataAcordo a ";

			Query query = manager.createQuery(sql);

			List<Object[]> acordos = query.getResultList();
			int size = acordos.size();
			int x = 1;
			for (Object[] duplicataAcordo : acordos) {
				imprimirPorcetagem(x, size, " Acordo ");
				x++;
				StringBuilder linha = new StringBuilder();
				linha.append(duplicataAcordo[0]);
				linha.append(separador+duplicataAcordo[1]);
				linha.append(separador+duplicataAcordo[2]);
				linha.append(separador+duplicataAcordo[3]);
				linha.append(separador+duplicataAcordo[4]);
				writer.println(linha);

			}
			writer.println("FIM");
			writer.close();
			System.out.println("Terminou Arquivo Acordo");
			if (transacaoIndependente)
				transaction.commit();

		} catch (Exception ex) {
			if (transacaoIndependente) {
				transaction.rollback();
				ex.printStackTrace();
				throw ex;
			}
			ex.printStackTrace();
			throw ex;
		} finally {
			if (transacaoIndependente)
				manager.close();
		}

	}
	private static String cabecalhoDuplicataAcordo() {
		StringBuilder sb = new StringBuilder();
		sb.append("EMPRESA");
		sb.append(separador+"CLIENTE");
		sb.append(separador+"DUPLICATA NOVA");
		sb.append(separador+"DUPLICATA ANTIGA");
		sb.append(separador+"PARCECLA ANTIGA");

		return sb.toString();
	}
	private static void gerarArquivoClienteCache(EntityManager manager, EntityTransaction transaction) throws Exception {
		Boolean transacaoIndependente = false;
		try {
			if (manager == null || transaction == null) {
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			System.out.println("Gerando arquivo de Cliente...");

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

			PrintWriter writer = getWriter("ArquivoNepalCache_Cliente", cabecalhoCliente());
			//					  0		1		2			3		4		5
			String sql ="Select c.id,c.nome,c.logradouro,c.cep,cid.nome,ufcid.sigla, " +
					//	6			7		8		9				10
					"c.cpfCnpj,c.bairro,con.nome,c.dataNascimento,c.telefoneCelular01, " +
					//			11		12				13					14
					"c.salario,c.estadoCivil,c.rgInscricaoEstadual,c.empresaTrabalha, " +
					//			15			16					17					18
					"cidc.nome,c.logradouroComercial,c.telefoneComercial,c.profissao, " +
					//			19			20		21				22
					"c.nomePai,c.nomeMae,c.observacao,c.informacaoComercial, " +
					//			23		24					25					26
					"con.rg,con.dataNascimento,con.empresaTrabalha,con.profissao, " +
					//			27						28						29			30
					"con.logradouroComercial,con.telefoneComercial,con.salario,c.outraRenda " +
					" from Cliente c " +
					"left outer join c.conjuge con " +
					"left outer join c.cidadeEndereco cid " +
					"left outer join cid.uf ufcid " +
					"left outer join c.cidadeComercial cidc" ;


			Query query = manager.createQuery(sql);
			List<Object[]> clientes = query.getResultList();


			int size = clientes.size();
			int x =1;
			for (Object[] cliente : clientes) {
				imprimirPorcetagem(x, size, " Clientes ");
				x++;
				StringBuilder linha = new StringBuilder();
				linha.append(cliente[0]);
				linha.append(separador+cliente[1]);
				linha.append(separador+cliente[2]);
				linha.append(separador+cliente[3]);
				if(cliente[4] != null){
					linha.append(separador+cliente[4]);
					linha.append(separador+cliente[5]);

				}else{
					linha.append(separador+"0");
					linha.append(separador+"0");
				}
				linha.append(separador+cliente[6]);
				linha.append(separador+cliente[7]);

				if(cliente[8] != null)
					linha.append(separador+cliente[8]);
				else
					linha.append(separador+"null");
				//Arrumar aqui
				String dataNacimento = (cliente[9]==null?"null":dateFormat.format((Date)cliente[9]));
				linha.append(separador+dataNacimento);
				linha.append(separador+(cliente[10]==null?0:cliente[10]));

				BigDecimal renda = new BigDecimal(0);
				renda.add(new BigDecimal(cliente[11] == null ? 0.0 : (Double)cliente[11]));
				linha.append(separador+(renda.setScale(2, BigDecimal.ROUND_HALF_EVEN)));
				linha.append(separador+cliente[12]);
				linha.append(separador+cliente[13]);
				linha.append(separador+cliente[14]);
				String cidadeComercial = (cliente[15]==null?"0":cliente[15].toString());
				linha.append(separador+cidadeComercial);
				linha.append(separador+cliente[16]);
				linha.append(separador+cliente[17]);
				linha.append(separador+cliente[18]);
				linha.append(separador+cliente[30]);
				linha.append(separador+cliente[19]);
				linha.append(separador+cliente[20]);
				linha.append(separador+cliente[21]);
				linha.append(separador+cliente[22]);
				linha.append(separador+"");
				linha.append(separador+"");
				linha.append(separador+"");

				if(cliente[8] != null){
					linha.append(separador+cliente[23]);
					String dataNacimentoConjuge = cliente[24]==null?"0":dateFormat.format((Date)cliente[24]);
					linha.append(separador+dataNacimentoConjuge);
					linha.append(separador+cliente[25]);
					linha.append(separador+cliente[26]);
					linha.append(separador+cliente[27]);
					linha.append(separador+cliente[28]);
					Double salario = cliente[29] == null ? 0.0 : (Double)cliente[29];
					linha.append(separador+new BigDecimal(salario).setScale(2, BigDecimal.ROUND_HALF_EVEN));
				}
				else{
					linha.append(separador+"null");
					linha.append(separador+"null");
					linha.append(separador+"null");				
					linha.append(separador+"null");
					linha.append(separador+"null");
					linha.append(separador+"null");
					linha.append(separador+"null");
				}


				String sqlEmail ="Select e.email from EmailCliente e where e.cliente.id = "+cliente[0];

				TypedQuery<String> queryEmail = manager.createQuery(sqlEmail, String.class);
				queryEmail.setMaxResults(3);
				List<String> emails = queryEmail.getResultList();

				for (String mail : emails) { 
					linha.append(separador+mail);
				}

				linha.append(separador+"0");
				linha.append(separador+"0");
				writer.println(linha);
			}
			writer.println("FIM");
			writer.close();
			System.out.println("Terminou Arquivo Cliente");
			if (transacaoIndependente)
				transaction.commit();

		} catch (Exception ex) {
			if (transacaoIndependente) {
				transaction.rollback();
				ex.printStackTrace();
				throw ex;
			}
			ex.printStackTrace();
			throw ex;
		} finally {
			if (transacaoIndependente)
				manager.close();
		}

	}
	private static String cabecalhoCliente() {

		StringBuilder sb = new StringBuilder();

		sb.append("Codigo");
		sb.append(separador+"NOME");
		sb.append(separador+"ENDERECO");
		sb.append(separador+"CEP");
		sb.append(separador+"CIDADE");
		sb.append(separador+"ESTADO");
		sb.append(separador+"CPF");
		sb.append(separador+"BAIRRO");
		sb.append(separador+"CONJUGE");
		sb.append(separador+"DATA NASCIMENTO");
		sb.append(separador+"TELEFONE");
		sb.append(separador+"RENDA");
		sb.append(separador+"ESTADO CIVIL");
		sb.append(separador+"RG");
		sb.append(separador+"EMPREGO");
		sb.append(separador+"CIDADE EMPREGO");
		sb.append(separador+"ENDERECO COMERCIAL");
		sb.append(separador+"TELEFONE COMERCIAL");
		sb.append(separador+"PROFISSAO");
		sb.append(separador+"OUTRA RENDA");
		sb.append(separador+"PAI");
		sb.append(separador+"MAE");
		sb.append(separador+"OBSERVACAO1");
		sb.append(separador+"OBSERVACAO2");
		sb.append(separador+"OBSERVACAO3");
		sb.append(separador+"OBSERVACAO4");
		sb.append(separador+"OBSERVACAO5");
		sb.append(separador+"RG_CONJUGE");
		sb.append(separador+"DATA_NASCIMENTO_CONJUGE");
		sb.append(separador+"EMPRESA_TRABALHO_CONJUGE");
		sb.append(separador+"PROFISSAO_CONJUGE");
		sb.append(separador+"ENDERECO_TRABALHO_CONJUGE");
		sb.append(separador+"TELEFONE_COMERCIAL_CONJUGE");
		sb.append(separador+"SALARIO_CONJUGE");
		sb.append(separador+"EMAIL1");
		sb.append(separador+"EMAIL2");
		sb.append(separador+"EMAIL3");
		sb.append(separador+"NATURAL");
		sb.append(separador+"ORGAO_EXP_RG");

		return sb.toString();
	}
	private static void gerarArquivoAgendaCache(EntityManager manager, EntityTransaction transaction) throws Exception {
		Boolean transacaoIndependente = false;
		try {
			if (manager == null || transaction == null) {
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			System.out.println("Gerando arquivo de Agenda...");
			PrintWriter writer = getWriter("ArquivoNepalCache_Agenda",cabecalhoAgenda());

			String sql = "Select a.id,a.nome,a.cidade,a.celular1,a.celular2," +
					"a.emailPessoal,a.emailComercial,a.telefoneComecial,a.telefoneResidencial " +
					"from Agenda a";


			Query query = manager.createQuery(sql);
			List<Object[]> agendas = query.getResultList();

			int size = agendas.size(),x=1;
			for (Object[] agenda : agendas) {
				imprimirPorcetagem(x, size, " Agenda ");
				x++;
				StringBuilder linha = new StringBuilder();
				linha.append(agenda[0]);
				linha.append(separador+agenda[1]);
				linha.append(separador+agenda[2]);
				linha.append(separador+agenda[3]);
				linha.append(separador+agenda[4]);
				linha.append(separador+agenda[5]);
				linha.append(separador+agenda[6]);
				linha.append(separador+agenda[7]);
				linha.append(separador+agenda[8]);
				writer.println(linha.toString());
			}
			writer.println("FIM");
			writer.close();
			System.out.println("Terminou Arquivo Agenda");
			if (transacaoIndependente)
				transaction.commit();

		} catch (Exception ex) {
			if (transacaoIndependente) {
				transaction.rollback();
				ex.printStackTrace();
				throw ex;
			}
			ex.printStackTrace();
			throw ex;
		} finally {
			if (transacaoIndependente)
				manager.close();
		}

	}
	private static String cabecalhoAgenda() {

		StringBuilder sb = new StringBuilder();
		sb.append("Codigo");
		sb.append(separador+"NOME");
		sb.append(separador+"CIDADE");
		sb.append(separador+"CELULAR1");
		sb.append(separador+"CELULAR2");
		sb.append(separador+"EMAIL PESSOAL");
		sb.append(separador+"EMAIL COMERCIAL");
		sb.append(separador+"TELEFONE COMERCIAL");
		sb.append(separador+"TELEFONE RESIDENCIAL");

		return sb.toString();
	}
	private static PrintWriter getWriter(String nomeArquivo,String cabecalho) throws IOException{
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		File file = new File("C:\\desenv\\"+nomeArquivo+" "+df.format(new Date()));
		if(file.exists())
			file.delete();
		// 10 serrale , 910 jorrovi 
		FileWriter fileWriter = new FileWriter(file);
		PrintWriter writer = new PrintWriter(fileWriter);
		writer.println(cabecalho);
		return writer;
	}
	private static void gerarArquivoDuplicataCache( EntityManager manager, EntityTransaction transaction) throws Exception{
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			//                               0			1					2						3
			String sql = "select d.empresaFisica.id, d.cliente.id, p.numeroControle ,p.id, " +
					//			4					5			6								7				8		
					"d.numeroDuplicata,d.dataCompra,v.codigoVendedorCache,d.valorTotalCompra,d.valorEntrada," +
					//9			10		   11
					"d.jorrovi,v.id,d.id from Duplicata d left outer join d.pedidoVenda p left outer join d.vendedor v";
			Query query = manager.createQuery(sql);
			List<Object[]> duplicatas = query.getResultList();
			System.out.println("Gerando arquivo de duplicatas... ");
			DuplicataParcelaService parcelaService = new DuplicataParcelaService();
			PrintWriter writer = getWriter("ArquivoNepalCache_Duplicata", cabecalhoDuplicata());
			PrintWriter writerVendaDuplicata = getWriter("ArquivoNepalCache_VendaDuplicata",cabecalhoVendaDuplicata());
			int size = duplicatas.size()+1;
			int x=1;
			for (Object[] duplicata : duplicatas) {
				imprimirPorcetagem(x, size, " Duplicata ");
				x++;
				if(duplicata[3]!=null){
					StringBuilder linhaVendaDuplicata = new StringBuilder();
					linhaVendaDuplicata.append(duplicata[4]);
					linhaVendaDuplicata.append(separador+duplicata[2]);
					writerVendaDuplicata.println(linhaVendaDuplicata);
				}
				StringBuilder linha = new StringBuilder();

				Date dataCompra= null;
				dataCompra  =(Date)duplicata[5];
				if(duplicata[9] !=null ){
					linha.append((Integer)duplicata[9] ==1?910:10);
				}
				else{			

					if((Integer)duplicata[10] !=0){

						linha.append(separador+((dataCompra.getTime() >= dataSeralle.getTime())?idEmpresaSeralle:idEmpresaJorrovi));
					}
					else{
						Duplicata dup = new Duplicata();
						dup.setDataCompra(dataCompra);
						dup.setId((Integer) duplicata[11]);
						DuplicataParcela parcela = new DuplicataParcela();
						parcela.setDuplicata(dup);
						//boolean seralle = parcelaService.verificaLojaParcelaAcordo(parcela, dataSeralle, manager, transaction);
						boolean seralle = true;
						linha.append(separador+(seralle?idEmpresaSeralle:idEmpresaJorrovi));
					}
				}
				linha.append(separador+duplicata[1]);
				linha.append(separador+duplicata[4]);

				linha.append(separador+dataCompra==null?"0":dateFormat.format(dataCompra));

				if(duplicata[3] != null){
					NotaFiscal notaFiscal = buscarNotaPorPedido((Integer) duplicata[3],manager,transaction);
					linha.append(separador+(notaFiscal==null?0:notaFiscal.getNumero()));
				}
				else{
					linha.append(separador+0);
				}
				if(duplicata[6]!=null)
					linha.append(separador+duplicata[6]);
				else
					linha.append(separador+"null");

				linha.append(separador+new BigDecimal((Double)duplicata[7]).setScale(2,BigDecimal.ROUND_HALF_EVEN).toString());
				BigDecimal valorEntrada = new BigDecimal((duplicata[8]==null?0.0:(Double)duplicata[8])).setScale(2,BigDecimal.ROUND_HALF_EVEN);
				linha.append(separador+valorEntrada);
				String valorPrestacoes = new BigDecimal((Double)duplicata[7]-valorEntrada.doubleValue()).setScale(2,BigDecimal.ROUND_HALF_EVEN).toString();
				linha.append(separador+valorPrestacoes);
				writer.println(linha.toString());
			}
			writerVendaDuplicata.printf("FIM");
			writerVendaDuplicata.close();
			writer.printf("FIM");
			writer.close();
			System.out.println("Terminou arquivo Duplicata");

		}catch(Exception ex)
		{
			if(transacaoIndependente){
				transaction.rollback();
				ex.printStackTrace();
				throw ex;
			}
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	private static String cabecalhoVendaDuplicata() {
		StringBuilder sb = new StringBuilder();
		sb.append("NUMERO DUPLICATA");
		sb.append(separador+"NUMERO CONTROLE" );
		return sb.toString();
	}
	private static String cabecalhoDuplicata() {
		StringBuilder builder = new StringBuilder();
		builder.append("LOJA");
		builder.append(separador+"CLIENTE");
		builder.append(separador+"NUMERO DUPLICATA");
		builder.append(separador+"DATA");
		builder.append(separador+"NOTA FISCAL");
		builder.append(separador+"VENDEDOR");
		builder.append(separador+"VALOR TOTAL COMPRA");
		builder.append(separador+"VALOR ENTRADA");
		builder.append(separador+"TOTAL PRESTACOES");
		return builder.toString();
	}
	private static NotaFiscal buscarNotaPorPedido(Integer pedidoVenda, EntityManager manager, EntityTransaction transaction) throws Exception{
		try {

			List<NotaFiscal> notas = new NotaFiscalService().listarPaginadoSqlLivreGenerico(manager, Integer.MAX_VALUE, 1, " idPedidovenda = "+pedidoVenda, null, null).getRecordList();
			if(notas.size()>0)
				return notas.get(0);
			else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	private static String cabecalhoParcela(){
		StringBuilder sb = new StringBuilder();
		sb.append("LOJA");
		sb.append(separador+"CLIENTE");
		sb.append(separador+"NUMERO DUPLICATA");
		sb.append(separador+"NUMERO PARCELA");
		sb.append(separador+"DATA VENCIMENTO");
		sb.append(separador+"VALOR");
		sb.append(separador+"SITUACAO");
		sb.append(separador+"VALOR PAGO");
		sb.append(separador+"DATA BAIXA");
		sb.append(separador+"SEGUNDO VENCIMENTO");
		sb.append(separador+"ACORDO");


		return sb.toString();
	}
	private static void gerarArquivoParcelasCache(EntityManager manager, EntityTransaction transaction) throws Exception{
		Boolean transacaoIndependente = false;
		try {
			if (manager == null || transaction == null) {
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			System.out.println("Gerando arquivo de Parcelas... ");
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			DuplicataParcelaService parcelaService = new DuplicataParcelaService();
			PrintWriter writer = getWriter("ArquivoNepalCache_Parcelas", cabecalhoParcela());
			//						0					1			2				3			
			String sql ="Select p.empresaFisica.id,p.cliente.id,p.numeroDuplicata,p.numeroParcela," +
					//4					5				6			7			8			9
					"p.dataVencimento,p.valorParcela,p.situacao,p.valorPago,p.dataBaixa,p.dataVencimentoAcordado," +
					//10		11				12						13
					"p.acordo,p.duplicata.id,p.duplicata.dataCompra,p.jurosPago from DuplicataParcela p";

			Query query = manager.createQuery(sql);
			List<Object[]> parcelas = query.getResultList();

			int size = parcelas.size();
			int x =1;
			Double valorAbertoJorrovi = 0.0;
			Double valorAbertoSerrale = 0.0;
			for (Object[] parcela : parcelas) {
				imprimirPorcetagem(x, size, " Parcelas ");
				x++;
				StringBuilder linha = new StringBuilder();
				Date dataCompra = (Date)parcela[12];
				Date dataVencimento = (Date)parcela[4];
				Date dataBaixa = parcela[8]==null?null:(Date)parcela[8];
				Date data02Vencimento = parcela[9]==null?null:(Date)parcela[9];
				Integer empresa = null;
				
				if(parcela[10] != null && parcela[10].equals("S")){
					Duplicata dup = new Duplicata();
					dup.setDataCompra(dataCompra);
					dup.setId((Integer) parcela[11]);
					DuplicataParcela par = new DuplicataParcela();
					par.setDuplicata(dup);
					//boolean seralle = parcelaService.verificaLojaParcelaAcordo(par, dataSeralle, manager, transaction);
					boolean seralle = true;
					empresa = (seralle?idEmpresaSeralle:idEmpresaJorrovi);
					linha.append(separador+empresa);
				}
				else
				{
					empresa = ((dataCompra.getTime() >= dataSeralle.getTime())?idEmpresaSeralle:idEmpresaJorrovi);
					linha.append(separador+empresa);
				}
				if(parcela[6].equals("0") || parcela[6].equals("2"))
				{
					DuplicataParcela par = new DuplicataParcela();
					par.setValorParcela((Double)parcela[5]);
					par.setDataBaixa((Date)parcela[8]);
					par.setSituacao(parcela[6].toString());
					par.setJurosPago((Double)parcela[13]);
					par.setValorPago((Double)parcela[7]);
					if(empresa.intValue() == idEmpresaSeralle.intValue() )
						valorAbertoSerrale += par.getValorParcelaSemJuros().doubleValue();
					else
						valorAbertoJorrovi += par.getValorParcelaSemJuros().doubleValue();
				}
					
				linha.append(separador+parcela[1]);
				linha.append(separador+parcela[2]);
				linha.append(separador+parcela[3]);

				if(parcela[6].equals("2"))
					linha.append(separador+(dataBaixa==null?dateFormat.format(dataVencimento):dateFormat.format(dataBaixa)));
				else
					linha.append(separador+dateFormat.format(dataVencimento));

				linha.append(separador+new BigDecimal((Double)parcela[5]).setScale(2,BigDecimal.ROUND_HALF_EVEN).toString());
				linha.append(separador+parcela[6]);
				Double valorPago = parcela[7] == null?0.0:(Double)parcela[7];
				linha.append(separador+new BigDecimal(valorPago).setScale(2,BigDecimal.ROUND_HALF_EVEN).toString());
				linha.append(separador+(dataBaixa==null?"0":dateFormat.format(dataBaixa)));

				String segundadata = data02Vencimento== null?"0":dateFormat.format(data02Vencimento);
				linha.append(separador+segundadata);
				linha.append(separador+(parcela[10] == null?"N":parcela[10]));
				writer.println(linha);
			}
			writer.println("FIM");
			writer.println("Valor Serrale: "+new BigDecimal(valorAbertoSerrale).setScale(2,BigDecimal.ROUND_HALF_EVEN));
			writer.println("Valor Jorrovi: "+new BigDecimal(valorAbertoJorrovi).setScale(2,BigDecimal.ROUND_HALF_EVEN));
			writer.close();
			System.out.println("Terminou Arquivo Parcelas");

			if (transacaoIndependente)
				transaction.commit();

		}
		catch (Exception ex) {
			if (transacaoIndependente) {
				transaction.rollback();
				ex.printStackTrace();
				throw ex;
			}
			ex.printStackTrace();
			throw ex;
		} finally {
			if (transacaoIndependente)
				manager.close();
		}
	}
	private static String cabecalhoMovimentoParcela(){
		//idDuplicata,idDuplicataParcela,idEmpresaFisica,idCliente,idUsuario,dataMovimento,tipoPagamento,valorCompraPago,valorEntradaJuros,valorRecebido,
		//dataVencimento,valorParcela,historico,idFormaPagamento,valorDesconto,horaMovimento

		StringBuilder sb = new StringBuilder();
		sb.append("NUMERO DUPLICATA");
		sb.append(separador+"NUMERO PARCELA");
		sb.append(separador+"LOJA");
		sb.append(separador+"CLIENTE");
		sb.append(separador+"USUARIO");
		sb.append(separador+"DATA MOVIMENTO");
		sb.append(separador+"TIPO PAGAMENTO");
		sb.append(separador+"VALOR COMPRA PAGO");
		sb.append(separador+"JUROS");
		sb.append(separador+"VALOR RECEBIDO");
		sb.append(separador+"DATA VENCIMENTO");
		sb.append(separador+"VALOR PARCELA");
		sb.append(separador+"HISTORICO");
		sb.append(separador+"FORMA PAGAMENTO");
		sb.append(separador+"DESCONTO");
		sb.append(separador+"DATA HORA MOVIMENTO");
		return sb.toString();
	}
	private static void gerarArquivoMovimentoParcelasCache(EntityManager manager, EntityTransaction transaction) throws Exception{
		Boolean transacaoIndependente = false;
		try {
			if (manager == null || transaction == null) {
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			System.out.println("Gerando arquivo de Movimento... ");
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat dateFormatHora = new SimpleDateFormat("hh:mm:ss");
			DuplicataParcelaService parcelaService = new DuplicataParcelaService();
			PrintWriter writer = getWriter("ArquivoNepalCache_MovimentoParcela" , cabecalhoMovimentoParcela());
			//									0					1				2					3			
			String sql = "Select m.numeroDuplicata,m.numeroParcela,m.empresaFisica.id,m.cliente.id," +
					//4			5			6					7				8
					" u.nome,m.dataMovimento,m.tipoPagamento,m.valorCompraPago,m.valorEntradaJuros," +
					//9						10					11							12			13			
					" m.valorRecebido,m.dataVencimento,m.duplicataParcela.valorParcela,m.historico,f.descricao," +
					//14				15					16
					" m.valorDesconto,m.horaMovimento,m.duplicataParcela.acordo, " +
					//		17					18
					" m.duplicata.dataCompra,m.duplicata.id "+
					" From MovimentoParcelaDuplicata m left outer join m.usuario u left outer join m.formaPagamento f";

			Query query = manager.createQuery(sql);
			List<Object[]> movimentos = query.getResultList();

			int size = movimentos.size();
			int x= 1;
			for (Object[] movimento : movimentos) {

				imprimirPorcetagem(x, size, " Movimentos ");
				x++;
				StringBuilder linha = new StringBuilder();
				linha.append(movimento[0]);
				linha.append(movimento[1]);
				if(movimento[16]!= null && movimento[16].equals("S")){
					Duplicata dup = new Duplicata();
					dup.setDataCompra((Date)movimento[17]);
					dup.setId((Integer) movimento[18]);
					DuplicataParcela parcela = new DuplicataParcela();
					parcela.setDuplicata(dup);
					//boolean seralle = parcelaService.verificaLojaParcelaAcordo(parcela, dataSeralle, manager, transaction);
					boolean seralle = true;
					linha.append(separador+(seralle?idEmpresaSeralle:idEmpresaJorrovi));
				}
				else
				{
					linha.append(separador+((((Date)movimento[17]).getTime() >= dataSeralle.getTime())?idEmpresaSeralle:idEmpresaJorrovi));
				}

				linha.append(separador+movimento[3]);
				linha.append(separador+movimento[4]);
				linha.append(separador+dateFormat.format((Date)movimento[5]));
				linha.append(separador+movimento[6]);
				linha.append(separador+new BigDecimal((Double)movimento[7]).setScale(2,BigDecimal.ROUND_HALF_EVEN));
				linha.append(separador+new BigDecimal((movimento[8]==null?0.0:(Double)movimento[8])).setScale(2,BigDecimal.ROUND_HALF_EVEN));
				linha.append(separador+new BigDecimal((Double)movimento[9]).setScale(2,BigDecimal.ROUND_HALF_EVEN));
				linha.append(separador+dateFormat.format((Date)movimento[10]));
				linha.append(separador+new BigDecimal((Double)movimento[11]).setScale(2,BigDecimal.ROUND_HALF_EVEN));
				linha.append(separador+movimento[12]);
				linha.append(separador+movimento[13]);
				linha.append(separador+new BigDecimal((movimento[14]==null?0.0:(Double)movimento[14])).setScale(2,BigDecimal.ROUND_HALF_EVEN));
				linha.append(separador+dateFormatHora.format((Date)movimento[15]));
				writer.println(linha);
			}
			writer.println("FIM");
			writer.close();
			System.out.println("Terminou Arquivo Movimento Parcela.");

			if (transacaoIndependente)
				transaction.commit();

		} catch (Exception ex) {
			if (transacaoIndependente) {
				transaction.rollback();
				ex.printStackTrace();
				throw ex;
			}
			ex.printStackTrace();
			throw ex;
		} finally {
			if (transacaoIndependente)
				manager.close();
		}
	}
	private static void gerarArquivoMovimentoParcelasAcordoCache(EntityManager manager, EntityTransaction transaction) throws Exception{
		Boolean transacaoIndependente = false;
		try {
			if (manager == null || transaction == null) {
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}

			System.out.println("Gerando arquivo de Movimento Acordo...");

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat dateFormatComHora = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

			DuplicataParcelaService parcelaService = new DuplicataParcelaService();
			PrintWriter writer = getWriter("ArquivoNepalCache_MovimentoParcelaAcordo" , cabecalhoMovimentoParcela());
			//			  		  0		1			2			3	
			String sql ="Select p.id,p.valorPago,p.jurosPago,p.valorDesconto," +
					//		4				5				6		7
					"p.numeroDuplicata,p.numeroParcela,p.acordo,p.duplicata.id," +
					//           8				9			10	 	11
					"p.duplicata.dataCompra,p.cliente.id,u.nome,p.dataBaixa," +
					//		12			13
					"p.valorParcela,p.dataVencimento From DuplicataParcela p " +
					"left outer join p.usuario u where p.situacao = 3";

			Query query = manager.createQuery(sql);
			List<Object[]> parcelas = query.getResultList();
			int size = parcelas.size();
			int x =1;
			for (Object[] parcela : parcelas) {
				imprimirPorcetagem(x, size, "Movimento de Acordo");
				x++;
				BigDecimal valorMovimento = new BigDecimal(0);
				BigDecimal valorJuros = new BigDecimal(0);
				BigDecimal valorDesconto = new BigDecimal(0);

				String sqlMovimentos ="Select m.valorRecebido,m.valorEntradaJuros,m.valorDesconto from MovimentoParcelaDuplicata m " +
						" where m.duplicataParcela.id =  "+((Integer)parcela[0]);

				Query queryMovimentos = manager.createQuery(sqlMovimentos);
				List<Object[]> movimentos = queryMovimentos.getResultList();

				for (Object[] movimento : movimentos) {
					valorMovimento = valorMovimento.add(new BigDecimal(movimento[0]==null?0.0:(Double)movimento[0])).setScale(2, BigDecimal.ROUND_HALF_EVEN);
					valorJuros = valorJuros.add(new BigDecimal(movimento[1]==null?0.0:(Double)movimento[1])).setScale(2, BigDecimal.ROUND_HALF_EVEN);
					valorDesconto = valorDesconto.add(new BigDecimal(movimento[2]==null?0.0:(Double)movimento[2])).setScale(2, BigDecimal.ROUND_HALF_EVEN);
				}

				valorMovimento = new BigDecimal(parcela[1]==null?0.0:(Double)parcela[1]).subtract(valorMovimento).setScale(2, BigDecimal.ROUND_HALF_EVEN);
				valorJuros = new BigDecimal(parcela[2]==null?0:(Double)parcela[2]).subtract(valorJuros).setScale(2, BigDecimal.ROUND_HALF_EVEN);
				valorDesconto = new BigDecimal(parcela[3]==null?0:(Double)parcela[3]).subtract(valorDesconto).setScale(2,BigDecimal.ROUND_HALF_EVEN);

				StringBuilder linha = new StringBuilder();
				linha.append(parcela[4]);
				linha.append(parcela[5]);

				if(parcela[6] != null && parcela[6].equals("S")){
					Duplicata dup = new Duplicata();
					dup.setDataCompra((Date)parcela[8]);
					dup.setId((Integer) parcela[7]);
					DuplicataParcela parc = new DuplicataParcela();
					parc.setDuplicata(dup);
					//boolean seralle = parcelaService.verificaLojaParcelaAcordo(parc, dataSeralle, manager, transaction);
					boolean seralle= true;
					linha.append(separador+(seralle?idEmpresaSeralle:idEmpresaJorrovi));
				}
				else
				{
					linha.append(separador+((((Date)parcela[8]).getTime() >= dataSeralle.getTime()?idEmpresaSeralle:idEmpresaJorrovi)));
				}

				linha.append(separador+parcela[9]);
				linha.append(separador+parcela[10]);
				linha.append(separador+dateFormat.format((Date)parcela[11]));
				linha.append(separador+3);
				linha.append(separador+new BigDecimal((Double)parcela[12]).setScale(2,BigDecimal.ROUND_HALF_EVEN));
				linha.append(separador+valorJuros);
				linha.append(separador+valorMovimento.setScale(2,BigDecimal.ROUND_HALF_EVEN));
				linha.append(separador+dateFormat.format((Date)parcela[13]));
				linha.append(separador+new BigDecimal((Double)parcela[12]).setScale(2,BigDecimal.ROUND_HALF_EVEN));
				linha.append(separador+"Pagamento Por Acordo");
				linha.append(separador+"Acordo");
				linha.append(separador+valorDesconto);
				linha.append(separador+dateFormatComHora.format((Date)parcela[11]));

				writer.println(linha);
			}
			writer.println("FIM");
			writer.close();
			System.out.println("Terminou Arquivo Movimento Acordo");
			if (transacaoIndependente)
				transaction.commit();

		} catch (Exception ex) {
			if (transacaoIndependente) {
				transaction.rollback();
				ex.printStackTrace();
				throw ex;
			}
			ex.printStackTrace();
			throw ex;
		} finally {
			if (transacaoIndependente)
				manager.close();
		}
	}
	private static void imprimirPorcetagem(Integer posicaoAtual,Integer total,String tabela){

		if(posicaoAtual%5000 == 0){
			Double div = posicaoAtual.doubleValue() / total.doubleValue();
			BigDecimal divisao = new BigDecimal(div*100).setScale(2,BigDecimal.ROUND_HALF_EVEN);
			System.out.println(divisao+"% ja concluidos da tabela "+tabela);

		}


	}
}
