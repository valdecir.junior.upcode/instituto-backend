package br.com.desenv.nepalign.wsdl;

import javax.xml.bind.annotation.XmlRootElement;

import br.com.desenv.nepalign.model.Usuario;

@XmlRootElement(name="SessionInfo")
public class SessionInfo 
{
	private Usuario usuario;
	private String amfChannelUrl;
	
	private long timezoneOffset;

	private Boolean uae;
	private Boolean isLoja;
	private String grupo;
	
	public Usuario getUsuario()
	{
		return this.usuario;
	}
	
	public void setUsuario(Usuario value)
	{
		this.usuario = value;
	}
	
	public String getAmfChannelUrl()
	{
		return this.amfChannelUrl;
	}
	
	public void setAmfChannelUrl(String value)
	{
		this.amfChannelUrl = value;
	}
	
	public long getTimezoneOffset() 
	{
		return timezoneOffset;
	}

	public void setTimezoneOffset(long timezoneOffset) 
	{
		this.timezoneOffset = timezoneOffset;
	}

	public Boolean getUae() 
	{
		return uae;
	}

	public void setUae(Boolean uae)
	{
		this.uae = uae;
	}

	public Boolean getIsLoja()
	{
		return isLoja;
	}

	public void setIsLoja(Boolean isLoja)
	{
		this.isLoja = isLoja;
	}

	public String getGrupo() 
	{
		return grupo;
	}

	public void setGrupo(String grupo)
	{
		this.grupo = grupo;
	}
}