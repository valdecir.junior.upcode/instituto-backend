package br.com.desenv.nepalign.relatorios;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao.Grupo;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.ItemRelatorioCateiraCobranca;
import br.com.desenv.nepalign.model.TabelaCarteiraCobranca;
import br.com.desenv.nepalign.model.Vendedor;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.TabelaCarteiraCobrancaService;
import br.com.desenv.nepalign.service.VendedorService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class RelatorioCarteiraCobranca {
	public Connection conn;	
	private HttpServletRequest request;	
	private HttpServletResponse response;

	public RelatorioCarteiraCobranca(HttpServletRequest request, HttpServletResponse response) throws ServletException, Exception
	{
		this.request=request;
		this.response=response;

	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente}, nomeAcao="Gerar Relatório Carteira Cobrança")
	public void gerarRelatorio() throws Exception
	{
		byte[] bytes = null;  
		try {  

			String caminho = "";
			EmpresaFisica empresa = null;
			if(request.getParameter("idEmpresaFisica") != null && !request.getParameter("idEmpresaFisica").equals(""))
			{
				empresa  = new EmpresaFisicaService().recuperarPorId(Integer.parseInt(request.getParameter("idEmpresaFisica"))); 
			}

			String[] ids = request.getParameter("idTabelas").split(",");
			List<TabelaCarteiraCobranca> listaTabelas = new ArrayList<>();
			for (int i = 0; i < ids.length; i++) 
			{
				listaTabelas.add(new TabelaCarteiraCobrancaService().recuperarPorId(Integer.parseInt(ids[i])));	
			}
			List<ItemRelatorioCateiraCobranca> listaItens = new ArrayList<>();
			TabelaCarteiraCobranca tp = listaTabelas.get(0);
			Calendar calprimerio = Calendar.getInstance();
			calprimerio.setTime(tp.getData());
			calprimerio.add(Calendar.MONTH, +1);
			calprimerio.set(Calendar.DAY_OF_MONTH, 1);
			DateFormat formatadorDataSql = new SimpleDateFormat("yyyy-MM-dd");
			String dataPrimeiro = formatadorDataSql.format(calprimerio.getTime());
			for (int i = 0; i < listaTabelas.size()-1; i++) 
			{
				TabelaCarteiraCobranca t = listaTabelas.get(i);
				Calendar cal = Calendar.getInstance();
				cal.setTime(t.getData());
				String mes = ""+(cal.get(Calendar.MONTH)+1);
				String ano = ""+cal.get(Calendar.YEAR);
				ItemRelatorioCateiraCobranca item = new ItemRelatorioCateiraCobranca();
				item.setMesAno((mes.length()==1?"0"+mes:mes)  +"/"+ano);
				String str = "";
				str = str + " select Round(sum(par.valorParcela),2) as total "; 
				str = str + " from duplicataParcela par  "; 
				str = str + " where month(par.dataVencimento) = "+mes; 
				str = str + " and year(par.dataVencimento)= "+ano; 
				str = str + " and par.idempresaFisica = "+t.getEmpresaFisica().getId(); 
				str = str + " and par.numeroParcela <>0  "; 
				Connection conn; 	        
				conn = ConexaoUtil.getConexaoPadrao();    
				java.sql.Statement rec = conn.createStatement();    
				ResultSet rs = rec.executeQuery(str);    
				if(rs.next())
				{
					item.setCarteiraInicial(rs.getDouble("total"));
				}
				rs.close();
				rec.close();
				conn.close();

				String strAC = "";
				strAC = strAC + " select sum(par.valorParcela) as valor  "; 
				strAC = strAC + " from duplicataParcela par  "; 
				strAC = strAC + " where month(par.dataVencimento) =   "+mes; 
				strAC = strAC + " and year(par.dataVencimento)= "+ano; 
				strAC = strAC + " and par.idempresaFisica =  "+t.getEmpresaFisica().getId(); 
				strAC = strAC + " and par.tipoPagamento = '5' "; 

				conn = ConexaoUtil.getConexaoPadrao();
				rec = conn.createStatement();
				rs = rec.executeQuery(strAC);
				if(rs.next())
				{
					item.setAcordo(rs.getDouble("valor"));
				}
				item.setCarteiraInicial(item.getCarteiraInicial()-(item.getAcordo()==null?0.0:item.getAcordo()));

				rs.close();
				rec.close();
				conn.close();

				String sqlRE = "";
				sqlRE = sqlRE + " select Round(sum(p.valorPago - ifnull(p.jurosPago,0) + ifnull(p.valorDesconto,0)),2) as valorRecebido "; 
				sqlRE = sqlRE + " from duplicataparcela p "; 
				sqlRE = sqlRE + " where month(p.datavencimento) =  "+mes; 
				sqlRE = sqlRE + " and year(p.datavencimento) =  "+ano; 
				sqlRE = sqlRE + " and p.idEmpresaFisica = "+t.getEmpresaFisica().getId(); 
				sqlRE = sqlRE + " and p.numeroParcela <> 0  "; 
				sqlRE = sqlRE + " and p.tipoPagamento in(1,2) "; 

				System.out.println(sqlRE);
				conn = ConexaoUtil.getConexaoPadrao();
				rec = conn.createStatement();
				rs = rec.executeQuery(sqlRE);
				if(rs.next())
				{
					item.setRecebimentoLiquido(rs.getDouble("valorRecebido"));
				}

				rs.close();
				rec.close();
				conn.close();

				String strMes = "";
				strMes = strMes + " select Round(sum(p.valorPago - ifnull(p.jurosPago,0) + ifnull(p.valorDesconto,0)),2) as valorRecebido "; 
				strMes = strMes + " from duplicataparcela p "; 
				strMes = strMes + " where month(p.databaixa) =  "+mes; 
				strMes = strMes + " and year(p.databaixa) =  "+ano; 
				strMes = strMes + " and month(p.dataVencimento) =  "+mes; 
				strMes = strMes + " and year(p.dataVencimento) =  "+ano; 
				strMes = strMes + " and p.idEmpresaFisica =  "+t.getEmpresaFisica().getId(); 
				strMes = strMes + " and p.numeroParcela <>0  "; 
				strMes = strMes + " and p.tipoPagamento in(1,2) "; 

				conn = ConexaoUtil.getConexaoPadrao();
				rec = conn.createStatement();
				rs = rec.executeQuery(strMes);

				if(rs.next())
				{
					item.setRecebimentoMesVirgente(rs.getDouble("valorRecebido"));  
				}
				rs.close();
				rec.close();
				conn.close();

				item.setObj(t.getPercentual());
				Double efc = item.getRecebimentoLiquido() / item.getCarteiraInicial();
				efc = efc *100;	    	  
				item.setEficacia(efc);
				Double falta = item.getCarteiraInicial() * (item.getObj()/100);
				falta = falta - item.getRecebimentoLiquido();
				item.setFaltaReceber(falta);
				item.setAbertoFinal(item.getCarteiraInicial()-item.getRecebimentoLiquido());
				item.setAbertoInicial(item.getRecebimentoMesVirgente()+item.getAbertoFinal());

				rs.close();
				rec.close();
				conn.close();



				listaItens.add(item);
			}
			Map<String, Object> retorno = this.gerarUltimoItem(listaTabelas,dataPrimeiro);
			ItemRelatorioCateiraCobranca item = (ItemRelatorioCateiraCobranca) retorno.get("item");
			String dataInicial = (String) retorno.get("dataInicial");
			String dataFinal = (String) retorno.get("dataFinal");
			listaItens.add(item);
			caminho = "relatorioCarteiraCobranca.jasper"; 

			JRBeanCollectionDataSource jrRS = new JRBeanCollectionDataSource(listaItens) ;

			URL urlArquivo = getClass().getResource("/relatorios/"+caminho);
			String rel = urlArquivo.getPath();

			Image logo = null;
			InputStream inputStreamDaImagem = null;
			URL urlImagens = getClass().getResource("/imagens/logo.jpg");
			String caminhoImagem = urlImagens.getPath();

			try {    
				File file = new File(caminhoImagem);  

				if(file.exists()){//testa se imagem existe  
					inputStreamDaImagem = new BufferedInputStream(
							new FileInputStream(caminhoImagem));
					logo = ImageIO.read(inputStreamDaImagem);
				}else{
					logo =null;
				}


			} catch (FileNotFoundException e) {       

				logo =null;
			}       

			URL urlSub= getClass().getResource("/relatorios/");

			Map<String, Object> params = new HashMap<String, Object>();  

			params.put("logo", logo);             
			params.put("HABILITAR_DATA", "N");
			params.put("SUBREPORT_DIR", urlSub.getPath());
			params.put("empresa", "F-"+empresa.getId()+"-"+empresa.getRazaoSocial());
			params.put("dataInicial", dataInicial);
			params.put("dataFinal", dataFinal);
			JasperPrint impressao = JasperFillManager.fillReport(rel, params,jrRS); 
			bytes = JasperExportManager.exportReportToPdf(impressao);  


		}catch (JRException | SQLException e) {  
			throw new ServletException(e);  
		}  

		if (bytes != null && bytes.length > 0) {  
			// envia o relatário em formato PDF para o browser  
			response.setContentType("application/pdf");  
			response.setContentLength(bytes.length);  
			ServletOutputStream ouputStream = response.getOutputStream();  
			ouputStream.write(bytes, 0, bytes.length);  
			ouputStream.flush();  
			ouputStream.close();  
		}   
	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente}, nomeAcao="Gerar Relatório Carteira Cobrança Data Fixa")
	public void gerarRelatorioDataFixa() throws Exception
	{
		byte[] bytes = null;  
		int quantidadeEficacia = 0;
		Double totalEficacia = 0.0;
		Double mediaAritmeticaEficacia = 0.0;
		
		try {  
			String dataSqlEmissaoInicial = null;
			String dataSqlEmissaoFinal = null;
						
			if(!request.getParameter("dataEmissaoInicial").equals(""))
				dataSqlEmissaoInicial = IgnUtil.dateFormatSql.format(IgnUtil.dateFormat.parse(request.getParameter("dataEmissaoInicial")));
			if(!request.getParameter("dataEmissaoFinal").equals(""))
				dataSqlEmissaoFinal = IgnUtil.dateFormatSql.format(IgnUtil.dateFormat.parse(request.getParameter("dataEmissaoFinal")));

			String caminho = "";
		
			EmpresaFisica empresa = null;
			if(request.getParameter("idEmpresaFisica") != null && !request.getParameter("idEmpresaFisica").equals(""))
			{
				empresa  = new EmpresaFisicaService().recuperarPorId(Integer.parseInt(request.getParameter("idEmpresaFisica"))); 
			}
			String idPlanoValeCalcado = DsvConstante.getParametrosSistema().get("idPlanoPagamentoValeCalcadoCrediario");
			String[] ids = request.getParameter("idTabelas").split(",");
			List<TabelaCarteiraCobranca> listaTabelas = new ArrayList<>();
			for (int i = 0; i < ids.length; i++) 
			{
				listaTabelas.add(new TabelaCarteiraCobrancaService().recuperarPorId(Integer.parseInt(ids[i])));	
			}
			List<ItemRelatorioCateiraCobranca> listaItens = new ArrayList<>();
			TabelaCarteiraCobranca tp = listaTabelas.get(0);
			Calendar calprimerio = Calendar.getInstance();
			calprimerio.setTime(tp.getData());
			calprimerio.add(Calendar.MONTH, +1);
			calprimerio.set(Calendar.DAY_OF_MONTH, 1);
			DateFormat formatadorDataSql = new SimpleDateFormat("yyyy-MM-dd");
			String dataPrimeiro = formatadorDataSql.format(calprimerio.getTime());
			
			for (int i = 0; i < listaTabelas.size()-1; i++) 
			{
				TabelaCarteiraCobranca t = listaTabelas.get(i);
				Calendar cal = Calendar.getInstance();
				cal.setTime(t.getData());
				String mes = ""+(cal.get(Calendar.MONTH)+1);
				String ano = ""+cal.get(Calendar.YEAR);
				ItemRelatorioCateiraCobranca item = new ItemRelatorioCateiraCobranca();
				item.setMesAno((mes.length()==1?"0"+mes:mes)  +"/"+ano);
				String str = "";
				str = str + " select Round(sum(par.valorParcela),2) as total "; 
				str = str + " from duplicataParcela par  "; 
				str = str + " inner join  duplicata d on par.idduplicata = d.idduplicata  "; 
				str = str + " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
				str = str + " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
				str = str + " group by ac.idDuplicataNova) "; 
				str = str + " as acordo on acordo.idDuplicataNova=d.idduplicata ";
				str = str + " left outer join  pedidovenda pv on pv.idpedidovenda = d.idpedidovenda  "; 
				str = str + " where month(par.dataVencimento) = "+mes; 
				str = str + " and year(par.dataVencimento)= "+ano; 
				str = str + " and par.idempresaFisica = "+t.getEmpresaFisica().getId(); 
				str = str + " and par.numeroParcela <>0  "; 
				str = str + " and if(pv.idpedidovenda is null,true,pv.idplanopagamento not in("+idPlanoValeCalcado+")) "; 
				
				if(dataSqlEmissaoInicial != null)
					str += " and if((par.acordo<>'S' or par.acordo IS NULL),d.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00',acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
				
				if(dataSqlEmissaoFinal != null)
					str += " and if((par.acordo<>'S' or par.acordo IS NULL),d.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59')";
				
				
				Connection conn; 	        
				conn = ConexaoUtil.getConexaoPadrao();    
				java.sql.Statement rec = conn.createStatement();    
				ResultSet rs = rec.executeQuery(str);    
				if(rs.next())
				{
					item.setCarteiraInicial(rs.getDouble("total"));
				}
				rs.close();
				rec.close();
				conn.close();

				String strAC = "";
				strAC = strAC + " select sum(par.valorParcela) as valor  "; 
				strAC = strAC + " from duplicataParcela par  "; 
				strAC = strAC + " inner join  duplicata d on par.idduplicata = d.idduplicata  ";
				strAC = strAC + " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
				strAC = strAC + " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
				strAC = strAC + " group by ac.idDuplicataNova) "; 
				strAC = strAC + " as acordo on acordo.idDuplicataNova=d.idduplicata ";
				strAC = strAC + " where month(par.dataVencimento) =   "+mes; 
				strAC = strAC + " and year(par.dataVencimento)= "+ano; 
				strAC = strAC + " and par.idempresaFisica =  "+t.getEmpresaFisica().getId(); 
				strAC = strAC + " and par.tipoPagamento = '5' "; 
				if(dataSqlEmissaoInicial != null)
					strAC += " and if((par.acordo<>'S' or par.acordo IS NULL),d.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00',acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
				if(dataSqlEmissaoFinal != null)
					strAC += " and if((par.acordo<>'S' or par.acordo IS NULL),d.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59')";
			
				

				conn = ConexaoUtil.getConexaoPadrao();
				rec = conn.createStatement();
				rs = rec.executeQuery(strAC);
				if(rs.next())
				{
					item.setAcordo(rs.getDouble("valor"));
				}
				item.setCarteiraInicial(item.getCarteiraInicial()-(item.getAcordo()==null?0.0:item.getAcordo()));

				rs.close();
				rec.close();
				conn.close();

				String sqlRE = "";
				sqlRE = sqlRE + " select Round(sum(p.valorPago - ifnull(p.jurosPago,0) + ifnull(p.valorDesconto,0)),2) as valorRecebido "; 
				sqlRE = sqlRE + " from duplicataparcela p "; 
				sqlRE = sqlRE + " inner join  duplicata d on p.idduplicata = d.idduplicata  ";
				sqlRE = sqlRE + " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
				sqlRE = sqlRE + " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
				sqlRE = sqlRE + " group by ac.idDuplicataNova) "; 
				sqlRE = sqlRE + " as acordo on acordo.idDuplicataNova=d.idduplicata ";
				sqlRE = sqlRE + " left outer join  pedidovenda pv on pv.idpedidovenda = d.idpedidovenda  "; 
				sqlRE = sqlRE + " where month(p.datavencimento) =  "+mes; 
				sqlRE = sqlRE + " and year(p.datavencimento) =  "+ano; 
				sqlRE = sqlRE + " and p.idEmpresaFisica = "+t.getEmpresaFisica().getId(); 
				sqlRE = sqlRE + " and p.numeroParcela <> 0  "; 
				sqlRE = sqlRE + " and p.tipoPagamento in(1,2) "; 
				sqlRE = sqlRE + " and if(pv.idpedidovenda is null,true,pv.idplanopagamento not in("+idPlanoValeCalcado+")) "; 
				if(dataSqlEmissaoInicial != null)
					sqlRE += " and if((p.acordo<>'S' or p.acordo IS NULL),d.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00',acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
				
				if(dataSqlEmissaoFinal != null)
					sqlRE += " and if((p.acordo<>'S' or p.acordo IS NULL),d.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59')";
			
				conn = ConexaoUtil.getConexaoPadrao();
				rec = conn.createStatement();
				rs = rec.executeQuery(sqlRE);
				if(rs.next())
				{
					item.setRecebimentoLiquido(rs.getDouble("valorRecebido"));
				}

				rs.close();
				rec.close();
				conn.close();

				String strMes = "";
				strMes = strMes + " select Round(sum(p.valorPago - ifnull(p.jurosPago,0) + ifnull(p.valorDesconto,0)),2) as valorRecebido "; 
				strMes = strMes + " from duplicataparcela p "; 
				strMes = strMes + " inner join duplicata d on d.idDuplicata = p.idDuplicata "; 
				strMes = strMes + " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
				strMes = strMes + " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
				strMes = strMes + " group by ac.idDuplicataNova) "; 
				strMes = strMes + " as acordo on acordo.idDuplicataNova=d.idduplicata ";
				strMes = strMes + " left outer join  pedidovenda pv on pv.idpedidovenda = d.idpedidovenda "; 
				strMes = strMes + " where month(p.databaixa) =  "+mes; 
				strMes = strMes + " and year(p.databaixa) =  "+ano; 
				strMes = strMes + " and month(p.dataVencimento) =  "+mes; 
				strMes = strMes + " and year(p.dataVencimento) =  "+ano; 
				strMes = strMes + " and p.idEmpresaFisica =  "+t.getEmpresaFisica().getId(); 
				strMes = strMes + " and p.numeroParcela <>0  "; 
				strMes = strMes + " and p.tipoPagamento in(1,2) "; 
				strMes = strMes + " and if(pv.idpedidovenda is null,true,pv.idplanopagamento not in("+idPlanoValeCalcado+")) "; 
				if(dataSqlEmissaoInicial != null)
					strMes += " and if((p.acordo<>'S' or p.acordo IS NULL),d.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00',acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
				
				if(dataSqlEmissaoFinal != null)
					strMes += " and if((p.acordo<>'S' or p.acordo IS NULL),d.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59')";
			
				conn = ConexaoUtil.getConexaoPadrao();
				rec = conn.createStatement();
				rs = rec.executeQuery(strMes);

				if(rs.next())
					item.setRecebimentoMesVirgente(rs.getDouble("valorRecebido"));  
	
				rs.close();
				rec.close();
				conn.close();

				item.setObj(t.getPercentual());
				Double efc = item.getRecebimentoLiquido() / item.getCarteiraInicial();
				
				
				efc = efc *100;	    	 
				item.setEficacia(efc);
				Double falta = item.getCarteiraInicial() * (item.getObj()/100);
				falta = falta - item.getRecebimentoLiquido();
				item.setFaltaReceber(falta);
				item.setAbertoFinal(item.getCarteiraInicial()-item.getRecebimentoLiquido());
				item.setAbertoInicial(item.getRecebimentoMesVirgente()+item.getAbertoFinal());

				if(!efc.isNaN())
				{
					quantidadeEficacia +=1;
					totalEficacia += efc.isNaN()?0.0:efc;
				}
				
				rs.close();
				rec.close();
				conn.close();
				
				listaItens.add(item);
			}
			
			Map<String, Object> retorno = this.gerarUltimoItemDataFixa (listaTabelas,dataPrimeiro,dataSqlEmissaoInicial, dataSqlEmissaoFinal);
			//Map<String, Object> retorno = this.gerarUltimoItemDataFixa(listaTabelas,dataPrimeiro,dataBase);
			ItemRelatorioCateiraCobranca item = (ItemRelatorioCateiraCobranca) retorno.get("item");
			
			if((item.getEficacia() != null) && (!item.getEficacia().isNaN()))
			{
				totalEficacia += item.getEficacia().isNaN()?0.0:item.getEficacia();
				quantidadeEficacia += 1;
			}
				
			if(quantidadeEficacia > 0)
				mediaAritmeticaEficacia = (totalEficacia / quantidadeEficacia);
			
			item.setMediaAritmeticaEficacia(mediaAritmeticaEficacia);
			String dataInicial = (String) retorno.get("dataInicial");
			String dataFinal = (String) retorno.get("dataFinal");
			listaItens.add(item);
			
			System.out.println("Total Eficacia: "+totalEficacia+"    Qtd: "+quantidadeEficacia);
			
			caminho = "relatorioCarteiraCobranca.jasper"; 

			JRBeanCollectionDataSource jrRS = new JRBeanCollectionDataSource(listaItens) ;

			URL urlArquivo = getClass().getResource("/relatorios/"+caminho);
			String rel = urlArquivo.getPath();

			Image logo = null;
			InputStream inputStreamDaImagem = null;
			URL urlImagens = getClass().getResource("/imagens/logo.jpg");
			String caminhoImagem = urlImagens.getPath();

			try {    
				File file = new File(caminhoImagem);  

				if(file.exists()){//testa se imagem existe  
					inputStreamDaImagem = new BufferedInputStream(
							new FileInputStream(caminhoImagem));
					logo = ImageIO.read(inputStreamDaImagem);
				}else{
					logo =null;
				}


			} catch (FileNotFoundException e) {       

				logo =null;
			}       

			URL urlSub= getClass().getResource("/relatorios/");

			Map<String, Object> params = new HashMap<String, Object>();  

			params.put("logo", logo);             
			params.put("HABILITAR_DATA", "N");
			params.put("SUBREPORT_DIR", urlSub.getPath());
			params.put("empresa", "F-"+empresa.getId()+"-"+empresa.getRazaoSocial());
			params.put("dataInicial", dataInicial);
			params.put("dataFinal", dataFinal);
			JasperPrint impressao = JasperFillManager.fillReport(rel, params,jrRS); 
			bytes = JasperExportManager.exportReportToPdf(impressao);  


		}catch (JRException | SQLException e) {  
			throw new ServletException(e);  
		}  

		if (bytes != null && bytes.length > 0) {  
			// envia o relatário em formato PDF para o browser  
			response.setContentType("application/pdf");  
			response.setContentLength(bytes.length);  
			ServletOutputStream ouputStream = response.getOutputStream();  
			ouputStream.write(bytes, 0, bytes.length);  
			ouputStream.flush();  
			ouputStream.close();  
		}   
	}
	public Map<String, Object>  gerarUltimoItem(List<TabelaCarteiraCobranca> listaTabelas,String dataPrimeiro) throws SQLException
	{

		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
		SimpleDateFormat dfa = new SimpleDateFormat ("dd/MM/yyyy");
		String dataInicial = "";
		String dataFinal = "";
		String dataSInicial = "";
		String dataSFinal = "";

		ItemRelatorioCateiraCobranca item = new ItemRelatorioCateiraCobranca();
		item.setMesAno("*ACIMA*");
		TabelaCarteiraCobranca t = listaTabelas.get(listaTabelas.size()-1);

		Calendar cal = Calendar.getInstance();
		cal.setTime(t.getData()); 
		cal.set(Calendar.DAY_OF_MONTH, 1);
		dataSInicial = df.format(cal.getTime());
		dataInicial = dfa.format(cal.getTime());

		t = listaTabelas.get(listaTabelas.size()-2);
		cal = Calendar.getInstance();
		cal.setTime(t.getData()); 
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		dataFinal =  dfa.format(cal.getTime());
		dataSFinal = df.format(cal.getTime());


		String str = "";
		str = str + "  select Round(sum(par.valorParcela),2) as total   "; 
		str = str + "  from duplicataParcela par   "; 
		str = str + "  where par.dataVencimento <= '"+dataSFinal+"' "; 
		str = str + "  and par.dataVencimento >= '"+dataSInicial+"'"; 
		str = str + "  and par.idempresaFisica =  "+t.getEmpresaFisica().getId(); 
		str = str + "  and par.numeroParcela <>0  "; 
		Connection conn; 	        
		conn = ConexaoUtil.getConexaoPadrao();    
		java.sql.Statement rec = conn.createStatement();    
		ResultSet rs = rec.executeQuery(str);    
		if(rs.next())
		{
			item.setCarteiraInicial(rs.getDouble("total"));
		}
		rs.close();
		rec.close();
		conn.close();


		String strAC = "";
		strAC = strAC + " select sum(par.valorParcela) as valor  "; 
		strAC = strAC + " from duplicataParcela par  "; 
		strAC = strAC + " where par.dataVencimento <='"+dataSFinal+"' " ;
		strAC = strAC + " and par.dataVencimento >= '"+dataSInicial+"'";
		strAC = strAC + " and par.idempresaFisica =  "+t.getEmpresaFisica().getId(); 
		strAC = strAC + " and par.tipoPagamento = '5' "; 

		conn = ConexaoUtil.getConexaoPadrao();
		rec = conn.createStatement();
		rs = rec.executeQuery(strAC);
		if(rs.next())
		{
			item.setAcordo(rs.getDouble("valor"));
		}
		item.setCarteiraInicial(item.getCarteiraInicial()-(item.getAcordo()==null?0.0:item.getAcordo()));

		rs.close();
		rec.close();
		conn.close();

		String strRE = "";
		strRE = strRE + " select Round(sum(p.valorPago - ifnull(p.jurosPago,0) + ifnull(p.valorDesconto,0)),2) as valorRecebido "; 
		strRE = strRE + " from duplicataparcela p "; 
		strRE = strRE + " where p.datavencimento <= '"+dataSFinal+"' "; 
		strRE = strRE + " and p.datavencimento >=  '"+dataSInicial+"' "; 
		strRE = strRE + " and p.idEmpresaFisica = "+t.getEmpresaFisica().getId(); 
		strRE = strRE + " and p.numeroParcela <>0  "; 
		strRE = strRE + " and p.tipoPagamento in(1,2) "; 

		conn = ConexaoUtil.getConexaoPadrao();    
		rec = conn.createStatement();    
		rs = rec.executeQuery(strRE);  
		if(rs.next())
		{
			item.setRecebimentoLiquido(rs.getDouble("valorRecebido"));
		}

		rs.close();
		rec.close();
		conn.close();

		String strMes = "";
		strMes = strMes + " select Round(sum(p.valorPago - ifnull(p.jurosPago,0) + ifnull(p.valorDesconto,0)),2) as valorRecebido "; 
		strMes = strMes + " from duplicataparcela p "; 
		strMes = strMes + " where p.databaixa <= '"+dataSFinal+"' "; 
		strMes = strMes + " and p.databaixa >=  '"+dataSInicial+"' "; 
		strMes = strMes + " and p.dataVencimento <= '"+dataSFinal+"' "; 
		strMes = strMes + " and p.dataVencimento >=  '"+dataSInicial+"' ";        
		strMes = strMes + " and p.idEmpresaFisica = "+t.getEmpresaFisica().getId(); 
		strMes = strMes + " and p.numeroParcela <>0  "; 
		strMes = strMes + " and p.tipoPagamento in(1,2) "; 
		conn = ConexaoUtil.getConexaoPadrao();    
		rec = conn.createStatement();    
		rs = rec.executeQuery(strMes); 

		if(rs.next())
		{
			item.setRecebimentoMesVirgente(rs.getDouble("valorRecebido"));
		}
		rs.close();
		rec.close();
		conn.close();



		item.setObj(listaTabelas.get(listaTabelas.size()-1).getPercentual());
		Double efc = item.getRecebimentoLiquido() / item.getCarteiraInicial();
		efc = efc *100;	    	  
		item.setEficacia(efc);
		Double falta = item.getCarteiraInicial() * (item.getObj()/100);
		falta = falta - item.getRecebimentoLiquido();
		item.setFaltaReceber(falta);
		item.setAbertoFinal(item.getCarteiraInicial()-item.getRecebimentoLiquido());
		item.setAbertoInicial(item.getRecebimentoMesVirgente()+item.getAbertoFinal());

		Map<String, Object> retorno = new HashMap<String, Object>();
		retorno.put("item", item);
		retorno.put("dataInicial",dataInicial);
		retorno.put("dataFinal", dataFinal);
		return retorno;

	}
	public Map<String, Object>  gerarUltimoItemDataFixa(List<TabelaCarteiraCobranca> listaTabelas,String dataPrimeiro,String dataBase) throws Exception
	{

		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
		SimpleDateFormat dfa = new SimpleDateFormat ("dd/MM/yyyy");
		String dataInicial = "";
		String dataFinal = "";
		String dataSInicial = "";
		String dataSFinal = "";
		String idPlanoValeCalcado = DsvConstante.getParametrosSistema().get("idPlanoPagamentoValeCalcadoCrediario");
		ItemRelatorioCateiraCobranca item = new ItemRelatorioCateiraCobranca();
		item.setMesAno("*ACIMA*");
		TabelaCarteiraCobranca t = listaTabelas.get(listaTabelas.size()-1);

		Calendar cal = Calendar.getInstance();
		cal.setTime(t.getData()); 
		cal.set(Calendar.DAY_OF_MONTH, 1);
		dataSInicial = df.format(cal.getTime());
		dataInicial = dfa.format(cal.getTime());

		t = listaTabelas.get(listaTabelas.size()-2);
		cal = Calendar.getInstance();
		cal.setTime(t.getData()); 
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		dataFinal =  dfa.format(cal.getTime());
		dataSFinal = df.format(cal.getTime());


		String str = "";
		str = str + "  select Round(sum(par.valorParcela),2) as total   "; 
		str = str + "  from duplicataParcela par   "; 
		str = str + " inner join  duplicata d on par.idduplicata = d.idduplicata  ";
		str = str + " left outer join  pedidovenda pv on pv.idpedidovenda = d.idpedidovenda  "; 
		str = str + "  where par.dataVencimento <= '"+dataSFinal+"' "; 
		str = str + "  and par.dataVencimento >= '"+dataSInicial+"'"; 
		str = str + "  and par.idempresaFisica =  "+t.getEmpresaFisica().getId(); 
		str = str + "  and par.numeroParcela <>0  "; 
		str = str + " and if(pv.idpedidovenda is null,true,pv.idplanopagamento not in("+idPlanoValeCalcado+")) "; 
		if(dataBase!=null)
			str = str + " and d.dataCompra >= '"+dataBase+"'"; 
		Connection conn; 	        
		conn = ConexaoUtil.getConexaoPadrao();    
		java.sql.Statement rec = conn.createStatement();    
		ResultSet rs = rec.executeQuery(str);    
		if(rs.next())
		{
			item.setCarteiraInicial(rs.getDouble("total"));
		}
		rs.close();
		rec.close();
		conn.close();


		String strAC = "";
		strAC = strAC + " select sum(par.valorParcela) as valor  "; 
		strAC = strAC + " from duplicataParcela par  "; 
		strAC = strAC + " inner join  duplicata d on par.idduplicata = d.idduplicata  "; 
		strAC = strAC + " where par.dataVencimento <='"+dataSFinal+"' " ;
		strAC = strAC + " and par.dataVencimento >= '"+dataSInicial+"'";
		strAC = strAC + " and par.idempresaFisica =  "+t.getEmpresaFisica().getId(); 
		strAC = strAC + " and par.tipoPagamento = '5' "; 
		if(dataBase!=null)
			strAC = strAC + " and d.dataCompra >= '"+dataBase+"'"; 

		conn = ConexaoUtil.getConexaoPadrao();
		rec = conn.createStatement();
		rs = rec.executeQuery(strAC);
		if(rs.next())
		{
			item.setAcordo(rs.getDouble("valor"));
		}
		item.setCarteiraInicial(item.getCarteiraInicial()-(item.getAcordo()==null?0.0:item.getAcordo()));

		rs.close();
		rec.close();
		conn.close();

		String strRE = "";
		strRE = strRE + " select Round(sum(p.valorPago - ifnull(p.jurosPago,0) + ifnull(p.valorDesconto,0)),2) as valorRecebido "; 
		strRE = strRE + " from duplicataparcela p "; 
		strRE = strRE + " inner join duplicata d on d.idDuplicata = p.idDuplicata "; 
		strRE = strRE + " left outer join  pedidovenda pv on pv.idpedidovenda = d.idpedidovenda  "; 
		strRE = strRE + " where p.datavencimento <= '"+dataSFinal+"' "; 
		strRE = strRE + " and p.datavencimento >=  '"+dataSInicial+"' "; 
		strRE = strRE + " and p.idEmpresaFisica = "+t.getEmpresaFisica().getId(); 
		strRE = strRE + " and p.numeroParcela <>0  "; 
		strRE = strRE + " and p.tipoPagamento in(1,2) "; 
		strRE = strRE + " and if(pv.idpedidovenda is null,true,pv.idplanopagamento not in("+idPlanoValeCalcado+")) "; 
		if(dataBase!=null)
			strRE = strRE + " and d.dataCompra >= '"+dataBase+"'"; 

		conn = ConexaoUtil.getConexaoPadrao();    
		rec = conn.createStatement();    
		rs = rec.executeQuery(strRE);  
		if(rs.next())
		{
			item.setRecebimentoLiquido(rs.getDouble("valorRecebido"));
		}

		rs.close();
		rec.close();
		conn.close();

		String strMes = "";
		strMes = strMes + " select Round(sum(p.valorPago - ifnull(p.jurosPago,0) + ifnull(p.valorDesconto,0)),2) as valorRecebido "; 
		strMes = strMes + " from duplicataparcela p "; 
		strMes = strMes + " inner join duplicata d on d.idDuplicata = p.idDuplicata "; 
		strMes = strMes + " left outer join  pedidovenda pv on pv.idpedidovenda = d.idpedidovenda  "; 
		strMes = strMes + " where p.databaixa <= '"+dataSFinal+"' "; 
		strMes = strMes + " and p.databaixa >=  '"+dataSInicial+"' "; 
		strMes = strMes + " and p.dataVencimento <= '"+dataSFinal+"' "; 
		strMes = strMes + " and p.dataVencimento >=  '"+dataSInicial+"' ";        
		strMes = strMes + " and p.idEmpresaFisica = "+t.getEmpresaFisica().getId(); 
		strMes = strMes + " and p.numeroParcela <>0  "; 
		strMes = strMes + " and p.tipoPagamento in(1,2) "; 
		strMes = strMes + " and if(pv.idpedidovenda is null,true,pv.idplanopagamento not in("+idPlanoValeCalcado+")) "; 
		if(dataBase!=null)
			strMes = strMes + " and d.dataCompra >= '"+dataBase+"'"; 

		conn = ConexaoUtil.getConexaoPadrao();    
		rec = conn.createStatement();    
		rs = rec.executeQuery(strMes); 

		if(rs.next())
		{
			item.setRecebimentoMesVirgente(rs.getDouble("valorRecebido"));
		}
		rs.close();
		rec.close();
		conn.close();



		item.setObj(listaTabelas.get(listaTabelas.size()-1).getPercentual());
		Double efc = item.getRecebimentoLiquido() / item.getCarteiraInicial();
		efc = efc *100;	    	  
		item.setEficacia(efc);
		Double falta = item.getCarteiraInicial() * (item.getObj()/100);
		falta = falta - item.getRecebimentoLiquido();
		item.setFaltaReceber(falta);
		item.setAbertoFinal(item.getCarteiraInicial()-item.getRecebimentoLiquido());
		item.setAbertoInicial(item.getRecebimentoMesVirgente()+item.getAbertoFinal());

		Map<String, Object> retorno = new HashMap<String, Object>();
		retorno.put("item", item);
		retorno.put("dataInicial",dataInicial);
		retorno.put("dataFinal", dataFinal);
		return retorno;

	}
	
	public Map<String, Object>  gerarUltimoItemDataFixa(List<TabelaCarteiraCobranca> listaTabelas,String dataPrimeiro,String dataSqlEmissaoInicial,String dataSqlEmissaoFinal) throws Exception
	{

		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
		SimpleDateFormat dfa = new SimpleDateFormat ("dd/MM/yyyy");
		String dataInicial = "";
		String dataFinal = "";
		String dataSInicial = "";
		String dataSFinal = "";
		String idPlanoValeCalcado = DsvConstante.getParametrosSistema().get("idPlanoPagamentoValeCalcadoCrediario");
		ItemRelatorioCateiraCobranca item = new ItemRelatorioCateiraCobranca();
		item.setMesAno("*ACIMA*");
		TabelaCarteiraCobranca t = listaTabelas.get(listaTabelas.size()-1);

		Calendar cal = Calendar.getInstance();
		cal.setTime(t.getData()); 
		cal.set(Calendar.DAY_OF_MONTH, 1);
		dataSInicial = df.format(cal.getTime());
		dataInicial = dfa.format(cal.getTime());

		t = listaTabelas.get(listaTabelas.size()-2);
		cal = Calendar.getInstance();
		cal.setTime(t.getData()); 
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		dataFinal =  dfa.format(cal.getTime());
		dataSFinal = df.format(cal.getTime());


		String str = "";
		str = str + "  select Round(sum(par.valorParcela),2) as total   "; 
		str = str + "  from duplicataParcela par   "; 
		str = str + " inner join  duplicata d on par.idduplicata = d.idduplicata  ";
		str = str + " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
		str = str + " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
		str = str + " group by ac.idDuplicataNova) "; 
		str = str + " as acordo on acordo.idDuplicataNova=d.idduplicata ";
		str = str + " left outer join  pedidovenda pv on pv.idpedidovenda = d.idpedidovenda  "; 
		str = str + "  where par.dataVencimento <= '"+dataSFinal+"' "; 
		str = str + "  and par.dataVencimento >= '"+dataSInicial+"'"; 
		str = str + "  and par.idempresaFisica =  "+t.getEmpresaFisica().getId(); 
		str = str + "  and par.numeroParcela <>0  "; 
		str = str + " and if(pv.idpedidovenda is null,true,pv.idplanopagamento not in("+idPlanoValeCalcado+")) "; 
		if(dataSqlEmissaoInicial != null)
			str += " and if(par.acordo<>'S',d.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00',acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
		
		if(dataSqlEmissaoFinal != null)
			str += " and if(par.acordo<>'S',d.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59')";
	
		Connection conn; 	        
		conn = ConexaoUtil.getConexaoPadrao();    
		java.sql.Statement rec = conn.createStatement();    
		ResultSet rs = rec.executeQuery(str);    
		if(rs.next())
		{
			item.setCarteiraInicial(rs.getDouble("total"));
		}
		rs.close();
		rec.close();
		conn.close();


		String strAC = "";
		strAC = strAC + " select sum(par.valorParcela) as valor  "; 
		strAC = strAC + " from duplicataParcela par  "; 
		strAC = strAC + " inner join  duplicata d on par.idduplicata = d.idduplicata  "; 
		strAC = strAC + " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
		strAC = strAC + " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
		strAC = strAC + " group by ac.idDuplicataNova) "; 
		strAC = strAC + " as acordo on acordo.idDuplicataNova=d.idduplicata ";
		strAC = strAC + " where par.dataVencimento <='"+dataSFinal+"' " ;
		strAC = strAC + " and par.dataVencimento >= '"+dataSInicial+"'";
		strAC = strAC + " and par.idempresaFisica =  "+t.getEmpresaFisica().getId(); 
		strAC = strAC + " and par.tipoPagamento = '5' "; 
		if(dataSqlEmissaoInicial != null)
			strAC += " and if(par.acordo<>'S',d.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00',acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
		
		if(dataSqlEmissaoFinal != null)
			strAC += " and if(par.acordo<>'S',d.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59')";
	

		conn = ConexaoUtil.getConexaoPadrao();
		rec = conn.createStatement();
		rs = rec.executeQuery(strAC);
		if(rs.next())
		{
			item.setAcordo(rs.getDouble("valor"));
		}
		item.setCarteiraInicial(item.getCarteiraInicial()-(item.getAcordo()==null?0.0:item.getAcordo()));

		rs.close();
		rec.close();
		conn.close();

		String strRE = "";
		strRE = strRE + " select Round(sum(p.valorPago - ifnull(p.jurosPago,0) + ifnull(p.valorDesconto,0)),2) as valorRecebido "; 
		strRE = strRE + " from duplicataparcela p "; 
		strRE = strRE + " inner join duplicata d on d.idDuplicata = p.idDuplicata "; 
		strRE = strRE + " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
		strRE = strRE + " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
		strRE = strRE + " group by ac.idDuplicataNova) "; 
		strRE = strRE + " as acordo on acordo.idDuplicataNova=d.idduplicata ";
		strRE = strRE + " left outer join  pedidovenda pv on pv.idpedidovenda = d.idpedidovenda  "; 
		strRE = strRE + " where p.datavencimento <= '"+dataSFinal+"' "; 
		strRE = strRE + " and p.datavencimento >=  '"+dataSInicial+"' "; 
		strRE = strRE + " and p.idEmpresaFisica = "+t.getEmpresaFisica().getId(); 
		strRE = strRE + " and p.numeroParcela <>0  "; 
		strRE = strRE + " and p.tipoPagamento in(1,2) "; 
		strRE = strRE + " and if(pv.idpedidovenda is null,true,pv.idplanopagamento not in("+idPlanoValeCalcado+")) "; 
		if(dataSqlEmissaoInicial != null)
			strRE += " and if(p.acordo<>'S',d.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00',acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
		
		if(dataSqlEmissaoFinal != null)
			strRE += " and if(p.acordo<>'S',d.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59')";
	
		conn = ConexaoUtil.getConexaoPadrao();    
		rec = conn.createStatement();    
		rs = rec.executeQuery(strRE);  
		if(rs.next())
		{
			item.setRecebimentoLiquido(rs.getDouble("valorRecebido"));
		}

		rs.close();
		rec.close();
		conn.close();

		String strMes = "";
		strMes = strMes + " select Round(sum(p.valorPago - ifnull(p.jurosPago,0) + ifnull(p.valorDesconto,0)),2) as valorRecebido "; 
		strMes = strMes + " from duplicataparcela p "; 
		strMes = strMes + " inner join duplicata d on d.idDuplicata = p.idDuplicata "; 
		strMes = strMes + " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
		strMes = strMes + " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
		strMes = strMes + " group by ac.idDuplicataNova) "; 
		strMes = strMes + " as acordo on acordo.idDuplicataNova=d.idduplicata ";
		strMes = strMes + " left outer join  pedidovenda pv on pv.idpedidovenda = d.idpedidovenda  "; 
		strMes = strMes + " where p.databaixa <= '"+dataSFinal+"' "; 
		strMes = strMes + " and p.databaixa >=  '"+dataSInicial+"' "; 
		strMes = strMes + " and p.dataVencimento <= '"+dataSFinal+"' "; 
		strMes = strMes + " and p.dataVencimento >=  '"+dataSInicial+"' ";        
		strMes = strMes + " and p.idEmpresaFisica = "+t.getEmpresaFisica().getId(); 
		strMes = strMes + " and p.numeroParcela <>0  "; 
		strMes = strMes + " and p.tipoPagamento in(1,2) "; 
		strMes = strMes + " and if(pv.idpedidovenda is null,true,pv.idplanopagamento not in("+idPlanoValeCalcado+")) "; 
		if(dataSqlEmissaoInicial != null)
			strMes += " and if(p.acordo<>'S',d.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00',acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
		
		if(dataSqlEmissaoFinal != null)
			strMes += " and if(p.acordo<>'S',d.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59')";
	
		conn = ConexaoUtil.getConexaoPadrao();    
		rec = conn.createStatement();    
		rs = rec.executeQuery(strMes); 

		if(rs.next())
		{
			item.setRecebimentoMesVirgente(rs.getDouble("valorRecebido"));
		}
		rs.close();
		rec.close();
		conn.close();



		item.setObj(listaTabelas.get(listaTabelas.size()-1).getPercentual());
		Double efc = item.getRecebimentoLiquido() / item.getCarteiraInicial();
		efc = efc *100;	    	  
		item.setEficacia(efc);
		Double falta = item.getCarteiraInicial() * (item.getObj()/100);
		falta = falta - item.getRecebimentoLiquido();
		item.setFaltaReceber(falta);
		item.setAbertoFinal(item.getCarteiraInicial()-item.getRecebimentoLiquido());
		item.setAbertoInicial(item.getRecebimentoMesVirgente()+item.getAbertoFinal());

		Map<String, Object> retorno = new HashMap<String, Object>();
		retorno.put("item", item);
		retorno.put("dataInicial",dataInicial);
		retorno.put("dataFinal", dataFinal);
		return retorno;

	}

	/////////////////////////////////////////////////////////////////////*POR VENDEDOR*/






	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente}, nomeAcao="Gerar Relatório Carteira Cobrança Por Vendedor")
	public void gerarRelatorioVendedor() throws Exception
	{
		byte[] bytes = null;  
		try 
		{  
			EmpresaFisica empresa = null;
			if(request.getParameter("idEmpresaFisica") != null && !request.getParameter("idEmpresaFisica").equals(""))
			{
				empresa  = new EmpresaFisicaService().recuperarPorId(Integer.parseInt(request.getParameter("idEmpresaFisica"))); 
			}

			List<Vendedor> listaVendedor = new ArrayList<>();

			String vendedores[] = request.getParameter("iIdVendedor").split(";"); 

			for (int i = 0; i < vendedores.length; i++) {
				Vendedor vendedorRecuperado = new Vendedor();
				vendedorRecuperado = new VendedorService().recuperarPorId(Integer.parseInt(vendedores[i]));
				listaVendedor.add(vendedorRecuperado);

			}
			/*
 	   	if(request.getParameter("iIdVendedor") != null && !request.getParameter("iIdVendedor").equals(""))
 	   	{
 	   		listaVendedor.add(new VendedorService().recuperarPorId(Integer.parseInt(request.getParameter("idVendedor"))));
 	   	}
 	   	else
 	   	{
 	   		Vendedor vc = new Vendedor();
 	   		vc.setEmpresaFisica(empresa);
 	   		listaVendedor = new VendedorService().listarPorObjetoFiltro(vc);
 	   	}
			 */
			List<ItemRelatorioCateiraCobranca> listaItens = new ArrayList<>();
			String caminho = "";
			for (int x = 0; x < listaVendedor.size(); x++)
			{
				Vendedor vendedor = new Vendedor();	
				vendedor = listaVendedor.get(x);

				String[] ids = request.getParameter("idTabelas").split(",");
				List<TabelaCarteiraCobranca> listaTabelas = new ArrayList<>();
				for (int j = 0; j < ids.length; j++) 
				{
					listaTabelas.add(new TabelaCarteiraCobrancaService().recuperarPorId(Integer.parseInt(ids[j])));	
				}

				TabelaCarteiraCobranca tp = listaTabelas.get(0);
				Calendar calprimerio = Calendar.getInstance();
				calprimerio.setTime(tp.getData());
				calprimerio.add(Calendar.MONTH, +1);
				calprimerio.set(Calendar.DAY_OF_MONTH, 1);
				DateFormat formatadorDataSql = new SimpleDateFormat("yyyy-MM-dd");
				String dataPrimeiro = formatadorDataSql.format(calprimerio.getTime());
				for (int j = 0; j < listaTabelas.size()-1; j++) 
				{
					TabelaCarteiraCobranca t = listaTabelas.get(j);
					Calendar cal = Calendar.getInstance();
					cal.setTime(t.getData());
					String mes = ""+(cal.get(Calendar.MONTH)+1);
					String ano = ""+cal.get(Calendar.YEAR);
					ItemRelatorioCateiraCobranca item = new ItemRelatorioCateiraCobranca();
					item.setVendedor(vendedor.getNome());
					item.setIdVendedor(vendedor.getId());
					item.setMesAno((mes.length()==1?"0"+mes:mes)  +"/"+ano);
					String str = "";
					str = str + " select  sum(if(par.tipopagamento='5',  par.valorParcela,0.0)) as acordo, sum(if(par.numeroParcela <>0,  par.valorParcela,0.0)) as parcela"; 
					str = str + " from duplicataParcela par  "; 
					str = str + " inner join duplicata d on par.idDuplicata = d.idDuplicata  "; 
					str = str + " inner join Vendedor v on v.idVendedor = d.idVendedor  "; 
					str = str + " where month(par.dataVencimento) = "+mes; 
					str = str + " and year(par.dataVencimento)= "+ano; 
					str = str + " and par.idempresaFisica = "+t.getEmpresaFisica().getId(); 
					if(!request.getParameter("iIdVendedor").equals(""))
						str = str + " and v.idVendedor =(  "+vendedor.getId()+") "; 
					System.out.println(str);
					Connection conn; 	        
					conn = ConexaoUtil.getConexaoPadrao();    
					java.sql.Statement rec = conn.createStatement();    
					ResultSet rs = rec.executeQuery(str);    
					if(rs.next())
					{
						item.setCarteiraInicial(rs.getDouble("parcela"));
						item.setAcordo(rs.getDouble("acordo"));
					}
					rs.close();
					rec.close();
					conn.close();    	 

					String sqlRE = "";
					sqlRE = sqlRE + " select Round(sum(p.valorPago - ifnull(p.jurosPago,0) + ifnull(p.valorDesconto,0)),2) as valorRecebido "; 
					sqlRE = sqlRE + " from duplicataparcela p "; 
					sqlRE = sqlRE + " inner join duplicata d on d.idDuplicata = p.idDuplicata "; 
					sqlRE = sqlRE + " inner join Vendedor v on v.idVendedor = d.idVendedor  "; 
					sqlRE = sqlRE + " where month(p.datavencimento) =  "+mes; 
					sqlRE = sqlRE + " and year(p.datavencimento) =  "+ano; 
					sqlRE = sqlRE + " and p.idEmpresaFisica = "+t.getEmpresaFisica().getId(); 
					sqlRE = sqlRE + " and p.numeroParcela <> 0  "; 
					sqlRE = sqlRE + " and p.tipoPagamento in(1,2) "; 
					sqlRE = sqlRE + " and v.idVendedor =  "+vendedor.getId(); 

					System.out.println(sqlRE);
					conn = ConexaoUtil.getConexaoPadrao();
					rec = conn.createStatement();
					rs = rec.executeQuery(sqlRE);
					if(rs.next())
					{
						item.setRecebimentoLiquido(rs.getDouble("valorRecebido"));
					}

					rs.close();
					rec.close();
					conn.close();

					String strMes = "";
					strMes = strMes + " select Round(sum(p.valorPago - ifnull(p.jurosPago,0) + ifnull(p.valorDesconto,0)),2) as valorRecebido "; 
					strMes = strMes + " from duplicataparcela p "; 
					strMes = strMes + " inner join duplicata d on d.idDuplicata = p.idDuplicata "; 
					strMes = strMes + " inner join Vendedor v on v.idVendedor = d.idVendedor  ";
					strMes = strMes + " where month(p.databaixa) =  "+mes; 
					strMes = strMes + " and year(p.databaixa) =  "+ano; 
					strMes = strMes + " and month(p.dataVencimento) =  "+mes; 
					strMes = strMes + " and year(p.dataVencimento) =  "+ano; 
					strMes = strMes + " and p.idEmpresaFisica =  "+t.getEmpresaFisica().getId(); 
					strMes = strMes + " and p.numeroParcela <>0  "; 
					strMes = strMes + " and p.tipoPagamento in(1,2) "; 
					strMes = strMes + " and v.idVendedor =  "+vendedor.getId(); 

					conn = ConexaoUtil.getConexaoPadrao();
					rec = conn.createStatement();
					rs = rec.executeQuery(strMes);

					if(rs.next())
					{
						item.setRecebimentoMesVirgente(rs.getDouble("valorRecebido"));  
					}
					rs.close();
					rec.close();
					conn.close();

					item.setObj(t.getPercentual());
					Double efc = item.getRecebimentoLiquido() / item.getCarteiraInicial();
					efc = efc *100;	    	  
					item.setEficacia(efc);
					Double falta = item.getCarteiraInicial() * (item.getObj()/100);
					falta = falta - item.getRecebimentoLiquido();
					item.setFaltaReceber(falta);
					item.setAbertoFinal(item.getCarteiraInicial()-item.getRecebimentoLiquido());
					item.setAbertoInicial(item.getRecebimentoMesVirgente()+item.getAbertoFinal());
					rs.close();
					rec.close();
					conn.close();

					listaItens.add(item);

				}
				Map<String, Object>  retorno = this.gerarUltimoItemVendedor(listaTabelas, dataPrimeiro, vendedor);
				ItemRelatorioCateiraCobranca itemUltimo = (ItemRelatorioCateiraCobranca) retorno.get("item");
				listaItens.add(itemUltimo);

			}
			caminho = "relatorioCarteiraCobrancaVendedor.jasper"; 

			JRBeanCollectionDataSource jrRS = new JRBeanCollectionDataSource(listaItens) ;




			URL urlArquivo = getClass().getResource("/relatorios/"+caminho);
			String rel = urlArquivo.getPath();

			Image logo = null;
			InputStream inputStreamDaImagem = null;
			URL urlImagens = getClass().getResource("/imagens/logo.jpg");
			String caminhoImagem = urlImagens.getPath();

			try {    
				File file = new File(caminhoImagem);  

				if(file.exists()){//testa se imagem existe  
					inputStreamDaImagem = new BufferedInputStream(
							new FileInputStream(caminhoImagem));
					logo = ImageIO.read(inputStreamDaImagem);
				}else{
					logo =null;
				}


			} catch (FileNotFoundException e) {       

				logo =null;
			}       

			URL urlSub= getClass().getResource("/relatorios/");

			Map<String, Object> params = new HashMap<String, Object>();

			params.put("logo", logo);             
			params.put("HABILITAR_DATA", "N");
			params.put("SUBREPORT_DIR", urlSub.getPath());
			params.put("empresa", "F-"+empresa.getId()+"-"+empresa.getRazaoSocial());

			JasperPrint impressao = JasperFillManager.fillReport(rel, params,jrRS); 
			bytes = JasperExportManager.exportReportToPdf(impressao);  


		}catch (JRException | SQLException e) {  
			throw new ServletException(e);  
		}  

		if (bytes != null && bytes.length > 0) {  
			// envia o relatário em formato PDF para o browser  
			response.setContentType("application/pdf");  
			response.setContentLength(bytes.length);  
			ServletOutputStream ouputStream = response.getOutputStream();  
			ouputStream.write(bytes, 0, bytes.length);  
			ouputStream.flush();  
			ouputStream.close();  
		}   
	}
	public  Map<String, Object>  gerarUltimoItemVendedor(List<TabelaCarteiraCobranca> listaTabelas,String dataPrimeiro,Vendedor vendedor) throws SQLException
	{
		System.out.println("Ultimo item");
		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
		SimpleDateFormat dfa = new SimpleDateFormat ("dd/MM/yyyy");
		String dataInicial = "";
		String dataFinal = "";
		String dataSInicial = "";
		String dataSFinal = "";

		ItemRelatorioCateiraCobranca item = new ItemRelatorioCateiraCobranca();
		item.setMesAno("*ACIMA*");
		TabelaCarteiraCobranca t = listaTabelas.get(listaTabelas.size()-1);
		item.setIdVendedor(vendedor.getId());
		item.setVendedor(vendedor.getNome());
		Calendar cal = Calendar.getInstance();
		cal.setTime(t.getData()); 
		cal.set(Calendar.DAY_OF_MONTH, 1);
		dataSInicial = df.format(cal.getTime());
		dataInicial = dfa.format(cal.getTime());

		t = listaTabelas.get(listaTabelas.size()-2);
		cal = Calendar.getInstance();
		cal.setTime(t.getData()); 
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		dataFinal =  dfa.format(cal.getTime());
		dataSFinal = df.format(cal.getTime());


		String str = "";
		str = str + "  select sum(if(par.tipopagamento='5',  par.valorParcela,0.0)) as acordo, sum(if(par.numeroParcela <>0,  par.valorParcela,0.0)) as parcela   "; 
		str = str + "  from duplicataParcela par   "; 
		str = str + " inner join duplicata d on d.idDuplicata = par.idDuplicata "; 
		str = str + " inner join Vendedor v on v.idVendedor = d.idVendedor  ";
		str = str + " where par.dataVencimento <= '"+dataSFinal+"' "; 
		str = str + " and par.dataVencimento >= '"+dataSInicial+"'"; 
		str = str + " and par.idempresaFisica =  "+t.getEmpresaFisica().getId(); 
		str = str + " and v.idVendedor =  "+vendedor.getId(); 
		Connection conn; 	        
		conn = ConexaoUtil.getConexaoPadrao();    
		java.sql.Statement rec = conn.createStatement();    
		ResultSet rs = rec.executeQuery(str);    
		if(rs.next())
		{
			item.setCarteiraInicial(rs.getDouble("parcela"));
			item.setAcordo(rs.getDouble("acordo"));
		}
		rs.close();
		rec.close();
		conn.close();

		String strRE = "";
		strRE = strRE + " select Round(sum(p.valorPago - ifnull(p.jurosPago,0) + ifnull(p.valorDesconto,0)),2) as valorRecebido "; 
		strRE = strRE + " from duplicataparcela p "; 
		strRE = strRE + " inner join duplicata d on d.idDuplicata = p.idDuplicata "; 
		strRE = strRE + " inner join Vendedor v on v.idVendedor = d.idVendedor  ";
		strRE = strRE + " where p.datavencimento <= '"+dataSFinal+"' "; 
		strRE = strRE + " and p.datavencimento >=  '"+dataSInicial+"' "; 
		strRE = strRE + " and p.idEmpresaFisica = "+t.getEmpresaFisica().getId(); 
		strRE = strRE + " and p.numeroParcela <>0  "; 
		strRE = strRE + " and p.tipoPagamento in(1,2) "; 
		strRE = strRE + " and v.idVendedor =  "+vendedor.getId(); 

		conn = ConexaoUtil.getConexaoPadrao();    
		rec = conn.createStatement();    
		rs = rec.executeQuery(strRE);  
		System.out.println(strRE);
		if(rs.next())
		{
			item.setRecebimentoLiquido(rs.getDouble("valorRecebido"));
		}

		rs.close();
		rec.close();
		conn.close();

		String strMes = "";
		strMes = strMes + " select Round(sum(p.valorPago - ifnull(p.jurosPago,0) + ifnull(p.valorDesconto,0)),2) as valorRecebido "; 
		strMes = strMes + " from duplicataparcela p "; 
		strMes = strMes + " inner join duplicata d on d.idDuplicata = p.idDuplicata "; 
		strMes = strMes + " inner join Vendedor v on v.idVendedor = d.idVendedor  ";
		strMes = strMes + " where p.databaixa <= '"+dataSFinal+"' "; 
		strMes = strMes + " and p.databaixa >=  '"+dataSInicial+"' "; 
		strMes = strMes + " and p.dataVencimento <= '"+dataSFinal+"' "; 
		strMes = strMes + " and p.dataVencimento >=  '"+dataSInicial+"' ";        
		strMes = strMes + " and p.idEmpresaFisica = "+t.getEmpresaFisica().getId(); 
		strMes = strMes + " and p.numeroParcela <>0  "; 
		strMes = strMes + " and p.tipoPagamento in(1,2) "; 
		strMes = strMes + " and v.idVendedor =  "+vendedor.getId(); 

		conn = ConexaoUtil.getConexaoPadrao();    
		rec = conn.createStatement();    
		rs = rec.executeQuery(strMes); 

		if(rs.next())
		{
			item.setRecebimentoMesVirgente(rs.getDouble("valorRecebido"));
		}
		rs.close();
		rec.close();
		conn.close();



		item.setObj(listaTabelas.get(listaTabelas.size()-1).getPercentual());
		Double efc = item.getRecebimentoLiquido() / item.getCarteiraInicial();
		efc = efc *100;	    	  
		item.setEficacia(efc);
		Double falta = item.getCarteiraInicial() * (item.getObj()/100);
		falta = falta - item.getRecebimentoLiquido();
		item.setFaltaReceber(falta);
		item.setAbertoFinal(item.getCarteiraInicial()-item.getRecebimentoLiquido());
		item.setAbertoInicial(item.getRecebimentoMesVirgente()+item.getAbertoFinal());

		Map<String, Object> retorno = new HashMap<String, Object>();
		retorno.put("item", item);
		retorno.put("dataInicial",dataInicial);
		retorno.put("dataFinal", dataFinal);
		System.out.println("Ultimo Item Vendedor "+vendedor.getNome());
		return retorno;

	}
}
