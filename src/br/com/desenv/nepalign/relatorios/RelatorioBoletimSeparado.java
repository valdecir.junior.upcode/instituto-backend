package br.com.desenv.nepalign.relatorios;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import org.apache.log4j.Logger;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao.Grupo;
import br.com.desenv.nepalign.model.CaixaDia;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.service.CaixaDiaService;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.UsuarioService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class RelatorioBoletimSeparado 
{
	String caminho;

	public Connection conn = ConexaoUtil.getConexaoPadrao();  
	private HttpServletRequest request;
	private HttpServletResponse response;
	
	private static final Logger logger = Logger.getLogger(IgnRelatorioSegurancaServlet.class);

	public RelatorioBoletimSeparado(HttpServletRequest request, HttpServletResponse response) throws ServletException, Exception
	{
		this.request=request;
		this.response=response;
	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Caixa}, nomeAcao="Gerar Relatório Boletim Separado")
	public void gerarRelatorio() throws Exception
	{
		byte[] bytes = null;  
		try 
		{  
			String dataEmissaoInicial = "";
			String dataEmissaoFinal = "";
			String dataSqlEmissaoInicial = null;
			String dataSqlEmissaoFinal = null;
			
			dataEmissaoInicial = request.getParameter("dataEmissaoInicial");
			dataEmissaoFinal = request.getParameter("dataEmissaoFinal");
			if(!dataEmissaoInicial.equals(""))
				dataSqlEmissaoInicial = IgnUtil.dateFormatSql.format(IgnUtil.dateFormat.parse(dataEmissaoInicial));
			if(!dataEmissaoFinal.equals(""))
				dataSqlEmissaoFinal = IgnUtil.dateFormatSql.format(IgnUtil.dateFormat.parse(dataEmissaoFinal));

			Integer idCaixaDia = Integer.parseInt(request.getParameter("idCaixaDia"));
			CaixaDia caixaDia = new CaixaDiaService().recuperarPorId(idCaixaDia);
			Integer idEmpresaFisica = Integer.parseInt(request.getParameter("idEmpresaFisica"));
			DateFormat formataData  = new SimpleDateFormat("dd/MM/yyyy");
			Date dataFormataInicial = formataData.parse(request.getParameter("dataCaixa"));
			DateFormat formatadorDataSql = new SimpleDateFormat("yyyy-MM-dd");
			Integer idSituacaoPedidoVendaCancelado = DsvConstante.getParametrosSistema().getIdSituacaoPedidoVendaCancelado();    
			String query = "";
			String querySub = "";
			Statement rec = null;
			ResultSet rs = null;
			JRResultSetDataSource jrRS = null;
			JRResultSetDataSource jrRSSub = null;
			
			String nomeGrupo = request.getParameter("grupoEmpresa").equals("SER")?"SERALLE":"JORROVI";
		
			Boolean uae = new UsuarioService().getSessionUae(request);

			if(uae)
			{
				if(nomeGrupo.equals("SERALLE"))
				{
					query +=" (SELECT 0 as tipo, 'VENDA GERAL DO DIA' as descricao, ifnull(sum(pedido.valorTotalPedido),0) as valorOriginal,  ifnull(sum(pedido.valorTotalPedido),0) as debito, 0 as credito  ";
		        	query +=" FROM pedidovenda pedido ";
		        	query +=" INNER JOIN PlanoPagamento plano on plano.idPlanoPagamento = pedido.IdPlanoPagamento ";
		        	query +=" LEFT OUTER JOIN tipopagamento tipoPagamento ON tipoPagamento.idTipoPagamento = plano.idTipoPagamento ";
		        	query +=" WHERE pedido.idEmpresaFisica = " + idEmpresaFisica + " and pedido.dataVenda between '"+formatadorDataSql.format(dataFormataInicial)+" 00:00:00' AND '"+formatadorDataSql.format(dataFormataInicial)+" 23:59:59'";
		        	query +=" and pedido.idSituacaoPedidoVenda <> "+ idSituacaoPedidoVendaCancelado + ") ";
		        	
		        	query +=" union ";
		        	query +=" (select 2 as tipo,'TROCO DO DIA ANTERIOR' as descricao, ";
		        	query +=" valorInicial AS valorOriginal, ";
		        	query +=" valorInicial AS DEBITO, ";
		        	query +=" 0 as Credito from caixadia where idcaixadia="+ idCaixaDia + ") ";

		        	query +=" UNION ";
		        	query +="(SELECT 2 as tipo, CONCAT('VENDA ',tipo.descricao) as descricao ,sum(pedido.valortotalpedido) as valorOriginal, 0 as DEBITO, sum(pedido.valortotalpedido) as CREDITO ";
		        	query +="FROM pedidovenda pedido ";  
		        	query +="INNER JOIN planopagamento plano on plano.idPlanoPagamento = pedido.IdPlanoPagamento ";  
		        	query +="INNER JOIN tipopagamento tipo on tipo.idTipoPagamento = plano.idTipoPagamento ";
		        	query +="INNER JOIN empresafisica empresa on empresa.idEmpresaFisica = pedido.idEmpresaFisica "; 
		        	query +="where pedido.idSituacaoPedidoVenda <> "+idSituacaoPedidoVendaCancelado+ " and pedido.idEmpresaFisica = "+idEmpresaFisica+" "; 
		        	query +="and pedido.dataVenda between '"+formatadorDataSql.format(dataFormataInicial)+" 00:00:00' AND '"+formatadorDataSql.format(dataFormataInicial)+" 23:59:59' ";
		        	query +="and plano.idTipoPagamento not in (1,4) ";
		        	query +="group by plano.idTipoPagamento ) ";

		        	query +=" union  (";
		        	query +=" SELECT 2 as idtipoPagamento, 'VENDA A CREDITO' as descricao,";  
		        	query +=" SUM(IFNULL(pedido.valorTotalPedido, 0)) as valorOriginal,";   
		        	query +=" 0 AS DEBITO," ;
		        	query +=" SUM(IFNULL(pedido.valorTotalPedido, 0)) as credito ";  
		        	query +=" FROM pedidovenda pedido";  
		        	query +=" INNER JOIN PlanoPagamento plano on plano.idPlanoPagamento = pedido.IdPlanoPagamento";  
		        	query +=" LEFT OUTER JOIN tipopagamento tipoPagamento ON tipoPagamento.idTipoPagamento = plano.idTipoPagamento";  
		        	query +=" WHERE pedido.idEmpresaFisica = " + idEmpresaFisica + " " ;
		        	query +=" and pedido.dataVenda between '"+formatadorDataSql.format(dataFormataInicial)+" 00:00:00' AND '"+formatadorDataSql.format(dataFormataInicial)+" 23:59:59'";
		        	query +=" and (pedido.idSituacaoPedidoVenda <> 5) and plano.idTipoPagamento = (4) ";
		        	query +=" group by plano.idTipoPagamento) ";
		         	
		        	query += " union ";
		        	query += " (SELECT 1 as tipo, 'ENTRADA ACORDO' as descricao, ";
					query += "  SUM(COALESCE(mov.valor,0)) as valorOriginal, ";
					query += "  SUM(COALESCE(mov.valor,0)) as debito, 0 as credito ";
					query += " from MovimentoCaixaDia mov inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ";
					query += " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
					query += " left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ";
					query += " left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata and movimentoParcela.idEmpresaFisica = caixa.idEmpresaFisica";
					query += " left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela and parcelaPaga.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicatapaga.idempresafisica = caixa.idempresafisica ";
					query += " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
					query += " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
					query += " group by ac.idDuplicataNova) "; 
					query += " as acordo on acordo.idDuplicataNova=duplicatapaga.idduplicata ";
					if((idCaixaDia != null) && (idCaixaDia != 0))
		        		query +=" where mov.idCaixaDia = "+ idCaixaDia + " and (parcelaPaga.numeroParcela = 0) and parcelaPaga.acordo='S' ";
					if(dataSqlEmissaoInicial != null && dataSqlEmissaoFinal != null)
						query += " and acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00' and acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59' ";
		        	query +=" GROUP BY tipomovimento.origem order by tipomovimento.origem) ";
		        	
					query += " union "; 
					query += " (SELECT 1 as tipo, CONCAT('ENTRADA ACORDO ',forma.descricao) as descricao, ";
					query += "  SUM(COALESCE(mov.valor,0)) as valorOriginal, ";
					query += "  0 as debito, SUM(COALESCE(mov.valor,0)) as credito ";
					query += " from MovimentoCaixaDia mov inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ";
					query += " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
					query += " left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ";
					query += " left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata and movimentoParcela.idEmpresaFisica = caixa.idEmpresaFisica";
					query += " left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela and parcelaPaga.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicatapaga.idempresafisica = caixa.idempresafisica ";
					query += " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
					query += " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
					query += " group by ac.idDuplicataNova) "; 
					query += " as acordo on acordo.idDuplicataNova=duplicatapaga.idduplicata ";
					if((idCaixaDia != null) && (idCaixaDia != 0)) 
						query += " where mov.idCaixaDia = "+ idCaixaDia + " and (parcelaPaga.numeroParcela = 0) AND forma.tipo<>1 and parcelaPaga.acordo='S' ";
					if(dataSqlEmissaoInicial != null && dataSqlEmissaoFinal != null)
						query += " and acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00' and acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59' ";
					query += " GROUP BY tipomovimento.origem, forma.descricao order by tipomovimento.origem, forma.descricao) ";
		        	
					query += " union ";
					query += " (SELECT 1 as tipo, 'ENTRADA CREDIARIO' as descricao, ";
					query += "  SUM(COALESCE(mov.valor,0)) as valorOriginal, ";
					query += "  SUM(COALESCE(mov.valor,0)) as debito, 0 as credito ";
					query += " from MovimentoCaixaDia mov inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ";
					query += " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
					query += " left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ";
					query += " left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata and movimentoParcela.idEmpresaFisica = caixa.idEmpresaFisica";
					query += " left outer join duplicataparcela as parcelaCriada on parcelaCriada.idDuplicataParcela = mov.idDuplicataParcela and parcelaCriada.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicata as duplicataCriada on duplicataCriada.idDuplicata = parcelaCriada.idDuplicata and duplicataCriada.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela and parcelaPaga.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicatapaga.idempresafisica = caixa.idempresafisica ";
		        	if((idCaixaDia != null) && (idCaixaDia != 0))
		        		query += " where mov.idCaixaDia = "+ idCaixaDia + " and (parcelaPaga.numeroParcela = 0) and parcelaPaga.acordo<>'S' ";
		        	if(dataSqlEmissaoInicial != null)
						query += " and duplicatapaga.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00' ";
					if(dataSqlEmissaoFinal != null)
						query += " and duplicatapaga.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59' ";

					query += " GROUP BY tipomovimento.origem order by tipomovimento.origem) ";
		        	
					query += " union ";
					query += " (SELECT 1 as tipo, CONCAT('ENTRADA CREDIARIO ',forma.descricao) as descricao, ";
					query += "  SUM(COALESCE(mov.valor,0)) as valorOriginal, ";
					query += "  0 as debito, SUM(COALESCE(mov.valor,0)) as credito ";
					query += " from MovimentoCaixaDia mov inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ";
					query += " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
					query += " left outer join tipoconta on tipoconta.idTipoConta = tipomovimento.idTipoConta ";
					query += " left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ";
					query += " left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata and movimentoParcela.idEmpresaFisica = caixa.idEmpresaFisica";
					query += " left outer join duplicataparcela as parcelaCriada on parcelaCriada.idDuplicataParcela = mov.idDuplicataParcela and parcelaCriada.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicata as duplicataCriada on duplicataCriada.idDuplicata = parcelaCriada.idDuplicata and duplicataCriada.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela and parcelaPaga.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicatapaga.idempresafisica = caixa.idempresafisica ";
		        	if((idCaixaDia != null) && (idCaixaDia != 0)) 
		        		query += " where mov.idCaixaDia = "+ idCaixaDia + " and (parcelaPaga.numeroParcela = 0) AND forma.tipo<>1 and parcelaPaga.acordo<>'S' "; 
		        	query += " GROUP BY tipomovimento.origem, forma.descricao order by tipomovimento.origem, forma.descricao) ";
		        	
		        	query += "union (";
		        	query += " select 2 as tipo, CONCAT('ENTRADA ',(";
		        	query += " select formaPagamentoPagPlano.descricao from pagamentoPedidoVenda pagPlano inner join formaPagamento formaPagamentoPagPlano ON formaPagamentoPagPlano.idFormaPagamento = pagPlano.idFormaPagamento ";
		        	query += " where pagPlano.idPedidoVenda = pedido.idPedidoVenda and pagPlano.entrada = 'N' limit 1)) as descricao, ";
		        	query += " SUM(IFNULL(pag.valor, 0)) as valorOriginal, SUM(IFNULL(pag.valor, 0)) AS DEBITO, ";
		        	query += " 0 as credito ";
		        	query += " FROM pedidovenda pedido ";
		        	query += " INNER JOIN PlanoPagamento plano on plano.idPlanoPagamento = pedido.IdPlanoPagamento ";
		        	query += " INNER JOIN pagamentopedidovenda pag on pag.idPedidoVenda = pedido.idPedidoVenda "; 
		        	query += " INNER JOIN formapagamento forma on forma.idFormaPagamento = pag.idFormaPagamento ";
		        	query += " INNER JOIN planopagamento planoPagamento on planoPagamento.idPlanoPagamento = pedido.IdPlanoPagamento";
		        	query += " INNER JOIN tipoPagamento tipoPagamento on tipoPagamento.idTipoPagamento = plano.idTipoPagamento";
		        	query += " WHERE pedido.idEmpresaFisica =" + idEmpresaFisica + " ";
		        	query += " and pedido.dataVenda between '"+formatadorDataSql.format(dataFormataInicial)+" 00:00:00' AND '"+formatadorDataSql.format(dataFormataInicial)+" 23:59:59' ";
		        	query += " and pedido.idSituacaoPedidoVenda <> 5 and pag.entrada = 'S' and tipoPagamento.idtipopagamento <>1 group by descricao) ";
		        	
		        	query +="union (";
		        	query +=" select 2 as tipo, CONCAT('ENTRADA ',(";
		        	query +=" select formaPagamentoPagPlano.descricao from pagamentoPedidoVenda pagPlano inner join formaPagamento formaPagamentoPagPlano ON formaPagamentoPagPlano.idFormaPagamento = pagPlano.idFormaPagamento ";
		        	query +=" where pagPlano.idPedidoVenda = pedido.idPedidoVenda and pagPlano.entrada = 'N' limit 1),' ',forma.descricao) as descricao, ";
		        	query +=" SUM(IFNULL(pag.valor, 0)) as valorOriginal, 0 AS DEBITO, ";
		        	query +=" SUM(IFNULL(pag.valor, 0)) as credito ";
		        	query +=" FROM pedidovenda pedido ";
		        	query +=" INNER JOIN PlanoPagamento plano on plano.idPlanoPagamento = pedido.IdPlanoPagamento ";
		        	query +=" INNER JOIN pagamentopedidovenda pag on pag.idPedidoVenda = pedido.idPedidoVenda "; 
		        	query +=" INNER JOIN formapagamento forma on forma.idFormaPagamento = pag.idFormaPagamento ";
		        	query +=" INNER JOIN planopagamento planoPagamento on planoPagamento.idPlanoPagamento = pedido.IdPlanoPagamento";
		        	query +=" INNER JOIN tipoPagamento tipoPagamento on tipoPagamento.idTipoPagamento = plano.idTipoPagamento";
		        	query +=" WHERE pedido.idEmpresaFisica =" + idEmpresaFisica + " ";
		        	query +=" and pedido.dataVenda between '"+formatadorDataSql.format(dataFormataInicial)+" 00:00:00' AND '"+formatadorDataSql.format(dataFormataInicial)+" 23:59:59' ";
		        	query +=" and pedido.idSituacaoPedidoVenda <> 5 and pag.entrada = 'S' and forma.tipo<>1 group by plano.descricao) ";
		        	
		        	query += " union (";
		        	query += " SELECT 2 as tipo, 'RECEB. DE PRESTAÇÃO' as descricao, "; 
					query += " SUM(COALESCE(movimentoparcela.valorRecebido,0)) as valorOriginal, ";
					query += " ROUND(sum(IFNULL(movimentoparcela.valorRecebido, 0)-IFNULL(if(movimentoparcela.tipopagamento=2,0,movimentoparcela.valorEntradaJuros),0)+IFNULL(if(movimentoparcela.tipopagamento=2,0,movimentoparcela.valorDesconto),0)),2) as debito, ";
					query += " 0 as credito ";
					query += " from MovimentoCaixaDia mov inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ";
					query += " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
					query += " left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ";
					query += " left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata and movimentoParcela.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela and parcelaPaga.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicatapaga.idempresafisica = caixa.idempresafisica ";
					query += " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
					query += " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
					query += " group by ac.idDuplicataNova) "; 
					query += " as acordo on acordo.idDuplicataNova=duplicatapaga.idduplicata ";
					query += " where mov.idCaixaDia = " + idCaixaDia + " and tipoMovimento.origem = 2 and parcelapaga.numeroparcela<>0 and duplicatapaga.idempresafisica=" + idEmpresaFisica + " " ;
					if(dataSqlEmissaoInicial != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00', acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
					if(dataSqlEmissaoFinal != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59') ) ";
					
					query += " union (";
					query += " SELECT 2 as tipo, CONCAT('RECEB. DE PRESTAÇÃO ',forma.descricao) as descricao,";
					query += " ROUND(SUM(COALESCE(movimentoparcela.valorRecebido,0)),2) as valorOriginal, ";
					query += " 0 as debito, ";
					query += " ROUND(SUM(COALESCE(movimentoparcela.valorRecebido,0)),2) as credito ";
					query += " from MovimentoCaixaDia mov inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ";
					query += " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
					query += " left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ";
					query += " left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata and movimentoParcela.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela and parcelaPaga.idEmpresaFisica = caixa.idempresaFisica ";
					query += " left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicatapaga.idempresafisica = caixa.idempresafisica ";
					query += " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
					query += " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
					query += " group by ac.idDuplicataNova) "; 
					query += " as acordo on acordo.idDuplicataNova=duplicatapaga.idduplicata ";
					query += " where mov.idCaixaDia = " + idCaixaDia + " and forma.tipo<>1 and parcelaPaga.numeroParcela <>0 and COALESCE(parcelaPaga.valorParcela,0) >0 and duplicatapaga.idempresafisica=" + idEmpresaFisica + " ";
					if(dataSqlEmissaoInicial != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00', acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
					if(dataSqlEmissaoFinal != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59') ";
		        	query += " GROUP BY forma.descricao order by forma.descricao ) ";

					
					query += " union ("; 
					query += " select 3 as tipo, concat('JUROS DE CREDIARIO') as descricao, ";
					query += " COALESCE(sum(movimentoparcela.valorEntradaJuros),0) as valor, ";
					query += " COALESCE(ROUND(SUM(if(movimentoparcela.tipopagamento=2,0,movimentoparcela.valorEntradaJuros)),2),0) as debito, ";
					query += " 0 as credito ";
					query += " from MovimentoCaixaDia mov ";
					query += " INNER JOIN CaixaDia caixa ON caixa.idCaixaDia = mov.idCaixaDia ";
					query += " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
					query += " left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata and movimentoParcela.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela and parcelaPaga.idEmpresaFisica = caixa.idEmpresaFisica "; 
					query += " left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicatapaga.idempresafisica = caixa.idempresafisica ";
					query += " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
					query += " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
					query += " group by ac.idDuplicataNova) "; 
					query += " as acordo on acordo.idDuplicataNova=duplicatapaga.idduplicata ";
					query += " where mov.idCaixaDia = " + idCaixaDia + " and caixa.idEmpresafisica =("+idEmpresaFisica+") " ;
					query += " and tipomovimento.origem=2 ";
					if(dataSqlEmissaoInicial != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00', acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
					if(dataSqlEmissaoFinal != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59') ) ";
	
					query += " union ("; 
					query += " select 3 as tipo, concat('DESCONTO DE CREDIARIO') as descricao, ";
					query += " COALESCE(sum(movimentoparcela.valorEntradaJuros),0) as valor, ";
					query += " 0 as debito, ";
					query += " COALESCE(ROUND(SUM(if(movimentoparcela.tipopagamento=2,0,movimentoparcela.valorDesconto)),2),0) as credito ";
					query += " from MovimentoCaixaDia mov ";
					query += " INNER JOIN CaixaDia caixa ON caixa.idCaixaDia = mov.idCaixaDia ";
					query += " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
					query += " left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata and movimentoParcela.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela and parcelaPaga.idEmpresaFisica = caixa.idEmpresaFisica "; 
					query += " left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicatapaga.idempresafisica = caixa.idempresafisica ";
					query += " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
					query += " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
					query += " group by ac.idDuplicataNova) "; 
					query += " as acordo on acordo.idDuplicataNova=duplicatapaga.idduplicata ";
					query += " where mov.idCaixaDia = " + idCaixaDia + " and caixa.idEmpresafisica =("+idEmpresaFisica+") " ;
					query += " and tipomovimento.origem=2 ";
					if(dataSqlEmissaoInicial != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00', acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
					if(dataSqlEmissaoFinal != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59') ) ";
					
					query += " union ";
					query += " (SELECT 4 as tipo, CONCAT(tipomovimento.descricao) as descricao, ";
					query += " SUM(COALESCE(mov.valor,0)) as valor, SUM(IF (mov.CreditoDebito ='C',mov.valor,0)) as debito, ";
					query += " SUM(IF (mov.CreditoDebito ='D',mov.valor,0)) as credito from MovimentoCaixaDia mov ";
					query += " inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ";
					query += " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
					query += " left outer join tipoconta on tipoconta.idTipoConta = tipomovimento.idTipoConta ";
					query += " left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ";
					query += " left outer join tipomovimentocaixadiagrupoempresa tipogrupoempresa on tipogrupoempresa.idTipoMovimentoCaixaDia = tipoMovimento.idTipoMovimentoCaixaDia ";
					query += " left outer join grupoempresa ge on ge.idGrupoEmpressa = tipogrupoempresa.idGrupoEmpresa ";
					query += " where mov.idCaixaDia = " + idCaixaDia + " and caixa.idEmpresafisica = "+idEmpresaFisica+" ";
					query += " and diversos='S' and (mov.mostrarBoletim <> 'N' or mov.mostrarBoletim is null) ";
					if(DsvConstante.is("JORROVI"))
					{
						if(nomeGrupo.equals("JORROVI"))
							query += " and (ge.descricao = '"+nomeGrupo+"' or tipogrupoempresa.idGrupoEmpresa is null)";
						if(nomeGrupo.equals("SERALLE"))
							query += " and (ge.descricao = '"+nomeGrupo+"')";
					}
					if(DsvConstante.is("SERALLE"))
					{
						if(nomeGrupo.equals("JORROVI"))
							query += " and ge.descricao = '"+nomeGrupo+"' ";
						if(nomeGrupo.equals("SERALLE"))
							query += " and (ge.descricao = '"+nomeGrupo+"' or tipogrupoempresa.idGrupoEmpresa is null)";
					}
					query += " GROUP BY tipomovimento.descricao order by tipomovimento.descricao) ";

					query += " union ";
					query += " (SELECT 4 as tipo, CONCAT(tipomovimento.descricao,' ',forma.descricao) as descricao, ";
					query += " SUM(COALESCE(mov.valor,0)) as valor, 0 as debito, ";
					query += " SUM(IF (mov.CreditoDebito ='C',mov.valor,0)) as credito from MovimentoCaixaDia mov ";
					query += " inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ";
					query += " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
					query += " left outer join tipoconta on tipoconta.idTipoConta = tipomovimento.idTipoConta ";
					query += " left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ";
					query += " left outer join tipomovimentocaixadiagrupoempresa tipogrupoempresa on tipogrupoempresa.idTipoMovimentoCaixaDia = tipoMovimento.idTipoMovimentoCaixaDia ";
					query += " left outer join grupoempresa ge on ge.idGrupoEmpressa = tipogrupoempresa.idGrupoEmpresa ";
					query += " where mov.idCaixaDia = " + idCaixaDia + " and caixa.idEmpresafisica =("+idEmpresaFisica+") ";
					query += " and diversos='S' and (mov.mostrarBoletim <> 'N' or mov.mostrarBoletim is null) and forma.tipo<>1 ";
					if(DsvConstante.is("JORROVI"))
					{
						if(nomeGrupo.equals("JORROVI"))
							query += " and (ge.descricao = '"+nomeGrupo+"' or tipogrupoempresa.idGrupoEmpresa is null)";
						if(nomeGrupo.equals("SERALLE"))
							query += " and (ge.descricao = '"+nomeGrupo+"')";
					}
					if(DsvConstante.is("SERALLE"))
					{
						if(nomeGrupo.equals("JORROVI"))
							query += " and ge.descricao = '"+nomeGrupo+"' ";
						if(nomeGrupo.equals("SERALLE"))
							query += " and (ge.descricao = '"+nomeGrupo+"' or tipogrupoempresa.idGrupoEmpresa is null)";
					}
					query += " GROUP BY tipomovimento.descricao, forma.idFormaPagamento order by tipomovimento.descricao, forma.idFormaPagamento) ";
				}
				//GRUPO JORROVI
				else if(nomeGrupo.equals("JORROVI"))
				{
					query += " (SELECT 1 as tipo, 'ENTRADA ACORDO' as descricao, ";
					query += "  SUM(COALESCE(mov.valor,0)) as valorOriginal, ";
					query += "  SUM(COALESCE(mov.valor,0)) as debito, 0 as credito ";
					query += " from MovimentoCaixaDia mov inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ";
					query += " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
					query += " left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ";
					query += " left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata and movimentoParcela.idEmpresaFisica = caixa.idEmpresaFisica";
					query += " left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela and parcelaPaga.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicatapaga.idempresafisica = caixa.idempresafisica ";
					query += " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
					query += " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
					query += " group by ac.idDuplicataNova) "; 
					query += " as acordo on acordo.idDuplicataNova=duplicatapaga.idduplicata ";
					if((idCaixaDia != null) && (idCaixaDia != 0))
		        		query +=" where mov.idCaixaDia = "+ idCaixaDia + " and (parcelaPaga.numeroParcela = 0) and parcelaPaga.acordo='S' ";
					if(dataSqlEmissaoInicial != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00', acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
					if(dataSqlEmissaoFinal != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59') ";
			    	query +=" GROUP BY tipomovimento.origem order by tipomovimento.origem) ";
		        	
					query += " union "; 
					query += " (SELECT 1 as tipo, CONCAT('ENTRADA ACORDO ',forma.descricao) as descricao, ";
					query += "  SUM(COALESCE(mov.valor,0)) as valorOriginal, ";
					query += "  0 as debito, SUM(COALESCE(mov.valor,0)) as credito ";
					query += " from MovimentoCaixaDia mov inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ";
					query += " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
					query += " left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ";
					query += " left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata and movimentoParcela.idEmpresaFisica = caixa.idEmpresaFisica";
					query += " left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela and parcelaPaga.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicatapaga.idempresafisica = caixa.idempresafisica ";
					query += " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
					query += " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
					query += " group by ac.idDuplicataNova) "; 
					query += " as acordo on acordo.idDuplicataNova=duplicatapaga.idduplicata ";
					if((idCaixaDia != null) && (idCaixaDia != 0)) 
						query += " where mov.idCaixaDia = "+ idCaixaDia + " and (parcelaPaga.numeroParcela = 0) AND forma.tipo<>1 and parcelaPaga.acordo='S' ";
					if(dataSqlEmissaoInicial != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00', acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
					if(dataSqlEmissaoFinal != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59') ";
					query += " GROUP BY tipomovimento.origem, forma.descricao order by tipomovimento.origem, forma.descricao) ";

					query += " union (";
		        	
		        	query += " SELECT 2 as tipo, 'RECEB. DE PRESTAÇÃO' as descricao, "; 
					query += " SUM(COALESCE(movimentoparcela.valorRecebido,0)) as valorOriginal, ";
					query += " ROUND(sum(IFNULL(movimentoparcela.valorRecebido, 0)-IFNULL(if(movimentoparcela.tipopagamento=2,0,movimentoparcela.valorEntradaJuros),0)+IFNULL(if(movimentoparcela.tipopagamento=2,0,movimentoparcela.valorDesconto),0)),2) as debito, 0 as credito ";
					query += " from MovimentoCaixaDia mov inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ";
					query += " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
					query += " left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ";
					query += " left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata and movimentoParcela.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela and parcelaPaga.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicatapaga.idempresafisica = caixa.idempresafisica ";
					query += " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
					query += " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
					query += " group by ac.idDuplicataNova) "; 
					query += " as acordo on acordo.idDuplicataNova=duplicatapaga.idduplicata ";
					query += " where mov.idCaixaDia = " + idCaixaDia + " and tipoMovimento.origem = 2 and parcelapaga.numeroparcela<>0 and duplicatapaga.idempresafisica=" + idEmpresaFisica + " " ;
					if(dataSqlEmissaoInicial != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00', acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
					if(dataSqlEmissaoFinal != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59') ) ";
			    			
					query += " union (";
					query += " SELECT 2 as tipo, CONCAT('RECEB. DE PRESTAÇÃO ',forma.descricao) as descricao,";
					query += " ROUND(SUM(COALESCE(movimentoparcela.valorRecebido,0)),2) as valorOriginal, ";
					query += " 0 as debito, ";
					query += " ROUND(SUM(COALESCE(movimentoparcela.valorRecebido,0)),2) as credito ";
					query += " from MovimentoCaixaDia mov inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ";
					query += " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
					query += " left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ";
					query += " left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata and movimentoParcela.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela and parcelaPaga.idEmpresaFisica = caixa.idempresaFisica ";
					query += " left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicatapaga.idempresafisica = caixa.idempresafisica ";
					query += " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
					query += " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
					query += " group by ac.idDuplicataNova) "; 
					query += " as acordo on acordo.idDuplicataNova=duplicatapaga.idduplicata ";
					query += " where mov.idCaixaDia = " + idCaixaDia + " and forma.tipo<>1 and parcelaPaga.numeroParcela <>0 and COALESCE(parcelaPaga.valorParcela,0) >0 and duplicatapaga.idempresafisica=" + idEmpresaFisica + " ";
					if(dataSqlEmissaoInicial != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00', acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
					if(dataSqlEmissaoFinal != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59') ";
			    	query += " group by forma.idformapagamento order by forma.descricao )";
		
					query += " union ("; 
					query += " select 3 as tipo, concat('JUROS DE CREDIARIO') as descricao, ";
					query += " COALESCE(sum(movimentoparcela.valorEntradaJuros),0) as valor, ";
					query += " COALESCE(ROUND(SUM(if(movimentoparcela.tipopagamento=2,0,movimentoparcela.valorEntradaJuros)),2),0) as debito, ";
					query += " 0 as credito ";
					query += " from MovimentoCaixaDia mov ";
					query += " INNER JOIN CaixaDia caixa ON caixa.idCaixaDia = mov.idCaixaDia ";
					query += " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
					query += " left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata and movimentoParcela.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela and parcelaPaga.idEmpresaFisica = caixa.idEmpresaFisica "; 
					query += " left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicatapaga.idempresafisica = caixa.idempresafisica ";
					query += " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
					query += " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
					query += " group by ac.idDuplicataNova) "; 
					query += " as acordo on acordo.idDuplicataNova=duplicatapaga.idduplicata ";
					query += " where mov.idCaixaDia = " + idCaixaDia + " and caixa.idEmpresafisica =("+idEmpresaFisica+") " ;
					query += " and tipomovimento.origem=2 ";
					if(dataSqlEmissaoInicial != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00', acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
					if(dataSqlEmissaoFinal != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59') )";
			    
					query += " union ("; 
					query += " select 3 as tipo, concat('DESCONTO DE CREDIARIO') as descricao, ";
					query += " COALESCE(sum(movimentoparcela.valorEntradaJuros),0) as valor, ";
					query += " 0 as debito, ";
					query += " COALESCE(ROUND(SUM(if(movimentoparcela.tipopagamento=2,0,movimentoparcela.valorDesconto)),2),0) as credito ";
					query += " from MovimentoCaixaDia mov ";
					query += " INNER JOIN CaixaDia caixa ON caixa.idCaixaDia = mov.idCaixaDia ";
					query += " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
					query += " left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata and movimentoParcela.idEmpresaFisica = caixa.idEmpresaFisica ";
					query += " left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela and parcelaPaga.idEmpresaFisica = caixa.idEmpresaFisica "; 
					query += " left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicatapaga.idempresafisica = caixa.idempresafisica ";
					query += " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
					query += " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
					query += " group by ac.idDuplicataNova) "; 
					query += " as acordo on acordo.idDuplicataNova=duplicatapaga.idduplicata ";
					query += " where mov.idCaixaDia = " + idCaixaDia + " and caixa.idEmpresafisica =("+idEmpresaFisica+") " ;
					query += " and tipomovimento.origem=2 ";
					if(dataSqlEmissaoInicial != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00', acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
					if(dataSqlEmissaoFinal != null)
						query += " and if((parcelaPaga.acordo<>'S' or parcelaPaga.acordo is null),duplicatapaga.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59')) ";
			    	
					query += " union ";
					query += " (SELECT 4 as tipo, CONCAT(tipomovimento.descricao) as descricao, ";
					query += " SUM(COALESCE(mov.valor,0)) as valor, SUM(IF (mov.CreditoDebito ='C',mov.valor,0)) as debito, ";
					query += " SUM(IF (mov.CreditoDebito ='D',mov.valor,0)) as credito from MovimentoCaixaDia mov ";
					query += " inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ";
					query += " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
					query += " left outer join tipoconta on tipoconta.idTipoConta = tipomovimento.idTipoConta ";
					query += " left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ";
					query += " left outer join tipomovimentocaixadiagrupoempresa tipogrupoempresa on tipogrupoempresa.idTipoMovimentoCaixaDia = tipoMovimento.idTipoMovimentoCaixaDia ";
					query += " left outer join grupoempresa ge on ge.idGrupoEmpressa = tipogrupoempresa.idGrupoEmpresa ";
					query += " where mov.idCaixaDia = " + idCaixaDia + " and caixa.idEmpresafisica = "+idEmpresaFisica+" ";
					query += " and diversos='S' and (mov.mostrarBoletim <> 'N' or mov.mostrarBoletim is null) ";
					if(DsvConstante.is("JORROVI"))
					{
						if(nomeGrupo.equals("JORROVI"))
							query += " and (ge.descricao = '"+nomeGrupo+"' or tipogrupoempresa.idGrupoEmpresa is null)";
						if(nomeGrupo.equals("SERALLE"))
							query += " and (ge.descricao = '"+nomeGrupo+"')";
					}
					if(DsvConstante.is("SERALLE"))
					{
						if(nomeGrupo.equals("JORROVI"))
							query += " and ge.descricao = '"+nomeGrupo+"' ";
						if(nomeGrupo.equals("SERALLE"))
							query += " and (ge.descricao = '"+nomeGrupo+"' or tipogrupoempresa.idGrupoEmpresa is null)";
					}
					query += " GROUP BY tipomovimento.descricao order by tipomovimento.descricao) ";

					query += " union ";
					query += " (SELECT 4 as tipo, CONCAT(tipomovimento.descricao,' ',forma.descricao) as descricao, ";
					query += " SUM(COALESCE(mov.valor,0)) as valor, 0 as debito, ";
					query += " SUM(IF (mov.CreditoDebito ='C',mov.valor,0)) as credito from MovimentoCaixaDia mov ";
					query += " inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ";
					query += " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
					query += " left outer join tipoconta on tipoconta.idTipoConta = tipomovimento.idTipoConta ";
					query += " left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ";
					query += " left outer join tipomovimentocaixadiagrupoempresa tipogrupoempresa on tipogrupoempresa.idTipoMovimentoCaixaDia = tipoMovimento.idTipoMovimentoCaixaDia ";
					query += " left outer join grupoempresa ge on ge.idGrupoEmpressa = tipogrupoempresa.idGrupoEmpresa ";
					query += " where mov.idCaixaDia = " + idCaixaDia + " and caixa.idEmpresafisica =("+idEmpresaFisica+") ";
					query += " and diversos='S' and (mov.mostrarBoletim <> 'N' or mov.mostrarBoletim is null) and forma.tipo<>1 ";
					if(DsvConstante.is("JORROVI"))
					{
						if(nomeGrupo.equals("JORROVI"))
							query += " and (ge.descricao = '"+nomeGrupo+"' or tipogrupoempresa.idGrupoEmpresa is null)";
						if(nomeGrupo.equals("SERALLE"))
							query += " and (ge.descricao = '"+nomeGrupo+"')";
					}
					if(DsvConstante.is("SERALLE"))
					{
						if(nomeGrupo.equals("JORROVI"))
							query += " and ge.descricao = '"+nomeGrupo+"' ";
						if(nomeGrupo.equals("SERALLE"))
							query += " and (ge.descricao = '"+nomeGrupo+"' or tipogrupoempresa.idGrupoEmpresa is null)";
					}
						
					query += " GROUP BY tipomovimento.descricao, forma.idFormaPagamento order by tipomovimento.descricao, forma.idFormaPagamento) ";

				}
				
				logger.info("\n QUERY BOLETIM SEPARADO. \n"+query+"\n");

				//Query do Subrelatorio 3 - observações
				querySub += " select mov.valor,mov.observacao as observacao,tipomovimento.descricao as descricaoTipo, tipoconta.descricao as descricaoTipoConta ,"; 
				querySub += " CONCAT(tipomovimento.descricao) as contaGerencial ";
				querySub += " from movimentocaixadia  mov ";
				querySub += " inner join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ";
				querySub += " left outer join tipoconta tipoconta on tipoconta.idTipoConta = tipomovimento.idtipoconta ";
				querySub += " left outer join tipocontacontager tipocontacontager on tipocontacontager.idTipoConta = tipoconta.idTipoConta ";
				querySub += " left outer join contagerencial contager on contager.idContaGerencial = tipocontacontager.idContaGerencial ";
				querySub += " left outer join tipomovimentocaixadiagrupoempresa tipogrupoempresa on tipogrupoempresa.idTipoMovimentoCaixaDia = tipoMovimento.idTipoMovimentoCaixaDia ";
				querySub += " left outer join grupoempresa ge on ge.idGrupoEmpressa = tipogrupoempresa.idGrupoEmpresa ";
				querySub += " where "; 
				querySub += " mov.idcaixadia=" + idCaixaDia +" and mov.diversos = 'S' and (mov.mostrarBoletim <> 'N' or mov.mostrarBoletim is null)";
				if(DsvConstante.is("JORROVI"))
				{
					if(nomeGrupo.equals("JORROVI"))
						querySub += " and (ge.descricao = '"+nomeGrupo+"' or tipogrupoempresa.idGrupoEmpresa is null)";
					if(nomeGrupo.equals("SERALLE"))
						querySub += " and (ge.descricao = '"+nomeGrupo+"')";
				}
				if(DsvConstante.is("SERALLE"))
				{
					if(nomeGrupo.equals("JORROVI"))
						querySub += " and ge.descricao = '"+nomeGrupo+"' ";
					if(nomeGrupo.equals("SERALLE"))
						querySub += " and (ge.descricao = '"+nomeGrupo+"' or tipogrupoempresa.idGrupoEmpresa is null)";
				}
				
				querySub += " order by contager.descricao ";
	
				  
				rec = conn.createStatement();    
	
				Statement recsub = conn.createStatement(); 
				ResultSet rsSub = recsub.executeQuery(querySub);
				
				rs = rec.executeQuery(query);
		
				
				jrRS = new JRResultSetDataSource(rs);
				jrRSSub = new JRResultSetDataSource(rsSub) ;

			} 
			else
			{ 
				caminho = "relatorioBoletimSeparado.jasper"; 
				query = "";
				querySub = "";
			}
			
			

			caminho = "relatorioBoletimSeparado.jasper"; 
			URL urlArquivo = getClass().getResource("/relatorios/"+caminho);
			String rel = urlArquivo.getPath();
			URL urlSub= getClass().getResource("/relatorios/");
				
			Map<String, Object> params = new HashMap<String, Object>();    

			String situacaoCaixa = "";
			if(caixaDia.getSituacao()==0)
				situacaoCaixa = "FECHADO";
			else if(caixaDia.getSituacao()==1)
				situacaoCaixa = "ABERTO";
			else if(caixaDia.getSituacao()==2)
				situacaoCaixa = "ACEITO";
			else if(caixaDia.getSituacao()==3)
				situacaoCaixa = "ACEITO COM DIVERGENCIAS";


			params.put("SUBREPORT_DIR", urlSub.getPath());
			params.put("REPORT_DATA_SOURCE_SUB", jrRSSub);
			params.put("idCaixaDia", idCaixaDia);
			params.put("dataCaixaFormatada",request.getParameter("dataCaixa"));
			EmpresaFisica empresaFisica = new EmpresaFisicaService().recuperarPorId(idEmpresaFisica);
			params.put("nomeEmpresa", empresaFisica.getId() +"-"+ empresaFisica.getRazaoSocial());
			params.put("idEmpresaFisica", ""+idEmpresaFisica);
			params.put("dataInicial", formatadorDataSql.format(dataFormataInicial));
			params.put("dataFinal", formatadorDataSql.format(dataFormataInicial));
			params.put("situacaoCaixa",situacaoCaixa);
			params.put("nomeGrupo", nomeGrupo.equals("JORROVI")?"JORROVI NOROESTE":"JORROVI NORTE");

			JasperPrint impressao = JasperFillManager.fillReport(rel, params,jrRS);            
			bytes = JasperExportManager.exportReportToPdf(impressao);  

		}
		catch (JRException | SQLException e) 
		{
			throw new ServletException(e);  
		}  

		if (bytes != null && bytes.length > 0) 
		{
			response.setContentType("application/pdf");  
			response.setContentLength(bytes.length);  
			ServletOutputStream ouputStream = response.getOutputStream();  
			ouputStream.write(bytes, 0, bytes.length);  
			ouputStream.flush();  
			ouputStream.close();  
		} 
		
		if(conn != null && !conn.isClosed())
			conn.close();
	}
} 