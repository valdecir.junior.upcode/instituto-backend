package br.com.desenv.nepalign.relatorios;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;

import org.apache.log4j.Logger;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.LancamentoContaPagarRateio;
import br.com.desenv.nepalign.service.LancamentoContaPagarService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

import com.kscbrasil.lib.database.KscConnection;
import com.kscbrasil.lib.type.KscInteger;

public class RelatorioContasPagas
{ 
	private static final Logger logger = Logger.getLogger("REPORT.FINANCEIRO");
	
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	
	private static final LancamentoContaPagarService lancamentoContaPagarService = new LancamentoContaPagarService();
	
	private boolean isUsuarioAutorizado(String idUsuario)
	{
		if(idUsuario == null || idUsuario.equals(""))
			return false;
		
		try 
		{
			for (final String usuario : DsvConstante.getParametrosSistema().get("USUARIO_AUTORIZADO_RELATORIO_CONTASPAGAR").split(","))
				if(usuario.equals(idUsuario))
					return true;
		}
		catch (Exception e) 
		{
			Logger.getRootLogger().error("Não foi possível verificar se o usuário está autorizado a visualizar informações adicionais do Contas a Pagar", e);
			return false;
		}
		
		return false;  
	}
	
	public RelatorioContasPagas(HttpServletRequest request, HttpServletResponse response) throws ServletException, Exception
	{
		this.request = request;
		this.response = response;
	}
	
	public void gerarRelatorio() throws Exception
	{
		if (request.getParameter("iEmpresaOrigem")!=null && request.getParameter("iEmpresaOrigem").equals("2"))
			executarReportContasPagasRateio();
		else
			executarReportContasPagas();
	}
	
	private void executarReportContasPagas() throws Exception
	{
		byte[] reportBytes = null;
		Connection con = null;
		
		try 
		{
			HashMap<String, Object> parametros = new HashMap<String, Object>();
			
			Date dataInicial = null;
			Date dataFinal = null;
			Date dataBaixaInicial = null;
			Date dataBaixaFinal = null;

			int tipoRel = 1;
			
			try
			{
				tipoRel = new KscInteger(request.getParameter("iTipoRelatorio")).intValue();
			}
			catch(Exception ex)
			{
				logger.warn("Não foi possível recuperar o Tipo do Relatório", ex);
				throw new Exception("Escolha um Tipo do Relatório");
			}
			
			if (request.getParameter("iDataInicial") != null && !request.getParameter("iDataInicial").equals("")) 
				dataInicial = IgnUtil.dateFormat.parse(request.getParameter("iDataInicial"));
			else
				throw new Exception("Informe uma Data Inicial");

			if (request.getParameter("iDataFinal") != null && !request.getParameter("iDataFinal").equals("")) 
				dataFinal = IgnUtil.dateFormat.parse(request.getParameter("iDataFinal"));
			else
				throw new Exception("Informe uma Data Final");

			if (request.getParameter("iDataBaixaInicial") != null && !request.getParameter("iDataBaixaInicial").equals("")) 
			{
				dataBaixaInicial = IgnUtil.dateFormat.parse(request.getParameter("iDataBaixaInicial"));
				parametros.put("iDataBaixaInicial", dataBaixaInicial);
			}

			if (request.getParameter("iDataBaixaFinal") != null && !request.getParameter("iDataBaixaFinal").equals("")) 
			{
				dataBaixaFinal = IgnUtil.dateFormat.parse(request.getParameter("iDataBaixaFinal"));
				parametros.put("iDataBaixaFinal", dataBaixaFinal);
			}

			String where = null, file = null;
			
			where = " where l.dataQuitacao is not null AND blcp.dataPagamento between '" 
					+ IgnUtil.dateFormatSql.format(dataInicial)
					+ " 00:00:00 ' AND '"
					+ IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59'";

			if (!request.getParameter("iIdFornecedor").equals(""))
				where = where.concat(" and l.idFornecedor like '" + request.getParameter("iIdFornecedor") + "'");
			if (!request.getParameter("iIdCentroFinanceiro").equals("")) 
				where = where.concat(" and bl.idCentroFinanceiro like '" + request.getParameter("iIdCentroFinanceiro") + "'");
			if (!request.getParameter("iIdContaGerencial").equals("")) 
				where = where.concat(" and l.idContaGerencial in (" + request.getParameter("iIdContaGerencial") + ") ");
			if (!request.getParameter("iIdContaCorrente").equals(""))
				where = where.concat(" and blcp.idContaCorrente like '" + request.getParameter("iIdContaCorrente") + "'");
			if (!request.getParameter("iIdEmpresa").equals("")) 
				where = where.concat(" and l.idEmpresa in (" + request.getParameter("iIdEmpresa") + ") ");
			if (request.getParameter("iIdGrupoFornecedor") != null && !request.getParameter("iIdGrupoFornecedor").equals("")) 
				where = where.concat(" and grupoFornecedor.idGrupoFornecedor in (" + request.getParameter("iIdGrupoFornecedor") + ") ");
			if (request.getParameter("iIdFormaPagamento") != null && !request.getParameter("iIdFormaPagamento").equals("")) 
				where = where.concat(" and blcp.idFormaPagamento like '" + request.getParameter("iIdFormaPagamento") + "'");
			if (request.getParameter("iDespesaOrigemLoja") != null && request.getParameter("iDespesaOrigemLoja").equals("S")) 
				where = where.concat(" and preLancamento.idPreLancamentoContaPagar is not null ");
			if (dataBaixaInicial != null && dataBaixaFinal != null) 
				where = where.concat(" and blcp.dataLancamento BETWEEN " + IgnUtil.dateFormatSql.format(dataBaixaInicial) + " and " + IgnUtil.dateFormatSql.format(dataBaixaFinal) + " ");
			if(!isUsuarioAutorizado(request.getParameter("iIdUsuario")))
				where = where.concat(" and cg.contaContabil not in (" + DsvConstante.getParametrosSistema().get("CONTAGERENCIAL_OMITIDA_RELATORIO") + ") ");

			String sql = "";
			sql = sql + " SELECT distinct cc.idContaCorrente as contaCorrente, l.observacao as historicoPadrao, l.favorecido as favorecido, l.idLancamentoContaPagar,  ";
			sql = sql + " l.documentoPagamento, fp.descricao as formaPagamento,ifnull(blcp.valorMulta,0) as valorJuros, ifnull(blcp.valorDesconto,0) as valorDesconto, ";
			sql = sql + " concat(cg.contaContabil,'-',cg.descricao) as contaGerencial, sum(ROUND(l.valorOriginal, 2)) as valorOriginal, situacao.descricao as situacao, ";
			sql = sql + " IF(blcp.valorMulta<>0,concat('+',blcp.valorMulta),if(blcp.valorDesconto<>0,concat('-',blcp.valorDesconto),0.00)) as jurosDesconto, ";
			sql = sql + " CONCAT(IF(LENGTH(EmpresaFisica.idEmpresaFisica) = 1, CONCAT(0,EmpresaFisica.idEmpresaFisica), EmpresaFisica.idEmpresaFisica), '-', EmpresaFisica.razaoSocial) as empresa,  ";
			sql = sql + " l.observacao as obsLanc,l.idFornecedor, l.dataVencimento,(sum(l.valorOriginal)-sum(l.valorPago)) as saldoAPagar,   ";
			sql = sql + " blcp.dataPagamento as dataQuitacao, sum(blcp.valor) as valorPago,  ((sum(blcp.valor) + sum(IFNULL(blcp.valorDesconto, 0)) - SUM(ifnull(blcp.valorMulta,0)))) as valorTotal,  ";
			sql = sql + " l.numeroDocumento,if(fo.nomeFantasia = null,l.observacao, CONCAT(fo.idFornecedor, '-', fo.nomeFantasia)) as nomeFantasia, tc.descricao as descTipoConta, blcp.dataLancamento ";
			sql = sql + " FROM ((((((((((LancamentoContaPagar l )  ";
			sql = sql + " left join baixaLancamentoContaPagar blcp on blcp.idLancamentoContaPagar = l.idLancamentoContaPagar)  ";
			sql = sql + " inner join contaGerencial cg on cg.idContaGerencial = l.idContaGerencial) ";
			sql = sql + " inner join situacaolancamento situacao on situacao.idSituacaoLancamento = l.idSituacaoLancamento  ";
			sql = sql + " left outer join PreLancamentoContaPagar preLancamento on preLancamento.idLancamentoContaPagar = l.idLancamentoContaPagar)  ";
			sql = sql + " left outer join Fornecedor fo on  fo.idFornecedor = l.idFornecedor)  ";
			sql = sql + " left outer join GrupoFornecedor grupoFornecedor on  fo.idGrupoFornecedor = grupoFornecedor.idGrupoFornecedor  ";
			sql = sql + " left outer join ContaCorrente cc on  cc.idContaCorrente = blcp.idContaCorrente)  ";
			sql = sql + " left outer join Banco banc on cc.idBanco = banc.idBanco)  ";
			sql = sql + " left outer join TipoConta tc on  tc.idTipoConta = l.idTipoConta and tc.credito=0)  ";
			sql = sql + " left outer join FormaPagamento fp on  fp.idFormaPagamento = blcp.idFormaPagamento)  ";
			sql = sql + " left outer join EmpresaFisica on EmpresaFisica.idEmpresaFisica = l.idEmpresa)   ";
			
			sql = sql.concat(where);

			if (tipoRel==1)
			{
				sql = sql.concat(" group by l.idLancamentoContaPagar ");
				sql = sql.concat(" order by l.idEmpresa, l.dataQuitacao,(0 + replace(l.numeroDocumento,'/',''))");
				file = "contasPagas.jasper";
			}
			else if(tipoRel==2)
			{
				sql = sql.concat(" group by l.idLancamentoContaPagar ");
				sql = sql.concat(" order by l.idEmpresa, fo.nomeFantasia, l.dataQuitacao,(0 + replace(l.numeroDocumento,'/',''))");
				file = "contasPagasPorFornecedor.jasper";				
			}
			else if (tipoRel==3)
			{
				if(request.getParameter("iPorData")!=null && request.getParameter("iPorData").equals("S"))
				{
					sql = sql.concat(" group by l.idLancamentoContaPagar ");
					sql = sql.concat(" order by l.idEmpresa,  blcp.dataPagamento,(0 + replace(l.numeroDocumento,'/',''))");
					file = "contasPagasPorTipoContaData.jasper";	
				}
				else
				{
					sql = sql.concat(" group by l.idLancamentoContaPagar ");
					sql = sql.concat(" order by l.idEmpresa, cg.contaContabil, l.dataQuitacao, fo.nomeFantasia, (0 + replace(l.numeroDocumento,'/',''))");
					file = "contasPagasPorTipoConta.jasper";	
				}	
			}			
			else
			{
				sql = sql.concat(" group by EmpresaFisica.idEmpresaFisica, tc.idTipoConta ");
				sql = sql.concat(" order by l.idEmpresa, cg.contaContabil, l.dataQuitacao, fo.nomeFantasia, (0 + replace(l.numeroDocumento,'/',''))");
				file = "contasPagasPorTipoContaResumido.jasper";				
			}
			
			con = ConexaoUtil.getConexaoPadrao();
			
			reportBytes = JasperExportManager.exportReportToPdf(JasperFillManager.fillReport(getClass().getResource("/relatorios/".concat(file)).getPath(), 
					parametros, 
					new JRResultSetDataSource(con.createStatement().executeQuery(String.valueOf(sql)))));
		} 
		catch (net.sf.jasperreports.engine.JRException ke) 
		{
			logger.error("Não foi possível compilar o arquivo do relatório.", ke);
			throw new Exception("Não foi possível gerar o relatório. Código de erro #001");
		} 
		catch (Exception ke) 
		{
			logger.error("Não foi possível gerar o relatório.", ke);
			throw new Exception("Não foi possível gerar o relatório. Código de erro #002");
		}
		finally
		{
			if(con != null && !con.isClosed())
				con.close();
			
			if (reportBytes != null && reportBytes.length > 0) 
			{
				response.setContentType("application/pdf");  
				response.setContentLength(reportBytes.length);  
				response.getOutputStream().write(reportBytes, 0, reportBytes.length);  
				response.getOutputStream().flush();  
				response.getOutputStream().close();  
			}
		}
	}
	
	private void executarReportContasPagasRateio() throws Exception
	{
		byte[] reportBytes = null;
		Connection con = null;
		EntityManager manager = null;
		
		try 
		{
			HashMap<String, Object> parametros = new HashMap<String, Object>();
			
			Date dataInicial = null;
			Date dataFinal = null;
			Date dataBaixaInicial = null;
			Date dataBaixaFinal = null;
			
			int tipoRel = 1;
			
			try
			{
				tipoRel = new KscInteger(request.getParameter("iTipoRelatorio")).intValue();
			}
			catch(Exception ex)
			{
				logger.warn("Não foi possível recuperar o Tipo do Relatório", ex);
				throw new Exception("Escolha um Tipo do Relatório");
			}

			if (request.getParameter("iDataInicial") != null && !request.getParameter("iDataInicial").equals("")) 
				dataInicial = IgnUtil.dateFormat.parse(request.getParameter("iDataInicial"));
			else
				throw new Exception("Informe uma Data Inicial");

			if (request.getParameter("iDataFinal") != null && !request.getParameter("iDataFinal").equals("")) 
				dataFinal = IgnUtil.dateFormat.parse(request.getParameter("iDataFinal"));
			else
				throw new Exception("Informe uma Data Final");

			if (request.getParameter("iDataBaixaInicial") != null && !request.getParameter("iDataBaixaInicial").equals("")) 
			{
				dataBaixaInicial = IgnUtil.dateFormat.parse(request.getParameter("iDataBaixaInicial"));
				parametros.put("iDataBaixaInicial", dataBaixaInicial);
			}

			if (request.getParameter("iDataBaixaFinal") != null && !request.getParameter("iDataBaixaFinal").equals("")) 
			{
				dataBaixaFinal = IgnUtil.dateFormat.parse(request.getParameter("iDataBaixaFinal"));
				parametros.put("iDataBaixaFinal", dataBaixaFinal);
			}

			String where = null, file = null;

			where = " where l.dataQuitacao is not null AND blcp.dataPagamento between '" 
					+ IgnUtil.dateFormatSql.format(dataInicial)
					+ " 00:00:00 ' AND '"
					+ IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59'";

			if (!request.getParameter("iIdFornecedor").equals(""))
				where = where.concat(" and l.idFornecedor like '" + request.getParameter("iIdFornecedor") + "'");
			if (!request.getParameter("iIdCentroFinanceiro").equals("")) 
				where = where.concat(" and bl.idCentroFinanceiro like '" + request.getParameter("iIdCentroFinanceiro") + "'");
			if (request.getParameter("iIdTipoConta")!=null && !request.getParameter("iIdTipoConta").equals("")) 
				where = where.concat(" and l.idTipoConta like '" + request.getParameter("iIdTipoConta") + "'");
			if (!request.getParameter("iIdContaCorrente").equals(""))
				where = where.concat(" and blcp.idContaCorrente like '" + request.getParameter("iIdContaCorrente") + "'");
			if (!request.getParameter("iIdContaGerencial").equals(""))
				where = where.concat(" and l.idContaGerencial in (" + request.getParameter("iIdContaGerencial") + ") ");
			if (!request.getParameter("iIdGrupoFornecedor").equals("")) 
				where = where.concat(" and grupoFornecedor.idGrupoFornecedor in (" + request.getParameter("iIdGrupoFornecedor") + ") ");
			if (request.getParameter("iIdFormaPagamento")!= null && !request.getParameter("iIdFormaPagamento").equals("")) 
				where = where.concat(" and blcp.idFormaPagamento like '" + request.getParameter("iIdFormaPagamento") + "'");
			if (dataBaixaInicial != null && dataBaixaFinal != null) 
				where = where.concat(" and blcp.dataLancamento BETWEEN " + IgnUtil.dateFormatSql.format(dataBaixaInicial) + " and " + IgnUtil.dateFormatSql.format(dataBaixaFinal) + " ");
			if(!isUsuarioAutorizado(request.getParameter("iIdUsuario")))
				where = where.concat(" and cg.contaContabil not in (" + DsvConstante.getParametrosSistema().get("CONTAGERENCIAL_OMITIDA_RELATORIO") + ") ");
			
			con = KscConnection.getInstance();
			manager = ConexaoUtil.getEntityManager();
			
			fillTemporaryTable(manager, con, where);
			
			String sql = "";
			sql = sql + " SELECT distinct cc.idContaCorrente as contaCorrente, l.observacao as historicoPadrao, l.favorecido as favorecido, l.idLancamentoContaPagar,  ";
			sql = sql + " l.documentoPagamento, fp.descricao as formaPagamento,sum(ifnull(l.valorMulta,0)) as valorJuros, " +
					"sum(ifnull(l.valorDesconto,0)) as valorDesconto, ";
			sql = sql + " concat(cg.contaContabil,'-',cg.descricao) as contaGerencial, l.valorOriginal as valorOriginal, situacao.descricao as situacao, ";
			sql = sql + " sum(IF(l.valorMulta<>0,concat('+',l.valorMulta),if(l.valorDesconto<>0,concat('-', l.valorDesconto),0.00))) as jurosDesconto, ";
			sql = sql + " CONCAT(IF(LENGTH(EmpresaFisica.idEmpresaFisica) = 1, CONCAT(0,EmpresaFisica.idEmpresaFisica), EmpresaFisica.idEmpresaFisica), '-', EmpresaFisica.razaoSocial) as empresa,  ";
			sql = sql + " l.observacao as obsLanc,l.idFornecedor, l.dataLancamento, l.dataVencimento,(l.valorOriginal-l.valorPago) as saldoAPagar,   ";
			sql = sql + " blcp.dataPagamento as dataQuitacao, l.valorPago as valorPago,  ((l.valorPago + IFNULL(l.valorDesconto, 0) - ifnull(l.valorMulta,0))) as valorTotal,  ";
			sql = sql + " l.numeroDocumento,if(fo.nomeFantasia = null, l.observacao, CONCAT(fo.idFornecedor, '-', fo.nomeFantasia)) as nomeFantasia, tc.descricao as descTipoConta  ";
			sql = sql + " FROM (((((((((TmpLancamentoContaPagar l )  ";
			sql = sql + " left join baixaLancamentoContaPagar blcp on blcp.idLancamentoContaPagar = l.idLancamentoContaPagar)  ";
			sql = sql + " inner join contaGerencial cg on cg.idContaGerencial = l.idContaGerencial)  ";
			sql = sql + " inner join situacaolancamento situacao on situacao.idSituacaoLancamento = l.idSituacaoLancamento  ";
			sql = sql + " left outer join Fornecedor fo on  fo.idFornecedor = l.idFornecedor)  ";
			sql = sql + " left outer join GrupoFornecedor grupoFornecedor on  fo.idGrupoFornecedor = grupoFornecedor.idGrupoFornecedor  ";
			sql = sql + " left outer join ContaCorrente cc on  cc.idContaCorrente = blcp.idContaCorrente)  ";
			sql = sql + " left outer join Banco banc on cc.idBanco = banc.idBanco)  ";
			sql = sql + " left outer join TipoConta tc on  tc.idTipoConta = l.idTipoConta and tc.credito=0)  ";
			sql = sql + " left outer join FormaPagamento fp on  fp.idFormaPagamento = blcp.idFormaPagamento)  ";
			sql = sql + " left outer join EmpresaFisica on EmpresaFisica.idEmpresaFisica = l.idEmpresa)   ";
			
			if (!request.getParameter("iIdEmpresa").equals("")) 
				where = where.concat(" and l.idEmpresa in (" + request.getParameter("iIdEmpresa") + ") ");

			sql = sql.concat(where);

			if (tipoRel==1)
			{
				sql = sql.concat(" group by l.idTmpLancamentoContaPagar, l.idEmpresa ");
				sql = sql.concat(" order by l.idEmpresa, l.dataQuitacao,(0 + replace(l.numeroDocumento,'/',''))");
				file = "contasPagas.jasper";
			}
			else if(tipoRel==2)
			{
				sql = sql.concat(" group by l.idTmpLancamentoContaPagar, l.idEmpresa ");
				sql = sql.concat(" order by l.idEmpresa, fo.nomeFantasia, l.dataQuitacao,(0 + replace(l.numeroDocumento,'/',''))");
				file = "contasPagasPorFornecedor.jasper";				
			}
			else if (tipoRel==3)
			{
				if(request.getParameter("iPorData")!=null && request.getParameter("iPorData").equals("S"))
				{
					sql = sql.concat(" group by l.idTmpLancamentoContaPagar, l.idEmpresa ");
					sql = sql.concat(" order by l.idEmpresa,blcp.dataPagamento,(0 + replace(l.numeroDocumento,'/',''))");
					file = "contasPagasPorTipoContaData.jasper";	
				}
				else
				{
				sql = sql.concat(" group by l.idTmpLancamentoContaPagar, l.idEmpresa ");
				sql = sql.concat(" order by l.idEmpresa, cg.contaContabil, l.dataQuitacao, fo.nomeFantasia, (0 + replace(l.numeroDocumento,'/',''))");
				file = "contasPagasPorTipoConta.jasper";			
				}
			}			
			else
			{
				sql = sql.concat(" group by EmpresaFisica.idEmpresaFisica, tc.idTipoConta ");
				sql = sql.concat(" order by l.idEmpresa, cg.contaContabil, l.dataQuitacao, fo.nomeFantasia, (0 + replace(l.numeroDocumento,'/',''))");
				file = "contasPagasPorTipoContaResumido.jasper";				
			}
			
			parametros.put("iDataFinal", dataFinal);
			parametros.put("iDataInicial", dataInicial);
			parametros.put("iDescricaoRateio", " - COM RATEIO");
 
			reportBytes = JasperExportManager.exportReportToPdf(JasperFillManager.fillReport(getClass().getResource("/relatorios/".concat(file)).getPath(), 
					parametros, 
					new JRResultSetDataSource(con.createStatement().executeQuery(String.valueOf(sql)))));
		}
		catch (net.sf.jasperreports.engine.JRException ke) 
		{
			logger.error("Não foi possível compilar o arquivo do relatório.", ke);
			throw new Exception("Não foi possível gerar o relatório. Código de erro #001");
		} 
		catch (Exception ke) 
		{
			logger.error("Não foi possível gerar o relatório.", ke);
			throw new Exception("Não foi possível gerar o relatório. Código de erro #002");
		}
		finally
		{
			if(con != null && !con.isClosed())
				con.close();
			if(manager != null && manager.isOpen())
				 manager.close();
			
			if (reportBytes != null && reportBytes.length > 0) 
			{
				response.setContentType("application/pdf");  
				response.setContentLength(reportBytes.length);  
				response.getOutputStream().write(reportBytes, 0, reportBytes.length);  
				response.getOutputStream().flush();  
				response.getOutputStream().close();  
			}
		}
	}	

	private void fillTemporaryTable(EntityManager manager, Connection con, String where) throws Exception
	{
		String baseSQL = "SELECT ";
		baseSQL = baseSQL + "l.`idLancamentoContaPagar`,l.`idEmpresa`, l.`idFornecedor`, ";
		baseSQL = baseSQL + "l.`idCentroFinanceiro`, l.`idContaGerencial`,l.`idTipoConta`, ";
		baseSQL = baseSQL + "l.`idContaCorrente`, l.`dataLancamento`, l.`dataVencimento`,	l.`dataQuitacao`, ";
		baseSQL = baseSQL + "l.`valorOriginal`,	l.`valorDesconto`,	l.`valorMulta`,	l.`percentualMulta`, ";
		baseSQL = baseSQL + "l.`valorPago`, l.`documentoPagamento`, l.`valorJuroDiario`,	l.`percentualJuroDiario`, ";
		baseSQL = baseSQL + "l.`favorecido`, l.`numeroDocumento`,	l.`observacao`, l.`dataLimiteDesconto`, ";
		baseSQL = baseSQL + "l.`situacaoEmissaoCheque`, l.`idFormaPagamento`, l.`idLancamentoContaPagarFixo`, ";
		baseSQL = baseSQL + "l.`idSituacaoLancamento`, l.`codigoBarra`, l.`codigoBarraConvertido`, l.`nossoNumero`, ";
		baseSQL = baseSQL + "l.`prazoPagamento`,l.`idLancamentoAgrupador`,l.`planoContaAntigo`,l.`codigoSeguranca`, ";
		baseSQL = baseSQL + "l.`idPagamentoDocumentoEntradaSaida`, l.empresasRateio, sum(blcp.valor) as 'blcp.valor', sum(blcp.valorDesconto) as 'blcp.valorDesconto', sum(blcp.valorMulta) as 'blcp.valorMulta' ";
		baseSQL = baseSQL + " FROM (((((((((LancamentoContaPagar l )  ";
		baseSQL = baseSQL + " left join baixaLancamentoContaPagar blcp on blcp.idLancamentoContaPagar = l.idLancamentoContaPagar)  ";
		baseSQL = baseSQL + " inner join contaGerencial cg on cg.idContaGerencial = l.idContaGerencial) ";
		baseSQL = baseSQL + " inner join situacaolancamento situacao on situacao.idSituacaoLancamento = l.idSituacaoLancamento  ";
		baseSQL = baseSQL + " left outer join Fornecedor fo on  fo.idFornecedor = l.idFornecedor)  ";
		baseSQL = baseSQL + " left outer join GrupoFornecedor grupoFornecedor on  fo.idGrupoFornecedor = grupoFornecedor.idGrupoFornecedor  ";
		baseSQL = baseSQL + " left outer join ContaCorrente cc on  cc.idContaCorrente = blcp.idContaCorrente)  ";
		baseSQL = baseSQL + " left outer join Banco banc on cc.idBanco = banc.idBanco)  ";
		baseSQL = baseSQL + " left outer join TipoConta tc on  tc.idTipoConta = l.idTipoConta and tc.credito=0)  ";
		baseSQL = baseSQL + " left outer join FormaPagamento fp on  fp.idFormaPagamento = blcp.idFormaPagamento)  ";
		baseSQL = baseSQL + " left outer join EmpresaFisica on EmpresaFisica.idEmpresaFisica = l.idEmpresa)   ";
		
		ResultSet rs = con.createStatement().executeQuery(baseSQL.concat(where).concat("group by l.`idLancamentoContaPagar`"));
		
		String sqlTemp = "";
		Statement stmTemp = con.createStatement();
		
		try
		{
		
			sqlTemp = "CREATE TEMPORARY TABLE `nepalign`.`tmplancamentocontapagar` ( " +
					  "`idTmpLancamentoContaPagar` int(11) NOT NULL AUTO_INCREMENT, " +
					  "`idLancamentoContaPagar` int(11) NOT NULL, " +
					  "`idEmpresa` int(11) NOT NULL , " +
					  "`idFornecedor` int(11) NOT NULL , " +
					  "`idCentroFinanceiro` int(11) DEFAULT NULL , " +
					  "`idContaGerencial` int(11) DEFAULT NULL , " +
					  "`idTipoConta` int(11) NOT NULL , " +
					  "`idContaCorrente` int(11) DEFAULT NULL , " +
					  "`dataLancamento` date NOT NULL , " +
					  "`dataVencimento` date NOT NULL , " +
					  "`dataQuitacao` date DEFAULT NULL , " +
					  "`valorOriginal` double NOT NULL DEFAULT '0' , " +
					  "`valorDesconto` double DEFAULT NULL  , " +
					  "`valorMulta` double DEFAULT NULL , " +
					  "`percentualMulta` double DEFAULT NULL , " +
					  "`valorPago` double DEFAULT NULL , " +
					  "`documentoPagamento` varchar(150) DEFAULT NULL , " +
					  "`valorJuroDiario` double unsigned zerofill DEFAULT NULL , " +
					  "`percentualJuroDiario` double DEFAULT NULL , " +
					  "`favorecido` varchar(50) DEFAULT NULL , " +
					  "`numeroDocumento` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL , " +
					  "`observacao` VARCHAR(1000) , " +
					  "`dataLimiteDesconto` date DEFAULT NULL , " +
					  "`situacaoEmissaoCheque` int(1) unsigned DEFAULT NULL , " +
					  "`idFormaPagamento` int(11) DEFAULT NULL , " +
					  "`idLancamentoContaPagarFixo` int(11) DEFAULT NULL , " +
					  "`idSituacaoLancamento` int(11) DEFAULT NULL , " +
					  "`codigoBarra` varchar(100) DEFAULT NULL , " +
					  "`codigoBarraConvertido` varchar(44) DEFAULT NULL , " +
					  "`nossoNumero` varchar(250) DEFAULT NULL , " +
					  "`prazoPagamento` varchar(20) DEFAULT NULL , " +
					  "`idLancamentoAgrupador` int(11) DEFAULT NULL , " +
					  "`planoContaAntigo` varchar(45) DEFAULT NULL, " +
					  "`codigoSeguranca` varchar(255) DEFAULT NULL, " +
					  "`idPagamentoDocumentoEntradaSaida` int(11) DEFAULT NULL, " +
					  "`empresasRateio` varchar(255) DEFAULT NULL, " +
					  "`proporcao` double(8,4), " + 
					  "PRIMARY KEY (`idTmpLancamentoContaPagar`)) " +
						" ENGINE=MEMORY DEFAULT CHARSET=utf8;";
			
			stmTemp.execute(sqlTemp);
			
			while (rs.next())
			{
				List<LancamentoContaPagarRateio> rateios = lancamentoContaPagarService.recuperarRateios(manager, rs.getInt("idLancamentoContaPagar"));
				
				BigDecimal valorTotalLancamento = new BigDecimal(0.0);
				BigDecimal valorTotalPago = new BigDecimal(0.0);
				BigDecimal valorTotalMulta = new BigDecimal(0.0);
				BigDecimal valorTotalDesconto = new BigDecimal(0.0);
		
				if(rateios == null || rateios.size() == 0x00)
					stmTemp.execute(makeInsert(rs, new BigDecimal(1.0), rs.getInt("idEmpresa"),
							new BigDecimal(rs.getDouble("valorOriginal")),
							new BigDecimal(rs.getDouble("blcp.valorDesconto")),
							new BigDecimal(rs.getDouble("blcp.valorMulta")),
							new BigDecimal(rs.getDouble("blcp.valor"))));
				else
				{
					boolean empresaOrigemNoRateio = false;
					
					for (LancamentoContaPagarRateio rateio : rateios)
						if(rateio.getEmpresaFisica().getId().equals(rs.getInt("idEmpresa")))
							empresaOrigemNoRateio = true;
					
					if(!empresaOrigemNoRateio)
					{
						
					}
					
					for (LancamentoContaPagarRateio rateio : rateios)
					{
						BigDecimal proporcao = rateio.getPorcentagemRateio().divide(new BigDecimal(100.0));
						valorTotalLancamento = valorTotalLancamento.add((rs.getObject("valorOriginal") == null ? new BigDecimal(0.0) : new BigDecimal(rs.getDouble("valorOriginal")).multiply(proporcao))).setScale(0x02, RoundingMode.DOWN);
						valorTotalPago = valorTotalPago.add((rs.getObject("blcp.valor") == null ? new BigDecimal(0.0) : new BigDecimal(rs.getDouble("blcp.valor")).multiply(proporcao))).setScale(0x02, RoundingMode.DOWN);
						valorTotalDesconto = valorTotalDesconto.add((rs.getObject("blcp.valorDesconto") == null ? new BigDecimal(0.0) : new BigDecimal(rs.getDouble("blcp.valorDesconto")).multiply(proporcao))).setScale(0x02, RoundingMode.DOWN);
						valorTotalMulta = valorTotalMulta.add((rs.getObject("blcp.valorMulta") == null ? new BigDecimal(0.0) : new BigDecimal(rs.getDouble("blcp.valorMulta")).multiply(proporcao))).setScale(0x02, RoundingMode.DOWN);
						

					}
					

					
					BigDecimal restoValorOriginal = new BigDecimal(rs.getDouble("valorOriginal")).subtract(valorTotalLancamento);
					BigDecimal restoValorPago = new BigDecimal(rs.getDouble("blcp.valor")).subtract(valorTotalPago);
					BigDecimal restoValorMulta = new BigDecimal(rs.getDouble("blcp.valorMulta")).subtract(valorTotalMulta);
					BigDecimal restoValorDesconto = new BigDecimal(rs.getDouble("blcp.valorDesconto")).subtract(valorTotalDesconto);
					
					for (LancamentoContaPagarRateio rateio : rateios)
					{
						stmTemp.execute(makeInsert(rs, rateio.getPorcentagemRateio().divide(new BigDecimal(100.0)), rateio.getEmpresaFisica().getId(),
								new BigDecimal(rs.getDouble("valorOriginal")).multiply(rateio.getPorcentagemRateio().divide(new BigDecimal(100.0))).add(restoValorOriginal).setScale(0x02, RoundingMode.DOWN),
								new BigDecimal(rs.getDouble("blcp.valorDesconto")).multiply(rateio.getPorcentagemRateio().divide(new BigDecimal(100.0))).add(restoValorDesconto).setScale(0x02, RoundingMode.DOWN),
								new BigDecimal(rs.getDouble("blcp.valorMulta")).multiply(rateio.getPorcentagemRateio().divide(new BigDecimal(100.0))).add(restoValorMulta).setScale(0x02, RoundingMode.DOWN),
								new BigDecimal(rs.getDouble("blcp.valor")).multiply(rateio.getPorcentagemRateio().divide(new BigDecimal(100.0))).add(restoValorPago).setScale(0x02, RoundingMode.DOWN)));
						
						restoValorOriginal = new BigDecimal(0);
						restoValorPago = new BigDecimal(0);
						restoValorMulta = new BigDecimal(0);
						restoValorDesconto = new BigDecimal(0);
						
					}	
				}					
			}
		}
		catch(Exception ex)
		{
			logger.error("Não foi possível preencher tabela tmplancamentocontapagar", ex);
			throw ex;
		}
	}
	
	private String makeInsert(ResultSet rsBaixaLancamentoContaPagar, BigDecimal proporcao, int idEmpresa, 
			BigDecimal valorOriginal, BigDecimal valorDesconto,
			BigDecimal valorMulta, BigDecimal valorPago) throws Exception
	{
		String sqlInsert;

		try
		{
			sqlInsert = "INSERT INTO `nepalign`.`tmplancamentocontapagar` " + 
			"(`idLancamentoContaPagar`, " +
			"`idEmpresa`, " +
			"`idFornecedor`, " +
			"`idCentroFinanceiro`, " +
			"`idContaGerencial`, " +
			"`idTipoConta`, " +
			"`idContaCorrente`, " +
			"`dataLancamento`, " +
			"`dataVencimento`, " +
			"`dataQuitacao`, " +
			"`valorOriginal`, " +
			"`valorDesconto`, " +
			"`valorMulta`, " +
			"`percentualMulta`, " +
			"`valorPago`, " +
			"`documentoPagamento`, " +
			"`valorJuroDiario`, " +
			"`percentualJuroDiario`, " +
			"`favorecido`, " +
			"`numeroDocumento`, " +
			"`observacao`, " +
			"`dataLimiteDesconto`, " +
			"`situacaoEmissaoCheque`, " +
			"`idFormaPagamento`, " +
			"`idLancamentoContaPagarFixo`, " +
			"`idSituacaoLancamento`, " +
			"`codigoBarra`, " +
			"`codigoBarraConvertido`, " +
			"`nossoNumero`, " +
			"`prazoPagamento`, " +
			"`idLancamentoAgrupador`, " +
			"`planoContaAntigo`, " +
			"`codigoSeguranca`, " +
			"`idPagamentoDocumentoEntradaSaida`, " +
			"`empresasRateio`, " +
			"proporcao) " +
			"VALUES " +
			"( " +
			"" + rsBaixaLancamentoContaPagar.getInt("idLancamentoContaPagar") + ", " +
			"" + idEmpresa + ", " +
			"" + rsBaixaLancamentoContaPagar.getInt("idFornecedor") + ", " +
			"" + rsBaixaLancamentoContaPagar.getInt("idCentroFinanceiro") + ", " +
			"" + rsBaixaLancamentoContaPagar.getInt("idContaGerencial") + ", " +
			"" + rsBaixaLancamentoContaPagar.getInt("idTipoConta") + ", " +
			"" + rsBaixaLancamentoContaPagar.getInt("idContaCorrente") + ", " +
			"'" + rsBaixaLancamentoContaPagar.getDate("dataLancamento") + "', " +
			"'" + rsBaixaLancamentoContaPagar.getDate("dataVencimento") + "', " +
			(rsBaixaLancamentoContaPagar.getObject("dataQuitacao")==null?"null, ":"'" + rsBaixaLancamentoContaPagar.getDate("dataQuitacao") + "', ") +
			"" + valorOriginal + ", " +
			"" + valorDesconto + ", " +
			"" + valorMulta + ", " +
			"" + rsBaixaLancamentoContaPagar.getDouble("percentualMulta") + ", " +
			"" + valorPago + ", " +
			"\"" + rsBaixaLancamentoContaPagar.getString("documentoPagamento") + "\", " +
			"" + rsBaixaLancamentoContaPagar.getDouble("valorJuroDiario") + ", " + 
			"" + rsBaixaLancamentoContaPagar.getDouble("percentualJuroDiario") + ", " +
			(rsBaixaLancamentoContaPagar.getObject("favorecido")==null?"null, ":"\"" + rsBaixaLancamentoContaPagar.getString("favorecido") + "\", ") +
			"\"" + rsBaixaLancamentoContaPagar.getString("numeroDocumento") + "\", " +
			"'" + rsBaixaLancamentoContaPagar.getString("observacao") + "', " +
			//"'" + "" + "', " + sem observação
			(rsBaixaLancamentoContaPagar.getObject("dataLimiteDesconto")==null?"null, ":"'" + rsBaixaLancamentoContaPagar.getDate("dataLimiteDesconto") + "', ") +
			"" + rsBaixaLancamentoContaPagar.getInt("situacaoEmissaoCheque") + ", " +
			"" + rsBaixaLancamentoContaPagar.getInt("idFormaPagamento") + ", " +
			"" + rsBaixaLancamentoContaPagar.getInt("idLancamentoContaPagarFixo") + ", " +
			"" + rsBaixaLancamentoContaPagar.getInt("idSituacaoLancamento") + ", " +
			"\"" + rsBaixaLancamentoContaPagar.getString("codigoBarra") + "\", " +
			"\"" + rsBaixaLancamentoContaPagar.getString("codigoBarraConvertido") + "\", " +
			"\"" + rsBaixaLancamentoContaPagar.getString("nossoNumero") + "\", " +
			"\"" + rsBaixaLancamentoContaPagar.getString("prazoPagamento") + "\", " +
			"" + rsBaixaLancamentoContaPagar.getInt("idLancamentoAgrupador") + ", " +
			"'" + rsBaixaLancamentoContaPagar.getString("planoContaAntigo") + "', " +
			"'" + rsBaixaLancamentoContaPagar.getString("codigoSeguranca") + "', " +
			"" + rsBaixaLancamentoContaPagar.getInt("idPagamentoDocumentoEntradaSaida") + ", " +
			(rsBaixaLancamentoContaPagar.getObject("empresasRateio")==null?"null, ":"'" + rsBaixaLancamentoContaPagar.getString("empresasRateio") + "', ") +
			"" + proporcao + " " +
			"); ";		
			return sqlInsert;
		}
		catch(Exception ex)
		{
			logger.error("Não foi possível gerar script de insersão registro na tmplancamentocontapagar", ex);
			throw ex;
		}
	}	
}