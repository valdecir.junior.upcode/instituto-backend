package br.com.desenv.nepalign.relatorios;

import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;

import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class RelatorioNotificacaoRecebimentos {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
	}

	public  byte[] gerarRelatorioPDF(Date data,String idEmpresa) throws Exception
	{
		Double totalAbertoInicial = 0.0;
		Double totalRecebidos = 0.0;
		Double totalAbertoAtual =0.0;
		Connection con;
		String reportPath = "C:/desenv/Notificacao/relatorios/relatorioNotificacaoRecebimento.jasper";
		String subDir = "C:/desenv/Notificacao/relatorios/";
		if(System.getProperty("os.name").equals("Mac OS X"))
			reportPath = "/users/tarcisio/documents/desenv/ambiente/workspace/nepalign/src/relatorios/relatorioNotificacaoRecebimento.jasper";
		else
			reportPath = "C:/desenv/Notificacao/relatorios/relatorioNotificacaoRecebimento.jasper";
		
		if(System.getProperty("os.name").equals("Mac OS X"))
			subDir = "/users/tarcisio/documents/desenv/ambiente/workspace/nepalign/src/relatorios/";
		else
			subDir = "C:/desenv/Notificacao/relatorios/";
		
		con = ConexaoUtil.getConexaoPadrao();
		ResultSet rs = gerarResultSetRelatorioNotificacaoRecebimentosVencidas(data,"1,2,3,4,5,6,7,8,9,10,13",con);
		while(rs.next())
		{
			totalAbertoAtual += rs.getDouble("valorParcelasAtrasadas");
			totalAbertoInicial += rs.getDouble("valorTotalInicial");
			totalRecebidos  += rs.getDouble("valorParcelasRecebidas");
		}
		rs.beforeFirst();
		ResultSet rsAVencer = gerarResultSetRelatorioNotificacaoRecebimentosAVencer(data,"1,2,3,4,5,6,7,8,9,10,13",con);
		while(rsAVencer.next())
		{
			totalAbertoAtual += rsAVencer.getDouble("valorParcelasAtrasadas");
			totalAbertoInicial += rsAVencer.getDouble("valorTotalInicial");
			totalRecebidos  += rsAVencer.getDouble("valorParcelasRecebidas");
		}
		rsAVencer.beforeFirst();
		System.out.println(""+totalAbertoAtual+" | "+totalRecebidos+" | "+totalAbertoInicial);
		JRResultSetDataSource DsAVencer = new JRResultSetDataSource(rsAVencer);
		Map<String, Object> params = new HashMap<String, Object>();  
		params.put("dataRelatorio", data);
		params.put("totalRecebidos", totalRecebidos);
		params.put("totalAbertoAtual", totalAbertoAtual);
		params.put("totalAbertoInicial", totalAbertoInicial);
		params.put("DsAVencer", DsAVencer);
		params.put("SUBREPORT_DIR", subDir);
		byte[] pdf = JasperExportManager.exportReportToPdf(JasperFillManager.fillReport(reportPath, params, new JRResultSetDataSource(rs)));
		return pdf;
	}
	public static ResultSet gerarResultSetRelatorioNotificacaoRecebimentosVencidas(Date data,String idEmpresa,Connection con) throws Exception
	{
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat dfParam = new SimpleDateFormat("dd/MM/yyyy");
		String dataInicial = df.format(data);
		Date dataSerraleDate = DsvConstante.getParametrosSistema().get("dataSeralle")==null?new Date():dfParam.parse(DsvConstante.getParametrosSistema().get("dataSeralle"));
		String dataSeralle = df.format(dataSerraleDate);

		String sql = "";
		sql = sql + "  SELECT 'credi' as tipo,concat('F',emp.idEmpresaFisica,'-') as idEmpresa,  emp.razaoSocial, ";
		sql = sql + "    IF(DATEDIFF(CURRENT_DATE,dupp.dataVencimento)<=10, 10,   ";
		sql = sql + " 	IF(DATEDIFF(CURRENT_DATE,dupp.dataVencimento)<=20,20,   ";
		sql = sql + " 	 IF(DATEDIFF(CURRENT_DATE,dupp.dataVencimento)<=30,30,  	 ";
		sql = sql + "   IF(DATEDIFF(CURRENT_DATE,dupp.dataVencimento)<=60,60,  	  ";
		sql = sql + "   IF(DATEDIFF(CURRENT_DATE,dupp.dataVencimento)<=90,90,  	  ";
		sql = sql + "    IF(DATEDIFF(CURRENT_DATE,dupp.dataVencimento)<=120,120,30000)))))) AS tempo_atrazo,0.0 as parcial, ";
		sql = sql + "   IFNULL(sum(dupp.valorParcela),0) AS totalValorParcela   ";
		sql = sql + " FROM duplicataparcela dupp  ";
		sql = sql + "  left outer join duplicata dup on dup.idDuplicata = dupp.idDuplicata   ";
		sql = sql + " left outer join empresaFisica emp on emp.idEmpresaFisica = dupp.idEmpresaFisica   ";
		sql = sql + " left outer join ( ";
		sql = sql + " 		select antiga.dataCompra as dataCompraAntiga,ac.idDuplicataNova  ";
		sql = sql + "  		from duplicataacordo ac   ";
		sql = sql + "  		inner join duplicata antiga on antiga.numeroDuplicata = ac.numeroDuplicataAntiga  ";
		sql = sql + "  		group by ac.idDuplicataNova  ";
		sql = sql + "  		order by antiga.dataCompra  ";
		sql = sql + "  ) as acordo on acordo.idDuplicataNova = dupp.idDuplicata   ";
		sql = sql + " where  (dupp.situacao = '0' or dupp.situacao = '2') and dupp.dataVencimento<'"+dataInicial+"'  ";
		sql = sql + "  and dup.idEmpresaFisica in ("+idEmpresa+")  and DATEDIFF('"+dataInicial+"',dupp.dataVencimento) > 0 ";
		sql = sql + " and if(true,dupp.idduplicataparcela is not null, DATEDIFF(CURRENT_DATE,dupp.dataVencimento)<=360 )  ";
		sql = sql + "   and if(dup.datacompra < '"+dataSeralle+"', ";
		sql = sql + " 			true, ";
		sql = sql + " 			if(dupp.acordo= 'S', ";
		sql = sql + " 					acordo.dataCompraAntiga < '"+dataSeralle+"', ";
		sql = sql + " 					false)) ";
		sql = sql + "  group by  tempo_atrazo ";

		sql = sql + " union SELECT  'receb' as tipo,concat('F',emp.idEmpresaFisica,'-') as idEmpresa,emp.razaoSocial, ";
		sql = sql + " IF(DATEDIFF(par.databaixa,par.dataVencimento)<=10, 10,    ";
		sql = sql + "  	IF(DATEDIFF(par.databaixa,par.dataVencimento)<=20,20,    ";
		sql = sql + "  	 IF(DATEDIFF(par.databaixa,par.dataVencimento)<=30,30,  	  ";
		sql = sql + "    IF(DATEDIFF(par.databaixa,par.dataVencimento)<=60,60,  	   ";
		sql = sql + "    IF(DATEDIFF(par.databaixa,par.dataVencimento)<=90,90,  	   ";
		sql = sql + "     IF(DATEDIFF(par.databaixa,par.dataVencimento)<=120,120,30000)))))) AS tempo_atrazo,0.0 as parcial, ";
		sql = sql + " sum(if(mov.tipoPagamento = 2, mov.valorRecebido,mov.valorRecebido - IFNULL(mov.valorEntradaJuros, 0) + IFNULL(mov.valorDesconto, 0))) as totalValorParcela ";
		sql = sql + "  ";
		sql = sql + " FROM movimentoparceladuplicata mov ";
		sql = sql + " INNER JOIN duplicataparcela par ON par.idDuplicataParcela = mov.idDuplicataParcela ";
		sql = sql + " INNER JOIN duplicata dupli ON dupli.idDuplicata = par.idDuplicata ";
		sql = sql + " inner join empresafisica emp on emp.idempresafisica = dupli.idempresafisica ";
		sql = sql + " 	left outer join ( ";
		sql = sql + " 				select antiga.dataCompra as dataCompraAntiga,ac.idDuplicataNova  ";
		sql = sql + " 				from duplicataacordo ac   ";
		sql = sql + " 				inner join duplicata antiga on antiga.numeroDuplicata = ac.numeroDuplicataAntiga  ";
		sql = sql + " 				group by ac.idDuplicataNova  ";
		sql = sql + " 				order by antiga.dataCompra  ";
		sql = sql + " 		 ) as acordo on acordo.idDuplicataNova = par.idDuplicata  ";
		sql = sql + " Where dataMovimento between '"+dataInicial+" 00:00:00' and '"+dataInicial+" 23:59:59' ";
		sql = sql + " and (par.numeroParcela <> 0) and DATEDIFF('"+dataInicial+"',par.dataVencimento) > 0 ";
		sql = sql + " 	and if(dupli.datacompra < '"+dataSeralle+"', ";
		sql = sql + " 			true, ";
		sql = sql + " 			if(par.acordo= 'S', ";
		sql = sql + " 					acordo.dataCompraAntiga < '"+dataSeralle+"', ";
		sql = sql + " 					false)) ";
		sql = sql + " group by tempo_atrazo ";

		sql = sql + " union SELECT   'receb' as tipo,concat('F',emp.idEmpresaFisica,'-') as idEmpresa,emp.razaoSocial, ";
		sql = sql + "  ";
		sql = sql + " IF(DATEDIFF(par.databaixa,par.dataVencimento)<=10, 10,    ";
		sql = sql + "  	IF(DATEDIFF(par.databaixa,par.dataVencimento)<=20,20,    ";
		sql = sql + "  	 IF(DATEDIFF(par.databaixa,par.dataVencimento)<=30,30,  	  ";
		sql = sql + "    IF(DATEDIFF(par.databaixa,par.dataVencimento)<=60,60,  	   ";
		sql = sql + "    IF(DATEDIFF(par.databaixa,par.dataVencimento)<=90,90,  	   ";
		sql = sql + "     IF(DATEDIFF(par.databaixa,par.dataVencimento)<=120,120,30000)))))) AS tempo_atrazo, ";
		sql = sql + "  sum( @parcial :=(select sum(valorrecebido) from  movimentoparceladuplicata m where m.idDuplicataParcela = par.idDuplicataParcela and tipopagamento = 2)) as parcial, ";
		sql = sql + "   sum(par.valorParcela - if(@parcial is null,0.0,@parcial)) as totalValorParcela  ";
		sql = sql + "  FROM duplicataparcela par  ";
		sql = sql + "  INNER JOIN duplicata dup on dup.idDuplicata = par.idDuplicata   ";
		sql = sql + "  INNER JOIN formapagamento forma on forma.idformapagamento = par.idformapagamento   ";
		sql = sql + " inner join empresafisica emp on emp.idEmpresaFisica = dup.idEmpresaFisica ";
		sql = sql + " left outer join ( ";
		sql = sql + " 					select antiga.dataCompra as dataCompraAntiga,ac.idDuplicataNova  ";
		sql = sql + " 					from duplicataacordo ac   ";
		sql = sql + " 					inner join duplicata antiga on antiga.numeroDuplicata = ac.numeroDuplicataAntiga  ";
		sql = sql + " 					group by ac.idDuplicataNova  ";
		sql = sql + " 					order by antiga.dataCompra  ";
		sql = sql + " 			 ) as acordo on acordo.idDuplicataNova = par.idDuplicata  ";
		sql = sql + " where tipopagamento = 5  ";
		sql = sql + "  and par.situacao = 3  ";
		sql = sql + "  and forma.idformapagamento =  48  ";
		sql = sql + " and dataBaixa between '"+dataInicial+" 00:00:00'  and '"+dataInicial+" 23:59:59'  ";
		sql = sql + " 			and if(dup.datacompra < '"+dataSeralle+"', ";
		sql = sql + " 			true, ";
		sql = sql + " 			if(par.acordo= 'S', ";
		sql = sql + " 					acordo.dataCompraAntiga < '"+dataSeralle+"', ";
		sql = sql + " 					false)) ";
		sql = sql + " and DATEDIFF('"+dataInicial+"',par.dataVencimento) > 0 ";
		sql = sql + " group by tempo_atrazo ";
		
	
		con = ConexaoUtil.getConexaoPadrao();
		ResultSet rs = con.createStatement().executeQuery(sql);
		
		String strTabelaTemporaria = "";
		strTabelaTemporaria = strTabelaTemporaria + " CREATE TEMPORARY TABLE IF NOT EXISTS ";
		strTabelaTemporaria = strTabelaTemporaria + " tmpNotificaoRecebimentos ";
		strTabelaTemporaria = strTabelaTemporaria + " ( ";
		strTabelaTemporaria = strTabelaTemporaria + " 	atrasoDias int(10), ";
		strTabelaTemporaria = strTabelaTemporaria + " 	idEmpresaFisica varchar(11), ";
		strTabelaTemporaria = strTabelaTemporaria + " 	razaoSocial varchar(255), ";
		strTabelaTemporaria = strTabelaTemporaria + " 	valorParcelasAtraso Double, ";
		strTabelaTemporaria = strTabelaTemporaria + "     valorParcelasRecebidas Double ";
		strTabelaTemporaria = strTabelaTemporaria + " ) ENGINE=MEMORY ";
		con.createStatement().execute(strTabelaTemporaria);
		
		while(rs.next())
		{
			String sqlInsertTblTemp = "";
			sqlInsertTblTemp = sqlInsertTblTemp + " INSERT INTO `tmpNotificaoRecebimentos` ";
			sqlInsertTblTemp = sqlInsertTblTemp + " (`atrasoDias`, ";
			sqlInsertTblTemp = sqlInsertTblTemp + " `idEmpresaFisica`, ";
			sqlInsertTblTemp = sqlInsertTblTemp + " `razaoSocial`, ";
			sqlInsertTblTemp = sqlInsertTblTemp + " `valorParcelasAtraso`, ";
			sqlInsertTblTemp = sqlInsertTblTemp + " `valorParcelasRecebidas`) ";
			sqlInsertTblTemp = sqlInsertTblTemp + " VALUES ";
			sqlInsertTblTemp = sqlInsertTblTemp + " ("+rs.getInt("tempo_atrazo")+", ";
			sqlInsertTblTemp = sqlInsertTblTemp + "'"+rs.getString("idEmpresa")+"', ";
			sqlInsertTblTemp = sqlInsertTblTemp + "'"+rs.getString("razaoSocial")+"', ";
			if(rs.getString("tipo").equals("credi"))
			{
				sqlInsertTblTemp = sqlInsertTblTemp + " '"+rs.getDouble("totalValorParcela")+"', ";
				sqlInsertTblTemp = sqlInsertTblTemp + " NULL ); ";
			}
			else if(rs.getString("tipo").equals("receb"))
			{
				sqlInsertTblTemp = sqlInsertTblTemp + " NULL , ";
				sqlInsertTblTemp = sqlInsertTblTemp + " '"+rs.getDouble("totalValorParcela")+"'); ";
			}
			sqlInsertTblTemp = sqlInsertTblTemp + "  ";
			con.createStatement().execute(sqlInsertTblTemp);

		}
		String sqlRelatorio = "";
		sqlRelatorio = sqlRelatorio + " select r.atrasoDias,r.idEmpresaFisica,r.razaoSocial, ";
		sqlRelatorio = sqlRelatorio + " sum(r.valorParcelasAtraso) as valorParcelasAtrasadas,sum(r.valorParcelasRecebidas) as valorParcelasRecebidas,";
		sqlRelatorio = sqlRelatorio + " (sum(if(r.valorParcelasAtraso is null,0.0,r.valorParcelasAtraso)) +  sum(if(r.valorParcelasRecebidas is null,0.0,r.valorParcelasRecebidas))) as valorTotalInicial";
		sqlRelatorio = sqlRelatorio + " from tmpNotificaoRecebimentos r ";
		sqlRelatorio = sqlRelatorio + " group by atrasoDias ";
		sqlRelatorio = sqlRelatorio + "  ";	
		ResultSet rsRelatorio = con.createStatement().executeQuery(sqlRelatorio);
		return rsRelatorio;
	}
	public static ResultSet gerarResultSetRelatorioNotificacaoRecebimentosAVencer(Date data,String idEmpresa,Connection con) throws Exception
	{
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat dfParam = new SimpleDateFormat("dd/MM/yyyy");
		String dataInicial = df.format(data);
		Date dataSerraleDate = DsvConstante.getParametrosSistema().get("dataSeralle")==null?new Date():dfParam.parse(DsvConstante.getParametrosSistema().get("dataSeralle"));
		String dataSeralle = df.format(dataSerraleDate);
		
		String sql ="";
		sql = sql + "   SELECT  ";
		sql = sql + "     'avencer' as tipo, ";
		sql = sql + "     concat('F', emp.idEmpresaFisica, '-') as idEmpresa, ";
		sql = sql + "     emp.razaoSocial, ";
		sql = sql + "     IF(DATEDIFF(dupp.dataVencimento, '"+dataInicial+"') <= 10, ";
		sql = sql + "         10, ";
		sql = sql + "         IF(DATEDIFF(dupp.dataVencimento, '"+dataInicial+"') <= 20, ";
		sql = sql + "             20, ";
		sql = sql + "             IF(DATEDIFF(dupp.dataVencimento, '"+dataInicial+"') <= 30, ";
		sql = sql + "                  30, ";
		sql = sql + "                 IF(DATEDIFF(dupp.dataVencimento,'"+dataInicial+"') <= 60, ";
		sql = sql + "                     60, ";
		sql = sql + "                     IF(DATEDIFF(dupp.dataVencimento, '"+dataInicial+"') <= 90, ";
		sql = sql + "                         90, ";
		sql = sql + "                         IF(DATEDIFF(dupp.dataVencimento, '"+dataInicial+"') <= 120, ";
		sql = sql + "                             120, ";
		sql = sql + "                             IF(DATEDIFF(dupp.dataVencimento, '"+dataInicial+"') <= 360, ";
		sql = sql + "                                 360, ";
		sql = sql + "                                 30000))))))) AS tempo_atrazo,0.0 as parcial, ";
		sql = sql + "     IFNULL(sum(dupp.valorParcela), 0) AS totalValorParcela ";
		sql = sql + " FROM ";
		sql = sql + "     duplicataparcela dupp ";
		sql = sql + "         left outer join ";
		sql = sql + "     duplicata dup ON dup.idDuplicata = dupp.idDuplicata ";
		sql = sql + "         left outer join ";
		sql = sql + "     empresaFisica emp ON emp.idEmpresaFisica = dup.idEmpresaFisica ";
		sql = sql + " left outer join ( ";
		sql = sql + " 		select antiga.dataCompra as dataCompraAntiga,ac.idDuplicataNova ";
		sql = sql + " 		from duplicataacordo ac  ";
		sql = sql + " 		inner join duplicata antiga on antiga.numeroDuplicata = ac.numeroDuplicataAntiga ";
		sql = sql + " 		group by ac.idDuplicataNova ";
		sql = sql + " 		order by antiga.dataCompra ";
		sql = sql + " ) as acordo on acordo.idDuplicataNova = dupp.idDuplicata ";
		sql = sql + " where ";
		sql = sql + "     dupp.dataVencimento > '"+dataInicial+"' ";
		sql = sql + "         and (dupp.situacao = '0' ";
		sql = sql + "         or dupp.situacao = '2') ";
		sql = sql + "         and DATEDIFF(dupp.dataVencimento, '"+dataInicial+"') > 0 ";
		sql = sql + "         and dup.idEmpresaFisica in ("+idEmpresa+") ";
		sql = sql + " 		and dup.datacompra <'"+dataSeralle+"' ";
		sql = sql + " 		and DATEDIFF(dupp.dataVencimento,'"+dataInicial+"') > 0 ";
		sql = sql + " group by   tempo_atrazo ";
		sql = sql + " UNION SELECT  ";
		sql = sql + "     'avencerAcordo' as tipo, ";
		sql = sql + "     concat('F', emp.idEmpresaFisica, '-') as idEmpresa, ";
		sql = sql + "     emp.razaoSocial, ";
		sql = sql + "     IF(DATEDIFF(dupp.dataVencimento,'"+dataInicial+"') <= 10, ";
		sql = sql + "         10, ";
		sql = sql + "         IF(DATEDIFF(dupp.dataVencimento, '"+dataInicial+"') <= 20, ";
		sql = sql + "             20, ";
		sql = sql + "             IF(DATEDIFF(dupp.dataVencimento, '"+dataInicial+"') <= 30, ";
		sql = sql + "                  30, ";
		sql = sql + "                 IF(DATEDIFF(dupp.dataVencimento, '"+dataInicial+"') <= 60, ";
		sql = sql + "                     60, ";
		sql = sql + "                     IF(DATEDIFF(dupp.dataVencimento, '"+dataInicial+"') <= 90, ";
		sql = sql + "                         90, ";
		sql = sql + "                         IF(DATEDIFF(dupp.dataVencimento, '"+dataInicial+"') <= 120, ";
		sql = sql + "                             120, ";
		sql = sql + "                             IF(DATEDIFF(dupp.dataVencimento, '"+dataInicial+"') <= 360, ";
		sql = sql + "                                 360, ";
		sql = sql + "                                 30000))))))) AS tempo_atrazo,0.0 as parcial, ";
		sql = sql + "     IFNULL(sum(dupp.valorParcela), 0) AS totalValorParcela ";
		sql = sql + " FROM ";
		sql = sql + "     duplicataparcela dupp ";
		sql = sql + "         left outer join ";
		sql = sql + "     duplicata dup ON dup.idDuplicata = dupp.idDuplicata ";
		sql = sql + "         left outer join ";
		sql = sql + "     empresaFisica emp ON emp.idEmpresaFisica = dup.idEmpresaFisica ";
		sql = sql + " left outer join ( ";
		sql = sql + " 		select antiga.dataCompra as dataCompraAntiga,ac.idDuplicataNova ";
		sql = sql + " 		from duplicataacordo ac  ";
		sql = sql + " 		inner join duplicata antiga on antiga.numeroDuplicata = ac.numeroDuplicataAntiga ";
		sql = sql + " 		group by ac.idDuplicataNova ";
		sql = sql + " 		order by antiga.dataCompra ";
		sql = sql + " ) as acordo on acordo.idDuplicataNova = dupp.idDuplicata ";
		sql = sql + " where ";
		sql = sql + "     dupp.dataVencimento > '"+dataInicial+"' ";
		sql = sql + "         and (dupp.situacao = '0' ";
		sql = sql + "         or dupp.situacao = '2') ";
		sql = sql + "         and DATEDIFF(dupp.dataVencimento, '"+dataInicial+"') > 0 ";
		sql = sql + "         and dup.idEmpresaFisica in ("+idEmpresa+") ";
		sql = sql + " 		and dupp.acordo = 'S' ";
		sql = sql + " 		and dup.dataCompra > '"+dataSeralle+"' ";
		sql = sql + " 		and DATEDIFF(dupp.dataVencimento,'"+dataInicial+"') > 0";
		sql = sql + " 		and acordo.dataCompraAntiga < '"+dataSeralle+"' ";
		sql = sql + " group by  tempo_atrazo ";
		
		sql = sql + " union SELECT  'receb' as tipo,concat('F',emp.idEmpresaFisica,'-') as idEmpresa,emp.razaoSocial, ";
		sql = sql + " IF(DATEDIFF(par.dataVencimento,par.databaixa)<=10, 10,    ";
		sql = sql + "  	IF(DATEDIFF(par.dataVencimento,par.databaixa)<=20,20,    ";
		sql = sql + "  	 IF(DATEDIFF(par.dataVencimento,par.databaixa)<=30,30,  	  ";
		sql = sql + "    IF(DATEDIFF(par.dataVencimento,par.databaixa)<=60,60,  	   ";
		sql = sql + "    IF(DATEDIFF(par.dataVencimento,par.databaixa)<=90,90,  	   ";
		sql = sql + "     IF(DATEDIFF(par.dataVencimento,par.databaixa)<=120,120,  	     ";
		sql = sql + "    IF(DATEDIFF(par.dataVencimento,par.databaixa)<=360,360,30000))))))) AS tempo_atrazo,0.0 as parcial, ";
		sql = sql + " sum(if(mov.tipoPagamento = 2, mov.valorRecebido,mov.valorRecebido - IFNULL(mov.valorEntradaJuros, 0) + IFNULL(mov.valorDesconto, 0))) as totalValorParcela ";
		sql = sql + "  ";
		sql = sql + " FROM movimentoparceladuplicata mov ";
		sql = sql + " INNER JOIN duplicataparcela par ON par.idDuplicataParcela = mov.idDuplicataParcela ";
		sql = sql + " INNER JOIN duplicata dupli ON dupli.idDuplicata = par.idDuplicata ";
		sql = sql + " inner join empresafisica emp on emp.idempresafisica = dupli.idempresafisica ";
		sql = sql + " 	left outer join ( ";
		sql = sql + " 				select antiga.dataCompra as dataCompraAntiga,ac.idDuplicataNova  ";
		sql = sql + " 				from duplicataacordo ac   ";
		sql = sql + " 				inner join duplicata antiga on antiga.numeroDuplicata = ac.numeroDuplicataAntiga  ";
		sql = sql + " 				group by ac.idDuplicataNova  ";
		sql = sql + " 				order by antiga.dataCompra  ";
		sql = sql + " 		 ) as acordo on acordo.idDuplicataNova = par.idDuplicata  ";
		sql = sql + " Where dataMovimento between '"+dataInicial+" 00:00:00' and '"+dataInicial+" 23:59:59' ";
		sql = sql + " and (par.numeroParcela <> 0) ";
		sql = sql + "  and DATEDIFF(par.dataVencimento,'"+dataInicial+"') > 0 ";
		sql = sql + " 	and if(dupli.datacompra < '"+dataSeralle+"', ";
		sql = sql + " 			true, ";
		sql = sql + " 			if(par.acordo= 'S', ";
		sql = sql + " 					acordo.dataCompraAntiga < '"+dataSeralle+"', ";
		sql = sql + " 					false)) ";
		sql = sql + " group by tempo_atrazo ";

		sql = sql + " union SELECT   'receb' as tipo,concat('F',emp.idEmpresaFisica,'-') as idEmpresa,emp.razaoSocial, ";
		sql = sql + "  ";
		sql = sql + " IF(DATEDIFF(par.dataVencimento,par.databaixa)<=10, 10,    ";
		sql = sql + "  	IF(DATEDIFF(par.dataVencimento,par.databaixa)<=20,20,    ";
		sql = sql + "  	 IF(DATEDIFF(par.dataVencimento,par.databaixa)<=30,30,  	  ";
		sql = sql + "    IF(DATEDIFF(par.dataVencimento,par.databaixa)<=60,60,  	   ";
		sql = sql + "    IF(DATEDIFF(par.dataVencimento,par.databaixa)<=90,90,  	   ";
		sql = sql + "     IF(DATEDIFF(par.dataVencimento,par.databaixa)<=120,120,  	     ";
		sql = sql + "    IF(DATEDIFF(par.dataVencimento,par.databaixa)<=360,360,30000))))))) AS tempo_atrazo, ";
		sql = sql + "  sum( @parcial :=(select sum(valorrecebido) from  movimentoparceladuplicata m where m.idDuplicataParcela = par.idDuplicataParcela and tipopagamento = 2)) as parcial, ";
		sql = sql + "   sum(par.valorParcela - if(@parcial is null,0.0,@parcial)) as totalValorParcela  ";
		sql = sql + "  FROM duplicataparcela par  ";
		sql = sql + "  INNER JOIN duplicata dup on dup.idDuplicata = par.idDuplicata   ";
		sql = sql + "  INNER JOIN formapagamento forma on forma.idformapagamento = par.idformapagamento   ";
		sql = sql + " inner join empresafisica emp on emp.idEmpresaFisica = dup.idEmpresaFisica ";
		sql = sql + " left outer join ( ";
		sql = sql + " 					select antiga.dataCompra as dataCompraAntiga,ac.idDuplicataNova  ";
		sql = sql + " 					from duplicataacordo ac   ";
		sql = sql + " 					inner join duplicata antiga on antiga.numeroDuplicata = ac.numeroDuplicataAntiga  ";
		sql = sql + " 					group by ac.idDuplicataNova  ";
		sql = sql + " 					order by antiga.dataCompra  ";
		sql = sql + " 			 ) as acordo on acordo.idDuplicataNova = par.idDuplicata  ";
		sql = sql + " where tipopagamento = 5  ";
		sql = sql + "  and par.situacao = 3  ";
		sql = sql + "  and forma.idformapagamento =  48  ";
		sql = sql + " and dataBaixa between '"+dataInicial+" 00:00:00'  and '"+dataInicial+" 23:59:59'  ";
		sql = sql + " 			and if(dup.datacompra < '"+dataSeralle+"', ";
		sql = sql + " 			true, ";
		sql = sql + " 			if(par.acordo= 'S', ";
		sql = sql + " 					acordo.dataCompraAntiga < '"+dataSeralle+"', ";
		sql = sql + " 					false)) ";
		sql = sql + " and DATEDIFF(par.dataVencimento,'"+dataInicial+"') > 0";
		sql = sql + " group by tempo_atrazo ";
		System.out.println(""+sql);
		ResultSet rs = con.createStatement().executeQuery(sql);
		
		String strTabelaTemporaria = "";
		strTabelaTemporaria = strTabelaTemporaria + " CREATE TEMPORARY TABLE IF NOT EXISTS ";
		strTabelaTemporaria = strTabelaTemporaria + " tmpNotificaoRecebimentos ";
		strTabelaTemporaria = strTabelaTemporaria + " ( ";
		strTabelaTemporaria = strTabelaTemporaria + " 	atrasoDias int(10), ";
		strTabelaTemporaria = strTabelaTemporaria + " 	idEmpresaFisica varchar(11), ";
		strTabelaTemporaria = strTabelaTemporaria + " 	razaoSocial varchar(255), ";
		strTabelaTemporaria = strTabelaTemporaria + " 	valorParcelasAtraso Double, ";
		strTabelaTemporaria = strTabelaTemporaria + "   valorParcelasRecebidas Double ";
		strTabelaTemporaria = strTabelaTemporaria + " ) ENGINE=MEMORY ";
		con.createStatement().execute(strTabelaTemporaria);
		
		while(rs.next())
		{
			String sqlInsertTblTemp = "";
			sqlInsertTblTemp = sqlInsertTblTemp + " INSERT INTO `tmpNotificaoRecebimentos` ";
			sqlInsertTblTemp = sqlInsertTblTemp + " (`atrasoDias`, ";
			sqlInsertTblTemp = sqlInsertTblTemp + " `idEmpresaFisica`, ";
			sqlInsertTblTemp = sqlInsertTblTemp + " `razaoSocial`, ";
			sqlInsertTblTemp = sqlInsertTblTemp + " `valorParcelasAtraso`, ";
			sqlInsertTblTemp = sqlInsertTblTemp + " `valorParcelasRecebidas`) ";
			sqlInsertTblTemp = sqlInsertTblTemp + " VALUES ";
			sqlInsertTblTemp = sqlInsertTblTemp + " ("+rs.getInt("tempo_atrazo")+", ";
			sqlInsertTblTemp = sqlInsertTblTemp + "'"+rs.getString("idEmpresa")+"', ";
			sqlInsertTblTemp = sqlInsertTblTemp + "'"+rs.getString("razaoSocial")+"', ";
			if(rs.getString("tipo").equals("avencer") || rs.getString("tipo").equals("avencerAcordo"))
			{
				sqlInsertTblTemp = sqlInsertTblTemp + " '"+rs.getDouble("totalValorParcela")+"', ";
				sqlInsertTblTemp = sqlInsertTblTemp + " NULL ); ";
			}
			else if(rs.getString("tipo").equals("receb"))
			{
				sqlInsertTblTemp = sqlInsertTblTemp + " NULL , ";
				sqlInsertTblTemp = sqlInsertTblTemp + " '"+rs.getDouble("totalValorParcela")+"'); ";
			}
			sqlInsertTblTemp = sqlInsertTblTemp + "  ";
			con.createStatement().execute(sqlInsertTblTemp);

		}
		String sqlRelatorio = "";
		sqlRelatorio = sqlRelatorio + " select r.atrasoDias,r.idEmpresaFisica,r.razaoSocial, ";
		sqlRelatorio = sqlRelatorio + " sum(r.valorParcelasAtraso) as valorParcelasAtrasadas,sum(r.valorParcelasRecebidas) as valorParcelasRecebidas,";
		sqlRelatorio = sqlRelatorio + " (sum(if(r.valorParcelasAtraso is null,0.0,r.valorParcelasAtraso)) +  sum(if(r.valorParcelasRecebidas is null,0.0,r.valorParcelasRecebidas))) as valorTotalInicial";
		sqlRelatorio = sqlRelatorio + " from tmpNotificaoRecebimentos r ";
		sqlRelatorio = sqlRelatorio + " group by atrasoDias ";
		sqlRelatorio = sqlRelatorio + "  ";	
		ResultSet rsRelatorio = con.createStatement().executeQuery(sqlRelatorio);
		return rsRelatorio;
	}
}
