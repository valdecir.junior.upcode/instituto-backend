package br.com.desenv.nepalign.relatorios;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.desenv.padraoignorante.conexao.ConexaoUtil;


import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

public class RelatoriosInstituto 
{

	protected HttpServletRequest request;
	protected HttpServletResponse response;	
	
	public RelatoriosInstituto(HttpServletRequest request, HttpServletResponse response) throws ServletException, Exception
	{
		this.request = request;
		this.response = response;
	}
	
	
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao
	public void gerarRelatorioAtendidosGeral() throws ServletException, Exception 
	{
		String nomeRelatorio=null;
		
		try
		{

			Connection con = ConexaoUtil.getConexaoPadrao();  
			Statement stm = con.createStatement();
			int tipoRelatorio = (request.getParameter("tipoRelatorio")==null?0:Integer.parseInt(request.getParameter("tipoRelatorio")));
			int idAtendido= (request.getParameter("idAtendido")==null?0:Integer.parseInt(request.getParameter("idAtendido")));
			int idTurma= (request.getParameter("idTurma")==null?0:Integer.parseInt(request.getParameter("idTurma")));
			int idAtividade= (request.getParameter("idAtividade")==null?0:Integer.parseInt(request.getParameter("idAtividade")));
			int situacaoCadastral= (request.getParameter("situacaoCadastral")==null?0:Integer.parseInt(request.getParameter("situacaoCadastral")));
			StringBuffer sb = new StringBuffer();
			
			if (tipoRelatorio==1) // atendido e suas turmas
			{
				
				sb.append("select atendido.idAtendido, atendido.nomeCompleto, atendido.foneFixo, atendido.fonecelular, turma.descricao,");
				sb.append("turma.diaSemana, turma.horarioInicial, turma.horarioFinal, localatividade.nomeLocal ");
				sb.append("from turmaatendido ");
				sb.append("inner join turma on turma.idTurma = turmaatendido.idTurma ");
				sb.append("inner join atendido on atendido.idAtendido = turmaAtendido.idAtendido ");
				sb.append("inner join localatividade on localatividade.idLocalAtividade = turma.idLocalAtividade ");
				sb.append("where 1=1 ");
				
				if (idAtendido>0)
					sb.append(" AND atendido.idAtendido = " + idAtendido);
				if (idTurma>0)
					sb.append(" AND turma.idTurma = " + idTurma);
				if (idAtividade>0)
					sb.append(" AND turma.idAtividadeOferecida = " + idAtividade);
				if (situacaoCadastral>0)
					sb.append(" AND atendido.situacaoCadastral = " + situacaoCadastral);
				
				//#where atendido.idAtendido in (377, 239)
			
				sb.append(" order by nomeCompleto, diaSemana, horarioInicial");
				
				nomeRelatorio="/relatorios/RelCronogramaGeralPorAntendido.jasper";
			}
			
			if (tipoRelatorio==2) //controle de presen�a
			{
				
				sb.append("select atendido.idAtendido, atendido.nomeCompleto, atendido.foneFixo, atendido.fonecelular, turma.descricao nomeTurma,");
				sb.append("turma.diaSemana, turma.horarioInicial, turma.horarioFinal, localatividade.nomeLocal, ");
				sb.append("turma.capacidadeAtendidos, professor.nome nomeProfessor ");
				sb.append("from turmaatendido ");
				sb.append("inner join turma on turma.idTurma = turmaatendido.idTurma ");
				sb.append("inner join atendido on atendido.idAtendido = turmaAtendido.idAtendido ");
				sb.append("inner join localatividade on localatividade.idLocalAtividade = turma.idLocalAtividade ");
				sb.append("inner join professor on professor.idProfessor = turma.idProfessor ");
				sb.append("where 1=1 ");
				
				if (idAtendido>0)
					sb.append(" AND atendido.idAtendido = " + idAtendido);
				if (idTurma>0)
					sb.append(" AND turma.idTurma = " + idTurma);
				if (idAtividade>0)
					sb.append(" AND turma.idAtividadeOferecida = " + idAtividade);
				if (situacaoCadastral>0)
					sb.append(" AND atendido.situacaoCadastral = " + situacaoCadastral);
				
				//#where atendido.idAtendido in (377, 239)
			
				sb.append(" order by turma.descricao, diaSemana, horarioInicial, atendido.nomeCompleto");
				nomeRelatorio="/relatorios/RelControlePresencaTurmaAtendido.jasper";
			}	
			if (tipoRelatorio==4) //Hor�rios de aulas
			{
				
				sb.append("select atendido.idAtendido "); 
				sb.append("from turmaatendido ");
				sb.append("inner join turma on turma.idTurma = turmaatendido.idTurma ");
				sb.append("inner join atendido on atendido.idAtendido = turmaAtendido.idAtendido ");
				sb.append("inner join localatividade on localatividade.idLocalAtividade = turma.idLocalAtividade ");
				sb.append("where 1=1 ");
				
				if (idAtendido>0)
					sb.append(" AND atendido.idAtendido = " + idAtendido);
				if (idTurma>0)
					sb.append(" AND turma.idTurma = " + idTurma);
				if (idAtividade>0)
					sb.append(" AND turma.idAtividadeOferecida = " + idAtividade);
				if (situacaoCadastral>0)
					sb.append(" AND atendido.situacaoCadastral = " + situacaoCadastral);
				
				//#where atendido.idAtendido in (377, 239)
			
				sb.append(" order by nomeCompleto, diaSemana, horarioInicial");
				nomeRelatorio="/relatorios/RelMasterHorarioAtendidos.jasper";
			}				

			if (tipoRelatorio==3) //Etiqueta
			{
				
				sb.append("select idAtendido codigo, LPAD(idAtendido, 5,0) codigoFormatado, ");
				sb.append("nomeCompleto, foneFixo, foneCelular, DATE_FORMAT(dataNascimento, '0%y0MY%m') as dataFormatada ");
				sb.append("from Atendido  ");
				sb.append("where 1=1 ");
				
				if (idAtendido>0)
					sb.append(" AND atendido.idAtendido = " + idAtendido);
				/*if (idTurma>0)
					sb.append(" AND turma.idTurma = " + idTurma);
				if (idAtividade>0)
					sb.append(" AND turma.idAtividadeOferecida = " + idAtividade);*/
				if (situacaoCadastral>0)
					sb.append(" AND atendido.situacaoCadastral = " + situacaoCadastral);
				
				//#where atendido.idAtendido in (377, 239)
			
				sb.append(" order by nomeCompleto ");
				nomeRelatorio="/relatorios/geraEtiquetaAtendidos.jasper";
			}			
			System.out.println(sb.toString());
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath());
			params.put("REPORT_CONNECTION", ConexaoUtil.getConexaoPadrao());
	   	    
	   	    //params.put("REPORT_DATA_SOURCE_DESPESAS", this.recuperarDataSourceDespesas(caixa));
	   	    						
			ResultSet rs = stm.executeQuery(String.valueOf(sb.toString()));
			rs.beforeFirst();
			JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);

			byte[] bytes = JasperExportManager.exportReportToPdf(JasperFillManager.fillReport(getClass().getResource(nomeRelatorio).getPath(), params, new JRResultSetDataSource(con.createStatement().executeQuery(sb.toString()))));
		    if (bytes != null && bytes.length > 0) 
		    {    
		    	response.setContentType("application/pdf");  
		    	response.setContentLength(bytes.length);  
		    	ServletOutputStream ouputStream = response.getOutputStream();  
		    	ouputStream.write(bytes, 0, bytes.length);  
		    	ouputStream.flush();  
		    	ouputStream.close();  
		    } 
		    
			/*JasperPrint impressao = JasperFillManager.fillReport(getClass().getResource("/relatorios/RelCronogramaGeralPorAntendido.jasper").getPath(), params, jrRS);
			//byte[] bytes = JasperExportManager.exportReportToPdf(impressao);  
			this.response.setContentLength(bytes.length);
			this.response.setContentType("application/pdf");
			this.response.getOutputStream().write(bytes, 0, bytes.length);     	
			this.response.getOutputStream().flush();
			this.response.getOutputStream().close();*/
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}
}
