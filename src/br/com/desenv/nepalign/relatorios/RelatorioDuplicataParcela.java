package br.com.desenv.nepalign.relatorios;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao.Grupo;
import br.com.desenv.nepalign.model.DuplicataParcela;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.ItemRelatorioDivergenciasCrediario;
import br.com.desenv.nepalign.model.ParcelaAPagar;
import br.com.desenv.nepalign.service.DuplicataParcelaService;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.UsuarioService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class RelatorioDuplicataParcela 
{
	String caminho;	
	private Connection conn;
	private HttpServletRequest request;
	private HttpServletResponse response;

	public RelatorioDuplicataParcela(HttpServletRequest request, HttpServletResponse response) throws ServletException, Exception
	{
		this.request=request;
		this.response=response;
	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Duplicatas Pagas")
	public void gerarRelatorioDupliatasPagas() throws Exception
	{
		byte[] bytes = null;  

		try 
		{  
			Boolean uae = new UsuarioService().getSessionUae(request);

			conn = ConexaoUtil.getConexaoPadrao();    
			Statement stm = conn.createStatement();
			EmpresaFisica empresa = null;

			if(!request.getParameter("idEmpresaFisica").equals(""))
				empresa = new EmpresaFisicaService().recuperarPorId(Integer.parseInt((request.getParameter("idEmpresaFisica"))));
			String comAcordo = (request.getParameter("comAcordo")==null?"N":request.getParameter("comAcordo"));
			String soAcordo = (request.getParameter("soAcordo")==null?"N":request.getParameter("soAcordo"));
			if(soAcordo.equals("S"))
				caminho = "subRelatorioDuplicatasPagasAcordo.jasper";
			else
				caminho = "relatorioDuplicatasPagas.jasper"; 

			URL urlArquivo = getClass().getResource("/relatorios/"+caminho);
			String rel = urlArquivo.getPath();

			Image logo = null;
			InputStream inputStreamDaImagem = null;   
			URL urlImagens = getClass().getResource("/imagens/logo.jpg");
			String caminhoImagem = urlImagens.getPath();
			try {    
				File file = new File(caminhoImagem);   

				if(file.exists())//testa se imagem existe  
					inputStreamDaImagem = new BufferedInputStream(
							new FileInputStream(caminhoImagem));

				logo = ImageIO.read(inputStreamDaImagem);
			} catch (FileNotFoundException e) {       
				e.printStackTrace();       
			}   
			java.sql.Statement rec = conn.createStatement();   

			String dataEmissaoInicial = "";
			String dataEmissaoFinal = "";
			String dataSqlEmissaoInicial = null;
			String dataSqlEmissaoFinal = null;
			SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
			SimpleDateFormat dfa = new SimpleDateFormat ("dd/MM/yyyy");
			Date dFinal = null;
			Date dInicial = null;
			String dataSInicial = "";
			String dataSFinal = "";
			Integer diasInicialAtraso = null;
			Integer diasFinalAtraso = null;

			if(request.getParameter("diasInicialAtraso")!=null && !request.getParameter("diasInicialAtraso").equals(""))
				diasInicialAtraso = Integer.parseInt(request.getParameter("diasInicialAtraso"));
			else
			{
				dataEmissaoInicial = request.getParameter("dataEmissaoInicial");
				dataEmissaoFinal = request.getParameter("dataEmissaoFinal");
			}


			if(request.getParameter("diasFinalAtraso")!=null && !request.getParameter("diasFinalAtraso").equals(""))
				diasFinalAtraso = Integer.parseInt(request.getParameter("diasFinalAtraso"));



			if(request.getParameter("dataInicial")!=null && !request.getParameter("dataInicial").equals(""))
			{
				dInicial = dfa.parse(request.getParameter("dataInicial"));
				dataSInicial =  df.format(dInicial);
			}
			if(request.getParameter("dataFinal")!=null && !request.getParameter("dataFinal").equals(""))
			{
				dFinal =  dfa.parse(request.getParameter("dataFinal"));
				dataSFinal =  df.format(dFinal);
			}

			if(!dataEmissaoInicial.equals(""))
				dataSqlEmissaoInicial = IgnUtil.dateFormatSql.format(IgnUtil.dateFormat.parse(dataEmissaoInicial));
			if(!dataEmissaoFinal.equals(""))
				dataSqlEmissaoFinal = IgnUtil.dateFormatSql.format(IgnUtil.dateFormat.parse(dataEmissaoFinal));

			String str = ""; 
			JasperPrint impressao = null;

			if(soAcordo.equals("N"))
			{	
				if(uae)
				{
					str = str + " SELECT par.acordo, DATEDIFF(mov.dataMovimento,par.dataVencimento) as 'diasAtraso', "; 
					str = str + " if(mov.tipoPagamento=2,0.0,mov.valorEntradaJuros) as 'juros',mov.valorRecebido,par.valorParcela,if(mov.tipoPagamento=2,0.0,mov.valorDesconto), ";
					str = str + " if(mov.tipoPagamento = 2, mov.valorRecebido,mov.valorRecebido - IFNULL(mov.valorEntradaJuros,0) + IFNULL(mov.valorDesconto,0)) as valorDuplicata, ";
					str = str + " forma.descricao as 'forma', dupli.numeroDuplicata,mov.valorDesconto, "; 
					str = str + " CASE mov.tipoPagamento "; 
					str = str + " WHEN '0' THEN 'COMPRA' "; 
					str = str + " WHEN '1' THEN 'PAGAMENTO TOTAL' "; 
					str = str + " WHEN '2' THEN 'PAGAMENTO PARCIAL' "; 
					str = str + " WHEN '5' THEN 'ACORDO'  "; 
					str = str + " END AS historico, "; 
					str = str + " concat(cli.idCliente,'-',cli.nome ) as 'cliente', par.numeroParcela, mov.dataMovimento, mov.horaMovimento, " ;
					str = str + " usu.nome as nomeUsuario ";
					str = str + " FROM movimentoparceladuplicata mov ";
					//str = str + " LEFT OUTER JOIN movimentoCaixaDia movCaixa on movCaixa.idMovimentoparcelaDuplicata = mov.idMovimentoParcelaDuplicata ";
					//str = str + " INNER JOIN caixaDia caixa on caixa.idCaixadia = movCaixa.idCaixaDia ";
					str = str + " LEFT OUTER JOIN usuario usu on usu.idUsuario = mov.idUsuario ";
					str = str + " INNER JOIN cliente cli on  cli.idCliente = mov.idCliente "; 
					str = str + " INNER JOIN formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento "; 
					str = str + " INNER JOIN duplicataparcela par on par.idDuplicataParcela = mov.idDuplicataParcela "; 
					str = str + " INNER JOIN duplicata dupli on dupli.idDuplicata = mov.idDuplicata ";
					str = str + " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
					str = str + " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
					str = str + " group by ac.idDuplicataNova) "; 
					str = str + " as acordo on acordo.idDuplicataNova=dupli.idduplicata ";

					int x = 0;
					if(!dataSInicial.equals(""))
					{
						if(x==0)
						{
							str = str + " Where dataMovimento >= '"+dataSInicial+" 00:00:00'"; 
							x++; 
						}
						else
						{
							str = str + " And dataMovimento >= '"+dataSInicial+" 00:00:00'"; 
						}
					}

					if(!dataSFinal.equals(""))
					{
						if(x==0)
						{
							str = str + " Where dataMovimento <= '"+dataSFinal+" 23:59:59'"; 
							x++;
						}
						else
						{
							str = str + " And dataMovimento <= '"+dataSFinal+" 23:59:59'";	 
						} 
					}

					if(x==0)
					{
						str = str + " where (par.numeroParcela <> 0 ) ";
						x++;
					}
					else
						str = str + " and (par.numeroParcela <> 0 ) ";


					if(!request.getParameter("idEmpresaFisica").equals(""))
					{
						if(x==0)
						{
							str = str +" Where mov.idEmpresaFisica = " +empresa.getId();
							x++; 
						}
						else
							str = str +" and mov.idEmpresaFisica = " +empresa.getId();
					}

					if(request.getParameter("idCliente")!=null  && !request.getParameter("idCliente").equals(""))
					{
						if(x==0)
						{
							str = str +" where mov.idCliente = " + request.getParameter("idCliente") ;
							x++;
						}
						else
							str = str +" and mov.idCliente = " + request.getParameter("idCliente") ;
					}

					if(diasInicialAtraso!=null)
					{
						if(x==0)
						{
							str = str +" where DATEDIFF(mov.dataMovimento,par.dataVencimento) >= " + diasInicialAtraso ;
							x++;
						}
						else
							str = str +" and DATEDIFF(mov.dataMovimento,par.dataVencimento) >= " + diasInicialAtraso ;
					}

					if(diasFinalAtraso!=null)
					{
						if(x==0)
						{
							str = str +" where DATEDIFF(mov.dataMovimento,par.dataVencimento) <= " + diasFinalAtraso ;
							x++;
						}
						else
							str = str +" and DATEDIFF(mov.dataMovimento,par.dataVencimento) <= " + diasFinalAtraso ;
					}
					if(dataSqlEmissaoInicial != null)
					{
						if(x==0)
						{
							str += " where if((par<>'S' or par.acordo is null),dupli.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00',acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
							x++;
						}
						else
							str += " and if((par.acordo<>'S' or par.acordo is null),dupli.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00',acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00' ) ";
					}

					if(dataSqlEmissaoFinal != null)
					{
						if(x==0)
						{
							str += " where if((par.acordo<>'S' or par.acordo is null),dupli.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59') ";
							x++;
						}
						else
							str += " and if((par.acordo<>'S' or par.acordo is null),dupli.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59')";
					}

					str = str + " order by mov.idmovimentoparceladuplicata";

				}
				else
				{
					str = str + " SELECT par.acordo, DATEDIFF(mov.dataMovimento,par.dataVencimento) as 'diasAtraso', "; 
					str = str + " if(mov.tipoPagamento=2,0.0,mov.valorEntradaJuros) as 'juros',mov.valorRecebido,par.valorParcela,if(mov.tipoPagamento=2,0.0,mov.valorDesconto), ";
					str = str + " if(mov.tipoPagamento = 2, mov.valorRecebido,mov.valorRecebido - IFNULL(mov.valorEntradaJuros,0) + IFNULL(mov.valorDesconto,0)) as valorDuplicata, ";
					str = str + " forma.descricao as 'forma', dupli.numeroDuplicata,mov.valorDesconto, "; 
					str = str + " CASE mov.tipoPagamento "; 
					str = str + " WHEN '0' THEN 'COMPRA' "; 
					str = str + " WHEN '1' THEN 'PAGAMENTO TOTAL' "; 
					str = str + " WHEN '2' THEN 'PAGAMENTO PARCIAL' "; 
					str = str + " WHEN '5' THEN 'ACORDO'  "; 
					str = str + " END AS historico, "; 
					str = str + " concat(cli.idCliente,'-',cli.nome ) as 'cliente', par.numeroParcela, mov.dataMovimento, mov.horaMovimento, " ;
					str = str + " usu.nome as nomeUsuario ";
					str = str + " FROM movimentoparceladuplicata mov ";
				//	str = str + " LEFT OUTER JOIN movimentoCaixaDia movCaixa on movCaixa.idMovimentoparcelaDuplicata = mov.idMovimentoParcelaDuplicata ";
					//str = str + " INNER JOIN caixaDia caixa on caixa.idCaixadia = movCaixa.idCaixaDia ";
					str = str + " LEFT OUTER JOIN usuario usu on usu.idUsuario = mov.idUsuario ";
					str = str + " INNER JOIN cliente cli on  cli.idCliente = mov.idCliente "; 
					str = str + " INNER JOIN formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento "; 
					str = str + " INNER JOIN duplicataparcela par on par.idDuplicataParcela = mov.idDuplicataParcela "; 
					str = str + " INNER JOIN duplicata dupli on dupli.idDuplicata = mov.idDuplicata ";
					str = str + " INNER JOIN pedidoVenda pv on pv.idPedidoVenda = dupli.idPedidoVenda ";
					str = str + " LEFT OUTER JOIN cupomFiscal cupom on cupom.idCupomFiscal = pv.idCupomFiscal ";
					str = str + " LEFT OUTER JOIN notaFiscal nfce ON nfce.idNotafiscal = (SELECT idNotaFiscal FROM notaFiscal WHERE idPedidoVenda = pv.idPedidoVenda AND tpAmb = 1 LIMIT 1) ";
					str = str + " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
					str = str + " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
					str = str + " group by ac.idDuplicataNova) "; 
					str = str + " as acordo on acordo.idDuplicataNova=dupli.idduplicata ";

					int x = 0;
					if(!dataSInicial.equals(""))
					{
						if(x==0)
						{
							str = str + " Where dataMovimento >= ' "+dataSInicial+" 00:00:00'"; 
							x++; 
						}
						else
						{
							str = str + " And dataMovimento >= ' "+dataSInicial+" 00:00:00'"; 
						}
					}

					if(x==0)
					{
						str = str + " where (par.numeroParcela <> 0 ) ";
						x++;
					}
					else
						str = str + " and (par.numeroParcela <> 0 ) ";

					if(!dataSFinal.equals(""))
					{
						if(x==0)
						{
							str = str + " Where dataMovimento <= ' "+dataSFinal+" 23:59:59'"; 
							x++;
						}
						else
						{
							str = str + " And dataMovimento <= ' "+dataSFinal+" 23:59:59'";	 
						} 
					}

					if(!request.getParameter("idEmpresaFisica").equals(""))
					{
						if(x==0)
						{
							str = str +" Where mov.idEmpresaFisica = " +empresa.getId();
							x++; 
						}
						else
						{
							str = str +" and mov.idEmpresaFisica = " +empresa.getId();
						}                		
					}

					if(request.getParameter("idCliente")!=null  && !request.getParameter("idCliente").equals(""))
					{
						if(x==0)
						{
							str = str +" where mov.idCliente = " + request.getParameter("idCliente") ;
							x++;
						}
						else
						{
							str = str +" and mov.idCliente = " + request.getParameter("idCliente") ;
						}
					}

					if(diasInicialAtraso!=null)
					{
						if(x==0)
						{
							str = str +" where DATEDIFF(mov.dataMovimento,par.dataVencimento) >= " + diasInicialAtraso ;
							x++;
						}
						else
						{
							str = str +" and DATEDIFF(mov.dataMovimento,par.dataVencimento) >= " + diasInicialAtraso ;
						}
					}

					if(diasFinalAtraso!=null)
					{
						if(x==0)
						{
							str = str +" where DATEDIFF(mov.dataMovimento,par.dataVencimento) <= " + diasFinalAtraso ;
							x++;
						}
						else
						{
							str = str +" and DATEDIFF(mov.dataMovimento,par.dataVencimento) <= " + diasFinalAtraso ;
						}
					}

					if(dataSqlEmissaoInicial != null)
					{
						if(x==0)
						{
							str += " where if((par.acordo<>'S' or par.acordo is null),dupli.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00',acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
							x++;
						}
						else
							str += " and if((par.acordo<>'S' or par.acordo is null),dupli.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00',acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00' ) ";
					}

					if(dataSqlEmissaoFinal != null)
					{
						if(x==0)
						{
							str += " where if((par.acordo<>'S' or par.acordo is null),dupli.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59') ";
							x++;
						}
						else
							str += " and if((par.acordo<>'S' or par.acordo is null),dupli.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59')";
					}
					str = str+ " AND (cupom.idCupomFiscal IS NOT NULL OR nfce.idNotaFiscal IS NOT NULL) ";
					str = str + " order by mov.idmovimentoparceladuplicata";
				} // FIM UAE
				System.out.println(str);
				ResultSet rs = rec.executeQuery(str);
				JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);

				String sqlSub = "";

				if(uae)
				{
					sqlSub = sqlSub + " select par.acordo,dup.numeroDuplicata,f.descricao,Round(sum(m.valorRecebido),2) as valor ";
					sqlSub = sqlSub + " from movimentoparceladuplicata m ";
					sqlSub = sqlSub + " LEFT OUTER JOIN movimentoCaixaDia movCaixa on movCaixa.idMovimentoparcelaDuplicata = m.idMovimentoParcelaDuplicata ";
					sqlSub = sqlSub + " INNER JOIN caixaDia caixa on caixa.idCaixadia = movCaixa.idCaixaDia ";
					sqlSub = sqlSub + " INNER JOIN duplicata dup on dup.idDuplicata = m.idduplicata ";
					sqlSub = sqlSub + " INNER JOIN formapagamento f on f.idformapagamento = m.idFormaPagamento "; 
					sqlSub = sqlSub + " INNER JOIN duplicataparcela par on par.idduplicataparcela = m.idduplicataparcela";
					sqlSub = sqlSub + " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
					sqlSub = sqlSub + " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
					sqlSub = sqlSub + " group by ac.idDuplicataNova) "; 
					sqlSub = sqlSub + " as acordo on acordo.idDuplicataNova=dup.idduplicata ";
					sqlSub = sqlSub + " where dataAbertura >='"+dataSInicial+" 00:00:00'"; 
					sqlSub = sqlSub + " and dataAbertura <='"+dataSFinal+" 23:59:59'"; 
					if(!request.getParameter("idEmpresaFisica").equals(""))
						sqlSub = sqlSub + " and m.idEmpresaFisica in("+empresa.getId()+") "; 
					sqlSub = sqlSub + " and (m.numeroParcela <> 0 )";
					if(diasInicialAtraso!=null)
						sqlSub = sqlSub +" and DATEDIFF(m.dataMovimento,par.dataVencimento) >= " + diasInicialAtraso ;
					if(diasFinalAtraso!=null)
						sqlSub = sqlSub +" and DATEDIFF(m.dataMovimento,par.dataVencimento) <= " + diasFinalAtraso ;
					if(dataSqlEmissaoInicial != null)
						sqlSub += " and if((par.acordo<>'S' or par.acordo is null),dup.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00',acordo.dataCompra >='"+dataSqlEmissaoInicial+" 00:00:00' ) ";
					if(dataSqlEmissaoFinal != null)
						sqlSub += " and if((par.acordo<>'S' or par.acordo is null),dup.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59') ";

					sqlSub = sqlSub + " group by f.idFormaPagamento "; 
					sqlSub = sqlSub + " order by f.idFormaPagamento "; 
				}
				else
				{
					sqlSub = sqlSub + " select par.acordo, dup.numeroDuplicata, f.descricao,Round(sum(m.valorRecebido),2) as valor ";
					sqlSub = sqlSub + " from movimentoparceladuplicata m ";
				//	sqlSub = sqlSub + " LEFT OUTER JOIN movimentoCaixaDia movCaixa on movCaixa.idMovimentoparcelaDuplicata = m.idMovimentoParcelaDuplicata ";
					//sqlSub = sqlSub + " INNER JOIN caixaDia caixa on caixa.idCaixadia = movCaixa.idCaixaDia ";
					sqlSub = sqlSub + " inner join duplicata dup on dup.idDuplicata = m.idDuplicata ";
					sqlSub = sqlSub + " inner join pedidovenda pv on pv.idPedidoVenda = dup.idPedidoVenda ";
					sqlSub = sqlSub + " LEFT OUTER JOIN cupomFiscal cupom on cupom.idCupomFiscal = pv.idCupomFiscal ";
					sqlSub = sqlSub + " LEFT OUTER JOIN notaFiscal nfce ON nfce.idNotafiscal = (SELECT idNotaFiscal FROM notaFiscal WHERE idPedidoVenda = pv.idPedidoVenda AND tpAmb = 1 LIMIT 1) ";
					sqlSub = sqlSub + " inner join formapagamento f on f.idformapagamento = m.idFormaPagamento "; 
					sqlSub = sqlSub + " inner join duplicataparcela par on par.idduplicataparcela = m.idduplicataparcela";
					sqlSub = sqlSub + " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
					sqlSub = sqlSub + " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
					sqlSub = sqlSub + " group by ac.idDuplicataNova) "; 
					sqlSub = sqlSub + " as acordo on acordo.idDuplicataNova=dup.idduplicata ";
					sqlSub = sqlSub + " where dataMovimento >='"+dataSInicial+" 00:00:00'"; 
					sqlSub = sqlSub + " and dataMovimento <='"+dataSFinal+" 23:59:59'"; 
					sqlSub = sqlSub + " AND (cupom.idCupomFiscal IS NOT NULL OR nfce.idNotaFiscal IS NOT NULL) ";

					if(!request.getParameter("idEmpresaFisica").equals(""))
						sqlSub = sqlSub + " and m.idEmpresaFisica in("+empresa.getId()+") "; 
					sqlSub = sqlSub + " and (m.numeroParcela <> 0 )";
					if(diasInicialAtraso!=null)
						sqlSub = sqlSub +" and DATEDIFF(m.dataMovimento,par.dataVencimento) >= " + diasInicialAtraso ;
					if(diasFinalAtraso!=null)
						sqlSub = sqlSub +" and DATEDIFF(m.dataMovimento,par.dataVencimento) <= " + diasFinalAtraso ;
					if(dataSqlEmissaoInicial != null)
						sqlSub += " and if((par.acordo<>'S' or par.acordo is null),dup.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00',acordo.dataCompra >='"+dataSqlEmissaoInicial+" 00:00:00' ) ";
					if(dataSqlEmissaoFinal != null)
						sqlSub += " and if((par.acordo<>'S' or par.acordo is null),dup.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59') ";

					sqlSub = sqlSub + " group by f.idFormaPagamento "; 
					sqlSub = sqlSub + " order by f.idFormaPagamento "; 
				}

				conn = ConexaoUtil.getConexaoPadrao();  

				stm = conn.createStatement();
				ResultSet rsResumo = stm.executeQuery(sqlSub); 
				JRResultSetDataSource DSR = new JRResultSetDataSource(rsResumo);


				Map<String, Object> params = new HashMap<String, Object>();                  
				params.put("logo", logo);                  

				URL urlSub = getClass().getResource("/relatorios/");
				params.put("SUBREPORT_DIR", urlSub.getPath()); 
				params.put("REPORT_CONNECTION", conn); 
				params.put("HABILITAR_DATA", "s");
				if(!request.getParameter("idEmpresaFisica").equals(""))
					params.put("empresaDescricao",""+empresa.getId()+" - "+empresa.getRazaoSocial());
				params.put("dataInicial", request.getParameter("dataInicial"));
				params.put("dataFinal", request.getParameter("dataFinal"));
				params.put( "DSR",DSR);
				params.put("opcao", soAcordo.equals("S")?"SOMENTE ACORDO":comAcordo.equals("S")?"PARCELAS E ACORDOS":"SOMENTE PARCELAS");

				String strSub = "";
				if(comAcordo.equals("S"))
				{
					if(uae)
					{
						strSub = strSub + " SELECT par.*,concat(cli.idCliente,'-',cli.nome),DATEDIFF(dataBaixa,dataVencimento),forma.descricao,dataBaixa,usu.nome as usuario, ";
						strSub = strSub + " @parcial :=(select sum(valorrecebido) from  movimentoparceladuplicata m where m.idDuplicataParcela = par.idDuplicataParcela and tipopagamento = 2), "; 
						strSub = strSub + " par.valorParcela - if(@parcial is null,0.0,@parcial) as valor , "; 
						strSub = strSub + " par.valorPago - if(@parcial is null,0.0,@parcial) as recebido "; 
						strSub = strSub + " FROM duplicataparcela par "; 
						strSub = strSub + " INNER JOIN duplicata dup on dup.idDuplicata = par.idDuplicata ";
						strSub = strSub + " INNER JOIN cliente cli on cli.idcliente = par.idcliente "; 
						strSub = strSub + " left outer JOIN usuario usu on usu.idUsuario = par.idUsuario";
						strSub = strSub + " INNER JOIN formapagamento forma on forma.idformapagamento = par.idformapagamento "; 
						strSub = strSub + " where tipopagamento = 5 "; 
						strSub = strSub + " and par.situacao = 3 "; 
						strSub = strSub + " and forma.idformapagamento =  "+ DsvConstante.getParametrosSistema().get("idFormaPagamentoAcordo");

						if(request.getParameter("diasInicialAtraso")!=null && !request.getParameter("diasInicialAtraso").equals("0"))
							strSub = strSub + " and DATEDIFF(dataBaixa,dataVencimento)>= "+request.getParameter("diasInicialAtraso"); 
						if(request.getParameter("diasFinalAtraso")!=null && !request.getParameter("diasFinalAtraso").equals("0"))
							strSub = strSub + " and DATEDIFF(dataBaixa,dataVencimento)<= "+request.getParameter("diasFinalAtraso"); 
						if(!dataSInicial.equals(""))
							strSub = strSub + " And dataBaixa >= ' "+dataSInicial+" 00:00:00'"; 
						if(!dataSFinal.equals(""))
							strSub = strSub + " And dataBaixa <= ' "+dataSFinal+" 23:59:59'";	 
						if(request.getParameter("idCliente")!=null  && !request.getParameter("idCliente").equals(""))
							strSub = strSub +" and par.idCliente = " + request.getParameter("idCliente") ;
						if(!request.getParameter("idEmpresaFisica").equals(""))
							strSub = strSub +" and par.idEmpresaFisica = " +empresa.getId();
						if(diasInicialAtraso!=null)
							strSub = strSub +" and DATEDIFF(par.dataBaixa,par.dataVencimento) >= " + diasInicialAtraso ;
						if(diasFinalAtraso!=null)
							strSub = strSub +" and DATEDIFF(par.dataBaixa,par.dataVencimento) <= " + diasFinalAtraso ;
						if(dataSqlEmissaoInicial != null)
							strSub += " and dup.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00' ";
						if(dataSqlEmissaoFinal != null)
							strSub += " and dup.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59' ";
					}
					else
					{
						strSub = strSub + " SELECT par.*,concat(cli.idCliente,'-',cli.nome),DATEDIFF(dataBaixa,dataVencimento),forma.descricao,dataBaixa,usu.nome as usuario, ";
						strSub = strSub + " @parcial :=(select sum(valorrecebido) from  movimentoparceladuplicata m where m.idDuplicataParcela = par.idDuplicataParcela and tipopagamento = 2), "; 
						strSub = strSub + " par.valorParcela - if(@parcial is null,0.0,@parcial) as valor , "; 
						strSub = strSub + " par.valorPago - if(@parcial is null,0.0,@parcial) as recebido "; 
						strSub = strSub + " FROM duplicataparcela par "; 
						strSub = strSub + " INNER JOIN duplicata dup on dup.idDuplicata = par.idDuplicata ";
						strSub = strSub + " INNER JOIN pedidoVenda pv on pv.idPedidoVenda = dup.idPedidoVenda ";
						strSub = strSub + " LEFT OUTER JOIN cupomFiscal cupom on cupom.idCupomFiscal = pv.idCupomFiscal ";
						strSub = strSub + " LEFT OUTER JOIN notaFiscal nfce ON nfce.idNotafiscal = (SELECT idNotaFiscal FROM notaFiscal WHERE idPedidoVenda = pv.idPedidoVenda AND tpAmb = 1 LIMIT 1) ";
						strSub = strSub + " INNER JOIN cliente cli on cli.idcliente = par.idcliente "; 
						strSub = strSub + " left outer JOIN usuario usu on usu.idUsuario = par.idUsuario";
						strSub = strSub + " INNER JOIN formapagamento forma on forma.idformapagamento = par.idformapagamento "; 
						strSub = strSub + " where tipopagamento = 5 "; 
						strSub = strSub + " and par.situacao = 3 "; 
						strSub = strSub + " and forma.idformapagamento =  "+ DsvConstante.getParametrosSistema().get("idFormaPagamentoAcordo");
						strSub = strSub + " AND (cupom.idCupomFiscal IS NOT NULL OR nfce.idNotaFiscal IS NOT NULL) ";

						if(!dataSInicial.equals(""))
							strSub = strSub + " And dataBaixa >= ' "+dataSInicial+" 00:00:00'"; 
						if(!dataSFinal.equals(""))
							strSub = strSub + " And dataBaixa <= ' "+dataSFinal+" 23:59:59'";	 
						if(request.getParameter("idCliente")!=null  && !request.getParameter("idCliente").equals(""))
							strSub = strSub +" and par.idCliente = " + request.getParameter("idCliente") ;
						if(!request.getParameter("idEmpresaFisica").equals(""))
							strSub = strSub +" and par.idEmpresaFisica = " +empresa.getId();
						if(diasInicialAtraso!=null)
							strSub = strSub +" and DATEDIFF(par.dataBaixa,par.dataVencimento) >= " + diasInicialAtraso ;
						if(diasFinalAtraso!=null)
							strSub = strSub +" and DATEDIFF(par.dataBaixa,par.dataVencimento) <= " + diasFinalAtraso ;
						if(dataSqlEmissaoInicial != null)
							strSub += " and dup.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00' ";
						if(dataSqlEmissaoFinal != null)
							strSub += " and dup.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59' ";
					}

					rec = conn.createStatement();
					ResultSet rsSub = rec.executeQuery(strSub);
					Double totalJurosAcordo =0.0;
					Double totalDescontoAcordo =0.0;
					Double totalPagoAcordo =0.0;
					Double totalParcelaAcordo =0.0;
					while(rsSub.next())
					{
						totalJurosAcordo += rsSub.getDouble("jurosPago");
						totalDescontoAcordo += rsSub.getDouble("valorDesconto");
						totalPagoAcordo += rsSub.getDouble("recebido");
						totalParcelaAcordo += rsSub.getDouble("valor");
					}
					params.put("totalJurosAcordo",totalJurosAcordo );
					params.put("totalDescontoAcordo",totalDescontoAcordo );
					params.put("totalPagoAcordo",totalPagoAcordo );
					params.put("totalParcelaAcordo",totalParcelaAcordo);
					params.put("mostarTotal","N");   

					rsSub.beforeFirst();

					JRResultSetDataSource dsSub = new JRResultSetDataSource(rsSub);
					params.put("DataResouceSub", dsSub);

				}

				impressao = JasperFillManager.fillReport(rel, params,jrRS);	
			}
			else
			{
				String strSub = "";
				if(uae)
				{
					strSub = strSub + " SELECT par.*,concat(cli.idCliente,'-',cli.nome),DATEDIFF(dataBaixa,dataVencimento),forma.descricao,dataBaixa,usu.nome as usuario, "; 
					strSub = strSub + " @parcial :=(select sum(valorrecebido) from  movimentoparceladuplicata m where m.idDuplicataParcela = par.idDuplicataParcela and tipopagamento = 2), "; 
					strSub = strSub + " par.valorParcela - if(@parcial is null,0.0,@parcial) as valor , "; 
					strSub = strSub + " par.valorPago - if(@parcial is null,0.0,@parcial) as recebido "; 
					strSub = strSub + " FROM duplicataparcela par "; 
					strSub = strSub + " INNER JOIN duplicata dup on dup.idDuplicata = par.idDuplicata ";
					strSub = strSub + " INNER JOIN cliente cli on cli.idcliente = par.idcliente "; 
					strSub = strSub + " left outer JOIN usuario usu on usu.idUsuario = par.idUsuario";
					strSub = strSub + " INNER JOIN formapagamento forma on forma.idformapagamento = par.idformapagamento "; 
					strSub = strSub + " where tipopagamento = 5 "; 
					strSub = strSub + " and par.situacao = 3 "; 
					strSub = strSub + " and forma.idformapagamento =  "+ DsvConstante.getParametrosSistema().get("idFormaPagamentoAcordo");

					if(!dataSInicial.equals(""))
						strSub = strSub + " And dataBaixa >= '"+dataSInicial+" 00:00:00'"; 
					if(!dataSFinal.equals(""))
						strSub = strSub + " And dataBaixa <= '"+dataSFinal+" 23:59:59'";	 
					if(request.getParameter("idCliente")!=null  && !request.getParameter("idCliente").equals(""))
						strSub = strSub +" and par.idCliente = " + request.getParameter("idCliente") ;
					if(!request.getParameter("idEmpresaFisica").equals(""))
						strSub = strSub +" and Par.idEmpresaFisica = " +empresa.getId();
					if(diasInicialAtraso!=null)
						strSub = strSub +" and DATEDIFF(par.dataBaixa,par.dataVencimento) >= " + diasInicialAtraso ;
					if(diasFinalAtraso!=null)
						strSub = strSub +" and DATEDIFF(par.dataBaixa,par.dataVencimento) <= " + diasFinalAtraso ;
					if(dataSqlEmissaoInicial != null)
						strSub += " and dup.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00' ";
					if(dataSqlEmissaoFinal != null)
						strSub += " and dup.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59' ";
				}
				else
				{
					strSub = strSub + " SELECT par.*,concat(cli.idCliente,'-',cli.nome),DATEDIFF(dataBaixa,dataVencimento),forma.descricao,dataBaixa,usu.nome as usuario, "; 
					strSub = strSub + " @parcial :=(select sum(valorrecebido) from  movimentoparceladuplicata m where m.idDuplicataParcela = par.idDuplicataParcela and tipopagamento = 2), "; 
					strSub = strSub + " par.valorParcela - if(@parcial is null,0.0,@parcial) as valor , "; 
					strSub = strSub + " par.valorPago - if(@parcial is null,0.0,@parcial) as recebido "; 
					strSub = strSub + " FROM duplicataparcela par "; 
					strSub = strSub + " INNER JOIN duplicata dup on dup.idDuplicata = par.idDuplicata ";
					strSub = strSub + " INNER JOIN pedidovenda pv on pv.idPedidoVenda = dup.idPedidoVenda ";
					strSub = strSub + " LEFT OUTER JOIN cupomFiscal cupom on cupom.idCupomFiscal = pv.idCupomFiscal ";
					strSub = strSub + " LEFT OUTER JOIN notaFiscal nfce ON nfce.idNotafiscal = (SELECT idNotaFiscal FROM notaFiscal WHERE idPedidoVenda = pv.idPedidoVenda AND tpAmb = 1 LIMIT 1) ";
					strSub = strSub + " INNER JOIN cliente cli on cli.idcliente = par.idcliente "; 
					strSub = strSub + " left outer JOIN usuario usu on usu.idUsuario = par.idUsuario";
					strSub = strSub + " INNER JOIN formapagamento forma on forma.idformapagamento = par.idformapagamento "; 
					strSub = strSub + " where tipopagamento = 5 "; 
					strSub = strSub + " and par.situacao = 3 "; 
					strSub = strSub + " and forma.idformapagamento =  "+ DsvConstante.getParametrosSistema().get("idFormaPagamentoAcordo");
					strSub = strSub + " AND (cupom.idCupomFiscal IS NOT NULL OR nfce.idNotaFiscal IS NOT NULL) ";

					if(!dataSInicial.equals(""))
						strSub = strSub + " And dataBaixa >= '"+dataSInicial+" 00:00:00'"; 
					if(!dataSFinal.equals(""))
						strSub = strSub + " And dataBaixa <= '"+dataSFinal+" 23:59:59'";	 
					if(request.getParameter("idCliente")!=null  && !request.getParameter("idCliente").equals(""))
						strSub = strSub +" and par.idCliente = " + request.getParameter("idCliente") ;
					if(!request.getParameter("idEmpresaFisica").equals(""))
						strSub = strSub +" and Par.idEmpresaFisica = " +empresa.getId();
					if(diasInicialAtraso!=null)
						strSub = strSub +" and DATEDIFF(par.dataBaixa,par.dataVencimento) >= " + diasInicialAtraso ;
					if(diasFinalAtraso!=null)
						strSub = strSub +" and DATEDIFF(par.dataBaixa,par.dataVencimento) <= " + diasFinalAtraso ;
					if(dataSqlEmissaoInicial != null)
						str += " and dup.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00' ";
					if(dataSqlEmissaoFinal != null)
						str += " and dup.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59' ";

				}

				ResultSet rsSub = rec.executeQuery(strSub);
				Double totalJurosAcordo =0.0;
				Double totalDescontoAcordo =0.0;
				Double totalPagoAcordo =0.0;
				Double totalParcelaAcordo =0.0;

				while(rsSub.next())
				{
					totalJurosAcordo += rsSub.getDouble("jurosPago");
					totalDescontoAcordo += rsSub.getDouble("valorDesconto");
					totalPagoAcordo += rsSub.getDouble("valorPago");
					totalParcelaAcordo += rsSub.getDouble("valorParcela");
				}
				rsSub.beforeFirst();
				Map<String, Object> params = new HashMap<String, Object>();

				params.put("totalJurosAcordo",totalJurosAcordo );
				params.put("totalDescontoAcordo",totalDescontoAcordo );
				params.put("totalPagoAcordo",totalPagoAcordo );
				params.put("totalParcelaAcordo",totalParcelaAcordo);
				params.put("mostarTotal","S");                  
				params.put("logo", logo);                  

				URL urlSub = getClass().getResource("/relatorios/");
				params.put("SUBREPORT_DIR", urlSub.getPath()); 
				params.put("REPORT_CONNECTION", conn);
				params.put("HABILITAR_DATA", "s");
				if(!request.getParameter("idEmpresaFisica").equals(""))
					params.put("empresaDescricao",""+empresa.getId()+" - "+empresa.getRazaoSocial());
				params.put("dataInicial", request.getParameter("dataInicial"));
				params.put("dataFinal", request.getParameter("dataFinal"));
				params.put("mostarTitulo", "S");
				params.put("opcao", soAcordo.equals("S")?"SOMENTE ACORDO":comAcordo.equals("S")?"PARCELAS E ACORDOS":"SOMENTE PARCELAS");
				while(rsSub.next())
				{
					totalJurosAcordo += rsSub.getDouble("jurosPago");
					totalDescontoAcordo += rsSub.getDouble("valorDesconto");
					totalPagoAcordo += rsSub.getDouble("valorPago");
					totalParcelaAcordo += rsSub.getDouble("valorParcela");
				}
				rsSub.beforeFirst();

				JRResultSetDataSource dsSub = new JRResultSetDataSource(rsSub);
				params.put("DataResouceSub", dsSub);
				impressao = JasperFillManager.fillReport(rel, params,dsSub); 
			}

			bytes = JasperExportManager.exportReportToPdf(impressao); 

		}
		catch (JRException | SQLException e) 
		{
			throw new ServletException(e);  
		}  

		if (bytes != null && bytes.length > 0) 
		{
			response.setContentType("application/pdf");  
			response.setContentLength(bytes.length);  
			ServletOutputStream ouputStream = response.getOutputStream();  
			ouputStream.write(bytes, 0, bytes.length);  
			ouputStream.flush();  
			ouputStream.close();  
		}

		if(conn != null && !conn.isClosed())
			conn.close();
	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Parcelas a Pagar")
	public void gerarParcelasAPagar() throws NumberFormatException, Exception
	{
		byte[] bytes = null;  

		try 
		{
			conn = ConexaoUtil.getConexaoPadrao();    
			boolean cupom = new UsuarioService().getSessionUae(request);

			caminho = "relatorioDuplicataParcelasAPagar.jasper"; 

			URL urlArquivo = getClass().getResource("/relatorios/"+caminho);
			String rel = urlArquivo.getPath();
			Image logo = null;
			InputStream inputStreamDaImagem = null;   
			URL urlImagens = getClass().getResource("/imagens/logo.jpg");
			String caminhoImagem = urlImagens.getPath();//recebe o caminho da imagem  
			try {    
				File file = new File(caminhoImagem);   

				if(file.exists())//testa se imagem existe  
					inputStreamDaImagem = new BufferedInputStream(
							new FileInputStream(caminhoImagem));

				logo = ImageIO.read(inputStreamDaImagem);
			} catch (FileNotFoundException e) {       
				e.printStackTrace();       
			}   

			EmpresaFisica empresa = null;
			if(request.getParameter("idEmpresaFisica") != null && !request.getParameter("idEmpresaFisica").equals(""))
			{
				empresa  = new EmpresaFisicaService().recuperarPorId(Integer.parseInt(request.getParameter("idEmpresaFisica"))); 
			}

			SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
			SimpleDateFormat dfa = new SimpleDateFormat ("dd/MM/yyyy");
			Date dFinal = null;
			Date dInicial = null;
			String dataSInicial = "";
			String dataSFinal = "";
			if(request.getParameter("dataInicial")!=null && !request.getParameter("dataInicial").equals(""))
			{
				dInicial = dfa.parse(request.getParameter("dataInicial"));
				dataSInicial =  df.format(dInicial);
			}
			if(request.getParameter("dataFinal")!=null && !request.getParameter("dataFinal").equals(""))
			{
				dFinal =  dfa.parse(request.getParameter("dataFinal"));
				dataSFinal =  df.format(dFinal);
			}
			Integer idCliente = null;
			if(request.getParameter("idCliente")!=null  && !request.getParameter("idCliente").equals(""))
			{
				idCliente  = Integer.parseInt(request.getParameter("idCliente"));
			}
			JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(cupom==true?this.retornarListaAPagar(idCliente, dataSInicial, dataSFinal):null);     
			Map<String, Object> params = new HashMap<String, Object>();
			URL urlSub = getClass().getResource("/relatorios/");

			params.put("SUBREPORT_DIR", urlSub.getPath()); 
			params.put("REPORT_CONNECTION", conn);
			params.put("HABILITAR_DATA", "s");
			params.put("empresaDescricao",""+empresa.getId()+" - "+empresa.getRazaoSocial());
			params.put("dataInicial", request.getParameter("dataInicial"));
			params.put("dataFinal", request.getParameter("dataFinal"));
			params.put("logo",logo);

			JasperPrint impressao = JasperFillManager.fillReport(rel, params,ds);            

			bytes = JasperExportManager.exportReportToPdf(impressao);  
		}
		catch (JRException | SQLException e) 
		{
			throw new ServletException(e);  
		}  

		if (bytes != null && bytes.length > 0) 
		{ 
			response.setContentType("application/pdf");  
			response.setContentLength(bytes.length);  
			ServletOutputStream ouputStream = response.getOutputStream();  
			ouputStream.write(bytes, 0, bytes.length);  
			ouputStream.flush();  
			ouputStream.close();  
		}   

		if(conn != null && !conn.isClosed())
			conn.close();
	}

	private List<ParcelaAPagar> retornarListaAPagar(Integer idCliente,String dataSInicial,String dataSFinal) throws Exception
	{
		String str = "";
		str = str + " SELECT par.idduplicataparcela as id "; 
		str = str + "  FROM duplicataparcela par "; 
		str = str + " Inner join cliente cli on cli.idCliente = par.idCliente "; 
		str = str + " where par.situacao in ('0','2') ";
		if(idCliente != null && idCliente != 0)
		{
			str = str + " and par.idcliente =" + idCliente;
		}
		if(!dataSInicial.equals("") && dataSInicial != null )
		{
			str = str + "	and par.dataVencimento >= '"+ dataSInicial+"'";

		}
		if(!dataSFinal.equals("") && dataSInicial != null)
		{
			str = str + "	and par.dataVencimento <= '"+dataSFinal+"' "; 
		}
		str = str + " order by cli.nome";

		EntityManager manager = ConexaoUtil.getEntityManager();
		Connection conn = ConexaoUtil.getConexaoPadrao();        
		java.sql.Statement rec = conn.createStatement();       
		ResultSet rs = rec.executeQuery(str); 

		List<DuplicataParcela> listaDupli = new ArrayList<>();

		while(rs.next())
		{
			listaDupli.add(new DuplicataParcelaService().recuperarPorId(manager, rs.getInt("id")));
		}

		List<ParcelaAPagar> lista = retornarListaDuplicataComJuros(listaDupli);

		if(conn != null && !conn.isClosed())
			conn.close();
		if(manager != null && manager.isOpen())
			manager.close();

		return lista;
	}

	private List<ParcelaAPagar> retornarListaDuplicataComJuros(List<DuplicataParcela> listaDupli)
	{

		List<ParcelaAPagar> listaPagar = new ArrayList<>();
		DuplicataParcela par = new DuplicataParcela();
		for (int i = 0; i < listaDupli.size(); i++) 
		{
			DuplicataParcelaService service =  new DuplicataParcelaService();
			ParcelaAPagar pag = new ParcelaAPagar();
			par = listaDupli.get(i);
			pag.setCliente(""+par.getCliente().getId()+" - " +par.getCliente().getNome());
			pag.setDataBaixa((par.getDataBaixa()==null?null:par.getDataBaixa()));
			pag.setDataVencimento(par.getDataVencimento());
			pag.setDesconto(par.getValorDesconto()==null?0.0:par.getValorDesconto());
			pag.setNumeroParcela(par.getNumeroParcela());
			pag.setSituacao(par.getSituacao());
			pag.setValorParcela(par.getValorParcela());
			Double valorAPagar = service.retornaValorParcelaComJuros(par);
			pag.setValorTotal(valorAPagar);
			pag.setDiasAtrazo(service.retornoDiasAtrazoParcela(par));
			pag.setValorPago(par.getValorPago()==null?0.0:par.getValorPago());
			if(par.getSituacao().equals('0'))
			{
				pag.setJuros(0.0);
			}
			else
			{
				pag.setJuros(service.retornaValorParcelaComJuros(par) - service.retornaValorParcelaSemJuros(par));
			}

			listaPagar.add(pag);
		}
		return listaPagar;

	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Duplicatas Geradas")
	public void geraDuplicatasGerardas() throws NumberFormatException, Exception
	{

		byte[] bytes = null;  

		try 
		{   	 	        
			conn = ConexaoUtil.getConexaoPadrao();  
			caminho = "relatorioCrediario.jasper"; 

			URL urlArquivo = getClass().getResource("/relatorios/"+caminho);
			String rel = urlArquivo.getPath();
			Image logo = null;
			InputStream inputStreamDaImagem = null;   
			URL urlImagens = getClass().getResource("/imagens/logo.jpg");
			String caminhoImagem = urlImagens.getPath();
			try {    
				File file = new File(caminhoImagem);   

				if(file.exists())
					inputStreamDaImagem = new BufferedInputStream(
							new FileInputStream(caminhoImagem));

				logo = ImageIO.read(inputStreamDaImagem);
			} catch (FileNotFoundException e) {       
				e.printStackTrace();       
			}

			String str = "";

			SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
			SimpleDateFormat dfa = new SimpleDateFormat ("dd/MM/yyyy");
			Date dFinal = null;
			Date dInicial = null;
			String dataSInicial = "";
			String dataSFinal = "";
			if(request.getParameter("dataInicial")!=null && !request.getParameter("dataInicial").equals(""))
			{
				dInicial = dfa.parse(request.getParameter("dataInicial"));
				dataSInicial =  df.format(dInicial);
			}
			if(request.getParameter("dataFinal")!=null && !request.getParameter("dataFinal").equals(""))
			{
				dFinal =  dfa.parse(request.getParameter("dataFinal"));
				dataSFinal =  df.format(dFinal); 
			}

			Boolean uae = new UsuarioService().getSessionUae(request);

			if(uae)
			{
				str = str + " SELECT du.idDuplicata,pp.idTipoPagamento, if(du.valorEntrada is null,0.0,du.valorEntrada) as valorEntrada,du.valorTotalCompra ,par.numeroDuplicata, "; 
				str = str + " if (length(concat(coalesce(ven.codigovendedorcache,ven.idvendedor),' - ',ven.nome ))>19,  substr(concat(coalesce(ven.codigovendedorcache,ven.idvendedor),' - ',ven.nome ),1,19),  concat(coalesce(ven.codigovendedorcache,ven.idvendedor),' - ',ven.nome )) as vendedor ,du.dataHoraInclusao,count(par.idDuplicataParcela) as qtparcelas "; 
				str = str + " ,concat(cli.idCliente,' - ',cli.nome ) as 'cliente',par.acordo,if(par.acordo='S',du.valorTotalCompra,0.0) as valorAcordo, ";
				str = str + " if((par.acordo<>'S' or par.acordo is null) and pp.idTipoPagamento <> 5,du.valorTotalCompra,if(par.acordo is null ,du.valorTotalCompra,0.0)) as valorNormal, ";
				str = str + " IF(pp.idtipopagamento =5,du.valorTotalCompra,0) AS valorVale,du.valorTotalCompra, "; 
				str = str + " IF(du.semPedidoVenda = 'S',du.valorTotalCompra,0) AS valorImplantado, du.semPedidoVenda,if(pp.IdPlanoPagamento is null,'Implantado', pp.descricao) as plano "; 
				str = str + " FROM duplicataparcela par "; 
				str = str + " inner join duplicata du on du.idDuplicata = par.idDuplicata "; 
				str = str + " left outer join pedidovenda pv on pv.idPedidoVenda = du.idPedidoVenda and pv.idEmpresaFisica = du.idEmpresaFisica ";
				str = str + " left outer join planopagamento pp on pv.IdPlanoPagamento = pp.idPlanoPagamento "; 
				str = str + " inner  join cliente cli on cli.idCliente = par.idCliente "; 
				str = str + " left outer join vendedor ven on if(du.idVendedor is null,ven.idVendedor = pv.idVendedor,ven.idVendedor = du.idVendedor) "; 
				int x =0;

				if(!dataSInicial.equals(""))
				{
					if(x==0)
					{
						str = str + " where du.dataHoraInclusao >= '" +dataSInicial+" 00:00:00' ";
						x++;
					}
					else
					{
						str = str + " and du.dataHoraInclusao >= '" +dataSInicial+" 00:00:00' ";
					}
				}

				if(!dataSFinal.equals(""))
				{
					if(x==0)
					{
						str = str + " where du.dataHoraInclusao <= '" +dataSFinal + " 23:59:59'";
						x++;
					}
					else
					{
						str = str + " and du.dataHoraInclusao <= '" +dataSFinal + " 23:59:59'";
					}
				}

				if(request.getParameter("idEmpresaFisica") != null && !request.getParameter("idEmpresaFisica").equals(""))
				{
					if(x==0)
					{
						str = str + " where par.idEmpresaFisica in (" +request.getParameter("idEmpresaFisica")+")";
						x++;
					}
					else
					{
						str = str + " and par.idEmpresaFisica in (" +request.getParameter("idEmpresaFisica")+")";
					}
				}
				str = str + " group by par.idDuplicata "; 
				str = str + " order by  du.valorTotalCompra asc";
			}
			else
			{
				str = str + " SELECT du.idDuplicata,pp.idTipoPagamento, if(du.valorEntrada is null,0.0,du.valorEntrada) as valorEntrada,du.valorTotalCompra ,par.numeroDuplicata, "; 
				str = str + " if (length(concat(coalesce(ven.codigovendedorcache,ven.idvendedor),' - ',ven.nome ))>19,  substr(concat(coalesce(ven.codigovendedorcache,ven.idvendedor),' - ',ven.nome ),1,19),  concat(coalesce(ven.codigovendedorcache,ven.idvendedor),' - ',ven.nome )) as vendedor ,du.dataHoraInclusao,count(par.idDuplicataParcela) as qtparcelas "; 
				str = str + " ,concat(cli.idCliente,' - ',cli.nome ) as 'cliente',par.acordo,if(par.acordo='S',du.valorTotalCompra,0.0) as valorAcordo, ";
				str = str + " if((par.acordo<>'S' or par.acordo is null) and pp.idTipoPagamento <> 5,du.valorTotalCompra,if(par.acordo is null ,du.valorTotalCompra,0.0)) as valorNormal, ";
				str = str + " IF(pp.idtipopagamento =5,du.valorTotalCompra,0) AS valorVale,du.valorTotalCompra, "; 
				str = str + " IF(du.semPedidoVenda = 'S',du.valorTotalCompra,0) AS valorImplantado, du.semPedidoVenda,if(pp.IdPlanoPagamento is null,'Implantado', pp.descricao) as plano "; 
				str = str + " FROM duplicataparcela par ";
				str = str + " inner join duplicata du on du.idDuplicata = par.idDuplicata "; 
				str = str + " left outer join pedidovenda pv on pv.idPedidoVenda = du.idPedidoVenda and pv.idEmpresaFisica = du.idEmpresaFisica ";
				str = str + " LEFT OUTER JOIN cupomfiscal cupom on cupom.idCupomFiscal = pv.idCupomFiscal ";
				str = str + " LEFT OUTER JOIN notaFiscal nfce ON nfce.idNotafiscal = (SELECT idNotaFiscal FROM notaFiscal WHERE idPedidoVenda = pv.idPedidoVenda AND tpAmb = 1 LIMIT 1) ";
				str = str + " left outer join planopagamento pp on pv.IdPlanoPagamento = pp.idPlanoPagamento "; 
				str = str + " inner  join cliente cli on cli.idCliente = par.idCliente "; 
				str = str + " left outer join vendedor ven on if(du.idVendedor is null,ven.idVendedor = pv.idVendedor,ven.idVendedor = du.idVendedor) "; 
				int x =0;

				if(!dataSInicial.equals(""))
				{
					if(x==0)
					{
						str = str + " where du.dataHoraInclusao >= '" +dataSInicial+" 00:00:00' ";
						x++;
					}
					else
					{
						str = str + " and du.dataHoraInclusao >= '" +dataSInicial+" 00:00:00' ";
					}
				}

				if(!dataSFinal.equals(""))
				{
					if(x==0)
					{
						str = str + " where du.dataHoraInclusao <= '" +dataSFinal + " 23:59:59'";
						x++;
					}
					else
					{
						str = str + " and du.dataHoraInclusao <= '" +dataSFinal + " 23:59:59'";
					}
				}

				if(request.getParameter("idEmpresaFisica") != null && !request.getParameter("idEmpresaFisica").equals(""))
				{
					if(x==0)
					{
						str = str + " where par.idEmpresaFisica in (" +request.getParameter("idEmpresaFisica")+")";
						x++;
					}
					else
					{
						str = str + " and par.idEmpresaFisica in (" +request.getParameter("idEmpresaFisica")+")";
					}
				}

				str = str + " AND (cupom.idCupomFiscal IS NOT NULL OR nfce.idNotaFiscal IS NOT NULL) ";

				str = str + " group by par.idDuplicata "; 
				str = str + " order by  du.valorTotalCompra asc";
			}

			java.sql.Statement rec = conn.createStatement();  

			ResultSet rs = rec.executeQuery(str);  

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("logo", logo); 

			JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);

			String strLog = "";

			if(uae)
			{
				strLog = strLog + " SELECT log.*,  "; 
				strLog = strLog + " if(log.nomeClasse = ' Duplicata',du.numeroDuplicata,if(log.nomeClasse = ' Duplicata Parcela',concat(dp.numeroDuplicata,'/',dp.numeroparcela),null)) as des "; 
				strLog = strLog + " FROM logduplicata log   "; 
				strLog = strLog + " left outer join duplicataparcela dp on dp.idDuplicataParcela = log.idDocumentoOrigem "; 
				strLog = strLog + " left outer join duplicata du on du.numeroDuplicata = log.idDocumentoOrigem  "; 
				strLog = strLog + "  where  (nomeClasse = ' Duplicata Parcela' or nomeClasse = ' Duplicata') "; 
				strLog = strLog + " and log.data >= '"+dataSInicial+" 00:00:00'";
				strLog = strLog + " and log.data <= '"+dataSFinal+" 23:59:59'";
				strLog = strLog + " and SUBSTRING(log.motivo, 1, 15) <> 'EXCLUIU PARCELA' AND SUBSTRING(log.motivo, 1, 9) <> 'ATUALIZAR' ";
				if(request.getParameter("idEmpresaFisica") != null && !request.getParameter("idEmpresaFisica").equals(""))
					strLog = strLog + " and log.idempresafisica in ("+request.getParameter("idEmpresaFisica")+")";
			}
			else
			{
				strLog = strLog + " SELECT log.*,  "; 
				strLog = strLog + " if(log.nomeClasse = ' Duplicata',du.numeroDuplicata,if(log.nomeClasse = ' Duplicata Parcela',concat(dp.numeroDuplicata,'/',dp.numeroparcela),null)) as des "; 
				strLog = strLog + " FROM logduplicata log   "; 
				strLog = strLog + " left outer join duplicataparcela dp on dp.idDuplicataParcela = log.idDocumentoOrigem ";
				strLog = strLog + " left outer join duplicata du on du.numeroDuplicata = log.idDocumentoOrigem  ";
				strLog = strLog + " inner join pedidovenda pv on pv.idpedidoVenda = du.idPedidoVenda ";
				strLog = strLog + " inner join cupomfiscal cupom on cupom.idCupomFiscal = pv.idCupomFiscal ";
				strLog = strLog + "  where  (nomeClasse = ' Duplicata Parcela' or nomeClasse = ' Duplicata') "; 
				strLog = strLog + " and log.data >= '"+dataSInicial+" 00:00:00'";
				strLog = strLog + " and log.data <= '"+dataSFinal+" 23:59:59'";
				strLog = strLog + " AND substring(log.motivo,1,15) <> 'EXCLUIU PARCELA' ";
				if(request.getParameter("idEmpresaFisica") != null && !request.getParameter("idEmpresaFisica").equals(""))
					strLog = strLog + " and log.idempresafisica in ("+request.getParameter("idEmpresaFisica")+")";
			}

			conn = ConexaoUtil.getConexaoPadrao();    
			Statement stmLog = conn.createStatement();

			ResultSet rsLog = stmLog.executeQuery(strLog);
			JRResultSetDataSource jrRSLogDS = new JRResultSetDataSource(rsLog);

			String sqlSub = "";

			if(uae)
			{
				sqlSub = sqlSub + " select f.descricao, Round(sum(mov.valor),2) as valor  "; 
				sqlSub = sqlSub + " from movimentocaixadia mov "; 
				sqlSub = sqlSub + " inner join caixadia cx on cx.idcaixadia = mov.idcaixadia "; 
				sqlSub = sqlSub + " inner join movimentoparceladuplicata mp on mp.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata and mp.idEmpresaFisica = cx.idEmpresaFisica "; 
				sqlSub = sqlSub + " inner join formapagamento f on f.idFormaPagamento = mov.idFormaPagamento "; 
				sqlSub = sqlSub + " where mp.numeroParcela = 0 "; 
				sqlSub = sqlSub + " and mov.creditoDebito <> 'D' "; 
				sqlSub = sqlSub + " and mov.dataVencimento >='"+dataSInicial+" 00:00:00'  "; 
				sqlSub = sqlSub + " and mov.dataVencimento <='"+dataSFinal+" 23:59:59' "; 
				if(request.getParameter("idEmpresaFisica") != null && !request.getParameter("idEmpresaFisica").equals(""))
					sqlSub = sqlSub + " and mp.idEmpresaFisica in("+request.getParameter("idEmpresaFisica")+")";
				sqlSub = sqlSub + " group by f.idFormaPagamento "; 
				sqlSub = sqlSub + " order by f.idFormaPagamento "; 	
			}
			else
			{
				sqlSub = sqlSub + " select f.descricao, Round(sum(mov.valor),2) as valor  "; 
				sqlSub = sqlSub + " from movimentocaixadia mov "; 
				sqlSub = sqlSub + " inner join caixadia cx on cx.idcaixadia = mov.idcaixadia "; 
				sqlSub = sqlSub + " inner join movimentoparceladuplicata mp on mp.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata and mp.idEmpresaFisica = cx.idEmpresaFisica ";
				sqlSub = sqlSub + " inner join duplicata dup on dup.idDuplicata = mp.idDuplicata ";
				sqlSub = sqlSub + " inner join pedidovenda pv on pv.idPedidoVenda = dup.idPedidoVenda ";
				sqlSub = sqlSub + " inner join cupomFiscal cupom on cupom.idCupomFiscal = pv.idCupomFiscal ";
				sqlSub = sqlSub + " inner join formapagamento f on f.idFormaPagamento = mov.idFormaPagamento "; 
				sqlSub = sqlSub + " where mp.numeroParcela = 0 "; 
				sqlSub = sqlSub + " and mov.creditoDebito <> 'D' "; 
				sqlSub = sqlSub + " and mov.dataVencimento >='"+dataSInicial+" 00:00:00'  "; 
				sqlSub = sqlSub + " and mov.dataVencimento <='"+dataSFinal+" 23:59:59' "; 
				if(request.getParameter("idEmpresaFisica") != null && !request.getParameter("idEmpresaFisica").equals(""))
					sqlSub = sqlSub + " and mp.idEmpresaFisica in("+request.getParameter("idEmpresaFisica")+")";
				sqlSub = sqlSub + " group by f.idFormaPagamento "; 
				sqlSub = sqlSub + " order by f.idFormaPagamento "; 	
			}


			conn = ConexaoUtil.getConexaoPadrao();    
			Statement stmSub = conn.createStatement();

			ResultSet rsSub= stmSub.executeQuery(sqlSub);
			JRResultSetDataSource DSSub = new JRResultSetDataSource(rsSub);
			String strCred = "";
			if(uae)
			{
				strCred = strCred + " select concat(u.idUsuario,' - ',u.nome) as Crediarista, ";
				strCred = strCred + " count(d.idduplicata) as qnt, ";
				strCred = strCred + " Round(sum(valorTotalCompra),2) as valorTotal, ";
				strCred = strCred + " Round(sum(valorEntrada),2) as valorEntrada ";
				strCred = strCred + " from duplicata d ";
				strCred = strCred + " inner join usuario u on u.idusuario = d.idusuario ";
				strCred = strCred + " where d.dataHoraInclusao >= '"+dataSInicial+" 00:00:00' ";
				strCred = strCred + " and d.dataHoraInclusao <= '"+dataSFinal+" 23:59:59' ";

				if(request.getParameter("idEmpresaFisica") != null && !request.getParameter("idEmpresaFisica").equals(""))
					str = str + " and d.idEmpresaFisica in (" +request.getParameter("idEmpresaFisica")+")";

				strCred = strCred + " group by u.idusuario ";
			}
			else
			{
				strCred = strCred + " select concat(u.idUsuario,' - ',u.nome) as Crediarista, ";
				strCred = strCred + " count(d.idduplicata) as qnt, ";
				strCred = strCred + " Round(sum(valorTotalCompra),2) as valorTotal, ";
				strCred = strCred + " Round(sum(if(valorEntrada is null,0.0,valorEntrada)),2) as valorEntrada ";
				strCred = strCred + " from duplicata d ";
				strCred = strCred + " inner join usuario u on u.idusuario = d.idusuario ";
				strCred = strCred + " inner join pedidovenda pv on pv.idPedidoVenda = d.idPedidoVenda ";
				strCred = strCred + " inner join cupomFiscal cupom on cupom.idCupomFiscal = pv.idCupomFiscal ";
				strCred = strCred + " where d.dataHoraInclusao >= '"+dataSInicial+" 00:00:00' ";
				strCred = strCred + " and d.dataHoraInclusao <= '"+dataSFinal+" 23:59:59' ";

				if(request.getParameter("idEmpresaFisica") != null && !request.getParameter("idEmpresaFisica").equals(""))
					str = str + " and d.idEmpresaFisica in (" +request.getParameter("idEmpresaFisica")+")";

				strCred = strCred + " group by u.idusuario ";
			}

			conn = ConexaoUtil.getConexaoPadrao();    
			Statement stmCred = conn.createStatement();

			ResultSet rsCred= stmCred.executeQuery(strCred);

			JRResultSetDataSource DSCred = new JRResultSetDataSource(rsCred);
			URL urlSub = getClass().getResource("/relatorios/");

			params.put("SUBREPORT_DIR", urlSub.getPath()); 
			params.put("REPORT_CONNECTION", conn);
			params.put("DSLog", jrRSLogDS);
			params.put("DSSub", DSSub);
			params.put("DSCred", DSCred);

			if(request.getParameter("idEmpresaFisica") != null && !request.getParameter("idEmpresaFisica").equals(""))
				params.put("empresaDescricao",""+request.getParameter("idEmpresaFisica"));

			if(!dataSInicial.equals(""))
			{
				params.put("HABILITAR_DATA", "s");
				params.put("dataInicial", request.getParameter("dataInicial"));        
			}
			else
				params.put("HABILITAR_DATA", "n");

			if(!dataSFinal.equals("") )
			{
				params.put("HABILITAR_DATA", "s");
				params.put("dataFinal", request.getParameter("dataFinal"));
			}
			else
				params.put("HABILITAR_DATA", "n");

			JasperPrint impressao = JasperFillManager.fillReport(rel, params,jrRS);            
			bytes = JasperExportManager.exportReportToPdf(impressao); 
		}
		catch (JRException | SQLException e)
		{
			throw new ServletException(e);  
		}  

		if (bytes != null && bytes.length > 0) 
		{ 
			response.setContentType("application/pdf");  
			response.setContentLength(bytes.length);  
			ServletOutputStream ouputStream = response.getOutputStream();  
			ouputStream.write(bytes, 0, bytes.length);  
			ouputStream.flush();  
			ouputStream.close();  
		}   

		if(conn != null && !conn.isClosed())
			conn.close();
	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Divergencia Pagamento")
	public void gerarRelatorioDivergenciasPagamento() throws Exception
	{
		byte[] bytes = null;  
		try
		{
			conn = ConexaoUtil.getConexaoPadrao();    

			boolean cupom = new UsuarioService().getSessionUae(request);

			Statement stm = conn.createStatement();
			caminho = "relatorioDivergenciasCrediario.jasper"; 

			URL urlArquivo = getClass().getResource("/relatorios/"+caminho);
			String rel = urlArquivo.getPath();

			Image logo = null;
			InputStream inputStreamDaImagem = null;   
			URL urlImagens = getClass().getResource("/imagens/logo.jpg");
			String caminhoImagem = urlImagens.getPath();
			try {    
				File file = new File(caminhoImagem);   

				if(file.exists())//testa se imagem existe  
					inputStreamDaImagem = new BufferedInputStream(
							new FileInputStream(caminhoImagem));

				logo = ImageIO.read(inputStreamDaImagem);
			} catch (FileNotFoundException e) {       
				e.printStackTrace();       
			}

			SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
			SimpleDateFormat dfa = new SimpleDateFormat ("dd/MM/yyyy");
			Date dFinal = null;
			Date dInicial = null;
			String dataSInicial = "";
			String dataSFinal = "";
			if(request.getParameter("dataInicial")!=null && !request.getParameter("dataInicial").equals(""))
			{
				dInicial = dfa.parse(request.getParameter("dataInicial"));
				dataSInicial =  df.format(dInicial);
			}
			if(request.getParameter("dataFinal")!=null && !request.getParameter("dataFinal").equals(""))
			{
				dFinal =  dfa.parse(request.getParameter("dataFinal"));
				dataSFinal =  df.format(dFinal);
			}

			String str = "";
			str = str + " SELECT sum(mov.valorRecebido) as valorRecebido,sum(mov.valorEntradaJuros) as juros, "; 
			str = str + " sum(mov.valorDesconto) as valorDesconto, "; 
			str = str + " concat(cli.idCliente,' - ',cli.nome) as cliente, "; 
			str = str + " par.numeroparcela, par.acordo,"; 
			str = str + " par.numeroDuplicata,par.situacao "; 
			str = str + " ,par.dataBaixa,par.valorParcela,par.datavencimento,par.valorAPagarOriginal,par.idempresafisica "; 
			str = str + "  FROM movimentoparceladuplicata mov "; 
			str = str + " inner join duplicataparcela par on par.idDuplicataParcela = mov.idDuplicataParcela "; 
			str = str + " inner join cliente cli on cli.idCliente = par.idCliente "; 
			str = str + " where par.situacao = '1' "; 
			if(request.getParameter("idEmpresaFisica")!=null && !request.getParameter("idEmpresaFisica").equals(""))
			{
				str = str + " and par.idEmpresaFisica in ("+request.getParameter("idEmpresaFisica")+")";  
			}
			if(!dataSInicial.equals(""))
			{
				str = str + " and par.databaixa >='"+dataSInicial+" 00:00:00' ";    
			}
			if(!dataSFinal.equals(""))
			{
				str = str + " and par.databaixa <='"+dataSFinal+" 23:59:59' ";    
			}
			if(request.getParameter("idCliente")!=null && !request.getParameter("idCliente").equals(""))
			{
				str = str + " and par.idcliente = " +request.getParameter("idCliente"); 
			}
			str = str + " group by mov.idduplicataparcela "; 
			str = str + " order by par.dataBaixa asc "; 
			System.out.println(str);
			ResultSet rs = stm.executeQuery(str);
			if(!rs.next())
			{
				throw new Exception("Sem Registros!"); 
			}
			rs.beforeFirst();
			List<ItemRelatorioDivergenciasCrediario> lista = new ArrayList<>();
			while(rs.next())
			{
				ItemRelatorioDivergenciasCrediario item = new ItemRelatorioDivergenciasCrediario();
				item.setCliente(rs.getString("cliente"));
				item.setDataBaixa(rs.getDate("dataBaixa"));
				item.setDatavencimento(rs.getDate("datavencimento"));
				item.setIdempresafisica(rs.getInt("idempresafisica"));
				item.setJuros(rs.getDouble("juros"));
				item.setNumeroDuplicata(rs.getLong("numeroDuplicata"));
				item.setNumeroparcela(rs.getInt("numeroparcela"));
				item.setSituacao(rs.getString("situacao"));
				item.setValorDesconto(rs.getDouble("valorDesconto"));
				item.setValorParcela(rs.getDouble("valorParcela"));
				item.setValorRecebido(rs.getDouble("valorRecebido"));
				item.setAcordo(rs.getString("acordo"));
				Double valorReal = 0.0;
				EmpresaFisica empresa = new EmpresaFisicaService().recuperarPorId(rs.getInt("idempresafisica"));
				valorReal = new DuplicataParcelaService().retornaValorParcelaComJurosComDataFinal(rs.getDouble("valorParcela"), rs.getDate("datavencimento"),rs.getDate("dataBaixa"), empresa);
				item.setValorAPagarOriginal(valorReal);
				item.setDiferenca(new BigDecimal(item.getValorRecebido()-valorReal).setScale(2,BigDecimal.ROUND_HALF_EVEN).doubleValue());
				item.setValorJurosOriginal(new BigDecimal(valorReal-item.getValorParcela()).setScale(2,BigDecimal.ROUND_HALF_EVEN).doubleValue());
				Double difMin = null;
				if(request.getParameter("diferencaMinima")!=null && !request.getParameter("diferencaMinima").equals(""))
				{
					difMin = Double.parseDouble(request.getParameter("diferencaMinima"));
				}
				if(difMin!=null)
				{
					if((item.getDiferenca()<0?item.getDiferenca()*(-1):item.getDiferenca())>=difMin)
					{
						if((item.getDiferenca()!=0.0))
						{
							lista.add(item);
						}
					}    	
				}
				else
				{
					if((item.getDiferenca()!=0.0))
					{
						lista.add(item);
					}	
				}
			}

			rs.beforeFirst();
			rs.close();
			stm.close();
			conn.close();

			JRBeanCollectionDataSource jrRS = new JRBeanCollectionDataSource(cupom==true?lista:null,false);

			String strLog = "";
			strLog = strLog + " select log.*, if(log.nomeClasse = ' Duplicata',du.numeroDuplicata,if(log.nomeClasse = ' Duplicata Parcela',concat(dp.numeroDuplicata,'/',dp.numeroparcela),if(log.nomeClasse = ' Pedido Venda',pv.numeroControle,null))) as des  FROM logduplicata log ";
			strLog = strLog + " left outer join duplicataparcela dp on dp.idDuplicataParcela = log.idDocumentoOrigem  "; 
			strLog = strLog + "  left outer join duplicata du on du.numeroDuplicata = log.idDocumentoOrigem   "; 
			strLog = strLog + " left outer join pedidovenda pv on pv.idPedidoVenda = log.idDocumentoOrigem  "; 
			strLog = strLog + " where log.data >='"+dataSInicial+" 00:00:00' "; 
			strLog = strLog + " and log.data <= '"+dataSFinal+" 23:59:59' "; 
			strLog = strLog + " and (log.nomeClasse = ' Duplicata' "; 
			strLog = strLog + " or log.nomeClasse = ' Duplicata Parcela' ";
			strLog = strLog + " or log.nomeClasse = ' Movimento Parcela Duplicata' ";
			strLog = strLog + " or log.nomeClasse = ' Caixa Dia'";
			strLog = strLog + " or log.nomeClasse = ' Duplicata Acordo')";
			if(request.getParameter("idEmpresaFisica")!=null && !request.getParameter("idEmpresaFisica").equals(""))
			{
				strLog = strLog + " and log.idEmpresaFisica in ("+request.getParameter("idEmpresaFisica")+")"; 
			}
			if(request.getParameter("idCliente")!=null && !request.getParameter("idCliente").equals(""))
			{
				strLog = strLog + " and log.idCliente = "+request.getParameter("idCliente");
			}

			conn = ConexaoUtil.getConexaoPadrao();        
			stm = conn.createStatement();
			ResultSet rsLog = stm.executeQuery(strLog);
			JRResultSetDataSource jrRsLog = new JRResultSetDataSource(rsLog);

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("tituloRelatorio", "Relatário Divergencias Recebimento Crediario");
			params.put("logo", logo); 
			params.put("HABILITAR_DATA", "S");
			params.put("RelatorioLogDS", jrRsLog);
			params.put("dataInicial", request.getParameter("dataInicial"));        
			params.put("dataFinal", request.getParameter("dataFinal"));  
			URL urlSub = getClass().getResource("/relatorios/");       
			params.put("SUBREPORT_DIR", urlSub.getPath()); 
			params.put("REPORT_CONNECTION", conn);

			JasperPrint impressao = JasperFillManager.fillReport(rel, params,jrRS);        
			bytes = JasperExportManager.exportReportToPdf(impressao); 

		}
		catch (JRException | SQLException e) 
		{
			throw new ServletException(e);  
		}  

		if (bytes != null && bytes.length > 0) 
		{
			response.setContentType("application/pdf");  
			response.setContentLength(bytes.length);  
			ServletOutputStream ouputStream = response.getOutputStream();  
			ouputStream.write(bytes, 0, bytes.length);  
			ouputStream.flush();  
			ouputStream.close();  
		}

		if(conn != null && !conn.isClosed())
			conn.close();
	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Total Recebimento Dia")
	public void gerarRelatorioTotaisDeREcebimentoDia() throws Exception
	{
		byte[] bytes = null;  
		try
		{
			conn = ConexaoUtil.getConexaoPadrao();    
			Statement stm = conn.createStatement();
			caminho = "relatorioTotaisRecebimentoDia.jasper"; 
			URL urlArquivo = getClass().getResource("/relatorios/"+caminho);
			String rel = urlArquivo.getPath();
			Image logo = null;
			InputStream inputStreamDaImagem = null;   
			URL urlImagens = getClass().getResource("/imagens/logo.jpg");
			String caminhoImagem = urlImagens.getPath();//recebe o caminho da imagem  
			Boolean uae = new UsuarioService().getSessionUae(request);

			try {    
				File file = new File(caminhoImagem);   

				if(file.exists())//testa se imagem existe  
					inputStreamDaImagem = new BufferedInputStream(
							new FileInputStream(caminhoImagem));

				logo = ImageIO.read(inputStreamDaImagem);
			} catch (FileNotFoundException e) {       
				e.printStackTrace();       
			}

			SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
			SimpleDateFormat dfa = new SimpleDateFormat ("dd/MM/yyyy");
			Date dFinal = null;
			Date dInicial = null;
			String dataSInicial = "";
			String dataSFinal = "";
			if(request.getParameter("dataInicial")!=null && !request.getParameter("dataInicial").equals(""))
			{
				dInicial = dfa.parse(request.getParameter("dataInicial"));
				dataSInicial =  df.format(dInicial);
			}
			if(request.getParameter("dataFinal")!=null && !request.getParameter("dataFinal").equals(""))
			{
				dFinal =  dfa.parse(request.getParameter("dataFinal"));
				dataSFinal =  df.format(dFinal);
			}
			String str = "";

			if(uae)
			{
				str = str + " select CASE mov.tipoPagamento "; 
				str = str + "     WHEN 1 THEN 'Recebimento Integral' "; 
				str = str + "     WHEN 2 THEN 'Recebimento Parcial' "; 
				str = str + "     When 5 THEN 'ACORDO' "; 
				str = str + " END AS 'Tipo Pagamento', "; 
				str = str + " ROUND(SUM(IFNULL(mov.valorRecebido, 0))) as 'Total Pago',  "; 
				str = str + " round(sum(if(mov.tipopagamento=2,0,mov.valorEntradaJuros))-sum(if(mov.tipopagamento=2,0,mov.valorDesconto)),2) as 'Total Juros' "; 
				str = str + " from movimentoparceladuplicata mov "; 
				str = str + " inner join duplicataparcela par on par.idDuplicataParcela = mov.idDuplicataParcela";
				str = str + " where mov.dataMovimento >= '"+dataSInicial+"' and mov.dataMovimento <= '"+dataSFinal+"' "; 
				str = str + " and mov.idEmpresaFisica in ("+request.getParameter("idEmpresaFisicas")+") "; 
				str = str + " and par.numeroParcela <> 0 "; 
				str = str + " group by mov.tipoPagamento "; 
			}
			else
			{
				str = str + " select CASE mov.tipoPagamento "; 
				str = str + "     WHEN 1 THEN 'Recebimento Integral' "; 
				str = str + "     WHEN 2 THEN 'Recebimento Parcial' "; 
				str = str + "     When 5 THEN 'ACORDO' "; 
				str = str + " END AS 'Tipo Pagamento', "; 
				str = str + " ROUND(SUM(IFNULL(mov.valorRecebido, 0))) as 'Total Pago',  "; 
				str = str + " round(sum(if(mov.tipopagamento=2,0,mov.valorEntradaJuros))-sum(if(mov.tipopagamento=2,0,mov.valorDesconto)),2) as 'Total Juros' "; 
				str = str + " from movimentoparceladuplicata mov "; 
				str = str + " inner join duplicataparcela par on par.idDuplicataParcela = mov.idDuplicataParcela";
				str = str + " INNER JOIN duplicata dup on dup.idDuplicata = par.idDuplicata ";
				str = str + " INNER JOIN pedidoVenda pv on pv.idPedidoVenda = dup.idPedidoVenda ";
				str = str + " where mov.dataMovimento >= '"+dataSInicial+"' and mov.dataMovimento <= '"+dataSFinal+"' "; 
				str = str + " and mov.idEmpresaFisica in ("+request.getParameter("idEmpresaFisicas")+") "; 
				str = str + " and par.numeroParcela <> 0 "; 
				str = str + " group by mov.tipoPagamento "; 
			}

			ResultSet rs = stm.executeQuery(str);  

			if(!rs.next())
				throw new Exception("Sem Registros!"); 

			rs.beforeFirst();
			Map<String, Object> params = new HashMap<String, Object>();

			params.put("logo", logo); 
			params.put("tituloRelatorio", "Relatorio Totais de Recebimento do Dia");
			params.put("HABILITAR_DATA", "S");
			params.put("dataInicial", request.getParameter("dataInicial"));        
			params.put("dataFinal", request.getParameter("dataFinal"));  
			URL urlSub = getClass().getResource("/relatorios/");       
			params.put("SUBREPORT_DIR", urlSub.getPath()); 
			params.put("REPORT_CONNECTION", conn);
			JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);
			JasperPrint impressao = JasperFillManager.fillReport(rel, params,jrRS);        
			bytes = JasperExportManager.exportReportToPdf(impressao); 

		}
		catch (JRException | SQLException e) 
		{
			throw new ServletException(e);  
		}  

		if (bytes != null && bytes.length > 0) 
		{ 
			response.setContentType("application/pdf");  
			response.setContentLength(bytes.length);  
			ServletOutputStream ouputStream = response.getOutputStream();  
			ouputStream.write(bytes, 0, bytes.length);  
			ouputStream.flush();  
			ouputStream.close();  
		}

		if(conn != null && !conn.isClosed())
			conn.close();
	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Contratos Liquidados")
	public void gerarRelatorioRelacaoContratosLiquidados() throws Exception
	{
		byte[] bytes = null;  
		try
		{ 
			conn = ConexaoUtil.getConexaoPadrao();    
			Statement stm = conn.createStatement();
			caminho = "relatorioContratosLiquidados.jasper"; 
			boolean cupom  = new UsuarioService().getSessionUae(request);
			URL urlArquivo = getClass().getResource("/relatorios/"+caminho);
			String rel = urlArquivo.getPath();
			Image logo = null;
			InputStream inputStreamDaImagem = null;   
			URL urlImagens = getClass().getResource("/imagens/logo.jpg");
			String caminhoImagem = urlImagens.getPath();//recebe o caminho da imagem  
			try
			{    
				File file = new File(caminhoImagem);   

				if(file.exists())//testa se imagem existe  
					inputStreamDaImagem = new BufferedInputStream(new FileInputStream(caminhoImagem));
				logo = ImageIO.read(inputStreamDaImagem);
			}
			catch (FileNotFoundException e)
			{       
				e.printStackTrace();       
			}

			SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
			SimpleDateFormat dfa = new SimpleDateFormat ("dd/MM/yyyy");
			Date dFinal = null;
			Date dInicial = null;
			String dataSInicial = "";
			String dataSFinal = "";
			Integer idEmpresaFisica = Integer.parseInt(request.getParameter("idEmpresaFisica"));
			EmpresaFisica empresaFisica = new EmpresaFisicaService().recuperarPorId(idEmpresaFisica);
			String nomeEmpresa = "F" + empresaFisica.getId()+" - "+empresaFisica.getRazaoSocial()+"";

			if(request.getParameter("dataInicial")!=null && !request.getParameter("dataInicial").equals(""))
			{
				dInicial = dfa.parse(request.getParameter("dataInicial"));
				dataSInicial =  df.format(dInicial);
			}
			if(request.getParameter("dataFinal")!=null && !request.getParameter("dataFinal").equals(""))
			{
				dFinal =  dfa.parse(request.getParameter("dataFinal"));
				dataSFinal =  df.format(dFinal);
			}


			String str = "";
			if(cupom)
			{
				str = str + " SELECT d.numeroFatura as FATURA, cli.nome as CLIENTE, cli.idCliente as CODIGOCLIENTE, d.dataCompra AS DT_VENDA, ";
				str = str + " d.numeroTotalParcela AS TP, d.valorTotalCompra AS VR_VENDA, d.valorEntrada AS VR_ENTRADA,";
				str = str + " d.valorTotalCompra - d.valorEntrada AS VR_FINANC";
				str = str + " FROM duplicata d ";
				str = str + " INNER JOIN cliente cli on cli.idcliente = d.idcliente "; 
				str = str + " INNER JOIN duplicataparcela dp1 on dp1.idduplicata = d.idduplicata ";
			}
			else
			{
				str = str + " SELECT d.numeroFatura as FATURA, cli.nome as CLIENTE, cli.idCliente as CODIGOCLIENTE, d.dataCompra AS DT_VENDA, ";
				str = str + " d.numeroTotalParcela AS TP, d.valorTotalCompra AS VR_VENDA, d.valorEntrada AS VR_ENTRADA,";
				str = str + " d.valorTotalCompra - d.valorEntrada AS VR_FINANC";
				str = str + " FROM duplicata d ";
				str = str + " INNER JOIN cliente cli on cli.idcliente = d.idcliente "; 
				str = str + " INNER JOIN duplicataparcela dp1 on dp1.idduplicata = d.idduplicata ";
				str = str + " INNER JOIN pedidovenda pv on pv.idpedidovenda = d.idpedidovenda ";
				str = str + " LEFT OUTER JOIN cupomFiscal cf on cf.idcupomfiscal = pv.idcupomfiscal ";
				str = str + " LEFT OUTER JOIN notaFiscal nfce ON nfce.idNotafiscal = (SELECT idNotaFiscal FROM notaFiscal WHERE idPedidoVenda = pv.idPedidoVenda AND tpAmb = 1 LIMIT 1) ";
			}

			str = str + " WHERE  ";
			str = str + " ((SELECT COUNT(*) from duplicataParcela dp where dp.idDuplicata = d.idDuplicata) =  ";
			str = str + " (SELECT COUNT(*) from duplicataParcela dp where dp.idDuplicata = d.idDuplicata and dp.situacao in (1,3))) ";

			if(!cupom)
				str = str + " AND (cf.idCupomFiscal IS NOT NULL OR nfce.idNotaFiscal IS NOT NULL) ";

			if(idEmpresaFisica != null)
				str = str + " and d.idEmpresaFisica = " + idEmpresaFisica + " ";

			if(request.getParameter("dataInicial")!=null && !request.getParameter("dataInicial").equals(""))
				str = str + " and d.dataCompra >= '" + dataSInicial + " 00:00:00' ";

			if(request.getParameter("dataFinal")!=null && !request.getParameter("dataFinal").equals(""))
				str = str + " and d.dataCompra <= '" + dataSFinal + " 23:59:59' ";

			str = str + " GROUP BY d.idduplicata";
			str = str + " ORDER BY d.dataCompra, d.numeroFatura";
			str = str + " LIMIT 50000 ";

			ResultSet rs = stm.executeQuery(str);  

			if(!rs.next())
				throw new Exception("Sem Registros!"); 

			rs.beforeFirst();
			Map<String, Object> params = new HashMap<String, Object>();

			params.put("logo", logo); 
			params.put("tituloRelatorio", "RELACAO DE CONTRATOS LIQUIDADOS");
			params.put("HABILITAR_DATA", "S");
			params.put("dataInicial", request.getParameter("dataInicial"));        
			params.put("dataFinal", request.getParameter("dataFinal"));  
			params.put("empresaDescricao", nomeEmpresa);
			URL urlSub = getClass().getResource("/relatorios/");       
			params.put("SUBREPORT_DIR", urlSub.getPath()); 
			params.put("REPORT_CONNECTION", conn);
			JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);
			JasperPrint impressao = JasperFillManager.fillReport(rel, params,jrRS);        
			bytes = JasperExportManager.exportReportToPdf(impressao); 
		}
		catch (JRException | SQLException e) 
		{  
			throw new ServletException(e);  
		}  

		if (bytes != null && bytes.length > 0) 
		{  
			response.setContentType("application/pdf");  
			response.setContentLength(bytes.length);  
			ServletOutputStream ouputStream = response.getOutputStream();  
			ouputStream.write(bytes, 0, bytes.length);  
			ouputStream.flush();  
			ouputStream.close();  
		}

		if(conn != null && !conn.isClosed())
			conn.close();
	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Segundo Vencimento")
	public void gerarRelatorioSegundoVencimentoPeriodo() throws Exception 
	{
		Map<String, Object> params = new HashMap<String, Object>();
		Connection con = ConexaoUtil.getConexaoPadrao();
		Statement stm = con.createStatement();
		InputStream imageInputStream = null;
		Image logo = null;
		DateFormat formataData = new SimpleDateFormat("dd/MM/yyyy");
		Date dataInicialFormatada = request.getParameter("dataInicial").equals("") ? null : formataData.parse(request.getParameter("dataInicial"));
		Date dataFinalFormatada = request.getParameter("dataFinal").equals("")   ? null : formataData.parse(request.getParameter("dataFinal"));
		DateFormat formatadorDataSQL = new SimpleDateFormat("yyyy-MM-dd");
		String idEmpresaFisica = request.getParameter("idEmpresaFisica");
		String idCliente = request.getParameter("idCliente");
		EmpresaFisica empresaFisica = new EmpresaFisica();
		if(idEmpresaFisica != null)
			empresaFisica = new EmpresaFisicaService().recuperarPorId(Integer.parseInt(idEmpresaFisica));

		try {
			File file = new File(getClass().getResource("/imagens/logo.jpg").getPath());
			if(file.exists()) {
				imageInputStream = new BufferedInputStream(new FileInputStream(getClass().getResource("/imagens/logo.jpg").getPath()));
				logo = ImageIO.read(imageInputStream);
			}
		} finally {
			if(imageInputStream != null) {
				imageInputStream.close();
			}
		}

		String str = "";
		str += " select dpp.dataVencimento02Acordado as segundoVencimento, ";
		str += " concat(cli.idCliente,' - ', cli.nome) as cliente, ";
		str += " concat(coalesce(cli.telefoneResidencial, ''), ' ', coalesce(cli.telefoneCelular01, ''), ' ', coalesce(cli.telefoneComercial, ''), ' ') as telefones, ";
		str += " concat(dpp.numeroDuplicata,' / ',dpp.numeroParcela) as duplicata, ";
		str += " dpp.dataVencimento as vencimento, ";
		str += " dpp.valorParcela as valorParcela, ";
		str += " emp.idEmpresaFisica as idEmpresa, ";
		str += " concat('F', emp.idEmpresaFisica, '-', emp.razaoSocial) as razao ";
		str += " from duplicataParcela dpp ";
		str += " inner join cliente cli on dpp.idCliente = cli.idCliente ";
		str += " inner join empresaFisica emp on dpp.idEmpresaFisica = emp.idEmpresaFisica ";
		str += " where dpp.situacao = 0 ";
		str += " and dpp.dataVencimento02Acordado is not null ";

		if(idCliente != null && !idCliente.equals("")) {
			str += " and cli.idCliente = '"+ idCliente +"' ";
			//params.put("idCliente", idCliente);
		}

		if(idEmpresaFisica != null) {
			str += " and emp.idempresafisica in ("+ idEmpresaFisica +") ";
			//params.put("idEmpresaFisica", idEmpresaFisica);
		}

		if(dataInicialFormatada != null && !dataInicialFormatada.equals("")) {
			str += " and dpp.dataVencimento02Acordado >= '"+ formatadorDataSQL.format(dataInicialFormatada) +" 00:00:00' and dpp.dataVencimento02Acordado <= '"+ formatadorDataSQL.format(dataFinalFormatada) +" 23:59:59' ";
			params.put("dataInicial", request.getParameter("dataInicial"));
			params.put("dataFinal", request.getParameter("dataFinal"));
		}

		str += " order by cli.nome, dpp.dataVencimento02Acordado ";
		System.out.println(str);
		ResultSet rs = stm.executeQuery(str);



		JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);

		params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath());
		params.put("IMAGE_STREAM", logo);
		params.put("SUBREPORT_DATASOURCE", jrRS);
		params.put("empresa", "F"+empresaFisica.getId()+" - "+empresaFisica.getNomeFantasia());
		params.put("tituloRelatorio", "LISTA DE DUPLICATAS POR 2 VENCIMENTO");

		JasperPrint impressao = JasperFillManager.fillReport(getClass().getResource("/relatorios/relatorioSegundoVencimentoPeriodo.jasper").getPath(), params, jrRS);
		byte[] bytes = JasperExportManager.exportReportToPdf(impressao);

		this.response.setContentLength(bytes.length);
		this.response.setContentType("application/pdf");
		this.response.getOutputStream().write(bytes, 0, bytes.length);    
		this.response.getOutputStream().flush(); 
		this.response.getOutputStream().close();
	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Parcelas Vencimento")
	public void gerarRelatorioParcelasVencimento() throws Exception
	{
		try
		{
			Map<String, Object> params = new HashMap<String, Object>();
			Connection con = ConexaoUtil.getConexaoPadrao();
			Statement stm = con.createStatement();
			InputStream imageInputStream = null;
			Image logo = null;
			DateFormat formataData = new SimpleDateFormat("dd/MM/yyyy");
			Date dataInicialFormatada = request.getParameter("dataInicial").equals("") ? null : formataData.parse(request.getParameter("dataInicial"));
			Date dataFinalFormatada = request.getParameter("dataFinal").equals("")   ? null : formataData.parse(request.getParameter("dataFinal"));
			Date dataEmissaoInicialFormatada = request.getParameter("dataEmissaoInicial").equals("") ? null : formataData.parse(request.getParameter("dataEmissaoInicial"));
			Date dataEmissaoFinalFormatada = request.getParameter("dataEmissaoFinal").equals("")   ? null : formataData.parse(request.getParameter("dataEmissaoFinal"));
			DateFormat formatadorDataSQL = new SimpleDateFormat("yyyy-MM-dd");
			String idEmpresaFisica = request.getParameter("idEmpresaFisica");
			String situacao = request.getParameter("situacao");
			String acordo = request.getParameter("acordo");
			try {
				File file = new File(getClass().getResource("/imagens/logo.jpg").getPath());
				if(file.exists()) {
					imageInputStream = new BufferedInputStream(new FileInputStream(getClass().getResource("/imagens/logo.jpg").getPath()));
					logo = ImageIO.read(imageInputStream);
				}
			} finally {
				if(imageInputStream != null) {
					imageInputStream.close();
				}
			}

			String str = "";
			str = str + " SELECT  "; 
			str = str + " concat(cli.idCliente,' - ', cli.nome ) as Cliente, "; 
			str = str + " concat(p.numeroDuplicata,'/',p.numeroParcela) as parcela, "; 
			str = str + " p.valorParcela, "; 
			str = str + " p.valorPago, "; 
			str = str + " p.situacao, "; 
			str = str + " p.datavencimento "; 
			str = str + "  FROM duplicataparcela p "; 
			str = str + " inner join cliente cli on cli.idCliente = p.idCliente "; 
			str = str + " inner join duplicata dupli on dupli.idduplicata = p.idduplicata "; 
			str = str + " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
			str = str + " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata = ac.numeroDuplicataAntiga ";
			str = str + " group by ac.idDuplicataNova) "; 
			str = str + " as acordo on acordo.idDuplicataNova = p.idduplicata ";
			str = str + " where p.dataVencimento >='"+ formatadorDataSQL.format(dataInicialFormatada) +" 00:00:00' "; 
			str = str + " and	  p.dataVencimento <='"+ formatadorDataSQL.format(dataFinalFormatada) +" 23:59:59' "; 
			if(dataEmissaoInicialFormatada != null)
				str = str + " and if((p.acordo<>'S' or p.acordo is null),dupli.dataCompra >= '"+formatadorDataSQL.format(dataEmissaoInicialFormatada)+" 00:00:00',acordo.dataCompra >= '"+formatadorDataSQL.format(dataEmissaoInicialFormatada)+" 00:00:00' ) ";
			if(dataEmissaoFinalFormatada != null)
				str = str +"  and if((p.acordo<>'S' or p.acordo is null),dupli.dataCompra <= '"+formatadorDataSQL.format(dataEmissaoFinalFormatada)+" 23:59:59',acordo.dataCompra <= '"+formatadorDataSQL.format(dataEmissaoFinalFormatada)+" 23:59:59')";

			if(situacao!=null && !situacao.equals(""))
			{
				str = str + " and p.situacao =  "+situacao; 
			}
			else
			{
				if(acordo!=null && !acordo.equals("") && acordo.equals("S"))
				{
					str = str + " and p.situacao in(1,2,0,3)";  
				}
				else
				{
					str = str + " and p.situacao in(1,2,0)"; 
				}
			}


			if(idEmpresaFisica != null) {
				str += " and  p.idEmpresaFisica in ("+ idEmpresaFisica +") ";
				//params.put("idEmpresaFisica", idEmpresaFisica);
			}
			str += " order by p.datavencimento ";
			System.out.println(str);
			ResultSet rs = stm.executeQuery(str);

			if(!rs.next()) {
				throw new Exception("Sem Registros");
			}

			rs.beforeFirst();

			JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);

			params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath());
			params.put("IMAGE_STREAM", logo);
			params.put("tituloRelatorio", "LISTA DE PARCELA POR DATA VENCIMENTO");
			params.put("dataInicial", dataInicialFormatada);
			params.put("dataFinal", dataFinalFormatada);
			JasperPrint impressao = JasperFillManager.fillReport(getClass().getResource("/relatorios/relatorioDuplicataParcelaDataVencimento.jasper").getPath(), params, jrRS);
			byte[] bytes = JasperExportManager.exportReportToPdf(impressao);

			this.response.setContentLength(bytes.length);
			this.response.setContentType("application/pdf");
			this.response.getOutputStream().write(bytes, 0, bytes.length);    
			this.response.getOutputStream().flush(); 
			this.response.getOutputStream().close();
		}
		catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}
	}


	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Prestações Recebidas Gráfico")
	public void relatorioPrestacoesRecebidasGrafico() throws Exception
	{
		Map<String, Object> params = new HashMap<String, Object>(); 
		Connection con = ConexaoUtil.getConexaoPadrao();    
		Statement stm = con.createStatement();
		String data = "";
		Integer mesInicial = null;
		Integer mesFinal= null;
		Integer anoInicial = null;
		Integer anoFinal = null;
		String empresas = request.getParameter("empresas");

		if (request.getParameter("dataInicial") == null && request.getParameter("dataInicial").equals("")) 
			throw new Exception("Data inicial invalida.");
		else
		{
			data = request.getParameter("dataInicial").replaceAll("/", "");
			mesInicial = Integer.parseInt(data.substring(0, 2));
			anoInicial = Integer.parseInt(data.substring(2, 6));
		}

		if (request.getParameter("dataFinal") == null && request.getParameter("dataFinal").equals("")) 
			throw new Exception("Data final invalida.");
		else
		{
			data = request.getParameter("dataFinal").replaceAll("/", "");
			mesFinal = Integer.parseInt(data.substring(0, 2));
			anoFinal = Integer.parseInt(data.substring(2, 6));
		}



		if(mesInicial> 12 || mesInicial < 1)
			throw new Exception("Mes Inicial invalido.");

		if(mesFinal> 12 || mesFinal < 1)
			throw new Exception("Mes Final invalido.");

		if(anoInicial > anoFinal)
			throw new Exception("Ano Inicial nao pode ser maior que Ano Final.");

		if(anoInicial >= anoFinal && mesInicial > mesFinal)
			throw new Exception("Mes/Ano Inicial nao pode ser maior que Mes/Ano Final");

		//if(anoFinal > anoInicial && mesFinal < mesInicial)
		//mesFinal = null;


		String str = "";
		str = str + "  SELECT mov.idEmpresaFisica as idEmpresaFisica,  month(dataMovimento) as Mes, year(dataMovimento) as ano, round(sum(mov.valorRecebido),2) as valorRecebido ";
		str = str + "  FROM movimentoparceladuplicata mov  left outer JOIN usuario usu on usu.idUsuario = mov.idUsuario   ";
		str = str + "  INNER JOIN cliente cli on  cli.idCliente = mov.idCliente  INNER JOIN formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento  ";
		str = str + "  INNER JOIN duplicataparcela par on par.idDuplicataParcela = mov.idDuplicataParcela   ";
		str = str + "  INNER JOIN duplicata dupli on dupli.idDuplicata = mov.idDuplicata  ";
		str = str + "  INNER JOIN pedidoVenda pv on pv.idPedidoVenda = dupli.idPedidoVenda   ";
		str = str + "  WHERE ";

		if(mesInicial != null && anoInicial != null)
			str += " (month(dataMovimento) >= "+mesInicial+" and year(datamovimento)="+anoInicial+") ";


		if(mesFinal != null && anoFinal != null)
		{
			str += " or (month(dataMovimento) >= "+mesFinal+" and year(dataMovimento)="+anoFinal+") ";
		}

		if (!request.getParameter("empresas").equals(""))
			str += " and mov.idEmpresaFisica in("+request.getParameter("empresas")+") ";


		str = str + "  and (par.numeroParcela <> 0 )     ";
		str = str + "  group by year(dataMovimento),month(dataMovimento) ";
		str = str + "  order by year(dataMovimento),month(dataMovimento) ";

		ResultSet rs = stm.executeQuery(str);
		JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);

		params.put("REPORT_CONNECTION", con);
		params.put("mesInicial",mesInicial);
		params.put("mesFinal",mesFinal);
		params.put("anoInicial",anoInicial);
		params.put("anoFinal",anoFinal);
		params.put("empresas",empresas);

		JasperPrint impressao = JasperFillManager.fillReport(getClass().getResource("/relatorios/relatorioRecebimentoPrestacaoGrafico.jasper").getPath(), params, jrRS);                          
		byte[] bytes = JasperExportManager.exportReportToPdf(impressao);  

		this.response.setContentLength(bytes.length);
		this.response.setContentType("application/pdf");
		this.response.getOutputStream().write(bytes, 0, bytes.length);     	
		this.response.getOutputStream().flush();
		this.response.getOutputStream().close(); 
	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Parcelas Vencidas por Cidade")
	public void gerarRelatorioParcelasVencidasPorCidade() throws Exception
	{
		Map<String, Object> params = new HashMap<String, Object>();
		Connection con = ConexaoUtil.getConexaoPadrao();
		Statement stm = con.createStatement();
		InputStream imageInputStream = null;
		Image logo = null;
		Integer diasInicial = null;
		Integer diasFinal = null;
		Integer idCidade = null;
		String idEmpresas = null;
		if(request.getParameter("idCidade")!=null && !request.getParameter("idCidade").equals(""))
		{
			idCidade = Integer.parseInt(request.getParameter("idCidade")); 
		}
		if(request.getParameter("diasInicial")!=null && !request.getParameter("diasInicial").equals("") )
		{
			diasInicial = Integer.parseInt(request.getParameter("diasInicial")); 
		}
		if(request.getParameter("diasFinal")!=null && !request.getParameter("diasFinal").equals("") )
		{
			diasFinal = Integer.parseInt(request.getParameter("diasFinal")); 
		}
		if(request.getParameter("idEmpresas")!=null && !request.getParameter("idEmpresas").equals("") )
		{
			idEmpresas = request.getParameter("idEmpresas"); 
		}
		try {
			File file = new File(getClass().getResource("/imagens/logo.jpg").getPath());
			if(file.exists()) {
				imageInputStream = new BufferedInputStream(new FileInputStream(getClass().getResource("/imagens/logo.jpg").getPath()));
				logo = ImageIO.read(imageInputStream);
			}
		} finally {
			if(imageInputStream != null) {
				imageInputStream.close();
			}
		}
		String str = "";
		str = str + " select (DATEDIFF(par.dataVencimento,Current_DATE)*(-1)) as diasVencidos, ";
		str = str + " calculo_juros_duplicata(par.valorparcela,par.dataVencimento, ";
		str = str + " ef.taxaJuros,par.situacao,par.valorPago,par.databaixa ,par.jurosPago) as juros, ";
		str = str + "  dup.dataCompra, CONCAT(par.numeroParcela,'/',dup.numeroTotalParcela) as prest,par.numeroDuplicata,par.dataVencimento, par.valorParcela,";
		str = str + " cid.nome as cidade, ";
		str = str + " cid.siglaUf as estado, ";
		str = str + "  c.nome,c.idcliente, cid.idCidade,";
		str = str + "  if(situacao = 2,if(databaixa<datavencimento,datediff(current_date(),datavencimento),datediff(current_date(),databaixa)), datediff(current_date(),datavencimento)) as dias ";
		str = str + " from  duplicataparcela par  ";
		str = str + " inner join empresafisica ef on ef.idEmpresaFisica = par.idEmpresaFisica ";
		str = str + " inner join duplicata dup on dup.idduplicata=par.idduplicata ";
		str = str + " inner join cliente c on c.idcliente = par.idcliente ";
		str = str + " left outer join cidade cid on c.idCidadeEndereco = cid.idcidade ";
		str = str + " where par.situacao in(0,2)  ";
		if(idCidade!=null)
		{
			str = str + " and c.idcidadeendereco in("+idCidade+") ";
		}
		if(idEmpresas!=null)
		{
			str = str + " and dup.idEmpresaFisica in("+idEmpresas+") ";
		}
		if(diasInicial!=null)
		{
			str = str + " and if(situacao = 2,if(databaixa<datavencimento,datediff(current_date(),datavencimento),datediff(current_date(),databaixa)), datediff(current_date(),datavencimento))>="+diasInicial;
		}
		if(diasFinal!=null)
		{
			str = str + " and if(situacao = 2,if(databaixa<datavencimento,datediff(current_date(),datavencimento),datediff(current_date(),databaixa)), datediff(current_date(),datavencimento))<="+diasFinal;
		}
		str = str + " order by cid.idCidade,c.idcliente ";

		System.out.println(str);
		ResultSet rs = stm.executeQuery(str);

		if(!rs.next()) {
			throw new Exception("Sem Registros");
		}
		rs.beforeFirst();
		JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);
		params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath());
		params.put("IMAGE_STREAM", logo);
		params.put("tituloRelatorio", "Relatorio de Parcelas Por Cidade");
		JasperPrint impressao = JasperFillManager.fillReport(getClass().getResource("/relatorios/relatorioDuplciatasAtrasadasPorCidade.jasper").getPath(), params, jrRS);
		byte[] bytes = JasperExportManager.exportReportToPdf(impressao);

		this.response.setContentLength(bytes.length);
		this.response.setContentType("application/pdf");
		this.response.getOutputStream().write(bytes, 0, bytes.length);    
		this.response.getOutputStream().flush(); 
		this.response.getOutputStream().close();
	}
}