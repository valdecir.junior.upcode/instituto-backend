package br.com.desenv.nepalign.relatorios;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.LancamentoContaPagarRateio;
import br.com.desenv.nepalign.service.LancamentoContaPagarService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class RelatorioReembolsoDespesaService 
{
	private static final Logger logger = Logger.getLogger("REPORT.FINANCEIRO");
	
	private HttpServletRequest request;
	private HttpServletResponse response;
	
	private static final LancamentoContaPagarService lancamentoContaPagarService = new LancamentoContaPagarService();
	private static final String[] empresasEquivalentes = {"1,101", "2,52", "3,33", "4,44", "5,55", "6,56", "7,57", "8,58", "9,59", "10,110", "13, 113"};
	
	public RelatorioReembolsoDespesaService(HttpServletRequest request, HttpServletResponse response) throws ServletException, Exception 
	{
		this.request = request; 
		this.response = response;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void gerarRelatorio() throws Exception
	{
		Image logo = null; 
		Connection conn = null;
		byte[] reportBytes = null;
		EntityManager manager = null;
		List dataProvider = new ArrayList();
		
		String reportPath = null;
		
		Comparator<Integer> comparator = new Comparator<Integer>() 
		{
			 @Override
             public int compare(Integer first, Integer second)
             {
				 return first.compareTo(second);
             }
		};
		
		Map<Integer, Map<Integer, Map<String, List>>> ordainedList = new TreeMap<Integer, Map<Integer, Map<String, List>>>(comparator);

		try 
		{          
			reportPath = getClass().getResource("/relatorios/ReembolsoDespesas.jasper").getPath();
			conn = ConexaoUtil.getConexaoPadrao();
			manager = ConexaoUtil.getEntityManager();
			
			if(request.getParameter("iTipoRelatorio") == null || request.getParameter("iIdTipoRelatorio") == "")
				throw new Exception("Tipo do relatório não informado");
			
			StringBuilder sql = new StringBuilder();
			sql.append(" select CONCAT(empresaFisicaOrigemDespesa.idEmpresaFisica, '-', empresaFisicaOrigemDespesa.razaoSocial) as empresaOrigemDespesa, ");
			sql.append(" CONCAT(empresaFisicaBaixa.idEmpresaFisica, '-', empresaFisicaBaixa.razaoSocial) as empresaBaixa, ");
			sql.append(" CONCAT(contaCorrenteBaixa.idContaCorrente, '-', contaCorrenteBaixa.descricao) as contaBaixa, ");
			sql.append(" sum(COALESCE(baixaLancamento.valor,0) - COALESCE(baixaLancamento.valorJuros, 0) + COALESCE(baixaLancamento.valorDesconto, 0)) as valorPago, CONCAT(contaGerencial.contaContabil, '-', contaGerencial.descricao) as contaGerencial, ");
			sql.append(" CONCAT(banco.idBanco, '-', banco.descricao) as bancoBaixa, lancamento.empresasRateio as rateio, lancamento.idLancamentoContaPagar from baixaLancamentoContaPagar baixaLancamento ");
			sql.append(" INNER JOIN lancamentoContaPagar lancamento ON lancamento.idLancamentoContaPagar = baixaLancamento.idLancamentoContaPagar ");
			sql.append(" INNER JOIN empresaFisica empresaFisicaOrigemDespesa ON empresaFisicaOrigemDespesa.idEmpresaFisica = lancamento.idEmpresa ");
			sql.append(" INNER JOIN contaCorrente contaCorrenteBaixa ON contaCorrenteBaixa.idContaCorrente = baixaLancamento.idContaCorrente ");
			sql.append(" INNER JOIN contaGerencial contaGerencial ON contaGerencial.idContaGerencial = lancamento.idContaGerencial ");
			sql.append(" LEFT OUTER JOIN empresaFisica empresaFisicaBaixa ON empresaFisicaBaixa.idEmpresaFisica = contaCorrenteBaixa.idEmpresa ");
			sql.append(" INNER JOIN banco banco ON contaCorrenteBaixa.idBanco = banco.idbanco ");
			sql.append(" INNER JOIN fornecedor fornecedor ON fornecedor.idFornecedor = lancamento.idFornecedor ");
			sql.append(" INNER JOIN grupoFornecedor grupoFornecedor ON grupoFornecedor.idGrupoFornecedor = fornecedor.idGrupoFornecedor ");
			sql.append(" WHERE grupoFornecedor.idGrupoFornecedor in (2,4) ");
 
			String caminhoImagem = "";//getClass().getResource(DsvConstante.getParametrosSistema().get("CAMINHO_LOGO_RELATORIOS")).getPath();
			
			if (request.getParameter("iDataInicial") != null && request.getParameter("iDataFinal") != null && !request.getParameter("iDataFinal").equals("") && !request.getParameter("iDataInicial").equals(""))
				sql.append(" AND baixaLancamento.dataPagamento BETWEEN '" + new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("iDataInicial"))) + "' AND '" + new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("iDataFinal"))) + "' ");
			/*if (request.getParameter("iIdEmpresa") != null && !request.getParameter("iIdEmpresa").equals(""))
				sql.append(" AND (lancamento.idEmpresa in (" + request.getParameter("iIdEmpresa") + ")  or verificaExistenciaRateioEmpresa(lancamento.empresasRateio, " + request.getParameter("iIdEmpresa") + " = 1");*/
			if (request.getParameter("iIdContaGerencial") != null && !request.getParameter("iIdContaGerencial").equals(""))
				sql.append(" AND lancamento.idContaGerencial in (" + request.getParameter("iIdContaGerencial") + ") ");
			if (request.getParameter("iIdContaCorrente") != null && !request.getParameter("iIdContaCorrente").equals(""))
				sql.append(" AND baixaLancamento.idContaCorrente in (" + request.getParameter("iIdContaCorrente") + ") ");
			if (request.getParameter("iIdFornecedor") != null && !request.getParameter("iIdFornecedor").equals(""))
				sql.append(" AND lancamento.iIdFornecedor in (" + request.getParameter("iIdFornecedor") + ") ");
			
			sql.append(" group by lancamento.idLancamentoContaPagar order by empresaFisicaOrigemDespesa.idEmpresaFisica ");
			
			ResultSet rs = conn.createStatement().executeQuery(String.valueOf(sql));
						 
			Comparator<String> contaGerencialComparator = new Comparator<String>()
			{
				@Override 
				public int compare(String first, String second)
				{
					String codigoContaNivel1First = first.split("-")[0].split("\\.")[0];
					String codigoContaNivel2First = first.split("-")[0].split("\\.")[1];
					
					String codigoContaNivel1Second = second.split("-")[0].split("\\.")[0];
					String codigoContaNivel2Second = second.split("-")[0].split("\\.")[1];
					
					if(((Integer) Integer.parseInt(codigoContaNivel1First + codigoContaNivel2First)).compareTo(Integer.parseInt(codigoContaNivel1Second + codigoContaNivel2Second)) == 0 &&
							first.split("\\.").length > 2 && second.split("\\.").length > 2)
					{
						String codigocontaNivel3First = first.split("-")[0].split("\\.")[2];
						String codigocontaNivel3Second = second.split("-")[0].split("\\.")[2];
						
						return ((Integer) Integer.parseInt(codigocontaNivel3First)).compareTo(Integer.parseInt(codigocontaNivel3Second));
					}
					else
						return ((Integer) Integer.parseInt(codigoContaNivel1First + codigoContaNivel2First)).compareTo(Integer.parseInt(codigoContaNivel1Second + codigoContaNivel2Second));
				}
			};
			
			while(rs.next())
			{
				List<LancamentoContaPagarRateio> rateios = lancamentoContaPagarService.recuperarRateios(manager, rs.getInt("lancamento.idLancamentoContaPagar"));
				
				if(rateios == null || rateios.size() == 0x00)
				{
					if(rs.getString("empresaBaixa").split("-")[0].equals(rs.getString("empresaOrigemDespesa").split("-")[0]) || verificarEquivalenciaEmpresa(rs.getString("empresaBaixa").split("-")[0], rs.getString("empresaOrigemDespesa").split("-")[0]))
						continue;
 
					RelatorioReembolsoDespesaObject reembolsoObj = new RelatorioReembolsoDespesaObject();
					reembolsoObj.setEmpresaOrigemDespesa(rs.getString("empresaOrigemDespesa"));
					reembolsoObj.setEmpresaBaixa(rs.getString("empresaBaixa"));
					reembolsoObj.setContaBaixa(rs.getString("contaBaixa"));
					reembolsoObj.setContaGerencial(rs.getString("contaGerencial"));
					reembolsoObj.setBancoBaixa(rs.getString("bancoBaixa"));
					reembolsoObj.setValorPago(rs.getDouble("valorPago"));
					
					Map<Integer, Map<String, List>> dataProviderEmpresaReembolso = ordainedList.get(Integer.parseInt(rs.getString("empresaBaixa").split("-")[0]));
					
					if(dataProviderEmpresaReembolso == null)
						dataProviderEmpresaReembolso = new TreeMap<Integer, Map<String,List>>();
					
					Map<String, List> empresaMasterDataProvider = dataProviderEmpresaReembolso.get(Integer.parseInt(rs.getString("empresaOrigemDespesa").split("-")[0]));
					
					if(empresaMasterDataProvider == null)
						empresaMasterDataProvider = new TreeMap<String, List>(contaGerencialComparator);
					
					List empresaDataProvider = empresaMasterDataProvider.get(String.valueOf(rs.getString("contaGerencial")));
					
					if(empresaDataProvider == null)
						empresaDataProvider = new ArrayList();
					
					empresaDataProvider.add(reembolsoObj);
					
					empresaMasterDataProvider.put(String.valueOf(rs.getString("contaGerencial")), empresaDataProvider);
					
					dataProviderEmpresaReembolso.put(Integer.parseInt(rs.getString("empresaOrigemDespesa").split("-")[0]), empresaMasterDataProvider);
					
					ordainedList.put(Integer.parseInt(rs.getString("empresaBaixa").split("-")[0]), dataProviderEmpresaReembolso);
				}
				else
				{
					BigDecimal valorTotalPago = new BigDecimal(0.0);
					
					for (LancamentoContaPagarRateio rateio : rateios)
					{
						BigDecimal proporcao = rateio.getPorcentagemRateio().divide(new BigDecimal(100.0));
						valorTotalPago = valorTotalPago.add((rs.getObject("valorPago") == null ? new BigDecimal(0.0) : new BigDecimal(rs.getDouble("valorPago")).multiply(proporcao))).setScale(0x02, RoundingMode.DOWN);
					}
					
					BigDecimal restoValorPago = new BigDecimal(rs.getDouble("valorPago")).subtract(valorTotalPago);
					
					for(LancamentoContaPagarRateio rateio : rateios)
					{
						if(rs.getString("empresaBaixa").split("-")[0].equals(String.valueOf(rateio.getEmpresaFisica().getId())) || verificarEquivalenciaEmpresa(rs.getString("empresaBaixa").split("-")[0], String.valueOf(rateio.getEmpresaFisica().getId())))
							continue;
						
						RelatorioReembolsoDespesaObject reembolsoObj = new RelatorioReembolsoDespesaObject();
						reembolsoObj.setEmpresaOrigemDespesa(String.valueOf(rateio.getEmpresaFisica().getId()).concat("-").concat(rateio.getEmpresaFisica().getRazaoSocial()));
						reembolsoObj.setEmpresaBaixa(rs.getString("empresaBaixa"));
						reembolsoObj.setContaBaixa(rs.getString("contaBaixa"));
						reembolsoObj.setContaGerencial(rs.getString("contaGerencial"));
						reembolsoObj.setBancoBaixa(rs.getString("bancoBaixa"));
						reembolsoObj.setValorPago(new BigDecimal(rs.getDouble("valorPago")).multiply(rateio.getPorcentagemRateio().divide(new BigDecimal(100.0))).add(restoValorPago).setScale(0x02, RoundingMode.DOWN).doubleValue());
						restoValorPago = new BigDecimal(0);
						
						Map<Integer, Map<String, List>> dataProviderEmpresaReembolso = ordainedList.get(Integer.parseInt(rs.getString("empresaBaixa").split("-")[0]));
						
						if(dataProviderEmpresaReembolso == null)
							dataProviderEmpresaReembolso = new TreeMap<Integer, Map<String,List>>();
						
						Map<String, List> empresaMasterDataProvider = dataProviderEmpresaReembolso.get(rateio.getEmpresaFisica().getId());
						
						if(empresaMasterDataProvider == null)
							empresaMasterDataProvider = new TreeMap<String, List>(contaGerencialComparator);
						
						List empresaDataProvider = empresaMasterDataProvider.get(String.valueOf(rs.getString("contaGerencial")));
						
						if(empresaDataProvider == null)
							empresaDataProvider = new ArrayList();
						
						empresaDataProvider.add(reembolsoObj);
						
						empresaMasterDataProvider.put(String.valueOf(rs.getString("contaGerencial")), empresaDataProvider);
						
						dataProviderEmpresaReembolso.put(rateio.getEmpresaFisica().getId(), empresaMasterDataProvider);
						
						ordainedList.put(Integer.parseInt(rs.getString("empresaBaixa").split("-")[0]), dataProviderEmpresaReembolso);
					}	
				}
			} 
		
			Iterator it = ordainedList.entrySet().iterator();
			
			while(it.hasNext())
			{
				Map.Entry<Integer, Map<Integer, Map<String, List>>> valueList = ((Entry<Integer, Map<Integer, Map<String, List>>>) it.next());
				
				Iterator it2 = valueList.getValue().entrySet().iterator();
				
				while(it2.hasNext())
				{
					Map.Entry<Integer, Map<String, List>> valueList2 = ((Map.Entry<Integer, Map<String, List>>) it2.next());

					Iterator it3 = valueList2.getValue().entrySet().iterator();
					 
					while(it3.hasNext()) 
					{
						Map.Entry<String, List> valueList3 = ((Map.Entry<String, List>) it3.next());
						
						for(Object obj : valueList3.getValue())
						{
							RelatorioReembolsoDespesaObject reembolsoDespesaObject = (RelatorioReembolsoDespesaObject) obj;
							
							if(request.getParameter("iIdEmpresa") != null && request.getParameter("iIdEmpresa") != "")
							{
								if(new IgnUtil().contains(reembolsoDespesaObject.getEmpresaBaixa().split("-")[0],Arrays.asList(request.getParameter("iIdEmpresa").split("\\s*,\\s*"))))
									dataProvider.add(obj);	
							}	
							else
								dataProvider.add(obj);	
						}
					}
				}
			}
			
			if(dataProvider.size()  < 1)
				throw new Exception("Sem Registros");
			
			try 
			{    
				if(new File(caminhoImagem).exists())
					logo = ImageIO.read(new BufferedInputStream(new FileInputStream(caminhoImagem)));
				else
					logo = null;
			} 
			catch (FileNotFoundException e)  
			{
				logo = null;
			}       
	                  
			Map<String, Object> params = new HashMap<String, Object>();  
			params.put("logo", logo);  		
			params.put("EMPRESAS", request.getParameter("iIdEmpresa"));
			params.put("TIPO_RELATORIO", request.getParameter("iTipoRelatorio"));
			params.put("iDataInicial", request.getParameter("iDataInicial"));
			params.put("iDataFinal", request.getParameter("iDataFinal"));
           
			reportBytes = JasperExportManager.exportReportToPdf(JasperFillManager.fillReport(reportPath, params, new JRBeanCollectionDataSource(dataProvider)));
		}
		catch (Exception e) 
		{  
			logger.error("Não foi possível gerar o relatório", e);
			throw e;
		}
		finally
		{
			if (reportBytes != null && reportBytes.length > 0) 
			{    
				response.setContentType("application/pdf");  
				response.setContentLength(reportBytes.length);  
				ServletOutputStream ouputStream = response.getOutputStream();  
				ouputStream.write(reportBytes, 0, reportBytes.length);  
				ouputStream.flush();  
				ouputStream.close();  
			}
			
			logo = null;
			
			dataProvider.clear();
			dataProvider = null;
			
			ordainedList.clear();
			ordainedList = null;
			
			if(conn != null)
				conn.close();
			
			if(manager != null && manager.isOpen())
				manager.close();
			
			conn = null; 
			reportBytes = null;
			
			System.gc();	
		}
	}
	
	final Boolean verificarEquivalenciaEmpresa(String empresa1, String empresa2)
	{
		Boolean existeEmpresa1 = false;
		Boolean existeEmpresa2 = false;
		
		for(String empresas : empresasEquivalentes)
		{
			for(String empresa : empresas.split(","))
			{
				if(empresa.equals(empresa1))
					existeEmpresa1 = true;
				if(empresa.equals(empresa2))
					existeEmpresa2 = true;
			}
			
			if(existeEmpresa1 && existeEmpresa2)
				return true;
			
			existeEmpresa1 = false;
			existeEmpresa2 = false;
		}
		
		return false;
	}
}