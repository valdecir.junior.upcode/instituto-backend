package br.com.desenv.nepalign.relatorios;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRTextExporterParameter;

import org.apache.log4j.Logger;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao.Grupo;
import br.com.desenv.nepalign.model.Cidade;
import br.com.desenv.nepalign.model.Cliente;
import br.com.desenv.nepalign.model.DuplicataAcordo;
import br.com.desenv.nepalign.model.DuplicataParcela;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.Impressora;
import br.com.desenv.nepalign.model.ItensRelatorioAcionamento;
import br.com.desenv.nepalign.model.ParametroSistema;
import br.com.desenv.nepalign.model.SeprocadoReativados;
import br.com.desenv.nepalign.service.CidadeService;
import br.com.desenv.nepalign.service.ClienteService;
import br.com.desenv.nepalign.service.DuplicataAcordoService;
import br.com.desenv.nepalign.service.DuplicataParcelaService;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.ImpressoraService;
import br.com.desenv.nepalign.service.ItensRelatorioAcionamentoService;
import br.com.desenv.nepalign.service.ParametroSistemaService;
import br.com.desenv.nepalign.service.SeprocadoReativadosService;
import br.com.desenv.nepalign.service.UsuarioService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.util.DsvImpressaoMatricial;
import br.com.desenv.nepalign.util.DsvImpressoraMatricial;
import br.com.desenv.nepalign.util.JRTextExporter;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class RelatorioCliente {
	String caminho;
	private static final long serialVersionUID = 1L;
	public Connection conn;
	private HttpServletRequest request;
	private HttpServletResponse response;
	private final static Logger logRelatorio = Logger.getLogger(IgnRelatorioSegurancaServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()

	 */
	public RelatorioCliente(HttpServletRequest request, HttpServletResponse response) throws ServletException, Exception
	{
		this.request=request;
		this.response=response;
	}

	public void gerarRelatorio() throws ServletException, IOException
	{
		byte[] bytes = null;

		try {  
			//Obtém a conexão com o banco    
			Connection conn;    

			conn = ConexaoUtil.getConexaoPadrao();  

			String sql = "SELECT cli.idCliente, cli.nome as 'clienteNome', cli.cpfcnpj, cli.idCidadeEndereco, cli.logradouro, cli.rg, cli.cep, cid.nome as 'cidadeNome'"
					+" FROM cliente cli"+" INNER JOIN Cidade cid ON cli.idCidadeEndereco = cid.idCidade";    
			java.sql.Statement rec = conn.createStatement();    
			ResultSet rs = rec.executeQuery(sql);    


			caminho = "relatorioPadrao.jasper"; 


			Statement stm = conn.createStatement();

			JRResultSetDataSource jrRS = new JRResultSetDataSource(rs) ;


			String rel = ("C:/Desenv/workspace/nepalign/WebContent/relatorios/" +caminho ); 
			Image logo = null;
			InputStream inputStreamDaImagem = null;   
			String caminhoImagem = "C:/Desenv/workspace/nepalign/WebContent/imagem/logo.jpg";//recebe o caminho da imagem  
			try {    
				File file = new File(caminhoImagem);   

				if(file.exists())//testa se imagem existe  
					inputStreamDaImagem = new BufferedInputStream(
							new FileInputStream(caminhoImagem));

				logo = ImageIO.read(inputStreamDaImagem);
			} catch (FileNotFoundException e) {       
				e.printStackTrace();       
			}       


			Map<String, Object> params = new HashMap<String, Object>();  

			params.put("logo", logo);  

			//JasperReport relatorioContrato = (JasperReport)JRLoader.loadObjectFromFile( rel );  
			JasperPrint impressao = JasperFillManager.fillReport(rel, params,jrRS);            
			// lendo arquivo jasper    
			//File relatorioContrato = new File(rel);             



			bytes = JasperExportManager.exportReportToPdf(impressao);  


		}catch (JRException | SQLException e) {  
			throw new ServletException(e);  
		}  

		if (bytes != null && bytes.length > 0) {  
			// envia o relatário em formato PDF para o browser  
			response.setContentType("application/pdf");  
			response.setContentLength(bytes.length);  
			ServletOutputStream ouputStream = response.getOutputStream();  
			ouputStream.write(bytes, 0, bytes.length);  
			ouputStream.flush();  
			ouputStream.close();  
		}   
	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Caixa, Grupo.Crediario}, nomeAcao="Gerar Relatório Cliente Email Geral")
	public void gerarRelatorioClientesEmailGeral() throws Exception {
		byte[] bytes = null;

		try {
			Connection conn = ConexaoUtil.getConexaoPadrao();

			Integer idEmpresaFisica = Integer.parseInt(request.getParameter("idEmpresaFisica"));
			EmpresaFisica empresaFisica = new EmpresaFisica();

			if(idEmpresaFisica != null) {
				empresaFisica = new EmpresaFisicaService().recuperarPorId(idEmpresaFisica);
			}

			String nomeEmpresa = "F" + empresaFisica.getId() + " - " + empresaFisica.getRazaoSocial() + "";

			String diaInicial = request.getParameter("diaInicial");
			String mesInicial = request.getParameter("mesInicial");
			String diaFinal = request.getParameter("diaFinal");
			String mesFinal = request.getParameter("mesFinal");

			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append(" c.idCliente AS idCliente, ");
			sql.append(" c.nome AS nomeCliente, ");
			sql.append(" c.dataNascimento AS nascimento, ");
			sql.append(" CONCAT(LPAD(DAY(c.dataNascimento), 2, '0'), '/', LPAD(MONTH(c.dataNascimento), 2, '0')) AS diaMes, ");
			sql.append(" GROUP_CONCAT(LOWER(ec.email) SEPARATOR '; ') AS emails, ");
			sql.append(" c.telefoneResidencial,c.telefoneCelular01 ");

			sql.append(" FROM ");
			sql.append(" cliente c ");

			sql.append(" INNER JOIN emailcliente ec on ec.idcliente = c.idcliente ");

			sql.append("WHERE ");
			sql.append(" DATE_FORMAT(c.dataNascimento, '0000-%m-%d') >= '0000-" + mesInicial + "-" + diaInicial + "' ");
			sql.append("AND ");
			sql.append(" DATE_FORMAT(c.dataNascimento, '0000-%m-%d') <= '0000-" + mesFinal + "-" + diaFinal + "' ");

			sql.append("GROUP BY ");
			sql.append(" c.idCliente ");
			sql.append("ORDER BY ");
			sql.append(" c.nome ");

			logRelatorio.info("\nCLIENTES EMAIL GERAL\n"+ sql.toString());

			ResultSet rs = conn.createStatement().executeQuery(String.valueOf(sql));

			JRResultSetDataSource jrRS = new JRResultSetDataSource(rs) ;
			Image logo = null;
			InputStream imageInputStream = null;

			try {
				File file = new File(getClass().getResource("/imagens/logo.jpg").getPath());

				if(file.exists()) {
					imageInputStream = new BufferedInputStream(new FileInputStream(getClass().getResource("/imagens/logo.jpg").getPath()));
					logo = ImageIO.read(imageInputStream);
				}

			} finally {

				if(imageInputStream != null) {
					imageInputStream.close();
				}

			}

			Integer tipoRelatorio = Integer.parseInt(request.getParameter("tipoRelatorio"));

			Map<String, Object> params = new HashMap<String, Object>();
			if(tipoRelatorio==1)
				params.put("tituloRelatorio", "CLIENTES C/ E-MAIL - ANIVERSARIANTES");
			else
				params.put("tituloRelatorio", "CLIENTES C/ E-MAIL - PROMOCIONAL"); 
			params.put("nomeEmpresa", nomeEmpresa);
			params.put("logo", logo);

			URL urlSub = getClass().getResource("/relatorios/");
			params.put("SUBREPORT_DIR", urlSub.getPath());

			JasperPrint impressao = JasperFillManager.fillReport(getClass().getResource("/relatorios/relatorioClientesEmail.jasper").getPath(), params, jrRS);
			// lendo arquivo jasper    
			bytes = JasperExportManager.exportReportToPdf(impressao);  

		}catch (JRException | SQLException e) {  
			throw new ServletException(e);  
		}  

		if (bytes != null && bytes.length > 0) {  
			// envia o relatário em formato PDF para o browser  
			response.setContentType("application/pdf");  
			response.setContentLength(bytes.length);  
			ServletOutputStream ouputStream = response.getOutputStream();  
			ouputStream.write(bytes, 0, bytes.length);  
			ouputStream.flush();  
			ouputStream.close();  
		}

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		process(request, response);
	}*/

	public String getCaminho() {
		return caminho;
	}

	public void setCaminho(String caminho) {
		this.caminho = caminho;
	}

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		//service(request, response);


		PrintWriter out=response.getWriter();


		out.println("<html><head><title>RELATÓRIO SISCON - TESTE</title></head><body><h1>O RELATORIO DO SISCOM SERA EXIBIDO VIA SERVLET</h1></body></html>");

		out.close();
	}

	/*protected void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		PrintWriter out=response.getWriter();


		out.println("<html><head><title>RELATÓRIO SISCON - TESTE</title></head><body><h1>O RELATORIO DO SISCOM SERA EXIBIDO VIA SERVLET</h1></body></html>");

		out.close();
	}*/

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente}, nomeAcao="Gerar Relatório Seprocados")
	public void relatorioSeprocados() throws Exception
	{
		EntityManager manager = null;
		EntityTransaction transaction = null;
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			Integer diaSeprocar = DsvConstante.getParametrosSistema().get("diasSeprocar")==null?45:DsvConstante.getParametrosSistema().getInt("diasSeprocar");
			EmpresaFisica empresa = new EmpresaFisicaService().recuperarPorId(Integer.parseInt(request.getParameter("idEmpresaFisica")));
			Integer idEmpresaFisica = Integer.parseInt(request.getParameter("idEmpresaFisica"));
			ParametroSistema pa = new ParametroSistema();
			pa.setChave("ultimaDataSeprocados");
			pa.setEmpresaFisica(empresa);

			List<ParametroSistema> listparam = new ParametroSistemaService().listarPorObjetoFiltro(pa);
			ParametroSistema param =null;
			if(listparam.size()>0)
			{
				param = listparam.get(0);
			}
			String dataS = "";
			if(param!=null)
			{
				dataS  = new ParametroSistemaService().listarPorObjetoFiltro(pa).get(0).getValor();
			}
			SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
			Date ultimaPesquisa = null;
			if(dataS != null && !dataS.equals(""))
			{
				ultimaPesquisa = formatador.parse(dataS);
			}
			else
			{

				ParametroSistema novoParam = new ParametroSistema();
				novoParam.setChave("ultimaDataSeprocados");
				novoParam.setValor(formatador.format(new Date()));
				novoParam.setEmpresaFisica(new EmpresaFisicaService().recuperarPorId(idEmpresaFisica));
				new ParametroSistemaService().salvar(manager, transaction, novoParam);


			}

			String hjS = formatador.format(new Date());
			Date hj = formatador.parse(hjS);
			List<Cliente> listaCli = new ArrayList<>();

			if(ultimaPesquisa!=null)
			{
				if(hj.getTime()==ultimaPesquisa.getTime())
				{
					SeprocadoReativados srC = new SeprocadoReativados();
					srC.setDataNova(hj);
					srC.setSeprocadoReativado("S");
					srC.setEmpresaFisica(new EmpresaFisicaService().recuperarPorId(manager, idEmpresaFisica));
					List<SeprocadoReativados> listaSR = new SeprocadoReativadosService().listarPorObjetoFiltro(manager, srC, "cliente", "asc", null);
					for (int i = 0; i < listaSR.size(); i++) 
					{
						listaCli.add(listaSR.get(i).getCliente());	
					}
				}
				else
				{
					listaCli = new ClienteService().recuperarClienteParaSeprocar(ultimaPesquisa,idEmpresaFisica);
				}
			}
			else
			{
				listaCli = new ClienteService().recuperarClienteParaSeprocar(ultimaPesquisa,idEmpresaFisica);
			}
			byte[] bytes = null;

			try 
			{   
				String ids ="";
				for (int i = 0; i < listaCli.size(); i++) 
				{
					if(i==0)
					{
						ids = ""+ listaCli.get(i).getId();
					}
					else
					{
						ids = ids+","+ listaCli.get(i).getId();	
					}
				}
				String str ="";

				str = str + " SELECT concat(cli.idCliente,' - ',cli.nome) as cliente,cli.nomeMae,cli.nomePai,cli.logradouro,cli.logradouroComercial,cli.nomeConjuge "; 
				str = str + " ,cli.dataNascimento,cli.rgInscricaoEstadual,cli.cpfCnpj, CONCAT(cli.nomeMae,' / ',cli.nomePai) AS filiacao, cli.empresaTrabalha,cli.telefoneComercial, cli.telefoneResidencial, "; 
				str = str + " cli.bairroCliente,cli.estadoCivil,cli.profissao,cli.enderecoCliente,cli.logradouroComercial,cli.idCliente, "; 
				str = str + " cli.numeroEnderecoCliente,cli.numeroComercial,cid.nome as cid,uf.nome as uf,cidt.nome as cidt, uft.nome as uft "; 
				str = str + " FROM cliente cli "; 
				str = str + " left outer join cidade cid on cid.idCidade = cli.idCidadeEndereco "; 
				str = str + " left outer join uf uf on uf.idUf = cid.idUf "; 
				str = str + " left outer join cidade cidt on cidt.idCidade = cli.idCidadeComercial "; 
				str = str + " left outer join uf uft on uft.idUf = cidt.idUf "; 
				if(ids.equals(""))
				{
					str = str + " where cli.idCliente is null ";
				}
				else
				{
					str = str + " where cli.idCliente in ("+ids+") ";
				} 
				str = str +" order by cli.nome asc ";
				Connection conn;
				conn = ConexaoUtil.getConexaoPadrao(); 
				System.out.println(str);
				ResultSet rs = conn.createStatement().executeQuery(str);    
				URL urlArquivo = getClass().getResource("/relatorios/relatorioClienteSeprocados.jasper");
				String rel = urlArquivo.getPath();

				Image logo = null;

				String caminhoImagem = getClass().getResource("/imagens/logo.jpg").getPath();  

				try 
				{        
					if(new File(caminhoImagem).exists())
						logo = ImageIO.read(new BufferedInputStream(new FileInputStream(caminhoImagem)));
					else
						logo = null;
				}
				catch (FileNotFoundException e) 
				{       
					logo = null;
				}       

				JRResultSetDataSource ds = new JRResultSetDataSource(rs);     
				Map<String, Object> params = new HashMap<String, Object>();   
				params.put("logo", logo);
				if(ultimaPesquisa!=null)
				{
					params.put("dataInicial",formatador.format(ultimaPesquisa));  
				}
				else
				{
					params.put("dataInicial",""); 
				}
				params.put("REPORT_CONNECTION", ConexaoUtil.getConexaoPadrao());
				params.put("diasSeprocar", diaSeprocar);
				params.put("dataFinal", formatador.format(new Date()));
				params.put("tituloRelatorio", "Relatorio Seprocados");
				params.put("HABILITAR_DATA", "S");
				params.put("empresaDescricao",""+empresa.getRazaoSocial());  
				params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath());

				JasperPrint impressao = JasperFillManager.fillReport(rel, params,ds);            
				bytes = JasperExportManager.exportReportToPdf(impressao);    
			}
			catch (JRException e) 
			{  
				throw new ServletException(e);  
			}  

			if (bytes != null && bytes.length > 0) 
			{	  
				response.setContentType("application/pdf");  
				response.setContentLength(bytes.length);  
				ServletOutputStream ouputStream = response.getOutputStream();
				ouputStream.write(bytes, 0, bytes.length);  
				ouputStream.flush();  
				ouputStream.close();  
			} 

			for (int i = 0; i < listaCli.size(); i++) 
			{
				if(ultimaPesquisa==null)
				{
					SeprocadoReativados sr = new SeprocadoReativados();
					sr.setCliente(listaCli.get(i));
					sr.setDataNova(new Date());
					sr.setSeprocadoReativado("S");
					sr.setDataAntiga(listaCli.get(i).getDataEntradaScpc());
					sr.setEmpresaFisica(empresa);
					sr.setObservacao("Seprocado automaticamente pelo sistema");
					sr.setUsuario(new UsuarioService().recuperarSessaoUsuarioLogado());
					listaCli.get(i).setDataEntradaScpc(new Date());
					new ClienteService().atualizar(manager, transaction, listaCli.get(i));
					new SeprocadoReativadosService().salvar(manager, transaction, sr); 
				}
				else if(hj.getTime()>ultimaPesquisa.getTime())
				{
					SeprocadoReativados sr = new SeprocadoReativados();
					sr.setCliente(listaCli.get(i));
					sr.setDataNova(new Date());
					sr.setSeprocadoReativado("S");
					sr.setDataAntiga(listaCli.get(i).getDataEntradaScpc());
					sr.setEmpresaFisica(empresa);
					sr.setObservacao("Seprocado automaticamente pelo sistema");
					sr.setUsuario(new UsuarioService().recuperarSessaoUsuarioLogado());
					listaCli.get(i).setDataEntradaScpc(new Date());
					new ClienteService().atualizar(manager, transaction, listaCli.get(i));
					new SeprocadoReativadosService().salvar(manager, transaction, sr);
				}
			}
			String dataAlterar = formatador.format(new Date());
			if(dataS!=null && !dataS.equals("") )
			{
				String str = "";
				str = str + " UPDATE parametrosistema SET  valor = '"+dataAlterar+"'";	
				str = str + "  WHERE chave = 'ultimaDataSeprocados' and  idEmpresaFisica =   "+request.getParameter("idEmpresaFisica"); 
				System.out.println(str);
				Query query = manager.createNativeQuery(str); 
				query.executeUpdate();
			}

			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente}, nomeAcao="Gerar Relatório Reativados")
	public void gerarReativados() throws NumberFormatException, Exception
	{
		EntityManager manager = null;
		EntityTransaction transaction = null;
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			byte[] bytes = null;
			Integer diaSeprocar = DsvConstante.getParametrosSistema().get("diasSeprocar")==null?45:DsvConstante.getParametrosSistema().getInt("diasSeprocar");
			List<Cliente> listaCli = new ArrayList<>();
			EmpresaFisica empresa = new EmpresaFisicaService().recuperarPorId(Integer.parseInt(request.getParameter("idEmpresaFisica")));

			Integer idEmpresaFisica = Integer.parseInt(request.getParameter("idEmpresaFisica"));

			ParametroSistema pa = new ParametroSistema();
			pa.setChave("ultimaDataReativados");
			pa.setEmpresaFisica(empresa);
			List<ParametroSistema> listparam = new ParametroSistemaService().listarPorObjetoFiltro(pa);
			ParametroSistema param =null;
			if(listparam.size()>0)
			{
				param = listparam.get(0);
			}
			String dataS = "";
			if(param!=null)
			{
				dataS  = new ParametroSistemaService().listarPorObjetoFiltro(pa).get(0).getValor();
			}

			SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat formatadorSql = new SimpleDateFormat("yyyy-MM-dd");
			Date ultimaPesquisa = null;
			if(dataS != null && !dataS.equals(""))
			{
				ultimaPesquisa = formatador.parse(dataS);
			}
			else
			{

				ParametroSistema novoParam = new ParametroSistema();
				novoParam.setChave("ultimaDataReativados");
				novoParam.setValor(formatador.format(new Date()));
				novoParam.setEmpresaFisica(new EmpresaFisicaService().recuperarPorId(idEmpresaFisica));
				new ParametroSistemaService().salvar(manager, transaction, novoParam);

			}
			String hjS = formatadorSql.format(new Date());
			String dataInicial = "";
			if(ultimaPesquisa != null)
			{
				dataInicial = formatadorSql.format(ultimaPesquisa);	
			}


			String strs = "";
			strs = strs + " SELECT se.idSeprocadosReativados as id FROM seprocadosreativados se "; 
			strs = strs + " where se.reativadoSeprocado = 'R' "; 
			strs = strs + " and se.dataNova <= '"+hjS+" 23:59:59'"; 
			if(!dataInicial.equals(""))
			{
				strs = strs + " and se.dataNova >= '"+dataInicial+" 00:00:00'"; 	
			}

			strs = strs + " and se.idEmpresaFisica = "+idEmpresaFisica; 
			conn = ConexaoUtil.getConexaoPadrao(); 
			ResultSet rs1 = conn.createStatement().executeQuery(strs); 

			while (rs1.next()) 
			{
				SeprocadoReativados sepro = new SeprocadoReativadosService().recuperarPorId(rs1.getInt("id"));
				listaCli.add(sepro.getCliente());
			}
			try 
			{       
				String ids ="";
				for (int i = 0; i < listaCli.size(); i++) 
				{
					if(i==0)
					{
						ids = ""+ listaCli.get(i).getId();
					}
					else
					{
						ids = ids+","+ listaCli.get(i).getId();	
					}
				}
				String str ="";


				str = str + " SELECT concat(cli.idCliente,' - ',cli.nome) as cliente,cli.nomeMae,cli.nomePai,cli.logradouro,cli.logradouroComercial,cli.nomeConjuge "; 
				str = str + " ,cli.dataNascimento,cli.rgInscricaoEstadual,cli.cpfCnpj,cli.idCliente, CONCAT(cli.nomeMae,' / ',cli.nomePai) AS filiacao, cli.empresaTrabalha,cli.telefoneComercial, cli.telefoneResidencial, "; 
				str = str + " cli.bairroCliente,cli.estadoCivil,cli.profissao,cli.enderecoCliente,cli.logradouroComercial, "; 
				str = str + " cli.numeroEnderecoCliente,cli.numeroComercial,cid.nome as cid,uf.nome as uf,cidt.nome as cidt, uft.nome as uft "; 
				str = str + " FROM cliente cli "; 
				str = str + " left outer join cidade cid on cid.idCidade = cli.idCidadeEndereco "; 
				str = str + " left outer join uf uf on uf.idUf = cid.idUf "; 
				str = str + " left outer join cidade cidt on cidt.idCidade = cli.idCidadeComercial "; 
				str = str + " left outer join uf uft on uft.idUf = cidt.idUf ";
				if(ids.equals(""))
				{
					str = str + " where cli.idCliente is null ";
				}
				else
				{
					str = str + " where cli.idCliente in ("+ids+") ";
				}

				str = str + " order by cli.nome asc ";
				Connection conn;
				conn = ConexaoUtil.getConexaoPadrao(); 
				System.out.println(str);
				ResultSet rs = conn.createStatement().executeQuery(str);    
				URL urlArquivo = getClass().getResource("/relatorios/relatorioClienteSeprocados.jasper");
				String rel = urlArquivo.getPath();
				Image logo = null;

				String caminhoImagem = getClass().getResource("/imagens/logo.jpg").getPath();  

				try 
				{        
					if(new File(caminhoImagem).exists())
						logo = ImageIO.read(new BufferedInputStream(new FileInputStream(caminhoImagem)));
					else
						logo = null;
				}
				catch (FileNotFoundException e) 
				{       
					logo = null;
				}       

				Map<String, Object> params = new HashMap<String, Object>();   
				params.put("logo", logo);
				params.put("diasSeprocar", diaSeprocar);
				params.put("dataInicial",ultimaPesquisa==null?"":formatador.format(ultimaPesquisa));
				params.put("dataFinal", formatador.format(new Date()));
				params.put("tituloRelatorio", "Relatório Reativados");
				params.put("HABILITAR_DATA", "S");
				params.put("empresaDescricao",""+empresa.getRazaoSocial());
				params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath());
				params.put("REPORT_CONNECTION", ConexaoUtil.getConexaoPadrao());
				JRResultSetDataSource ds = new JRResultSetDataSource(rs);     

				JasperPrint impressao = JasperFillManager.fillReport(rel, params,ds);            
				bytes = JasperExportManager.exportReportToPdf(impressao);       
			}
			catch (JRException e) 
			{  
				throw new ServletException(e);  
			}  

			if (bytes != null && bytes.length > 0) 
			{	  
				response.setContentType("application/pdf");  
				response.setContentLength(bytes.length);  
				ServletOutputStream ouputStream = response.getOutputStream();
				ouputStream.write(bytes, 0, bytes.length);  
				ouputStream.flush();  
				ouputStream.close();  
			} 
			if(dataS!=null && !dataS.equals("") )
			{
				String str = "";
				str = str + " UPDATE parametrosistema SET  valor = '"+formatador.format(new Date())+"'";	
				str = str + "  WHERE chave = 'ultimaDataReativados' and  idEmpresaFisica =   "+request.getParameter("idEmpresaFisica"); 

				Query query = manager.createNativeQuery(str); 
				query.executeUpdate();
			}
			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Posição Cliente")
	public void gerarRelatorioPosicaoCliente() throws Exception
	{
		Map<String, Object> params = new HashMap<String, Object>(); 
		Connection con = ConexaoUtil.getConexaoPadrao();    
		Statement stm = con.createStatement();
		InputStream imageInputStream = null;
		Image logo = null; 
		Date dataSeralle = DsvConstante.getParametrosSistema().get("dataSeralle")==null?null:new SimpleDateFormat("dd/MM/yyyy").parse(DsvConstante.getParametrosSistema().get("dataSeralle"));
		SimpleDateFormat dfsql = new SimpleDateFormat("yyyy-MM-dd");
		String str = "";


		str = str + " select par.numeroDuplicata ,par.numeroParcela,par.dataVencimento,par.valorParcela, "; 
		str = str + " CASE par.situacao "; 
		str = str + " when '0' then 'ABERTO' "; 
		str = str + " when '1' then 'BAIXADO' "; 
		str = str + " when '2' then 'PARCIAL' ";  
		str = str + " when '3' then 'BAIX.ACORDO' "; 
		str = str + " END as situacao,par.valorPago,par.dataBaixa,par.valorDesconto,par.jurosInformado,if(par.jurosPago is null,0,par.jurosPago) as jurosPago, "; 
		str = str + " concat(cli.idCliente,' - ',cli.nome) as cliente,concat(emp.idEmpresafisica,'  ',emp.razaoSocial ) as empresa,par.valorDesconto,emp.idempresafisica ";
		str = str + " ,if(par.situacao='0',DATEDIFF(current_date,par.datavencimento), if(par.situacao='1' or par.situacao='3' ,DATEDIFF(par.dataBaixa , par.dataVencimento),DATEDIFF(current_date, par.dataBaixa ))) as diasAtraso ";
		str = str + " ,if(historico.alerta=1,historico.observacao,null) as alerta ";
		str = str + " ,concat(coalesce(ven.codigoVendedorCache,ven.idVendedor),' - ',ven.nome ) as vendedor ,cli.mediaAtraso,du.idduplicata,par.idduplicataparcela,";
		str = str + " calculo_juros_duplicata (par.valorParcela,par.dataVencimento,emp.taxaJuros,par.situacao,par.valorPago,par.dataBaixa,par.jurosPago) as juros, par.acordo as acordo,par.dataVencimento02Acordado as 'segundoVenc', ";
		str = str + "concat('LOJA',emp.idEmpresaFisica) as loja ";

		str = str + " from duplicataParcela par  "; 
		str = str + " inner join cliente cli on cli.idCliente = par.idCliente "; 
		str = str + " left outer join historicocobranca historico on historico.idcliente = cli.idcliente ";
		str = str + " inner join empresafisica emp on emp.idEmpresaFisica = par.idEmpresaFisica "; 
		str = str + " inner join duplicata du on du.idDuplicata = par.idDuplicata "; 
		str = str + " left outer join vendedor ven on ven.idVendedor = du.idVendedor "; 
		str = str + " where par.idcliente =  "+request.getParameter("idCliente");
		str = str + " group by par.idDuplicataparcela ";
		if(request.getParameter("ordernarData")!=null && request.getParameter("ordernarData").equals("S") ) 
			str = str + " order by par.dataVencimento asc  "; 
		if(request.getParameter("ordernarFatura")!=null && request.getParameter("ordernarFatura").equals("S") )
			str = str + " order by du.numeroDuplicata asc  "; 

		System.out.println(str);
		logRelatorio.info("\nPOSIÇÃO CLIENTE\n"+ str);


		ResultSet rs = stm.executeQuery(String.valueOf(str));
		Double valorAPagar = 0.0;
		int maiorAtraso = 0;
		List<Integer> listaIds = new ArrayList<>();


		while(rs.next())
		{
			listaIds.add(rs.getInt("idDuplicata"));
			if(maiorAtraso<rs.getInt("diasAtraso"))
			{
				maiorAtraso = rs.getInt("diasAtraso");
			}
			if(rs.getString("situacao").equals("Aberto")||rs.getString("situacao").equals("Parcial"))
			{
				DuplicataParcela par = new DuplicataParcelaService().recuperarPorId(rs.getInt("idduplicataparcela"));
				Double valorApagarPar = new DuplicataParcelaService().retornaValorParcelaComJuros(par);
				valorAPagar = valorAPagar+ (valorApagarPar-rs.getDouble("valorPago"));
			}
		}
		System.out.println(maiorAtraso);
		rs.beforeFirst();

		List<Integer> lista = new ArrayList<>();

		for (int i = 0; i < listaIds.size(); i++)
		{
			Boolean permetido = true;
			Integer id = listaIds.get(i);

			for(int x = 0; x < lista.size(); x++)
			{
				if(lista.get(x).equals(id))
					permetido = false;	
			}

			if(permetido)
				lista.add(id);

		}
		int numeroCompra  = 0;
		for (int i = 0; i < lista.size(); i++)
		{
			numeroCompra++; 	
		}
		JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);
		String strLog = "";

		strLog = strLog + " SELECT log.*, "; 
		strLog = strLog + " if(log.nomeClasse = ' Duplicata',du.numeroDuplicata,null) as des,  "; 
		strLog = strLog + " if(log.nomeClasse = ' Duplicata Parcela',concat(dp.numeroDuplicata,'/',dp.numeroparcela),null) as des,   "; 
		strLog = strLog + " if(log.nomeClasse = ' Movimento Parcela Duplicata',concat(mov.numeroDuplicata,'/',mov.numeroparcela),null) as des,   "; 
		strLog = strLog + " if(log.nomeClasse = ' Pedido Venda',pv.numeroControle ,null) as des   "; 
		strLog = strLog + " FROM logduplicata log  "; 
		strLog = strLog + " left outer join duplicataparcela dp on dp.idDuplicataParcela = log.idDocumentoOrigem "; 
		strLog = strLog + " left outer join duplicata du on du.numeroDuplicata = log.idDocumentoOrigem "; 
		strLog = strLog + " left outer join movimentoparcelaDuplicata mov on mov.idmovimentoparcelaDuplicata = log.idDocumentoOrigem "; 
		strLog = strLog + " left outer join pedidovenda pv on pv.idPedidoVenda = log.idDocumentoOrigem "; 
		strLog = strLog + " where log.idCliente = "+request.getParameter("idCliente");



		con = ConexaoUtil.getConexaoPadrao();    
		Statement stmLog = con.createStatement();
		ResultSet rsLog = stmLog.executeQuery(strLog);
		JRResultSetDataSource jrRSLogDS = new JRResultSetDataSource(rsLog);
		try
		{    
			File file = new File(getClass().getResource("/imagens/logo.jpg").getPath());  
			if(file.exists())
			{ 
				imageInputStream = new BufferedInputStream(new FileInputStream(getClass().getResource("/imagens/logo.jpg").getPath()));
				logo = ImageIO.read(imageInputStream);
			} 
		}
		finally
		{
			if(imageInputStream != null)
				imageInputStream.close();
		}

		Cliente cliente = new ClienteService().recuperarPorId(Integer.parseInt(request.getParameter("idCliente")));
		if(cliente!=null)
		{
			params.put("nomeCliente",cliente.getId()+"-"+cliente.getNome());
		}

		Cliente cli = new ClienteService().recuperarPorId(Integer.parseInt(request.getParameter("idCliente")));
		Boolean seprocado = new SeprocadoReativadosService().verificarClienteSeprocado(cli);
		String alerta = "";
		alerta = new ClienteService().getAlertaCliente(cli);
		params.put("Seprocado", seprocado);
		params.put("logo", logo);
		params.put("tituloRelatorio", "Relatorio Posição Cliente");
		params.put("HABILITAR_DATA", "N");
		params.put("LogDS", jrRSLogDS);
		params.put("maiorAtraso",""+ maiorAtraso);
		params.put("valorEmAberto", valorAPagar);
		params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath());
		params.put("numeroCompra",""+ numeroCompra);
		params.put("alerta", alerta);
		JasperPrint impressao = JasperFillManager.fillReport(getClass().getResource("/relatorios/relatorioPosicaoCliente.jasper").getPath(), params, jrRS);                          
		byte[] bytes = JasperExportManager.exportReportToPdf(impressao);  

		this.response.setContentLength(bytes.length);
		this.response.setContentType("application/pdf");
		this.response.getOutputStream().write(bytes, 0, bytes.length);     	
		this.response.getOutputStream().flush();
		this.response.getOutputStream().close();

	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Clientes VIPS")
	public void gerarRelatorioClientesVips() throws Exception
	{
		byte[] bytes = null;
		try
		{
			///Teste

			Map<String, Object> params = new HashMap<String, Object>(); 
			Connection con = ConexaoUtil.getConexaoPadrao();    
			Statement stm = con.createStatement();
			InputStream imageInputStream = null;
			Image logo = null; 
			Integer mediaAtraso = Integer.parseInt(request.getParameter("iMediaAtraso"));
			Integer qtdeCompras = Integer.parseInt(request.getParameter("iQtdeCompras"));
			String tipoCliente = request.getParameter("tipoCliente")==null?null:request.getParameter("tipoCliente");
			String nomeEmpresa = "";
			boolean ordenadoPorCliente = Boolean.parseBoolean(request.getParameter("ordenadoPorCliente"));
			boolean franquia = Boolean.parseBoolean(request.getParameter("franquia"));

			StringBuilder sql = new StringBuilder();
			sql.append(" select concat('F',emp.idEmpresaFisica,'-',emp.razaoSocial) as nomeEmpresa, ");
			sql.append(" cli.mediaAtraso as mediaAtraso, count(dup.idDuplicata) as QtdCompra, cli.idCliente, cli.nome, cli.logradouro, cli.bairro, cli.numeroEndereco,");
			sql.append(" cli.telefoneResidencial, cli.telefoneCelular01,cli.telefoneComercial, cli.telefoneCliente, ");
			sql.append(" CONCAT(cli.logradouro, ' ',cli.numeroEndereco,'   ', cli.bairro, '     ',cid.nomeSemAcento) as enderecoCliente ");
			sql.append(" from duplicata dup ");
			sql.append(" inner join cliente cli on cli.idCliente = dup.idCliente ");
			sql.append(" inner join cidade cid on cid.idCidade = cli.idCidadeEndereco ");
			sql.append(" inner join empresaFisica emp on emp.idempresafisica = dup.idEmpresaFisica ");
			if(request.getParameter("iIdEmpresa") != null && request.getParameter("iIdEmpresa") != null)
				sql.append(" where emp.idEmpresaFisica= " + request.getParameter("iIdEmpresa") + "");

			if(mediaAtraso != null)
				sql.append(" and mediaAtraso <= " + mediaAtraso);

			if(tipoCliente != null && tipoCliente.equals("1"))
				sql.append(" and dup.dataCompra < '2013-10-01 23:59:59'");
			if(tipoCliente != null && tipoCliente.equals("2"))
				sql.append(" and dup.dataCompra >= '2013-10-01 00:00:00'");


			sql.append(" group by cli.idCliente ");
			if(qtdeCompras !=null)
				sql.append(" having count(dup.idDuplicata) >= " + qtdeCompras);

			if (ordenadoPorCliente) {
				sql.append(" ORDER BY cli.nome ");
			} else {
				sql.append(" order by count(dup.idDuplicata) desc ");
			}

			System.out.println(sql);
			ResultSet rs = stm.executeQuery(String.valueOf(sql));
			JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);


			try
			{    
				File file = new File(getClass().getResource("/imagens/logo.jpg").getPath());  
				if(file.exists())
				{ 
					imageInputStream = new BufferedInputStream(new FileInputStream(getClass().getResource("/imagens/logo.jpg").getPath()));
					logo = ImageIO.read(imageInputStream);
				} 
			}
			finally
			{
				if(imageInputStream != null)
					imageInputStream.close();
			}

			if(rs.next())
			{
				nomeEmpresa = rs.getString("nomeEmpresa");
			}
			rs.beforeFirst();
			caminho = "relatorioRelacaoClientesVip.jasper"; 

			params.put("logo", logo);
			params.put("empresaDescricao",nomeEmpresa);
			params.put("qtdeCompras", qtdeCompras);
			params.put("mediaAtraso", mediaAtraso);
			params.put("tituloRelatorio", "Clientes VIP"); 

			URL urlArquivo = getClass().getResource("/relatorios/"+caminho);
			String rel = urlArquivo.getPath();

			JasperPrint impressao = JasperFillManager.fillReport(rel, params,jrRS);            

			bytes = JasperExportManager.exportReportToPdf(impressao);  

		}catch (JRException | SQLException e) {  
			throw new ServletException(e);  
		}  

		if (bytes != null && bytes.length > 0) {  
			// envia o relatário em formato PDF para o browser  
			response.setContentType("application/pdf");  
			response.setContentLength(bytes.length);  
			ServletOutputStream ouputStream = response.getOutputStream();  
			ouputStream.write(bytes, 0, bytes.length);  
			ouputStream.flush();  
			ouputStream.close();  
		}   
	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Clientes VIPS Franquia")
	public void gerarRelatorioClientesVipsFranquia() throws Exception
	{
		byte[] bytes = null;
		try
		{


			Map<String, Object> params = new HashMap<String, Object>(); 
			Connection con = ConexaoUtil.getConexaoPadrao();    
			Statement stm = con.createStatement();
			InputStream imageInputStream = null;
			Image logo = null; 
			Integer mediaAtraso = Integer.parseInt(request.getParameter("iMediaAtraso"));
			Integer qtdeCompras = Integer.parseInt(request.getParameter("iQtdeCompras"));
			String tipoCliente = request.getParameter("tipoCliente")==null?null:request.getParameter("tipoCliente");
			String nomeEmpresa = "";
			boolean ordenadoPorCliente = Boolean.parseBoolean(request.getParameter("ordenadoPorCliente"));
			StringBuilder sql = new StringBuilder();
			sql.append(" select concat('F',emp.idEmpresaFisica,'-',emp.razaoSocial) as nomeEmpresa,  cli.mediaAtraso as mediaAtraso,  ");
			sql.append(" count(item.iditempedidovenda) as QtdCompra, cli.idCliente, cli.nome, cli.logradouro, cli.bairro, cli.numeroEndereco, ");
			sql.append("  cli.telefoneResidencial, cli.telefoneCelular01,cli.telefoneComercial, cli.telefoneCliente,   ");
			sql.append("  CONCAT(cli.logradouro, ' ',cli.numeroEndereco,'   ', cli.bairro, '     ',cid.nomeSemAcento) as enderecoCliente  ");
			sql.append("  from itempedidovenda item ");
			sql.append("  inner join pedidovenda pv on item.idpedidovenda = pv.idpedidovenda ");
			sql.append("  inner join cliente cli on cli.idCliente = pv.idCliente   ");
			sql.append("  inner join cidade cid on cid.idCidade = cli.idCidadeEndereco   ");
			sql.append("  inner join empresaFisica emp on emp.idempresafisica = pv.idEmpresaFisica  ");
			sql.append("  inner join produto p on p.idproduto = item.idproduto ");

			if(request.getParameter("iIdEmpresa") != null && request.getParameter("iIdEmpresa") != null)
				sql.append(" where emp.idEmpresaFisica= " + request.getParameter("iIdEmpresa") + "");

			if(mediaAtraso != null)
				sql.append(" and mediaAtraso <= " + mediaAtraso);

			if(tipoCliente != null && tipoCliente.equals("1"))
				sql.append(" and dup.dataCompra < '2013-10-01 23:59:59'");
			if(tipoCliente != null && tipoCliente.equals("2"))
				sql.append(" and dup.dataCompra >= '2013-10-01 00:00:00'");

			sql.append("  and p.idmarca in(select idmarca from marca where franquia = 1)");
			sql.append("  and cli.idCliente<>0");


			sql.append(" group by cli.idCliente ");
			if(qtdeCompras !=null)
				sql.append(" having count(item.iditempedidovenda) >= " + qtdeCompras);

			if (ordenadoPorCliente) {
				sql.append(" ORDER BY cli.nome ");
			} else {
				sql.append(" order by count(item.iditempedidovenda) desc ");
			}

			System.out.println(sql);
			ResultSet rs = stm.executeQuery(String.valueOf(sql));
			JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);


			try
			{    
				File file = new File(getClass().getResource("/imagens/logo.jpg").getPath());  
				if(file.exists())
				{ 
					imageInputStream = new BufferedInputStream(new FileInputStream(getClass().getResource("/imagens/logo.jpg").getPath()));
					logo = ImageIO.read(imageInputStream);
				} 
			}
			finally
			{
				if(imageInputStream != null)
					imageInputStream.close();
			}

			if(rs.next())
			{
				nomeEmpresa = rs.getString("nomeEmpresa");
			}
			rs.beforeFirst();
			caminho = "relatorioRelacaoClientesVip.jasper"; 

			params.put("logo", logo);
			params.put("empresaDescricao",nomeEmpresa);
			params.put("qtdeCompras", qtdeCompras);
			params.put("mediaAtraso", mediaAtraso);
			params.put("tituloRelatorio", "Clientes VIP"); 

			URL urlArquivo = getClass().getResource("/relatorios/"+caminho);
			String rel = urlArquivo.getPath();

			JasperPrint impressao = JasperFillManager.fillReport(rel, params,jrRS);            

			bytes = JasperExportManager.exportReportToPdf(impressao);  

		}catch (JRException | SQLException e) {  
			throw new ServletException(e);  
		}  

		if (bytes != null && bytes.length > 0) {  
			// envia o relatário em formato PDF para o browser  
			response.setContentType("application/pdf");  
			response.setContentLength(bytes.length);  
			ServletOutputStream ouputStream = response.getOutputStream();  
			ouputStream.write(bytes, 0, bytes.length);  
			ouputStream.flush();  
			ouputStream.close();  
		}   
	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Clientes Aniversário")
	public void gerarRelatorioClienteDataAniversario() throws Exception {
		Integer idEmpresa = Integer.parseInt(request.getParameter("idEmpresa"));
		String diaInicial = request.getParameter("dataInicial").substring(0, 2);
		String mesInicial = request.getParameter("dataInicial").substring(2, 4);
		String diaFinal = request.getParameter("dataFinal").substring(0, 2);
		String mesFinal = request.getParameter("dataFinal").substring(2, 4);
		EmpresaFisica empresaFisica = new EmpresaFisicaService().recuperarPorId(idEmpresa);
		String nomeEmpresa = "F"+ empresaFisica.getId() +" - "+ empresaFisica.getRazaoSocial() +"";

		String dataNascimentoInicial = "0000-" + mesInicial + "-" + diaInicial;
		String dataNascimentoFinal = "0000-" + mesFinal + "-" + diaFinal;

		//Integer diaAniversario = null;
		//Integer mesAniversario = null;
		Map<String, Object> params = new HashMap<String, Object>();
		Connection con = ConexaoUtil.getConexaoPadrao();
		Statement stm = con.createStatement();
		InputStream imageInputStream = null;
		Image logo = null; 

		String str = "";
		str = str + " select cliente.nome as nomeCliente, cliente.idCliente, cliente.dataNascimento, uf.nome as nomeUf, uf.sigla, cidade.nome as nomeCidade, ";
		str = str + " lpad(DAY(cliente.dataNascimento),2,'0') as diaNascimento,if(cliente.telefonecelular01 is null,cliente.telefoneResidencial,cliente.telefonecelular01 ) as telefonecelular01, ";
		str = str + " lpad(MONTH(cliente.dataNascimento),2,'0') as mesNascimento, ";
		str = str + " concat(lpad(DAY(cliente.dataNascimento),2,'0'), '/', lpad(MONTH(cliente.dataNascimento),2,'0')) as diaMes, ";
		str = str + " cliente.telefoneResidencial,cliente.telefoneCelular01, du.dataCompra ";

		str = str + " from cliente as cliente ";
		str = str + " left outer join cidade cidade on cidade.idCidade = cliente.idCidadeEndereco ";
		str = str + " inner join uf uf on uf.idUf = cidade.idUf ";
		str = str + " LEFT OUTER JOIN duplicata du on du.idcliente = cliente.idcliente " ;
		if(diaInicial != null && diaInicial != "" && mesInicial != null && mesInicial != "" && diaFinal != null && diaFinal != "" && mesFinal != null && mesFinal != "") 
			str = str + " where DATE_FORMAT(dataNascimento, '0000-%m-%d') between '" + dataNascimentoInicial + "' and '" + dataNascimentoFinal + "' ";
		str = str + " group by cliente.idCliente order by cliente.nome ";

		ResultSet rs = stm.executeQuery(String.valueOf(str));
		JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);

		try {
			File file = new File(getClass().getResource("/imagens/logo.jpg").getPath());
			if(file.exists()) {
				imageInputStream = new BufferedInputStream(new FileInputStream(getClass().getResource("/imagens/logo.jpg").getPath()));
				logo = ImageIO.read(imageInputStream);
			}
		}
		finally {
			if(imageInputStream != null) {
				imageInputStream.close();
			}
		}
		params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath()); 
		params.put("logo", logo);
		params.put("tituloRelatorio", "Relatorio Cliente/Data Aniversario");
		params.put("nomeEmpresa", nomeEmpresa);
		JasperPrint impressao = JasperFillManager.fillReport(getClass().getResource("/relatorios/relatorioClienteDataAniversario.jasper").getPath(), params, jrRS);
		byte[] bytes = JasperExportManager.exportReportToPdf(impressao);
		this.response.setContentLength(bytes.length);
		this.response.setContentType("application/pdf");
		this.response.getOutputStream().write(bytes, 0, bytes.length);
		this.response.getOutputStream().flush();
		this.response.getOutputStream().close();
	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Cliente que não compra")
	public void gerarRelatorioClienteQueNaoCompra() throws Exception {
		Integer idEmpresa = Integer.parseInt(request.getParameter("idEmpresa"));
		EmpresaFisica empresaFisica = new EmpresaFisicaService().recuperarPorId(idEmpresa);
		String nomeEmpresa = "F"+ empresaFisica.getId() +" - "+ empresaFisica.getRazaoSocial() +"";
		Integer quantidadeDiasInicial = Integer.parseInt(request.getParameter("quantidadeDiasInicial"));
		Integer quantidadeDiasFinal = Integer.parseInt(request.getParameter("quantidadeDiasFinal"));

		Map<String, Object> params = new HashMap<String, Object>();
		Connection con = ConexaoUtil.getConexaoPadrao();
		Statement stm = con.createStatement();
		InputStream imageInputStream = null;
		Image logo = null; 

		String str = "";
		str = str + " select cliente.idCliente, cliente.nome as nomeCliente, max(duplicata.dataCompra) as dataCompra, ";
		str = str + " DATEDIFF(current_date, max(dataCompra)) as quantDias, ";
		str = str + " if(cliente.telefoneCelular01 is not null,cliente.telefoneCelular01 , if(cliente.telefoneCelular02 is not null,cliente.telefoneCelular02,if(cliente.telefoneComercial is not null,cliente.telefoneComercial,cliente.telefoneResidencial))) as telefone ";
		str = str + " from duplicata as duplicata ";
		str = str + " inner join cliente as cliente on cliente.idCliente = duplicata.idCliente ";
		str = str + " left outer join duplicataparcela par on par.idcliente = cliente.idCliente and par.situacao in(0,2) and par.seprocadaReativada = 'S' ";
		str = str + " where par.idduplicataparcela is null ";
		str = str + " group by idCliente ";
		str = str + " having DATEDIFF(current_date,max(dataCompra)) >= '"+ quantidadeDiasInicial +"' and DATEDIFF(current_date,max(dataCompra)) <= '"+ quantidadeDiasFinal + "' ";
		str = str + " order by nomeCliente ";

		ResultSet rs = stm.executeQuery(String.valueOf(str));
		JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);

		try {
			File file = new File(getClass().getResource("/imagens/logo.jpg").getPath());
			if(file.exists()) {
				imageInputStream = new BufferedInputStream(new FileInputStream(getClass().getResource("/imagens/logo.jpg").getPath()));
				logo = ImageIO.read(imageInputStream);
			}
		}
		finally {
			if(imageInputStream != null) {
				imageInputStream.close();
			}
		}
		params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath()); 
		params.put("REPORT_CONNECTION",ConexaoUtil.getConexaoPadrao());
		params.put("logo", logo);
		params.put("tituloRelatorio", "Relatorio de Clientes que nao Compram");
		params.put("nomeEmpresa", nomeEmpresa);
		JasperPrint impressao = JasperFillManager.fillReport(getClass().getResource("/relatorios/relatorioClienteQueNaoCompra.jasper").getPath(), params, jrRS);
		byte[] bytes = JasperExportManager.exportReportToPdf(impressao);
		this.response.setContentLength(bytes.length);
		this.response.setContentType("application/pdf");
		this.response.getOutputStream().write(bytes, 0, bytes.length);
		this.response.getOutputStream().flush();
		this.response.getOutputStream().close();
	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Ficha de Acionamento2")
	public void gerarRelatorioFichaAcionamento() throws Exception
	{
		try
		{
			Date datainicio = new Date();
			Integer idEmpresa = Integer.parseInt(request.getParameter("idEmpresaFisica"));
			EmpresaFisica empresaFisica = new EmpresaFisicaService().recuperarPorId(idEmpresa);
			String nomeEmpresa = "F"+ empresaFisica.getId() +" - "+ empresaFisica.getRazaoSocial() +"";
			SimpleDateFormat dfa = new SimpleDateFormat ("dd/MM/yyyy");

			Date dInicial = null;
			String idCidade  = null;
			String diaAtrazFinal = null;
			String diaAtraz = null;
			Date def = null;
			Date dei = null;
			String cliente = null;
			if(request.getParameter("dataBase")!=null && !request.getParameter("dataBase").equals(""))
			{
				dInicial = dfa.parse(request.getParameter("dataBase"));
			}
			if(request.getParameter("diaAtraz")!=null && !request.getParameter("diaAtraz").equals("")){
				 diaAtraz = request.getParameter("diaAtraz");
			}
			if(request.getParameter("diaAtrazFinal")!=null && !request.getParameter("diaAtrazFinal").equals("")){
				diaAtrazFinal = request.getParameter("diaAtrazFinal");
			}
			if(request.getParameter("cidade")!=null && !request.getParameter("cidade").equals("")){
				idCidade = request.getParameter("cidade");
			}
			if(request.getParameter("dataEmissaoInicial")!=null && !request.getParameter("dataEmissaoInicial").equals("")){
				 dei = dfa.parse(request.getParameter("dataEmissaoInicial"));
			}
			if(request.getParameter("dataEmissaoFinal")!=null && !request.getParameter("dataEmissaoFinal").equals("")){
				 def = dfa.parse(request.getParameter("dataEmissaoFinal"));
			}
			if(request.getParameter("cliente")!= null && !request.getParameter("cliente").equals("") )
				cliente = request.getParameter("cliente");
			Map<String, Object> params = new HashMap<String, Object>();
			InputStream imageInputStream = null;
			Image logo = null; 
			ResultSet rsSQL = new ClienteService().relatorioFichaAcionamento(empresaFisica, dInicial,diaAtraz , diaAtrazFinal,dei , def, idCidade,cliente);

			
			JRResultSetDataSource jrRS = new JRResultSetDataSource(rsSQL);

			try {
				File file = new File(getClass().getResource("/imagens/logo.jpg").getPath());
				if(file.exists()) {
					imageInputStream = new BufferedInputStream(new FileInputStream(getClass().getResource("/imagens/logo.jpg").getPath()));
					logo = ImageIO.read(imageInputStream);
				}
			}
			finally {
				if(imageInputStream != null) {
					imageInputStream.close();
				}
			}

			params.put("logo", logo);
			params.put("tituloRelatorio", "Ficha Acionamento");
			params.put("empresaDescricao", nomeEmpresa);
			params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath()); 
			params.put("dataInicial", request.getParameter("dataBase"));
			params.put("dataFinal", "");

			JasperPrint impressao = JasperFillManager.fillReport(getClass().getResource("/relatorios/relatorioFichaAcionamento.jasper").getPath(), params, jrRS);

			try
			{
				
				byte[] bytes = JasperExportManager.exportReportToPdf(impressao);
				this.response.setContentLength(bytes.length);
				this.response.setContentType("application/pdf");
				this.response.getOutputStream().write(bytes, 0, bytes.length);
				this.response.getOutputStream().flush();
				this.response.getOutputStream().close();
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			Date datafinal = new Date();
			System.out.println(new IgnUtil().diferencaDatas(datainicio, datafinal, 1));
		}
		catch(Exception ex){

			ex.printStackTrace();
			throw ex;
		}
	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Etiqueta Postagem")
	public void gerarEtiquetaPostagem() throws Exception
	{

		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
		SimpleDateFormat dfa = new SimpleDateFormat ("dd/MM/yyyy");

		Date dInicial = null;
		String dataBase = "";
		Integer diasInicial = null;
		Integer diasFinal = null;
		if(request.getParameter("diasInicial")!=null && !request.getParameter("diasInicial").equals(""))
		{
			diasInicial = Integer.parseInt(request.getParameter("diasInicial"));
		}
		if(request.getParameter("diasFinal")!=null && !request.getParameter("diasFinal").equals(""))
		{
			diasFinal = Integer.parseInt(request.getParameter("diasFinal"));
		}
		if(request.getParameter("dataBase")!=null && !request.getParameter("dataBase").equals(""))
		{
			dInicial = dfa.parse(request.getParameter("dataBase"));
			dataBase =  df.format(dInicial);
		}

		Map<String, Object> params = new HashMap<String, Object>();
		Connection con = ConexaoUtil.getConexaoPadrao();
		Statement stm = con.createStatement();
		InputStream imageInputStream = null;
		Image logo = null; 

		String str = "";
		str = str + " select cli.idCliente,cli.nome,cli.logradouro, "; 
		str = str + "  cli.numeroEndereco as numero , "; 
		str = str + "  cli.bairro, "; 
		str = str + "  cid.nome as cidade, "; 
		str = str + "  uf.sigla as uf, "; 
		str = str + "  cli.cep as cep, "; 
		str = str + "  if(par.dataVencimento02Acordado is not null,par.dataVencimento02Acordado,par.dataVencimento) as dtVen, "; 
		str = str + "  par.numeroParcela, "; 
		str = str + "  par.numeroduplicata, d.dataCompra,par.valorParcela, "; 
		str = str + "   datediff(if(par.dataVencimento02Acordado is not null,par.dataVencimento02Acordado,par.dataVencimento),current_date()) as dias, "; 
		str = str + "   e.razaoSocial,e.endereco,e.numero as numEmp,e.nomeFantasia,e.telefone "; 
		str = str + " ,ce.nome as cidadeEmpresa,ufe.sigla as ufEmpresa "; 
		str = str + "  from duplicataparcela par "; 
		str = str + "  inner join cliente cli on cli.idCliente = par.idcliente "; 
		str = str + "  left join cidade cid on cid.idCidade = cli.idCidadeEndereco "; 
		str = str + "  left join uf uf on uf.idUf  = cid.idUf "; 
		str = str + "  inner join duplicata d on d.idDuplicata = par.idDuplicata "; 
		str = str + "  inner join empresafisica e on e.idEmpresaFisica = par.idEmpresaFisica "; 
		str = str + "  inner join cidade ce on ce.idCidade = e.idCidade "; 
		str = str + "  inner join uf ufe on ufe.idUf = ce.idUf "; 
		str = str + "  where par.situacao in(0,2) ";
		if(diasInicial!=null)
		{
			str = str + "  and datediff('"+dataBase+"',if(par.dataVencimento02Acordado is not null,par.dataVencimento02Acordado,par.dataVencimento))>="+diasInicial;
		}
		if(diasFinal != null)
		{
			str = str + "  and   datediff('"+dataBase+"',if(par.dataVencimento02Acordado is not null,par.dataVencimento02Acordado,par.dataVencimento))<="+diasFinal;
		}

		str = str + "  order by cli.idcliente "; 
		str = str + "  "; 
		System.out.println(str);
		ResultSet rs = stm.executeQuery(String.valueOf(str));
		JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);				

		try {
			File file = new File(getClass().getResource("/imagens/logo.jpg").getPath());
			if(file.exists()) {
				imageInputStream = new BufferedInputStream(new FileInputStream(getClass().getResource("/imagens/logo.jpg").getPath()));
				logo = ImageIO.read(imageInputStream);
			}
		}
		finally {
			if(imageInputStream != null) {
				imageInputStream.close();
			}
		}

		params.put("logo", logo);
		params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath()); 
		params.put("REPORT_CONNECTION",ConexaoUtil.getConexaoPadrao());
		params.put("data",dInicial );
		String arquivo = "";
		if(request.getParameter("etiqueta").equals("S"))
		{
			arquivo	= "etiquetaPostagem.jasper";
		}
		else
		{
			arquivo= "cartaCobranca.jasper";
		}
		JasperPrint impressao = JasperFillManager.fillReport(getClass().getResource("/relatorios/"+arquivo).getPath(), params, jrRS);
		byte[] bytes = JasperExportManager.exportReportToPdf(impressao);
		this.response.setContentLength(bytes.length);
		this.response.setContentType("application/pdf");
		this.response.getOutputStream().write(bytes, 0, bytes.length);
		this.response.getOutputStream().flush();
		this.response.getOutputStream().close();
	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Clientes sem foto")
	public void gerarRelatorioSemFotos() throws Exception
	{
		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
		SimpleDateFormat dfa = new SimpleDateFormat ("dd/MM/yyyy");

		Date dInicial = null;
		Date dataInicial = null;
		Date dataFinal = null;
		if(request.getParameter("dataInicial")!=null && !request.getParameter("dataInicial").equals(""))
		{
			dataInicial = dfa.parse(request.getParameter("dataInicial"));
		}
		if(request.getParameter("dataFinal")!=null && !request.getParameter("dataFinal").equals(""))
		{
			dataFinal =  dfa.parse(request.getParameter("dataFinal"));
		}

		Map<String, Object> params = new HashMap<String, Object>();
		Connection con = ConexaoUtil.getConexaoPadrao();
		Statement stm = con.createStatement();
		Image logo = null; 

		String str = "";
		str = str + " Select concat(c.idcliente,' - ',c.nome) as cliente, "; 
		str = str + " fotoCli.idImagemUsuario as foto ,  "; 
		str = str + " fotoCliRg.idImagemUsuario as rg, "; 
		str = str + " fotoCliVerso.idImagemUsuario as verso , "; 
		str = str + " fotoCliCpf.idImagemUsuario as cpf,  "; 
		str = str + " b.idBiometriaCliente  as biometria , c.dataHoraCadastro"; 
		str = str + " from cliente c "; 
		str = str + " left outer join biometriacliente b on b.idcliente = c.idCliente "; 
		str = str + "  "; 
		str = str + " left outer join imagemUsuario fotoCli 		on fotoCli.infoCampoChave = c.idCliente and fotoCli.campoChave = 'FOTO CLIENTE' "; 
		str = str + " left outer join imagemUsuario fotoCliRg 	on fotoCliRg.infoCampoChave = c.idCliente and fotoCliRg.campoChave = 'FOTO RG' "; 
		str = str + " left outer join imagemUsuario fotoCliVerso 	on fotoCliVerso.infoCampoChave = c.idCliente and fotoCliVerso.campoChave = 'FOTO RG VERSO' "; 
		str = str + " left outer join imagemUsuario fotoCliCpf 	on fotoCliCpf.infoCampoChave = c.idCliente and fotoCliCpf.campoChave = 'FOTO CPF' "; 
		str = str + "  "; 
		str = str + " where (fotoCli.idImagemUsuario is null or fotoCliRg.idImagemUsuario is null or fotoCliVerso.idImagemUsuario is null or fotoCliCpf.idImagemUsuario is null or b.idBiometriaCliente is null)  "; 
		str = str + " and c.dataHoraCadastro >=' "+df.format(dataInicial)+" 00:00:00' and c.dataHoraCadastro <= '"+df.format(dataFinal)+" 23:59:59' "; 
		str = str + " group by c.idcliente order by c.dataHoraCadastro desc";
		System.out.println(str);
		ResultSet rs = stm.executeQuery(String.valueOf(str));
		JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);				
		Image logoCorreto = null;
		Image logoErrado = null;
		InputStream inputStreamDaImagem = null;
		InputStream inputStreamDaImagemLogoErrado = null;
		InputStream inputStreamDaImagemLogoCorreto = null;
		URL urlImagens = getClass().getResource("/imagens/logo.jpg");
		URL urlLogoCorreto = getClass().getResource("/imagens/correto.png");
		URL urlLogoErrado = getClass().getResource("/imagens/errado.png");
		String caminhoImagem = urlImagens.getPath();
		String caminhoLogoErrado = urlLogoErrado.getPath();
		String caminhoLogoCorreto = urlLogoCorreto.getPath();

		try {    
			File fileLogo = new File(caminhoImagem);  
			File fileLogoErrado = new File(caminhoLogoErrado);  
			File fileLogoCorreto = new File(caminhoLogoCorreto);  
			if(fileLogo.exists() && fileLogoCorreto.exists() && fileLogoErrado.exists()){//testa se imagem existe  
				inputStreamDaImagem = new BufferedInputStream(new FileInputStream(caminhoImagem));
				inputStreamDaImagemLogoErrado = new BufferedInputStream(new FileInputStream(caminhoLogoErrado));
				inputStreamDaImagemLogoCorreto = new BufferedInputStream(new FileInputStream(caminhoLogoCorreto));
				logo = ImageIO.read(inputStreamDaImagem);
				logoErrado = ImageIO.read(inputStreamDaImagemLogoErrado);
				logoCorreto = ImageIO.read(inputStreamDaImagemLogoCorreto);
			}else{
				logo =null;
				logoCorreto = null;
				logoErrado = null;
			}


		} catch (FileNotFoundException e) {       
			//e.printStackTrace();
			logo =null;
			logoCorreto = null;
			logoErrado = null;
		}       

		params.put("logo", logo);  
		params.put("dataFinal", dfa.format(dataFinal));
		params.put("dataInicial",dfa.format(dataInicial)); 
		params.put("tituloRelatorio", "Relatorio Clientes Novos sem Fotos");
		params.put("logoCorreto", logoCorreto);  
		params.put("logoErrado", logoErrado);  
		params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath()); 
		params.put("REPORT_CONNECTION",ConexaoUtil.getConexaoPadrao());
		params.put("data",dInicial );
		String arquivo = "relatorioClienteSemFotos.jasper";
		JasperPrint impressao = JasperFillManager.fillReport(getClass().getResource("/relatorios/"+arquivo).getPath(), params, jrRS);
		byte[] bytes = JasperExportManager.exportReportToPdf(impressao);
		this.response.setContentLength(bytes.length);
		this.response.setContentType("application/pdf");
		this.response.getOutputStream().write(bytes, 0, bytes.length);
		this.response.getOutputStream().flush();
		this.response.getOutputStream().close();
	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Clientes com foto")
	public void gerarRelatorioComFotos() throws Exception
	{
		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
		SimpleDateFormat dfa = new SimpleDateFormat ("dd/MM/yyyy");

		Date dInicial = null;
		Date dataInicial = null;
		Date dataFinal = null;
		if(request.getParameter("dataInicial")!=null && !request.getParameter("dataInicial").equals(""))
		{
			dataInicial = dfa.parse(request.getParameter("dataInicial"));
		}
		if(request.getParameter("dataFinal")!=null && !request.getParameter("dataFinal").equals(""))
		{
			dataFinal =  dfa.parse(request.getParameter("dataFinal"));
		}

		Map<String, Object> params = new HashMap<String, Object>();
		Connection con = ConexaoUtil.getConexaoPadrao();
		Statement stm = con.createStatement();
		InputStream imageInputStream = null;
		Image logo = null; 

		String str = "";
		str = str + " Select concat(c.idcliente,' - ',c.nome) as cliente, "; 
		str = str + " fotoCli.idImagemUsuario as foto ,  "; 
		str = str + " fotoCliRg.idImagemUsuario as rg, "; 
		str = str + " fotoCliVerso.idImagemUsuario as verso , "; 
		str = str + " fotoCliCpf.idImagemUsuario as cpf,  "; 
		str = str + " b.idBiometriaCliente  as biometria , c.dataHoraCadastro"; 
		str = str + " from cliente c "; 
		str = str + " left outer join biometriacliente b on b.idcliente = c.idCliente "; 
		str = str + "  "; 
		str = str + " left outer join imagemUsuario fotoCli 		on fotoCli.infoCampoChave = c.idCliente and fotoCli.campoChave = 'FOTO CLIENTE' "; 
		str = str + " left outer join imagemUsuario fotoCliRg 	on fotoCliRg.infoCampoChave = c.idCliente and fotoCliRg.campoChave = 'FOTO RG' "; 
		str = str + " left outer join imagemUsuario fotoCliVerso 	on fotoCliVerso.infoCampoChave = c.idCliente and fotoCliVerso.campoChave = 'FOTO RG VERSO' "; 
		str = str + " left outer join imagemUsuario fotoCliCpf 	on fotoCliCpf.infoCampoChave = c.idCliente and fotoCliCpf.campoChave = 'FOTO CPF' "; 
		str = str + "  "; 
		str = str + " where (fotoCli.idImagemUsuario is not null AND fotoCliRg.idImagemUsuario is not null AND fotoCliVerso.idImagemUsuario is not null AND fotoCliCpf.idImagemUsuario is not null AND b.idBiometriaCliente is not null)  "; 
		str = str + " and c.dataHoraCadastro >=' "+df.format(dataInicial)+" 00:00:00' and c.dataHoraCadastro <= '"+df.format(dataFinal)+" 23:59:59' "; 
		str = str + " group by c.idcliente order by c.dataHoraCadastro desc";
		System.out.println(str);
		ResultSet rs = stm.executeQuery(String.valueOf(str));
		JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);				
		Image logoCorreto = null;
		Image logoErrado = null;
		InputStream inputStreamDaImagem = null;
		InputStream inputStreamDaImagemLogoErrado = null;
		InputStream inputStreamDaImagemLogoCorreto = null;
		URL urlImagens = getClass().getResource("/imagens/logo.jpg");
		URL urlLogoCorreto = getClass().getResource("/imagens/correto.png");
		URL urlLogoErrado = getClass().getResource("/imagens/errado.png");
		String caminhoImagem = urlImagens.getPath();
		String caminhoLogoErrado = urlLogoErrado.getPath();
		String caminhoLogoCorreto = urlLogoCorreto.getPath();

		try {    
			File fileLogo = new File(caminhoImagem);  
			File fileLogoErrado = new File(caminhoLogoErrado);  
			File fileLogoCorreto = new File(caminhoLogoCorreto);  
			if(fileLogo.exists() && fileLogoCorreto.exists() && fileLogoErrado.exists()){//testa se imagem existe  
				inputStreamDaImagem = new BufferedInputStream(new FileInputStream(caminhoImagem));
				inputStreamDaImagemLogoErrado = new BufferedInputStream(new FileInputStream(caminhoLogoErrado));
				inputStreamDaImagemLogoCorreto = new BufferedInputStream(new FileInputStream(caminhoLogoCorreto));
				logo = ImageIO.read(inputStreamDaImagem);
				logoErrado = ImageIO.read(inputStreamDaImagemLogoErrado);
				logoCorreto = ImageIO.read(inputStreamDaImagemLogoCorreto);
			}else{
				logo =null;
				logoCorreto = null;
				logoErrado = null;
			}


		} catch (FileNotFoundException e) {       
			//e.printStackTrace();
			logo =null;
			logoCorreto = null;
			logoErrado = null;
		}       

		params.put("logo", logo);  
		params.put("dataFinal", dfa.format(dataFinal));
		params.put("dataInicial",dfa.format(dataInicial)); 
		params.put("tituloRelatorio", "Relatorio Clientes Novos sem Fotos");
		params.put("logoCorreto", logoCorreto);  
		params.put("logoErrado", logoErrado);  
		params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath()); 
		params.put("REPORT_CONNECTION",ConexaoUtil.getConexaoPadrao());
		params.put("data",dInicial );
		String arquivo = "relatorioClienteSemFotos.jasper";
		JasperPrint impressao = JasperFillManager.fillReport(getClass().getResource("/relatorios/"+arquivo).getPath(), params, jrRS);
		byte[] bytes = JasperExportManager.exportReportToPdf(impressao);
		this.response.setContentLength(bytes.length);
		this.response.setContentType("application/pdf");
		this.response.getOutputStream().write(bytes, 0, bytes.length);
		this.response.getOutputStream().flush();
		this.response.getOutputStream().close();
	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Cliente com duplicata com foto")
	public void gerarRelatorioComFotosPorDuplciata() throws Exception
	{
		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
		SimpleDateFormat dfa = new SimpleDateFormat ("dd/MM/yyyy");

		Date dInicial = null;
		Date dataInicial = null;
		Date dataFinal = null;
		if(request.getParameter("dataInicial")!=null && !request.getParameter("dataInicial").equals(""))
		{
			dataInicial = dfa.parse(request.getParameter("dataInicial"));
		}
		if(request.getParameter("dataFinal")!=null && !request.getParameter("dataFinal").equals(""))
		{
			dataFinal =  dfa.parse(request.getParameter("dataFinal"));
		}

		Map<String, Object> params = new HashMap<String, Object>();
		Connection con = ConexaoUtil.getConexaoPadrao();
		Statement stm = con.createStatement();
		InputStream imageInputStream = null;
		Image logo = null; 

		String str = "";
		str = str + " Select concat(c.idcliente,' - ',c.nome) as cliente,  fotoCli.idImagemUsuario as foto ,  "; 
		str = str + "   fotoCliRg.idImagemUsuario as rg,  fotoCliVerso.idImagemUsuario as verso ,  "; 
		str = str + "   fotoCliCpf.idImagemUsuario as cpf,   b.idBiometriaCliente  as biometria , d.dataHoraInclusao as dataHoraCadastro  "; 
		str = str + "   from duplicata d "; 
		str = str + "   left outer join Cliente c on c.idCliente = d.idCliente "; 
		str = str + "   left outer join biometriacliente b on b.idcliente = c.idCliente  "; 
		str = str + "   left outer join imagemUsuario fotoCli 		on fotoCli.infoCampoChave = c.idCliente and fotoCli.campoChave = 'FOTO CLIENTE' "; 
		str = str + "   left outer join imagemUsuario fotoCliRg 	on fotoCliRg.infoCampoChave = c.idCliente and fotoCliRg.campoChave = 'FOTO RG'  "; 
		str = str + "   left outer join imagemUsuario fotoCliVerso 	on fotoCliVerso.infoCampoChave = c.idCliente and fotoCliVerso.campoChave = 'FOTO RG VERSO'   "; 
		str = str + "   left outer join imagemUsuario fotoCliCpf 	on fotoCliCpf.infoCampoChave = c.idCliente and fotoCliCpf.campoChave = 'FOTO CPF'    "; 
		str = str + " where (fotoCli.idImagemUsuario is not null AND fotoCliRg.idImagemUsuario is not null AND fotoCliVerso.idImagemUsuario is not null AND fotoCliCpf.idImagemUsuario is not null AND b.idBiometriaCliente is not null)  "; 
		str = str + " and d.dataHoraInclusao >=' "+df.format(dataInicial)+" 00:00:00' and d.dataHoraInclusao <= '"+df.format(dataFinal)+" 23:59:59' "; 
		str = str + " group by c.idcliente order by c.dataHoraCadastro desc";

		System.out.println(str);
		ResultSet rs = stm.executeQuery(String.valueOf(str));
		JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);				
		Image logoCorreto = null;
		Image logoErrado = null;
		InputStream inputStreamDaImagem = null;
		InputStream inputStreamDaImagemLogoErrado = null;
		InputStream inputStreamDaImagemLogoCorreto = null;
		URL urlImagens = getClass().getResource("/imagens/logo.jpg");
		URL urlLogoCorreto = getClass().getResource("/imagens/correto.png");
		URL urlLogoErrado = getClass().getResource("/imagens/errado.png");
		String caminhoImagem = urlImagens.getPath();
		String caminhoLogoErrado = urlLogoErrado.getPath();
		String caminhoLogoCorreto = urlLogoCorreto.getPath();

		try {    
			File fileLogo = new File(caminhoImagem);  
			File fileLogoErrado = new File(caminhoLogoErrado);  
			File fileLogoCorreto = new File(caminhoLogoCorreto);  
			if(fileLogo.exists() && fileLogoCorreto.exists() && fileLogoErrado.exists()){//testa se imagem existe  
				inputStreamDaImagem = new BufferedInputStream(new FileInputStream(caminhoImagem));
				inputStreamDaImagemLogoErrado = new BufferedInputStream(new FileInputStream(caminhoLogoErrado));
				inputStreamDaImagemLogoCorreto = new BufferedInputStream(new FileInputStream(caminhoLogoCorreto));
				logo = ImageIO.read(inputStreamDaImagem);
				logoErrado = ImageIO.read(inputStreamDaImagemLogoErrado);
				logoCorreto = ImageIO.read(inputStreamDaImagemLogoCorreto);
			}else{
				logo =null;
				logoCorreto = null;
				logoErrado = null;
			}


		} catch (FileNotFoundException e) {       
			//e.printStackTrace();
			logo =null;
			logoCorreto = null;
			logoErrado = null;
		}       

		params.put("logo", logo);  
		params.put("dataFinal", dfa.format(dataFinal));
		params.put("dataInicial",dfa.format(dataInicial)); 
		params.put("tituloRelatorio", "Relatorio Clientes Novos sem Fotos");
		params.put("logoCorreto", logoCorreto);  
		params.put("logoErrado", logoErrado);  
		params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath()); 
		params.put("REPORT_CONNECTION",ConexaoUtil.getConexaoPadrao());
		params.put("data",dInicial );
		String arquivo = "relatorioClienteSemFotos.jasper";
		JasperPrint impressao = JasperFillManager.fillReport(getClass().getResource("/relatorios/"+arquivo).getPath(), params, jrRS);
		byte[] bytes = JasperExportManager.exportReportToPdf(impressao);
		this.response.setContentLength(bytes.length);
		this.response.setContentType("application/pdf");
		this.response.getOutputStream().write(bytes, 0, bytes.length);
		this.response.getOutputStream().flush();
		this.response.getOutputStream().close();
	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Cliente com duplicata sem foto")
	public void gerarRelatorioSemFotosPorDuplciata() throws Exception
	{
		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
		SimpleDateFormat dfa = new SimpleDateFormat ("dd/MM/yyyy");

		Date dInicial = null;
		Date dataInicial = null;
		Date dataFinal = null;
		if(request.getParameter("dataInicial")!=null && !request.getParameter("dataInicial").equals(""))
		{
			dataInicial = dfa.parse(request.getParameter("dataInicial"));
		}
		if(request.getParameter("dataFinal")!=null && !request.getParameter("dataFinal").equals(""))
		{
			dataFinal =  dfa.parse(request.getParameter("dataFinal"));
		}

		Map<String, Object> params = new HashMap<String, Object>();
		Connection con = ConexaoUtil.getConexaoPadrao();
		Statement stm = con.createStatement();
		InputStream imageInputStream = null;
		Image logo = null; 

		String str = "";
		str = str + " Select concat(c.idcliente,' - ',c.nome) as cliente,  fotoCli.idImagemUsuario as foto ,  "; 
		str = str + "   fotoCliRg.idImagemUsuario as rg,  fotoCliVerso.idImagemUsuario as verso ,  "; 
		str = str + "   fotoCliCpf.idImagemUsuario as cpf,   b.idBiometriaCliente  as biometria ,  d.dataHoraInclusao as dataHoraCadastro  "; 
		str = str + "   from duplicata d "; 
		str = str + "   left outer join Cliente c on c.idCliente = d.idCliente "; 
		str = str + "   left outer join biometriacliente b on b.idcliente = c.idCliente  "; 
		str = str + "   left outer join imagemUsuario fotoCli 		on fotoCli.infoCampoChave = c.idCliente and fotoCli.campoChave = 'FOTO CLIENTE' "; 
		str = str + "   left outer join imagemUsuario fotoCliRg 	on fotoCliRg.infoCampoChave = c.idCliente and fotoCliRg.campoChave = 'FOTO RG'  "; 
		str = str + "   left outer join imagemUsuario fotoCliVerso 	on fotoCliVerso.infoCampoChave = c.idCliente and fotoCliVerso.campoChave = 'FOTO RG VERSO'   "; 
		str = str + "   left outer join imagemUsuario fotoCliCpf 	on fotoCliCpf.infoCampoChave = c.idCliente and fotoCliCpf.campoChave = 'FOTO CPF'    "; 
		str = str + " where (fotoCli.idImagemUsuario is  null or fotoCliRg.idImagemUsuario is  null or fotoCliVerso.idImagemUsuario is null or fotoCliCpf.idImagemUsuario is null or b.idBiometriaCliente is null)  "; 
		str = str + " and d.dataHoraInclusao >=' "+df.format(dataInicial)+" 00:00:00' and d.dataHoraInclusao <= '"+df.format(dataFinal)+" 23:59:59' "; 
		str = str + " group by c.idcliente order by c.dataHoraCadastro desc";

		System.out.println(str);
		ResultSet rs = stm.executeQuery(String.valueOf(str));
		JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);				
		Image logoCorreto = null;
		Image logoErrado = null;
		InputStream inputStreamDaImagem = null;
		InputStream inputStreamDaImagemLogoErrado = null;
		InputStream inputStreamDaImagemLogoCorreto = null;
		URL urlImagens = getClass().getResource("/imagens/logo.jpg");
		URL urlLogoCorreto = getClass().getResource("/imagens/correto.png");
		URL urlLogoErrado = getClass().getResource("/imagens/errado.png");
		String caminhoImagem = urlImagens.getPath();
		String caminhoLogoErrado = urlLogoErrado.getPath();
		String caminhoLogoCorreto = urlLogoCorreto.getPath();

		try {    
			File fileLogo = new File(caminhoImagem);  
			File fileLogoErrado = new File(caminhoLogoErrado);  
			File fileLogoCorreto = new File(caminhoLogoCorreto);  
			if(fileLogo.exists() && fileLogoCorreto.exists() && fileLogoErrado.exists()){//testa se imagem existe  
				inputStreamDaImagem = new BufferedInputStream(new FileInputStream(caminhoImagem));
				inputStreamDaImagemLogoErrado = new BufferedInputStream(new FileInputStream(caminhoLogoErrado));
				inputStreamDaImagemLogoCorreto = new BufferedInputStream(new FileInputStream(caminhoLogoCorreto));
				logo = ImageIO.read(inputStreamDaImagem);
				logoErrado = ImageIO.read(inputStreamDaImagemLogoErrado);
				logoCorreto = ImageIO.read(inputStreamDaImagemLogoCorreto);
			}else{
				logo =null;
				logoCorreto = null;
				logoErrado = null;
			}


		} catch (FileNotFoundException e) {       
			//e.printStackTrace();
			logo =null;
			logoCorreto = null;
			logoErrado = null;
		}       

		params.put("logo", logo);  
		params.put("dataFinal", dfa.format(dataFinal));
		params.put("dataInicial",dfa.format(dataInicial)); 
		params.put("tituloRelatorio", "Relatorio Clientes Novos sem Fotos");
		params.put("logoCorreto", logoCorreto);  
		params.put("logoErrado", logoErrado);  
		params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath()); 
		params.put("REPORT_CONNECTION",ConexaoUtil.getConexaoPadrao());
		params.put("data",dInicial );
		String arquivo = "relatorioClienteSemFotos.jasper";
		JasperPrint impressao = JasperFillManager.fillReport(getClass().getResource("/relatorios/"+arquivo).getPath(), params, jrRS);
		byte[] bytes = JasperExportManager.exportReportToPdf(impressao);
		this.response.setContentLength(bytes.length);
		this.response.setContentType("application/pdf");
		this.response.getOutputStream().write(bytes, 0, bytes.length);
		this.response.getOutputStream().flush();
		this.response.getOutputStream().close();
	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Cliente por cidade")
	public void gerarRelatorioClientePorCidade() throws Exception
	{
		byte[] bytes = null;
		try
		{
			Cidade cidade = new Cidade();
			if(!request.getParameter("idCidade").equals(""))
				cidade = new CidadeService().recuperarPorId(Integer.parseInt(request.getParameter("idCidade")));


			String str ="";

			str = str + " SELECT concat(cli.idCliente,' - ',cli.nome) as cliente,cli.nomeMae,cli.nomePai,cli.logradouro,cli.logradouroComercial,cli.nomeConjuge "; 
			str = str + " ,cli.dataNascimento,cli.rgInscricaoEstadual,cli.cpfCnpj, CONCAT(cli.nomeMae,' / ',cli.nomePai) AS filiacao, cli.empresaTrabalha,cli.telefoneComercial, cli.telefoneResidencial, "; 
			str = str + " cli.bairroCliente,cli.estadoCivil,cli.profissao,cli.enderecoCliente,cli.logradouroComercial,cli.idCliente, "; 
			str = str + " cli.numeroEnderecoCliente,cli.numeroComercial,cid.nome as cid,uf.nome as uf,cidt.nome as cidt, uft.nome as uft "; 
			str = str + " FROM cliente cli "; 
			str = str + " left outer join cidade cid on cid.idCidade = cli.idCidadeEndereco "; 
			str = str + " left outer join uf uf on uf.idUf = cid.idUf "; 
			str = str + " left outer join cidade cidt on cidt.idCidade = cli.idCidadeComercial "; 
			str = str + " left outer join uf uft on uft.idUf = cidt.idUf "; 
			if(!request.getParameter("idCidade").equals(""))
			{
				if(request.getParameter("excluirCidade")!= null && request.getParameter("excluirCidade").equals("S"))
					str = str +" where cid.idCidade <> " + request.getParameter("idCidade");
				else
					str = str +" where cid.idCidade = " + request.getParameter("idCidade");

			}
			str = str +" order by cli.nome asc ";
			Connection conn;
			conn = ConexaoUtil.getConexaoPadrao(); 
			System.out.println(str);
			ResultSet rs = conn.createStatement().executeQuery(str);    
			URL urlArquivo = getClass().getResource("/relatorios/relatorioClientePorCidade.jasper");
			String rel = urlArquivo.getPath();

			Image logo = null;

			String caminhoImagem = getClass().getResource("/imagens/logo.jpg").getPath();  

			try 
			{        
				if(new File(caminhoImagem).exists())
					logo = ImageIO.read(new BufferedInputStream(new FileInputStream(caminhoImagem)));
				else
					logo = null;
			}
			catch (FileNotFoundException e) 
			{       
				logo = null;
			}       

			JRResultSetDataSource ds = new JRResultSetDataSource(rs);     
			Map<String, Object> params = new HashMap<String, Object>();   
			params.put("logo", logo);

			params.put("REPORT_CONNECTION", ConexaoUtil.getConexaoPadrao());
			params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath());
			params.put("tituloRelatorio", "CLIENTES POR CIDADE");
			params.put("HABILITAR_DATA", "S");
			params.put("cidade", !request.getParameter("idCidade").equals("")?cidade.getNome():"");

			JasperPrint impressao = JasperFillManager.fillReport(rel, params,ds);            
			bytes = JasperExportManager.exportReportToPdf(impressao);    
		}
		catch (JRException e) 
		{  
			throw new ServletException(e);  
		}  

		if (bytes != null && bytes.length > 0) 
		{	  
			response.setContentType("application/pdf");  
			response.setContentLength(bytes.length);  
			ServletOutputStream ouputStream = response.getOutputStream();
			ouputStream.write(bytes, 0, bytes.length);  
			ouputStream.flush();  
			ouputStream.close();  
		} 

	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Clientes novos")
	public void gerarRelatorioClientesNovos() throws Exception
	{
		byte[] bytes = null;
		try
		{

			SimpleDateFormat sd  = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat sds  = new SimpleDateFormat("yyyy-MM-dd");
			Date di = sd.parse(request.getParameter("dataInicial"));
			Date df = sd.parse(request.getParameter("dataFinal"));
			String str ="";

			str = str + " SELECT concat(cli.idCliente,' - ',cli.nome) as cliente,cli.nomeMae,cli.nomePai,cli.logradouro,cli.logradouroComercial,cli.nomeConjuge "; 
			str = str + " ,cli.dataNascimento,cli.rgInscricaoEstadual,cli.cpfCnpj, CONCAT(cli.nomeMae,' / ',cli.nomePai) AS filiacao, cli.empresaTrabalha,cli.telefoneComercial, cli.telefoneResidencial, "; 
			str = str + " cli.bairroCliente,cli.estadoCivil,cli.profissao,cli.enderecoCliente,cli.logradouroComercial,cli.idCliente, "; 
			str = str + " cli.numeroEnderecoCliente,cli.numeroComercial,cid.nome as cid,uf.nome as uf,cidt.nome as cidt, uft.nome as uft "; 
			str = str + " FROM cliente cli "; 
			str = str + " left outer join cidade cid on cid.idCidade = cli.idCidadeEndereco "; 
			str = str + " left outer join uf uf on uf.idUf = cid.idUf "; 
			str = str + " left outer join cidade cidt on cidt.idCidade = cli.idCidadeComercial "; 
			str = str + " left outer join uf uft on uft.idUf = cidt.idUf "; 
			str = str +" where cli.datahoracadastro >= "+sds.format(di)+" 00:00:00" ;
			str = str +" and cli.datahoracadastro <= "+sds.format(df)+" 23:59:59" ;
			str = str +" order  cli.datahoracadastro  ";
			Connection conn;
			conn = ConexaoUtil.getConexaoPadrao(); 
			System.out.println(str);
			ResultSet rs = conn.createStatement().executeQuery(str);    
			URL urlArquivo = getClass().getResource("/relatorios/relatorioClienteSeprocados.jasper");
			String rel = urlArquivo.getPath();

			Image logo = null;

			String caminhoImagem = getClass().getResource("/imagens/logo.jpg").getPath();  

			try 
			{        
				if(new File(caminhoImagem).exists())
					logo = ImageIO.read(new BufferedInputStream(new FileInputStream(caminhoImagem)));
				else
					logo = null;
			}
			catch (FileNotFoundException e) 
			{       
				logo = null;
			}       

			JRResultSetDataSource ds = new JRResultSetDataSource(rs);     
			Map<String, Object> params = new HashMap<String, Object>();   
			params.put("logo", logo);

			params.put("REPORT_CONNECTION", ConexaoUtil.getConexaoPadrao());
			params.put("tituloRelatorio", "Relatorio Seprocados");
			params.put("HABILITAR_DATA", "S");
			params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath());

			JasperPrint impressao = JasperFillManager.fillReport(rel, params,ds);            
			bytes = JasperExportManager.exportReportToPdf(impressao);    
		}
		catch (JRException e) 
		{  
			throw new ServletException(e);  
		}  

		if (bytes != null && bytes.length > 0) 
		{	  
			response.setContentType("application/pdf");  
			response.setContentLength(bytes.length);  
			ServletOutputStream ouputStream = response.getOutputStream();
			ouputStream.write(bytes, 0, bytes.length);  
			ouputStream.flush();  
			ouputStream.close();  
		} 

	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente}, nomeAcao="Gerar Relatório Seprocados Por Parcela")
	public void relatorioSeprocadosPorParcela() throws Exception
	{
		byte[] bytes = null;
		Boolean transacaoIndependente = false;
		EntityManager manager =null; 
		EntityTransaction transaction = null;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}

			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
			Connection conn = ConexaoUtil.getConexaoPadrao(); 
			String dataBase = null;
			Date hj = sd.parse(sd.format(new Date()));
			String idParcelas = "";
			String dataEmissaoInicialSQL = null;
			String dataEmissaoFinalSQL = null;
			Date dataEmissaoInicial = null;
			Date dataEmissaoFinal = null;
			Boolean isSeralle = false;

			if(request.getParameter("dataEmissaoInicial")!=null && !request.getParameter("dataEmissaoInicial").equals(""))
			{
				dataEmissaoInicial = sd.parse(request.getParameter("dataEmissaoInicial"));
				dataEmissaoInicialSQL = df.format(sd.parse(request.getParameter("dataEmissaoInicial")));
			}
			if(request.getParameter("dataEmissaoFinal")!=null && !request.getParameter("dataEmissaoFinal").equals(""))
			{
				dataEmissaoFinal = sd.parse(request.getParameter("dataEmissaoFinal"));
				dataEmissaoFinalSQL = df.format( sd.parse(request.getParameter("dataEmissaoFinal")));
			}

			Date dataSerraleDate = DsvConstante.getParametrosSistema().get("dataSeralle")==null?null:sd.parse(DsvConstante.getParametrosSistema().get("dataSeralle"));
			String dataSeralle = dataSerraleDate==null?null:df.format(dataSerraleDate);
			if(dataSerraleDate != null && dataEmissaoInicial.getTime() >= dataSerraleDate.getTime())
			{
				isSeralle = true;
			}
			String where  = " impresso = 'N' and reativadoSeprocado = 'S'";
			List<SeprocadoReativados> lista = new SeprocadoReativadosService().listarPaginadoSqlLivreGenerico(manager, Integer.MAX_VALUE, 1, where, "dataNova", "asc").getRecordList();
			for (int i = 0; i < lista.size(); i++)
			{

				SeprocadoReativados s = lista.get(i);
				s.setImpresso("S");
				s.setDataImpressao(new Date());
				new SeprocadoReativadosService().atualizar(manager, transaction, s);
				if(idParcelas.equals(""))
					idParcelas = s.getDuplicataParcela().getId().toString();
				else
					idParcelas += ","+s.getDuplicataParcela().getId().toString();
			}
			where  = " impresso = 'N' and reativadoSeprocado = 'R'";
			lista = new SeprocadoReativadosService().listarPaginadoSqlLivreGenerico(manager, Integer.MAX_VALUE, 1, where, "dataNova", "asc").getRecordList();
			for (int i = 0; i < lista.size(); i++)
			{

				SeprocadoReativados s = lista.get(i);
				DuplicataParcela par = lista.get(i).getDuplicataParcela();
				Boolean continua = false;
				if(isSeralle)
				{
					if(par.getAcordo() != null && par.getAcordo().equals("S"))
					{
						Date dataCompra = par.getDuplicata().getDataCompra();
						DuplicataAcordo acordoCriterio = new DuplicataAcordo();
						acordoCriterio.setDuplicataNova(par.getDuplicata());
						List<DuplicataAcordo> listaAcordo = new DuplicataAcordoService().listarPorObjetoFiltro(manager, acordoCriterio, null, null, null);
						if(listaAcordo.size()>0)
						{
							Date dataCompraAntiga = listaAcordo.get(0).getDuplicataAntigaParcela().getDuplicata().getDataCompra();
							if(dataCompraAntiga.getTime() >= dataSerraleDate.getTime() 
									&& dataCompra.getTime() >= dataEmissaoInicial.getTime() 
									&& dataCompra.getTime() <= dataEmissaoFinal.getTime()  )
							{
								continua = true;
							}
						}
					}
					else
					{
						Date dataCompra = par.getDuplicata().getDataCompra();
						if(dataCompra.getTime() >= dataEmissaoInicial.getTime() 
								&& dataCompra.getTime() <= dataEmissaoFinal.getTime()  )
						{
							continua = true;
						}
					}
				}
				else
				{
					if(par.getAcordo() != null && par.getAcordo().equals("S"))
					{
						if(dataSerraleDate == null)
							dataSerraleDate = new Date();

						Date dataCompra = par.getDuplicata().getDataCompra();
						DuplicataAcordo acordoCriterio = new DuplicataAcordo();
						acordoCriterio.setDuplicataNova(par.getDuplicata());
						List<DuplicataAcordo> listaAcordo = new DuplicataAcordoService().listarPorObjetoFiltro(manager, acordoCriterio, null, null, null);
						if(listaAcordo.size()>0)
						{
							Date dataCompraAntiga = listaAcordo.get(0).getDuplicataAntigaParcela().getDuplicata().getDataCompra();
							if(dataCompraAntiga.getTime() <= dataSerraleDate.getTime())
							{
								continua = true;
							}
						}
					}
					else
					{
						Date dataCompra = par.getDuplicata().getDataCompra();
						if(dataCompra.getTime() >= dataEmissaoInicial.getTime() 
								&& dataCompra.getTime() <= dataEmissaoFinal.getTime()  )
						{
							continua = true;
						}
					}
				}
				if(continua)
				{
					s.setImpresso("S");
					s.setDataImpressao(new Date());
					new SeprocadoReativadosService().atualizar(manager, transaction, s);
					if(idParcelas.equals(""))
						idParcelas = s.getDuplicataParcela().getId().toString();
					else
						idParcelas += ","+s.getDuplicataParcela().getId().toString();
				}
			}
			dataBase = df.format(hj);

			if(transacaoIndependente)
				transaction.commit();

			ResultSet rsSQL = null;
			String sql = "";
			if(!idParcelas.equals(""))
			{
				sql = sql + " SELECT concat(cli.idCliente,' - ',cli.nome) as cliente,cli.nomeMae,cli.nomePai,cli.logradouro,cli.logradouroComercial,cli.nomeConjuge  "; 
				sql = sql + "  ,cli.dataNascimento,cli.rgInscricaoEstadual,cli.cpfCnpj, CONCAT(cli.nomeMae,' / ',cli.nomePai) AS filiacao, cli.empresaTrabalha,cli.telefoneComercial,cli.telefoneResidencial, cli.bairroCliente,cli.estadoCivil,cli.profissao,cli.enderecoCliente,cli.logradouroComercial, "; 
				sql = sql + " cli.idCliente,  "; 
				sql = sql + "  cli.numeroEnderecoCliente,cli.numeroComercial,cid.nome as cid,uf.nome as uf,cidt.nome as cidt, uft.nome as uft from seprocadosreativados s "; 
				sql = sql + " inner join cliente cli on s.idCliente = cli.idCliente "; 
				sql = sql + " left outer join cidade cid on cid.idCidade = cli.idCidadeEndereco  "; 
				sql = sql + " left outer join uf uf on uf.idUf = cid.idUf   "; 
				sql = sql + " left outer join cidade cidt on cidt.idCidade = cli.idCidadeComercial  "; 
				sql = sql + " left outer join uf uft on uft.idUf = cidt.idUf  "; 
				sql = sql + " left outer join duplicataparcela par  on par.idduplicataparcela = s.idduplicataparcela  "; 
				sql = sql + " left outer join duplicata du on du.idduplicata = par.idduplicata  "; 


				sql = sql + " where s.idduplicataparcela in("+idParcelas+")";

				sql = sql + " group by cli.idcliente order by cli.nome asc  "; 

				conn = ConexaoUtil.getConexaoPadrao(); 
				System.out.println(sql);
				rsSQL = conn.createStatement().executeQuery(sql); 
			}
			URL urlArquivo = getClass().getResource("/relatorios/relatorioClienteSeprocados.jasper");
			String rel = urlArquivo.getPath();
			Image logo = null;
			String caminhoImagem = getClass().getResource("/imagens/logo.jpg").getPath();  
			try 
			{        
				if(new File(caminhoImagem).exists())
					logo = ImageIO.read(new BufferedInputStream(new FileInputStream(caminhoImagem)));
				else
					logo = null;
			}
			catch (FileNotFoundException e) 
			{       
				logo = null;
			}       
			JRResultSetDataSource ds = new JRResultSetDataSource(rsSQL);     
			Map<String, Object> params = new HashMap<String, Object>();   
			params.put("logo", logo);
			params.put("REPORT_CONNECTION", ConexaoUtil.getConexaoPadrao());
			params.put("tituloRelatorio", "Relatorio Seprocados E Reativados");
			params.put("HABILITAR_DATA", "S");
			params.put("dataInicial", sd.format(df.parse(dataBase)));
			params.put("dataFinal", sd.format(df.parse(dataBase)));

			params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath());
			params.put("dataBase", dataBase);
			if(!idParcelas.equals(""))
				params.put("idParcelas", idParcelas);

			JasperPrint impressao = JasperFillManager.fillReport(rel, params,ds);            
			bytes = JasperExportManager.exportReportToPdf(impressao); 


		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			if(transacaoIndependente)
				transaction.rollback();

			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
		if (bytes != null && bytes.length > 0) 
		{	  
			response.setContentType("application/pdf");  
			response.setContentLength(bytes.length);  
			ServletOutputStream ouputStream = response.getOutputStream();
			ouputStream.write(bytes, 0, bytes.length);  
			ouputStream.flush();  
			ouputStream.close();  
		} 
	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente}, nomeAcao="Reimprimir Seprocados")
	public void ReimprimirSeprocado() throws Exception
	{

		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
		SimpleDateFormat dfa = new SimpleDateFormat ("dd/MM/yyyy");

		Date diasInicial = null;
		Date diasFinal = null;
		Date dataEmissaoInicial = null;
		Date dataEmissaoFinal = null;	
		String dataEmissaoFinalSQL = null;
		String dataEmissaoInicialSQL = null;
		String idParcelas = "";
		Integer cliente = null;
		boolean cupom = new UsuarioService().getSessionUae();
		InputStream imageInputStream = null;
		Image logo = null; 
		Boolean isSeralle = false;
		if(request.getParameter("dataInicial")!=null && !request.getParameter("dataInicial").equals(""))
		{
			diasInicial = dfa.parse(request.getParameter("dataInicial"));
		}
		if(request.getParameter("dataFinal")!=null && !request.getParameter("dataFinal").equals(""))
		{
			diasFinal =  dfa.parse(request.getParameter("dataFinal"));
		}
		if(request.getParameter("dataEmissaoInicial")!=null && !request.getParameter("dataEmissaoInicial").equals(""))
		{
			dataEmissaoInicial =dfa.parse(request.getParameter("dataEmissaoInicial"));
			dataEmissaoInicialSQL = df.format(dfa.parse(request.getParameter("dataEmissaoInicial")));
		}
		if(request.getParameter("dataEmissaoFinal")!=null && !request.getParameter("dataEmissaoFinal").equals(""))
		{
			dataEmissaoFinal = dfa.parse(request.getParameter("dataEmissaoFinal"));
			dataEmissaoFinalSQL = df.format( dfa.parse(request.getParameter("dataEmissaoFinal")));
		}
		if(request.getParameter("idCliente")!=null && !request.getParameter("idCliente").equals(""))
		{
			cliente = Integer.parseInt(request.getParameter("idCliente"));
		}
		Date dataSerraleDate = DsvConstante.getParametrosSistema().get("dataSeralle")==null?null:dfa.parse(DsvConstante.getParametrosSistema().get("dataSeralle"));
		String dataSeralle = dataSerraleDate==null?null:df.format(dataSerraleDate);
		if(dataSerraleDate != null && dataEmissaoInicial.getTime() >= dataSerraleDate.getTime())
		{
			isSeralle = true;
		}
		String tipo = request.getParameter("tipo");
		Map<String, Object> params = new HashMap<String, Object>();
		Connection con = ConexaoUtil.getConexaoPadrao();
		Statement stm = con.createStatement();
		String where  = " dataimpressao >= '"+df.format(diasInicial)+" 00:00:00' and dataimpressao <= '"+df.format(diasFinal)+" 23:59:59' and impresso = 'S'";
		if(cliente!=null)
			where += " and idCliente in ("+cliente+")";

		List<SeprocadoReativados> lista = new SeprocadoReativadosService().listarPaginadoSqlLivreGenerico(Integer.MAX_VALUE, 1, where, "dataImpressao", "desc").getRecordList();
		for (int i = 0; i < lista.size(); i++)
		{

			SeprocadoReativados s = lista.get(i);
			DuplicataParcela par = lista.get(i).getDuplicataParcela();
			Boolean continua = false;
			if(isSeralle)
			{
				if(par.getAcordo() != null && par.getAcordo().equals("S"))
				{
					Date dataCompra = par.getDuplicata().getDataCompra();
					DuplicataAcordo acordoCriterio = new DuplicataAcordo();
					acordoCriterio.setDuplicataNova(par.getDuplicata());
					List<DuplicataAcordo> listaAcordo = new DuplicataAcordoService().listarPorObjetoFiltro( acordoCriterio);
					if(listaAcordo.size()>0)
					{
						Date dataCompraAntiga = listaAcordo.get(0).getDuplicataAntigaParcela().getDuplicata().getDataCompra();
						if(dataCompraAntiga.getTime() >= dataSerraleDate.getTime() 
								&& dataCompra.getTime() >= dataEmissaoInicial.getTime() 
								&& dataCompra.getTime() <= dataEmissaoFinal.getTime()  )
						{
							continua = true;
						}
					}
				}
				else
				{
					Date dataCompra = par.getDuplicata().getDataCompra();
					if(dataCompra.getTime() >= dataEmissaoInicial.getTime() 
							&& dataCompra.getTime() <= dataEmissaoFinal.getTime()  )
					{
						continua = true;
					}
				}
			}
			else
			{ 
				if(dataSerraleDate == null){
					dataSerraleDate = new Date();
				}
				if(par.getAcordo() != null && par.getAcordo().equals("S"))
				{
					Date dataCompra = par.getDuplicata().getDataCompra();
					DuplicataAcordo acordoCriterio = new DuplicataAcordo();
					acordoCriterio.setDuplicataNova(par.getDuplicata());
					List<DuplicataAcordo> listaAcordo = new DuplicataAcordoService().listarPorObjetoFiltro(acordoCriterio);
					if(listaAcordo.size()>0)
					{
						Date dataCompraAntiga = listaAcordo.get(0).getDuplicataAntigaParcela().getDuplicata().getDataCompra();
						if(dataCompraAntiga.getTime() <= dataSerraleDate.getTime() 
							  )
						{
							continua = true;
						}
					}
				}
				else
				{
					Date dataCompra = par.getDuplicata().getDataCompra();
					if(dataCompra.getTime() >= dataEmissaoInicial.getTime() 
							&& dataCompra.getTime() <= dataEmissaoFinal.getTime()  )
					{
						continua = true;
					}
				}
			}
			if(continua)
			{
				if(idParcelas.equals(""))
					idParcelas = s.getDuplicataParcela().getId().toString();
				else
					idParcelas += ","+s.getDuplicataParcela().getId().toString();
			}
		}
		String str = "";
		ResultSet rs = null;
		if(!idParcelas.equals(""))
		{
			str = str + " SELECT concat(cli.idCliente,' - ',cli.nome) as cliente,cli.nomeMae,cli.nomePai,cli.logradouro,cli.logradouroComercial,cli.nomeConjuge "; 
			str = str + " ,cli.dataNascimento,cli.rgInscricaoEstadual,cli.cpfCnpj, CONCAT(cli.nomeMae,' / ',cli.nomePai) AS filiacao, cli.empresaTrabalha,cli.telefoneComercial, cli.telefoneResidencial, "; 
			str = str + " cli.bairroCliente,cli.estadoCivil,cli.profissao,cli.enderecoCliente,cli.logradouroComercial,cli.idCliente, "; 
			str = str + " cli.numeroEnderecoCliente,cli.numeroComercial,cid.nome as cid,uf.nome as uf,cidt.nome as cidt, uft.nome as uft "; 
			str = str + " from seprocadosreativados sr "; 
			str = str + " inner join cliente cli on cli.idCliente = sr.idCliente";
			str = str + " left outer join cidade cid on cid.idCidade = cli.idCidadeEndereco "; 
			str = str + " left outer join uf uf on uf.idUf = cid.idUf "; 
			str = str + " left outer join cidade cidt on cidt.idCidade = cli.idCidadeComercial "; 
			str = str + " left outer join uf uft on uft.idUf = cidt.idUf "; 
			str = str + " where sr.idduplicataparcela in("+idParcelas+") "; 

			str = str + "  group by cli.idCliente  order by cli.nome ";
			System.out.println(str);
			rs = stm.executeQuery(String.valueOf(str));
		}
		JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);				

		try {
			File file = new File(getClass().getResource("/imagens/logo.jpg").getPath());
			if(file.exists()) {
				imageInputStream = new BufferedInputStream(new FileInputStream(getClass().getResource("/imagens/logo.jpg").getPath()));
				logo = ImageIO.read(imageInputStream);
			}
		}
		finally {
			if(imageInputStream != null) {
				imageInputStream.close();
			}
		}

		params.put("dataInicial",dfa.format(diasInicial)); 
		Integer diaSeprocar = DsvConstante.getParametrosSistema().get("diasSeprocar")==null?45:DsvConstante.getParametrosSistema().getInt("diasSeprocar");
		params.put("REPORT_CONNECTION", ConexaoUtil.getConexaoPadrao());
		params.put("diasSeprocar", diaSeprocar);
		params.put("dataFinal", dfa.format(diasFinal));
		params.put("dataBase",diasInicial==null?null:(df.format(diasInicial)+" 00:00:00"));
		params.put("diasFinal",diasFinal==null?null:(df.format(diasFinal)+" 23:59:59"));
		params.put("tituloRelatorio", "REIMPRESSÃO SEPROCADOS/REATIVATOS ");
		params.put("HABILITAR_DATA", "S");
		params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath());
		params.put("logo", logo);
		if(!idParcelas.equals(""))
			params.put("idParcelas", idParcelas);
		String arquivo = "relatorioClienteSeprocados.jasper";

		JasperPrint impressao = JasperFillManager.fillReport(getClass().getResource("/relatorios/"+arquivo).getPath(), params, jrRS);
		byte[] bytes = JasperExportManager.exportReportToPdf(impressao);
		this.response.setContentLength(bytes.length);
		this.response.setContentType("application/pdf");
		this.response.getOutputStream().write(bytes, 0, bytes.length);
		this.response.getOutputStream().flush();
		this.response.getOutputStream().close();

	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Gerente, Grupo.Crediario}, nomeAcao="Gerar Relatório Cobrança Resumida")
	public void relatorioCobrancaResumida() throws Exception
	{
		byte[] bytes = null;
		Boolean transacaoIndependente = false;
		EntityManager manager =null; 
		EntityTransaction transaction = null;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			Connection conn;
			conn = ConexaoUtil.getConexaoPadrao(); 
			Date dataUltimaCompra = null;
			String tempoResidencia = null;
			String tempoTrabalho = null;
			String tempoTrabalhoConjugue = null;
			String alertaCliente = null;
			if(request.getParameter("dataUltimaCompra") != null && !request.getParameter("dataUltimaCompra").equals("") )
			{
				dataUltimaCompra = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("dataUltimaCompra"));
			}
			if(request.getParameter("tempoResidencia") != null && !request.getParameter("tempoResidencia").equals("") )
			{
				tempoResidencia = request.getParameter("tempoResidencia");
			}
			if(request.getParameter("tempoTrabalho") != null && !request.getParameter("tempoTrabalho").equals("") )
			{
				tempoTrabalho = request.getParameter("tempoTrabalho");
			}
			if(request.getParameter("tempoTrabalhoConjugue") != null && !request.getParameter("tempoTrabalhoConjugue").equals("") )
			{
				tempoTrabalhoConjugue = request.getParameter("tempoTrabalhoConjugue");
			}
			if(request.getParameter("alertaCliente") != null && !request.getParameter("alertaCliente").equals("") )
			{
				alertaCliente = request.getParameter("alertaCliente");
			}

			String sql = "";
			sql = sql + " SELECT ";
			sql = sql + "  concat(cli.idCliente,' - ',cli.nome) as cliente, ";
			sql = sql + " cli.dataNascimento,cli.rgInscricaoEstadual,cli.cpfCnpj, ";
			sql = sql + "  cli.nomeMae,cli.nomePai,cli.estadoCivil, ";
			sql = sql + " cli.telefoneResidencial,cli.telefoneCelular01,cli.telefoneCelular02, ";
			sql = sql + " cli.logradouro,cli.numeroEndereco, cid.nome as cid,uf.sigla as uf,cli.cep, ";
			sql = sql + "  cli.bairro, cli.itinerarioEndereco, ";
			sql = sql + " cli.residenciaPropria,cli.tempoResidencia , cli.valorAluguelPrestacao, ";
			sql = sql + " cli.contato, ";
			sql = sql + "  ";
			sql = sql + "  cli.empresaTrabalha,cli.tempoTrabalho,cidt.nome as cidt, uft.sigla as uft , ";
			sql = sql + " cli.logradouroComercial,cli.numeroComercial,cli.telefoneComercial, ";
			sql = sql + " cli.profissao,cli.salario,cli.outraRenda, ";
			sql = sql + " ava.nome as avalista,'' as dataUltimaCompra, ";
			sql = sql + " cli.mediaAtraso,cli.dataBaixaSCPC,cli.dataEntradaSCPC, ";
			sql = sql + "  ";
			sql = sql + " cli.observacao, ";
			sql = sql + " cli.informacaoComercial, ";
			sql = sql + " cli.idconjuge ";
			sql = sql + "  ";
			sql = sql + "  from  cliente cli ";
			sql = sql + "  left outer join cidade cid on cid.idCidade = cli.idCidadeEndereco ";
			sql = sql + "  left outer join uf uf on uf.idUf = cid.idUf ";
			sql = sql + "  left outer join cidade cidt on cidt.idCidade = cli.idCidadeComercial ";
			sql = sql + "  left outer join uf uft on uft.idUf = cidt.idUf ";
			sql = sql + " left outer join avalista ava on ava.idAvalista = cli.idAvalista ";
			sql = sql + " where idcliente =  " + request.getParameter("idCliente");


			ResultSet rsSQL = conn.createStatement().executeQuery(sql); 

			URL urlArquivo = getClass().getResource("/relatorios/relatorioCobrancaResumida.jasper");
			String rel = urlArquivo.getPath();
			JRResultSetDataSource ds = new JRResultSetDataSource(rsSQL);     
			Map<String, Object> params = new HashMap<String, Object>();   
			params.put("REPORT_CONNECTION", ConexaoUtil.getConexaoPadrao());
			params.put("SUBREPORT_DIR", getClass().getResource("/relatorios/").getPath());
			params.put("tempoResidencia", tempoResidencia);
			params.put("tempoTrabalho", tempoTrabalho);
			params.put("tempoTrabalhoconjugue", tempoTrabalhoConjugue);
			params.put("dataUltimaCompra", dataUltimaCompra);
			params.put("alertaCliente", alertaCliente);
			JasperPrint impressao = JasperFillManager.fillReport(rel, params,ds);            
			bytes = JasperExportManager.exportReportToPdf(impressao); 


		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			if(transacaoIndependente)
				transaction.rollback();

			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
		if (bytes != null && bytes.length > 0) 
		{	  
			response.setContentType("application/pdf");  
			response.setContentLength(bytes.length);  
			ServletOutputStream ouputStream = response.getOutputStream();
			ouputStream.write(bytes, 0, bytes.length);  
			ouputStream.flush();  
			ouputStream.close();  
		} 
	}
}