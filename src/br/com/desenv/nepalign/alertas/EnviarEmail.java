package br.com.desenv.nepalign.alertas;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.sun.istack.internal.ByteArrayDataSource;

import br.com.desenv.nepalign.util.DsvConstante;

public class EnviarEmail 
{
	private String from;
	private String password;
	private String smtpServer;
	
		
	public void enviarEmail(String from, String password, String smtpServer, Address[] emailsTo, String subject, String messageBody, String setFromLabel, Map<String, byte[]> arquivosAnexos) throws Exception
	{
		Properties props = new Properties();

		props.put("mail.smtp.host", smtpServer);
		props.put("mail.smtp.ssl.enable", "true");
		props.put("mail.smtp.port", DsvConstante.getParametrosSistema().get("portaSmtpEmailDeEnvioAlertaEmail"));
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.transport.protocol", "smtp");
		
		Session session = Session.getDefaultInstance(props, new LoginAuthenticator(from, password));
		
		try 
		{
			final Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from, setFromLabel));
			message.setRecipients(Message.RecipientType.BCC, emailsTo);
			message.setSubject(subject);
			
			Multipart multipart = new MimeMultipart();
			
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(messageBody, "text/html;charset=utf-8");
			multipart.addBodyPart(messageBodyPart);
			
			for (Entry<String, byte[]> entry : arquivosAnexos.entrySet())
			{
				messageBodyPart = new MimeBodyPart();
				messageBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(entry.getValue(), "application/pdf")));
				messageBodyPart.setFileName(entry.getKey());
				multipart.addBodyPart(messageBodyPart);
			}
			
			message.setContent(multipart);

			Transport.send(message);
		} 
		catch (MessagingException e) 
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public void enviarEmail(String from,String password,String smtpServer, Address[] emailsTo, String subject, String messageBody, String setFromLabel, String arquivoAnexo) throws Exception
	{
		this.from = from;
		this.password = password;
		this.smtpServer = smtpServer; 
		Properties props = new Properties();

		props.put("mail.smtp.host", this.smtpServer);
		props.put("mail.smtp.ssl.enable", "true");
		props.put("mail.smtp.port", DsvConstante.getParametrosSistema().get("portaSmtpEmailDeEnvioAlertaEmail"));
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.transport.protocol", "smtp");


		// Create a Session object based on the properties and
		// Authenticator object
		Session session = Session.getDefaultInstance(props, new LoginAuthenticator(this.from, this.password));

		try 
		{
			// Create a Message object using the session created above
			Message message = new MimeMessage(session);
			// setting email address to Message from where message is being sent
			message.setFrom(new InternetAddress(this.from, setFromLabel));
			// setting the email addressess to which user wants to send message
			message.setRecipients(Message.RecipientType.BCC, emailsTo);
			// setting the subject for the email
			message.setSubject(subject);
			// setting the text message which user wants to send to recipients
			message.setContent(messageBody, "text/html; charset=utf-8");
			// anexa o arquivo na mensagem

			if(arquivoAnexo != null && arquivoAnexo != "")
			{
				String[] fileName = arquivoAnexo.split("/");
				BodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setText(messageBody);
				Multipart multipart = new MimeMultipart();
				multipart.addBodyPart(messageBodyPart);
				messageBodyPart = new MimeBodyPart();
				DataSource source = new FileDataSource(arquivoAnexo);
				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setFileName(fileName[fileName.length-1]);
				multipart.addBodyPart(messageBodyPart);
				message.setContent(multipart);
			}


			// Using the Transport class send() method to send message
			Transport.send(message);
		} 
		catch (MessagingException e) 
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

}

class LoginAuthenticator extends Authenticator 
{
	PasswordAuthentication authentication = null;

	public LoginAuthenticator(String username, String password) 
	{
		authentication = new PasswordAuthentication(username,password);
	}

	@Override
	protected PasswordAuthentication getPasswordAuthentication() 
	{
		return authentication;
	}
}