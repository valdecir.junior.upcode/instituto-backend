package br.com.desenv.nepalign.alertas.sms;

import br.com.desenv.nepalign.model.AlertaLoja;
import br.com.desenv.nepalign.model.EstoqueProduto;
import br.com.desenv.nepalign.service.AlertaLojaService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.util.DsvParametrosSistema;

public class AlertaSms
{
	public void enviarSms(AlertaLoja alertaLoja,String ddd, String numero) throws Exception
	{
		SMSService ser = new SMSService();
		
		EstoqueProduto estoque = new EstoqueProduto(); 
		estoque = alertaLoja.getAlertaPedidoVenda().getItemPedidoVenda().getEstoqueProduto();
		
		String msg = "Novo Pedido Encontrado " +
				" Produto:" + estoque.getProduto().getCodigoProduto() +
				" Ordem: " + estoque.getOrdemProduto().getCodigo() +
				" Data: " + alertaLoja.getAlertaPedidoVenda().getDataAlerta() +
				" Pedido: "+alertaLoja.getAlertaPedidoVenda().getItemPedidoVenda().getPedidoVenda().getId() +
				" Numeração: " + estoque.getTamanho().getDescricao() +
				" Cor: " + estoque.getCor().getDescricaoCorEmpresa() +
				" Empresa Origem do alerta : " + alertaLoja.getAlertaPedidoVenda().getEmpresaOrigemAlerta().getRazaoSocial();
		
		if(msg.length() > 140)
		{
			 msg = "Novo Pedido Encontrado " +
						" Produto:" + estoque.getProduto().getCodigoProduto() +
						" Ordem: " + estoque.getOrdemProduto().getCodigo() +
						" Data: " + alertaLoja.getAlertaPedidoVenda().getDataAlerta() +
						" Pedido: "+alertaLoja.getAlertaPedidoVenda().getItemPedidoVenda().getPedidoVenda().getId() +
						" Numeração: " + estoque.getTamanho().getDescricao() +
						" Cor: " + estoque.getCor().getDescricaoCorEmpresa();
		}
		
		ser.enviarSMS(ddd, numero, msg, DsvConstante.getInstanceParametrosSistema().getUsuarioServicoSMS(), DsvConstante.getInstanceParametrosSistema().getSenhaServicoSMS());
	}
}