package br.com.desenv.nepalign.alertas;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.MenuShortcut;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.xml.stream.events.StartDocument;

import br.com.desenv.nepalign.model.AlertaLoja;
import br.com.desenv.nepalign.model.AlertaPedidoVenda;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.ItemPedidoVenda;
import br.com.desenv.nepalign.model.PedidoVenda;
import br.com.desenv.nepalign.model.SituacaoPedidoVenda;
import br.com.desenv.nepalign.model.StatusAlerta;
import br.com.desenv.nepalign.model.StatusItemPedido;
import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.nepalign.service.AlertaLojaService;
import br.com.desenv.nepalign.service.AlertaPedidoVendaService;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.util.DsvParametrosSistema;

public class SystemTryIcon {
	public Timer timer;
	public String strMensagem = "";
	public TrayIcon trayIcon;
	static AudioClip soundToPlay;
	static RemindTask alertaTask;

	public static void main(String[] args) throws Exception, MalformedURLException 
	{
		new Loader();
		new SystemTryIcon();
	}

	public SystemTryIcon() throws Exception, MalformedURLException 
	{
		createSystemTray();
		URL url = this.getClass().getClassLoader().getResource("som.wav");
		soundToPlay = Applet.newAudioClip(url); // Is Utilized?
		strMensagem = "Mensagem do sistema.";
		alertaTask = new RemindTask();
		timer = new Timer();
		timer.schedule(alertaTask, 0, DsvConstante
				.getInstanceParametrosSistema().getSleepTimeTaskAlertaSonoro());
	}

	public static void tocaRaul(Boolean parar, AudioClip toPlay)
			throws MalformedURLException {
		if (parar)
			toPlay.stop();
		else
			toPlay.loop();
	}

	private void createSystemTray() throws Exception, MalformedURLException {
		SystemTray tray = SystemTray.getSystemTray();

		if (!tray.isSupported()) {
			System.err.println("não foi possível rodar o aplicativo");
			System.exit(0);
		}

		Image icon = new ImageIcon(this.getClass().getResource(
				"/download_19x19.gif")).getImage();
		PopupMenu menu = new PopupMenu();

		MenuItem itemSair = new MenuItem("Sair");
		itemSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					alertaTask.pararTodosAlertas();
				} catch (MalformedURLException e1) {
					e1.printStackTrace();
				}

			}
		});
		menu.add(itemSair);

		MenuItem itemOption = new MenuItem("Sistema");
		String urlSistema = DsvConstante.getInstanceParametrosSistema().getUrl_Sistema();
		itemOption.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				abrirSistema("");
			}
		});
		menu.add(itemOption);

		trayIcon = new TrayIcon(icon, "Avise-me Alertas", menu);

		try {
			tray.add(trayIcon);
			trayIcon.displayMessage("Sistema de Alerta",
					"Informa Pedidos Novos", TrayIcon.MessageType.INFO);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static final String errMsg = "Erro ao tentar abrir o browser";

	private void abrirSistema(String url) {

		String osName = System.getProperty("os.name");
		try {
			if (osName.startsWith("Mac OS")) {
				Class fileMgr = Class.forName("com.apple.eio.FileManager");
				Method openURL = fileMgr.getDeclaredMethod("openURL",
						new Class[] { String.class });
				openURL.invoke(null, new Object[] { url });
			} else if (osName.startsWith("Windows"))
				Runtime.getRuntime().exec(
						"rundll32 url.dll,FileProtocolHandler " + url);
			else { // assume Unix or Linux
				String[] browsers = { "firefox", "opera", "konqueror",
						"epiphany", "mozilla", "netscape" };
				String browser = null;
				for (int count = 0; count < browsers.length && browser == null; count++)
					if (Runtime.getRuntime()
							.exec(new String[] { "which", browsers[count] })
							.waitFor() == 0)
						browser = browsers[count];
				if (browser == null)
					throw new Exception("Navegador não encontrado!");
				else
					Runtime.getRuntime().exec(new String[] { browser, url });
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					errMsg + ":\n" + e.getLocalizedMessage());
		}
	}

	public AlertaLoja montaObjeto(int status, Integer statusItemPedidoId,
			Integer statusPedido) throws Exception {
		EmpresaFisica empresaFisica = new EmpresaFisica();
		empresaFisica.setId(DsvConstante.getInstanceParametrosSistema()
				.getEmpresa_AlertaSonoro());

		StatusAlerta statusAlerta = new StatusAlerta();
		statusAlerta.setId(status);

		ItemPedidoVenda itemPedidoVenda = null;
		StatusItemPedido statusItemPedido = new StatusItemPedido();
		if (statusItemPedidoId != null) {
			itemPedidoVenda = new ItemPedidoVenda();
			statusItemPedido.setId(statusItemPedidoId);
			itemPedidoVenda.setStatusItemPedido(statusItemPedido);
		}

		PedidoVenda pedidoVenda = null;
		if (statusPedido != null) {
			pedidoVenda = new PedidoVenda();
			SituacaoPedidoVenda situacaoPedidoVenda = new SituacaoPedidoVenda();
			situacaoPedidoVenda.setId(statusPedido);
			pedidoVenda.setSituacaoPedidoVenda(situacaoPedidoVenda);
			if (itemPedidoVenda == null)
				itemPedidoVenda = new ItemPedidoVenda();

			itemPedidoVenda.setPedidoVenda(pedidoVenda);
		}

		AlertaPedidoVenda alertaPedidoVenda = new AlertaPedidoVenda();
		alertaPedidoVenda.setStatusAlerta(statusAlerta);
		if (itemPedidoVenda != null)
			alertaPedidoVenda.setItemPedidoVenda(itemPedidoVenda);
		AlertaLoja alertaLojaFiltro = new AlertaLoja();

		alertaLojaFiltro.setEmpresaFisica(empresaFisica);
		alertaLojaFiltro.setAlertaPedidoVenda(alertaPedidoVenda);
		alertaLojaFiltro.setRespostaAlerta("S");

		return alertaLojaFiltro;

	}

	class RemindTask extends TimerTask {
		protected AudioClip alertSoundPendente;
		protected AudioClip alertSoundNovo;
		protected AudioClip alertSoundApropiado;

		protected Integer delaySoundApropiado;
		
		protected EmpresaFisica empresaAlerta;

		public RemindTask() {
			this.delaySoundApropiado = 0;
		}

		public void run() {
			try
			{
				this.empresaAlerta = new EmpresaFisicaService().recuperarPorId(DsvConstante.getInstanceParametrosSistema().getEmpresa_AlertaSonoro());
				if (this.alertSoundNovo == null)
					this.alertSoundNovo = Applet.newAudioClip(this.getClass()
							.getClassLoader().getResource("som.wav"));
				if (this.alertSoundPendente == null)
					this.alertSoundPendente = Applet.newAudioClip(this.getClass()
							.getClassLoader().getResource("alarme_despachar.wav"));
				if (this.alertSoundApropiado == null)
					this.alertSoundApropiado = Applet.newAudioClip(this.getClass()
							.getClassLoader().getResource("somalarme.wav"));
	
				if (DsvConstante.getInstanceParametrosSistema()
						.getAlertaSonoroPedidoPendenteEnvio())
					this.alertaPedidoPendente();
				if (DsvConstante.getInstanceParametrosSistema()
						.getAlertaSonoroPedidoRespondido())
					this.alertaPedidoApropiado();
				if (DsvConstante.getInstanceParametrosSistema()
						.getAlertaSonoroPedidoNovo())
					this.alertaPedidoNovo();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		public void alertaPedidoPendente() {
			try {
				AlertaLojaService alertaService = new AlertaLojaService();
				List<AlertaLoja> list2 = alertaService.buscarPaginadoPorFiltro(
						100, 1, montaObjeto(4, 1, 3)).getRecordList();

				if (list2.size() > 0)
				{
					long nowTime = Calendar.getInstance().getTime().getTime();
					for (int i = 0; i < list2.size(); i++) 
					{
						AlertaLoja alertaLoja = (AlertaLoja) list2.get(0); 
						long orderTime = alertaLoja.getAlertaPedidoVenda().getDataAlerta().getTime();

						if(((nowTime - orderTime) / 1000 / 60 / 60) >= DsvConstante.getInstanceParametrosSistema().getTempoPedidoPendente())
						{
							if(alertaLoja.getAlertaPedidoVenda().getEmpresaVencedora().getId().equals(DsvConstante.getInstanceParametrosSistema().getEmpresa_AlertaSonoro()))
							{
								trayIcon.displayMessage("Gerenciador",
										"Pedido número "
												+ alertaLoja.getAlertaPedidoVenda()
														.getItemPedidoVenda()
														.getPedidoVenda().getId()
												+ " do vendedor "
												+ alertaLoja.getAlertaPedidoVenda()
														.getItemPedidoVenda()
														.getPedidoVenda()
														.getVendedor().getNome()
												+ " não foi despachado...",
										TrayIcon.MessageType.INFO);
								tocaRaul(false, this.alertSoundPendente);
	
								i = list2.size() + 1;
							}
						}
					}
				}
				else
				{
					tocaRaul(true, this.alertSoundPendente);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		public void alertaPedidoApropiado()
		{
			try
			{

				if (this.delaySoundApropiado != 0)
				{
					this.delaySoundApropiado -= DsvConstante.getInstanceParametrosSistema().getSleepTimeTaskAlertaSonoro();
					tocaRaul(true, this.alertSoundApropiado);
				}
				else if (this.delaySoundApropiado == 0) 
				{
					List<Integer> pedidosVerificados = new ArrayList<Integer>();
					AlertaLojaService alertaService = new AlertaLojaService();
					List<AlertaLoja> list2 = new ArrayList<AlertaLoja>();
					AlertaLoja alertaLojaFiltro = montaObjeto(4, null, null);
					alertaLojaFiltro.setEmpresaFisica(null);
					alertaLojaFiltro.getAlertaPedidoVenda().setEmpresaOrigemAlerta(empresaAlerta);
					list2 = alertaService.buscarPaginadoPorFiltro(100, 1,alertaLojaFiltro, 0, 3).getRecordList();

					if (list2.size() > 0)
					{
						for (int i = 0; i < list2.size(); i++)
						{
							AlertaLoja alertaLoja = (AlertaLoja) list2.get(i);
							
							if(!pedidosVerificados.contains(alertaLoja.getAlertaPedidoVenda().getItemPedidoVenda().getPedidoVenda().getId()))
							{
								pedidosVerificados.add(alertaLoja.getAlertaPedidoVenda().getItemPedidoVenda().getPedidoVenda().getId());
								if(alertaLoja.getAlertaPedidoVenda().getEmpresaOrigemAlerta().getId().equals(DsvConstante.getInstanceParametrosSistema().getEmpresa_AlertaSonoro()))
								{
									if(this.validacaoAlertaApropiado(alertaLoja.getAlertaPedidoVenda().getItemPedidoVenda().getPedidoVenda()))
									{
										trayIcon.displayMessage("Gerenciador","Pedido n?mero " + alertaLoja.getAlertaPedidoVenda().getItemPedidoVenda().getPedidoVenda().getId()
												+ " do vendedor " + alertaLoja.getAlertaPedidoVenda().getItemPedidoVenda().getPedidoVenda().getVendedor().getNome()
												+ " foi apropiado pela loja "+ alertaLoja.getAlertaPedidoVenda().getEmpresaVencedora().getRazaoSocial(),
												TrayIcon.MessageType.INFO);
										tocaRaul(false, this.alertSoundApropiado);

										this.delaySoundApropiado = DsvConstante.getInstanceParametrosSistema().getSleepTimeTaskAlertaSonoro() * 20;
										i = list2.size() + 1;
									}
								}
							}
						}
					} 
					else
					{
						tocaRaul(true, this.alertSoundApropiado);
					}
					pedidosVerificados.clear();
				}
			}
			catch (Exception ex) 
			{
				ex.printStackTrace();
			}
		}

		public void alertaPedidoNovo() {
			try {
				AlertaLojaService alertaService = new AlertaLojaService();
				List<AlertaLoja> list = new ArrayList<AlertaLoja>();
				try {
					list = alertaService.buscarPaginadoPorFiltro(100, 1,
							montaObjeto(1, null, null)).getRecordList();
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (list.size() > 0) {
					trayIcon.displayMessage("Pedido Novo",
							"Existem Pedidos não Respondidos",
							TrayIcon.MessageType.INFO);
					tocaRaul(false, this.alertSoundNovo);
				} else {
					tocaRaul(true, this.alertSoundNovo);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		public void pararTodosAlertas() throws MalformedURLException
		{
			tocaRaul(true, this.alertSoundNovo);
			tocaRaul(true, this.alertSoundApropiado);
			tocaRaul(true, this.alertSoundPendente);
		}
		
		protected Boolean validacaoAlertaApropiado(PedidoVenda pedidoVenda) throws Exception
		{
			try
			{
				Boolean resultBoolean = true;
				
				List<AlertaPedidoVenda> alertaPedidoVendaResultList = new AlertaPedidoVendaService().buscarPorPedidoVenda(pedidoVenda);
				
				for(AlertaPedidoVenda alertaPedidoVenda:alertaPedidoVendaResultList)
				{
					if(alertaPedidoVenda.getStatusAlerta().getId() != 4)
					{
						resultBoolean = false;
					}
				}
				return resultBoolean;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}
	}
}
