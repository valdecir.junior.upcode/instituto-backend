package br.com.desenv.nepalign.alertas;

import java.util.prefs.Preferences;

import org.infinispan.util.SysPropertyActions;

import com.sun.org.apache.bcel.internal.generic.GOTO;

import br.com.desenv.frameworkignorante.WinRegistry;
import br.com.desenv.nepalign.util.DsvConstante;

public class Loader
{
	public static void main(String[] args)
	{
		try
		{
			Loader loader = new Loader();
			loader.writeValue("");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public void writeValue(String value) throws Exception
	{
		WinRegistry.writeStringValue(0x80000002, "Software\\Microsoft\\Windows\\CurrentVersion\\Run", "Oi", "oi");
	}
	
	public Loader()
	{
		try
		{
			if(!this.CheckStartup())
			{
				throw new Exception("Não foi possível criar iniciar o sistema com o windows!");
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public Boolean CheckStartup() throws Exception
	{
		try
		{

			String value = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE,"Software\\Microsoft\\Windows\\CurrentVersion\\Run\\","NepalSystemTray");

			if(value == null)
			{
				WinRegistry.writeStringValue(0x80000002, "Software\\Microsoft\\Windows\\CurrentVersion\\Run\\", "NepalSystemTray", DsvConstante.getInstanceParametrosSistema().getCaminhoSistemaAlertaSonoro());
				this.CheckStartup();
			}
			
			return true;
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
}
