package br.com.desenv.nepalign.conexao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexao {

	public static Connection con = null;
	
	public void Conectar() throws IOException {

		System.out.println("Conectando ao banco...");

		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/banco?useTimezone=true&serverTimezone=America/Sao_Paulo",
					"root", "123456");
			System.out.println("Conectado.");

		} catch (ClassNotFoundException ex) {
			System.out.println("Classe n�o encontrada, adicione o driver nas bibliotecas.");
			Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
		} catch (SQLException e) {
			System.out.println(e);
			throw new RuntimeException(e);
		}

	}
	
}
