package br.com.desenv.nepalign.webservice;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.net.util.Base64;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import br.com.desenv.nepalign.model.Atendido;
import br.com.desenv.nepalign.model.Professor;
import br.com.desenv.nepalign.model.Turma;
import br.com.desenv.nepalign.model.TurmaAtendido;
import br.com.desenv.nepalign.service.AtendidoService;
import br.com.desenv.nepalign.service.ProfessorService;
import br.com.desenv.nepalign.service.TurmaAtendidoService;
import br.com.desenv.nepalign.service.TurmaService;

@Path("/atendido")
public class AtendidoWebService {

	private static final Gson gson = new Gson();
	private static final AtendidoService atendidoService = new AtendidoService();
	private static final ProfessorService professorService = new ProfessorService();
	private static final TurmaAtendidoService turmaAtendidoService = new TurmaAtendidoService();

	@GET
	@Path("/carregarGrade/{idAtendido}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public final String carregarGrade(@PathParam("idAtendido") Integer idAtendido) throws Exception {
		if (idAtendido.toString().startsWith("9") && idAtendido.toString().length() == 6) {// PROFESSOR
			return carregarGradeProfessor(idAtendido);
		} else if (idAtendido.toString().startsWith("1") && idAtendido.toString().length() == 6) {// PROFESSOR
			return carregarGradeGeral();
		} else
			return carregarGradeAtendido(idAtendido);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String carregarGradeGeral() throws Exception {
		JsonObject result = new JsonObject();
		List<Turma> listaTurma = new TurmaService().listar();

		Collections.sort(listaTurma, new Comparator() {
			public int compare(Object o1, Object o2) {
				Turma p1 = (Turma) o1;
				Turma p2 = (Turma) o2;
				return Integer.parseInt(p1.getDiaSemana()) < Integer.parseInt(p2.getDiaSemana()) ? -1
						: (Integer.parseInt(p1.getDiaSemana()) > Integer.parseInt(p2.getDiaSemana()) ? +1 : 0);
			}
		});

		result.add("tipo", gson.toJsonTree("SERVIDOR"));
		result.add("grade", gson.toJsonTree(listaTurma));
		return gson.toJson(result);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String carregarGradeProfessor(Integer id) throws Exception {

		id = Integer.parseInt(id.toString().substring(1, id.toString().length()));

		JsonObject result = new JsonObject();

		Professor professor = professorService.recuperarPorId(id);

		if (professor == null) {
			result.add("erro", gson.toJsonTree(404));
			result.add("message", gson.toJsonTree("Usuário não encontrado."));
			return gson.toJson(result);
		}

		Turma filtro = new Turma();
		filtro.setProfessor(professor);

		List<Turma> listaTurma = new TurmaService().listarPorObjetoFiltro(filtro);
		Collections.sort(listaTurma, new Comparator() {
			public int compare(Object o1, Object o2) {
				Turma p1 = (Turma) o1;
				Turma p2 = (Turma) o2;
				return Integer.parseInt(p1.getDiaSemana()) < Integer.parseInt(p2.getDiaSemana()) ? -1
						: (Integer.parseInt(p1.getDiaSemana()) > Integer.parseInt(p2.getDiaSemana()) ? +1 : 0);
			}
		});

		result.add("professor", gson.toJsonTree(professor));
		result.add("grade", gson.toJsonTree(listaTurma));
		result.add("tipo", gson.toJsonTree("PROFESSOR"));
		result.add("proxAtividade", gson.toJsonTree(proximaAtividade(listaTurma)));
		return gson.toJson(result);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String carregarGradeAtendido(Integer id) throws Exception {
		JsonObject result = new JsonObject();

		Atendido atendido = atendidoService.recuperarPorId(id);
		List<Turma> listaTurma = new ArrayList<>();
		List<TurmaAtendido> listaTurmaAtendido = turmaAtendidoService
				.listarPorObjetoFiltro((new TurmaAtendido(null, atendido)));

		for (TurmaAtendido turmaAtendido : listaTurmaAtendido) {
			listaTurma.add(turmaAtendido.getTurma());
		}

		Collections.sort(listaTurma, new Comparator() {
			public int compare(Object o1, Object o2) {
				Turma p1 = (Turma) o1;
				Turma p2 = (Turma) o2;
				return Integer.parseInt(p1.getDiaSemana()) < Integer.parseInt(p2.getDiaSemana()) ? -1
						: (Integer.parseInt(p1.getDiaSemana()) > Integer.parseInt(p2.getDiaSemana()) ? +1 : 0);
			}
		});

		if (atendido == null) {
			result.add("erro", gson.toJsonTree(404));
			result.add("message", gson.toJsonTree("Usuário não encontrado."));
			return gson.toJson(result);
		}

		result.add("tipo", gson.toJsonTree("ATENDIDO"));
		result.add("atendido", gson.toJsonTree(atendido));
		result.add("grade", gson.toJsonTree(listaTurma));
		result.add("proxAtividade", gson.toJsonTree(proximaAtividade(listaTurma)));
		if (atendido.getFoto() != null) {
			result.add("foto", gson.toJsonTree(Base64.encodeBase64String(atendido.getFoto())));
		}
		return gson.toJson(result);

	}

	@SuppressWarnings("deprecation")
	private Turma proximaAtividade(List<Turma> listaTurma) {
		Calendar cal = Calendar.getInstance();
		Date atual = new Date();
		Turma proximaTurma = new Turma();
		long aux = 0;
		for (Turma turmaAtendido : listaTurma) {
			cal.setTime(atual);
			int dif = 0;
			for (int i = cal.get(Calendar.DAY_OF_WEEK); i <= 7; i++) {
				if (i == Integer.parseInt(turmaAtendido.getDiaSemana()))
					break;
				if (i == 7)
					i = 0;
				dif++;
			}

			long dataTurma = new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
					cal.get(Calendar.DAY_OF_MONTH) + dif, turmaAtendido.getHorarioInicial().getHours(),
					turmaAtendido.getHorarioInicial().getMinutes()).getTime().getTime();

			if ((aux == 0 || aux > dataTurma) && dataTurma > new Date().getTime()) {
				aux = dataTurma;
				proximaTurma = turmaAtendido;
			}
		}
		return proximaTurma;
	}
}
