package br.com.desenv.nepalign.connectionFactory;

import java.sql.Connection;

import java.sql.DriverManager;

import java.sql.SQLException;

import br.com.desenv.padraoignorante.conexao.ConexaoUtil;
 

public class ConnectionFactory {

	@Deprecated
	public static Connection getConnection()
	{
	
		try 
		{
			return ConexaoUtil.getConexaoPadrao(); 
		} 
		catch (Exception e) {
	
			throw new RuntimeException(e);
		}
	}
}

