package br.com.desenv.nepalign.tasks;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.Formatador;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.ItemPedidoVenda;
import br.com.desenv.nepalign.model.NotaFiscal;
import br.com.desenv.nepalign.model.PagamentoPedidoVenda;
import br.com.desenv.nepalign.model.PedidoVenda;
import br.com.desenv.nepalign.model.PlanoPagamento;
import br.com.desenv.nepalign.model.SaldoEstoqueProduto;
import br.com.desenv.nepalign.service.ClienteService;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.ItemPedidoVendaService;
import br.com.desenv.nepalign.service.NotaFiscalService;
import br.com.desenv.nepalign.service.PagamentoPedidoVendaService;
import br.com.desenv.nepalign.service.PedidoVendaService;
import br.com.desenv.nepalign.service.PlanoPagamentoService;
import br.com.desenv.nepalign.service.SaldoEstoqueProdutoService;
import br.com.desenv.nepalign.service.SituacaoPedidoVendaService;
import br.com.desenv.nepalign.service.StatusItemPedidoService;
import br.com.desenv.nepalign.service.TipoMovimentacaoEstoqueService;
import br.com.desenv.nepalign.service.VendedorService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class EmissaoNotaFiscalAutomatica extends GenericTaskIGN 
{
	Logger logger = Logger.getLogger("br.com.desenv.nepalign.tasks");

	private Random r = new Random();
	private PedidoVendaService pedidoVendaService = new PedidoVendaService();
	private Integer idTipoMovimentacaoEstoque = 2;
	private Integer idVendedor = 1039285;
	private Integer idCliente = 0;
	private Integer idSituacaoPedidoVenda = 2;
	private Integer valorTotalDia;

	private Integer faixaItemMin = 0;
	private Integer faixaItemMax = 100;
	
	private Integer faixaTimeMin = 30;
	private Integer faixaTimeMax = 60;

	private List<PlanoPagamento> listaPlanoPagamento;
	private List<SaldoEstoqueProduto> listaSaldoEstoqueProduto;

	public EmissaoNotaFiscalAutomatica(EmpresaFisica empresa) throws Exception {
		listaPlanoPagamento = new PlanoPagamentoService().listarPaginadoSqlLivreGenerico(Integer.MAX_VALUE, 1, "idEmpresaFisica = " + empresa.getId()).getRecordList();
	}

	public static void main(String[] args) throws InterruptedException{
		try {
			int idEmpresa = Integer.parseInt(args[0]);
			EmpresaFisica empresa = new EmpresaFisicaService().recuperarPorId(idEmpresa);
			EmissaoNotaFiscalAutomatica main = new EmissaoNotaFiscalAutomatica(empresa);
			main.valorTotalDia = Integer.parseInt(args[1]);
			System.out.println("idEmpresa: " + idEmpresa + " valorTotalDia: " + main.valorTotalDia + " / " + args[2]);
			if (args[2].equals("venda"))
				main.gerarVendas(empresa);
			else if (args[2].equals("nota"))
				main.gerarNotaFiscalPedidoVenda();
			Thread.sleep(10 * 1000);
		} catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
		Thread.sleep(10 * 1000);
	}

	public void gerarVendas(EmpresaFisica empresa) {
		EntityManager manager = ConexaoUtil.getEntityManager();
		manager.getTransaction().begin();

		Double valorTotalVenda = 0.0;

		try {
			do{
				PedidoVenda pedidoVenda = new PedidoVenda();
				pedidoVenda.setTipoMovimentacaoEstoque(new TipoMovimentacaoEstoqueService().recuperarPorId(manager, idTipoMovimentacaoEstoque));
				pedidoVenda.setEmpresaFisica(empresa);
				pedidoVenda.setVendedor(new VendedorService().recuperarPorId(manager, idVendedor));
				pedidoVenda.setCliente(new ClienteService().recuperarPorId(manager, idCliente));
				pedidoVenda.setUsuario(pedidoVenda.getVendedor().getUsuario());
				pedidoVenda.setSituacaoPedidoVenda(new SituacaoPedidoVendaService().recuperarPorId(manager, idSituacaoPedidoVenda));
				pedidoVenda.setPlanoPagamento(gerarPlanoPagamento(manager));
				pedidoVenda.setDataVenda(new Date());
				pedidoVenda.setCodigoPlano(pedidoVenda.getPlanoPagamento().getCodigoPlano());
				pedidoVenda.setValorTotalPedido(0.);

				pedidoVendaService.salvar(manager, manager.getTransaction(), pedidoVenda);

				Double valorTotalPedido = gerarItensVenda(manager, pedidoVenda, empresa);

				pedidoVenda.setNumeroControle(gerarNumeroControle(pedidoVenda));
				pedidoVenda.setValorTotalPedido(valorTotalPedido);
				pedidoVenda.setSituacaoPedidoVenda(new SituacaoPedidoVendaService().recuperarPorId(manager, 3));
				pedidoVendaService.atualizar(manager, manager.getTransaction(), pedidoVenda);
				gerarPagamentoPedidoVenda(pedidoVenda, manager);

				valorTotalVenda += valorTotalPedido;
			}while (valorTotalVenda <= valorTotalDia);
			manager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			manager.close();
		}
	}


	private void gerarPagamentoPedidoVenda(PedidoVenda pedidoVenda, EntityManager manager) throws Exception {
		PagamentoPedidoVenda pagamento = new PagamentoPedidoVenda();
		pagamento.setPedidoVenda(pedidoVenda);
		pagamento.setFormaPagamento(pedidoVenda.getPlanoPagamento().getFormaPagamento());
		pagamento.setValor(pedidoVenda.getValorTotalPedido());
		pagamento.setDataPagamento(new Date());
		pagamento.setUsuario(pedidoVenda.getUsuario());
		pagamento.setFormaPagamento(pedidoVenda.getPlanoPagamento().getFormaPagamento());
		pagamento.setNumeroDocumento(pedidoVenda.getNumeroControle());
		pagamento.setUsuario(pedidoVenda.getUsuario());
		new PagamentoPedidoVendaService().salvar(manager, manager.getTransaction(), pagamento);
	}

	// METODOS PRIVADOS

	private PlanoPagamento gerarPlanoPagamento(EntityManager manager) throws Exception {
		return listaPlanoPagamento.get(r.nextInt(listaPlanoPagamento.size()));
	}

	private String gerarNumeroControle(PedidoVenda pedidoVenda) {
		return pedidoVenda.getEmpresaFisica().getAnoAberto().toString() + pedidoVenda.getVendedor().getId().toString() + pedidoVenda.getId() + pedidoVenda.getEmpresaFisica().getMesAberto().toString();

	}

	private Double gerarItensVenda(EntityManager manager, PedidoVenda pedidoVenda, EmpresaFisica empresaFisica) throws Exception {
		Double valorTotalPedido = 0.;
		Integer valorVenda = r.nextInt(faixaItemMax) + faixaItemMin;
		Integer itemSequencial = 1;

		if (listaSaldoEstoqueProduto == null) {
			String where = "idEmpresaFisica = " + empresaFisica.getId() + " and ano = " + empresaFisica.getAnoAberto()
			+ " and mes = " + empresaFisica.getMesAberto() + " and saldo > 0 and custo > 0 and custo <= "+faixaItemMax;
			listaSaldoEstoqueProduto = new SaldoEstoqueProdutoService().listarPaginadoSqlLivreGenerico(Integer.MAX_VALUE, 1, where).getRecordList();
		}

		do {
			SaldoEstoqueProduto saldo = listaSaldoEstoqueProduto.get(r.nextInt(listaSaldoEstoqueProduto.size()));
			ItemPedidoVenda itemPedidoVenda = new ItemPedidoVenda();

			Double precoVenda = Formatador.round(saldo.getCusto() + (saldo.getCusto() * ((r.nextInt(10) + 10.) / 100.)), 2);
			
			itemPedidoVenda.setPedidoVenda(pedidoVenda);
			itemPedidoVenda.setProduto(saldo.getEstoqueProduto().getProduto());
			itemPedidoVenda.setEstoqueProduto(saldo.getEstoqueProduto());
			itemPedidoVenda.setQuantidade(1.0);
			itemPedidoVenda.setCustoProduto(saldo.getCusto());
			itemPedidoVenda.setPrecoTabela(saldo.getVenda());
			itemPedidoVenda.setEntradaSaida("S");
			itemPedidoVenda.setPrecoVenda(precoVenda);
			itemPedidoVenda.setItemSequencial(itemSequencial++);
			itemPedidoVenda.setStatusItemPedido(new StatusItemPedidoService().recuperarPorId(manager, 2));

			if (valorTotalPedido + itemPedidoVenda.getPrecoVenda() > valorTotalPedido && valorTotalPedido != 0){
				break;	
			}else{
				new ItemPedidoVendaService().salvar(manager, manager.getTransaction(), itemPedidoVenda);
				valorTotalPedido += itemPedidoVenda.getPrecoVenda();
			}
		} while (valorTotalPedido < valorVenda);

		valorTotalPedido += Formatador.round(valorTotalPedido, 2);
		
		System.out.println("Valor desejado: "+valorVenda+" Valor do pedido: " + valorTotalPedido+"/ quantidade de itens: "+ itemSequencial);
		return Formatador.round(valorTotalPedido, 2);
	}
	
	public void gerarNotaFiscalPedidoVenda() throws Exception{
		SituacaoPedidoVendaService situacaoPedidoVendaService = new SituacaoPedidoVendaService();
		
		EntityManager manager = ConexaoUtil.getEntityManager();
		manager.getTransaction().begin();

		String where = "dataVenda between '2017-09-22 00:00:00' and '2017-09-22 23:59:59' and idSituacaoPedidoVenda = 3";
		List<PedidoVenda> lista = new PedidoVendaService().listarPaginadoSqlLivreGenerico(manager, Integer.MAX_VALUE, 1, where, null, null).getRecordList();
		List<NotaFiscal> listaNotaFiscal = new ArrayList<>();

		int valorNotaGerado = 0;
		
		for(PedidoVenda pedidoVenda : lista){
			System.out.println("toc toc");
			Thread.sleep(((r.nextInt(faixaTimeMax) + faixaTimeMin) * 1000) * 60);
			NotaFiscal notaFiscal = new NotaFiscal(); 
			notaFiscal.setPedidoVenda(pedidoVenda);
			notaFiscal.setEntradaSaida("S");
			notaFiscal.setCpfReceptor(null);
			
			listaNotaFiscal.add(new NotaFiscalService().gerarNotaFiscalConsumidor(pedidoVenda, notaFiscal, false, manager, manager.getTransaction()));

			pedidoVenda.setSituacaoPedidoVenda(situacaoPedidoVendaService.recuperarPorId(manager, 6));
			new PedidoVendaService().atualizar(manager, manager.getTransaction(), pedidoVenda);

			valorNotaGerado += pedidoVenda.getValorTotalPedido();
			if (valorNotaGerado > valorTotalDia)
				break;
		}

		manager.getTransaction().commit();
		manager.clear();

		for (NotaFiscal notaFiscal : listaNotaFiscal) {
			new NotaFiscalService().enviarNotaFiscalNfeConsumidor(notaFiscal);
		}
	}
}
