package br.com.desenv.nepalign.tasks;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.mail.Address;
import javax.mail.internet.InternetAddress;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.alertas.EnviarEmail;
import br.com.desenv.nepalign.relatorios.RelatorioDocumentoEntradaSaidaLoja;
import br.com.desenv.nepalign.util.DsvConstante;

public class RelatorioDocumentoEntradaSaidaPendenteTask extends GenericTaskIGN
{
	public static void main(String[] args) throws Exception
	{
		new RelatorioDocumentoEntradaSaidaPendenteTask().run();
	}
	
	protected void run() throws Exception
	{
		final Map<String, byte[]> attachments = new HashMap<String, byte[]>();
		attachments.put("Relatório Entradas Pendentes", new RelatorioDocumentoEntradaSaidaLoja(null, null).gerarRelatorioDocumentoEntradaSaidaPendenteEntrega("C:/desenv/DocumentoEntradaSaidaPendenteTask/relatorio/relatorioPedidoCompraVendaEntregaPendente.jasper"));
		
		final String[] addresses = DsvConstante.getParametrosSistema().get("EMAIL_ENTRADA_RECENTES").split(";");
		final Address[] addressTo = new Address[addresses.length];
		
		for(int i = 0; i < addresses.length; i++)
			addressTo[i] = new InternetAddress(addresses[i]);
			
		new EnviarEmail().enviarEmail("noreply@manamaue.com.br", "mana@vitoria7", "smtp.manamaue.com.br", addressTo,
				"Nepal - Entrega de Mercadorias Pendentes [" + IgnUtil.dateFormat.format(new Date()) + "]!", 
				"Olá, o relatório de pedidos que faltaram ser entregues hoje (" + IgnUtil.dateFormat.format(new Date()) + ") está disponível em anexo!<br><br><br>Por favor não responda essa mensagem. Esse é um e-mail automático do Sistema Nepal!", 
				"Sistema Nepal", 
				attachments);
	}
}
 