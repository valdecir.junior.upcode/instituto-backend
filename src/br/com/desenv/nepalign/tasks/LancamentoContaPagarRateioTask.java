package br.com.desenv.nepalign.tasks;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.LancamentoContaPagar;
import br.com.desenv.nepalign.model.LancamentoContaPagarRateio;
import br.com.desenv.nepalign.service.LancamentoContaPagarRateioService;
import br.com.desenv.nepalign.service.LancamentoContaPagarService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;


public class LancamentoContaPagarRateioTask extends GenericTaskIGN
{
	private static final LancamentoContaPagarService lancamentoContaPagarService = new LancamentoContaPagarService();
	private static final LancamentoContaPagarRateioService lancamentoContaPagarRateioService = new LancamentoContaPagarRateioService();
	
	public static void main(String[] args) throws Exception
	{
		final EntityManager manager = ConexaoUtil.getEntityManager();
		manager.getTransaction().begin();
		
		/*Integer idLancamentoContaPagarInicio = Integer.parseInt(args[0x00]);
		Integer idLancamentoContaPagarFim = Integer.parseInt(args[0x01]);*/
		
		String lancamentos = "410986,409882,407271";
		 
		try
		{
			
			for(String idLancamento : lancamentos.split(","))
			{
				final LancamentoContaPagar lancamentoContaPagar = manager.find(LancamentoContaPagar.class, Integer.parseInt(idLancamento));
				System.out.println("Alterando lançamento " + lancamentoContaPagar.getNumeroDocumento() + " da empresa " + lancamentoContaPagar.getEmpresaFisica().getIdFormatado());
				
				if(lancamentoContaPagar.getObservacao() != null)
					lancamentoContaPagar.setObservacao(lancamentoContaPagar.getObservacao().concat(" Lançamento Empresa " + lancamentoContaPagar.getEmpresaFisica().getIdFormatado() + " alterado para 52. " + IgnUtil.dateFormat.format(new Date())));
				else
					lancamentoContaPagar.setObservacao("Lançamento Empresa " + lancamentoContaPagar.getEmpresaFisica().getIdFormatado() + " alterado para 52. " + IgnUtil.dateFormat.format(new Date()));
				
				lancamentoContaPagar.setNumeroDocumento(lancamentoContaPagar.getNumeroDocumento().concat("(" + lancamentoContaPagar.getEmpresaFisica().getIdFormatado() + ")")); 
				lancamentoContaPagar.setEmpresaFisica(manager.find(EmpresaFisica.class, 52));
				manager.merge(lancamentoContaPagar);

				HashMap<Integer, BigDecimal> novosRateios = new HashMap<Integer, BigDecimal>();
				novosRateios.put(52, new BigDecimal(35));
				novosRateios.put(44, new BigDecimal(35));
				novosRateios.put(58, new BigDecimal(15));
				novosRateios.put(55, new BigDecimal(10));
				novosRateios.put(113, new BigDecimal(5));
				
				System.out.println("Incluíndo Rateios .... ");
				
				for(Map.Entry<Integer, BigDecimal> empresaRateio : novosRateios.entrySet())
				{
					final LancamentoContaPagarRateio novoRateio = new LancamentoContaPagarRateio();
					novoRateio.setLancamentoContaPagar(lancamentoContaPagar);
					novoRateio.setEmpresaFisica(manager.find(EmpresaFisica.class, empresaRateio.getKey()));
					novoRateio.setPorcentagemRateio(empresaRateio.getValue());
					lancamentoContaPagarRateioService.salvar(manager, manager.getTransaction(), novoRateio, false);
					
					System.out.println(" EMPRESA " + novoRateio.getEmpresaFisica().getIdFormatado() + " PORCENTAGEM " + novoRateio.getPorcentagemRateio());
				}
				
				System.out.println("Lançamento OK!");
			}

			manager.getTransaction().commit();	
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			if(manager.getTransaction().isActive())
				manager.getTransaction().rollback();
		}
		finally 
		{
			if(manager.isOpen())
				manager.close();
		}
	}
	
	/*for(; idLancamentoContaPagarInicio <= idLancamentoContaPagarFim; idLancamentoContaPagarInicio++)
	{
		final LancamentoContaPagar lancamentoContaPagar = manager.find(LancamentoContaPagar.class, idLancamentoContaPagarInicio);
		
		List<LancamentoContaPagarRateio> rateios = lancamentoContaPagarService.recuperarRateios(manager, idLancamentoContaPagarInicio);
		 
		if(rateios != null && rateios.size() > 0x00)
		{
			System.out.println("Esse lançamento já estava com rateio....");
			System.out.println("Informações do rateio já existe: ");
			
			for(LancamentoContaPagarRateio rateio : rateios)
			{
				System.out.println(" EMPRESA " + rateio.getEmpresaFisica().getIdFormatado() + " PORCENTAGEM " + rateio.getPorcentagemRateio());
				lancamentoContaPagarRateioService.excluir(manager, manager.getTransaction(), rateio, false);
			}
		}
		
		System.out.println("Incluíndo Rateios .... ");
		
		HashMap<Integer, BigDecimal> novosRateios = new HashMap<Integer, BigDecimal>();
		novosRateios.put(52, new BigDecimal(35));
		novosRateios.put(44, new BigDecimal(35));
		novosRateios.put(58, new BigDecimal(15));
		novosRateios.put(55, new BigDecimal(10));
		novosRateios.put(113, new BigDecimal(5));
		
		for(Map.Entry<Integer, BigDecimal> empresaRateio : novosRateios.entrySet())
		{
			final LancamentoContaPagarRateio novoRateio = new LancamentoContaPagarRateio();
			novoRateio.setLancamentoContaPagar(lancamentoContaPagar);
			novoRateio.setEmpresaFisica(manager.find(EmpresaFisica.class, empresaRateio.getKey()));
			novoRateio.setPorcentagemRateio(empresaRateio.getValue());
			lancamentoContaPagarRateioService.salvar(manager, manager.getTransaction(), novoRateio, false);
			
			System.out.println(" EMPRESA " + novoRateio.getEmpresaFisica().getIdFormatado() + " PORCENTAGEM " + novoRateio.getPorcentagemRateio());
		}

		System.out.println("Lançamento OK!");
	}*/
}
