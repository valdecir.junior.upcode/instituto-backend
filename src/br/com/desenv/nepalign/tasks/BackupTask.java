package br.com.desenv.nepalign.tasks;

import java.io.File;

import br.com.desenv.frameworkignorante.BackupRunner;

public class BackupTask extends GenericTaskIGN
{
	private static Integer numAtual = 0x00;
	
	public static void main(String[] args) throws Exception
	{
		System.out.println("Iniciando Backup");

		final BackupRunner runner = new BackupRunner(null);
		runner.setFileName("C:/desenv/backup/".concat(getFileName(Integer.parseInt(args[0x00]))));
		runner.setOverwritePolicy(true);
		runner.setBackupPolicy(BackupRunner.POLICY_DATABASE_ONLY);
		runner.run();
		
		System.out.println("Backup completo");
		if(numAtual > 0x00)
		{
			final File backupAntigo = new File("C:/desenv/backup/sys_bkp_" + numAtual + ".rar");
			
			if(backupAntigo.exists())
			{
				backupAntigo.delete();
				System.out.println("Backup antigo apagado");
			}
		}
	}
	
	private static String getFileName(final int maxBackupFiles)
	{
		Integer backupNum = 0x00; 
		
		for(final File file : new File("C:/desenv/backup/").listFiles())
			if(file.getName().startsWith("sys_bkp"))
				backupNum = Integer.parseInt(file.getName().split("_")[0x02].substring(0x00, 0x01));
		
		numAtual = backupNum;
		
		return "sys_bkp_" + (backupNum < maxBackupFiles ? backupNum + 0x01 : 0x01);
	}
}
