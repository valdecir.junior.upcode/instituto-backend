package br.com.desenv.nepalign.tasks;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;

public abstract class GenericTaskIGN 
{
	/**
	 * Construtor para iniciar banco de dados
	 */
	public GenericTaskIGN()
	{
		new GenericPersistenceIGN<>();
	}
}