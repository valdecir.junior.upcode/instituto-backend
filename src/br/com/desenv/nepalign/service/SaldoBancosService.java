package br.com.desenv.nepalign.service;

import java.util.Date;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.Bancos;
import br.com.desenv.nepalign.model.LogOperacao;
import br.com.desenv.nepalign.model.SaldoBancos;
import br.com.desenv.nepalign.persistence.SaldoBancosPersistence;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class SaldoBancosService extends GenericServiceIGN<SaldoBancos, SaldoBancosPersistence>
{
	private static final UsuarioService usuarioService = new UsuarioService();
	private static final LogOperacaoService logOperacaoService = new LogOperacaoService();
	
	public SaldoBancosService() { super(); }
	
	public SaldoBancos recuperarSaldoBancosAtual(EntityManager manager, Bancos bancos) throws Exception
	{
		return ((SaldoBancosPersistence) getPersistence()).recuperarSaldoBancosAtual(manager, bancos);
	}
	
	public SaldoBancos recuperarSaldoBancosAtual(Bancos bancos) throws Exception
	{
		return ((SaldoBancosPersistence) getPersistence()).recuperarSaldoBancosAtual(null, bancos);
	}
	
	public boolean atualizarSaldoBanco(EntityManager manager, Bancos bancos, Double valor) throws Exception
	{
		gravaOperacaoSaldo(manager, bancos, valor);
		return ((SaldoBancosPersistence) getPersistence()).atualizarSaldoBanco(manager, bancos, valor);
	}
	
	public boolean atualizarSaldoBanco(Bancos bancos, Double valor) throws Exception
	{
		gravaOperacaoSaldo(null, bancos, valor);
		return ((SaldoBancosPersistence) getPersistence()).atualizarSaldoBanco(null, bancos, valor);
	}
	
	private void gravaOperacaoSaldo(EntityManager manager, final Bancos bancos, final Double movimento) throws Exception
	{
		boolean managerIndependente = false;
		
		if((managerIndependente = (manager == null)))
		{
			manager = ConexaoUtil.getEntityManager();
			manager.getTransaction().begin();
		}
		
		try
		{
			final LogOperacao logOperacao = new LogOperacao();
			logOperacao.setData(new Date());
			logOperacao.setCampo("SALDOBANCOS");
			logOperacao.setTipoLog(2);
			logOperacao.setUsuario(usuarioService.recuperarSessaoUsuarioLogado());
			logOperacao.setObservacao("SALDO DO BANCOS " + bancos.getId() + " ATUALIZADO COM VALOR " + movimento);
			logOperacaoService.salvar(manager, manager.getTransaction(), logOperacao, false);
			
			if(managerIndependente)
				manager.getTransaction().commit();
		}
		catch(Exception ex)
		{
			if(managerIndependente)
				manager.getTransaction().rollback();
			
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
}