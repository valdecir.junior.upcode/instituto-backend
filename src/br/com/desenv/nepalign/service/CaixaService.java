package br.com.desenv.nepalign.service;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.Caixa;
import br.com.desenv.nepalign.persistence.CaixaPersistence;

public class CaixaService extends GenericServiceIGN<Caixa, CaixaPersistence>
{
	public CaixaService() { super(); }
}