package br.com.desenv.nepalign.service;

import java.util.Timer;
import java.util.TimerTask;

import br.com.desenv.nepalign.model.NotaFiscal;
import br.com.desenv.nepalign.util.DsvConstante;

public class ImpressaoNotaFiscalService extends TimerTask
{
	private Integer runCount = 0;
	private String detailMessage;
	private Boolean running = false;
	private Timer timerParent;
	private Integer idNotaFiscal;
	
	public Integer getRunTime()
	{
		return (this.runCount * 2);
	}
	
	public String getDetailMessage()
	{
		return this.detailMessage;
	}
	
	public void setDetailMessage(String value)
	{
		this.detailMessage = value;
	}
	
	public Boolean getRunning()
	{
		return this.running;
	}
	
	public void setRunning(Boolean value)
	{
		this.running = value;
	}
	
	public void setIdNotaFiscal(Integer value)
	{
		this.idNotaFiscal = value;
	}
	
	public Integer getIdNotaFiscal()
	{
		return this.idNotaFiscal;
	}
	
	public void setTimerParent(Timer value)
	{
		this.timerParent = value;
	}
	
	public Timer getTimerParent()
	{
		return this.timerParent;
	}
	
	public ImpressaoNotaFiscalService()
	{
		this.setRunning(true);
	}
	
	public void run()
	{ 
		try
		{
			NotaFiscal notaFiscal = new NotaFiscalService().recuperarPorId(this.idNotaFiscal);
			
			if(notaFiscal.getSituacaoNotaFiscal().getId() == DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalAutorizadaSefaz())
				new NotaFiscalService().imprimirNotaFiscal(notaFiscal);
			
			if(notaFiscal.getSituacaoNotaFiscal().getId() == DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalImpressa())
			{
				this.detailMessage = "Aguarde, sua Nota Fiscal está sendo impressa";
				this.timerParent.cancel();
				this.setRunning(false);
			}
			else if(this.getRunTime() >= 60)
			{
				this.detailMessage = "não foi possível imprimir a nota fiscal. Situação atual da Nota : " + notaFiscal.getSituacaoNotaFiscal().getDescricao();
				this.timerParent.cancel();
				this.setRunning(false);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			runCount++;
		}
	}
}
