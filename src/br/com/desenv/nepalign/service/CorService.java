package br.com.desenv.nepalign.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.encoding.XMLType;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.Cor;
import br.com.desenv.nepalign.model.EstoqueProduto;
import br.com.desenv.nepalign.persistence.CorPersistence;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class CorService extends GenericServiceIGN<Cor, CorPersistence>
{

	public CorService() 
	{
		super();
	}
	public Cor recuperarCorEscritorio(Integer idCor) throws Exception
	{
		return recuperarCorEscritorio(idCor, null, null);
	}
	public Cor recuperarCorEscritorio(Integer idCor,EntityManager manager, EntityTransaction transaction ) throws Exception
	{
		Boolean transacaoIndependente = false;
		Cor cor = null;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}



			if(DsvConstante.getParametrosSistema().get("caminhoWebServiceVendaEscritorio")==null)
			{
				return cor;
			}
			String endpoint = DsvConstante.getParametrosSistema().get("caminhoWebServiceVendaEscritorio");
			Service  service = new Service();
			Call call= (Call) service.createCall();
			call.setTargetEndpointAddress(new java.net.URL(endpoint));
			call.setOperationName("recuperarCor");
			call.addParameter("idCor", XMLType.XSD_INT, ParameterMode.IN);
			call.setReturnType(XMLType.XSD_STRING);

			String retorno = (String) call.invoke(new Object[] {idCor  });
			String[] arr = retorno.split(",");
			cor = new Cor();
			cor.setCodigo(Integer.parseInt(arr[0]));
			if(arr[1].equals("null"))
			{
				cor.setCorBasica1(null);
			}
			else
			{
				cor.setCorBasica1(new CorBasicaService().recuperarPorId(Integer.parseInt(arr[1])));
			}
			if(arr[2].equals("null"))
			{
				cor.setCorBasica2(null);
			}
			else
			{
				cor.setCorBasica2(new CorBasicaService().recuperarPorId(Integer.parseInt(arr[2])));
			}
			if(arr[3].equals("null"))
			{
				cor.setCorBasica3(null);
			}
			else
			{
				cor.setCorBasica3(new CorBasicaService().recuperarPorId(Integer.parseInt(arr[3])));
			}
			cor.setDescricaoCorEmpresa(arr[4]);
			cor.setDescricaoCorFabrica(arr[5]);
			cor = this.salvar(manager, transaction, cor);
			System.out.println("Usou WebService de Cor:"+idCor);

			if(transacaoIndependente)
				transaction.commit();

		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
		return cor;
	}


}

