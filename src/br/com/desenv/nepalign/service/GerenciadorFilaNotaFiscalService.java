package br.com.desenv.nepalign.service;


import gov.tjpr.migracao.persistence.Conexao;
import gov.tjpr.migracao.service.impl.PedidovendaCacheService;
import gov.tjpr.migracao.util.Constante;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;


import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.joda.time.DateTime;
import org.joda.time.Days;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.ItemNotaFiscal;
import br.com.desenv.nepalign.model.ItemPedidoVenda;
import br.com.desenv.nepalign.model.NotaFiscal;
import br.com.desenv.nepalign.model.PagamentoNotaFiscal;
import br.com.desenv.nepalign.model.PagamentoPedidoVenda;
import br.com.desenv.nepalign.model.PedidoVenda;
import br.com.desenv.nepalign.model.PlanoPagamento;
import br.com.desenv.nepalign.model.SituacaoNotaFiscal;
import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.nepalign.persistence.NotaFiscalPersistence;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class GerenciadorFilaNotaFiscalService extends GenericServiceIGN<NotaFiscal, NotaFiscalPersistence>
{

	private NotaFiscalService notaFiscalService;   
	private Logger log;
	int idPedidoVenda = 0;
	private static final EntityManager manager = ConexaoUtil.getEntityManager();
	private static final Connection connection = ConexaoUtil.getConexaoPadrao(); 
	private int qtPedImporPorVez = 20;


	public GerenciadorFilaNotaFiscalService() {
		super();
		notaFiscalService = new NotaFiscalService();
		this.log = Logger.getGlobal();		
	}

	public void gerenciarProcessoImportacaoCache(EntityManager manager,  int idEmpresaFisica) throws Exception
	{
		Boolean transacaoIndependente =false;

		try
		{
			if(manager == null)
			{
				manager = ConexaoUtil.getEntityManager();
				manager.getTransaction().begin();
				transacaoIndependente = true;
			}
			List<String> listaPedidos = null;
			PedidoVenda pedido = null;
			String numeroControle = "";
			int pedErro = 0;
			try
			{
				listaPedidos = recuperaPedidoCacheParaImportar(Integer.valueOf(idEmpresaFisica), manager);
				int iPed;

				for(iPed = 0; iPed < (listaPedidos.size() > qtPedImporPorVez ? qtPedImporPorVez : listaPedidos.size()); iPed++)
					try 
				{
						numeroControle = listaPedidos.get(iPed);
						pedido = gerarPedidoVendaBaseadoControleCache(Integer.valueOf(idEmpresaFisica), numeroControle,manager);

						ajustarValoresPedidoVenda(idEmpresaFisica, pedido.getId().intValue(), manager);

						//System.out.println("Pedido Importado! número de Controle : " + numeroControle);
				}
				catch(Exception exPed)
				{ 
					pedErro++;
					log.severe((new StringBuilder("Controle do caché com erro de importação: ")).append(numeroControle).toString());
					exPed.printStackTrace();
				}

				log.fine((new StringBuilder("Pedidos importados: ")).append(iPed).toString());
				log.fine((new StringBuilder("Pedidos com erro: ")).append(pedErro).toString());
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				throw new Exception("Erro ao recuperar pedidos para importação");
			}

			if(transacaoIndependente)
				manager.getTransaction().commit();
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				manager.getTransaction().rollback();
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}


	}



	public void gerenciarProcessoGeracaoNotasFiscais(int idEmpresaFisica, EntityManager manager)throws Exception
	{
		Boolean transacaoIndependente =false;

		try
		{
			if(manager == null)
			{
				manager = ConexaoUtil.getEntityManager();
				//manager.getTransaction().begin();
				transacaoIndependente = true;
			}

			int modoOperacao;
			PedidoVendaService pedService;
			int delayNotaFiscal;
			int contNotas;
			int contErros;
			EmpresaFisica empresa;
			Date dataFinal;
			Calendar dataIncial;
			NotaFiscalService nfService;
			SituacaoNotaFiscal situacaoNf;
			String planosPagamentoCartao;
			PedidoVendaService pedidoVendaService;
			//EntityManager manager;
			//EntityTransaction transaction;
			List<PedidoVenda> listaPedidos = null;
			pedService = new PedidoVendaService();
			int diasParaLeitura = -3;
			delayNotaFiscal = 20000;
			contNotas = 0;
			contErros = 0;
			empresa = new EmpresaFisica();
			empresa.setId(Integer.valueOf(idEmpresaFisica));
			dataFinal = new Date();
			dataIncial = Calendar.getInstance();
			dataIncial.setTime(dataFinal);
			dataIncial.add(6, diasParaLeitura);
			nfService = new NotaFiscalService();
			situacaoNf = new SituacaoNotaFiscal();
			ArrayList<NotaFiscal> listaNotas = null;
			List<NotaFiscal>  listaNotasGeradas = null;
			planosPagamentoCartao = "2, 3";
			PedidoVenda pedidoParaNotaFiscal = null;
			pedidoVendaService = new PedidoVendaService();
			//manager = ConexaoUtil.getEntityManager();
			//	transaction = manager.getTransaction();
			ItemPedidoVendaService itPedService = new ItemPedidoVendaService();
			List<ItemPedidoVenda> listaItensPeddo = null;

			try
			{
				//recupera parametro de modo de operação da fila. 
				//Caso não exista o parametro trabalha no modo sem compensação
				modoOperacao = Integer.parseInt(DsvConstante.getParametrosSistema().get("FILA_MODO_OPERACAO"));
			}
			catch(Exception exModoOperacao)
			{
				modoOperacao = 0;
				log.info("Parametro FILA_MODO_OPERACAO não está definido. Entrando no modo sem compensação...");
			}


			listaNotasGeradas = new ArrayList<NotaFiscal>();
			situacaoNf.setId(Integer.valueOf(13));
			PedidoVenda criterio = new PedidoVenda();
			criterio.setEmpresaFisica(empresa);
			listaPedidos = pedService.listarPedidosPendentesEmissaoNota(empresa, dataIncial.getTime(), dataFinal, planosPagamentoCartao, manager);
			NotaFiscal notaFiscal;

			for (PedidoVenda pedidoVenda : listaPedidos) 
			{
				try
				{

					listaItensPeddo = itPedService.buscaPorPedidoVenda(pedidoVenda);
					pedidoVenda.setListaItensPedidoVenda(listaItensPeddo);

					Thread.sleep((int)((delayNotaFiscal * Math.random())));
					notaFiscal = verificarControleAntesGeracaoNota(manager, idEmpresaFisica, pedidoVenda.getNumeroControle());

					if(notaFiscal == null) 
					{	
						manager.getTransaction().begin();
						if (modoOperacao==1){
							pedidoParaNotaFiscal = pedidoVendaService.retornaPedidoAjustadoNF(pedidoVenda, planosPagamentoCartao, manager);}
						else{
							pedidoParaNotaFiscal = pedidoVenda;
							PagamentoPedidoVenda criterioPagamento = new PagamentoPedidoVenda();
							criterioPagamento.setPedidoVenda(pedidoVenda);
							pedidoParaNotaFiscal.setListaPagamentosPedidoVenda(new PagamentoPedidoVendaService().listarPorObjetoFiltro(manager, criterioPagamento, null, null, null));}

						if (pedidoVenda.getListaItensPedidoVenda().size()==pedidoParaNotaFiscal.getListaItensPedidoVenda().size()){
							notaFiscal = gerarNotaFiscalFila(empresa.getId(), pedidoParaNotaFiscal, null, Boolean.valueOf(false), manager);
						}

						else{
							notaFiscal = gerarNotaFiscalFila(empresa.getId(), pedidoParaNotaFiscal, null, Boolean.valueOf(true), manager);
						}


						listaNotas = new ArrayList<NotaFiscal>();
						listaNotas.add(notaFiscal);
						notaFiscalService.enviarNotaFiscalNfe(listaNotas, manager, manager.getTransaction());						
						contNotas++;
						mudarStatusItensPedido(pedidoParaNotaFiscal.getListaItensPedidoVenda(), 2, manager); 	
						//commita pois anota foi gerada. Se não tiver essa linha irão ocorrer erros quando a emissão manual for usada junto com a fila.
						manager.getTransaction().commit();
					}
				}
				catch(Exception exNota)
				{
					//if(transaction != null && transaction.isActive())
					//	transaction.rollback();
					log.severe((new StringBuilder("Tentativa de gerar NF falhou. Controle: ")).append(pedidoVenda.getNumeroControle()).toString());
					exNota.printStackTrace();
					contErros++;
				}
			}

			situacaoNf = new SituacaoNotaFiscal();
			situacaoNf = new SituacaoNotaFiscalService().recuperarPorId(manager, Integer.parseInt(DsvConstante.getParametrosSistema().get("idSituacaoNotaFiscalAutorizadaSefaz")));
			notaFiscal = new NotaFiscal();
			notaFiscal.setEmpresa(empresa);
			notaFiscal.setDataEmissao(new Date());
			notaFiscal.setSituacaoNotaFiscal(situacaoNf);
			listaNotasGeradas = nfService.listarPorObjetoFiltro(notaFiscal);

			notaFiscalService.imprimirNotaFiscal(listaNotasGeradas, null, null);

			log.fine((new StringBuilder("Notas Geradas: ")).append(contNotas).toString());
			log.fine((new StringBuilder("Notas com erro: ")).append(contErros).toString());

			//if(transacaoIndependente)
			//manager.getTransaction().commit();

		}
		catch(Exception ex)
		{

			throw ex;
		}



	}

	public List<String> recuperaPedidoCacheParaImportar(Integer idEmpresaFisica, EntityManager manager)
			throws Exception
			{
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		DateTime dataInicial = new DateTime(sdf.parse("31/12/1840"));
		int dias = Days.daysBetween(dataInicial, new DateTime(new Date())).getDays();
		//diminui 3 dias para poder pegar pedidos que ficaram pra traz
		dias = dias - 3;
		ArrayList<String> lstPedidos = new ArrayList<String>();
		String sqlPedidosCache = (new StringBuilder("SELECT * FROM IMPORTACAO.TBPEDIDOVENDA WHERE tipo = 1 and dataVenda >= ")).append(dias).toString();
		Connection conCache = Conexao.getConexaoCache();
		Statement stm = conCache.createStatement();
		PedidoVenda criterio = new PedidoVenda();
		PedidoVendaService pedService = new PedidoVendaService();
		EmpresaFisica empresa = new EmpresaFisica();
		empresa.setId(idEmpresaFisica);
		for(ResultSet rsCache = stm.executeQuery(sqlPedidosCache); rsCache.next();)
		{
			String controle = rsCache.getString("NUMEROCONTROLE");
			criterio = new PedidoVenda();
			criterio.setEmpresaFisica(empresa);
			criterio.setNumeroControle(controle);
			if(pedService.listarPorObjetoFiltro(manager, criterio, null, null, null).isEmpty())
				lstPedidos.add(controle);
		}

		if(conCache != null && !conCache.isClosed())
			conCache.close();
		return lstPedidos;
			}

	/*public PedidoVenda importarPedidoCache(Integer idEmpresaFisica, String numeroControle)
			throws Exception
	{
		PedidovendaCacheService pedCacheService;
		Connection con;
		PedidoVendaService pedidoService;
		pedCacheService = new PedidovendaCacheService();
		con = null;
		PedidoVenda pedNovo = null;
		pedidoService = new PedidoVendaService();
		PedidoVenda pedidovenda;
		PlanoPagamento planoPagamento;
		PagamentoPedidoVenda pagPedido;
		try
		{
			String urlCache = DsvConstante.getParametrosSistema().get("URL_CACHE");
			con = ConexaoUtil.getConexaoPadrao();
			Integer idPedidoNepal = pedCacheService.migrarCachePedidoEspecifico(idEmpresaFisica.toString(), numeroControle, urlCache, 0, con);
			pedNovo = (PedidoVenda)pedidoService.recuperarPorId(idPedidoNepal);
			planoPagamento = pedNovo.getPlanoPagamento();
			pagPedido = new PagamentoPedidoVenda();
			pagPedido.setPedidoVenda(pedNovo);
			pagPedido.setAgenciaCheque(null);
			pagPedido.setBancoCheque(null);
			pagPedido.setCaixaDia(null);
			pagPedido.setChequeDataEmetido(null);
			pagPedido.setChequeRecebido(null);
			pagPedido.setCnpjCpf(null);
			pagPedido.setControlePagamentoCartao(null);
			pagPedido.setCpfCheque(null);
			pagPedido.setDataPagamento(new Date());
			pagPedido.setEntrada("N");
			pagPedido.setFormaPagamento(planoPagamento.getFormaPagamento());
			pagPedido.setIdNotaFiscal(null);
			pagPedido.setItemSequencial("1");
			pagPedido.setLancamentoContaReceber(null);
			pagPedido.setNomeCheque(null);
			pagPedido.setNumeroCheque(null);
			pagPedido.setNumeroDocumento(pedNovo.getNumeroControle());
			pagPedido.setNumeroDuplicata(null);
			pagPedido.setNumeroParcelas(Integer.valueOf(1));
			pagPedido.setPagamentoCartao(null);
			pagPedido.setSituacaoLancamento(Integer.valueOf(1));
			Usuario usu = new Usuario();
			usu.setId(Integer.valueOf(11));
			pagPedido.setUsuario(usu);
			pagPedido.setValor(pedNovo.getValorTotalPedido());
			(new PagamentoPedidoVendaService()).salvar(pagPedido);
			pedidovenda = pedNovo;

			return pedidovenda;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(con != null && !con.isClosed())
				con.close();
		}
	}*/

	public NotaFiscal gerarNotaFiscalFila(Integer idEmpresaFisica, PedidoVenda pedidoVenda, String cpf, Boolean flgValoAjustado, EntityManager manager)
			throws Exception
			{
		NotaFiscal nf = null;
		try
		{
			nf = new NotaFiscal();
			nf.setPedidoVenda(pedidoVenda);
			EmpresaFisica empresa = new EmpresaFisica();
			empresa.setId(idEmpresaFisica);
			nf.setEmpresa(empresa);
			nf.setModelo(Integer.valueOf(65));
			return notaFiscalService.gerarNotaFiscalConsumidor(pedidoVenda, nf, flgValoAjustado, manager, manager.getTransaction());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
			}

	public void ajustarValoresPedidoVenda(int idEmpresaFisica, int idPedidoVenda, EntityManager manager)
			throws Exception
			{
		PedidoVenda pedido = new PedidoVenda();
		PedidoVendaService pedService = new PedidoVendaService();
		ItemPedidoVenda itemPedidoCriterio = new ItemPedidoVenda();
		PagamentoPedidoVenda pgPedido = new PagamentoPedidoVenda();
		ItemPedidoVendaService itemService = new ItemPedidoVendaService();
		PagamentoPedidoVendaService pagamentoService = new PagamentoPedidoVendaService();
		double valorTotalPedido = 0.0D;
		double percMargen = 120D;
		double margemMinima = 30D;
		double novoValorVenda = 0.0D;
		try
		{
			pedido = pedService.recuperarPorId(manager, idPedidoVenda);
		}
		catch(Exception pedEx)
		{
			throw new Exception("Pedido não existe.");
		}
		itemPedidoCriterio.setPedidoVenda(pedido);
		List<ItemPedidoVenda> itensPedido = itemService.listarPorObjetoFiltro(manager, itemPedidoCriterio, null, null, null);  
		if(itensPedido.isEmpty())
			throw new Exception("Pedido de venda sem itens.");

		for (ItemPedidoVenda itemPedidoVenda : itensPedido) 
		{
			double limite = 10D * Math.random();
			double valorItem = itemPedidoVenda.getPrecoVenda().doubleValue();
			double valorCustoAp = valorItem / ((percMargen + 100D) / 100D);
			novoValorVenda = valorCustoAp + (valorCustoAp * (margemMinima + limite)) / 100D;
			BigDecimal bd = new BigDecimal(novoValorVenda).setScale(2, RoundingMode.HALF_EVEN);
			itemPedidoVenda.setPrecoVendaAjustado(bd.doubleValue());
			itemPedidoVenda.setSituacaoFila(Integer.valueOf(1));
			itemService.atualizar(manager, manager.getTransaction(), itemPedidoVenda);
			valorTotalPedido = itemPedidoVenda.getPrecoVendaAjustado()*itemPedidoVenda.getQuantidade() + valorTotalPedido;
		}

		pgPedido.setPedidoVenda(pedido);

		List<PagamentoPedidoVenda> pagamentos = pagamentoService.listarPorObjetoFiltro(manager, pgPedido, null, null, null);

		if(pagamentos.isEmpty())
			throw new Exception("Pedido de venda sem pagamentos.");

		double valorParcela = valorTotalPedido / pagamentos.size();
		for (PagamentoPedidoVenda pagamentoPedidoVenda : pagamentos) 
		{
			pagamentoPedidoVenda.setValorAjustado(Double.valueOf(valorParcela));
			pagamentoService.atualizar(manager, manager.getTransaction(), pagamentoPedidoVenda);
		}

		pedido.setSituacaoFila(Integer.valueOf(1));
		pedido.setValorTotalVendaAjustado(valorTotalPedido);
		pedService.atualizar(manager, manager.getTransaction(), pedido);
			}



	public void excluirControle(PedidoVenda pedidoVenda, EntityManager manager) throws Exception
	{
		boolean transacaoIndependente = false;
		try
		{
			if (manager==null)
			{
				manager = ConexaoUtil.getEntityManager();
				manager.getTransaction().begin();
				transacaoIndependente = true;
			}


			ItemPedidoVenda itemPedidoCriterio = new ItemPedidoVenda();
			itemPedidoCriterio.setPedidoVenda(pedidoVenda);
			List<ItemPedidoVenda> listaItensExclusao = new ItemPedidoVendaService().listarPorObjetoFiltro(itemPedidoCriterio);

			PagamentoPedidoVenda pagamentoPedidoVendaCriterio = new PagamentoPedidoVenda();
			pagamentoPedidoVendaCriterio.setPedidoVenda(pedidoVenda);
			List<PagamentoPedidoVenda> listaPagamentoExclusao = new PagamentoPedidoVendaService().listarPorObjetoFiltro(pagamentoPedidoVendaCriterio);

			
			NotaFiscal notaFiscalCriterio = new NotaFiscal();
			notaFiscalCriterio.setPedidoVenda(pedidoVenda);
			List<NotaFiscal> listaNota = new NotaFiscalService().listarPorObjetoFiltro(notaFiscalCriterio);

			if(listaPagamentoExclusao.size() > 0)
			{
				for (PagamentoPedidoVenda pagamentoPedidoVenda : listaPagamentoExclusao) {
					PagamentoPedidoVendaService pagamentoService = new PagamentoPedidoVendaService();
					pagamentoService.excluir(pagamentoPedidoVenda);
				}
			}

			if(listaItensExclusao.size() > 0)
			{
				for (ItemPedidoVenda itemPedidoVenda : listaItensExclusao) {
					ItemPedidoVendaService itemPedidoVendaService = new ItemPedidoVendaService();
					itemPedidoVendaService.excluir(itemPedidoVenda);
				}	
			}
			
			if(listaNota.size() > 0)
			{
				for (NotaFiscal notaFiscal : listaNota) {
					NotaFiscalService notaFiscalService = new NotaFiscalService();
					notaFiscal.setPedidoVenda(null);
					notaFiscalService.atualizar(notaFiscal);
				}
			}


			PedidoVendaService pedidoVendaService = new PedidoVendaService();
			pedidoVendaService.excluir(pedidoVenda);
		}
		catch(Exception ex)
		{
			if (transacaoIndependente==true)
				manager.getTransaction().rollback();

			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if (transacaoIndependente==true && manager!=null)
				manager.close();
		}
	}


	public PedidoVenda gerarPedidoVendaBaseadoControleCache(final Integer idEmpresaFisica, final String numeroControle)
			throws Exception
			{
		if(numeroControle == null || numeroControle.equals(""))
			throw new Exception("Numero controle deve ser informado!");
		if(idEmpresaFisica == null || idEmpresaFisica.intValue() == 0)
			throw new Exception("Empresa F\355sica deve ser informada!");

		PedidoVendaService pedidoService = new PedidoVendaService();

		EmpresaFisica empresaCriterio = new EmpresaFisica();
		empresaCriterio.setId(idEmpresaFisica);

		PedidoVenda criterio = new PedidoVenda();
		criterio.setEmpresaFisica(empresaCriterio);
		criterio.setNumeroControle(numeroControle);
		List<PedidoVenda> listaPedidos = pedidoService.listarPorObjetoFiltro(manager, criterio, null, null, null);
		if(listaPedidos.size() > 0)
		{
			PedidoVenda pedidoExclusao = listaPedidos.get(0);
			this.excluirControle(pedidoExclusao, null);
		}

		PedidoVenda pedidoVenda = this.gerarPedidoVendaBaseadoControleCache(idEmpresaFisica, numeroControle, null);

		return pedidoVenda;
	}
	
	public PedidoVenda gerarPedidoVendaBaseadoControleCache(final Integer idEmpresaFisica, final String numeroControle, EntityManager manager)
			throws Exception
			{
		final PedidovendaCacheService pedCacheService = new PedidovendaCacheService();
		final String urlCache = DsvConstante.getParametrosSistema().get("strConexaoCache");

		PedidoVenda pedNovo = null;
		boolean transacaoIndependente = false;

		try
		{
			if (manager==null)
			{
				manager = ConexaoUtil.getEntityManager();
				manager.getTransaction().begin();
				transacaoIndependente = true;
			}

			int idUsuarioPadrao = 0;
			try
			{
				idUsuarioPadrao = DsvConstante.getParametrosSistema().getInt("idVendedorPadrao");
			}
			catch(Exception exPed)
			{
				throw new Exception("Parameto de idUsuarioPadrao n\303\243o foi encontrado.");
			}

			((Session) manager.getDelegate()).doWork(
					new Work()  
					{
						@Override
						public void execute(Connection connection) throws SQLException 
						{
							try {
								idPedidoVenda = pedCacheService.migrarCachePedidoEspecifico(idEmpresaFisica.toString(), numeroControle, urlCache, 0, connection);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					});

			if (idPedidoVenda>0)
			{
				pedNovo = (PedidoVenda) manager.createNativeQuery("SELECT * FROM pedidoVenda WHERE numeroControle = '" + numeroControle + "';", PedidoVenda.class).getSingleResult();
				System.out.println("Pedido Importado! Numero de Controle : " + pedNovo.getNumeroControle());
				PlanoPagamento planoPagamento = pedNovo.getPlanoPagamento();
				PagamentoPedidoVenda pagPedido = new PagamentoPedidoVenda();
				pagPedido.setPedidoVenda(pedNovo);
				pagPedido.setAgenciaCheque(null);
				pagPedido.setBancoCheque(null);
				pagPedido.setCaixaDia(null);
				pagPedido.setChequeDataEmetido(null);
				pagPedido.setChequeRecebido(null);
				pagPedido.setCnpjCpf(null);
				pagPedido.setControlePagamentoCartao(null);
				pagPedido.setCpfCheque(null);
				pagPedido.setDataPagamento(new Date());
				pagPedido.setEntrada("N");
				pagPedido.setFormaPagamento(planoPagamento.getFormaPagamento());
				pagPedido.setIdNotaFiscal(null);
				pagPedido.setItemSequencial("1");
				pagPedido.setLancamentoContaReceber(null);
				pagPedido.setNomeCheque(null);
				pagPedido.setNumeroCheque(null);
				pagPedido.setNumeroDocumento(pedNovo.getNumeroControle());
				pagPedido.setNumeroDuplicata(null);
				pagPedido.setNumeroParcelas(Integer.valueOf(1));
				pagPedido.setPagamentoCartao(null);
				pagPedido.setSituacaoLancamento(Integer.valueOf(1));
				Usuario usu = new Usuario();
				usu.setId(Integer.valueOf(11));
				pagPedido.setUsuario(usu);
				pagPedido.setValor(pedNovo.getValorTotalPedido());
				(new PagamentoPedidoVendaService()).salvar(manager, manager.getTransaction(), pagPedido);
			}
			else
			{
				log.severe("Pedido venda " + numeroControle + " não pode ser importado do CACHÉ");
			}

			if (transacaoIndependente==true)
				manager.getTransaction().commit();

			return pedNovo;
		}
		catch(Exception ex)
		{
			if (transacaoIndependente==true)
				manager.getTransaction().rollback();

			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if (transacaoIndependente==true && manager!=null)
				manager.close();
		}

			}	
	public NotaFiscal gerarNotaFiscalBaseadoControleCache(EntityManager manager, Integer idEmpresaFisica, String numeroControle, String cpf)
			throws Exception
			{
		NotaFiscal nf = null;
		PedidovendaCacheService pedCacheService = new PedidovendaCacheService();
		PedidoVenda pedNovo = null;
		PedidoVendaService pedidoService = new PedidoVendaService();

		try
		{
			PedidoVenda criterio = new PedidoVenda();
			criterio.setNumeroControle(numeroControle);
			List<PedidoVenda> listaPedidos = pedidoService.listarPorObjetoFiltro(manager, criterio, null, null, null);
			if(listaPedidos.size() > 0)
			{
				pedNovo = listaPedidos.get(0);
				NotaFiscal criterioNotaFiscal = new NotaFiscal();
				criterioNotaFiscal.setPedidoVenda(pedNovo);
				List<NotaFiscal> listaNotas = (new NotaFiscalService()).listarPorObjetoFiltro(manager, criterioNotaFiscal, null, null, null);
				if(listaNotas.size() == 1)
				{
					nf = listaNotas.get(0);
					if(nf.getSituacaoNotaFiscal().getId().intValue() == DsvConstante.getParametrosSistema().getInt("idSituacaoNotaFiscalGerada ") || nf.getSituacaoNotaFiscal().getId().intValue() == DsvConstante.getParametrosSistema().getInt("idSituacaoNotaFiscalRejeitada "))
						Logger.getGlobal().fine((new StringBuilder("Controle ")).append(numeroControle).append(" pode ter gerado mais de uma nota fiscal.").toString());
					else
						(new NotaFiscalService()).excluir(listaNotas, manager.getTransaction(), manager);
				} else
					if(listaNotas.size() > 1)
						throw new Exception((new StringBuilder("Controle ")).append(numeroControle).append(" j\341 foi utilizado em mais de uma nota fiscal.").toString());
			} else
			{
				String urlCache = DsvConstante.getParametrosSistema().get("URL_CACHE");
				Integer idPedidoNepal = pedCacheService.migrarCachePedidoEspecifico(idEmpresaFisica.toString(), numeroControle, urlCache, 0, connection);
				pedNovo = pedidoService.recuperarPorId(manager, idPedidoNepal); 
				PlanoPagamento planoPagamento = pedNovo.getPlanoPagamento();
				PagamentoPedidoVenda pagPedido = new PagamentoPedidoVenda();
				pagPedido.setAgenciaCheque(null);
				pagPedido.setBancoCheque(null);
				pagPedido.setCaixaDia(null);
				pagPedido.setChequeDataEmetido(null);
				pagPedido.setChequeRecebido(null);
				pagPedido.setCnpjCpf(null);
				pagPedido.setControlePagamentoCartao(null);
				pagPedido.setCpfCheque(null);
				pagPedido.setDataPagamento(new Date());
				pagPedido.setEntrada("N");
				pagPedido.setFormaPagamento(planoPagamento.getFormaPagamento());
				pagPedido.setIdNotaFiscal(null);
				pagPedido.setItemSequencial("1");
				pagPedido.setLancamentoContaReceber(null);
				pagPedido.setNomeCheque(null);
				pagPedido.setNumeroCheque(null);
				pagPedido.setNumeroDocumento(pedNovo.getNumeroControle());
				pagPedido.setNumeroDuplicata(null);
				pagPedido.setNumeroParcelas(Integer.valueOf(1));
				pagPedido.setPagamentoCartao(null);
				pagPedido.setSituacaoLancamento(Integer.valueOf(1));
				Usuario usu = new Usuario();
				usu.setId(Integer.valueOf(11));
				pagPedido.setUsuario(usu);
				pagPedido.setValor(pedNovo.getValorTotalPedido());
				(new PagamentoPedidoVendaService()).salvar(manager, manager.getTransaction(), pagPedido);
			}
			nf = new NotaFiscal();
			nf.setPedidoVenda(pedNovo);
			EmpresaFisica empresa = new EmpresaFisica();
			empresa.setId(idEmpresaFisica);
			nf.setEmpresa(empresa);
			nf.setModelo(Integer.valueOf(65));
			return notaFiscalService.gerarNotaFiscalConsumidor(pedNovo, nf, Boolean.valueOf(false), manager, manager.getTransaction());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
			}
	public NotaFiscal verificarControleAntesGeracaoNota(int idEmpresaFisica, String numeroControle)throws Exception
	{
		NotaFiscal notaFiscal = this.verificarControleAntesGeracaoNota(null, idEmpresaFisica, numeroControle);
		return notaFiscal;

	}
	public NotaFiscal verificarControleAntesGeracaoNota(EntityManager manager, int idEmpresaFisica, String numeroControle)
			throws Exception
			{
		boolean transacaoIndependente = false;


		try
		{
			if (manager==null)
			{
				manager = ConexaoUtil.getEntityManager();
				manager.getTransaction().begin();
				transacaoIndependente = true;
			}
			NotaFiscal notaFiscal = null;
			NotaFiscal criterio = new NotaFiscal();
			NotaFiscalService nfService = new NotaFiscalService();
			PedidoVenda criterioPedidoVenda = new PedidoVenda();
			EmpresaFisica empresaFisica = new EmpresaFisica();
			empresaFisica.setId(Integer.valueOf(idEmpresaFisica));
			criterioPedidoVenda.setEmpresaFisica(empresaFisica);
			criterioPedidoVenda.setNumeroControle(numeroControle);
			List<PedidoVenda> lista = (new PedidoVendaService()).listarPorObjetoFiltro(manager, criterioPedidoVenda, null, null, null);
			if(lista.size() > 0)
			{
				PedidoVenda pedidoVenda = lista.get(0);
				criterio.setPedidoVenda(pedidoVenda);
				List<NotaFiscal> listaNota = nfService.listarPorObjetoFiltro(manager, criterio, null, null, null);
				if(listaNota.size() > 0)
					notaFiscal = listaNota.get(0);
			}

			if (transacaoIndependente==true)
				manager.getTransaction().commit();

			return notaFiscal;

		}
		catch(Exception ex)
		{
			if (transacaoIndependente==true)
				manager.getTransaction().rollback();

			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if (transacaoIndependente==true && manager!=null)
				manager.close();
		}
	}
	
	public PedidoVenda verificarControleImportado(int idEmpresaFisica, String numeroControle)throws Exception
	{
		PedidoVenda pedidoVenda = this.verificarControleImportado(null, idEmpresaFisica, numeroControle);
		return pedidoVenda;

	}
	public PedidoVenda verificarControleImportado(EntityManager manager, int idEmpresaFisica, String numeroControle)
			throws Exception
			{
		boolean transacaoIndependente = false;


		try
		{
			if (manager==null)
			{
				manager = ConexaoUtil.getEntityManager();
				manager.getTransaction().begin();
				transacaoIndependente = true;
			}
			PedidoVenda criterioPedidoVenda = new PedidoVenda();
			EmpresaFisica empresaFisica = new EmpresaFisica();
			empresaFisica.setId(Integer.valueOf(idEmpresaFisica));
			criterioPedidoVenda.setEmpresaFisica(empresaFisica);
			criterioPedidoVenda.setNumeroControle(numeroControle);
			List<PedidoVenda> lista = (new PedidoVendaService()).listarPorObjetoFiltro(manager, criterioPedidoVenda, null, null, null);
			PedidoVenda pedidoVenda = null;
			if(lista.size() > 0)
				pedidoVenda = lista.get(0);

			if (transacaoIndependente==true)
				manager.getTransaction().commit();

			return pedidoVenda;

		}
		catch(Exception ex)
		{
			if (transacaoIndependente==true)
				manager.getTransaction().rollback();

			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if (transacaoIndependente==true && manager!=null)
				manager.close();
		}
			}
	

	private void mudarStatusItensPedido(List<ItemPedidoVenda> listaItensPedido, int novoStatusItem, EntityManager manager) throws Exception
	{
		ItemPedidoVendaService itemPedService = new ItemPedidoVendaService();
		for (ItemPedidoVenda itemPedidoVenda : listaItensPedido) 
		{
			itemPedidoVenda.setSituacaoFila(2);
			itemPedService.atualizar(manager, manager.getTransaction(), itemPedidoVenda);
		}
	}


	public static void main(String[] args)throws Exception
	{
		final GerenciadorFilaNotaFiscalService ger = new GerenciadorFilaNotaFiscalService();
		//int tempoMinimoGeracao = 1500000; //25 min
		int tempoMinimoGeracao = 3000000; //50 min
		System.out.println("Fila iniciada em: " + new Date());
		Runtime.getRuntime().addShutdownHook(new Thread()
		{
			@Override
			public void run()
			{
				System.out.println("Encerrando aplicação...");

				if(manager.isOpen())
					manager.close();

				try 
				{
					if(!connection.isClosed())
						connection.close();
				} 
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
		});

		while(true) 
		{


			try
			{
				System.out.println("Inicio do processamento dos pedidos. " + new Date());
				manager.getTransaction().begin();
				ger.gerenciarProcessoImportacaoCache(manager,Constante.CODIGO_EMPRESA_PADRAO);
				manager.getTransaction().commit();
				System.out.println("Final do processamento dos pedidos. " + new Date());
				//manager.close();
			}
			catch(Exception ex)
			{
				if(manager.getTransaction().isActive())
					manager.getTransaction().rollback();
				ex.printStackTrace();
				Logger.getGlobal().log(Level.SEVERE, "Falha na importa\347\343o de pedidos do CACH\311.");
			}

			try
			{
				System.out.println("Inicio do processamento das notas. " + new Date());
				//manager.getTransaction().begin();
				ger.gerenciarProcessoGeracaoNotasFiscais(Constante.CODIGO_EMPRESA_PADRAO, manager);
				//manager.getTransaction().commit();
				//manager.close();
				System.out.println("Final do processamento das notas. " + new Date());
			}
			catch(Exception ex)
			{
				if(manager.getTransaction().isActive())
					manager.getTransaction().rollback();

				Logger.getGlobal().log(Level.SEVERE, "Falha na geração de notas da fila.");
				ex.printStackTrace();
			}

			Thread.sleep((int)(tempoMinimoGeracao));	
		}
	}	
}
