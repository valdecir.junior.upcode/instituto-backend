package br.com.desenv.nepalign.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.User;
import br.com.desenv.nepalign.model.Atendido;
import br.com.desenv.nepalign.persistence.AtendidoPersistence;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class AtendidoService extends GenericServiceIGN<Atendido, AtendidoPersistence> {

	public static String FILEPATH = "..//imagens//";
	public static File file = new File(FILEPATH);
	public static Connection con = null;
	public static String caminho = "..//imagens//";

	public byte[] foto = null;

	public AtendidoService() {
		super();
	}

	public byte[] buscarFoto(Atendido atendido) throws Exception {

		EntityManager manager = ConexaoUtil.getEntityManager();

		Integer id = atendido.getId();
		String nomeFoto = id + ".jpg";

		File file = new File("..//imagens//");

		File afile[] = file.listFiles();

		int i = 0;

		for (int j = afile.length; i < j; i++) {

			File arquivos = afile[i];

			if (nomeFoto.equals(arquivos.getName())) {

				System.out.println(arquivos.getName());

				FileInputStream fis = new FileInputStream(arquivos.getAbsoluteFile());
				ByteArrayOutputStream bos = new ByteArrayOutputStream();

				byte[] buf = new byte[1024];

				try {
					for (int readNum; (readNum = fis.read(buf)) != -1;) {
						bos.write(buf, 0, readNum); // no doubt here is 0
					}
				} catch (IOException ex) {
					System.out.println(ex);
				}
				foto = bos.toByteArray();
			}
		}

		manager.close();
		return foto;

	}

	public void salvarFoto(byte[] foto, Atendido atendido) throws Exception {

		try {

			String caminhoFile = atendido.getFotoCaminho();

			atendido.setFotoCaminho(caminhoFile);

			Integer idAtendido = atendido.getId();

			FileOutputStream os = new FileOutputStream(file + "//" + idAtendido + ".jpg");

			// Starts writing the bytes in it

			System.out.println("Successfully" + " fotos inserted");

			// Close the file
			os.write(foto);

			os.close();

		} catch (Exception e) {

			e.printStackTrace();

			throw e;

		} finally {

		}

	}

	public void atualizarFoto(byte[] foto, Atendido atendido) throws Exception {

		try {

			String caminhoFile = atendido.getFotoCaminho();

			atendido.setFotoCaminho(caminhoFile);

			Integer idAtendido = atendido.getId();

			FileOutputStream os = new FileOutputStream(file + "//" + idAtendido + ".jpg");

			// Starts writing the bytes in it

			System.out.println("Successfully" + " byte inserted");

			// Close the file
			os.write(foto);

			os.close();

		} catch (Exception e) {

			e.printStackTrace();

			throw e;

		} finally {

		}
	}
}