package br.com.desenv.nepalign.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javassist.bytecode.stackmap.TypeData.ClassName;

import org.iso_relax.dispatcher.impl.IgnoreVerifier;

import com.sun.xml.ws.security.opt.impl.enc.DataEncryptionProcessor;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.MovimentacaoEstoque;
import br.com.desenv.nepalign.util.AuditorInformacoesSistema;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;



public class AjusteEstoqueService 
{
	private static final Logger log = Logger.getLogger(ClassName.class.getName());
	
	private void ajustaCodigosBarrasSemCusto() throws Exception
	{
		Connection con = null;
		Statement stmep = null;
		ResultSet rsep = null;	
		Statement stm = null;
		ResultSet rs = null;
		Statement stm2 = null;
		ResultSet rs2 = null;		
		Statement stm3 = null;
		ResultSet rs3 = null;		
		Statement stm4 = null;
		ResultSet rs4 = null;		
		Statement stmUpdate = null;
		ResultSet rsUpdate = null;		
		int mes = 5;
		int ano = 2014;
		
		String sql = "select idEmpresaFisica, idEstoqueProduto from SaldoEstoqueProduto where (custo = 0 or custo is null) " +
				"and ano = " + ano + " and mes = " + mes + " and idEmpresaFisica <> 60 and idEmpresaFisica <> 6 order by idEstoqueProduto ";
		
		con = ConexaoUtil.getConexaoPadrao();
		stm = con.createStatement();
		rs = stm.executeQuery(sql);
		//tree
		
				
		int idEmpresaFisica;
		int idEstoqueProduto;
		int idProduto;
		double novoPreco;
		double novoCusto;
		String sql2 = "";
		String sql3 = "";
		String sql4 = "";
		int cont=0;
		int contAchouCaso1=0;
		int contAchouCaso2=0;
		int contAchouCaso3=0;
		int contAchouCaso4=0;
		HashMap<Integer, Integer> estoquesSemPreco = new HashMap<>();
		String empGrupo1 = "2,4,9";
		String empGrupo2 = "1,3,5,7,8,13";
		
		while (rs.next())
		{
			stmep = con.createStatement();
			idEmpresaFisica = rs.getInt("idEmpresaFisica");
			idEstoqueProduto = rs.getInt("idEstoqueProduto");			
			rsep = stmep.executeQuery("SELECT idProduto from EstoqueProduto where idEstoqueProduto = " + idEstoqueProduto);
			rsep.next();
			idProduto = rsep.getInt("idProduto");
			stmep.close();
			
			//tenta primeiro achar o preço do produto em outros Saldos do mesmo codigo de barras e empresa; Caso 1
			idEmpresaFisica = rs.getInt("idEmpresaFisica");
			idEstoqueProduto = rs.getInt("idEstoqueProduto");
			sql2 = "select max(venda) as novoPreco, max(custo) as custo, idEmpresaFisica, idEstoqueProduto from SaldoEstoqueProduto " + 
					"where ano = " + ano + " and mes = " + mes + " and idEmpresaFisica = " + idEmpresaFisica + 
					" and idEstoqueProduto = " + idEstoqueProduto + "";
			
			stm2 = con.createStatement();
			rs2 = stm2.executeQuery(sql2);
			novoPreco=0;
			novoCusto=0;
			if (rs2.next())
			{
				novoPreco = rs2.getDouble("novoPreco");
				novoCusto = rs2.getDouble("custo");
				
			}
			if (novoCusto>0)
			{
				contAchouCaso1++;
			}
			else
			{
				//Tenta achar em qualquer outra empresa que tenha o mesmo codigo de estoque
				sql2 = "select max(venda) as novoPreco, max(custo) as custo, idEmpresaFisica, idEstoqueProduto from SaldoEstoqueProduto " + 
						"where ano = " + ano + " and mes = " + mes + " and idEstoqueProduto = " + idEstoqueProduto + "";
				
				stm2 = con.createStatement();
				rs2 = stm2.executeQuery(sql2);
				
				if (rs2.next())
				{
					novoPreco = rs2.getDouble("novoPreco");
					novoCusto = rs2.getDouble("custo");
				}
				if (novoCusto > 0)
				{
					contAchouCaso2++;
				}
				else
				{
					//tenta achar na saldo estoque produto mas em qualquer empresa de qualquer Saldo Estoque Produto que tenha o mesmo ID Produto
					sql3 = "select max(venda) as novoPreco, max(custo) as custo, idEmpresaFisica, estoqueproduto.idEstoqueProduto " +
							"from SaldoEstoqueProduto " +
							" inner join estoqueproduto on estoqueproduto.idEstoqueProduto = saldoestoqueproduto.idEstoqueProduto " +
							"where ano = " + ano + " and mes = " + mes + " and idProduto = " + idProduto;
					
					stm3 = con.createStatement();
					rs3 = stm3.executeQuery(sql3);
					
					if (rs3.next())
					{
						novoPreco = rs3.getDouble("novoPreco");
						novoCusto = rs3.getDouble("custo");
					}
					if (novoCusto > 0)
					{
						contAchouCaso3++;
					}
					else
					{
						//Apela para a TMPSaldoEstoqueProduto que possui todos os codigos do cache´referentes a mes 1, ano 2014 do CACHÉ
						sql4 = "select max(venda) as novoPreco, max(custo) as custo, idEmpresaFisica, tmpsaldoestoqueproduto.idEstoqueProduto " +
								"from TmpSaldoEstoqueProduto " +
								" inner join estoqueproduto on estoqueproduto.idEstoqueProduto = tmpsaldoestoqueproduto.idEstoqueProduto " +
								"where ano = " + 2014 + " and mes = " + 1 + " and idProduto = " + idProduto;
						
						stm4 = con.createStatement();
						rs4 = stm4.executeQuery(sql4);
						
						if (rs4.next())
						{
							novoPreco = rs4.getDouble("novoPreco");
							novoCusto = rs4.getDouble("custo");
						}					
						if (novoCusto > 0)
						{
							contAchouCaso4++;
						}
						else
						{
							estoquesSemPreco.put(idEstoqueProduto, idEstoqueProduto);
							System.out.println("Preço não encontrado para o codigo de barras: " + idEstoqueProduto);
							
						}
					}
				}	
			}
			if (novoCusto>0)
			{
				stmUpdate = con.createStatement();
				StringBuilder str = new StringBuilder();
				str.append("UPDATE SaldoEstoqueProduto SET custo = " + novoCusto + "");  
				str.append("where idEstoqueProduto = " + idEstoqueProduto + " ");
				str.append(" and idEmpresaFisica = " + idEmpresaFisica + " ");
				str.append(" and ano = " + ano + " and mes = " + mes + " ");				
				stmUpdate.executeUpdate(str.toString());
			}
		}
		
		Set<Integer> chaves = estoquesSemPreco.keySet();
		for (Integer chave : chaves) 
		{
			System.out.println(chave);
			cont++;
		}

		System.out.println(cont);
		System.out.println("Caso 1: " + contAchouCaso1);
		System.out.println("Caso 2: " + contAchouCaso2);
		System.out.println("Caso 3: " + contAchouCaso3);
		System.out.println("Caso 4: " + contAchouCaso4);
		
	}
	private void ajustaCodigosBarrasSemPreco() throws Exception
	{
		
		Connection con = null;
		Statement stmep = null;
		ResultSet rsep = null;	
		Statement stm = null;
		ResultSet rs = null;
		Statement stm2 = null;
		ResultSet rs2 = null;		
		Statement stm3 = null;
		ResultSet rs3 = null;		
		Statement stm4 = null;
		ResultSet rs4 = null;		
		Statement stmUpdate = null;
		ResultSet rsUpdate = null;		
		int mes = 5;
		int ano = 2014;
		
		String sql = "select idEmpresaFisica, idEstoqueProduto from SaldoEstoqueProduto where (venda = 0 or venda is null) " +
				"and ano = " + ano + " and mes = " + mes + "  and idEmpresaFisica <> 60 and idEmpresaFisica <> 6  order by idEstoqueProduto ";
		
		con = ConexaoUtil.getConexaoPadrao();
		stm = con.createStatement();
		rs = stm.executeQuery(sql);
		//tree
		
				
		int idEmpresaFisica;
		int idEstoqueProduto;
		int idProduto;
		double novoPreco;
		double novoCusto;
		String sql2 = "";
		String sql3 = "";
		String sql4 = "";
		int cont=0;
		int contAchouCaso1=0;
		int contAchouCaso2=0;
		int contAchouCaso3=0;
		int contAchouCaso4=0;
		HashMap<Integer, Integer> estoquesSemPreco = new HashMap<>();
		String empGrupo1 = "2,4,9";
		String empGrupo2 = "1,3,5,7,8,9,13";
		String meuGrupo = "";
		
		while (rs.next())
		{
			stmep = con.createStatement();
			idEmpresaFisica = rs.getInt("idEmpresaFisica");
			idEstoqueProduto = rs.getInt("idEstoqueProduto");			
			rsep = stmep.executeQuery("SELECT idProduto from EstoqueProduto where idEstoqueProduto = " + idEstoqueProduto);
			rsep.next();
			idProduto = rsep.getInt("idProduto");
			stmep.close();
			
			//acha o grupo equivalente
			if (empGrupo1.indexOf(idEmpresaFisica)>-1)
				meuGrupo = empGrupo1;
			else
				meuGrupo = empGrupo2;
			
			//tenta primeiro achar o preço do produto em outros Saldos do mesmo codigo de barras e empresa; Caso 1
			idEmpresaFisica = rs.getInt("idEmpresaFisica");
			idEstoqueProduto = rs.getInt("idEstoqueProduto");
			sql2 = "select max(venda) as novoPreco, max(custo) as custo, idEmpresaFisica, idEstoqueProduto from SaldoEstoqueProduto " + 
					"where ano = " + ano + " and mes = " + mes + " and idEmpresaFisica in (" + meuGrupo + ") " + 
					" and idEstoqueProduto = " + idEstoqueProduto + "";		
			stm2 = con.createStatement();
			rs2 = stm2.executeQuery(sql2);
			novoPreco=0;
			novoCusto=0;
			if (rs2.next())
			{
				novoPreco = rs2.getDouble("novoPreco");
				novoCusto = rs2.getDouble("custo");
				
			}
			if (novoPreco>0)
			{
				contAchouCaso1++;
			}
			else
			{
				//Tenta achar em qualquer outra empresa que tenha o mesmo codigo de estoque
				sql2 = "select max(venda) as novoPreco, max(custo) as custo, idEmpresaFisica, idEstoqueProduto from SaldoEstoqueProduto " + 
						"where ano = " + ano + " and mes = " + mes + " and idEstoqueProduto = " + idEstoqueProduto + " and idEmpresaFisica in (" + meuGrupo + ") ";
				
				stm2 = con.createStatement();
				rs2 = stm2.executeQuery(sql2);
				
				if (rs2.next())
				{
					novoPreco = rs2.getDouble("novoPreco");
					novoCusto = rs2.getDouble("custo");
				}
				if (novoPreco > 0)
				{
					contAchouCaso2++;
				}
				else
				{
					//tenta achar na saldo estoque produto mas em qualquer empresa de qualquer Saldo Estoque Produto que tenha o mesmo ID Produto
					sql3 = "select max(venda) as novoPreco, max(custo) as custo, idEmpresaFisica, estoqueproduto.idEstoqueProduto " +
							"from SaldoEstoqueProduto " +
							" inner join estoqueproduto on estoqueproduto.idEstoqueProduto = saldoestoqueproduto.idEstoqueProduto " +
							"where ano = " + ano + " and mes = " + mes + " and idProduto = " + idProduto +  " and idEmpresaFisica in (" + meuGrupo + ")";
					
					stm3 = con.createStatement();
					rs3 = stm3.executeQuery(sql3);
					
					if (rs3.next())
					{
						novoPreco = rs3.getDouble("novoPreco");
						novoCusto = rs3.getDouble("custo");
					}
					if (novoPreco > 0)
					{
						contAchouCaso3++;
					}
					else
					{
						//Apela para a TMPSaldoEstoqueProduto que possui todos os codigos do cache´referentes a mes 1, ano 2014 do CACHÉ
						sql4 = "select max(venda) as novoPreco, max(custo) as custo, idEmpresaFisica, tmpsaldoestoqueproduto.idEstoqueProduto " +
								"from TmpSaldoEstoqueProduto " +
								" inner join estoqueproduto on estoqueproduto.idEstoqueProduto = tmpsaldoestoqueproduto.idEstoqueProduto " +
								"where ano = " + 2014 + " and mes = " + 1 + " and idProduto = " + idProduto  +  " and idEmpresaFisica in (" + meuGrupo + ")";
						
						stm4 = con.createStatement();
						rs4 = stm4.executeQuery(sql4);
						
						if (rs4.next())
						{
							novoPreco = rs4.getDouble("novoPreco");
							novoCusto = rs4.getDouble("custo");
						}					
						if (novoPreco > 0)
						{
							contAchouCaso4++;
						}
						else
						{
							estoquesSemPreco.put(idEstoqueProduto, idEstoqueProduto);
							System.out.println("Preço não encontrado para o codigo de barras: " + idEstoqueProduto);
							
						}
					}
				}	
			}
			if (novoPreco>0)
			{
				stmUpdate = con.createStatement();
				StringBuilder str = new StringBuilder();
				str.append("UPDATE SaldoEstoqueProduto SET venda = " + novoPreco + "");  
				str.append("where idEstoqueProduto = " + idEstoqueProduto + " ");
				str.append(" and idEmpresaFisica = " + idEmpresaFisica + " ");
				str.append(" and ano = " + ano + " and mes = " + mes + " ");				
				stmUpdate.executeUpdate(str.toString());
			}
		}
		
		Set<Integer> chaves = estoquesSemPreco.keySet();
		for (Integer chave : chaves) 
		{
			System.out.println(chave);
			cont++;
		}

		System.out.println(cont);
		System.out.println("Caso 1: " + contAchouCaso1);
		System.out.println("Caso 2: " + contAchouCaso2);
		System.out.println("Caso 3: " + contAchouCaso3);
		System.out.println("Caso 4: " + contAchouCaso4);
		
	}
	
	public void ajustaMovimentacoesDuplicadas(Integer idEmpresa, Integer mesInicial, Integer mesFinal, int ano, Date dataFinal, OutputStream registroSaida, Connection con) throws Exception
	{
		Statement stmControles = null;
		ResultSet rsControles = null;
		String sqlControles;
		String strMeses="";
		int idEmpresaFisica;
		int quantidade;
		String numeroDocumento;
		Date dataMovimentacao;
		int idMovimentacao;
		String sqlDelecao;
		String sqlDelecaoItem;

		
		SimpleDateFormat sdfLog = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		if (mesInicial>mesFinal)
			throw new Exception("Mes Inicial não pode ser maior que mes final.");
		
		String strDataInicial = ano+"-"+(mesInicial>9?""+mesInicial:"0"+mesInicial)+"-01 00:00:00";
		String strDataFinal = sdf.format(dataFinal) + " 23:59:59";

		if (Integer.parseInt(strDataFinal.split("-")[1]) != mesFinal )
			throw new Exception("Data informada tem de estar dentro do mes final.");
		
		if (Integer.parseInt(strDataFinal.split("-")[0]) != ano)
			throw new Exception("Data informada tem de estar dentro do mes final.");
		
		for (int meuMes = mesInicial; meuMes<=mesFinal;meuMes++){
			strMeses = strMeses + ((strMeses.equals(""))?""+meuMes:","+meuMes);}
		
		try
		{
			//registroSaida.write(("INFO: " + sdf.format(new Date()) + " Iniciando ajuste de controles duplicados. \n").getBytes());
			if (con==null)
				throw new Exception("Conexão deve estar ativa.");
			
			sqlControles = "select idMovimentacaoEstoque, idEmpresaFisica, idEmpresaDestino, count(numeroDocumento) quantidade, numeroDocumento, " +
								"dataMovimentacao, idMovimentacaoEstoque " +  
								"FROM movimentacaoestoque where idEmpresaFisica = " + idEmpresa + " AND ";
			sqlControles += "dataMovimentacao >= '" + strDataInicial + "' AND dataMovimentacao <= '" + strDataFinal + "' and numeroDocumento not LIKE '%NE%' ";
			sqlControles += "group by idEmpresaFisica, numeroDocumento ";
			sqlControles += "having (count(numeroDocumento) > 1) ";
			sqlControles += "ORDER BY idMovimentacaoEstoque, dataMovimentacao ";
			//System.out.println(sqlControles);
			
			stmControles = con.createStatement();
			rsControles = stmControles.executeQuery(sqlControles);
			
			while (rsControles.next())
			{
				quantidade = rsControles.getInt("quantidade");
				numeroDocumento = rsControles.getString("numeroDocumento");
				dataMovimentacao = rsControles.getDate("dataMovimentacao");
				idMovimentacao = rsControles.getInt("idMovimentacaoEstoque");
				//registroSaida.write(("ERRO: " + "Movimentação duplicada: Id: " + idMovimentacao + ", Num. Documento: " + numeroDocumento + 
									//" Dt: " + sdf.format(dataMovimentacao) + " Qt. Duplicidades: " + quantidade +"\n").getBytes()); 
				
				sqlDelecaoItem = "DELETE FROM ItemMovimentacaoEstoque where idMovimentacaoEstoque = " + idMovimentacao+";";
				sqlDelecao = "DELETE FROM MovimentacaoEstoque where idMovimentacaoEstoque = " + idMovimentacao + " AND idEmpresaFisica = " + idEmpresa+";";
				//MovimentacaoEstoqueService movService = new MovimentacaoEstoqueService();
				//MovimentacaoEstoque criterio = new MovimentacaoEstoque();
				//criterio.setId(idMovimentacao);
				//movService.excluirMovimentacaoEstoque(criterio);
				//System.out.println("Movimentação duplicada apagada: Id: " + idMovimentacao + ", Num. Documento: " + numeroDocumento + 
				//					" Dt: " + sdf.format(dataMovimentacao) + " Qt. Duplicidades: " + quantidade);
				System.out.println(sqlDelecaoItem);
				System.out.println(sqlDelecao);
				
			}
			//registroSaida.write(("INFO: " + sdf.format(new Date()) + " Final da verificação de controles duplicados. \n\n").getBytes());
			
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
	
	
	public void ajustarSaldoMultiplasEmpresas(Integer[] empresas, int mes, int ano, Boolean corrigir, OutputStream registroSaida) throws Exception
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		AuditorInformacoesSistema audit = new AuditorInformacoesSistema();
		Calendar cal = Calendar.getInstance();

		String strData = "01" + "/" + (mes>9?""+mes:"0"+mes) + "/" +  ano;
		cal.setTime(sdf.parse(strData));
		strData = cal.getActualMaximum(Calendar.DAY_OF_MONTH) + "/" + (mes>9?""+mes:"0"+mes) + "/" +  ano;
		cal.setTime(sdf.parse(strData));
		
		for (int i=0;i<empresas.length;i++)
		{
			registroSaida.write(("Empresa " + empresas[i] + " Mes/Ano: " + mes+"/"+ano + " Corrigir: " + corrigir+"\n").getBytes());
			audit.reconstroiSaldoAteData(empresas[i], mes, mes, ano, cal.getTime(), registroSaida, corrigir);
			registroSaida.write(("Final ...\n\n").getBytes());
		}	
	}
	
	public void desconstroiSaldoMultiplasEmpresas(Integer[] empresas, int mes, int ano, Boolean corrigir, OutputStream registroSaida) throws Exception
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();

		String strData = "01" + "/" + (mes>9?""+mes:"0"+mes) + "/" +  ano;
		cal.setTime(sdf.parse(strData));
		strData = cal.getActualMaximum(Calendar.DAY_OF_MONTH) + "/" + (mes>9?""+mes:"0"+mes) + "/" +  ano;
		cal.setTime(sdf.parse(strData));
		
		int anoDoDestino = ano;
		int mesDoDestino = mes-1;
		if (mesDoDestino==0)
		{
			mesDoDestino=12;
			anoDoDestino = ano-1;
		}
		
		for (int i=0;i<empresas.length;i++)
		{
			registroSaida.write(("Empresa " + empresas[i] + " Mes/Ano: " + mes+"/"+ano + " Corrigir: " + corrigir+"\n").getBytes());
			desconstroiSaldo(empresas[i], mes, mesDoDestino, ano, anoDoDestino, cal.getTime(), registroSaida, corrigir);
			registroSaida.write(("Final ...\n\n").getBytes());
		}	
	}	
	
	public void desconstroiSaldo(Integer idEmpresa, Integer mesOrigem, Integer mesDestino, int anoOrigem, int anoDestino, Date dataBase, OutputStream registroSaida, boolean atualizar) throws Exception
	{
		Connection con = null;
		Statement stm = null;
		Statement stmSaldo = null;
		ResultSet rsSaldo = null;
		ResultSet rsEntradas = null;
		ResultSet rsSaidas = null;
		String sqlSaldo;
		String sqlEntradas;
		String sqlSaidas;
		int idEstoqueProduto;
		double saldoOrigem;
		double saldoDoDestino;
		double saldoRecalculado;
		double totalSaidas;
		double totalEntradas;
		SimpleDateFormat sdf;
		String strMeses = "";
		SimpleDateFormat sdfLog = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		long contReg=0;
		double totalSaldoDoDestino;
		double totalSaldoRecalculado;
		double totalSaldoOrigem;
		double totalQuantidadeEntrada;
		double totalQuantidadeSaida;
		
		if (mesOrigem<=mesDestino)
			throw new Exception("Mes origem tem de ser maior que mes de destino.");
		
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		String strDataInicial = anoOrigem+"-"+(mesOrigem>9?""+mesOrigem:"0"+mesOrigem)+"-01 00:00:00";
		String strDataFinal = sdf.format(dataBase) + " 23:59:59"; 

		if (Integer.parseInt(strDataFinal.split("-")[1]) != mesOrigem )
			throw new Exception("Data informada tem de estar dentro do mes final.");
		
		if (Integer.parseInt(strDataFinal.split("-")[0]) != anoOrigem)
			throw new Exception("Data informada tem de estar dentro do mes final.");
		
		for (int meuMes = mesOrigem; meuMes>mesDestino;meuMes--){
			strMeses = strMeses + ((strMeses.equals(""))?""+meuMes:","+meuMes);}
		
		if (atualizar)
			registroSaida.write(("INFO: " + sdfLog.format(new Date()) +" " + "Inicio da DESCONSTRUÇÃO de saldo. " + mesOrigem + " " + mesDestino + " " + anoDestino + " " + strDataInicial + " " + strDataFinal +  "\n").getBytes());
		else
			registroSaida.write(("INFO: " + sdfLog.format(new Date()) +" " + "Inicio da VALIDAÇÃO INVERSA de saldo. " + mesOrigem + " " + mesDestino + " " + anoDestino + " " + strDataInicial + " " + strDataFinal +  "\n").getBytes());
		
		try
		{
			totalSaldoDoDestino = 0;
			totalSaldoRecalculado = 0;
			totalSaldoOrigem = 0;
			totalQuantidadeSaida = 0;
			totalQuantidadeEntrada = 0;
			con = ConexaoUtil.getConexaoPadrao();
			stmSaldo = con.createStatement();
			sqlSaldo = "select pro.idProduto, ep.idEstoqueProduto, pro.nomeProduto,  custo, venda, saldo " +
					 "from SaldoEstoqueProduto sep " +
					 "LEFT OUTER JOIN EstoqueProduto ep ON ep.idEstoqueProduto = sep.idEstoqueProduto " +
					 "LEFT OUTER JOIN Produto pro on pro.idProduto = ep.idProduto " +
					 "WHERE idEmpresaFisica = " + idEmpresa + " and mes in (" + strMeses + ") AND ANO = " + anoOrigem +
					 " GROUP BY sep.idEmpresaFisica, sep.idEstoqueProduto " +
					 " ORDER BY sep.idEmpresaFisica, sep.idEstoqueProduto ";
					 
			
			log.log(Level.INFO, "SqlSaldo: " + sqlSaldo);
			
			rsSaldo = stmSaldo.executeQuery(sqlSaldo);
			stm = con.createStatement();
			while (rsSaldo.next())
			{
				idEstoqueProduto = rsSaldo.getInt("idEstoqueProduto");
				saldoOrigem = recuperaSaldoMesAno(idEmpresa, mesOrigem, anoOrigem, idEstoqueProduto, con);
				saldoDoDestino = recuperaSaldoMesAno(idEmpresa, mesDestino, anoDestino, idEstoqueProduto, con);
				totalSaldoDoDestino = totalSaldoDoDestino + saldoDoDestino;
				totalSaldoOrigem=totalSaldoOrigem+saldoOrigem;
				
				
				sqlSaidas =  "select idEstoqueProduto, idProduto, SUM(quantidade) quantidade, mov.entradaSaida " + 
							"from ItemMovimentacaoEstoque imov " + 
							"INNER JOIN MovimentacaoEstoque mov ON mov.idMovimentacaoEstoque = imov.idMovimentacaoEstoque " +
							"WHERE mov.idEmpresaFisica = " + idEmpresa + " and " + 
							"mov.dataMovimentacao >= '" + strDataInicial + "' and mov.dataMovimentacao <= '" + strDataFinal + "' " + 
							" AND imov.idEstoqueProduto = " + idEstoqueProduto + " AND mov.entradaSaida = 'S'";
				//log.log(Level.INFO, "SqlSaidas: " + sqlSaidas);
				
				rsSaidas = stm.executeQuery(sqlSaidas);
				if (rsSaidas.next())
					totalSaidas = rsSaidas.getDouble("quantidade");
				else
					totalSaidas = 0;
				rsSaidas.close();
				
				totalQuantidadeSaida=totalQuantidadeSaida+totalSaidas;
				
				sqlEntradas =  "select idEstoqueProduto, idProduto, SUM(quantidade) quantidade, mov.entradaSaida " + 
						"from ItemMovimentacaoEstoque imov " + 
						"INNER JOIN MovimentacaoEstoque mov ON mov.idMovimentacaoEstoque = imov.idMovimentacaoEstoque " +
						"WHERE mov.idEmpresaFisica = " + idEmpresa + " and " + 
						"mov.dataMovimentacao >= '" + strDataInicial + "' and mov.dataMovimentacao <= '" + strDataFinal + "' " +
						" AND imov.idEstoqueProduto = " + idEstoqueProduto + " AND mov.entradaSaida = 'E'";
				//log.log(Level.INFO, "SqlEntradas: " + sqlEntradas);
				
				rsEntradas = stm.executeQuery(sqlEntradas);
				if (rsEntradas.next())
					totalEntradas = rsEntradas.getDouble("quantidade");
				else
					totalEntradas = 0;
				rsEntradas.close();
				
				totalQuantidadeEntrada = totalQuantidadeEntrada + totalEntradas;
				
				saldoRecalculado = saldoOrigem - totalEntradas + totalSaidas;
				totalSaldoRecalculado = totalSaldoRecalculado + saldoRecalculado;
				
				//log.log(Level.INFO, "Resultado: " + idEstoqueProduto + " " + saldoMesAnterior + " " + totalEntradas + " " + totalSaidas + " " + saldoRecalculado +  " || " + saldoAtual);
				String registro = idEstoqueProduto + ";" + saldoOrigem + ";" + totalEntradas + ";" + totalSaidas + ";" + saldoRecalculado +  ";" + saldoDoDestino;
				if (saldoRecalculado!=saldoDoDestino){
					registroSaida.write(("ERRO: DIFERENÇA =>> " + registro+ "\n").getBytes());}

				if (atualizar==true && saldoRecalculado!=saldoDoDestino)
				{
					atualizaEstoque(idEmpresa, mesDestino, anoDestino, idEstoqueProduto, (double)saldoRecalculado, con);
					registroSaida.write(("INFO: " + registro+"\n").getBytes());		
				}
				
				contReg++;
				if (contReg % 5000==0)
				{
					log.log(Level.INFO, contReg + " Registros lidos.");
					registroSaida.write(("INFO: " + contReg + " Registros lidos.\n").getBytes());
				}
				
			}
			registroSaida.write(("INFO: " + totalSaldoDoDestino + " itens no estoque original.\n").getBytes());
			registroSaida.write(("INFO: " + totalSaldoRecalculado + " itens no estoque depois do recalculo.\n").getBytes());
			registroSaida.write(("INFO: " + (totalSaldoDoDestino-totalSaldoRecalculado) + " itens de diferença.\n").getBytes());
			registroSaida.write(("INFO: " + (totalSaldoOrigem) + " itens no saldo de origem do mes " + mesOrigem +".\n").getBytes());
			registroSaida.write(("INFO: " + (totalQuantidadeEntrada) + " itens de entrada.\n").getBytes());
			registroSaida.write(("INFO: " + (totalQuantidadeSaida) + " itens de saida.\n").getBytes());
			registroSaida.write(("INFO: " + contReg + " Registros lidos ao total.\n").getBytes());
			rsSaldo.close();
			stmSaldo.close();
			
			if (atualizar)
				registroSaida.write(("INFO: " + sdfLog.format(new Date()) +" " + "Final da DESCONSTRUÇÃO de saldo. " +contReg +" Registros lidos. \n\n").getBytes());
			else
				registroSaida.write(("INFO: " + sdfLog.format(new Date()) +" " + "Final da VALIDAÇÃO INVERSA de saldo. " +contReg +" Registros lidos. \n\n").getBytes());			
			
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
	
	private double recuperaSaldoMesAno(int idEmpresaFisica, int mes, int ano, int idEstoqueProduto, Connection con) throws Exception
	{
		Statement stm;
		ResultSet rsSaldo;
		double resultado;
		
		String sqlSaldo = "select pro.idProduto, ep.idEstoqueProduto, pro.nomeProduto,  custo, venda, saldo " +
				 "from SaldoEstoqueProduto sep " +
				 "INNER JOIN EstoqueProduto ep ON ep.idEstoqueProduto = sep.idEstoqueProduto " +
				 "INNER JOIN Produto pro on pro.idProduto = ep.idProduto " +
				 "WHERE idEmpresaFisica = " + idEmpresaFisica + " and mes = " + mes + " and sep.idEstoqueProduto = " + idEstoqueProduto + " AND ANO = " + ano +
				 " ORDER BY sep.idEmpresaFisica, sep.idEstoqueProduto ";
		
		stm = con.createStatement();
		rsSaldo = stm.executeQuery(sqlSaldo);
		
		
		
		if (rsSaldo.next())
			resultado =  rsSaldo.getDouble("saldo");
		else
			resultado = 0.0;
		
		rsSaldo.close();
		stm.close();
		
		return resultado;	
	}
	public void atualizaEstoque(Integer idEmpresaFisica, Integer mes, Integer ano, Integer idEstoqueProduto, double quantidade, Connection con) throws Exception
	{
		String sql = "UPDATE SaldoEstoqueProduto set saldo = " + quantidade + 
				" WHERE idEmpresaFisica = " + idEmpresaFisica + " and mes = " + mes + " and idEstoqueProduto = " + idEstoqueProduto + " AND ANO = " + ano;
		
		Statement stm = con.createStatement();
		stm.execute(sql);
		stm.close();
		
	}	
	
	public static void main(String[] args) throws Exception
	{
		OutputStream baos = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMddMHHmmss");
		/*
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		int[] empresas = {1,2,3,4,5,6,7,8,9,10,13};
		
		AjusteEstoqueService ajuste = new AjusteEstoqueService();
		AuditorInformacoesSistema audit = new AuditorInformacoesSistema();
		
		for (int i=0;i<empresas.length;i++)
		{
			
			//ajuste.ajustaMovimentacoesDuplicadas(empresas[i], 11, 12, 2015, sdf.parse("31/12/2015"), baos, ConexaoUtil.getConexaoPadrao());
			audit.reconstroiSaldoAteData(empresas[i], 11, 11, 2015, sdf.parse("30/11/2015"), baos, false);
		}*/
		
		
		if (args==null || args.length<6){
			System.out.println("Faltaram argumentos. Você deve informar as empresas que serão validadas/ajustadas, o mes, o ano e o tipo de saída.");}
		else if (args.length>6)
			System.out.println("Existem argumentos a mais sendo enviados. São somente 6 argumentos.");
		else
		{
			String strTipoValidacao = args[0];
			String strTipoSaida = args[1];
			String[] strEmpresas = args[2].split(";");
			Integer mes = Integer.parseInt(args[3]);
			Integer ano = Integer.parseInt(args[4]);
			Boolean corrigir = Boolean.parseBoolean(args[5]);
			
			Integer[] listaEmpresas = new Integer[strEmpresas.length];
			for (int i=0;i<strEmpresas.length;i++)
			{
				listaEmpresas[i]=Integer.parseInt(strEmpresas[i]);
			}
			//Array de Bytes
			if (strTipoSaida.equals("B"))
			{
				baos = new ByteArrayOutputStream();
			}
			else if (strTipoSaida.equals("F"))
			{
				baos = new FileOutputStream(System.getProperty("user.dir")+ "/VALIDA_ESTOQUE_"+sdf.format(new Date())+".txt",false);
			}
			
			else
			{
				baos = System.out;
			}
			if (strTipoValidacao.equals("SD"))
			{
				new AjusteEstoqueService().ajustarSaldoMultiplasEmpresas(listaEmpresas, mes, ano, corrigir, baos);
			}
			if (strTipoValidacao.equals("DS"))
			{
				new AjusteEstoqueService().desconstroiSaldoMultiplasEmpresas(listaEmpresas, mes, ano, corrigir, baos);
			}
			
			baos.flush();
			baos.close();
		}
	}
}
