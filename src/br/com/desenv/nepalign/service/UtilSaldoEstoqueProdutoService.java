package br.com.desenv.nepalign.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.frameworkignorante.ListaEmpresaEntidadeChave;
import br.com.desenv.nepalign.model.ItemMovimentacaoEstoque;
import br.com.desenv.nepalign.model.MovimentacaoEstoque;
import br.com.desenv.nepalign.model.TmpItemMovimentacaoEstoque;
import br.com.desenv.nepalign.model.TmpMovimentacaoEstoque;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class UtilSaldoEstoqueProdutoService 
{
	private void regerarSaldoBaseadoEmMovimentacao() throws Exception 
	{
		List<String> listaCodigoBarrasAjuste = new ArrayList<String>();
		listaCodigoBarrasAjuste.add("568");
		listaCodigoBarrasAjuste.add("569");
		listaCodigoBarrasAjuste.add("761");
		listaCodigoBarrasAjuste.add("865");
		listaCodigoBarrasAjuste.add("1129"); 
		listaCodigoBarrasAjuste.add("3454");
		listaCodigoBarrasAjuste.add("6159");
		listaCodigoBarrasAjuste.add("6971");
		listaCodigoBarrasAjuste.add("9365");
		listaCodigoBarrasAjuste.add("10213");
		listaCodigoBarrasAjuste.add("10214");
		listaCodigoBarrasAjuste.add("10794");
		listaCodigoBarrasAjuste.add("11046");
		listaCodigoBarrasAjuste.add("11048");
		listaCodigoBarrasAjuste.add("11111");
		listaCodigoBarrasAjuste.add("11594");
		listaCodigoBarrasAjuste.add("11696");
		listaCodigoBarrasAjuste.add("11697");
		listaCodigoBarrasAjuste.add("13461");
		listaCodigoBarrasAjuste.add("13593");
		listaCodigoBarrasAjuste.add("13991");
		listaCodigoBarrasAjuste.add("14568");
		listaCodigoBarrasAjuste.add("16272");
		listaCodigoBarrasAjuste.add("16275");
		listaCodigoBarrasAjuste.add("17506");
		listaCodigoBarrasAjuste.add("18840");
		listaCodigoBarrasAjuste.add("18841");
		listaCodigoBarrasAjuste.add("18847");
		listaCodigoBarrasAjuste.add("18936");
		listaCodigoBarrasAjuste.add("24290");
		listaCodigoBarrasAjuste.add("24294");
		listaCodigoBarrasAjuste.add("36082");
		listaCodigoBarrasAjuste.add("38744");
		listaCodigoBarrasAjuste.add("51547");
		listaCodigoBarrasAjuste.add("56072");
		listaCodigoBarrasAjuste.add("56073");
		listaCodigoBarrasAjuste.add("65519");
		listaCodigoBarrasAjuste.add("65520");
		listaCodigoBarrasAjuste.add("65591");
		listaCodigoBarrasAjuste.add("71419");
		listaCodigoBarrasAjuste.add("74916");
		listaCodigoBarrasAjuste.add("77408");
		listaCodigoBarrasAjuste.add("80397");
		listaCodigoBarrasAjuste.add("89931");
		listaCodigoBarrasAjuste.add("91006");
		listaCodigoBarrasAjuste.add("92122");
		listaCodigoBarrasAjuste.add("92124");
		listaCodigoBarrasAjuste.add("92128");
		listaCodigoBarrasAjuste.add("92139");
		listaCodigoBarrasAjuste.add("96744");
		listaCodigoBarrasAjuste.add("96844");
		listaCodigoBarrasAjuste.add("96869");
		listaCodigoBarrasAjuste.add("97291");
		listaCodigoBarrasAjuste.add("98488");
		listaCodigoBarrasAjuste.add("98492");
		listaCodigoBarrasAjuste.add("98494");
		listaCodigoBarrasAjuste.add("98496");
		listaCodigoBarrasAjuste.add("99369");
		listaCodigoBarrasAjuste.add("99370");
		listaCodigoBarrasAjuste.add("99371");
		listaCodigoBarrasAjuste.add("99372");
		listaCodigoBarrasAjuste.add("99373");
		listaCodigoBarrasAjuste.add("99796");
		listaCodigoBarrasAjuste.add("99999");
		listaCodigoBarrasAjuste.add("100088");
		listaCodigoBarrasAjuste.add("101989");
		listaCodigoBarrasAjuste.add("103864");
		listaCodigoBarrasAjuste.add("104129");
		listaCodigoBarrasAjuste.add("104140");
		listaCodigoBarrasAjuste.add("104279");
		listaCodigoBarrasAjuste.add("104280");
		listaCodigoBarrasAjuste.add("104282");
		listaCodigoBarrasAjuste.add("104283");
		listaCodigoBarrasAjuste.add("104284");
		listaCodigoBarrasAjuste.add("104287");
		listaCodigoBarrasAjuste.add("104521");
		listaCodigoBarrasAjuste.add("105734");
		listaCodigoBarrasAjuste.add("105735");
		listaCodigoBarrasAjuste.add("105736");
		listaCodigoBarrasAjuste.add("108605");
		listaCodigoBarrasAjuste.add("113051");
		listaCodigoBarrasAjuste.add("116042");
		listaCodigoBarrasAjuste.add("116043");
		listaCodigoBarrasAjuste.add("116044");
		listaCodigoBarrasAjuste.add("116045");
		listaCodigoBarrasAjuste.add("116046");
		listaCodigoBarrasAjuste.add("116047");
		listaCodigoBarrasAjuste.add("116048");
		listaCodigoBarrasAjuste.add("117195");
		listaCodigoBarrasAjuste.add("117196");
		listaCodigoBarrasAjuste.add("117197");
		listaCodigoBarrasAjuste.add("117198");
		listaCodigoBarrasAjuste.add("117199");
		listaCodigoBarrasAjuste.add("117200");
		listaCodigoBarrasAjuste.add("117390");
		listaCodigoBarrasAjuste.add("117886");
		listaCodigoBarrasAjuste.add("117944");
		listaCodigoBarrasAjuste.add("117945");
		listaCodigoBarrasAjuste.add("118805");
		listaCodigoBarrasAjuste.add("119929");
		listaCodigoBarrasAjuste.add("120344");
		listaCodigoBarrasAjuste.add("120345");
		listaCodigoBarrasAjuste.add("120346");
		listaCodigoBarrasAjuste.add("120348");
		listaCodigoBarrasAjuste.add("120349");
		listaCodigoBarrasAjuste.add("120350");
		listaCodigoBarrasAjuste.add("125724");
		listaCodigoBarrasAjuste.add("125768");
		listaCodigoBarrasAjuste.add("125769");
		listaCodigoBarrasAjuste.add("126203");
		listaCodigoBarrasAjuste.add("128567");
		listaCodigoBarrasAjuste.add("132554");
		listaCodigoBarrasAjuste.add("132555");
		listaCodigoBarrasAjuste.add("133510");
		listaCodigoBarrasAjuste.add("133923");
		listaCodigoBarrasAjuste.add("134083");
		listaCodigoBarrasAjuste.add("134109");
		listaCodigoBarrasAjuste.add("134501");
		listaCodigoBarrasAjuste.add("134502");
		listaCodigoBarrasAjuste.add("134503");
		listaCodigoBarrasAjuste.add("134504");
		listaCodigoBarrasAjuste.add("134505");
		listaCodigoBarrasAjuste.add("134518");
		listaCodigoBarrasAjuste.add("134550");
		listaCodigoBarrasAjuste.add("134551");
		listaCodigoBarrasAjuste.add("134552");
		listaCodigoBarrasAjuste.add("134553");
		listaCodigoBarrasAjuste.add("134554");
		listaCodigoBarrasAjuste.add("134555");
		listaCodigoBarrasAjuste.add("134556");
		listaCodigoBarrasAjuste.add("134557");
		listaCodigoBarrasAjuste.add("134558");
		listaCodigoBarrasAjuste.add("134559");
		listaCodigoBarrasAjuste.add("134560");
		listaCodigoBarrasAjuste.add("134561");
		listaCodigoBarrasAjuste.add("134562");
		listaCodigoBarrasAjuste.add("134563");
		listaCodigoBarrasAjuste.add("134564");
		listaCodigoBarrasAjuste.add("134565");
		listaCodigoBarrasAjuste.add("134566");
		listaCodigoBarrasAjuste.add("134567");
		listaCodigoBarrasAjuste.add("134568");
		listaCodigoBarrasAjuste.add("134569");
		listaCodigoBarrasAjuste.add("134570");
		listaCodigoBarrasAjuste.add("134571");
		listaCodigoBarrasAjuste.add("134572");
		listaCodigoBarrasAjuste.add("134573");
		listaCodigoBarrasAjuste.add("134574");
		listaCodigoBarrasAjuste.add("134575");
		listaCodigoBarrasAjuste.add("134602");
		listaCodigoBarrasAjuste.add("134604");
		listaCodigoBarrasAjuste.add("134781");
		listaCodigoBarrasAjuste.add("134782");
		listaCodigoBarrasAjuste.add("135172");
		listaCodigoBarrasAjuste.add("135375");
		listaCodigoBarrasAjuste.add("135376");
		listaCodigoBarrasAjuste.add("135432");
		listaCodigoBarrasAjuste.add("135524");
		listaCodigoBarrasAjuste.add("135525");
		listaCodigoBarrasAjuste.add("135526");
		listaCodigoBarrasAjuste.add("135527");
		listaCodigoBarrasAjuste.add("135528");
		listaCodigoBarrasAjuste.add("135529");
		listaCodigoBarrasAjuste.add("136017");
		listaCodigoBarrasAjuste.add("137214");
		listaCodigoBarrasAjuste.add("137232");
		listaCodigoBarrasAjuste.add("138384");
		listaCodigoBarrasAjuste.add("138873");
		listaCodigoBarrasAjuste.add("138874");
		listaCodigoBarrasAjuste.add("139398");
		listaCodigoBarrasAjuste.add("139399");
		listaCodigoBarrasAjuste.add("139791");
		listaCodigoBarrasAjuste.add("139792");
		listaCodigoBarrasAjuste.add("139901");
		listaCodigoBarrasAjuste.add("139904");
		listaCodigoBarrasAjuste.add("139905");
		listaCodigoBarrasAjuste.add("139934");
		listaCodigoBarrasAjuste.add("139935");
		listaCodigoBarrasAjuste.add("139936");
		listaCodigoBarrasAjuste.add("139946");
		listaCodigoBarrasAjuste.add("139947");
		listaCodigoBarrasAjuste.add("139996");
		listaCodigoBarrasAjuste.add("139997");
		listaCodigoBarrasAjuste.add("140079");
		listaCodigoBarrasAjuste.add("140536");
		listaCodigoBarrasAjuste.add("140537");
		listaCodigoBarrasAjuste.add("140566");
		listaCodigoBarrasAjuste.add("140567");
		listaCodigoBarrasAjuste.add("141615");
		listaCodigoBarrasAjuste.add("141645");
		listaCodigoBarrasAjuste.add("143279");
		listaCodigoBarrasAjuste.add("143280");
		listaCodigoBarrasAjuste.add("143281");
		listaCodigoBarrasAjuste.add("143282");
		listaCodigoBarrasAjuste.add("143441");
		listaCodigoBarrasAjuste.add("143465");
		listaCodigoBarrasAjuste.add("143466");
		listaCodigoBarrasAjuste.add("143487");
		listaCodigoBarrasAjuste.add("143986");
		listaCodigoBarrasAjuste.add("144615");
		listaCodigoBarrasAjuste.add("145237");
		listaCodigoBarrasAjuste.add("145357");
		listaCodigoBarrasAjuste.add("145358");
		listaCodigoBarrasAjuste.add("145359");
		listaCodigoBarrasAjuste.add("145360");
		listaCodigoBarrasAjuste.add("145361");
		listaCodigoBarrasAjuste.add("145368");
		listaCodigoBarrasAjuste.add("145749");
		listaCodigoBarrasAjuste.add("145750");
		listaCodigoBarrasAjuste.add("145751");
		listaCodigoBarrasAjuste.add("145752");
		listaCodigoBarrasAjuste.add("145753");
		listaCodigoBarrasAjuste.add("145883");
		listaCodigoBarrasAjuste.add("145886");
		listaCodigoBarrasAjuste.add("145937");
		listaCodigoBarrasAjuste.add("145938");
		listaCodigoBarrasAjuste.add("147363");
		listaCodigoBarrasAjuste.add("149357");
		listaCodigoBarrasAjuste.add("149548");
		listaCodigoBarrasAjuste.add("149549");
		listaCodigoBarrasAjuste.add("150754");
		listaCodigoBarrasAjuste.add("150755");
		listaCodigoBarrasAjuste.add("150896");
		listaCodigoBarrasAjuste.add("150897");
		listaCodigoBarrasAjuste.add("151065");
		listaCodigoBarrasAjuste.add("152385");
		listaCodigoBarrasAjuste.add("152386");
		listaCodigoBarrasAjuste.add("152414");
		listaCodigoBarrasAjuste.add("152895");
		listaCodigoBarrasAjuste.add("152896");
		listaCodigoBarrasAjuste.add("152897");
		listaCodigoBarrasAjuste.add("152898");
		listaCodigoBarrasAjuste.add("152899");
		listaCodigoBarrasAjuste.add("152900");
		listaCodigoBarrasAjuste.add("152939");
		listaCodigoBarrasAjuste.add("152948");
		listaCodigoBarrasAjuste.add("152949");
		listaCodigoBarrasAjuste.add("152950");
		listaCodigoBarrasAjuste.add("153184");
		listaCodigoBarrasAjuste.add("153708");
		listaCodigoBarrasAjuste.add("157816");
		listaCodigoBarrasAjuste.add("157817");
		listaCodigoBarrasAjuste.add("157818");
		listaCodigoBarrasAjuste.add("157819");
		listaCodigoBarrasAjuste.add("157820");
		listaCodigoBarrasAjuste.add("157821");
		listaCodigoBarrasAjuste.add("157822");
		listaCodigoBarrasAjuste.add("157823");
		listaCodigoBarrasAjuste.add("157824");
		listaCodigoBarrasAjuste.add("157825");
		listaCodigoBarrasAjuste.add("157826");
		listaCodigoBarrasAjuste.add("157888");
		listaCodigoBarrasAjuste.add("158183");
		listaCodigoBarrasAjuste.add("158234");
		listaCodigoBarrasAjuste.add("158595");
		listaCodigoBarrasAjuste.add("161469");
		listaCodigoBarrasAjuste.add("163201");
		listaCodigoBarrasAjuste.add("163202");
		listaCodigoBarrasAjuste.add("163203");
		listaCodigoBarrasAjuste.add("163204");
		listaCodigoBarrasAjuste.add("163205");
		listaCodigoBarrasAjuste.add("163284");
		listaCodigoBarrasAjuste.add("163285");
		listaCodigoBarrasAjuste.add("163943");
		listaCodigoBarrasAjuste.add("163944");
		listaCodigoBarrasAjuste.add("163945");
		listaCodigoBarrasAjuste.add("163946");
		listaCodigoBarrasAjuste.add("163947");
		listaCodigoBarrasAjuste.add("171874");
		listaCodigoBarrasAjuste.add("172634");
		listaCodigoBarrasAjuste.add("173121");
		listaCodigoBarrasAjuste.add("173775");
		listaCodigoBarrasAjuste.add("173776");
		listaCodigoBarrasAjuste.add("173777");
		listaCodigoBarrasAjuste.add("173778");
		listaCodigoBarrasAjuste.add("174866");
		listaCodigoBarrasAjuste.add("174921");
		listaCodigoBarrasAjuste.add("174922");
		listaCodigoBarrasAjuste.add("175011");
		listaCodigoBarrasAjuste.add("175012");
		listaCodigoBarrasAjuste.add("175453");
		listaCodigoBarrasAjuste.add("176095");
		listaCodigoBarrasAjuste.add("176096");
		listaCodigoBarrasAjuste.add("176292");
		listaCodigoBarrasAjuste.add("176293");
		listaCodigoBarrasAjuste.add("176294");
		listaCodigoBarrasAjuste.add("178685");
		listaCodigoBarrasAjuste.add("178686");
		listaCodigoBarrasAjuste.add("178687");
		listaCodigoBarrasAjuste.add("178904");
		listaCodigoBarrasAjuste.add("178905");
		listaCodigoBarrasAjuste.add("178906");
		listaCodigoBarrasAjuste.add("178907");
		listaCodigoBarrasAjuste.add("178908");
		listaCodigoBarrasAjuste.add("178909");
		listaCodigoBarrasAjuste.add("178910");
		listaCodigoBarrasAjuste.add("178911");
		listaCodigoBarrasAjuste.add("178912");
		listaCodigoBarrasAjuste.add("178913");
		listaCodigoBarrasAjuste.add("178914");
		listaCodigoBarrasAjuste.add("178915");
		listaCodigoBarrasAjuste.add("178916");
		listaCodigoBarrasAjuste.add("178917");
		listaCodigoBarrasAjuste.add("178918");
		listaCodigoBarrasAjuste.add("178919");
		listaCodigoBarrasAjuste.add("178920");
		listaCodigoBarrasAjuste.add("178921");
		listaCodigoBarrasAjuste.add("178922");
		listaCodigoBarrasAjuste.add("178923");
		listaCodigoBarrasAjuste.add("178924");
		listaCodigoBarrasAjuste.add("178925");
		listaCodigoBarrasAjuste.add("179013");
		listaCodigoBarrasAjuste.add("179047");
		listaCodigoBarrasAjuste.add("179438");
		listaCodigoBarrasAjuste.add("179439");
		listaCodigoBarrasAjuste.add("179440");
		listaCodigoBarrasAjuste.add("179459");
		listaCodigoBarrasAjuste.add("179460");
		listaCodigoBarrasAjuste.add("179576");
		listaCodigoBarrasAjuste.add("180027");
		listaCodigoBarrasAjuste.add("180086");
		listaCodigoBarrasAjuste.add("180092");
		listaCodigoBarrasAjuste.add("180540");
		listaCodigoBarrasAjuste.add("180541");
		listaCodigoBarrasAjuste.add("180542");
		listaCodigoBarrasAjuste.add("180543");
		listaCodigoBarrasAjuste.add("180544");
		listaCodigoBarrasAjuste.add("181822");
		listaCodigoBarrasAjuste.add("182983");
		listaCodigoBarrasAjuste.add("182984");
		listaCodigoBarrasAjuste.add("183543");
		listaCodigoBarrasAjuste.add("183544");
		listaCodigoBarrasAjuste.add("183545");
		listaCodigoBarrasAjuste.add("183546");
		listaCodigoBarrasAjuste.add("183547");
		listaCodigoBarrasAjuste.add("183548");
		listaCodigoBarrasAjuste.add("183549");
		listaCodigoBarrasAjuste.add("183550");
		listaCodigoBarrasAjuste.add("183551");
		listaCodigoBarrasAjuste.add("183552");
		listaCodigoBarrasAjuste.add("183553");
		listaCodigoBarrasAjuste.add("186165");
		listaCodigoBarrasAjuste.add("186816");
		listaCodigoBarrasAjuste.add("186817");
		listaCodigoBarrasAjuste.add("189277");
		listaCodigoBarrasAjuste.add("189515");
		listaCodigoBarrasAjuste.add("189678");
		listaCodigoBarrasAjuste.add("189679");
		listaCodigoBarrasAjuste.add("189680");
		listaCodigoBarrasAjuste.add("189681");
		listaCodigoBarrasAjuste.add("190478");
		listaCodigoBarrasAjuste.add("190479");
		listaCodigoBarrasAjuste.add("190480");
		
		List<Integer> listaEmpresasAjuste = new ArrayList<Integer>();
		listaEmpresasAjuste.add(1);
		listaEmpresasAjuste.add(2);
		listaEmpresasAjuste.add(3);
		listaEmpresasAjuste.add(4);
		listaEmpresasAjuste.add(5);
		listaEmpresasAjuste.add(7);
		listaEmpresasAjuste.add(8);
		listaEmpresasAjuste.add(9);
		listaEmpresasAjuste.add(13);
		
		Connection conn = ConexaoUtil.getConexaoPadrao();
		Statement st = conn.createStatement();
		
		for(Integer idEmpresaFisica : listaEmpresasAjuste)
		{
			HashMap<String, Integer> quantidadePorCodigo = new HashMap<String, Integer>(); 
			 
			List<MovimentacaoEstoque> listaMovimentacaoEstoque = new MovimentacaoEstoqueService().listarPaginadoPorObjetoFiltro(Integer.MAX_VALUE, 1, new MovimentacaoEstoque(), null, null, " dataMovimentacao > '2014-10-31 23:59:59' and dataMovimentacao <= '2014-11-19 23:59:59' and empresaFisica.id = " + idEmpresaFisica.toString()).getRecordList();
			
			System.out.println("Encontradas " + listaMovimentacaoEstoque.size() + " Movimentações ");
			 
			for (MovimentacaoEstoque movimentacaoEstoque : listaMovimentacaoEstoque)
			{
				String query = "select idestoqueproduto as codigobarras, quantidade from itemmovimentacaoestoque where idmovimentacaoestoque = " + movimentacaoEstoque.getId() + ";";
				 
				ResultSet rs = st.executeQuery(query);
				
				while(rs.next())  
				{
					String codigoBarras = rs.getString(1);
					 Double quantidade = rs.getDouble(2);
					 
					if(new IgnUtil().contains(codigoBarras, listaCodigoBarrasAjuste)) 
					{ 
						int quantidadeSaldoCodigo = 0;
						 
						try
						{
							quantidadeSaldoCodigo = quantidadePorCodigo.get(codigoBarras); 
						} 
						catch(NullPointerException tryExc)
						{
							quantidadeSaldoCodigo = 0;
						}
						
						if(movimentacaoEstoque.getEntradaSaida().equals("E"))
							quantidadeSaldoCodigo += quantidade;
						else
							quantidadeSaldoCodigo -= quantidade;
						
						quantidadePorCodigo.remove(codigoBarras);
						quantidadePorCodigo.put(codigoBarras, quantidadeSaldoCodigo);		
					}
				}
			}
			
			for(String codigoBarras : listaCodigoBarrasAjuste)
			{
				String sql = "select coalesce(saldo,0), coalesce(custo, 0) from saldoestoqueproduto where idestoqueproduto = " + codigoBarras + " and idempresafisica = " + idEmpresaFisica.toString() + " and mes = 10 and ano = 2014;";
				
				ResultSet rs = st.executeQuery(sql);
				
				Double saldoContabilizado = 0.0;
				
				try
				{
					saldoContabilizado = Double.parseDouble(quantidadePorCodigo.get(codigoBarras).toString());		
				}
				catch(NullPointerException npe)
				{
					saldoContabilizado = 0.0;
				}
			
				Double saldoMesAnterior = 0.0;

				if(rs.next()) 
					saldoMesAnterior = rs.getDouble(1);	
			
				Double SaldoFinal = saldoMesAnterior + saldoContabilizado;
				
				System.out.println("update saldoestoqueproduto set saldo = " + SaldoFinal.toString() + " where idestoqueproduto = '" + codigoBarras + "' and idempresafisica = " + idEmpresaFisica.toString() + " and mes = 11 and ano = 2014;");			
			} 	
		}
	}
		 
	public void regerarSaldoBaseadoEmMovimentacaoTmp() throws Exception
	{
		Logger.getGlobal().info("Carregando chaves primárias das entidades.");
		ListaEmpresaEntidadeChave.getInstance();  

		Logger.getGlobal().info("Inicializando banco de dados.");
		
		Connection conn = null;
		EntityManager manager = null;  
		EntityTransaction transaction = null;
		
		try
		{
			manager = ConexaoUtil.getEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			
			conn = ConexaoUtil.getConexaoPadrao();
			
			Logger.getGlobal().info("Conectado! Transação ativa.");
		}
		catch(Exception conException)
		{
			throw new Exception("não foi possível iniciar a conexão com o banco de dados!");
		}

		Logger.getGlobal().info("Iniciando Services.");
		UsuarioService usuarioService = new UsuarioService();
		EstoqueService estoqueService = new EstoqueService();
		ClienteService clienteservice = new ClienteService();
		ProdutoService produtoService = new ProdutoService();
		FornecedorService fornecedorService = new FornecedorService();
		EmpresaFisicaService empresaFisicaService = new EmpresaFisicaService();
		UnidadeMedidaService unidadeMedidaService = new UnidadeMedidaService();
		EstoqueProdutoService estoqueProdutoService = new EstoqueProdutoService();
		MovimentacaoEstoqueService movimentacaoEstoqueService = new MovimentacaoEstoqueService();
		ItemMovimentacaoEstoqueService itemMovimentacaoEstoqueService = new ItemMovimentacaoEstoqueService();
		TipoMovimentacaoEstoqueService tipoMovimentacaoEstoqueService = new TipoMovimentacaoEstoqueService();
		StatusMovimentacaoEstoqueService statusMovimentacaoEstoqueService = new StatusMovimentacaoEstoqueService();
		TipoDocumentoMovimentoEstoqueService tipoDocumentoMovimentoEstoqueService = new TipoDocumentoMovimentoEstoqueService();
		
		try
		{
			if(DsvConstante.getParametrosSistema().get("EMPRESAS_INTEGRACAO_ESTOQUE") == null && DsvConstante.getParametrosSistema().get("EMPRESAS_INTEGRACAO_ESTOQUE").isEmpty())
				throw new Exception("parâmetro EMPRESAS_INTEGRACAO_ESTOQUE não encontrado! Processo abortado.");
			
			String[] empresasIntegracao = DsvConstante.getParametrosSistema().get("EMPRESAS_INTEGRACAO_ESTOQUE").split(",");
			
			Logger.getGlobal().info("Empresas informadas para a integração " + DsvConstante.getParametrosSistema().get("EMPRESAS_INTEGRACAO_ESTOQUE"));

			for(String empresa : empresasIntegracao)
			{
				String numeroEmpresaFisica = String.format ("%02d", Integer.parseInt(empresa));
				String databaseName = "nepalignloja" + numeroEmpresaFisica;
				
				Logger.getGlobal().info("Iniciando processo de integração para a empresa " + numeroEmpresaFisica + " no banco de dados " + databaseName);

				String tmpQuery = "SELECT * FROM " + databaseName + ".tmpMovimentacaoEstoque;";

				ResultSet rsTmpMovimentacaoEstoque = conn.createStatement().executeQuery(tmpQuery);
				
				if(!rsTmpMovimentacaoEstoque.next())
					throw new Exception("não existem movimentações a serem integradas!");
				
				rsTmpMovimentacaoEstoque.last();
				
				Logger.getGlobal().info("Encontradas " + rsTmpMovimentacaoEstoque.getRow() + " movimentações!");
				
				rsTmpMovimentacaoEstoque.first();

				while(rsTmpMovimentacaoEstoque.next())
				{
					MovimentacaoEstoque movimentacaoEstoque = new MovimentacaoEstoque();
					movimentacaoEstoque.setCliente(clienteservice.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idCliente")));
					movimentacaoEstoque.setDataMovimentacao(rsTmpMovimentacaoEstoque.getDate("dataMovimentacao"));
					movimentacaoEstoque.setEmpresaDestino(empresaFisicaService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idEmpresaDestino")));
					movimentacaoEstoque.setEmpresaFisica(empresaFisicaService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idEmpresaFisica")));
					movimentacaoEstoque.setEntradaSaida(rsTmpMovimentacaoEstoque.getString("entradaSaida"));
					movimentacaoEstoque.setEstoque(estoqueService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idEstoque")));
					movimentacaoEstoque.setFaturado(rsTmpMovimentacaoEstoque.getString("faturado"));
					movimentacaoEstoque.setForncedor(fornecedorService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idFornecedor")));
					movimentacaoEstoque.setIdDocumentoOrigem(rsTmpMovimentacaoEstoque.getInt("idDocumentoOrigem"));
					movimentacaoEstoque.setManual(rsTmpMovimentacaoEstoque.getString("manual"));
					movimentacaoEstoque.setNumeroDocumento(rsTmpMovimentacaoEstoque.getString("numeroDocumento"));
					movimentacaoEstoque.setNumeroVolume(rsTmpMovimentacaoEstoque.getInt("numeroVolume"));
					movimentacaoEstoque.setObservacao(rsTmpMovimentacaoEstoque.getString("observacao"));
					movimentacaoEstoque.setPeso(rsTmpMovimentacaoEstoque.getDouble("peso"));
					movimentacaoEstoque.setStatusMovimentacaoEstoque(statusMovimentacaoEstoqueService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idStatusMovimentacaoEstoque")));
					movimentacaoEstoque.setTipoDocumentoMovimento(tipoDocumentoMovimentoEstoqueService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idTipoDocumentoMovimentoEstoque")));
					movimentacaoEstoque.setTipoMovimentacaoEstoque(tipoMovimentacaoEstoqueService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idTipoMovimentacaoEstoque")));
					movimentacaoEstoque.setUsuario(usuarioService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idUsuario")));
					movimentacaoEstoque.setValorFrete(rsTmpMovimentacaoEstoque.getDouble("valorFrete"));
					
					Logger.getGlobal().info("Persistindo movimentação N. " + movimentacaoEstoque.getNumeroDocumento() + " do Tipo " + movimentacaoEstoque.getTipoMovimentacaoEstoque().getDescricao());

					movimentacaoEstoqueService.salvar(manager, transaction, movimentacaoEstoque, false);

					List<ItemMovimentacaoEstoque> listaItemMovimentacaoEstoque = new ArrayList<ItemMovimentacaoEstoque>();

					String tmpItemQuery = "SELECT * FROM " + databaseName + ".tmpItemMovimentacaoEstoque WHERE idTmpMovimentacaoEstoque = " + rsTmpMovimentacaoEstoque.getInt("idTmpMovimentacaoEstoque") + ";";

					ResultSet rsTmpItemMovimentacaoEstoque = conn.createStatement().executeQuery(tmpItemQuery);

					while(rsTmpItemMovimentacaoEstoque.next())
					{
						ItemMovimentacaoEstoque itemMovimentacaoEstoque = new ItemMovimentacaoEstoque();
						itemMovimentacaoEstoque.setCusto(rsTmpItemMovimentacaoEstoque.getDouble("custo"));
						itemMovimentacaoEstoque.setDataFabricacao(rsTmpItemMovimentacaoEstoque.getDate("dataFabricacao"));
						itemMovimentacaoEstoque.setDataValidade(rsTmpItemMovimentacaoEstoque.getDate("dataValidade"));
						itemMovimentacaoEstoque.setDocumentoOrigem(rsTmpItemMovimentacaoEstoque.getString("documentoOrigem"));
						itemMovimentacaoEstoque.setEstoqueProduto(estoqueProdutoService.recuperarPorId(manager, rsTmpItemMovimentacaoEstoque.getInt("idEstoqueProduto")));
						itemMovimentacaoEstoque.setMovimentacaoEstoque(movimentacaoEstoque);
						itemMovimentacaoEstoque.setNumeroLote(rsTmpItemMovimentacaoEstoque.getString("numeroLote"));
						itemMovimentacaoEstoque.setProduto(produtoService.recuperarPorId(manager, rsTmpItemMovimentacaoEstoque.getInt("idProduto")));
						itemMovimentacaoEstoque.setQuantidade(rsTmpItemMovimentacaoEstoque.getDouble("quantidade"));
						itemMovimentacaoEstoque.setSequencialItem(rsTmpItemMovimentacaoEstoque.getInt("sequencialItem"));
						itemMovimentacaoEstoque.setUnidadeMedida(unidadeMedidaService.recuperarPorId(manager, rsTmpItemMovimentacaoEstoque.getInt("idUnidadeMedida")));
						itemMovimentacaoEstoque.setValor(rsTmpItemMovimentacaoEstoque.getDouble("valor"));
						itemMovimentacaoEstoque.setVenda(rsTmpItemMovimentacaoEstoque.getDouble("venda"));

						Logger.getGlobal().info("Adicionado novo item com código de barras " + itemMovimentacaoEstoque.getEstoqueProduto().getCodigoBarras() + " com Quantidade " + itemMovimentacaoEstoque.getQuantidade());
						listaItemMovimentacaoEstoque.add(itemMovimentacaoEstoque);
					}
					Logger.getGlobal().info("Total de " + listaItemMovimentacaoEstoque.size() + " items! Persistindo.");
					itemMovimentacaoEstoqueService.salvarItems(listaItemMovimentacaoEstoque, false, manager, transaction);
					Logger.getGlobal().info("Movimentação salva com sucesso!");
				}
			}

			Logger.getGlobal().info("Movimentações integradas com sucesso!");
			Logger.getGlobal().info("Finalizando Transação.");
			transaction.commit();
			Logger.getGlobal().info("Transação finalizada. Dados salvos!");
			
			Logger.getGlobal().info("Fechando conexão com o banco de dados e finalizando os Services.");
			conn.close();
			manager.close();
			
			usuarioService = null;
			estoqueService = null;
			clienteservice = null;
			produtoService = null;
			fornecedorService = null;
			empresaFisicaService = null;
			unidadeMedidaService = null;
			estoqueProdutoService = null;
			movimentacaoEstoqueService = null;
			itemMovimentacaoEstoqueService = null;
			tipoMovimentacaoEstoqueService = null;
			statusMovimentacaoEstoqueService = null;
			tipoDocumentoMovimentoEstoqueService = null;
			Logger.getGlobal().info("Finalizado. Processo Concluído!");
		}
		catch(Exception ex)
		{
			if (transaction!=null && transaction.isActive())
				transaction.rollback();
			
			ex.printStackTrace();
			throw ex;
						
		}
	}
	
	public static void main(String[] args) throws Exception 
	{ 
		UtilSaldoEstoqueProdutoService util = new UtilSaldoEstoqueProdutoService();
		util.regerarSaldoBaseadoEmMovimentacaoTmp(); 
	} 
}  
