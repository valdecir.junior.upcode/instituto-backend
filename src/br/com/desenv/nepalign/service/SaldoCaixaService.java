package br.com.desenv.nepalign.service;

import java.util.Date;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.Caixa;
import br.com.desenv.nepalign.model.LogOperacao;
import br.com.desenv.nepalign.model.SaldoCaixa;
import br.com.desenv.nepalign.persistence.SaldoCaixaPersistence;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class SaldoCaixaService extends GenericServiceIGN<SaldoCaixa, SaldoCaixaPersistence>
{
	private static final UsuarioService usuarioService = new UsuarioService();
	private static final LogOperacaoService logOperacaoService = new LogOperacaoService();
	
	public SaldoCaixaService() { super(); }

	public SaldoCaixa recuperarSaldoCaixaAtual(EntityManager manager, Caixa caixa) throws Exception
	{
		return ((SaldoCaixaPersistence) getPersistence()).recuperarSaldoCaixaAtual(manager, caixa);
	}
	
	public SaldoCaixa recuperarSaldoCaixaAtual(Caixa caixa) throws Exception
	{
		return ((SaldoCaixaPersistence) getPersistence()).recuperarSaldoCaixaAtual(null, caixa);
	}
	
	public boolean atualizarSaldoCaixa(EntityManager manager, Caixa caixa, Double valor) throws Exception
	{
		gravaOperacaoSaldo(manager, caixa, valor);
		return ((SaldoCaixaPersistence) getPersistence()).atualizarSaldoCaixa(manager, caixa, valor);
	}
	
	public boolean atualizarSaldoCaixa(Caixa caixa, Double valor) throws Exception
	{
		gravaOperacaoSaldo(null, caixa, valor);
		return ((SaldoCaixaPersistence) getPersistence()).atualizarSaldoCaixa(null, caixa, valor);
	}
	
	private void gravaOperacaoSaldo(EntityManager manager, final Caixa caixa, final Double movimento) throws Exception
	{
		boolean managerIndependente = false;
		
		if((managerIndependente = (manager == null)))
		{
			manager = ConexaoUtil.getEntityManager();
			manager.getTransaction().begin();
		}
		
		try
		{
			final LogOperacao logOperacao = new LogOperacao();
			logOperacao.setData(new Date());
			logOperacao.setCampo("SALDOCAIXA");
			logOperacao.setTipoLog(2);
			logOperacao.setUsuario(usuarioService.recuperarSessaoUsuarioLogado());
			logOperacao.setObservacao("SALDO DO CAIXA " + caixa.getId() + " ATUALIZADO COM VALOR " + movimento);
			logOperacaoService.salvar(manager, manager.getTransaction(), logOperacao, false);
			
			if(managerIndependente)
				manager.getTransaction().commit();
		}
		catch(Exception ex)
		{
			if(managerIndependente)
				manager.getTransaction().rollback();
			
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
}