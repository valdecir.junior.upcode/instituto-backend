package br.com.desenv.nepalign.service;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.http.HttpServletRequest;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.IgnSegurancaService;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.EmpresaUsuario;
import br.com.desenv.nepalign.model.Grupo;
import br.com.desenv.nepalign.model.LogDuplicata;
import br.com.desenv.nepalign.model.ParametrosSistema;
import br.com.desenv.nepalign.model.PerfilUsuarioGrupoUsuario;
import br.com.desenv.nepalign.model.SenhaGerentes;
import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.nepalign.model.Usuariogrupo;
import br.com.desenv.nepalign.model.Vendedor;
import br.com.desenv.nepalign.persistence.UsuarioPersistence;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.wsdl.SessionInfo;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;
import flex.messaging.FlexContext;
import flex.messaging.FlexSession;

public class UsuarioService extends GenericServiceIGN<Usuario, UsuarioPersistence>
{
	public UsuarioService() 
	{
		super();
	}

	/**
	 * Valida o login vindo do WebService.
	 * @param login
	 * @param senha
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public ArrayList validaLoginWebService(String login, String senha) throws Exception
	{
		Usuario usuario = null;		
		Usuario criterio = new Usuario();
		List<Usuario> retorno;

		Usuariogrupo criterioGrupo = new Usuariogrupo();
		List<Usuariogrupo> retornoGrupo;

		EmpresaUsuario criterioEmpresa = new EmpresaUsuario();
		List <EmpresaUsuario> retornoEmpresaUsuario;

		criterio.setLogin(login);
		criterio.setSenha(senha.toLowerCase());

		ArrayList<Object> retornoList = new ArrayList<Object>();

		try
		{
			retorno = listarPorObjetoFiltro(criterio);

			if (retorno.size() < 1)
				throw new Exception("Usuário não encontrado!");
			else
			{

				usuario = retorno.get(0);
				if(usuario.getAtivo()!=null && usuario.getAtivo().equals("N"))
					throw new Exception("Usuario não está ativo!");
				if (!usuario.getSenha().equals(senha) && !usuario.getLogin().equals(login)) 
					throw new Exception("Usuario ou senha invalidos!");
				else
				{
					usuario.setSenha(null);

					try
					{
						UsuariogrupoService usuarioGrupoService = new UsuariogrupoService();						
						criterioGrupo.setUsuario(usuario);
						retornoGrupo = usuarioGrupoService.listarPorObjetoFiltro(criterioGrupo);

						if(retornoGrupo.size()<1)
							throw new Exception("Usuário sem grupo definido!\n\nEntre em contato com o suporte.");
					}
					catch(Exception e)
					{
						throw e;
					}

					try
					{
						EmpresaUsuarioService empresaUsuarioService = new EmpresaUsuarioService();
						criterioEmpresa.setUsuario(usuario);
						retornoEmpresaUsuario = empresaUsuarioService.listarPorObjetoFiltro(criterioEmpresa);

						if(retornoEmpresaUsuario.size() < 1)
							throw new Exception("Usuário sem empresa definida!\n\nEntre em contato com o suporte.");
					}
					catch(Exception e)
					{
						throw e;
					}

					retornoList.add(0,usuario);
					retornoList.add(1,retornoGrupo);
					retornoList.add(2,retornoEmpresaUsuario);

					return retornoList;
				}
			}			
		}
		catch(Exception e)
		{
			throw e;
		}
	}

	/**
	 * Coloca as informa??es da sess?o na FlexSession a partir do objeto SessioInfo
	 * @param sessionInfo
	 * @return
	 * @throws Exception
	 */
	public Boolean colocarDadosFlexSession(SessionInfo sessionInfo) throws Exception
	{
		logoutUsuario();

		try
		{
			setSessionClientTimezoneOffset(sessionInfo.getTimezoneOffset());
			setSessaoUsuario(sessionInfo.getUsuario(), null, null, new Object());	
		}
		catch(Exception ex)
		{
			logoutUsuario();
			throw ex;
		}
		return true;
	}

	/**
	 * Coloca as informa??es da sess?o na FlexSession.
	 * @param sessionInfo
	 * @return
	 * @throws Exception
	 */
	private void setSessaoUsuario(Usuario usuario, List<Usuariogrupo> listaGrupo, List<EmpresaUsuario> listaEmpresa, Object objParam)
	{
		FlexSession session = FlexContext.getFlexSession();

		session.setAttribute("usuarioLogado", usuario); 
		session.setAttribute("usuarioListaEmpresa", listaEmpresa);
		session.setAttribute("listaUsuarioGrupo", listaGrupo);
		session.setAttribute("parametrosSistema", objParam);
		session.setAttribute("usuarioLogadoNome", usuario.getNome());
	}
	
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao
	public void setUae(Boolean uae)
	{
		FlexSession session = FlexContext.getFlexSession();

		if(session == null)
			return;

		try
		{
			session.removeAttribute("uae");	
		}
		catch(Exception ex)
		{
			//
		}

		session.setAttribute("uae", uae);
	}

	/**
	 * Verifica se o Cliente est? com um offset diferente do offset atual do servidor.
	 * Verifica??o ? usada para evitar problema na convers?o das datas
	 * @param offSet
	 * @throws Exception
	 */
	public void setSessionClientTimezoneOffset(Long offSet) throws Exception
	{
		if(Calendar.getInstance().getTimeZone().getOffset(new Date().getTime()) != offSet)
			throw new Exception("Ajuste a data/hora do computador para usar o sistema.");

		FlexSession session = FlexContext.getFlexSession();
		session.setAttribute("clientTimezoneOffset", offSet);
	}

	/**
	 * Recupera o objeto Usu?rio da sess?o atual do Cliente
	 * @return
	 */
	public Usuario recuperarSessaoUsuarioLogado()
	{
		FlexSession session = FlexContext.getFlexSession();

		if (session != null)
			return (Usuario) session.getAttribute("usuarioLogado");
		else
			return null;
	}
	
	public Boolean getSessionUae() throws Exception
	{
		return getSessionUae(null);
	}

	public Boolean getSessionUae(HttpServletRequest request) throws Exception
	{
		if(!DsvConstante.isLoja())
			return true;
		
		FlexSession session = FlexContext.getFlexSession();
		try
		{
			if (session != null)
				return (Boolean) session.getAttribute("uae");
			else if(request != null)
			{
				if((Usuario) request.getSession().getAttribute("usuarioLogado") == null)
					return false;
				else
					return ((Usuario) request.getSession().getAttribute("usuarioLogado")).isUae();
			}	
			else return false;
		}
		catch(Exception exUae)
		{
			Logger.getAnonymousLogger().log(Level.SEVERE, "Não está sendo possível efetuar a recuperação do parametro UAE ao gerar relatórios.");
			exUae.printStackTrace();
			return false;
		}
	}

	/**
	 * Limpa os atributos da sessão do cliente.
	 */
	public void logoutUsuario()
	{
		FlexSession session = FlexContext.getFlexSession();
		session.setAttribute("usuarioLogado", null); 
		session.setAttribute("usuarioListaEmpresa", null);
		session.setAttribute("listaUsuarioGrupo", null);
		session.setAttribute("parametrosSistema", null);
	}

	/**
	 * Salva um usuário que a senha ainda não foi encryptada
	 * @param usuario
	 * @throws Exception
	 */
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao
	public void salvarUsuarioCriptografado(Usuario usuario) throws Exception
	{
		salvarUsuarioCriptografado(usuario, null, null);
	}
	
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao
	public void salvarUsuarioCriptografado(Usuario usuario,EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;

		if(manager == null || transaction == null)
		{
			manager = ConexaoUtil.getEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			transacaoIndependente = true; 
		}

		try
		{
			if(usuario.getPerfilUsuario()==null)
				throw new Exception("Selecionar um perfil de usuario!");
			if(usuario.getEmpresaPadrao()==null)
				throw new Exception("Selecionar uma empesa!");
			if(usuario.getSenha()==null || usuario.getSenha().trim().equals(""))
				throw new Exception("Senha não pode ser vazia!");

			usuario.setSenha(IgnUtil.criptografarMd5(removerPorcetagem(usuario.getSenha())).toLowerCase());
			usuario =  salvar(manager, transaction, usuario); 
			PerfilUsuarioGrupoUsuario perfilUsuarioGrupoUsuarioCriterio = new PerfilUsuarioGrupoUsuario();
			perfilUsuarioGrupoUsuarioCriterio.setPerfilUsuario(usuario.getPerfilUsuario());
			List<PerfilUsuarioGrupoUsuario> listaPerfilGrupo = new PerfilUsuarioGrupoUsuarioService().listarPorObjetoFiltro(manager, perfilUsuarioGrupoUsuarioCriterio	, null, null, Integer.MAX_VALUE);
			for (PerfilUsuarioGrupoUsuario perfilUsuarioGrupoUsuario : listaPerfilGrupo)
			{
				Grupo grupo = perfilUsuarioGrupoUsuario.getGrupo();
				Usuariogrupo usuariogrupo = new Usuariogrupo();
				usuariogrupo.setGrupo(grupo);
				usuariogrupo.setUsuario(usuario);
				usuariogrupo.setEmpresa(usuario.getEmpresaPadrao());
				new UsuariogrupoService().salvarSemAtualizarLista(manager, transaction, usuariogrupo);
			}
			EmpresaUsuario empresaUsuario = new EmpresaUsuario();
			empresaUsuario.setEmpresa(usuario.getEmpresaPadrao());
			empresaUsuario.setUsuario(usuario);
			new EmpresaUsuarioService().salvar(manager, transaction, empresaUsuario);
			
			LogDuplicata log = new LogDuplicata();
			log.setData(new Date());
			log.setIdEmpresaFisica(usuario.getEmpresaPadrao().getId());
			log.setMotivo("Adicionou um novo usuario!");
			log.setNomeClasse("Usuario");
			log.setNomeUsuario(usuario.getId()+" - "+usuario.getNome());
			 new LogDuplicataService().salvar(manager, transaction, log,false);
			if(transacaoIndependente)
				transaction.commit();

		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();

			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
			
			IgnSegurancaService.atualizaListaUsuarios();
		}
	}
	
	/**
	 * Altera um usuario.
	 * Permite encryptar senha antes da altera??o
	 * @param usuario
	 * @param encryptarSenha
	 * @return
	 * @throws Exception
	 */
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao
	public Usuario alterarUsuarioCriptografado(Usuario usuario, Boolean encryptarSenha) throws Exception
	{
		return alterarUsuarioCriptografado(usuario, encryptarSenha, null, null);
	}
	
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao
	public Usuario alterarUsuarioCriptografado(Usuario usuario, Boolean encryptarSenha,EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;

		if(manager == null || transaction == null)
		{
			manager = ConexaoUtil.getEntityManager(); 
			transaction = manager.getTransaction();
			transaction.begin();
			transacaoIndependente = true; 
		}

		try
		{
			if(usuario.getPerfilUsuario()==null)
				throw new Exception("Selecionar um perfil de usuario!");
			if(usuario.getEmpresaPadrao()==null)
				throw new Exception("Selecionar uma empesa!");
			if(usuario.getSenha()==null || usuario.getSenha().trim().equals(""))
				throw new Exception("Senha não pode ser vazia!");
			
			if(encryptarSenha)
				usuario.setSenha(IgnUtil.criptografarMd5(removerPorcetagem(usuario.getSenha()).toLowerCase()));
			else
				usuario.setSenha(usuario.getSenha().toLowerCase());
			
			Usuariogrupo usuariogrupoCriterio = new Usuariogrupo();
			usuariogrupoCriterio.setUsuario(usuario);
			List<Usuariogrupo> listaGrupoUsuarioAntiga = new UsuariogrupoService().listarPorObjetoFiltro(manager, usuariogrupoCriterio, null, null, null);
			for (int i = 0; i < listaGrupoUsuarioAntiga.size(); i++) 
			{
				new UsuariogrupoService().excluirSemAtualizarLista(manager, transaction, listaGrupoUsuarioAntiga.get(i));
			}
			EmpresaUsuario empresaUsuarioCriterio = new EmpresaUsuario();
			empresaUsuarioCriterio.setUsuario(usuario);
			List<EmpresaUsuario> listaEmpresaUsuarioAntiga = new EmpresaUsuarioService().listarPorObjetoFiltro(manager, empresaUsuarioCriterio, null, null, null);
			for (int i = 0; i < listaEmpresaUsuarioAntiga.size(); i++) 
			{
				new EmpresaUsuarioService().excluir(manager, transaction, listaEmpresaUsuarioAntiga.get(i));
			}
			PerfilUsuarioGrupoUsuario perfilUsuarioGrupoUsuarioCriterio = new PerfilUsuarioGrupoUsuario();
			perfilUsuarioGrupoUsuarioCriterio.setPerfilUsuario(usuario.getPerfilUsuario());
			List<PerfilUsuarioGrupoUsuario> listaPerfilGrupo = new PerfilUsuarioGrupoUsuarioService().listarPorObjetoFiltro(manager, perfilUsuarioGrupoUsuarioCriterio	, null, null, Integer.MAX_VALUE);
			for (PerfilUsuarioGrupoUsuario perfilUsuarioGrupoUsuario : listaPerfilGrupo)
			{
				Grupo grupo = perfilUsuarioGrupoUsuario.getGrupo();
				Usuariogrupo usuariogrupo = new Usuariogrupo();
				usuariogrupo.setGrupo(grupo);
				usuariogrupo.setUsuario(usuario);
				usuariogrupo.setEmpresa(usuario.getEmpresaPadrao());
				new UsuariogrupoService().salvarSemAtualizarLista(manager, transaction, usuariogrupo);
			}
			EmpresaUsuario empresaUsuario = new EmpresaUsuario();
			empresaUsuario.setEmpresa(usuario.getEmpresaPadrao());
			empresaUsuario.setUsuario(usuario);
			new EmpresaUsuarioService().salvar(manager, transaction, empresaUsuario);
			usuario = atualizar(manager, transaction, usuario); 
			
			if(transacaoIndependente)
				transaction.commit();

		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();

			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
			
			IgnSegurancaService.atualizaListaUsuarios();
		}
		return usuario;
	}
	
	/**
	 * Remove os sinais de percentual de uma {@link String}
	 * @param senha
	 * @return
	 */
	public String removerPorcetagem(String senha)
	{	
		String s=senha;      
		String removeString="";

		for(int i =0;i<s.length();i++)
		{
			if(s.charAt(i)=='%')
				removeString=removeString+"%";
			else 
				break;
		}

		String stirngRemover = s.replaceFirst(removeString, "");

		boolean x = true;

		while(x)
		{
			if(stirngRemover.endsWith("%"))
				stirngRemover = stirngRemover.substring(0, stirngRemover.length() - 1); 
			else
				x = false;
		}

		return stirngRemover;
	}

	/**
	 * Altera a senha do usuário
	 * @param usuario
	 * @param senhaAntiga
	 * @param novaSenha
	 * @param novaSenhaConfirma
	 * @throws Exception
	 */
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao
	public void alterarSenhaUsuario(Usuario usuario, String senhaAntiga, String novaSenha, String novaSenhaConfirma) throws Exception
	{
		this.alterarSenhaUsuario(usuario, senhaAntiga, novaSenha, novaSenhaConfirma, null, null);
	}

	/**
	 * Altera a senha do usuário
	 * @param usuario
	 * @param senhaAntiga
	 * @param novaSenha
	 * @param novaSenhaConfirma
	 * @param manager
	 * @param transaction
	 * @throws Exception
	 */
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao
	public void alterarSenhaUsuario(Usuario usuario, String senhaAntiga, String novaSenha, String novaSenhaConfirma, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;

		if(manager == null || transaction == null)
		{
			manager = ConexaoUtil.getEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			transacaoIndependente = true; 
		}

		try
		{
			usuario = this.recuperarPorId(manager, usuario.getId());

			if(novaSenha.length() < 1 || novaSenhaConfirma.length() < 1 || senhaAntiga.length() < 1)
				throw new Exception("Informe senhas validas");

			if(!novaSenha.equals(novaSenhaConfirma))
				throw new Exception("As duas senhas nao coincidem");

			if(!usuario.getSenha().equals(IgnUtil.criptografarMd5(senhaAntiga)))
				throw new Exception("Senha atual invalida");

			usuario.setSenha(IgnUtil.criptografarMd5(novaSenha).toLowerCase());
			this.atualizar(manager, transaction, usuario);

			if(transacaoIndependente)
				transaction.commit();

			this.logoutUsuario();
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();

			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}

	/**
	 * Salvar um novo usu?rio
	 */
	public Usuario salvar(Usuario entidade) throws Exception
	{
		Usuario usuarioCriterio = new Usuario();
		usuarioCriterio.setLogin(entidade.getLogin());
		usuarioCriterio.setSenha(entidade.getSenha());

		if(listarPorObjetoFiltro(usuarioCriterio).size() > 0)
			throw new Exception("J? existe um usu?rio cadastrado com essas informa??es!");

		return super.salvar(entidade);
	}

	/**
	 * Verifica se o usu?rio logado ? um vendedor.
	 * Se o usu?rio for um vendedor, a tela de venda ? aberta autom?ticamente (tratado pelo cliente)
	 * @param usuarioLogado
	 * @return
	 * @throws Exception
	 */
	public Boolean iniciarComoVendedor(Usuario usuarioLogado) throws Exception
	{
		try
		{
			Vendedor vendedorCriterio = new Vendedor();
			vendedorCriterio.setUsuario(usuarioLogado);

			Boolean vendedorCadastrado = new VendedorService().listarPorObjetoFiltro(vendedorCriterio).size() > 0;

			if(vendedorCadastrado && (!usuarioLogado.getUsuarioMaster().equals("S") &&!usuarioLogado.getUsuarioGlobal().equals("S")))
				return true;
			else
				return false;
		}
		catch(Exception ex)
		{
			return false;
		}
	}

	/**
	 * Atualiza as prefer?ncias do usu?rio
	 * @param usuario
	 * @param preferencias
	 * @throws Exception
	 */
	public void atualizarPreferenciasUsuario(Usuario usuario, byte[] preferencias) throws Exception
	{
		java.sql.Connection conn = null;
		try
		{
			conn = ConexaoUtil.getConexaoPadrao();

			String query = "UPDATE usuario set preferencias = ? WHERE idUsuario = " + usuario.getId().toString();

			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setBytes(1, preferencias);
			pstmt.execute();

			conn.close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Recupera as preferências do usuário em um byte[]
	 * @param usuario
	 * @return
	 * @throws Exception
	 */
	public byte[] recuperarPreferencias(Usuario usuario) throws Exception
	{
		try
		{
			return ((UsuarioPersistence) super.getPersistence()).recuperarPreferencias(usuario);	
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return new byte[0x0];
	}
	
	/**
	 * Verifica se a Senha UAE digitada é válida
	 * @param senha
	 * @return
	 * @throws Exception
	 */
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao
	public Boolean verificarUae(String senha) throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		
		try
		{
			SenhaGerentes senhaGerentes = new SenhaGerentes();
			senhaGerentes.setSenha(IgnUtil.criptografarMd5(senha).toLowerCase());
			senhaGerentes.setModulo(new ModuloSenhaGerentesService().recuperarPorDescricao("UAE"));
			
			return new SenhaGerentesService().listarPorObjetoFiltro(manager, senhaGerentes, null, null, null).size() > 0;	
		}
		finally
		{
			if(manager.isOpen())
				manager.close();	
		}
	}

	/**
	 * Verifica se o Id do usuário está em um parâmetro definido nos {@link ParametrosSistema}
	 * @param usuario
	 * @param nomeParametro
	 * @return
	 * @throws Exception
	 */
	public boolean usuarioAutorizado(final Usuario usuario, final String nomeParametro) throws Exception
	{
		if(usuario == null || DsvConstante.getParametrosSistema().get(nomeParametro)== null)
			return false; // SISTEMA??
		
		return new IgnUtil().contains((usuario == null ? recuperarSessaoUsuarioLogado() : usuario).getId().toString(), Arrays.asList(DsvConstante.getParametrosSistema().get(nomeParametro).split(",")));
	}
	
	/**
	 * Pesquisa o usuário e define se ele está autorizado à exibir os relatórios
	 * @param strDecodificada Dados do usuário
	 * @return {@link Usuario} caso o usuário for identificado. {@link null} caso não.
	 * @throws Exception Se o usuário não for encontrado, ou se a quantidade de parâmetros não for correta
	 */
	public Usuario retornaUsuarioAutenticacaoRelatorio(String strDecodificada) throws Exception
	{
		String valores[] = strDecodificada.split("\\|\\$\\|");
		
		Usuario criterio = new Usuario();
		criterio.setId(new Integer(valores[0]));
		criterio.setLogin(valores[1]);
		
		List<Usuario> listaUsuarios = listarPorObjetoFiltro(criterio);
		
		if(listaUsuarios.size() == 0x00)
			throw new Exception("Usuário não encontrado");
		if(valores.length != 3)
			throw new Exception("Número de parâmetros incorretos!");

		Usuario usuario = listaUsuarios.get(0);
		usuario.setUae(valores[2].equals("true"));

		return usuario;
	}

	public static void main(String[] args) throws Exception
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		UsuarioService usuarioService = new UsuarioService();
		Usuario usuario = usuarioService.recuperarPorId(532);

		dos.write(usuarioService.recuperarPreferencias(usuario));
		dos.writeUTF("[HOME]="); //usar antes de come?ar os dados das tiles
		dos.writeUTF("RESUMO DAS VENDAS POR GRUPO"); //titulo da tile
		dos.writeUTF("0xFFC125");// cor da tile
		dos.writeUTF("br.com.desenv.nepalign.view.component.RelatorioVendasGrupo"); //nome da tela

		dos.writeUTF("[HOME]="); //usar antes de come?ar os dados das tiles
		dos.writeUTF("RESUMO DAS VENDAS DO DIA"); //titulo da tile
		dos.writeUTF("0xFFC125");// cor da tile
		dos.writeUTF("br.com.desenv.nepalign.view.component.FormRelatorioVendaPorVendedor"); //nome da tela

		dos.writeUTF("[HOME]="); //usar antes de come?ar os dados das tiles
		dos.writeUTF("RESUMO DAS VENDAS (GRUPO / MARCA)"); //titulo da tile
		dos.writeUTF("0xFFC125");// cor da tile
		dos.writeUTF("br.com.desenv.nepalign.view.component.PedidoVendaComponenteRelatorioMarcaEGrupo"); //nome da tela

		dos.writeUTF("[HOME]="); //usar antes de come?ar os dados das tiles
		dos.writeUTF("RESUMO DAS VENDAS POR PLANO"); //titulo da tile
		dos.writeUTF("0xFFC125");// cor da tile
		dos.writeUTF("br.com.desenv.nepalign.view.component.FormRelatorioVendaPorPlano"); //nome da tela

		dos.writeUTF("[HOME]="); //usar antes de come?ar os dados das tiles
		dos.writeUTF("TOTAIS ESTOQUE/GRUPO"); //titulo da tile
		dos.writeUTF("0x36A8CE");// cor da tile
		dos.writeUTF("br.com.desenv.nepalign.view.component.FormRelatorioEstoqueGeral"); //nome da tela

		dos.writeUTF("[HOME]="); //usar antes de come?ar os dados das tiles
		dos.writeUTF("CONSULTA SALDO"); //titulo da tile
		dos.writeUTF("0x36A8CE");// cor da tile
		dos.writeUTF("br.com.desenv.nepalign.view.component.SaldoEstoqueProdutoComponenteRelatorio"); //nome da tela

		dos.writeUTF("[HOME]="); //usar antes de come?ar os dados das tiles
		dos.writeUTF("RESUMO CONTAS A PAGAR POR DIA/QUINZENA"); //titulo da tile
		dos.writeUTF("0x5FB336");// cor da tile
		dos.writeUTF("br.com.desenv.nepalign.view.component.FormRelatorioContasAPagarResumo"); //nome da tela

		dos.writeUTF("[HOME]="); //usar antes de come?ar os dados das tiles
		dos.writeUTF("PEDIDOS PENDENTES"); //titulo da tile
		dos.writeUTF("0xDA532C");// cor da tile
		dos.writeUTF("br.com.desenv.nepalign.view.component.PedidoCompraVendaPendenteComponenteRelatorio"); //nome da tela

		dos.writeUTF("[SHOW_LAST_MOVUPDATE]=");
		dos.writeBoolean(true);
		dos.writeUTF("[HOME.CONTROLLER]=");
		dos.writeUTF("ControleMenuRoberto.swf");

		usuarioService.atualizarPreferenciasUsuario(usuario, baos.toByteArray());
	}
}