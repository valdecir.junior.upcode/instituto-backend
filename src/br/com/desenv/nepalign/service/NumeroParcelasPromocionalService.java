package br.com.desenv.nepalign.service;

import java.util.Date;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.NumeroParcelasPromocional;
import br.com.desenv.nepalign.persistence.NumeroParcelasPromocionalPersistence;

public class NumeroParcelasPromocionalService extends GenericServiceIGN<NumeroParcelasPromocional, NumeroParcelasPromocionalPersistence>
{

	public NumeroParcelasPromocionalService() 
	{
		super();
	}
	public NumeroParcelasPromocional verificarParcelasPromocionais(Date data,EmpresaFisica empresa) throws Exception
	{
		return new NumeroParcelasPromocionalPersistence().verificarParcelasPromocionais(data, empresa);
	}
	

}

