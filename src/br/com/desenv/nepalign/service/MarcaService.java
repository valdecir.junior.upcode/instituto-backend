package br.com.desenv.nepalign.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.encoding.XMLType;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.Marca;
import br.com.desenv.nepalign.model.Tamanho;
import br.com.desenv.nepalign.persistence.MarcaPersistence;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class MarcaService extends GenericServiceIGN<Marca, MarcaPersistence>
{

	public MarcaService() 
	{
		super();
	}
	
	public Marca recuperarTamanhoEscritorio(Integer idMarca) throws Exception
	{
		return recuperarTamanhoEscritorio(idMarca, null, null);
	}
	public Marca recuperarTamanhoEscritorio(Integer idMarca,EntityManager manager, EntityTransaction transaction ) throws Exception
	{
		Boolean transacaoIndependente = false;
		Marca marca = null;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}



			if(DsvConstante.getParametrosSistema().get("caminhoWebServiceVendaEscritorio")==null)
			{
				return marca;
			}
			String endpoint = DsvConstante.getParametrosSistema().get("caminhoWebServiceVendaEscritorio");
			Service  service = new Service();
			Call call= (Call) service.createCall();
			call.setTargetEndpointAddress(new java.net.URL(endpoint));
			call.setOperationName("recuperaMarca");
			call.addParameter("idMarca", XMLType.XSD_INT, ParameterMode.IN);
			call.setReturnClass(Marca.class);
			Marca retorno =  (Marca) call.invoke(new Object[] {idMarca  });
//			String[] arr = retorno.split(",");
//			marca = new Marca();
//			marca.setCodigo(Integer.parseInt(arr[0]));
//			marca.setDescricao(arr[1]);
//			marca.setFranquia(Integer.parseInt(arr[2]));
			System.out.println("Usou WebService de marca:"+idMarca);
//			marca = this.salvar(manager, transaction, marca);
			if(transacaoIndependente)
				transaction.commit();

		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
		return marca;
	}
	
}

