package br.com.desenv.nepalign.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.ItemMovimentacaoEstoque;
import br.com.desenv.nepalign.model.MovimentacaoEstoque;
import br.com.desenv.nepalign.model.OperacaoMovimentacaoEstoque;
import br.com.desenv.nepalign.persistence.ItemMovimentacaoEstoquePersistence;
import br.com.desenv.nepalign.persistence.MovimentacaoEstoquePersistence;
import br.com.desenv.nepalign.persistence.OperacaoMovimentacaoEstoquePersistence;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class OperacaoMovimentacaoEstoqueService extends GenericServiceIGN<OperacaoMovimentacaoEstoque, OperacaoMovimentacaoEstoquePersistence>
{ 
	public OperacaoMovimentacaoEstoqueService() 
	{
		super();
	}  
	
	public static void main(String[] args) throws Exception
	{
		if(args[0x0] == null)
			return;
		
		if(args[0x0].equals("1"))
		{
			String dataInicial = args[0x1];
			String dataFinal = args[0x2];

			new OperacaoMovimentacaoEstoqueService().infosByOPME2(args[0x3], new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MM/yyyy").parse(dataInicial)), new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MM/yyyy").parse(dataFinal)));	
		}
		else
			new UtilSaldoEstoqueProdutoService().regerarSaldoBaseadoEmMovimentacaoTmp();
		
		
		/*String dataInicial = "22/11/2014";
		String dataFinal = "22/11/2014";

		new OperacaoMovimentacaoEstoqueService().infosByOPME2(new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MM/yyyy").parse(dataInicial)), new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MM/yyyy").parse(dataFinal)));*/	
	}
	
	private void infosByOP() throws Exception
	{
		DsvConstante.getParametrosSistema();
		
		Connection con = ConexaoUtil.getConexaoPadrao();
		ItemMovimentacaoEstoqueService itemMovimentacaoEstoqueService = new ItemMovimentacaoEstoqueService();
		MovimentacaoEstoqueService movimentacaoEstoqueService = new MovimentacaoEstoqueService();
		IgnUtil ignUtil = new IgnUtil();  
		
		String sqlQuery = "SELECT dataoperacao, nomeclasse, operacao, objeto FROM nepalign.Operacao where dataOperacao >= '2014-10-10 00:00:00' " + 
				" and nomeClasse = 'br.com.desenv.nepalign.persistence.ItemMovimentacaoEstoquePersistence' and operacao = 'salvar' " + 
				" order by dataOperacao;";
		 
		 
		Statement st = con.createStatement();
		Statement stAtualizacao = con.createStatement();
    	   
		ResultSet rs = st.executeQuery(sqlQuery); 
		
		boolean hasOp = rs.next();
		
		if(!hasOp)
			Logger.getGlobal().info("No Pending OP's");
		else 
		{
			Logger.getGlobal().info("Found Pending OP's!");
			rs.beforeFirst();	
		}
		
		while (rs.next())
		{
			String dataOperacao = rs.getString(1);
    		String classe = rs.getString(2);
			String operacao = rs.getString(3);
			byte[] objeto = rs.getBytes(4);
    		
    		//Logger.getGlobal().info("Handling OP " + idOperacaoMovimentacaoEstoque + " [" + operacao + "] to class " + classe);
    		
    		boolean handled = false;
    		
    		if(classe.equalsIgnoreCase(MovimentacaoEstoquePersistence.class.getName()))
    		{
        		MovimentacaoEstoque movimentacaoEstoque = (MovimentacaoEstoque) ignUtil.recuperarObjectoPorByteArray(objeto);
        		
        		StringBuilder sql = new StringBuilder();
        		sql.append(" INSERT INTO `nepalign`.`tmpmovimentacaoestoque` ");
        		sql.append(" (`idMovimentacaoEstoque`, ");
        		sql.append(" `idEmpresaFisica`, ");
        		sql.append(" `idEstoque`, ");
        		sql.append(" `idTipoMovimentacaoEstoque`, ");
        		sql.append(" `idTipoDocumentoMovimentoEstoque`, ");
        		sql.append(" `idCliente`, ");
        		sql.append(" `idUsuario`, ");
        		sql.append(" `entradaSaida`, ");
        		sql.append(" `numeroDocumento`, ");
        		sql.append(" `dataMovimentacao`, ");
        		sql.append(" `idDocumentoOrigem`, ");
        		sql.append(" `observacao`, ");
        		sql.append(" `faturado`, ");
        		sql.append(" `manual`, ");
        		sql.append(" `numeroVolume`, ");
        		sql.append(" `peso`, ");
        		sql.append(" `valorFrete`, ");
        		sql.append(" `idFornecedor`, "); 
        		sql.append(" `idStatusMovimentacaoEstoque`, ");
        		sql.append(" `idEmpresaDestino`) ");
        		sql.append(" VALUES ");
        		sql.append(" (" + movimentacaoEstoque.getId() + ", ");
        		sql.append(" " + movimentacaoEstoque.getEmpresaFisica().getId() + ", ");
        		sql.append(" " + movimentacaoEstoque.getEstoque().getId() + ", ");
        		sql.append(" " + movimentacaoEstoque.getTipoMovimentacaoEstoque().getId() + ", ");
        		sql.append(" " + movimentacaoEstoque.getTipoDocumentoMovimento().getId() + ", ");
        		sql.append(" " + (movimentacaoEstoque.getCliente() == null ? "null" : movimentacaoEstoque.getCliente().getId()) + ", ");
        		sql.append(" " + (movimentacaoEstoque.getUsuario() == null ? "null" : movimentacaoEstoque.getUsuario().getId()) + ", ");
        		sql.append(" '" + movimentacaoEstoque.getEntradaSaida() + "', ");
        		sql.append(" '" + movimentacaoEstoque.getNumeroDocumento() + "', ");
        		sql.append(" '" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(movimentacaoEstoque.getDataMovimentacao()) + "', ");
        		sql.append(" '" + (movimentacaoEstoque.getIdDocumentoOrigem() == null ? "0" : movimentacaoEstoque.getIdDocumentoOrigem()) + "', ");
        		sql.append(" '" + movimentacaoEstoque.getObservacao() + "', ");
        		sql.append(" '" + movimentacaoEstoque.getFaturado() + "', ");
        		sql.append(" '" + movimentacaoEstoque.getManual() + "', ");
        		sql.append(" '" + (movimentacaoEstoque.getNumeroVolume() == null ? "0" : movimentacaoEstoque.getNumeroVolume()) + "', ");
        		sql.append(" " + movimentacaoEstoque.getPeso() + ", ");
        		sql.append(" " + movimentacaoEstoque.getValorFrete() + ", ");
        		sql.append(" " + (movimentacaoEstoque.getForncedor() == null ? "null" :  movimentacaoEstoque.getForncedor().getId()) + ", ");
        		sql.append(" " + movimentacaoEstoque.getStatusMovimentacaoEstoque().getId() + ", ");
        		sql.append(" " + (movimentacaoEstoque.getEmpresaDestino() == null ? "null" : movimentacaoEstoque.getEmpresaDestino().getId()) + "); ");
        		sql.append("  ");
        		
        		//System.out.println(sql);
        		
        		movimentacaoEstoque.setUsuario(null);
        		movimentacaoEstoque.setForncedor(null);
        		movimentacaoEstoque.setCliente(null);
        		
        		try
        		{
            		if (operacao != null && operacao.equalsIgnoreCase("SALVAR"))
            		{
            			stAtualizacao.executeUpdate(sql.toString());
            			System.out.println("Salvar " + movimentacaoEstoque.getNumeroDocumento() + " " + movimentacaoEstoque.getId());
            		}
            		    //movimentacaoEstoqueService.salvar(movimentacaoEstoque, false);
            		else if (operacao != null && operacao.equalsIgnoreCase("ALTERAR"))
            		{
            			stAtualizacao.executeUpdate(sql.toString());
            			System.out.println("Alterar " + movimentacaoEstoque.getNumeroDocumento() + " " + movimentacaoEstoque.getId());
            		}
            			//movimentacaoEstoqueService.atualizar(movimentacaoEstoque, false)
            		else if (operacao != null && operacao.equalsIgnoreCase("EXCLUIR"))
            		{
            			//stAtualizacao.executeUpdate("DELETE FROM tmpmovimentacaoestoque WHERE idmovimentacaoestoque = " + movimentacaoEstoque.getId() + " limit 1");
            			System.out.println("Excluir " + movimentacaoEstoque.getNumeroDocumento() + " " + movimentacaoEstoque.getId());
            		}
            		    //movimentacaoEstoqueService.excluir(movimentacaoEstoque); 
            		
            		handled = true;
        		}
        		catch(Exception ex)
        		{
        			Logger.getGlobal().severe("Fail ME " + (movimentacaoEstoque == null ? "null" : movimentacaoEstoque.getId().toString()));
        		}
    		}
    		else if(classe.equalsIgnoreCase(ItemMovimentacaoEstoquePersistence.class.getName()))
    		{ 
    			//if(idOperacaoMovimentacaoEstoque == 8960)
    				//System.out.println("Parou"); 
        		ItemMovimentacaoEstoque itemMovimentacaoEstoque = (ItemMovimentacaoEstoque) ignUtil.recuperarObjectoPorByteArray(objeto);
        		 	
        		MovimentacaoEstoque movimentacaoEstoque = itemMovimentacaoEstoque.getMovimentacaoEstoque();
        		
        		StringBuilder sql = new StringBuilder();
        		sql.append(" INSERT INTO `nepalign`.`tmpmovimentacaoestoque` ");
        		sql.append(" (`idTmpMovimentacaoEstoque`, ");
        		sql.append(" `idEmpresaFisica`, ");
        		sql.append(" `idEstoque`, ");
        		sql.append(" `idTipoMovimentacaoEstoque`, ");
        		sql.append(" `idTipoDocumentoMovimentoEstoque`, ");
        		sql.append(" `idCliente`, ");
        		sql.append(" `idUsuario`, ");
        		sql.append(" `entradaSaida`, ");
        		sql.append(" `numeroDocumento`, ");
        		sql.append(" `dataMovimentacao`, ");
        		sql.append(" `idDocumentoOrigem`, ");
        		sql.append(" `observacao`, ");
        		sql.append(" `faturado`, ");
        		sql.append(" `manual`, ");
        		sql.append(" `numeroVolume`, ");
        		sql.append(" `peso`, ");
        		sql.append(" `valorFrete`, ");
        		sql.append(" `idFornecedor`, "); 
        		sql.append(" `idStatusMovimentacaoEstoque`, ");
        		sql.append(" `idEmpresaDestino`) ");
        		sql.append(" VALUES ");
        		sql.append(" (" + movimentacaoEstoque.getId() + ", ");
        		sql.append(" " + movimentacaoEstoque.getEmpresaFisica().getId() + ", ");
        		sql.append(" " + movimentacaoEstoque.getEstoque().getId() + ", ");
        		sql.append(" " + movimentacaoEstoque.getTipoMovimentacaoEstoque().getId() + ", ");
        		sql.append(" " + movimentacaoEstoque.getTipoDocumentoMovimento().getId() + ", ");
        		sql.append(" " + (movimentacaoEstoque.getCliente() == null ? "null" : movimentacaoEstoque.getCliente().getId()) + ", ");
        		sql.append(" " + (movimentacaoEstoque.getUsuario() == null ? "null" : movimentacaoEstoque.getUsuario().getId()) + ", ");
        		sql.append(" '" + movimentacaoEstoque.getEntradaSaida() + "', ");
        		sql.append(" '" + movimentacaoEstoque.getNumeroDocumento() + "', ");
        		sql.append(" '" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(movimentacaoEstoque.getDataMovimentacao()) + "', ");
        		sql.append(" '" + (movimentacaoEstoque.getIdDocumentoOrigem() == null ? "0" : movimentacaoEstoque.getIdDocumentoOrigem()) + "', ");
        		sql.append(" '" + movimentacaoEstoque.getObservacao() + "', ");
        		sql.append(" '" + movimentacaoEstoque.getFaturado() + "', ");
        		sql.append(" " + (movimentacaoEstoque.getManual() == null || movimentacaoEstoque.getManual().equals("null") ? "null" : "'" + movimentacaoEstoque.getManual()  + "'" ) + ", ");
        		sql.append(" '" + (movimentacaoEstoque.getNumeroVolume() == null ? "0" : movimentacaoEstoque.getNumeroVolume()) + "', ");
        		sql.append(" " + movimentacaoEstoque.getPeso() + ", ");
        		sql.append(" " + movimentacaoEstoque.getValorFrete() + ", ");
        		sql.append(" " + (movimentacaoEstoque.getForncedor() == null ? "null" :  movimentacaoEstoque.getForncedor().getId()) + ", ");
        		sql.append(" " + movimentacaoEstoque.getStatusMovimentacaoEstoque().getId() + ", ");
        		sql.append(" " + (movimentacaoEstoque.getEmpresaDestino() == null ? "null" : movimentacaoEstoque.getEmpresaDestino().getId()) + "); ");
        		sql.append("  ");
        		
        		//System.out.println(sql);
        		
        		movimentacaoEstoque.setUsuario(null);
        		movimentacaoEstoque.setForncedor(null);
        		movimentacaoEstoque.setCliente(null);
        		
        		try
        		{
            		if (operacao != null && operacao.equalsIgnoreCase("SALVAR"))
            		{
            			stAtualizacao.executeUpdate(sql.toString());
            			System.out.println("Salvar " + movimentacaoEstoque.getNumeroDocumento() + " " + movimentacaoEstoque.getId());
            		}
            		    //movimentacaoEstoqueService.salvar(movimentacaoEstoque, false);
            		else if (operacao != null && operacao.equalsIgnoreCase("ALTERAR"))
            		{
            			stAtualizacao.executeUpdate(sql.toString());
            			System.out.println("Alterar " + movimentacaoEstoque.getNumeroDocumento() + " " + movimentacaoEstoque.getId());
            		}
            			//movimentacaoEstoqueService.atualizar(movimentacaoEstoque, false)
            		else if (operacao != null && operacao.equalsIgnoreCase("EXCLUIR"))
            		{
            			//stAtualizacao.executeUpdate("DELETE FROM tmpmovimentacaoestoque WHERE idmovimentacaoestoque = " + movimentacaoEstoque.getId() + " limit 1");
            			System.out.println("Excluir " + movimentacaoEstoque.getNumeroDocumento() + " " + movimentacaoEstoque.getId());
            		}
            		    //movimentacaoEstoqueService.excluir(movimentacaoEstoque); 
            		
            		handled = true;
        		}
        		catch(Exception ex)
        		{
        			Logger.getGlobal().severe("Fail ME " + (movimentacaoEstoque == null ? "null" : movimentacaoEstoque.getId().toString()));
        		}
        		
        		
        		/*StringBuilder sql = new StringBuilder();
        		sql.append(" INSERT INTO `nepalign`.`tmpitemmovimentacaoestoque` ");
        		sql.append(" (`idItemMovimentacaoEstoque`, ");
        		sql.append(" `idEstoqueProduto`, ");
        		sql.append(" `idMovimentacaoEstoque`, ");
        		sql.append(" `idUnidadeMedida`, ");
        		sql.append(" `idProduto`, ");
        		sql.append(" `idDocumentoOrigem`, ");
        		sql.append(" `sequencialItem`, ");
        		sql.append(" `quantidade`, ");
        		sql.append(" `valor`, ");
        		sql.append(" `custo`, ");
        		sql.append(" `numeroLote`, ");
        		sql.append(" `dataValidade`, ");
        		sql.append(" `dataFabricacao`, ");
        		sql.append(" `venda`) ");
        		sql.append(" VALUES ");
        		sql.append(" (" + itemMovimentacaoEstoque.getId() + ", ");
        		sql.append(" " + itemMovimentacaoEstoque.getEstoqueProduto().getId() + ", ");
        		sql.append(" " + itemMovimentacaoEstoque.getMovimentacaoEstoque().getId() + ", ");
        		sql.append(" " + (itemMovimentacaoEstoque.getUnidadeMedida() == null ? "null" : itemMovimentacaoEstoque.getUnidadeMedida().getId()) + ", ");
        		sql.append(" " + itemMovimentacaoEstoque.getProduto().getId() + ", ");
        		sql.append(" '" + (itemMovimentacaoEstoque.getDocumentoOrigem() == null ? "null" : itemMovimentacaoEstoque.getDocumentoOrigem()) + "', ");
        		sql.append(" " + itemMovimentacaoEstoque.getSequencialItem() + ", ");
        		sql.append(" " + itemMovimentacaoEstoque.getQuantidade() + ", ");
        		sql.append(" " + itemMovimentacaoEstoque.getValor() + ", ");
        		sql.append(" " + itemMovimentacaoEstoque.getCusto() + ", ");
        		sql.append(" " + (itemMovimentacaoEstoque.getNumeroLote() == null ? "null" : itemMovimentacaoEstoque.getNumeroLote()) + ", ");
        		sql.append(" '" + (itemMovimentacaoEstoque.getDataValidade() == null ? "0000-00-00" : new SimpleDateFormat().format(itemMovimentacaoEstoque.getDataValidade())) + "', ");
        		sql.append(" '" + (itemMovimentacaoEstoque.getDataFabricacao() == null ? "0000-00-00" : new SimpleDateFormat().format(itemMovimentacaoEstoque.getDataFabricacao())) + "', ");
        		sql.append(" " + itemMovimentacaoEstoque.getVenda() + "); ");
        		sql.append("  ");
        		sql.append("  ");*/
        		
         		//itemMovimentacaoEstoque.setMovimentacaoEstoque(movimentacaoEstoqueService.recuperarPorId(itemMovimentacaoEstoque.getMovimentacaoEstoque().getId()));
        		
        		//System.out.println(itemMovimentacaoEstoque.getEstoqueProduto().getCodigoBarras() + " " + itemMovimentacaoEstoque.getQuantidade() + " " + operacao + " " + idOperacaoMovimentacaoEstoque);
        		 
        		
        		try
        		{
        			if(operacao != null && operacao.equalsIgnoreCase("SALVAR"))
        				//itemMovimentacaoEstoqueService.salvar(itemMovimentacaoEstoque, false);
        				//stAtualizacao.executeUpdate(sql.toString());
        				System.out.println("Salvar");
            		else if (operacao != null && operacao.equalsIgnoreCase("ALTERAR"))
            			//itemMovimentacaoEstoqueService.atualizar(itemMovimentacaoEstoque, false);
            			System.out.println("Alterar");
            		else if (operacao != null && operacao.equalsIgnoreCase("EXCLUIR"))
            			//itemMovimentacaoEstoqueService.excluir(itemMovimentacaoEstoque, false);
            			//stAtualizacao.executeUpdate("DELETE FROM tmpitemmovimentacaoestoque where iditemmovimentacaoestoque = " + itemMovimentacaoEstoque.getId() + " limit 1;");
            			System.out.println("Excluir " + itemMovimentacaoEstoque.getMovimentacaoEstoque().getId());
        			
        			handled = true;
        		}
        		catch(Exception ex)
        		{
        			Logger.getGlobal().severe("Fail on IME to ME " + (itemMovimentacaoEstoque.getMovimentacaoEstoque() == null ? "null" : itemMovimentacaoEstoque.getMovimentacaoEstoque().getId().toString()));
        		}
    		}
    		//else
    			//Logger.getGlobal().warning("Invalid OP Class : " + classe);

    		if(handled)
    		{
        		//stAtualizacao.executeUpdate("update nepalignloja"  + numeroEmpresaFisica + ".operacaomovimentacaoestoque set flagsincronizacao = 1 where idoperacao = " + idOperacaoMovimentacaoEstoque + ";");
        		
        		//Logger.getGlobal().info("OP Updated to Handled.");	
    		}
    		//else
    			//Logger.getGlobal().info("Fail to Handle");
		}

		if(!hasOp)
			Logger.getGlobal().info("No more OP's to handle");
	}
	
	private void infosByOPME() throws Exception
	{
		DsvConstante.getParametrosSistema();
		
		Connection con = ConexaoUtil.getConexaoPadrao();
		ItemMovimentacaoEstoqueService itemMovimentacaoEstoqueService = new ItemMovimentacaoEstoqueService();
		MovimentacaoEstoqueService movimentacaoEstoqueService = new MovimentacaoEstoqueService();
		IgnUtil ignUtil = new IgnUtil();  
		
		String sqlQuery = "select operacao, objeto, idoperacao, nomeClasse from nepalignloja08.operacaomovimentacaoestoque where dataoperacao between '2014-10-10 00:00:00' and '2014-10-11 23:59:59'";
		 
		 
		Statement st = con.createStatement();
		Statement stAtualizacao = con.createStatement();
    	   
		ResultSet rs = st.executeQuery(sqlQuery);
		
		boolean hasOp = rs.next();
		
		if(!hasOp)
			Logger.getGlobal().info("No Pending OP's");
		else 
		{
			Logger.getGlobal().info("Found Pending OP's!");
			rs.beforeFirst();	
		}
		
		while (rs.next())
		{
			String operacao = rs.getString(1);
			byte[] objeto = rs.getBytes(2);
    		int idOperacaoMovimentacaoEstoque = rs.getInt(3);
    		String classe = rs.getString(4);
    		
    		//Logger.getGlobal().info("Handling OP " + idOperacaoMovimentacaoEstoque + " [" + operacao + "] to class " + classe);
    		
    		boolean handled = false;
    		
    		if(classe.equalsIgnoreCase(MovimentacaoEstoquePersistence.class.getName()))
    		{
        		MovimentacaoEstoque movimentacaoEstoque = (MovimentacaoEstoque) ignUtil.recuperarObjectoPorByteArray(objeto);
        		
        		movimentacaoEstoque.setUsuario(null);
        		movimentacaoEstoque.setForncedor(null);
        		movimentacaoEstoque.setCliente(null);
        		
        		if(movimentacaoEstoque.getNumeroDocumento() == null)
        			continue;
        		if(movimentacaoEstoque.getNumeroDocumento().equals(""))
        		{
        			System.out.println(idOperacaoMovimentacaoEstoque);
        			continue;
        		}
        		
        		if(movimentacaoEstoque.getNumeroDocumento().equals("1481016485310CV"))
        		{
        			System.out.println("Parou");
        		}
                
        		try
        		{
            		if (operacao != null && operacao.equalsIgnoreCase("SALVAR"))
            		    //movimentacaoEstoqueService.salvar(movimentacaoEstoque, false);
            			System.out.println("Salvar " + movimentacaoEstoque.getNumeroDocumento() + " " + movimentacaoEstoque.getId() + " " + idOperacaoMovimentacaoEstoque);
            			//System.out.println(movimentacaoEstoque.getNumeroDocumento());
            		else if (operacao != null && operacao.equalsIgnoreCase("ALTERAR"))
            			//movimentacaoEstoqueService.atualizar(movimentacaoEstoque, false)
            			System.out.println("Alterar " + movimentacaoEstoque.getNumeroDocumento() + " " + movimentacaoEstoque.getId() + " " + idOperacaoMovimentacaoEstoque);
            		else if (operacao != null && operacao.equalsIgnoreCase("EXCLUIR"))
            		    //movimentacaoEstoqueService.excluir(movimentacaoEstoque); 
            			System.out.println("Excluir " + movimentacaoEstoque.getNumeroDocumento() + " " + movimentacaoEstoque.getId() + " " + idOperacaoMovimentacaoEstoque);
            		
            		handled = true;
        		}
        		catch(Exception ex)
        		{
        			Logger.getGlobal().severe("Fail ME " + (movimentacaoEstoque == null ? "null" : movimentacaoEstoque.getId().toString()));
        		}
    		}
    		else if(classe.equalsIgnoreCase(ItemMovimentacaoEstoquePersistence.class.getName()))
    		{ 
    			//if(idOperacaoMovimentacaoEstoque == 8960)
    				//System.out.println("Parou"); 
        		ItemMovimentacaoEstoque itemMovimentacaoEstoque = (ItemMovimentacaoEstoque) ignUtil.recuperarObjectoPorByteArray(objeto);
         		//itemMovimentacaoEstoque.setMovimentacaoEstoque(movimentacaoEstoqueService.recuperarPorId(itemMovimentacaoEstoque.getMovimentacaoEstoque().getId()));
        		
        		//System.out.println(itemMovimentacaoEstoque.getEstoqueProduto().getCodigoBarras() + " " + itemMovimentacaoEstoque.getQuantidade() + " " + operacao + " " + idOperacaoMovimentacaoEstoque);
        		 
        		
        		try
        		{
        			/*if(operacao != null && operacao.equalsIgnoreCase("SALVAR"))
        				//itemMovimentacaoEstoqueService.salvar(itemMovimentacaoEstoque, false);
        				//System.out.println("Salvar");
            		else if (operacao != null && operacao.equalsIgnoreCase("ALTERAR"))
            			//itemMovimentacaoEstoqueService.atualizar(itemMovimentacaoEstoque, false);
            			//System.out.println("Alterar");
            		else if (operacao != null && operacao.equalsIgnoreCase("EXCLUIR"))
            			//itemMovimentacaoEstoqueService.excluir(itemMovimentacaoEstoque, false);
            			System.out.println("Excluir");*/
        			
        			handled = true;
        		}
        		catch(Exception ex)
        		{
        			Logger.getGlobal().severe("Fail on IME to ME " + (itemMovimentacaoEstoque.getMovimentacaoEstoque() == null ? "null" : itemMovimentacaoEstoque.getMovimentacaoEstoque().getId().toString()));
        		}
    		}
    		//else
    			//Logger.getGlobal().warning("Invalid OP Class : " + classe);

    		if(handled)
    		{
        		//stAtualizacao.executeUpdate("update nepalignloja"  + numeroEmpresaFisica + ".operacaomovimentacaoestoque set flagsincronizacao = 1 where idoperacao = " + idOperacaoMovimentacaoEstoque + ";");
        		
        		//Logger.getGlobal().info("OP Updated to Handled.");	
    		} 
    		//else
    			//Logger.getGlobal().info("Fail to Handle");
		}

		if(!hasOp)
			Logger.getGlobal().info("No more OP's to handle");
	} 
	
	private void infosByOPME2(String idEmpresaFisica, String dataInicial, String dataFinal) throws Exception 
	{ 
		DsvConstante.getParametrosSistema();
		 
		Connection con = ConexaoUtil.getConexaoPadrao();   
		ItemMovimentacaoEstoqueService itemMovimentacaoEstoqueService = new ItemMovimentacaoEstoqueService();
		MovimentacaoEstoqueService movimentacaoEstoqueService = new MovimentacaoEstoqueService(); 
		IgnUtil ignUtil = new IgnUtil();
		String numeroEmpresaFisica = String.format ("%02d", Integer.parseInt(idEmpresaFisica));
		String databaseName = "nepalignloja" + numeroEmpresaFisica;
		  
		String sqlQuery = "select operacao, objeto, idoperacao, nomeClasse from " + databaseName + ".operacaomovimentacaoestoque where dataoperacao between '" + dataInicial + " 00:00:00' and '" + dataFinal + " 23:59:59'";    
		  
		   
		Statement st = con.createStatement();  
		Statement stAtualizacao = con.createStatement();    
    	   
		ResultSet rs = st.executeQuery(sqlQuery);   
		
		boolean hasOp = rs.next(); 
		
		if(!hasOp) 
			Logger.getGlobal().info("No Pending OP's");  
		else  
		{  
			Logger.getGlobal().info("Found Pending OP's!");  
			rs.beforeFirst();	
		}
		
		stAtualizacao.executeUpdate("delete from " + databaseName + ".tmpitemmovimentacaoestoque;");
		stAtualizacao.executeUpdate("delete from " + databaseName + ".tmpmovimentacaoestoque;");
		
		while (rs.next()) 
		{
			String operacao = rs.getString(1); 
			byte[] objeto = rs.getBytes(2);
    		int idOperacaoMovimentacaoEstoque = rs.getInt(3);
    		String classe = rs.getString(4);
    		
    		boolean handled = false;
    		 
    		if(classe.equalsIgnoreCase(MovimentacaoEstoquePersistence.class.getName()))
    		{
        		MovimentacaoEstoque movimentacaoEstoque = (MovimentacaoEstoque) ignUtil.recuperarObjectoPorByteArray(objeto);
        		
        		tratarMovimentacao(databaseName, movimentacaoEstoque, operacao, idOperacaoMovimentacaoEstoque, stAtualizacao);
    		}
    		else if(classe.equalsIgnoreCase(ItemMovimentacaoEstoquePersistence.class.getName()))
    		{  
        		ItemMovimentacaoEstoque itemMovimentacaoEstoque = (ItemMovimentacaoEstoque) ignUtil.recuperarObjectoPorByteArray(objeto);
         		
        		StringBuilder sql = new StringBuilder();
        		sql.append(" INSERT INTO `" + databaseName + "`.`tmpitemmovimentacaoestoque` ");
        		sql.append(" (`idTmpItemMovimentacaoEstoque`, ");
        		sql.append(" `idEstoqueProduto`, ");
        		sql.append(" `idTmpMovimentacaoEstoque`, ");
        		sql.append(" `idUnidadeMedida`, ");
        		sql.append(" `idProduto`, ");
        		sql.append(" `idDocumentoOrigem`, ");
        		sql.append(" `sequencialItem`, ");
        		sql.append(" `quantidade`, ");
        		sql.append(" `valor`, ");
        		sql.append(" `custo`, ");
        		sql.append(" `numeroLote`, ");
        		sql.append(" `dataValidade`, ");
        		sql.append(" `dataFabricacao`, ");
        		sql.append(" `venda`) ");
        		sql.append(" VALUES ");
        		sql.append(" (" + itemMovimentacaoEstoque.getId() + ", ");
        		sql.append(" " + itemMovimentacaoEstoque.getEstoqueProduto().getId() + ", ");
        		sql.append(" " + itemMovimentacaoEstoque.getMovimentacaoEstoque().getId() + ", ");
        		sql.append(" " + (itemMovimentacaoEstoque.getUnidadeMedida() == null ? "null" : itemMovimentacaoEstoque.getUnidadeMedida().getId()) + ", ");
        		sql.append(" " + itemMovimentacaoEstoque.getProduto().getId() + ", ");
        		sql.append(" '" + (itemMovimentacaoEstoque.getDocumentoOrigem() == null ? "null" : itemMovimentacaoEstoque.getDocumentoOrigem()) + "', ");
        		sql.append(" " + itemMovimentacaoEstoque.getSequencialItem() + ", ");
        		sql.append(" " + itemMovimentacaoEstoque.getQuantidade() + ", ");
        		sql.append(" " + itemMovimentacaoEstoque.getValor() + ", ");
        		sql.append(" " + itemMovimentacaoEstoque.getCusto() + ", ");
        		sql.append(" " + (itemMovimentacaoEstoque.getNumeroLote() == null ? "null" : itemMovimentacaoEstoque.getNumeroLote()) + ", ");
        		sql.append(" '" + (itemMovimentacaoEstoque.getDataValidade() == null ? "2014-01-01 00:00:00" : new SimpleDateFormat().format(itemMovimentacaoEstoque.getDataValidade())) + "', ");
        		sql.append(" '" + (itemMovimentacaoEstoque.getDataFabricacao() == null ? "2014-01-01 00:00:00" : new SimpleDateFormat().format(itemMovimentacaoEstoque.getDataFabricacao())) + "', ");
        		sql.append(" " + itemMovimentacaoEstoque.getVenda() + "); ");
        		sql.append("  ");
        		sql.append("  ");
        		 
        		try
        		{
        			if(operacao != null && operacao.equalsIgnoreCase("SALVAR"))
        			{
        				tratarMovimentacao(databaseName, itemMovimentacaoEstoque.getMovimentacaoEstoque(), "SALVAR", idOperacaoMovimentacaoEstoque, stAtualizacao);
        				
        				//itemMovimentacaoEstoqueService.salvar(itemMovimentacaoEstoque, false);
        				stAtualizacao.executeUpdate(sql.toString());
        				System.out.println("Salvar");	
        			} 
            		else if (operacao != null && operacao.equalsIgnoreCase("ALTERAR"))
            			//itemMovimentacaoEstoqueService.atualizar(itemMovimentacaoEstoque, false);
            			System.out.println("Alterar");
            		else if (operacao != null && operacao.equalsIgnoreCase("EXCLUIR"))
            		{
            			stAtualizacao.executeUpdate("DELETE FROM " + databaseName + ".tmpitemmovimentacaoestoque where idtmpitemmovimentacaoestoque = " + itemMovimentacaoEstoque.getId());
            			//itemMovimentacaoEstoqueService.excluir(itemMovimentacaoEstoque, false);
            			System.out.println("Excluir");	
            		} 
        			
        			handled = true;
        		}
        		catch(Exception ex)
        		{
        			Logger.getGlobal().severe("Fail on IME to ME " + (itemMovimentacaoEstoque.getMovimentacaoEstoque() == null ? "null" : itemMovimentacaoEstoque.getMovimentacaoEstoque().getId().toString()) + " " + ex.getMessage());
        		}
    		}
    		//else
    			//Logger.getGlobal().warning("Invalid OP Class : " + classe);

    		if(handled)
    		{
        		//stAtualizacao.executeUpdate("update nepalignloja"  + numeroEmpresaFisica + ".operacaomovimentacaoestoque set flagsincronizacao = 1 where idoperacao = " + idOperacaoMovimentacaoEstoque + ";");
        		
        		//Logger.getGlobal().info("OP Updated to Handled.");	
    		}
    		//else
    			//Logger.getGlobal().info("Fail to Handle"); 
		}

		if(!hasOp)
			Logger.getGlobal().info("No more OP's to handle");
	}
	
	
	public boolean tratarMovimentacao(String databaseName, MovimentacaoEstoque movimentacaoEstoque, String operacao, Integer idOperacaoMovimentacaoEstoque, Statement stAtualizacao) throws Exception
	{
		StringBuilder sql = new StringBuilder();
		sql.append(" INSERT INTO `" + databaseName + "`.`tmpmovimentacaoestoque` ");
		sql.append(" (`idTmpMovimentacaoEstoque`, ");
		sql.append(" `idEmpresaFisica`, ");
		sql.append(" `idEstoque`, ");
		sql.append(" `idTipoMovimentacaoEstoque`, ");
		sql.append(" `idTipoDocumentoMovimentoEstoque`, ");
		sql.append(" `idCliente`, ");
		sql.append(" `idUsuario`, "); 
		sql.append(" `entradaSaida`, ");
		sql.append(" `numeroDocumento`, ");
		sql.append(" `dataMovimentacao`, ");
		sql.append(" `idDocumentoOrigem`, ");
		sql.append(" `observacao`, ");
		sql.append(" `faturado`, ");
		sql.append(" `manual`, ");
		sql.append(" `numeroVolume`, ");
		sql.append(" `peso`, ");
		sql.append(" `valorFrete`, ");
		sql.append(" `idFornecedor`, "); 
		sql.append(" `idStatusMovimentacaoEstoque`, ");
		sql.append(" `idEmpresaDestino`) ");
		sql.append(" VALUES "); 
		sql.append(" (" + movimentacaoEstoque.getId() + ", ");
		sql.append(" " + movimentacaoEstoque.getEmpresaFisica().getId() + ", ");
		sql.append(" " + movimentacaoEstoque.getEstoque().getId() + ", ");
		sql.append(" " + movimentacaoEstoque.getTipoMovimentacaoEstoque().getId() + ", ");
		sql.append(" " + movimentacaoEstoque.getTipoDocumentoMovimento().getId() + ", ");
		sql.append(" " + (movimentacaoEstoque.getCliente() == null ? "null" : movimentacaoEstoque.getCliente().getId()) + ", ");
		sql.append(" " + (movimentacaoEstoque.getUsuario() == null ? "null" : movimentacaoEstoque.getUsuario().getId()) + ", ");
		sql.append(" '" + movimentacaoEstoque.getEntradaSaida() + "', ");
		sql.append(" '" + movimentacaoEstoque.getNumeroDocumento() + "', ");
		sql.append(" '" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(movimentacaoEstoque.getDataMovimentacao()) + "', ");
		sql.append(" '" + (movimentacaoEstoque.getIdDocumentoOrigem() == null ? "0" : movimentacaoEstoque.getIdDocumentoOrigem()) + "', ");
		sql.append(" '" + movimentacaoEstoque.getObservacao() + "', ");
		sql.append(" '" + movimentacaoEstoque.getFaturado() + "', ");
		sql.append(" '" + movimentacaoEstoque.getManual() + "', ");
		sql.append(" '" + (movimentacaoEstoque.getNumeroVolume() == null ? "0" : movimentacaoEstoque.getNumeroVolume()) + "', ");
		sql.append(" " + movimentacaoEstoque.getPeso() + ", ");
		sql.append(" " + movimentacaoEstoque.getValorFrete() + ", ");
		sql.append(" " + (movimentacaoEstoque.getForncedor() == null ? "null" :  movimentacaoEstoque.getForncedor().getId()) + ", ");
		sql.append(" " + movimentacaoEstoque.getStatusMovimentacaoEstoque().getId() + ", ");
		sql.append(" " + (movimentacaoEstoque.getEmpresaDestino() == null ? "null" : movimentacaoEstoque.getEmpresaDestino().getId()) + "); ");
               
		try
		{
    		if (operacao != null && operacao.equalsIgnoreCase("SALVAR")) 
    		{
    		    //movimentacaoEstoqueService.salvar(movimentacaoEstoque, false); 
    			stAtualizacao.executeUpdate(sql.toString()); 
    			System.out.println("Salvar " + movimentacaoEstoque.getNumeroDocumento() + " " + movimentacaoEstoque.getId() + " " + idOperacaoMovimentacaoEstoque);
    		}
    			//System.out.println(movimentacaoEstoque.getNumeroDocumento());
    		else if (operacao != null && operacao.equalsIgnoreCase("ALTERAR"))
    			//movimentacaoEstoqueService.atualizar(movimentacaoEstoque, false)
    			System.out.println("Alterar " + movimentacaoEstoque.getNumeroDocumento() + " " + movimentacaoEstoque.getId() + " " + idOperacaoMovimentacaoEstoque);
    		else if (operacao != null && operacao.equalsIgnoreCase("EXCLUIR")) 
    		{
    		    //movimentacaoEstoqueService.excluir(movimentacaoEstoque);
    			stAtualizacao.executeUpdate("DELETE FROM " + databaseName + ".tmpmovimentacaoestoque where idtmpmovimentacaoestoque = " + movimentacaoEstoque.getId());
    			System.out.println("Excluir " + movimentacaoEstoque.getNumeroDocumento() + " " + movimentacaoEstoque.getId() + " " + idOperacaoMovimentacaoEstoque);
    		}
    		return true;
		}
		catch(MySQLIntegrityConstraintViolationException e)
		{
			System.out.println("já existia Movimentação");
			return false;
		}
		catch(Exception ex)
		{
			Logger.getGlobal().severe("Fail ME " + (movimentacaoEstoque == null ? "null" : movimentacaoEstoque.getId().toString()) + " " + ex.getMessage());
			return false;
		}
	}
}