package br.com.desenv.nepalign.service;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;
import java.lang.Boolean;
import java.lang.Number;

import javax.mail.Address;
import javax.mail.internet.InternetAddress;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.velocity.runtime.directive.Parse;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.*;

import br.com.desenv.frameworkignorante.Formatador;
import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.frameworkignorante.Page;
import br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao;
import br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao.Grupo;
import br.com.desenv.nepalign.alertas.EnviarEmail;
import br.com.desenv.nepalign.model.AtribuicaoCaractFiscais;
import br.com.desenv.nepalign.model.CaracteristicasFiscais;
import br.com.desenv.nepalign.model.Cfop;
import br.com.desenv.nepalign.model.Cliente;
import br.com.desenv.nepalign.model.Cstcofins;
import br.com.desenv.nepalign.model.Cstpis;
import br.com.desenv.nepalign.model.Duplicata;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.FormaPagamento;
import br.com.desenv.nepalign.model.GrupoProduto;
import br.com.desenv.nepalign.model.ImpostoEmpresa;
import br.com.desenv.nepalign.model.ImpostosTaxasTarifas;
import br.com.desenv.nepalign.model.ImpostosTaxasTarifasEstado;
import br.com.desenv.nepalign.model.ItemNotaFiscal;
import br.com.desenv.nepalign.model.ItemPedidoVenda;
import br.com.desenv.nepalign.model.Logprodutossemncm;
import br.com.desenv.nepalign.model.Mva;
import br.com.desenv.nepalign.model.Ncm;
import br.com.desenv.nepalign.model.NotaFiscal;
import br.com.desenv.nepalign.model.Observcaractfiscempresa;
import br.com.desenv.nepalign.model.PagamentoNotaFiscal;
import br.com.desenv.nepalign.model.PagamentoPedidoVenda;
import br.com.desenv.nepalign.model.ParamentrosFaturamento;
import br.com.desenv.nepalign.model.PedidoVenda;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.model.SituacaoNotaFiscal;
import br.com.desenv.nepalign.model.TipoLista;
import br.com.desenv.nepalign.model.TipoMovimentacaoEstoque;
import br.com.desenv.nepalign.model.TipoNotaCfop;
import br.com.desenv.nepalign.model.TipoNotaFiscal;
import br.com.desenv.nepalign.model.Uf;
import br.com.desenv.nepalign.persistence.NotaFiscalPersistence;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

import com.kscbrasil.lib.exception.KscException;
import com.sun.msv.writer.relaxng.Context;

public class NotaFiscalService extends GenericServiceIGN<NotaFiscal, NotaFiscalPersistence>
{
	HashMap observacaoNota; 
	Context context;
	Boolean pararProcesso =false;
	Logger log;
	
	public NotaFiscalService() 
	{
		
		super();
		observacaoNota = new HashMap(); 
		this.log = Logger.getGlobal();
	}

	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Buscar Nota Fiscal por Pedido Venda")
	public NotaFiscal buscarNotaFiscalPedidoVenda(PedidoVenda pedidoVenda) throws Exception
	{
		try
		{
			NotaFiscal notaFiscalCriterio = new NotaFiscal();
			notaFiscalCriterio.setPedidoVenda(pedidoVenda);
			NotaFiscal notaFiscalEncontrada = new NotaFiscal();
			List<NotaFiscal> listaNotaFiscal = new NotaFiscalService().listarPorObjetoFiltro(notaFiscalCriterio);

			if(listaNotaFiscal.size()>0)
				notaFiscalEncontrada = listaNotaFiscal.get(listaNotaFiscal.size()-1);

			return notaFiscalEncontrada;		   
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception("Erro ao tentar recuperar Notas Fiscais atraves deste Pedido Venda");
		}
	}
	
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Gerar Nota Fiscal Consuidor")
	public NotaFiscal gerarNotaFiscalConsumidor(NotaFiscal notaFiscal) throws Exception
	{
		
		EntityTransaction transaction = null;
		EntityManager manager = null;
		try
		{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				
				this.verificarTroca(notaFiscal);
				
				NotaFiscal nf = notaFiscal;
				/*
				if(notaFiscal.getId()!= null)
				{
					if(notaFiscal.getSituacaoNotaFiscal().getId() == DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalRejeitada() ||
							notaFiscal.getSituacaoNotaFiscal().getId() == DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalGerada())
						this.excluirNotaFiscal(notaFiscal);
				}
				*/
				nf = gerarNotaFiscalConsumidor(notaFiscal.getPedidoVenda(), notaFiscal, false, manager, transaction);
				
				transaction.commit();
				return nf;
		}
		catch(Exception ex)
		{
			transaction.rollback();
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if (manager!=null)
				manager.close();
		}		
		
		
	}
	
	public void verificarTroca(NotaFiscal notaFiscal)throws Exception
	{
		PedidoVenda pedidoVenda = new PedidoVendaService().recuperarPorId(notaFiscal.getPedidoVenda().getId());
		
		if(pedidoVenda.getTipoMovimentacaoEstoque().getId() == Integer.parseInt(DsvConstante.getParametrosSistema().get("idTipoMovimentacaoEstoqueTrocaEntrada"))
				|| pedidoVenda.getTipoMovimentacaoEstoque().getId() == Integer.parseInt(DsvConstante.getParametrosSistema().get("idTipoMovimentacaoEstoqueTrocaSaida")))
		{
			throw new Exception ("NÃ£o Ã© possÃ­vel gerar NFC de TROCA.");
		}
	}
	
	public void excluirNotaFiscal(NotaFiscal notaFiscal)throws Exception
	{
		boolean conexaoPropria = false;
		EntityTransaction transaction = null;
		EntityManager manager = null;
		try
		{
			if (manager==null)
			{
				conexaoPropria =true;
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
			}
			List<ItemNotaFiscal> listaItemNotaFiscal = new ItemNotaFiscalService().listarPaginadoSqlLivreGenerico(Integer.MAX_VALUE, 1, "idNotaFiscal = "+notaFiscal.getId()).getRecordList();
			new ItemNotaFiscalService().excluirListaItemNotaFiscal(listaItemNotaFiscal);
			new PagamentoNotaFiscalService().excluirListaPagamentoNotaFiscal(notaFiscal,manager,transaction);
			this.excluir(manager, transaction, notaFiscal);
			
			transaction.commit();
		}
		catch(Exception ex)
		{
			if (conexaoPropria==true && transaction!=null && transaction.isActive())
				transaction.rollback();
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if (manager!=null && conexaoPropria==true)
				manager.close();
		}
	}

	public NotaFiscal gerarNotaFiscalConsumidor(PedidoVenda pedidoVenda, NotaFiscal notaFiscal) throws Exception
	{
		
		EntityTransaction transaction = null;
		EntityManager manager = null;
		try
		{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				NotaFiscal nf = gerarNotaFiscalConsumidor(pedidoVenda, notaFiscal, false, manager, transaction);
				transaction.commit();
				return nf;
		}
		catch(Exception ex)
		{
			transaction.rollback();
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if (manager!=null)
				manager.close();
		}		
		
		
	}
	
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Gerar Nota Fiscal Consuidor")
	public NotaFiscal gerarNotaFiscalConsumidor(PedidoVenda pedidoVenda, NotaFiscal notaFiscal, Boolean flgValorAjustado, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		NotaFiscal notaFiscalRetornada = new NotaFiscal();
		boolean conexaoPropria = false;
		List<ItemPedidoVenda> listaItemPedidoVenda = null;
		List<PagamentoPedidoVenda> listaPagamentoPedidoVenda = null;
		try
		{
			if (manager==null)
			{
				conexaoPropria =true;
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
			}
			//quando o pedido de venda ï¿½ ajustado, precisamos pegar os itens que vem com o pedido.
			//Se for pedido normal, recuperamos da base.
			if (flgValorAjustado==false)
			{
				ItemPedidoVenda itemCriterio = new ItemPedidoVenda();
				itemCriterio.setPedidoVenda(pedidoVenda);
				listaItemPedidoVenda = new ItemPedidoVendaService().listarPorObjetoFiltro(itemCriterio);
				PagamentoPedidoVenda pagamentoCriterio = new PagamentoPedidoVenda();
				pagamentoCriterio.setPedidoVenda(pedidoVenda);
				listaPagamentoPedidoVenda = new PagamentoPedidoVendaService().listarPorObjetoFiltro(pagamentoCriterio);				
			}
			else
			{
				listaItemPedidoVenda = pedidoVenda.getListaItensPedidoVenda();
				listaPagamentoPedidoVenda = pedidoVenda.getListaPagamentosPedidoVenda();
			}
			

			
			notaFiscal.setModelo(65); //Consumidor
			TipoMovimentacaoEstoque tipoMovimentacao = new TipoMovimentacaoEstoque();
			tipoMovimentacao = new TipoMovimentacaoEstoqueService().recuperarPorId(manager,pedidoVenda.getTipoMovimentacaoEstoque().getId());
			if(tipoMovimentacao.getId() == 5)
				throw new Exception ("Nao e possivel emitir NFC-e de Troca.");
			else
				notaFiscalRetornada = gerarNotaFiscalBaseadoPedidoVenda(notaFiscal,pedidoVenda, listaItemPedidoVenda, listaPagamentoPedidoVenda, flgValorAjustado, manager, transaction);
	
			if (transaction!=null && conexaoPropria == true)
				transaction.commit();
				
			return notaFiscalRetornada;
		}
		catch(Exception ex)
		{
			if (conexaoPropria==true && transaction!=null && transaction.isActive())
				transaction.rollback();
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if (manager!=null && conexaoPropria==true)
				manager.close();
		}
		
	}
	
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Enviar Nota Fiscal Consuidor")
	public NotaFiscal enviarNotaFiscalNfeConsumidor(NotaFiscal notaFiscal) throws Exception
	{
		NotaFiscal notaRetorno = new NotaFiscal();
		int cont = 0;
		try
		{
			if(notaFiscal.getSituacaoNotaFiscal().getId() != DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalGerada())
				throw new Exception("Apenas notas fiscais na situacao 'GERADA' podem ser enviadas para o SEFAZ.");

			List<NotaFiscal> listaNotaFiscal = new ArrayList<>();
			listaNotaFiscal.add(notaFiscal);
			notaRetorno = enviarNotaFiscalNfe(listaNotaFiscal, null, null);
			
			while (notaRetorno.getSituacaoNotaFiscal().getId()==DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalEnviadaDfe()||
					notaRetorno.getSituacaoNotaFiscal().getId()==DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalImportadaDfe())
			{
				notaRetorno = new NotaFiscalService().recuperarPorId(notaRetorno.getId());
				Thread.sleep(2000); // aguarda 5segundos
				cont++;
				if(cont==10 || this.pararProcesso==true)
					break;
			}
			//notaFiscalRetornada = enviarNotaFiscalNfe(nota, null, null);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw(ex);
		}
		
		return notaRetorno;
	}
	
	public void gerarNotaFiscalBaseadoPedidoVenda(NotaFiscal notaFiscal) throws Exception
	{
		this.gerarNotaFiscalBaseadoPedidoVenda(notaFiscal, null, null);	
	}

	public NotaFiscal gerarNotaFiscalBaseadoPedidoVenda(NotaFiscal entidade, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		return gerarNotaFiscalBaseadoPedidoVenda(entidade,null, null, null, false, manager, transaction);
	}
	
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Gerar Nota Fiscal/PedidoVenda")
	public NotaFiscal gerarNotaFiscalBaseadoPedidoVenda(NotaFiscal entidade, PedidoVenda paramPedidoVenda, List<ItemPedidoVenda> paramItensPedido, 
															List<PagamentoPedidoVenda> paramPagamentos,Boolean flgValorAjustado, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		NotaFiscal notaFiscalPersistido = new NotaFiscal();

		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}

			try 
			{
				Integer tipoAmbienteNotaFiscal;
					
				try{
					tipoAmbienteNotaFiscal = Integer.parseInt(DsvConstante.getParametrosSistema().get("AMBIENTENOTAFISCAL"));	
				}
				catch(Exception ex){
					throw new Exception ("Nao foi possivel recuperar o parametro: AMBIENTENOTAFISCAL");
				}
				
				
				entidade.setTpAmb(tipoAmbienteNotaFiscal);
				
				PedidoVenda pedidoVenda = new PedidoVenda();
				PedidoVendaService pedidoVendaService = new PedidoVendaService();
				Produto produto;
				Produto produtoCriterio = new Produto();
				ProdutoService produtoService = new ProdutoService();
				CaracteristicasFiscais caracteristicasFiscais = new CaracteristicasFiscais();
				ArrayList listaItens = new ArrayList();

				double valorOutrasDespesas = 0;
				double valorCustoBoleto;
				ImpostosTaxasTarifasEstado icmsCliente = new ImpostosTaxasTarifasEstado();

				String saltoobservacao="/"+'\\'+"/";
				NotaFiscalService notaFiscalService = new NotaFiscalService();
				String observacaoAutomatica= "";

				if (paramPedidoVenda==null)
					pedidoVenda = pedidoVendaService.recuperarPorId(manager, entidade.getPedidoVenda().getId());
				else
					pedidoVenda = paramPedidoVenda;
					

				ParamentrosFaturamento parametrosFaturamento = new ParamentrosFaturamento();
				try{
					parametrosFaturamento = new ParamentrosFaturamentoService().recuperarParametroPorEmpresaFisica(pedidoVenda.getEmpresaFisica(), manager, transaction);
				}
				catch(Exception ex){
					throw new Exception ("Nenhum PARAMETRO FATURAMENTO cadastrado para esta empresa.");
				}
				
				entidade.setNumero(parametrosFaturamento.getNumeroUltimaNotaImpressa()+1);
				
				Cliente cliente = null;

				if(entidade.getModelo()==55)
				{
					cliente = (new ClienteService().recuperarPorId(manager, entidade.getCliente().getId()));
					if((cliente.getId()== DsvConstante.getParametrosSistema().getIdClienteConsumidor() || cliente == null))
						throw new Exception("CLIENTE NAO SELECIONADO.");
				}


				EmpresaFisica empresa = new EmpresaFisica();
				empresa = new EmpresaFisicaService().recuperarPorId(manager, entidade.getPedidoVenda().getEmpresaFisica().getId());
				
			
				try{
					valorCustoBoleto = DsvConstante.getParametrosSistema().getValorPorBoleto();
					//if(valorCustoBoleto == 0)
						//System.out.println("Custo do boleto cadastrado com o valor 0(zero)");
				}
				catch(Exception exBol)
				{	System.out.println("Custo do boleto nao cadastrado.");
				valorCustoBoleto=0;
				}

				//recupera informacoes de pagamento caso exista cobranï¿½a de boleto;
				if (valorCustoBoleto>0)
				{
					if (pedidoVenda !=null)
					{
						PagamentoPedidoVenda criterioPagamento = new PagamentoPedidoVenda() ;
						FormaPagamento formaPagamentoCriterio = new FormaPagamento();
						PagamentoPedidoVendaService pagamentoPedidoVendaService = new PagamentoPedidoVendaService();
						criterioPagamento.setPedidoVenda(pedidoVenda);
						formaPagamentoCriterio.setId(DsvConstante.getParametrosSistema().getIdFormaPagamentoBoleto());
						criterioPagamento.setFormaPagamento(formaPagamentoCriterio);
						ArrayList<PagamentoPedidoVenda> resultadoPagamento = (ArrayList<PagamentoPedidoVenda>)pagamentoPedidoVendaService.listarPorObjetoFiltro(manager,criterioPagamento,null,null,null);
						if (resultadoPagamento.size()>1)
						{
							valorOutrasDespesas = valorOutrasDespesas + (valorCustoBoleto*(resultadoPagamento.size()-1));
						}
					}
				}

				entidade.setCfop(new CfopService().recuperarPorId(manager,DsvConstante.getParametrosSistema().getIdCfopVenda()));
				TipoNotaFiscal tipoNotaFiscal = new TipoNotaFiscal();
				tipoNotaFiscal = entidade.getPedidoVenda().getTipoMovimentacaoEstoque().getTipoNotaFiscal();
				entidade.setTipoNotaFiscal(tipoNotaFiscal);
				entidade.setDataEmissao(new Date());
				entidade.setBaseCalculoIss(entidade.getBaseCalculoIss() == null ? new Double(0.0) : entidade.getBaseCalculoIss());

				entidade.setCifFob(entidade.getCifFob() == null ? new String("C"): entidade.getCifFob());
				


				try
				{
					if(entidade.getModelo().intValue() == DsvConstante.getParametrosSistema().getModeloNotaFiscalConsumidor())
					{
						icmsCliente = this.recuperaICMSEstado(manager,1, empresa.getCidade().getUf());
					}
					else if(cliente != null)
					{
						icmsCliente = this.recuperaICMSEstado(manager,1, cliente.getCidadeEndereco().getUf());
					}
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}

				//verificar o parametro de valor 1(icms)
				entidade.setSituacaoNotaFiscal(new SituacaoNotaFiscalService().recuperarPorId(manager, DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalGerada()));

				double valorMercadoria = 0.0;
				double valorTotalProdutos = 0.0;
				double valorTotalServico = 0.0;
				double valorTotalICMS = 0.0;
				double valorTotalBaseICMS = 0.0;
				double valorTotalICMSST = 0.0;
				double valorTotalBaseICMSST = 0.0;			
				double valorTotalCOFINS = 0.0;
				double valorTotalPIS = 0.0;
				double valorTotalCSLL = 0.0;
				double valorTotalIRRF = 0.0;
				double valorTotalCOFINSRet = 0.0;
				double valorTotalPISRet = 0.0;
				double valorTotalCSLLRet = 0.0;
				double valorTotalIRRFRet = 0.0;
				double valorTotalDesconto = 0.0;
				double isePrecoComICMS = 0.0;
				double iseDescisencao = 0.0;
				double isePrecoSemICMS = 0.0;
				double difIcmsNormal = 0.0;
				double difIcmsDesconto = 0.0;
				
				int sequenciaItem = 0;
				StringBuffer observacoesItem = new StringBuffer();
				List<ItemPedidoVenda> itensPedidoVenda= null;
				
				ItemPedidoVenda itemPedidoVenda = new ItemPedidoVenda();
				if (paramItensPedido!=null && paramItensPedido.size()>0){
					itensPedidoVenda = paramItensPedido;}
				else{
					itemPedidoVenda.setPedidoVenda(pedidoVenda);
					itensPedidoVenda = new ItemPedidoVendaService().listarPorObjetoFiltro(manager,itemPedidoVenda,null,null,null);
				}

				for(ItemPedidoVenda item: itensPedidoVenda)
				{
					if (flgValorAjustado==false)
						valorTotalProdutos = (valorTotalProdutos + (item.getPrecoVenda() * item.getQuantidade()));
					else
						valorTotalProdutos = (valorTotalProdutos + (item.getPrecoVendaAjustado()==null?0.0:item.getPrecoVendaAjustado() * item.getQuantidade()));
				}


				if (itensPedidoVenda.size() > 0)
				{
					entidade.setMovimentacaoEstoque(null);
					entidade.setEmpresa(pedidoVenda.getEmpresaFisica());
					entidade.setValorTotalProduto(valorTotalProdutos);
					entidade.setValorTotalServico(valorTotalServico);
					entidade.setDataFinalizacao(new Date());
					entidade.setDataSaidaEntrada(new Date());
					int horas = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
					entidade.setHoraSaidaEntrada(new Date(horas));


					TipoNotaCfop tipoNotaCfop = new TipoNotaCfop();
					Cfop cfop = new Cfop();
					Cfop cfopFiltro = new Cfop();

					cfopFiltro.setCodigo(tipoNotaFiscal.getCfopDentroEstado());
					tipoNotaCfop.setTipoNotaFiscal(new TipoNotaFiscalService().recuperarPorId(manager,tipoNotaFiscal.getId()));

					if(entidade.getModelo()==65)
					{
						cfop = new CfopService().listarPorObjetoFiltro(manager,cfopFiltro, null, null, null).get(0);
						entidade.setCfop(cfop);
					}
					else
					{
						if(cliente.getCidadeEndereco() == null)
							throw new Exception("Cliente sem cidade");
						
						if (entidade.getEmpresa().getCidade().getUf().getId().intValue() == cliente.getCidadeEndereco().getUf().getId())
						{
							if (tipoNotaCfop.getCfopForaEstado()==null)
								cfop = new CfopService().listarPorObjetoFiltro(manager,cfopFiltro, null,null,null).get(0);
							else
								cfop = new CfopService().listarPorObjetoFiltro(manager,cfopFiltro,null,null,null).get(0);

							entidade.setCfop(cfop);
						}
						else
						{
							if (tipoNotaCfop.getCfopForaEstado()==null)
								cfop = new CfopService().listarPorObjetoFiltro(manager,cfopFiltro,null,null,null).get(0);
							else
								cfop = new CfopService().listarPorObjetoFiltro(manager,cfopFiltro,null,null,null).get(0);

							entidade.setCfop(cfop);
						}
					}

					entidade.setStatusNfe(0);
					entidade.setDataRetorno(new Date());
					entidade.setStatusImpressao(0);
					entidade.setStatusCancelamento(0);
					entidade.setStatusContingencia(0);
					entidade.setSerie(DsvConstante.getParametrosSistema().getSerieNotaFiscal());

					Iterator<ItemPedidoVenda> i = itensPedidoVenda.iterator();

					int item = 0;

					while (i.hasNext())
					{
						itemPedidoVenda = (ItemPedidoVenda) i.next();
						ItemNotaFiscal itemNotaFiscal = new ItemNotaFiscal();
						Ncm ncmCriterio = new Ncm();
						List<Ncm> listaNcm =  new ArrayList<>();
						produtoCriterio.setId(itemPedidoVenda.getEstoqueProduto().getProduto().getId());
						produto = produtoService.listarPorObjetoFiltro(produtoCriterio).get(0);
						item++;
						
						if(produto.getCodigoNcm() == null || produto.getCodigoNcm().equals("") || (produto.getCodigoNcm().length() < 8 || produto.getCodigoNcm().trim().replaceAll("0", "").length()==0))
						{
							if(produto.getGrupo() != null)
							{
								if(produto.getGrupo().getNcm() != null)
								{
									Logprodutossemncm logProdutos = new Logprodutossemncm();
									logProdutos.setDataLog(new Date());
									logProdutos.setEmpresa(entidade.getEmpresa().getId());
									logProdutos.setNcmAntigo(produto.getCodigoNcm());
									logProdutos.setNcmNovo(produto.getGrupo().getNcm());
									logProdutos.setNumeroNotaFiscal(entidade.getNumero());
									logProdutos.setObservacao("NCM PRODUTO NULL. FOI ATUALIZADO COM O NCM DO GRUPO");
									logProdutos.setProduto(produto.getId());
									
									produto.setCodigoNcm(produto.getGrupo().getNcm());
									
									new LogprodutossemncmService().salvar(manager, transaction, logProdutos);	
								}
								else
								{
									Logprodutossemncm logProdutos = new Logprodutossemncm();
									logProdutos.setDataLog(new Date());
									logProdutos.setEmpresa(entidade.getEmpresa().getId());
									logProdutos.setNcmAntigo(produto.getCodigoNcm());
									logProdutos.setNcmNovo("64039990");
									logProdutos.setNumeroNotaFiscal(entidade.getNumero());
									logProdutos.setObservacao("NCM DO PRODUTO NULL. FOI ATUALIZADO COM NCM PADRAO");
									logProdutos.setProduto(produto.getId());
									
									produto.setCodigoNcm("64039990");
									
									new LogprodutossemncmService().salvar(manager, transaction, logProdutos);
								}
								
							}
							else
							{
								Logprodutossemncm logProdutos = new Logprodutossemncm();
								logProdutos.setDataLog(new Date());
								logProdutos.setEmpresa(entidade.getEmpresa().getId());
								logProdutos.setNcmAntigo(produto.getCodigoNcm());
								logProdutos.setNcmNovo("64039990");
								logProdutos.setNumeroNotaFiscal(entidade.getNumero());
								logProdutos.setObservacao("NCM DO PRODUTO NULL. FOI ATUALIZADO COM NCM PADRAO");
								logProdutos.setProduto(produto.getId());
								
								produto.setCodigoNcm("64039990");
								
								new LogprodutossemncmService().salvar(manager, transaction, logProdutos);
							}
						}
						else 
						{
							ncmCriterio.setNcm(produto.getCodigoNcm());
							listaNcm = new NcmService().listarPorObjetoFiltro(ncmCriterio);
							if(listaNcm.size() < 0)
							{
								if(produto.getGrupo() != null)
								{
									Logprodutossemncm logProdutos = new Logprodutossemncm();
									logProdutos.setDataLog(new Date());
									logProdutos.setEmpresa(entidade.getEmpresa().getId());
									logProdutos.setNcmAntigo(produto.getCodigoNcm());
									logProdutos.setNcmNovo(produto.getGrupo().getNcm());
									logProdutos.setNumeroNotaFiscal(entidade.getNumero());
									logProdutos.setObservacao("NCM PRODUTO INVALIDO. FOI ATUALIZADO COM O NCM DO GRUPO");
									logProdutos.setProduto(produto.getId());
							
									produto.setCodigoNcm(produto.getGrupo().getNcm());
									
									new LogprodutossemncmService().salvar(manager, transaction, logProdutos);
								}
								else
								{
									Logprodutossemncm logProdutos = new Logprodutossemncm();
									logProdutos.setDataLog(new Date());
									logProdutos.setEmpresa(entidade.getEmpresa().getId());
									logProdutos.setNcmAntigo(produto.getCodigoNcm());
									logProdutos.setNcmNovo("64039990");
									logProdutos.setNumeroNotaFiscal(entidade.getNumero());
									logProdutos.setObservacao("NCM DO PRODUTO INVALIDO. FOI ATUALIZADO COM NCM PADRAO");
									logProdutos.setProduto(produto.getId());
							
									produto.setCodigoNcm("64039990"); 
									
									new LogprodutossemncmService().salvar(manager, transaction, logProdutos);
							
								}
							}
							else
							{
								Logprodutossemncm logProdutos = new Logprodutossemncm();
								logProdutos.setDataLog(new Date());
								logProdutos.setEmpresa(entidade.getEmpresa().getId());
								logProdutos.setNcmAntigo(produto.getCodigoNcm());
								logProdutos.setNcmNovo("64039990");
								logProdutos.setNumeroNotaFiscal(entidade.getNumero());
								logProdutos.setObservacao("NCM DO PRODUTO INVALIDO. FOI ATUALIZADO COM NCM PADRAO");
								logProdutos.setProduto(produto.getId());
						
								produto.setCodigoNcm("64039990"); 
								
								new LogprodutossemncmService().salvar(manager, transaction, logProdutos);
							}
							
						}
						
						if(produto.getOrigemMercadoria() == null)
							produto.setOrigemMercadoria(new OrigemMercadoriaService().recuperarPorId(manager,1));
						
						new ProdutoService().atualizar(manager, transaction,produto);
						
						itemNotaFiscal.setSequenciaItem(item);
						int seqItNf = itemNotaFiscal.getSequenciaItem();					

						itemNotaFiscal.setNotaFiscal(entidade);
						itemNotaFiscal.setProduto(produto);
						itemNotaFiscal.setUnidadeMedida(itemPedidoVenda.getProduto().getUnidadeMedida());
						itemNotaFiscal.setLote("");
						itemNotaFiscal.setQuantidade(itemPedidoVenda.getQuantidade());
						itemNotaFiscal.setValidade(null);
						
						if (flgValorAjustado==false){
							itemNotaFiscal.setValorUnitario(itemPedidoVenda.getPrecoVenda());
							itemNotaFiscal.setValorTotal(itemPedidoVenda.getPrecoVenda()*itemPedidoVenda.getQuantidade());}
						else
						{
							try
							{
								itemNotaFiscal.setValorUnitario(itemPedidoVenda.getPrecoVendaAjustado());
								itemNotaFiscal.setValorTotal(itemPedidoVenda.getPrecoVendaAjustado()*itemPedidoVenda.getQuantidade());
							}
							catch(Exception ex)
							{
								System.out.println("Erro ao recuperar valor ajustado. Valor nï¿½o foi calculado.");
							}
						}
						
						itemNotaFiscal.setAliquotaICMS(0.0);
						itemNotaFiscal.setAliquotaIPI(0.0);
						itemNotaFiscal.setCodigoSituacaoTributaria(null);
						itemNotaFiscal.setOrigemMercadoria(null);
						itemNotaFiscal.setMargemLucro(0.0);
						itemNotaFiscal.setPercCofins(0.0);
						itemNotaFiscal.setPercCsll(0.0);
						itemNotaFiscal.setPercIrrf(0.0);
						itemNotaFiscal.setPercPis(0.0);
						itemNotaFiscal.setPercReducaoBaseCalc(0.0);
						itemNotaFiscal.setPMC(0.0);
						itemNotaFiscal.setValorBaseCalcICMS(0.0);
						itemNotaFiscal.setValorBaseCalcICMSST(0.0);
						itemNotaFiscal.setValorICMS(0.0);
						itemNotaFiscal.setValorICMSST(0.0);
						itemNotaFiscal.setValorDesconto(0.0);

						if(entidade.getValorSeguro()!=null && entidade.getValorSeguro()>0)
						{
							if (flgValorAjustado==false)
								itemNotaFiscal.setValorSeguro(this.calculaValorProporcionalItem(entidade.getValorSeguro(), itemPedidoVenda.getPrecoVenda() * itemPedidoVenda.getQuantidade(),valorTotalProdutos));
							else
								itemNotaFiscal.setValorSeguro(this.calculaValorProporcionalItem(entidade.getValorSeguro(), itemPedidoVenda.getPrecoVendaAjustado()==null?0.00:itemPedidoVenda.getPrecoVendaAjustado() * itemPedidoVenda.getQuantidade(),valorTotalProdutos));
						}
						else
							itemNotaFiscal.setValorSeguro(0.0);

						if(entidade.getValorFrete()!=null && entidade.getValorFrete()>0)
						{
							if (flgValorAjustado==false)
								itemNotaFiscal.setValorFrete(this.calculaValorProporcionalItem(entidade.getValorFrete(), itemPedidoVenda.getPrecoVenda() * itemPedidoVenda.getQuantidade(),valorTotalProdutos));
							else
								itemNotaFiscal.setValorFrete(this.calculaValorProporcionalItem(entidade.getValorFrete(), itemPedidoVenda.getPrecoVendaAjustado() * itemPedidoVenda.getQuantidade(),valorTotalProdutos));
						}
						else
							itemNotaFiscal.setValorFrete(0.0);
						//determina caracteristicas ficais


						caracteristicasFiscais = determinaCaracteriscaFiscal(manager,empresa, cliente, produto, tipoNotaFiscal, parametrosFaturamento, seqItNf,entidade.getModelo());
						itemNotaFiscal = calcularICMSProprio(empresa, cliente, produto, caracteristicasFiscais, entidade, itemNotaFiscal, icmsCliente, entidade.getModelo());


						if (caracteristicasFiscais.getDescontoImpostos()!=null && caracteristicasFiscais.getDescontoImpostos()==1)
						{
							//o valor do desconto sï¿½ servirï¿½ no caso da Fidare para calculo da isenï¿½ï¿½o por isso estou zerando ele apos a soma
							if (itemNotaFiscal.getValorDesconto()!=null && itemNotaFiscal.getValorDesconto()>0)						
							{
								isePrecoComICMS = isePrecoComICMS + itemNotaFiscal.getValorDesconto().doubleValue() + itemNotaFiscal.getValorTotal().doubleValue();  
								isePrecoSemICMS = isePrecoSemICMS + itemNotaFiscal.getValorTotal().doubleValue();
								iseDescisencao = iseDescisencao + itemNotaFiscal.getValorDesconto().doubleValue();
							}
						}
						else
						{
							if (itemNotaFiscal.getValorDesconto()!=null && itemNotaFiscal.getValorDesconto().doubleValue()>0)						
							{
								difIcmsNormal = itemNotaFiscal.getValorICMS().doubleValue()+itemNotaFiscal.getValorDesconto().doubleValue()+difIcmsNormal;
								difIcmsDesconto = itemNotaFiscal.getValorDesconto().doubleValue()+ difIcmsDesconto;
							}
						}
						
						itemNotaFiscal.setValorDesconto(new Double(0)); //<=== Zerando valor
						itemNotaFiscal = calcularICMSST(empresa, cliente, produto, caracteristicasFiscais, entidade, itemNotaFiscal, icmsCliente.getValorPercentual().doubleValue());
						itemNotaFiscal = calcularCOFINS(empresa, cliente, produto, caracteristicasFiscais, entidade, itemNotaFiscal, manager, transaction);
						itemNotaFiscal = calcularCSLL(empresa, cliente, produto, caracteristicasFiscais, entidade, itemNotaFiscal);
						itemNotaFiscal = calcularIPI(empresa, cliente, produto, caracteristicasFiscais, entidade, itemNotaFiscal);
						itemNotaFiscal = calcularIRRF(empresa, cliente, produto, caracteristicasFiscais, entidade, itemNotaFiscal);
						itemNotaFiscal = calcularPIS(empresa, cliente, produto, caracteristicasFiscais, entidade, itemNotaFiscal, manager, transaction);
						valorMercadoria = valorMercadoria + itemNotaFiscal.getValorTotal().doubleValue();
						valorTotalICMS = valorTotalICMS + itemNotaFiscal.getValorICMS().doubleValue();
						valorTotalBaseICMS = valorTotalBaseICMS + itemNotaFiscal.getValorBaseCalcICMS().doubleValue();
						valorTotalICMSST = valorTotalICMSST + itemNotaFiscal.getValorICMSST().doubleValue();
						valorTotalBaseICMSST = valorTotalBaseICMSST + itemNotaFiscal.getValorBaseCalcICMSST().doubleValue();					
						valorTotalPIS = valorTotalPIS + (itemNotaFiscal.getPercPis().doubleValue() * itemNotaFiscal.getValorTotal().doubleValue() /100);					
						valorTotalCOFINS = valorTotalCOFINS + (itemNotaFiscal.getPercCofins().doubleValue() * itemNotaFiscal.getValorTotal().doubleValue() /100);
						valorTotalCSLL = valorTotalCSLL + (itemNotaFiscal.getPercCsll().doubleValue() * itemNotaFiscal.getValorTotal().doubleValue() /100);
						valorTotalIRRF = valorTotalIRRF + (itemNotaFiscal.getPercIrrf().doubleValue() * itemNotaFiscal.getValorTotal().doubleValue() /100);
						valorTotalDesconto = valorTotalDesconto + itemNotaFiscal.getValorDesconto().doubleValue();

						if (caracteristicasFiscais.getExibirPisNota()==1)
							valorTotalPISRet = valorTotalPISRet + (itemNotaFiscal.getPercPis().doubleValue() * (itemNotaFiscal.getValorTotal().doubleValue()-itemNotaFiscal.getValorDesconto().doubleValue()) /100);

						if (caracteristicasFiscais.getExibirCofinsNota()==1)
							valorTotalCOFINSRet = valorTotalCOFINSRet + (itemNotaFiscal.getPercCofins().doubleValue() * (itemNotaFiscal.getValorTotal().doubleValue()-itemNotaFiscal.getValorDesconto().doubleValue()) /100);

						if (caracteristicasFiscais.getExibirIrrfNota()==1)
							valorTotalIRRFRet = valorTotalIRRFRet + (itemNotaFiscal.getPercIrrf().doubleValue() * (itemNotaFiscal.getValorTotal().doubleValue()-itemNotaFiscal.getValorDesconto().doubleValue()) /100);

						if (caracteristicasFiscais.getExibirCsllNota()==1)
							valorTotalCSLLRet = valorTotalCSLLRet + (itemNotaFiscal.getPercCsll().doubleValue() * (itemNotaFiscal.getValorTotal().doubleValue()-itemNotaFiscal.getValorDesconto().doubleValue()) /100);					

						sequenciaItem = sequenciaItem + 1;

						listaItens.add(itemNotaFiscal);

						
						
					}

					//O CFOP com a utilizaï¿½ï¿½o da Nota Fiscal eletronica ï¿½ por item, mas para nï¿½o deixar o campo CFOP da Nota com
					//informaï¿½ï¿½es invï¿½lida colocamos o CFOP do ultimo item na nota
					ItemNotaFiscal itemNota =(ItemNotaFiscal)listaItens.get(listaItens.size()-1);
					if (itemNota!=null){
						if (itemNota.getCodigoCFOP()!=null){
							Cfop cfopItem = new CfopService().recuperarPorId(manager,itemNota.getCfop().getId()); 
							//notaFiscal.setIdCFOP(cfopItem.getIdCFOP());
						}
					}
					if(entidade.getValorOutrasDespesas()==null)
					{
						entidade.setValorOutrasDespesas(0.0);
						valorOutrasDespesas += valorOutrasDespesas + entidade.getValorOutrasDespesas();
					}
					else
					{
						valorOutrasDespesas += valorOutrasDespesas + entidade.getValorOutrasDespesas();	
					}

					entidade.setValorOutrasDespesas(new Double(valorOutrasDespesas));
					entidade.setValorIcms(new Double(valorTotalICMS));
					entidade.setBaseCalculoIcms(new Double(valorTotalBaseICMS));
					entidade.setValorIcmsSubstituicao(new Double(valorTotalICMSST));
					entidade.setBaseCalculoIcmsSubstituicao(new Double(valorTotalBaseICMSST));				
					entidade.setValorTotalCofins(new Double(valorTotalCOFINS));
					entidade.setValorTotalPis(new Double(valorTotalPIS));
					entidade.setValorTotalCsll(new Double(valorTotalCSLL));
					entidade.setValorTotalIrrf(new Double(valorTotalIRRF));
					entidade.setValorRetencaoCofins(new Double(valorTotalCOFINSRet));
					entidade.setValorRetencaoPis(new Double(valorTotalPISRet));
					entidade.setValorRetencaoCsll(new Double(valorTotalCSLLRet));
					entidade.setValorRetencaoIrrf(new Double(valorTotalIRRFRet));
					entidade.setValorDescontoTotal(new Double(valorTotalDesconto));
					entidade.setValorTotalIpi(entidade.getValorTotalIpi() == null ? new Double(0.0) : entidade.getValorTotalIpi());
					entidade.setValorIss(entidade.getValorIss() == null ? new Double(0.0): entidade.getValorIss());
					entidade.setValorIcmsSubstituicao(entidade.getValorIcmsSubstituicao() == null ? new Double(0.0): entidade.getValorIcmsSubstituicao());
					entidade.setValorDescontoTotal(entidade.getValorDescontoTotal() == null ? new Double(0.0) : entidade.getValorDescontoTotal());
					//entidade.setValorOutrasDespesas(entidade.getValorOutrasDespesas() == null ? new Double(0.0) :  entidade.getValorOutrasDespesas());
					entidade.setValorSeguro(entidade.getValorSeguro() == null ? new Double(0.0) :  entidade.getValorSeguro());
					entidade.setValorFrete(entidade.getValorFrete() == null ? new Double(0.0) : entidade.getValorFrete());

					entidade.setValorTotalNota(new Double(valorTotalProdutos + entidade.getValorFrete().doubleValue() + entidade.getValorSeguro().doubleValue() + 
							entidade.getValorIcmsSubstituicao().doubleValue() + entidade.getValorTotalIpi().doubleValue() + entidade.getValorOutrasDespesas().doubleValue() - entidade.getValorDescontoTotal().doubleValue())); 
					//valorTotalCOFINSRet - valorTotalPISRet - valorTotalCSLLRet - valorTotalIRRFRet));
					entidade.setValorTotalProduto(valorTotalProdutos); //perguntar

					entidade.setValorTotalContaReceber(new Double(valorTotalProdutos + entidade.getValorFrete().doubleValue() + entidade.getValorSeguro().doubleValue() + 
							entidade.getValorIcmsSubstituicao().doubleValue() + entidade.getValorTotalIpi().doubleValue() + entidade.getValorOutrasDespesas().doubleValue() - entidade.getValorDescontoTotal().doubleValue() - valorTotalCOFINSRet - valorTotalPISRet - valorTotalCSLLRet - valorTotalIRRFRet));				

					//listaItens.add(0, new Integer(listaItens.size()));	
					
					//Calcula os Impostos (De olho no imposto) - Mun, Est e Fed.
					calcularImposto(manager, entidade);

					if(entidade.getObservacao() != null && (!entidade.getObservacao().equals("")))
						observacaoAutomatica = entidade.getObservacao();

					String observacaoDeOlhoNoImposto = "";
					int cont = 0;
					try{
						if(entidade.getModelo() == 65)
						{
							if(entidade.getValorImpostoFederal() != null && entidade.getValorImpostoFederal() > 0 )
							{
								if(cont == 0)
									observacaoDeOlhoNoImposto = "Trib aprox R$: " + String.valueOf(Formatador.round(entidade.getValorImpostoFederal(), 2)).replace(".", ",") + " Federal";
								else
									observacaoDeOlhoNoImposto = entidade.getValorImpostoFederal()+ " Federal, ";
								cont++;
								
							}
							if(entidade.getValorImpostoEstadual() != null && entidade.getValorImpostoEstadual() > 0 )
							{
								if(cont == 0)
									observacaoDeOlhoNoImposto += "Trib aprox R$: " + entidade.getValorImpostoEstadual()+ " Estadual";
								else
									observacaoDeOlhoNoImposto += ", " + entidade.getValorImpostoEstadual()+ " Estadual";	
								cont++;
							}
							if(entidade.getValorImpostoMunicipal() != null && entidade.getValorImpostoMunicipal() > 0 )
							{
								if(cont == 0)
									observacaoDeOlhoNoImposto += "Trib aprox R$: " + entidade.getValorImpostoMunicipal()+ " Municipal";
								else
									observacaoDeOlhoNoImposto += ", " + entidade.getValorImpostoMunicipal()+ " Municipal";	
							}
							
							entidade.setObservacao(observacaoDeOlhoNoImposto);
						}
						else
						{
							if((observacaoAutomatica != null) && (!observacaoAutomatica.equals("")))
								observacaoAutomatica = observacaoAutomatica + parametrosFaturamento.getObservacaoPadraoNotaSaida();
							else
								observacaoAutomatica += observacaoAutomatica + parametrosFaturamento.getObservacaoPadraoNotaSaida();	
						}
					}
					catch(Exception e){
						e.printStackTrace();}

					observacoesItem.append(transfObsString());

					String obsCompl = "";
					//Gera observacao de retenï¿½ï¿½o na Nota:
					if (caracteristicasFiscais.getExibirIrrfNota()==1)
					{
						obsCompl = "RETENÇÕES: PIS: " + entidade.getValorRetencaoPis() + 
								" COFINS: " + entidade.getValorRetencaoCofins() +
								" IRRF: " + entidade.getValorRetencaoIrrf() +
								" CSLL: " + entidade.getValorRetencaoCsll() + " " + saltoobservacao;
					}
					if (iseDescisencao>0)
					{
						obsCompl = obsCompl+" PRECO COM ICMS: " + new Double(isePrecoComICMS) +
								" DESC.ISENCAO: " + new Double(iseDescisencao) + 
								" PRECO SEM ICMS: " + new Double(isePrecoSemICMS)+saltoobservacao; 
					}
					if (difIcmsNormal>0)
					{
						obsCompl = obsCompl+" VALOR DO ICMS NORMAL: " + new Double(difIcmsNormal) +
								" VALOR ICMS DIFERIDO: " + new Double(difIcmsDesconto) + saltoobservacao; 
					}
					//informaï¿½ï¿½o de DAS ME ou EPP
					double das = 0;
					try
					{
						das = (DsvConstante.getParametrosSistema().getPercDas());
						if (das>0)
						{
							double valorDas = das * entidade.getValorTotalNota().doubleValue() /100;
							obsCompl =  obsCompl + " Documento emitido por ME ou EPP optante pelo simples nacional, não gera direito a crédito de iss e ip." +
									"Permite o aproveitamento do crédito de icms no valor de " + new Double(valorDas) + ", correspondente alíquota de " + das + 
									"% nos termos do art. 23 da LC 123.";
						}
					}
					catch(Exception ex)
					{
						ex.printStackTrace();//
					}
					String strObsFinal="";
					
					if(entidade.getModelo() != 65)
					{
						if(!observacaoAutomatica.equals(""))
							strObsFinal += (observacaoAutomatica + " / ").replace('�', 'c');
						if(!observacoesItem.equals(""))
							strObsFinal += (observacoesItem.toString()).replace('�', 'C').replace('�', 'C') + " / " + obsCompl;
						
						strObsFinal = strObsFinal+" C:" + pedidoVenda.getNumeroControle();
						strObsFinal = strObsFinal.replaceAll("/N", " ");
						//entidade.setObservacoesComplementares(new String(strObsFinal));				
						entidade.setObservacao(strObsFinal);
	
					}
					
					//campos novos
					entidade.setUf(empresa.getCidade().getUf());
					entidade.setFinalidade(new Integer(1));
					entidade.setSituacao(new Integer(0));
					
					

					ItemNotaFiscalService itemNotaFiscalService = new ItemNotaFiscalService();

					notaFiscalPersistido = notaFiscalService.salvar(manager, transaction, entidade);
					
					System.out.println("NF GERADA NUM: " + entidade.getNumero()+" *** VALOR: "+entidade.getValorTotalNota());
					
			
					for (int j = 0;j<listaItens.size();j++) 
					{
						ItemNotaFiscal itemNota2 = (ItemNotaFiscal)listaItens.get(j);
						itemNotaFiscalService.salvarItemPorPedidoVenda(manager, transaction, itemNota2);

					}

					//atualiza numeracao da nota
					parametrosFaturamento.setNumeroUltimaNotaImpressa(entidade.getNumero());
					new ParamentrosFaturamentoService().atualizar(manager, transaction, parametrosFaturamento);

					if(entidade.getModelo()== DsvConstante.getParametrosSistema().getModeloNotaFiscalConsumidor())
					{
						List<PagamentoNotaFiscal> listaPagamentoNotaFiscal = new ArrayList<>();
						//Gera Pagamento NotaFiscal Automï¿½tico
						
						Duplicata duplicataCriterio = new Duplicata();
						duplicataCriterio.setPedidoVenda(entidade.getPedidoVenda());
						List<Duplicata> listaDuplicata = new DuplicataService().listarPorObjetoFiltro(manager,duplicataCriterio,null,null,null);

						if(listaDuplicata.size() > 0)
						{
							listaPagamentoNotaFiscal = new PagamentoNotaFiscalService().gerarPagamentoNotaFiscalPorDuplicataPedidoVenda(listaDuplicata, entidade, manager, transaction);
						}
						else
						{
							//ATEï¿½ï¿½O: Para o caso de nota fiscal com complemento, o flag de valor ajustado aqui deve ser false
							//Quando existir uma nota ajustada, mas sem complemento o flag deve ser true
							//pois quando tem complemento o valor final nï¿½o muda e quando nï¿½o tem o valor final pode ser menor
							listaPagamentoNotaFiscal = new PagamentoNotaFiscalService().gerarPagamentoNotaFiscalPorPagamentoPedidoVenda(entidade.getPedidoVenda(), entidade, paramPagamentos,false, manager, transaction);
						}
					}
					else
					{
						//Gera Pagamento NotaFiscal Automï¿½tico
						new PagamentoNotaFiscalService().salvarPagamentoNotaFiscalAutomatico(entidade.getId(), manager, transaction);
					}
				}
				else
				{
					throw new Exception("Nao existem itens no Documento para gerar a nota fiscal.");
				}


			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				throw(ex);
			}

			if(transacaoIndependente)
				transaction.commit();

			return notaFiscalPersistido;
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}

	private ItemNotaFiscal calcularCOFINS(EmpresaFisica empresa, Cliente cliente, Produto produto, CaracteristicasFiscais caractFiscais, NotaFiscal notaFiscal, ItemNotaFiscal itemNota, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		try
		{
			double valorCofins = 0;
			double aliqCofins = 0;
			if (caractFiscais.getCofins()!= null){
				if (caractFiscais.getCofins().doubleValue()>0){
					aliqCofins = caractFiscais.getCofins().doubleValue();
					valorCofins = aliqCofins * itemNota.getValorTotal().doubleValue() /100;
				}
			}

			itemNota.setPercCofins(aliqCofins);
			Cstcofins cstCriterio = new Cstcofins();
			cstCriterio.setId(caractFiscais.getCstcofins().getId());
			Cstcofins cst = new Cstcofins();
			try
			{
				cst = new CstcofinsService().listarPorObjetoFiltro(manager, cstCriterio, null, null, null).get(0);
			}
			catch(Exception excst)
			{
				excst.printStackTrace();
				throw new Exception("Erro ao recuperar CST do COFINS. IdCST: " + cst.getId() + " Produto: " + produto.getCodigoProduto());
			}
			itemNota.setCstCofins(cst.getCodigo());
			itemNota.setCodigoSituacaoTributariaCOFINS(cst);
		}
		catch (Exception e)
		{
			throw e;
		}		
		return itemNota;
	}
	private ItemNotaFiscal calcularCSLL(EmpresaFisica empresa, Cliente cliente, Produto produto, CaracteristicasFiscais caractFiscais, NotaFiscal notaFiscal, ItemNotaFiscal itemNota) throws Exception
	{
		try
		{
			double valorCsll = 0;
			double aliqCsll = 0;
			if (caractFiscais.getExibirCsllNota()==1)
			{
				if (caractFiscais.getCsll()!=null){
					if (caractFiscais.getCsll().doubleValue()>0){
						aliqCsll = caractFiscais.getCsll().doubleValue();
						valorCsll = aliqCsll * itemNota.getValorTotal().doubleValue() /100;
					}
				}
			}

			itemNota.setPercCsll(new Double(aliqCsll));
		}
		catch (Exception e)
		{
			throw e;
		}		
		return itemNota;
	}
	private ItemNotaFiscal calcularIRRF(EmpresaFisica empresa, Cliente cliente, Produto produto, CaracteristicasFiscais caractFiscais, NotaFiscal notaFiscal, ItemNotaFiscal itemNota) throws Exception
	{
		try
		{
			double valorIrrf = 0;
			double aliqIrrf = 0;
			if (caractFiscais.getExibirIrrfNota()==1)
			{
				if (caractFiscais.getIrrf()!=null){
					if (caractFiscais.getIrrf().doubleValue()>0){
						aliqIrrf = caractFiscais.getIrrf().doubleValue();
						valorIrrf = aliqIrrf * itemNota.getValorTotal().doubleValue() /100;
					}
				}
			}

			itemNota.setPercIrrf(new Double(aliqIrrf));
		}
		catch (Exception e)
		{
			throw e;
		}			
		return itemNota;
	}
	private ItemNotaFiscal calcularIPI(EmpresaFisica empresa, Cliente cliente, Produto produto, CaracteristicasFiscais caractFiscais, NotaFiscal notaFiscal, ItemNotaFiscal itemNota) throws Exception 
	{
		try
		{
			double valorIpi = 0;
			double aliqIpi = 0;
			if (caractFiscais.getIpi()!=null){
				if (caractFiscais.getIpi().doubleValue()>0){
					aliqIpi = caractFiscais.getIpi().doubleValue();
					valorIpi = aliqIpi * itemNota.getValorTotal().doubleValue() /100;
				}
			}

			itemNota.setAliquotaIPI(new Double(aliqIpi));
		}
		catch (Exception e)
		{
			throw e;
		}			
		return itemNota;
	}	

	private ItemNotaFiscal calcularPIS(EmpresaFisica empresa, Cliente cliente, Produto produto, CaracteristicasFiscais caractFiscais, NotaFiscal notaFiscal,ItemNotaFiscal itemNota, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		try
		{
			double valorPis = 0.0;
			double aliqPis = 0.0;
			if (caractFiscais.getPis()!=null){
				if (caractFiscais.getPis().doubleValue()>0){
					aliqPis = caractFiscais.getPis().doubleValue();
					valorPis = aliqPis * itemNota.getValorTotal().doubleValue() /100;
				}
			}

			itemNota.setPercPis(new Double(aliqPis));
			Cstpis cstPis = new Cstpis();
			cstPis.setId(caractFiscais.getCstpis().getId());
			try
			{
				cstPis = new CstpisService().recuperarPorId(cstPis.getId());
			}
			catch(Exception excst)
			{
				excst.printStackTrace();
				throw new Exception("Erro ao recuperar CST do PIS");
			}
			itemNota.setCstPis(cstPis.getCodigo());
			itemNota.setCodigoSituacaoTributariaPIS(cstPis);
		}
		catch (Exception e)
		{
			throw e;
		}

		return itemNota;
	}

	private ItemNotaFiscal calcularICMSST(EmpresaFisica empresa, Cliente cliente, Produto produto, CaracteristicasFiscais caractFiscais, NotaFiscal notaFiscal, ItemNotaFiscal itemNota, double icmsEstadoCliente) throws Exception
	{

		double valorBaseICMS_ST = 0;
		double valorICMSST = 0;
		double valorReducaoBaseCalc = 0;
		double pmc = 0;
		double mva =0;
		double percRedBaseCalcST = 0;
		double valorBaseICMS = itemNota.getValorTotal().doubleValue() + itemNota.getValorFrete().doubleValue() + itemNota.getValorSeguro().doubleValue();		
		try
		{
			if (caractFiscais.getSuspCalcSt() == 1)
			{
				//if (produto.getSubstitutoTributario().booleanValue()==true && cliente.getContribuinteIcms().booleanValue()==true)
				if(produto.getSubstitutoTributario() != null && produto.getSubstitutoTributario().equals("1"))
				{
					/*if ((!produto.getPmc17().isDbNull()) && produto.getPmc17().getValue().doubleValue()>0){
					valorBaseICMS_ST = (produto.getPmc17().doubleValue() * itemNota.getQuantidade().doubleValue()) + itemNota.getValorFrete().doubleValue() + itemNota.getValorSeguro().doubleValue();;
					pmc = produto.getPmc17().doubleValue();
				}
				else{
					 */
					double valorMva = valorBaseICMS * caractFiscais.getMva().doubleValue() / 100;
					valorBaseICMS_ST = valorBaseICMS + valorMva;
					mva = caractFiscais.getMva().doubleValue();

					//caso exista reduï¿½ï¿½o da base de calculo
					if (caractFiscais.getReducaoBCIcmsST()!=null)
					{
						if (caractFiscais.getReducaoBCIcmsST().doubleValue()>0){
							valorReducaoBaseCalc = valorBaseICMS_ST * caractFiscais.getReducaoBCIcmsST().doubleValue() / 100;
							valorBaseICMS_ST = valorBaseICMS_ST - valorReducaoBaseCalc;
							percRedBaseCalcST = caractFiscais.getReducaoBCIcmsST().doubleValue();
						}
					}
					valorICMSST = (valorBaseICMS_ST * caractFiscais.getObjetoMva().getIcmsDestino().doubleValue() / 100) - itemNota.getValorICMS().doubleValue(); 

					if (valorICMSST<0)
					{
						valorICMSST = 0;
						System.out.println("ICMS ST menor que zero.");
					}

				}
				else
				{
					//System.out.println("O Calculo de SUBSTITUICAO TRIBUTARIA foi suspenso pela ATRIBUICAO DA CARACTERISTICA.");
				}
			}
			itemNota.setPMC(new Double(pmc));
			itemNota.setMargemLucro(new Double(mva));
			itemNota.setValorBaseCalcICMSST(new Double(valorBaseICMS_ST));
			itemNota.setValorICMSST(new Double(valorICMSST));
			itemNota.setPercReducaoBaseCalcST(new Double(percRedBaseCalcST));
			return itemNota;
		}
		catch(Exception e)
		{
			throw e;
		}		
	}

	//@icms proprio @retenï¿½ï¿½o reduï¿½ï¿½o de BC
	private ItemNotaFiscal calcularICMSProprio(EmpresaFisica empresa, Cliente cliente, Produto produto, CaracteristicasFiscais caractFiscais, NotaFiscal notaFiscal, ItemNotaFiscal itemNota, ImpostosTaxasTarifasEstado icmsEmpresa, int modeloNF) throws Exception
	{
		double aliqICMS = 0;
		double valorReducaoBaseCalc = 0;
		double valorBaseICMS = 0;
		double valorICMS = 0;

		Integer idEstadoEmpresa = empresa.getCidade().getUf().getId();

		if(modeloNF == DsvConstante.getParametrosSistema().getModeloNotaFiscalConsumidor())
		{
			if(produto.getAliquotaIcms() != null)
				aliqICMS = produto.getAliquotaIcms(); //Verificar o valor a campo
		}
		else if ((idEstadoEmpresa.intValue()==cliente.getCidadeEndereco().getUf().getId().intValue()))
		{
			aliqICMS = produto.getAliquotaIcms(); //Verificar o valor a campo
		}
		else 
		{/*
				if (cliente.getContribuinteIcms().booleanValue()==false) //Verificar o campo ContribuinteIcms
				{
					aliqICMS = produto.getAliquotaIcmsSaida().doubleValue();
				}
				//interestadual
				else
				{
					aliqICMS = icmsEmpresa.getValorPercentualInter();
				}
			 */
			aliqICMS = icmsEmpresa.getValorPercentualInter();
		}

	//sempre que for dentro do estado independemente de Atribuiï¿½ï¿½o de caracterista fiscal o valor do ICMS vai ser o cadastrado no produto
		
		// se for fora do estado e o cliente for nï¿½o contribuiten tambï¿½m busca a aliquota do produto
		
		//Em ultimo caso, para assumir as caracteriscas ficais utiliza esta regra
		if (caractFiscais.getIcms()!=null)
			aliqICMS = caractFiscais.getIcms();		

		//caso exista desoneracao de imposto
		if ((caractFiscais.getDescontoImpostos()!=null) && caractFiscais.getDescontoImpostos()==1)
		{
			double percRet = (100 - aliqICMS)/100;
			double valorBase = (itemNota.getValorUnitario() * itemNota.getQuantidade()) + itemNota.getValorFrete() + itemNota.getValorSeguro(); 
			double novoValorProduto = valorBase / percRet;
			System.out.println("Existe desoneração. A aliquota de ICMS é: " + aliqICMS + ". O valor base é: " + valorBase + ".");
			//alterado para a Fidare pois o valor dos produtos jï¿½ vem sem ICMS.
			//itemNota.setValorUnitario(new KscDouble(novoValorProduto/itemNota.getQuantidade().doubleValue()));
			//itemNota.setValorTotal(new KscDouble(novoValorProduto));
			itemNota.setAliquotaICMS(new Double(0));
			valorBaseICMS = novoValorProduto;
			valorICMS = valorBaseICMS * aliqICMS / 100;
			itemNota.setValorDesconto(new Double(valorICMS));
			//valorBaseICMS = 0;
			//valorICMS = 0;
			//
			//aliqICMS = 0;
		}
		else
		{
			int tipoBcReduzida  = 0;	
			try	{
				if (DsvConstante.getParametrosSistema().getMostrarBaseCalcReduzida()==1){
					tipoBcReduzida = 1;}
				else{
					tipoBcReduzida = 0;}}
			catch(Exception bcRed)	{
				tipoBcReduzida = 0;}	

			//caso 0 nao mostra BC Reduzida e o %Reducao 'e aplicado diretamento a BC 
			if (tipoBcReduzida==0)
			{
				//caso exista reduï¿½ï¿½o da base de calculo
				if (caractFiscais.getReducaoBCIcms()!=null){
					if (caractFiscais.getReducaoBCIcms()>0){
						itemNota.setPercReducaoBaseCalc(caractFiscais.getReducaoBCIcms());
						valorReducaoBaseCalc = itemNota.getValorTotal() * caractFiscais.getReducaoBCIcms() / 100;
					}
				}
				//aplica percentual
				//alteracao para a Fidare
				//valorBaseICMS = itemNota.getValorTotal().doubleValue() + itemNota.getValorFrete().doubleValue() + itemNota.getValorSeguro().doubleValue() - valorReducaoBaseCalc;
				valorBaseICMS = itemNota.getValorTotal() + itemNota.getValorFrete() + itemNota.getValorSeguro();
				//valorICMS = valorBaseICMS * aliqICMS / 100;	
				valorICMS = (itemNota.getValorTotal() + itemNota.getValorFrete() + itemNota.getValorSeguro() - valorReducaoBaseCalc) * aliqICMS / 100;
				itemNota.setValorDesconto(new Double(0));
				if (valorReducaoBaseCalc>0)
				{
					double valorNormalICMS = valorBaseICMS  * aliqICMS / 100;
					itemNota.setValorDesconto(new Double(valorNormalICMS-valorICMS));
				}	
			}
			//caso 1 mostra BC reduzida e % 'e o resto da subtracao do percentual 'e aplicado na base 			
			//ex: 33.33 = 100 - 33.33 = 76.77 -> Este sera o percentual aplicado
			//caso 0 nao mostra BC Reduzida e o %Reducao 'e aplicado diretamento a BC 

			else if (tipoBcReduzida==1)
			{
				//LOJAS ESTAO CAINDO AQUI
				//caso exista reduï¿½ï¿½o da base de calculo
				if (caractFiscais.getReducaoBCIcms()!=null){
					if (caractFiscais.getReducaoBCIcms()>0){
						itemNota.setPercReducaoBaseCalc(caractFiscais.getReducaoBCIcms());
						valorReducaoBaseCalc = itemNota.getValorTotal() * (100 - caractFiscais.getReducaoBCIcms()) / 100;
					}
				}
				//aplica percentual
				valorBaseICMS = itemNota.getValorTotal() + itemNota.getValorFrete() + itemNota.getValorSeguro() - valorReducaoBaseCalc;
				valorBaseICMS = IgnUtil.roundUpDecimalPlaces(valorBaseICMS, 2);
				
				//valorBaseICMS = itemNota.getValorTotal().doubleValue() + itemNota.getValorFrete().doubleValue() + itemNota.getValorSeguro().doubleValue();
				//valorICMS = valorBaseICMS * aliqICMS / 100;	
				valorICMS = (itemNota.getValorTotal() + itemNota.getValorFrete() + itemNota.getValorSeguro() - valorReducaoBaseCalc) * aliqICMS / 100;
				valorICMS = IgnUtil.roundUpDecimalPlaces(valorICMS, 2);
				itemNota.setValorDesconto(new Double(0));
				if (valorReducaoBaseCalc>0)
				{
					double valorNormalICMS = valorBaseICMS  * aliqICMS / 100;
					itemNota.setValorDesconto(new Double(valorNormalICMS-valorICMS));
				}	
			}

		}

		//Para os casos da Fidare toda vez que o ICMS for zerado zero a base de calculo tb
		if (valorICMS==0)
			valorBaseICMS = 0;

		itemNota.setValorICMS(new Double(valorICMS));
		itemNota.setValorBaseCalcICMS(new Double(valorBaseICMS));
		itemNota.setAliquotaICMS(new Double(aliqICMS));
		itemNota.setCodigoSituacaoTributaria(caractFiscais.getCodigoSituacaoTributaria());		

		//determina CFOP do Item
		if(modeloNF == DsvConstante.getParametrosSistema().getModeloNotaFiscalConsumidor())
		{
			Cfop cfopCriterio = new Cfop();
			cfopCriterio = new CfopService().recuperarPorId(DsvConstante.getParametrosSistema().getIdCfopVenda());
			itemNota.setCodigoCFOP(""+cfopCriterio.getCodigo());
			itemNota.setCfop(cfopCriterio);
			//se o produto for sujeito a situacao tributaria e o sujeito for nao contribuinte
			//oeracao nao sujeita a substituicao tributaria. Mercadoria destinada a nao contribuinte.
		}
		else
		{
			if (idEstadoEmpresa.intValue() == cliente.getCidadeEndereco().getUf().getId().intValue())
			{
				
				if (caractFiscais.getCfop()!=null)
				{
					//CFOP cfop = new CFOP();
					//cfop.setIdCFOP(caractFiscais.getIdCFOP());
					//cfop = (CFOP) new CFOPService().consultarCFOP(cfop, "").get(1);
					itemNota.setCodigoCFOP(""+caractFiscais.getCfop().getCodigo());
					itemNota.setCfop(caractFiscais.getCfop());
				}
				
				//else if (cliente.getContribuinteIcms().booleanValue()==false && notaFiscal.getEntradaSaida().intValue() == 0)
				else
				{
					Cfop cfopCriterio = new Cfop();
					cfopCriterio = new CfopService().recuperarPorId(DsvConstante.getParametrosSistema().getIdCfopVenda());
					itemNota.setCodigoCFOP(""+cfopCriterio.getCodigo());
					itemNota.setCfop(cfopCriterio);
					//se o produto for sujeito a situacao tributaria e o sujeito for nao contribuinte
					//oeracao nao sujeita a substituicao tributaria. Mercadoria destinada a nao contribuinte.
				}
			}
			else
			{

				if (caractFiscais.getCfopForaUf()!=null)
				{
					itemNota.setCodigoCFOP(""+caractFiscais.getCfop().getCodigo());
					itemNota.setCfop(caractFiscais.getCfop());
				}
				//nao contribuinte 6108 - setado internamente
				//else if (cliente.getContribuinteIcms().booleanValue()==false && notaFiscal.getEntradaSaida().intValue() == 0)
				else
				{
					Cfop cfopCriterio = new CfopService().recuperarPorId(DsvConstante.getParametrosSistema().getIdCfopVendaNaoContribuinte());
					itemNota.setCodigoCFOP(""+cfopCriterio.getCodigo());
					itemNota.setCfop(cfopCriterio);
					//se o produto for sujeito a situacao tributaria e o sujeito for nao contribuinte
					//oeracao nao sujeita a substituicao tributaria. Mercadoria destinada a nao contribuinte.
				}			
			}
		}
		
		return itemNota;
	}

	private ImpostosTaxasTarifasEstado recuperaICMSEstado(EntityManager manager,Integer codigoICMS , Uf estado) throws Exception
	{
		ImpostosTaxasTarifas criterioIcms = new ImpostosTaxasTarifas();
		criterioIcms.setCodigo(codigoICMS);
		ImpostosTaxasTarifas icms = null;
		
		
		try{
			icms = new ImpostosTaxasTarifasService().listarPorObjetoFiltro(manager,criterioIcms,null,null,null).get(0);}
		catch(Exception e){
			throw new Exception("ICMS nao pode ser localizado na tabela de IMPOSTOS. Cadastre o ICMS com cï¿½digo 1.");}

		//Recupera ICMS do estado da empresa
		ImpostosTaxasTarifasEstado criterioIcmsEstado = new ImpostosTaxasTarifasEstado();
		criterioIcmsEstado.setUf(estado);
		try {
			criterioIcmsEstado.setImpostostaxastarifas(new ImpostosTaxasTarifasService().recuperarPorId(manager,icms.getId()));}
		catch (Exception e1) {
			e1.printStackTrace();}

		ImpostosTaxasTarifasEstado icmsEstado = null;
		try {
			icmsEstado = new ImpostosTaxasTarifasEstadoService().listarPorObjetoFiltro(manager,criterioIcmsEstado,null,null,null).get(0);}
		catch(Exception e)
		{ throw new Exception("Tabela do ICMS do estado: "+ estado.getNome()+" nao pode ser encontrado. ");}

		return icmsEstado;
	}

	private double calculaValorProporcionalItem(double valorTotal, double valorTotalItem, double valorTotalPedido)
	{
		double valorProporcional = 0;	
		valorProporcional = (valorTotalItem/valorTotalPedido) * valorTotal;
		return valorProporcional;
	}

	public CaracteristicasFiscais determinaCaracteriscaFiscal(EntityManager manager,EmpresaFisica empresa, Cliente cliente, Produto produto, TipoNotaFiscal tipoNota, ParamentrosFaturamento parametros, int seqItem, int modeloNF) throws Exception, KscException
	{
		//System.out.println(produto.getNomeProduto());
		CaracteristicasFiscais c = this.retornaCaracteristicasPadrao(manager,empresa, cliente, produto, tipoNota, parametros,modeloNF); 
		CaracteristicasFiscais cm = (CaracteristicasFiscais) this.determinaCaracteristicaFiscalMandatoria(manager,empresa, cliente, produto, tipoNota, c, seqItem, modeloNF).get(0);
		return cm;
	}

	public ArrayList determinaCaracteristicaFiscalMandatoria(EntityManager manager, EmpresaFisica empresa, Cliente cliente, Produto produto, TipoNotaFiscal tipoNota, CaracteristicasFiscais caracteristicasFiscaisMaster, int seqItem, int modeloNF) throws Exception
	{
		//determina a caracteristica fiscal a ser utilizada de acordo com a atribuiï¿½ï¿½o

		ArrayList resultado=null;
		try
		{
			//icms padrao - utilizado no caso de Retenï¿½ï¿½o de impostos onde ele deve considerar o ICMS padrï¿½o determinado pela caracteristica fiscal Inicial
			double icmsPadrao;
			if (caracteristicasFiscaisMaster.getIcms() != null)
				icmsPadrao = caracteristicasFiscaisMaster.getIcms();
			else{
				ImpostosTaxasTarifasEstado icmsEstadoEmpresa = recuperaICMSEstado(manager,1, empresa.getCidade().getUf());
				icmsPadrao = icmsEstadoEmpresa.getValorPercentual();
			}
			CaracteristicasFiscais caractFiscais = new CaracteristicasFiscais();
			CaracteristicasFiscais criterioCaract = new CaracteristicasFiscais();
			CaracteristicasFiscaisService caracFiscaisService = new CaracteristicasFiscaisService();
			AtribuicaoCaractFiscais atribCaractFiscais = new AtribuicaoCaractFiscais();


			//Hashtable hashObs = new Hashtable();

			int i;

			//busca caracteristicas ficais de acordo com sua hierarquia
			//recupera da mais fraca para a mais forte

			for (i=13;i>0;i--)
			{			
				//se atrib diferente de NULL recupera Caracterista fiscal
				atribCaractFiscais = buscaAtribuicao(manager,empresa, cliente, produto, tipoNota, caracteristicasFiscaisMaster, i,modeloNF);
				if (atribCaractFiscais!=null)
				{
					criterioCaract = new CaracteristicasFiscais();
					criterioCaract.setId(atribCaractFiscais.getCaracteristicasFiscais().getId());
					criterioCaract.setAtivo(1);
					resultado = (ArrayList)caracFiscaisService.listarPorObjetoFiltro(manager,criterioCaract,null,null,null);

					if (resultado.size()>0){
						caractFiscais = (CaracteristicasFiscais) resultado.get(0);
						//System.out.println(caractFiscais.getDescricao());
						if (!(caractFiscais.getUf()==null) && (caractFiscais.getUf().getId() != cliente.getCidadeEndereco().getUf().getId()))
						{
							System.out.print(" - Ignorado devido ao Estado da Caracteristica.");
						}
						else
						{
							caracteristicasFiscaisMaster = comparaEAtribuiCaracateristicasFiscais(manager,caracteristicasFiscaisMaster, caractFiscais, empresa, icmsPadrao, seqItem);
						}

					}
					else{
						System.out.println("Caracteristica fiscal atrelada a atribuicao nao foi encontrada.");
					}			
				}
			}
			//determinas e MVA e' necessario
			if (produto.getSubstitutoTributario()=="1" && (cliente.getRgInscricaoEstadual()=="" || cliente.getRgInscricaoEstadual()==null)) //subst. por IE
			{
				//KscDouble aliqInter = caracteristicasFiscaisMaster.getIcms();
				Double aliqInter = new Double(12);
				if (empresa.getCidade().getUf().getId().intValue() == cliente.getCidadeEndereco().getUf().getId().intValue())
				{
					aliqInter = new Double(0);
				}
				Mva mva = recuperaMVADestino(aliqInter, cliente.getCidadeEndereco().getUf(), empresa, produto.getTipoLista());
				if(mva.getPercMva()!=0)
				{
					caracteristicasFiscaisMaster.setMva(mva.getPercMva());
					caracteristicasFiscaisMaster.setObjetoMva(mva);
				}
				else
				{
					//mva.setSignatario(new Integer(1));
					mva.setPercMva(caracteristicasFiscaisMaster.getMva());
					mva.setIcmsDestino(new Double(12));
					caracteristicasFiscaisMaster.setObjetoMva(mva);
				}

				//if (mva.getSignatario()==0)
				//caracteristicasFiscaisMaster.setSuspCalcSt(new Integer(0));

				System.out.println("Aliq. def. para rec. MVA: " + aliqInter);
			}
			//para qualquer caso onde a operacao nao seja sujeita a substituicao tributaria o CST do ICMS vai ser o padrao isto ï¿½ 00;
			//else if (produto.getSubstitutoTributario()=="1" && cliente.getContribuinteIcms().booleanValue()==false) //se IE for "" ou null
			else if(produto.getSubstitutoTributario()=="1" && (cliente.getRgInscricaoEstadual()=="" || cliente.getRgInscricaoEstadual()==null))
			{
				caracteristicasFiscaisMaster.setMva(new Double(0.0));
				adicionaObservacao(manager,"OPERACAO NAO SUJEITA A SUBSTITUICAO TRIBUTARIA. MERCADORIA DESTINADA A NAO CONTRIBUINTE", seqItem);
				if (caracteristicasFiscaisMaster.getCodigoSituacaoTributaria().getId() != DsvConstante.getParametrosSistema().getIdCodigoSituacaoTributariaIsenta() )
				{
					caracteristicasFiscaisMaster.setCodigoSituacaoTributaria(new CodigoSituacaoTributariaService().recuperarPorId(manager,DsvConstante.getParametrosSistema().getIdCodigoSituacaoTributariaPadrao()));
				}
				//se o produto for sujeito a situacao tributaria e o sujeito for nao contribuinte
				//Operacao nao sujeita a substituicao tributaria. Mercadoria destinada a nao contribuinte.				
			}


			//retorna a caracteristica
			resultado = new ArrayList();
			//resultado.add(0, new Integer(1));
			resultado.add(caracteristicasFiscaisMaster);

			return resultado;
			//marlus@solucoesinfor.com.br
		}
		catch(Exception e)
		{
			throw e;
		}	
	}

	private Mva recuperaMVADestino(Double aliqInter, Uf estadoCliente, EmpresaFisica empresa, TipoLista tipoLista) throws Exception
	{
		Mva mva = new Mva();

		mva.setEmpresaFisica(empresa);
		mva.setUf(estadoCliente);
		mva.setAliqInterEstadual(aliqInter);
		mva.setTipoLista(tipoLista);

		ArrayList resultado = (ArrayList) new MvaService().listarPorObjetoFiltro(mva);

		if (resultado.size()>1)
			mva = (Mva) resultado.get(0);
		else
		{
			//mva.setSignatario(1);
			mva.setPercMva(new Double(0));
			mva.setIcmsDestino(new Double(18));
			//throw new Exception("MVA nao foi encontrado");
		}

		System.out.println("Mva: " + mva.getPercMva());

		return mva;
	}
	private CaracteristicasFiscais comparaEAtribuiCaracateristicasFiscais(EntityManager manager, CaracteristicasFiscais origem, CaracteristicasFiscais mandatoria, EmpresaFisica empresa, double icmsPadrao, int seqItem) throws Exception
	{

		try
		{			
			if (mandatoria.getAplicCofins()==1)
			{
				origem.setCofins(mandatoria.getCofins());
				origem.setCstcofins(mandatoria.getCstcofins());
			}
			if (mandatoria.getAplicCsll()==1)
			{
				origem.setCsll(mandatoria.getCsll());			
			}
			if (mandatoria.getAplicIcms()==1)
			{
				if (!(mandatoria.getDescontoImpostos()==null) && mandatoria.getDescontoImpostos()==0)
				{
					origem.setIcms(mandatoria.getIcms());
					if (!(mandatoria.getCodigoSituacaoTributaria()==null)){
						origem.setCodigoSituacaoTributaria(mandatoria.getCodigoSituacaoTributaria());}
				}
				else if (mandatoria.getDescontoImpostos()==null)
				{
					origem.setIcms(mandatoria.getIcms());
					if (!(mandatoria.getCodigoSituacaoTributaria()==null)){
						origem.setCodigoSituacaoTributaria(mandatoria.getCodigoSituacaoTributaria());}
				}
				else
				{
					origem.setIcms(icmsPadrao);
					System.out.println("AplicaÃ§Ã£o de ICMS na Caracterï¿½stica Fiscal anulada. MÃ©todo: comparaEAtribuiCaracateristicasFiscais");
				}
			}
			if ((!(mandatoria.getAplicCodigoSitTrib()==null)) && mandatoria.getAplicCodigoSitTrib()==1)
			{
				origem.setCodigoSituacaoTributaria(mandatoria.getCodigoSituacaoTributaria());
			}
			if (mandatoria.getAplicIdCfop()==1)
			{
				origem.setCfop(mandatoria.getCfop());			
			}
			if (mandatoria.getAplicCfopForaUf()==1)
			{
				origem.setCfopForaUf(mandatoria.getCfopForaUf());
			}
			if (mandatoria.getAplicIpi()==1)
			{
				origem.setIpi(mandatoria.getIpi());
				origem.setCstipi(mandatoria.getCstipi());			
			}
			if (mandatoria.getAplicIRRF()==1)
			{
				origem.setIrrf(mandatoria.getIrrf());			
			}
			if (mandatoria.getAplicMva()==1)
			{
				origem.setMva(mandatoria.getMva());
			}
			if (mandatoria.getAplicPis()==1)
			{
				origem.setPis(mandatoria.getPis());
				origem.setCstpis(mandatoria.getCstpis());
			}
			if (mandatoria.getAplicReducaoBCIcms()==1)
			{
				origem.setReducaoBCIcms(mandatoria.getReducaoBCIcms());
			}
			if (mandatoria.getAplicReducaoBCIcmsST()!=null && !(mandatoria.getAplicReducaoBCIcmsST()==null))
			{
				if (mandatoria.getAplicReducaoBCIcmsST()==1)
				{
					origem.setReducaoBCIcmsST(mandatoria.getReducaoBCIcmsST());
				}
			}
			if (!(mandatoria.getDescontoImpostos()==null))
			{
				origem.setDescontoImpostos(mandatoria.getDescontoImpostos());
			}
			if (mandatoria.getExibirCofinsNota()!=null)
			{
				if (mandatoria.getExibirCofinsNota()==1)
				{
					origem.setExibirCofinsNota(mandatoria.getExibirCofinsNota());
				}
			}
			if (mandatoria.getExibirCsllNota()!= null)
			{
				if (mandatoria.getExibirCsllNota()==1)
				{
					origem.setExibirCsllNota(mandatoria.getExibirCsllNota());
				}
			}
			if (mandatoria.getExibirIrrfNota()!=null)
			{
				if (mandatoria.getExibirIrrfNota()== 1)
				{
					origem.setExibirIrrfNota(mandatoria.getExibirIrrfNota());
				}
			}
			if (mandatoria.getExibirPisNota()!=null)
			{
				if (mandatoria.getExibirPisNota()==1)
				{
					origem.setExibirPisNota(mandatoria.getExibirPisNota());
				}
			}
			if (!(mandatoria.getSuspCalcSt()==null))
			{
				origem.setSuspCalcSt(mandatoria.getSuspCalcSt());
			}

			//trata obervaï¿½ï¿½es TAGS: <ICM>, <IPI>, <PIS>, <COF>, <SST>, <OUT>

			//origem.setObservacao(new KscString(mandatoria.getObservacao().getViewValue() + "\n" + origem.getObservacao().getViewValue()));
			String obs = retornaObservacaoEspecificaEmpresa(manager,mandatoria, empresa);

			adicionaObservacao(manager,obs, seqItem);
			System.out.println(origem.getObservacao());
		}
		catch(Exception e)
		{
			throw e;
		}

		return origem;
	}

	private String retornaObservacaoEspecificaEmpresa(EntityManager manager, CaracteristicasFiscais caracFiscais, EmpresaFisica empresa) throws Exception
	{
		String retorno = "";

		Observcaractfiscempresa crit = new Observcaractfiscempresa();
		crit.setCaracteristicasFiscais(caracFiscais);
		crit.setEmpresa(empresa);
		ArrayList arr = (ArrayList) new ObservcaractfiscempresaService().listarPorObjetoFiltro(manager,crit,null,null,null);

		if (arr.size()>1){	
			retorno = ((Observcaractfiscempresa) arr.get(0)).getObservacao();
		}
		else
			retorno = caracFiscais.getObservacao();

		return retorno;
	}

	private void adicionaObservacao(EntityManager manager,String observacao, int item)
	{
		String obs = "";
		String[] arTags = {"<ICM>", "<IPI>", "<PIS>", "<COF>", "<SST>", "<OUT>"};
		boolean possuiTag = false;
		String tag;

		if (observacao!=null)
		{
			for (int j=0;j<arTags.length;j++)
			{
				tag = arTags[j];
				if (observacao.indexOf(tag)>-1)
				{
					possuiTag = true;
					int inicio = observacao.indexOf(tag)+5;
					String tagFim = tag.substring(0,1) + "/" + tag.substring(1);
					int fim  = observacao.indexOf(tagFim);
					obs = observacao.substring(inicio,fim);
					System.out.println(tag + " " + obs);
					adicionaObservacao(manager,tag, obs, item);
				}
			}
			if (possuiTag==false && observacao.length()>0)
			{
				System.out.println("<OUT>" + " " + observacao);
				adicionaObservacao(manager,"<OUT>", observacao, item);
			}
		}
	}

	private void adicionaObservacao(EntityManager manager,String tag, String observacao, int item)
	{
		HashMap divisoes = null;
		ArrayList itens = null;;

		if (observacaoNota.containsKey(tag))
		{
			divisoes = (HashMap) observacaoNota.get(tag);
			if (divisoes.containsKey(observacao))
			{
				itens = (ArrayList) divisoes.get(observacao);
			}
			else
			{
				itens = new ArrayList();
				divisoes.put(observacao, itens);
			}
		}
		else
		{
			divisoes = new HashMap();
			itens = new ArrayList();
			observacaoNota.put(tag, divisoes);
			divisoes.put(observacao, itens);
		}
		itens.add(new Integer(item));
	}


	private CaracteristicasFiscais retornaCaracteristicasPadrao(EntityManager manager,EmpresaFisica empresa, Cliente cliente, Produto produto, TipoNotaFiscal tipoNota, ParamentrosFaturamento parametros, int modeloNF) throws Exception, KscException
	{
		CaracteristicasFiscais c = new CaracteristicasFiscais();
		ImpostosTaxasTarifasEstado icmsEstadoEmpresa = new ImpostosTaxasTarifasEstado();
		ImpostosTaxasTarifasEstado icmsEstadoCliente = new ImpostosTaxasTarifasEstado();

		
		if(modeloNF == 65)
		{
			icmsEstadoCliente = recuperaICMSEstado(manager,1, empresa.getCidade().getUf());
		}
			
		else if(cliente != null)
		{
			//Busca ICMS Estado Cliente
			icmsEstadoCliente = recuperaICMSEstado(manager,1, cliente.getCidadeEndereco().getUf());
		}

		//Busca ICMS Estado Empresa
		icmsEstadoEmpresa = recuperaICMSEstado(manager,1, empresa.getCidade().getUf());
		//Busca EstadoEmpresa
		Integer idEstadoEmpresa = empresa.getCidade().getUf().getId();


		try
		{
			//c.setCofins(produto.getPercCofins());
			c.setCofins(new Double(0.0));
			c.setCstcofins(new CstcofinsService().recuperarPorId(manager,Integer.parseInt(DsvConstante.getParametrosSistema().get("idCstCofinsPadrao")))); //Saber qual ï¿½ o Cofins Padrï¿½o
			Cstcofins cstCofins = new Cstcofins();
			try
			{
				cstCofins = new CstcofinsService().listarPorObjetoFiltro(c.getCstcofins()).get(0);
			}
			catch(Exception e)
			{
				throw new KscException("Nao foi possivel recuperar CST do COFINS.");
			}
		
			//c.setCsll(produto.getPercCSLL());
			c.setCsll(new Double(0.0));
			c.setDescontoImpostos(0); 
			c.setExibirCofinsNota(0);//descontar do valor total 
			c.setExibirCsllNota(0);  //descontar do valor total
			c.setExibirIrrfNota(0);  //descontar do valor total
			c.setExibirPisNota(0);   //descontar do valor total
			c.setAplicReducaoBCIcmsST(0);
			c.setSuspCalcSt(new Integer(1));

			if(modeloNF == DsvConstante.getParametrosSistema().getModeloNotaFiscalConsumidor())
			{
				c.setIcms(parametros.getAliquotaICMSPadrao());
				c.setCodigoSituacaoTributaria(new CodigoSituacaoTributariaService().recuperarPorId(manager,DsvConstante.getParametrosSistema().getIdCodigoSituacaoTributariaPadrao())); //Verficar qual ï¿½ o CST Padrï¿½o
				Cfop cfop = new CfopService().recuperarPorId(manager,tipoNota.getCfopDentroEstado());   

				c.setCfop(cfop);
				c.setCfopForaUf(null);
				c.setReducaoBCIcms(0.0);
				c.setMva(0.0);
				c.setSuspCalcSt(1);
			}
			//icms
			else if ((idEstadoEmpresa.intValue() == cliente.getCidadeEndereco().getUf().getId().intValue()))
			{
				c.setIcms(0.0);
				c.setCodigoSituacaoTributaria(new CodigoSituacaoTributariaService().recuperarPorId(manager,DsvConstante.getParametrosSistema().getIdCodigoSituacaoTributariaPadrao())); //Verficar qual ï¿½ o CST Padrï¿½o
				Cfop cfop = new CfopService().recuperarPorId(manager,tipoNota.getCfopDentroEstado());   

				c.setCfop(cfop);
				c.setCfopForaUf(null);
				c.setReducaoBCIcms(0.0);
				c.setMva(0.0);
				c.setSuspCalcSt(1);
			}
			else
			{
				//if (cliente.getContribuinteIcms().booleanValue()==false) //Verificar este campo
				if(cliente.getRgInscricaoEstadual() == null || cliente.getRgInscricaoEstadual() == "")	
					c.setIcms(icmsEstadoEmpresa.getValorPercentual());  //valorPercentualIter
				else
					c.setIcms(icmsEstadoEmpresa.getValorPercentualInter());  //valorPercentualIter

				c.setCodigoSituacaoTributaria(new CodigoSituacaoTributariaService().recuperarPorId(manager,DsvConstante.getParametrosSistema().getIdCodigoSituacaoTributariaPadrao()));

				Cfop cfop = new CfopService().recuperarPorId(manager,tipoNota.getCfopForaEstado());  
				c.setCfop(null);
				c.setCfopForaUf(cfop);
				c.setReducaoBCIcms(0.0);
				c.setMva(0.0);
				c.setSuspCalcSt(1);
			}
	
			
				
			c.setCstipi(null); //c.setIdCstipi("00")
			c.setIpi(null);//c.setIpi(produto.getPerIPI); 

			c.setCstpis(new CstpisService().recuperarPorId(manager,Integer.parseInt(DsvConstante.getParametrosSistema().get("idCstPisPadrao"))));
			Cstpis cstPis = new Cstpis();
			cstPis.setId(c.getCstpis().getId());
			try
			{
				cstPis = new CstpisService().listarPorObjetoFiltro(manager,cstPis,null,null,null).get(0);
			}
			catch(Exception e)
			{
				throw new Exception("Nao foi possivel recuperar CST do COFINS.");
			}
			c.setCstpis(cstPis);			


			c.setPis(null);//c.setPis(produto.getPercPISSocial());

			c.setUf(empresa.getCidade().getUf());
			c.setSubCodigoSituacaoTributaria(new SubCodigoSituacaoTributariaService().recuperarPorId(manager,DsvConstante.getParametrosSistema().getIdCodigoSituacaoTributariaPadrao()));
			c.setIrrf(null);//c.setIrrf(produto.getPerIR()); 
			c.setObservacao(parametros.getObservacaoPadraoNotaSaida());


			return c;

		}
		catch(Exception e)
		{
			throw e;
		}

	}

	private AtribuicaoCaractFiscais buscaAtribuicao(EntityManager manager, EmpresaFisica empresa, Cliente cliente, Produto produto, TipoNotaFiscal tipoNota, CaracteristicasFiscais caracteristicasFiscaisMaster, int tipo, int modelo) throws Exception
	{
		AtribuicaoCaractFiscais atribCaractFiscais = new AtribuicaoCaractFiscais();
		AtribuicaoCaractFiscais criterioAtrib = new AtribuicaoCaractFiscais();
		AtribuicaoCaractFiscaisService atribCaractFiscaisService = new AtribuicaoCaractFiscaisService();
		boolean temCriterio = false;

		try
		{
			criterioAtrib.setCliente(null);
			//criterioAtrib.setIdCaracteristicasFiscais(new KscString());
			criterioAtrib.setProduto(null);
			criterioAtrib.setAtividade(null);
			criterioAtrib.setCategoria(null);
			criterioAtrib.setGrupoCliente(null);
			criterioAtrib.setCodigoFiscal(null);
			criterioAtrib.setTipoNotaFiscal(null);
			criterioAtrib.setCaracteristicaProduto(null);
			criterioAtrib.setNaturezaFornecedor(null);

			//System.out.println("EstÃ¡ no tipo:" + tipo);

			//modificado por que GRUPO ï¿½ mais forte, existem as seguintes Carateristicas ligadas a ele
			//'128_NULL_32', 'ICMS ISENTO ï¿½RGï¿½O Pï¿½BLICO - CONVï¿½NIO 26/03', '', '', 0, 0, 1, , 0, , 0, , 0, , 0, , 0, , 0, , 0, '117_NULL_80', 1, 0, 0, 0, 0, '', 0, 0, 1, '', '', '', '', 1, 1, , , 0, '', 0, '198_NULL_1', '128_NULL_32', '', '', '', '', 1, '', '1_16_12', '', , '', '73_91_86', '', ''
			//'131_NULL_5', 'RETENï¿½ï¿½O ï¿½RGï¿½OS Pï¿½BLICOS FEDERAIS', '', '117_NULL_169', 0, , 0, , 0, 0.65, 1, 3, 1, , 0, , 0, , 0, , 0, '', 0, 1, 1, 1, 1, '', 0, , 1, '', '', '', '', , 1, , , 0, '117_NULL_187', 0, '207_NULL_4', '131_NULL_5', '', '', '', '', 1, '', '', '', , '', '73_91_85', '', ''
			//'289_NULL_149', 'VENDA DE MEDICAMENTOS PARA DISTRIBUIDORES NO RS', '55_20_42', '', 0, , 0, , 0, , 0, , 0, , 0, , 0, , 0, , 0, '', 0, 0, 0, 0, 0, 'ICMS SIBSTITUIï¿½ï¿½O TRIBUTï¿½RIA Nï¿½O SE APLICA CFE ART. 104, PARï¿½GRAFO ï¿½NICO, ALï¿½NEA A, LIVRO III, Tï¿½TULO III, CAPï¿½TULO II, SEï¿½ï¿½O XII, SUBSEï¿½ï¿½O I DO DECRETO 37699/97 - RS', 0, 1, 1, '', '', '', '', , 1, 0, , 0, '117_NULL_176', 1, '289_NULL_152', '289_NULL_149', '', '', '', '', 1, '', '', '', , '', '61_91_128', '', ''

			if (tipo==1)
			{
				criterioAtrib.setTipoNotaFiscal(new TipoNotaFiscalService().recuperarPorId(manager,tipoNota.getId()));
				temCriterio = true;
			}

			if(tipo ==2 || tipo==10 || tipo==11)
			{
				if(modelo != DsvConstante.getParametrosSistema().getModeloNotaFiscalConsumidor())
				{
					criterioAtrib.setGrupoCliente(new GrupoClienteService().recuperarPorId(manager,cliente.getGrupoCliente().getId()));
					temCriterio = true;	
				}
				
			}
			if(tipo == 3 || tipo ==5 || tipo==10 || tipo==13)
			{
				criterioAtrib.setProduto(produto);
				temCriterio = true;
			}
			if (tipo==3 || tipo==4 || tipo==9)
			{
				if(modelo != DsvConstante.getParametrosSistema().getModeloNotaFiscalConsumidor())
				{
					criterioAtrib.setCliente(cliente);
					temCriterio = true;	
				}
				
			}
			if (tipo==4 || tipo==8 || tipo==11)
			{
				criterioAtrib.setCodigoFiscal(produto.getCodigoNcm()+"%");
					
				//criterioAtrib.setCodigoFiscal(produto.getCodigoFiscalProduto().getCodigo()); 
				
				temCriterio = true;
			}
			if(tipo==12 || tipo==13)
			{
				if(modelo != DsvConstante.getParametrosSistema().getModeloNotaFiscalConsumidor())
				{
					criterioAtrib.setAtividade(cliente.getAtividade());
					temCriterio = true;	
				}
			}	

			/*if (tipo==1 || tipo==12 || tipo==13)
			{
				criterioAtrib.setIdGrupoCliente(cliente.getIdGrupoCliente());
				temCriterio = true;
			}*/

			/*if(tipo==5)
			{
				criterioAtrib.setCaracteristicaProduto(produto.ge); // Verificar o valor que serï¿½ setado
				temCriterio = true;
			}
			if(tipo==6)
			{
				criterioAtrib.setCategoria(produto.getCategoria());// Verificar o valor que serï¿½ setado 
				temCriterio = true;
			}
			 */


			atribCaractFiscais = null;
			List<AtribuicaoCaractFiscais> resultado = null;
			if (temCriterio==true)
			{
				/*if (tipo==3 || tipo==7 || tipo==13)
				{
					criterioAtrib.setCodigoFiscal()
				}
				else
				{
					resultado = atribCaractFiscaisService.listarPorObjetoFiltro(criterioAtrib);
				}*/

				resultado = atribCaractFiscaisService.listarPorObjetoFiltro(criterioAtrib);

				int i = 0;

				for (i=0;i<resultado.size();i++)
				{
					if (!(resultado.get(0).getEmpresaFisica()==null) && resultado.get(0).getEmpresaFisica().getId().intValue() == (empresa.getId().intValue()))
					{
						atribCaractFiscais = resultado.get(0);
					}
					else if (resultado.get(0).getEmpresaFisica()==null)
					{
						atribCaractFiscais = resultado.get(0);
					}

					//System.out.println("Existe tipo:" + tipo);
				}
			}

			return atribCaractFiscais;	
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	private String transfObsString()
	{
		String retorno = "";
		if (!observacaoNota.isEmpty())
		{
			Iterator i = observacaoNota.values().iterator();
			while(i.hasNext())
			{
				HashMap observ = (HashMap) i.next();
				Iterator j = observ.keySet().iterator();
				while (j.hasNext())
				{
					String obs = j.next().toString();
					Object[] itens = ((ArrayList) observ.get(obs)).toArray();
					String strItens="";
					for (int it=0;it<itens.length;it++)
					{
						String strIt = itens[it].toString();
						strItens = strItens + strIt.substring(strIt.length()-2) + "," ;
					}
					retorno = retorno + " Itens: " + strItens.substring(0, strItens.length()-1) + " - " +  obs + " / "; 
				}
			}
		}	
		return retorno;
	}

	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Excluir Nota Fiscal")
	public void excluir (List<NotaFiscal> listaNotaFiscal, EntityTransaction transaction, EntityManager manager) throws Exception
	{
		try
		{
			for(int i=0;i<listaNotaFiscal.size();i++)
			{
				NotaFiscal entidade = listaNotaFiscal.get(i);

				if(entidade.getSituacaoNotaFiscal().getId() == DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalGerada() || entidade.getSituacaoNotaFiscal().getId() == DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalRejeitada())
				{
					ItemNotaFiscal itemNotaFiscal = new ItemNotaFiscal();
					ItemNotaFiscalService itemNotaFiscalService = new ItemNotaFiscalService();

					itemNotaFiscal.setNotaFiscal(entidade);
					List<ItemNotaFiscal> listaItemNotaFiscal = itemNotaFiscalService.listarPorObjetoFiltro(itemNotaFiscal);
					for(ItemNotaFiscal itemNotaCriterio : listaItemNotaFiscal)
					{
						itemNotaFiscalService.excluir(manager, transaction, itemNotaCriterio);
					}
					try
					{
						super.excluir(manager, transaction, entidade);
					}
					catch(Exception e)
					{
						System.err.print(e.getMessage());
						if(e.getMessage().indexOf("Cannot delete or update a parent row: a foreign key constraint fails") > 0)
						{	
							throw new Exception("NotaFiscal nao pode ser excluido(a). Existem registros relacionados.");					
						}
						else 
						{
							throw new Exception(e.getMessage());
						}
					}
				}
				else
					throw new Exception("Apenas Notas Fiscais na situacao 'GERADA' ou 'REJEITADA' podem ser excluï¿½das");

			}

		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	//@Override
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Listar Paginado Nota Fiscal")
	public Page<NotaFiscal> listarPaginadoPorObjetoFiltroNotaPedidoVenda(int pageSize, int pageNumber, NotaFiscal entidade, String ordemPesquisa, String tipoOrdenacao)throws Exception
	{
		try
		{
			List<NotaFiscal> listaNotaFiscal = super.listarPorObjetoFiltro
					(entidade, ordemPesquisa, tipoOrdenacao);
			List<NotaFiscal> listaNotaFiscalResult = new ArrayList<NotaFiscal>();

			for(NotaFiscal notaFiscal : listaNotaFiscal)
			{
				if(notaFiscal.getPedidoVenda() != null)
					listaNotaFiscalResult.add(notaFiscal);
			} 
			return Formatador.gerarPagina(listaNotaFiscalResult, pageNumber, pageSize);
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}

	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Enviar Nota Fiscal") 
	public NotaFiscal enviarNotaFiscalNfe(NotaFiscal notaFiscal, EntityManager manager, EntityTransaction transaction)throws Exception
	{
		Boolean transacaoIndependente = false;

		NotaFiscal notaRetorno = new NotaFiscal();

		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			
			
			if(notaFiscal.getSituacaoNotaFiscal().getId() != DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalGerada())
				throw new Exception("Apenas notas fiscais na situacao 'GERADA' podem ser enviadas para o SEFAZ.");

			List<NotaFiscal> listaNotaFiscal = new ArrayList<>();
			listaNotaFiscal.add(notaFiscal);
			notaRetorno = enviarNotaFiscalNfe(listaNotaFiscal, manager, transaction);

			transaction.commit();
			
			while (notaRetorno.getSituacaoNotaFiscal().getId()==DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalEnviadaDfe()||
					notaRetorno.getSituacaoNotaFiscal().getId()==DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalImportadaDfe())
			{
				notaRetorno = new NotaFiscalService().recuperarPorId(notaRetorno.getId());
			}

			if(transacaoIndependente)
				transaction.commit();

			
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
		return notaRetorno;
	}

	
	public void reenviarNotaFiscalNfe(List<NotaFiscal> listaNotaFiscal)throws Exception
	{
		this.reenviarNotaFiscalNfe(listaNotaFiscal, null, null);
	}
	
	
	public NotaFiscal reenviarNotaFiscalNfe(NotaFiscal notaFiscal)throws Exception
	{
		
		NotaFiscal notaRetorno = new NotaFiscal();
		List<NotaFiscal> listaNotaFiscal = new ArrayList<>();
		listaNotaFiscal.add(notaFiscal);
		notaRetorno = reenviarNotaFiscalNfe(listaNotaFiscal, null, null);
		return notaRetorno;
	}
	
	
	public NotaFiscal reenviarNotaFiscalNfe(NotaFiscal notaFiscal, String cpf)throws Exception
	{
		
		NotaFiscal notaRetorno = new NotaFiscal();
		if(!cpf.equals(""))
			notaFiscal.setCpfReceptor(cpf);
		
		this.atualizar(notaFiscal);
		
		List<NotaFiscal> listaNotaFiscal = new ArrayList<>();
		listaNotaFiscal.add(notaFiscal);
		notaRetorno = reenviarNotaFiscalNfe(listaNotaFiscal,null, null);
		return notaRetorno;
	}
	
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Reenviar Nota Fiscal")
	public NotaFiscal reenviarNotaFiscalNfe(List<NotaFiscal> listaNotaFiscal, EntityManager manager, EntityTransaction transaction)throws Exception
	{
		Boolean transacaoIndependente = false;
		NotaFiscal notaFiscal = new NotaFiscal();
		EmpresaFisica empresa = new EmpresaFisica();
		NotaFiscalService notaFiscalService = new NotaFiscalService();
		NotaFiscal notaRetorno = new NotaFiscal();

		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			
			ParamentrosFaturamento criterioParametros = new ParamentrosFaturamento();
			criterioParametros.setEmpresaFisica(empresa);
			for(int i = 0;i<listaNotaFiscal.size();i++)
			{
				notaFiscal = listaNotaFiscal.get(i); 
				
				if(notaFiscal.getSituacaoNotaFiscal().getId() != DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalRejeitada())
					throw new Exception("Apenas notas fiscais na situaÃ§Ã£o 'REJEITADA' podem ser reenviadas para o SEFAZ.");


				if(notaFiscal.getTipoNotaFiscal().getId().intValue() == DsvConstante.getParametrosSistema().getIdTipoNotaFiscalVendaConsignacao())
					validaPagamento(manager, transaction, notaFiscal.getId());
				else if(notaFiscal.getTipoNotaFiscal().getId().intValue() == DsvConstante.getParametrosSistema().getIdTipoNotaFiscalVenda())
					validaPagamento(manager, transaction, notaFiscal.getId());
				else if(notaFiscal.getTipoNotaFiscal().getId().intValue() == DsvConstante.getParametrosSistema().getIdTipoNotaFiscalVendaAtivoImobilizado())
					validaPagamento(manager, transaction, notaFiscal.getId());

				
				//Apaga informaï¿½ï¿½es complementares caso existam e gera dados novamente
				new NotaFiscalEletronicaService().excluirInformacoesComplementares(notaFiscal, null, null, manager, transaction);
				new NotaFiscalEletronicaService().gerarInformacoesComplementares(notaFiscal, null, null, manager, transaction);

				SituacaoNotaFiscal situacaoNF = new SituacaoNotaFiscalService().recuperarPorId(manager, DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalEnviadaDfe());
				notaFiscal.setSituacaoNotaFiscal(situacaoNF);
				notaFiscal.setStatusNfe(new Integer(1));
				notaFiscal.setDataEmissao(new Date()); // coloquei este campo
				notaFiscal.setHoraSaidaEntrada(new Date()); // coloquei este campo

				notaRetorno = notaFiscalService.atualizar(manager, transaction, notaFiscal);

			}
			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();
			
			throw(ex);
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
		return notaRetorno;
	
	}

	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao
	public NotaFiscal enviarNotaFiscalNfe (List<NotaFiscal> listaNotaFiscal) throws Exception
	{
		NotaFiscal notaRetorno = this.enviarNotaFiscalNfe(listaNotaFiscal, null, null);
		return notaRetorno;
	}
	public NotaFiscal enviarNotaFiscalNfe (List<NotaFiscal> listaNotaFiscal, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		NotaFiscal notaFiscal = new NotaFiscal();
		EmpresaFisica empresa = new EmpresaFisica();
		NotaFiscalService notaFiscalService = new NotaFiscalService();

		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			
			

			ParamentrosFaturamento criterioParametros = new ParamentrosFaturamento();
			criterioParametros.setEmpresaFisica(empresa);
			for(int i = 0;i<listaNotaFiscal.size();i++)
			{
				notaFiscal = listaNotaFiscal.get(i); 
				
				if(notaFiscal.getSituacaoNotaFiscal().getId() != DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalGerada())
					throw new Exception("Apenas notas fiscais na situacao 'GERADA' podem ser enviadas para o SEFAZ.");


				if(notaFiscal.getTipoNotaFiscal().getId().intValue() == DsvConstante.getParametrosSistema().getIdTipoNotaFiscalVendaConsignacao())
					validaPagamento(manager, transaction, notaFiscal.getId());
				else if(notaFiscal.getTipoNotaFiscal().getId().intValue() == DsvConstante.getParametrosSistema().getIdTipoNotaFiscalVenda())
					validaPagamento(manager, transaction, notaFiscal.getId());
				else if(notaFiscal.getTipoNotaFiscal().getId().intValue() == DsvConstante.getParametrosSistema().getIdTipoNotaFiscalVendaAtivoImobilizado())
					validaPagamento(manager, transaction, notaFiscal.getId());

				
				//Apaga informaï¿½ï¿½es complementares caso existam e gera dados novamente
				new NotaFiscalEletronicaService().excluirInformacoesComplementares(notaFiscal, null, null, manager, transaction);
				new NotaFiscalEletronicaService().gerarInformacoesComplementares(notaFiscal, null, null, manager, transaction);

				SituacaoNotaFiscal situacaoNF = new SituacaoNotaFiscalService().recuperarPorId(manager, DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalEnviadaDfe());
				notaFiscal.setSituacaoNotaFiscal(situacaoNF);
				notaFiscal.setStatusNfe(new Integer(1));
				notaFiscal.setDataEmissao(new Date()); // coloquei este campo
				notaFiscal.setHoraSaidaEntrada(new Date()); // coloquei este campo

				notaFiscalService.atualizar(manager, transaction, notaFiscal);

			}
			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
		
		
		return notaFiscal;
		

	}


	
	protected void validaPagamento(EntityManager manager, EntityTransaction transaction, Integer idNotaFiscal)throws Exception
	{
		Boolean transacaoIndependente = false;

		try
		{
			NotaFiscal nf = new NotaFiscal();		   

			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}

			try
			{
				nf.setId(idNotaFiscal);
				List<NotaFiscal> listaNf = new NotaFiscalService().listarPorObjetoFiltro(manager,  nf, null, null, null);

				if(listaNf.size()!=1)
					throw new KscException("Sistema nao pode identificar a nota fiscal");
				else 
					nf = listaNf.get(0);

				boolean exigePagamento = true;

				if(nf.getPedidoVenda() != null)
				{
					if (nf.getPedidoVenda().getTipoMovimentacaoEstoque().getExigePagamento()!=null)
						exigePagamento = nf.getPedidoVenda().getTipoMovimentacaoEstoque().getExigePagamento()=="1"?true:false;
				}

				if (exigePagamento==true)
				{
					double totalPagto=0;
					double dif = 0;
					PagamentoNotaFiscal pgNotaFiscal = new PagamentoNotaFiscal();
					pgNotaFiscal.setNotaFiscal(nf);
					List<PagamentoNotaFiscal> al= new PagamentoNotaFiscalService().listarPorObjetoFiltro(pgNotaFiscal);
					for(int i=0;i<al.size();i++)
					{
						PagamentoNotaFiscal pnf = al.get(i);
						totalPagto=totalPagto+pnf.getValor().doubleValue();
					}
					if(nf.getValorTotalContaReceber().doubleValue()>0.0)
						dif = totalPagto - nf.getValorTotalContaReceber().doubleValue();
					else
						dif = totalPagto -nf.getValorTotalNota();

					if(dif!=0){
						if(dif>0 && dif>0.5)
							throw new Exception("O total de pagamentos ("+new Double(totalPagto)+") ï¿½ maior que o valor total da nota fiscal ("+nf.getValorTotalNota()+") ");
						else if(dif<0 && dif<-0.5)
							throw new Exception("O total de pagamentos ("+new Double(totalPagto)+") ï¿½ inferior ao valor total da nota fiscal ("+nf.getValorTotalNota()+") ");
					}
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				throw(ex);
			}

			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}


	public String gerarNotaFiscalPagamentoPedidoVenda (PedidoVenda pedidoVenda, Cliente cliente) throws Exception
	{
		Boolean impressaoSolicitada = false;
		String mensagemDetalhe = "";
		int count = 0;
		long runTime = 2000;

		NotaFiscal notaFiscal = new NotaFiscal();

		if(pedidoVenda.getCliente().getId() == 0 || pedidoVenda.getCliente() == null || cliente == null || cliente.getId() == 0)
			throw new Exception("CLIENTE NAO SELECIONADO.");

		notaFiscal = gerarNotaFiscalPagamentoPedidoVenda(pedidoVenda,cliente, null, null);
		NotaFiscalService notaFiscalService = new NotaFiscalService();

		enviarNotaFiscalNfe(notaFiscalService.recuperarPorId(notaFiscal.getId()), null, null);

		do

		{
			notaFiscal = new NotaFiscalService().recuperarPorId(notaFiscal.getId());

			if(notaFiscal.getSituacaoNotaFiscal().getId() == DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalAutorizadaSefaz()
					&& !impressaoSolicitada)
			{
				new NotaFiscalService().imprimirNotaFiscal(notaFiscal);
				impressaoSolicitada = true;
				System.out.println("Impressao solicitada a " + (count * runTime));
			}

			if(notaFiscal.getSituacaoNotaFiscal().getId() == DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalImpressa())
			{
				mensagemDetalhe = "Aguarde, sua Nota Fiscal estï¿½ sendo impressa";
				break;
			}
			else if(count * runTime >= 45000)
			{
				mensagemDetalhe = "NÃ£o foi possÃ­vel imprimir a nota fiscal. SituaÃ§Ã£o atual da Nota : " + notaFiscal.getSituacaoNotaFiscal().getDescricao();
				break;
			}

			count++;
			Thread.sleep(runTime);
		}
		while(mensagemDetalhe == "");

		return mensagemDetalhe;
	}

	public NotaFiscal gerarNotaFiscalPagamentoPedidoVenda (PedidoVenda pedidoVenda, Cliente cliente, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;

		try
		{
			NotaFiscal notaFiscal = new NotaFiscal();

			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}

			try
			{
				ParamentrosFaturamento parametrosFaturamento = new ParamentrosFaturamento();
				
				parametrosFaturamento = new ParamentrosFaturamentoService().recuperarParametroPorEmpresaFisica(pedidoVenda.getEmpresaFisica(),manager,transaction);
				
				if(parametrosFaturamento == null)
					throw new Exception("Parametro Faturamento nao encontrado para a empresa do Pedido Venda selecionado.");
					

				notaFiscal.setPedidoVenda(pedidoVenda);
				notaFiscal.setCliente(cliente);
				notaFiscal.setNumero(parametrosFaturamento.getNumeroUltimaNotaImpressa()+1);
				notaFiscal.setEntradaSaida(pedidoVenda.getTipoMovimentacaoEstoque().getEntradaSaida());
				notaFiscal = gerarNotaFiscalBaseadoPedidoVenda(notaFiscal,null, null);


				//SituacaoNotaFiscal situacaoNotaFiscalCriterio = new SituacaoNotaFiscal();
				//situacaoNotaFiscalCriterio.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoNotaFiscalEnviadaSefaz());
				//notaFiscal.setSituacaoNotaFiscal(situacaoNotaFiscalCriterio);

				new NotaFiscalService().atualizar(notaFiscal);
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				throw(ex);
			}

			if(transacaoIndependente)
				transaction.commit();

			return notaFiscal;
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}

	}

	public String cancelarNotaFiscal(List<NotaFiscal> listaNotaFiscal, String motivoCancelamento,EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			NotaFiscal notaFiscal = new NotaFiscal();
			Integer numeroNotaCancelada;
			String textoRetorno = "";
			for(int i =0 ;i<listaNotaFiscal.size(); i++)
			{
				notaFiscal = listaNotaFiscal.get(i);

				if (notaFiscal.getChaveAcesso() == null || notaFiscal.getChaveAcesso().equals("")){
					throw new Exception("Nota Fiscal nao possui chave de acesso para ser cancelada. Provavelmente nem foi autorizada.");}				
				if (notaFiscal.getNumProtocoloSefaz()==null || notaFiscal.getNumProtocoloSefaz().equals("")){
					throw new Exception("Nota Fiscal nao possui numero de protocolo para ser cancelada. Provavelmente nem foi autorizada.");}				
				if (notaFiscal.getSituacaoNotaFiscal().getId().intValue() == DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalEntregaEncerrada()){
					throw new Exception("Nota Fiscal ja esta na situacao de entrega encerrada. Provavelmente jï¿½ estï¿½ com o canhoto assinado. Neste caso vocï¿½ deve emitir uma nota de devoluï¿½ï¿½o.");}				

				try
				{
					SituacaoNotaFiscal situacaoNotaFiscal = new SituacaoNotaFiscal();
					situacaoNotaFiscal.setId(DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalCancelamentoSolicitado());
					notaFiscal.setSituacaoNotaFiscal(situacaoNotaFiscal);
				}
				catch(Exception e)
				{
					throw new Exception("Situacao de Cancelamento Solicitado da Nota Fiscal nao esta informado no arquivo propriedades.txt.");
				}

				notaFiscal.setStatusCancelamento(new Integer(1));	
				notaFiscal.setMotivoCancelamento(motivoCancelamento);
	
				excluirPagamentoNotaFiscal(notaFiscal, manager, transaction);
				new NotaFiscalService().atualizar(manager, transaction, notaFiscal);

				numeroNotaCancelada = notaFiscal.getNumero(); 
				textoRetorno = numeroNotaCancelada + ", " + textoRetorno;
			}
			if(transacaoIndependente)
				transaction.commit();

			return "Notas Fiscais Canceladas num: " + textoRetorno;
		}
		catch(Exception ex)
		{
			if (transacaoIndependente)
				transaction.rollback();

			ex.printStackTrace();

			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}

	protected void excluirPagamentoNotaFiscal(NotaFiscal notaFiscal, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}

			PagamentoNotaFiscal pnfCriterio = new PagamentoNotaFiscal();
			PagamentoNotaFiscalService pnfs = new PagamentoNotaFiscalService();

			pnfCriterio.setNotaFiscal(notaFiscal);

			List<PagamentoNotaFiscal> listaPagamentoNotaFiscal = pnfs.listarPorObjetoFiltro(manager,pnfCriterio, null, null, null);

			for(int i=0;i<listaPagamentoNotaFiscal.size();i++)
			{
				pnfCriterio = listaPagamentoNotaFiscal.get(i);
				PagamentoNotaFiscal criterio = new PagamentoNotaFiscal();
				criterio.setId(pnfCriterio.getId());
				pnfs.excluir(manager, transaction, criterio);
			}

			if(transacaoIndependente)
				transaction.commit();
		}

		catch(Exception ex)
		{
			if (transacaoIndependente)
				transaction.rollback();

			ex.printStackTrace();

			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}


	
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Imprimir Nota Fiscal Consuidor")
	public NotaFiscal imprimirNotaFiscalConsumidor(NotaFiscal notaFiscal) throws Exception
	{
		List<NotaFiscal> listaNotaFiscal = new ArrayList<>();
		NotaFiscal notaFiscalRetorno = new NotaFiscal();
		List<NotaFiscal> listaRetornada = new ArrayList<>();
		int cont = 0;
		try
		{
			listaNotaFiscal.add(notaFiscal);
			listaRetornada = imprimirNotaFiscal(listaNotaFiscal, null, null); 
			
			if(listaRetornada.size() > 0)
			{
				notaFiscalRetorno = listaRetornada.get(0);
				
				while (notaFiscalRetorno.getSituacaoNotaFiscal().getId()==DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalImpressaoSolicitada())
				{
					notaFiscalRetorno = new NotaFiscalService().recuperarPorId(notaFiscalRetorno.getId());	
					Thread.sleep(3000);
					cont++;
					if(cont==4)
						break;
				}
			}
			return notaFiscalRetorno;
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}

	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Enviar Nota Fiscal")
	public List<NotaFiscal> imprimirNotaFiscal(NotaFiscal notaFiscal) throws Exception
	{
		List<NotaFiscal> listaNotaFiscal = new ArrayList<>();
		List<NotaFiscal> listaRetornada = new ArrayList<>();
		try
		{
			listaNotaFiscal.add(notaFiscal);
			listaRetornada = imprimirNotaFiscal(listaNotaFiscal, null, null); 
			
			return listaRetornada;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}

	}
	
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Enviar Nota Fiscal Consuidor")
	public List<NotaFiscal> imprimirNotaFiscal(List<NotaFiscal> listaNotaFiscal, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			NotaFiscal notaFiscal = new NotaFiscal();
			List<NotaFiscal> listaRetornada = new ArrayList<>();
			
			if(listaNotaFiscal.size() > 0)
			{
				for(int i=0; i<listaNotaFiscal.size();i++)
				{
					notaFiscal = listaNotaFiscal.get(i);

					if(notaFiscal.getSituacaoNotaFiscal().getId().intValue() == DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalAutorizadaSefaz()
							|| notaFiscal.getSituacaoNotaFiscal().getId().intValue() == DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalImpressa())
					{

						notaFiscal.setSituacaoNotaFiscal(new SituacaoNotaFiscalService().recuperarPorId(manager,DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalImpressaoSolicitada()));
						notaFiscal.setStatusImpressao(new Integer(1));
						this.atualizar(manager, transaction, notaFiscal);
						listaRetornada.add(notaFiscal);
					}
					else
						throw new Exception("Apenas notas fiscais na situacao AUTORIZADA OU IMPRESSA podem ser Impressas.");
				}	
			}
			
			if(transacaoIndependente)
				transaction.commit();
			
			return listaRetornada;
		}
		
		catch(Exception ex)
		{
			if (transacaoIndependente)
				transaction.rollback();

			ex.printStackTrace();

			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao
	public void imprimirContigencia (List<NotaFiscal> listaNotaFiscal, EntityManager manager, EntityTransaction transaction)throws Exception
	{
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}

			if(listaNotaFiscal != null)
			{
				for (int i=0; i < listaNotaFiscal.size(); i++)
				{
					NotaFiscal notaFiscal = listaNotaFiscal.get(i);							

					if (notaFiscal.getSituacaoNotaFiscal().getId().intValue() == DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalAutorizadaSefaz() ||
							notaFiscal.getSituacaoNotaFiscal().getId().intValue() == DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalImpressa() ||
							notaFiscal.getSituacaoNotaFiscal().getId().intValue() == DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalGerada() ||
							notaFiscal.getSituacaoNotaFiscal().getId().intValue() == DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalEnviadaDfe())
						throw new Exception("Esta nota esta em processo normal. A contingencia nao e aceita nesse caso.");

					if(notaFiscal.getSituacaoNotaFiscal().getId().intValue() == DsvConstante.getParametrosSistema().getIdTipoNotaFiscalVendaConsignacao())
						validaPagamento(manager, transaction, notaFiscal.getId());
					else if(notaFiscal.getSituacaoNotaFiscal().getId().intValue() == DsvConstante.getParametrosSistema().getIdTipoNotaFiscalVenda())
						validaPagamento(manager, transaction, notaFiscal.getId());
					else if(notaFiscal.getSituacaoNotaFiscal().getId().intValue() == DsvConstante.getParametrosSistema().getIdTipoNotaFiscalVendaAtivoImobilizado())
						validaPagamento(manager, transaction, notaFiscal.getId());

					notaFiscal.setSituacaoNotaFiscal(new SituacaoNotaFiscalService().recuperarPorId(DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalImpressaContingencia()));
					notaFiscal.setStatusContingencia(new Integer(1));
					NotaFiscalService notaFiscalService = new NotaFiscalService();
					notaFiscalService.atualizar(notaFiscal);
				}
			}
			else
				throw new Exception("Nenhuma Nota Fiscal foi selecionada");

			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			if (transacaoIndependente)
				transaction.rollback();

			ex.printStackTrace();

			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}

	public void gerarNotaFiscalByPass() throws Exception
	{


		PedidoVenda entidade = new PedidoVendaService().recuperarPorId(319);
		ItemPedidoVenda entidadeItemCriterio = new ItemPedidoVenda();
		entidadeItemCriterio.setPedidoVenda(entidade);

		List<ItemPedidoVenda> listaItemPedidoVenda = new ItemPedidoVendaService().listarPorObjetoFiltro(entidadeItemCriterio);

		EntityManager manager = ConexaoUtil.getEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		transaction.begin();

		PedidoVenda entidadeBuffer = entidade;
		entidadeBuffer.setId(null);
		PedidoVenda entidadeSalva = new PedidoVendaService().salvar(manager, transaction, entidadeBuffer);

		for(ItemPedidoVenda itemEntidade : listaItemPedidoVenda)
		{
			itemEntidade.setId(null);
			itemEntidade.setPedidoVenda(entidadeSalva);

			new ItemPedidoVendaService().salvar(manager, transaction, itemEntidade);
		}

		transaction.commit();
		manager.close();

		System.out.println(this.gerarNotaFiscalPagamentoPedidoVenda(entidadeSalva, new ClienteService().recuperarPorId(55587)));
	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao
  	public boolean salvarEmailNotaFiscal(NotaFiscal nota, EntityManager manager, EntityTransaction transaction)throws Exception
	{
  		boolean retorno = false;
  		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			
  			if(nota.getId() == null || nota.getId()==0)
  				 throw new Exception("Aguarde a NOTA ser Gerada.");
  			else
  			{
  				this.atualizar(nota);
  				retorno = true;
  			}
  			
  			if(transacaoIndependente)
  				transaction.commit();
  		}
  		catch(Exception e)
  		{
  			if(transacaoIndependente)
  				transaction.rollback();
  			
  			e.printStackTrace();
  			
  			throw e;
  		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
		return retorno;
		
	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao
  	public void enviarNotaFiscalConsumidorPorEmail(List<NotaFiscal> listaNotaFiscal, EntityTransaction transaction, EntityManager manager)throws Exception 
  	{
  		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
  			String from = DsvConstante.getParametrosSistema().get("EMAIL_LOJA");
			String password = DsvConstante.getParametrosSistema().get("SENHA_EMAIL_LOJA");
			String smtpServer = DsvConstante.getParametrosSistema().get("SMTP_LOJA");
			String caminho="http://www.sped.fazenda.pr.gov.br/modules/conteudo/conteudo.php?conteudo=100";
			
  			if(listaNotaFiscal.size() > 0)
  			{
  				for (NotaFiscal notaFiscal : listaNotaFiscal) {
  					if(notaFiscal.getEmail() != null && notaFiscal.getEmail() != "")
  		  			{
  						DecimalFormat fmt = new DecimalFormat("0.00");
  	  					String valor = fmt.format(notaFiscal.getValorTotalNota());
  	  					String subject = "Nota Fiscal Consumidor da Loja: " + notaFiscal.getEmpresa().getNomeFantasia();
  	  					String messageBody = "";
  	  					messageBody += "NFC-e Numero: " + notaFiscal.getNumero()+ "<br>";
  	  					messageBody += "Data: "+ new SimpleDateFormat("dd/MM/yyyy").format(notaFiscal.getDataEmissao())+"<br>";
  	  					messageBody += "Valor da Compra: R$"+ valor +"<br>";
  	  					messageBody += "Chave de Acesso: "+ notaFiscal.getChaveAcesso()+"<br><br>";
  	  					messageBody += "<a href=" +caminho+">Acesse sua nota aqui.</a> E insira a chave de acesso no campo correspondente.<br><br>";
  	  					messageBody += "<br><br><br>Essa e uma mensagem automatica. Por favor nao responda.";
  	  					
  	  					String fromLabel = notaFiscal.getEmpresa().getNomeFantasia()+" - "+ notaFiscal.getEmpresa().getCidade().getNomeSemAcento();
  	  					Address[] addressTo = new Address[1];
  	  					addressTo[0] = new InternetAddress(notaFiscal.getEmail());
  	  					
  	  					try
  	  					{
  	  						new EnviarEmail().enviarEmail(from, password, smtpServer, addressTo, subject, messageBody, fromLabel, "");
  	  						notaFiscal.setEnviouEmail(1);
  	  						this.atualizar(notaFiscal);
  	  					}
  	  					catch(Exception ex)
  	  					{
  	  						throw ex;
  	  					}
  		  			}
  					else
  						throw new Exception("Nota Fiscal sem email cadastrado.");
 				}
  			}
  			if(transacaoIndependente)
  				transaction.commit();
  		}
  		catch(Exception e)
  		{
  			if(transacaoIndependente)
  				transaction.rollback();
  			
  			e.printStackTrace();
  			
  			throw e;
  		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
  	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao
  	public List<NotaFiscal> atualizarNotaFiscalPorLista(List<NotaFiscal> listaNotaFiscal)throws Exception
	{
		List<NotaFiscal> listaNotaFiscalAtualizado = new ArrayList<>();
		if(listaNotaFiscal.size() > 0)
		{
			for (NotaFiscal notaFiscal : listaNotaFiscal) {
				this.atualizar(notaFiscal);
				listaNotaFiscalAtualizado.add(notaFiscal);
			}
		}
		return listaNotaFiscalAtualizado;
	}
  	
  	/*
  	public void enviarEmailNotaFiscalConsumidor(List<PedidoVenda> listaPedidoVenda)throws Exception 
  	{
  		try
  		{
  			String from = DsvConstante.getParametrosSistema().get("EMAIL_LOJA");
			String password = DsvConstante.getParametrosSistema().get("SENHA_EMAIL_LOJA");
			String smtpServer = DsvConstante.getParametrosSistema().get("SMTP_LOJA");
			String caminho="http://www.sped.fazenda.pr.gov.br/modules/conteudo/conteudo.php?conteudo=100";
			
  			NotaFiscal notaFiscal = new NotaFiscal();
  			
  			if(listaPedidoVenda.size() > 0)
  			{
  				for (PedidoVenda pedidoVenda : listaPedidoVenda) {
  					if(pedidoVenda.getNotaFiscal() != null && pedidoVenda.getEmail() != null && pedidoVenda.getEmail() != "")
  		  			{
  		  				notaFiscal = new NotaFiscalService().recuperarPorId(pedidoVenda.getNotaFiscal().getId());
  		  			}
  					DecimalFormat fmt = new DecimalFormat("0.00");
  					String valor2 = fmt.format(pedidoVenda.getValorTotalPedido());
  					String valor = String.valueOf(Formatador.round(pedidoVenda.getValorTotalPedido(),2)).replace(".",",");
  					String subject = "Nota Fiscal Consumidor da Loja: " + pedidoVenda.getEmpresaFisica().getNomeFantasia();
  					String messageBody = "";
  					messageBody += "NFC-e Numero: " + notaFiscal.getNumero()+ "<br>";
  					messageBody += "Data: "+ new SimpleDateFormat("dd/MM/yyyy").format(pedidoVenda.getDataVenda())+"<br>";
  					messageBody += "Valor da Compra: R$"+ valor2 +"<br>";
  					messageBody += "Chave de Acesso: "+ notaFiscal.getChaveAcesso()+"<br><br>";
  					messageBody += "<a href=" +caminho+">Acesse sua nota aqui.</a> E insira a chave de acesso no campo correspondente.<br><br>";
  					messageBody += "<br><br><br>Essa ï¿½ uma mensagem automï¿½tica. Por favor nï¿½o responda.";
  					
  					String fromLabel = pedidoVenda.getEmpresaFisica().getNomeFantasia()+" - "+ pedidoVenda.getEmpresaFisica().getCidade().getNomeSemAcento();
  					Address[] addressTo = new Address[1];
  					addressTo[0] = new InternetAddress(pedidoVenda.getEmail());
  					
  					try
  					{
  						new EnviarEmail().enviarEmail(from, password, smtpServer, addressTo, subject, messageBody, fromLabel, null);
  						pedidoVenda.setEnviouEmail("S");
  						new PedidoVendaService().atualizar(pedidoVenda);
  					}
  					catch(Exception ex)
  					{
  						throw ex;
  					}
  					
  					
  					
				}
  			}
  		}
  		catch(Exception ex)
  		{
  			ex.printStackTrace();
  		}
  	}
  	*/
  	public String gerarUrlQrCode(NotaFiscal notaFiscal)throws Exception
  	{
  		int ambiente = Integer.parseInt(DsvConstante.getParametrosSistema().get("AMBIENTE_NOTA"));
  		String url, tpAmb,cDest = "";
  		
  		String data = (new SimpleDateFormat("dd/MM/yyyy").format(notaFiscal.getDataEmissao())).replaceAll("/", "");
  		String hora = (new SimpleDateFormat("HH:mm:ss").format(notaFiscal.getHoraSaidaEntrada())).replaceAll(":", "");
  		String dhEmi = data+hora;
  		   
  		if( ambiente == 1)
  		{
  			url = "http://www.sefaz.mt.gov.br/nfce/consultanfce?";
  			tpAmb = "1";
  		}
  		else
  		{
  			url = "http://homologacao.sefaz.mt.gov.br/nfce/consultanfce?";
  			tpAmb = "2";
  		}
  			
  		cDest = (notaFiscal.getCpfReceptor() != "" && notaFiscal.getCpfReceptor() != null)?notaFiscal.getCpfReceptor():notaFiscal.getCliente()!=null?notaFiscal.getCliente().getCpfCnpj():"";

  		String urlCompleta="";
  		urlCompleta +=  url +
  		              "chNFe="     + notaFiscal.getChaveAcesso() +
  		              "&nVersao="  + "100" +
  		              "&tpAmb="    + tpAmb +
  		              cDest +
  		              "&dhEmi="    + dhEmi +
  		              "&vNF="      + Formatador.round(notaFiscal.getValorIITotal(), 2)+
  		              "&vICMS="    + Formatador.round(notaFiscal.getValorIcms(), 2)+
  		              "&digVal="   + ""+
  		              "&cIdToken=" + "000001";

  		return urlCompleta;
  	}
  	
  	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao
  	public List<NotaFiscal> atualizarCpfNotaFiscal(List<NotaFiscal> listaNotaFiscal)throws Exception
	{
		List<NotaFiscal> listaNotaAtualizado = new ArrayList<>();
		Cliente cliente = null;
		if(listaNotaFiscal.size() > 0)
		{
			for (NotaFiscal notaFiscal : listaNotaFiscal) {
				if(notaFiscal.getSituacaoNotaFiscal().getId() == DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalRejeitada() ||
						notaFiscal.getSituacaoNotaFiscal().getId() == DsvConstante.getParametrosSistema().getIdSituacaoNotaFiscalGerada())
				{
					cliente = new ClienteService().recuperarClientePorCpf(notaFiscal.getCpfReceptor());
					if(cliente != null)
						notaFiscal.setCliente(cliente);
					this.atualizar(notaFiscal);
					listaNotaAtualizado.add(notaFiscal);
				}
				else
					throw new Exception("NÃ£o Ã© possÃ©vel alterar CPF de notas Autorizadas!");
					
			}
		}
		return listaNotaAtualizado;
	}
	
  	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao
  	public List<NotaFiscal> atualizarPorLista(List<NotaFiscal> listaNotaFiscal)throws Exception
	{
		List<NotaFiscal> listaNotaAtualizado = new ArrayList<>();

		if(listaNotaFiscal.size() > 0)
		{
			for (NotaFiscal notaFiscal : listaNotaFiscal) {
				this.atualizar(notaFiscal);
				listaNotaAtualizado.add(notaFiscal);
			}
					
		}
		return listaNotaAtualizado;
	}
  	
  	public NotaFiscal calcularImposto(EntityManager manager, NotaFiscal notaFiscal) throws Exception
  	{
  		ImpostoEmpresa impostoEmpresa = new ImpostoEmpresa();
  		impostoEmpresa = new ImpostoEmpresaService().recuperarImpostoEmpresa(notaFiscal.getEmpresa());
  		
  		Double valorImpostoMunicipal = 0.0;
  		Double valorImpostoEstadual = 0.0;
  		Double valorImpostoFederal = 0.0;
  		
  		if(impostoEmpresa.getImpostoMunicipal() != null)
  			valorImpostoMunicipal = Formatador.round((notaFiscal.getValorTotalNota() * impostoEmpresa.getImpostoMunicipal())/100,2);
  		
  		if(impostoEmpresa.getImpostoEstadual() != null)
  			valorImpostoEstadual = Formatador.round((notaFiscal.getValorTotalNota() * impostoEmpresa.getImpostoEstadual())/100,2);
  		
  		if(impostoEmpresa.getImpostoFederal() != null)
  			valorImpostoFederal = Formatador.round((notaFiscal.getValorTotalNota() * impostoEmpresa.getImpostoFederal())/100,2);
  		
  		notaFiscal.setValorImpostoMunicipal(valorImpostoMunicipal);
  		notaFiscal.setValorImpostoEstadual(valorImpostoEstadual);
  		notaFiscal.setValorImpostoFederal(valorImpostoFederal);
  		
  		return notaFiscal;
  		
  		
  	}
  	
  	public static void main(String[] args) throws Exception 
	{
  		NotaFiscal notaFiscal = new NotaFiscal();
  		notaFiscal.setValorTotalNota(100.00);
  		
  		new NotaFiscalService().gerarNotaFiscalBaseadoPedidoVenda(notaFiscal);
	}
  	
  	

}
