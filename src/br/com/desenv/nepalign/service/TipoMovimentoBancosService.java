package br.com.desenv.nepalign.service;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.TipoMovimentoBancos;
import br.com.desenv.nepalign.persistence.TipoMovimentoBancosPersistence;

public class TipoMovimentoBancosService extends GenericServiceIGN<TipoMovimentoBancos, TipoMovimentoBancosPersistence>
{
	public TipoMovimentoBancosService() { super(); }
}