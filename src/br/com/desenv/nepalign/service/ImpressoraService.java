package br.com.desenv.nepalign.service;

import java.nio.charset.Charset;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.Impressora;
import br.com.desenv.nepalign.persistence.ImpressoraPersistence;
import br.com.desenv.nepalign.util.DsvImpressaoTermica;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ImpressoraService extends GenericServiceIGN<Impressora, ImpressoraPersistence>
{
	public ImpressoraService() 
	{
		super();
	}
	
	public final Impressora recuperarImpressoraTerminal(final String ipTerminal) throws Exception
	{
		return recuperarImpressoraTerminal(null, ipTerminal);
	}
	
	public final Impressora recuperarImpressoraTerminal(EntityManager manager, final String ipTerminal) throws Exception
	{
		final Impressora impressora = new Impressora();
		impressora.setIp(ipTerminal);
		
		final List<Impressora> listaImpressoraTerminal = listarPorObjetoFiltro(impressora, "idImpressora", "desc");
		
		if(listaImpressoraTerminal.size() == 0x0000)
			return null;
		else return listaImpressoraTerminal.get(0x0000);
	}
	
	public void testarImpressao(final String caminho) throws Exception
	{
		final Impressora impressoraTeste = new Impressora();
		impressoraTeste.setCaminho(caminho);
		
		new DsvImpressaoTermica().imprimir("Teste de impressão\n\n\n\n\n".getBytes(Charset.forName("UTF-8")), false, impressoraTeste, null);
	}
	
	public void salvarImpressoraTerminal(final String ipTerminal, final String nomeTerminal, final String caminho) throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();

		try
		{
			manager.getTransaction().begin();
			
			Impressora impressora = recuperarImpressoraTerminal(manager, ipTerminal);
			
			if(impressora == null)
				impressora = new Impressora();
			
			impressora.setIp(ipTerminal);
			impressora.setNome(nomeTerminal);
			impressora.setCaminho(caminho);
			
			manager.merge(impressora);
			
			manager.getTransaction().commit();
		}
		catch(Exception ex)
		{
			if(manager.getTransaction().isActive())
				manager.getTransaction().rollback();
			
			throw ex;
		}
		finally
		{
			if(manager != null)
				manager.close();
		}
	}
}