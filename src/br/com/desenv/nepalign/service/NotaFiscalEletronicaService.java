package br.com.desenv.nepalign.service;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.nepalign.service.NotaFiscaComplNfeService;
import br.com.desenv.nepalign.model.Atividade;
import br.com.desenv.nepalign.model.Cidade;
import br.com.desenv.nepalign.model.Cliente;
import br.com.desenv.nepalign.model.CodigoSituacaoTributaria;
import br.com.desenv.nepalign.model.Empresa;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.EnderecoEmpresa;
import br.com.desenv.nepalign.model.ItemNotaAuxNfe;
import br.com.desenv.nepalign.model.ItemNotaCofinsNfe;
import br.com.desenv.nepalign.model.ItemNotaFiscal;
import br.com.desenv.nepalign.model.ItemNotaIcmsNfe;
import br.com.desenv.nepalign.model.ItemNotaPisNfe;
import br.com.desenv.nepalign.model.NotaFiscal;
import br.com.desenv.nepalign.model.NotaFiscaComplNfe;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class NotaFiscalEletronicaService {

	
	public void gerarInformacoesComplementares(NotaFiscal criterio, EmpresaFisica empresaFisica, Cliente cliente, EntityManager manager, EntityTransaction transaction) throws Exception  
	{
		Boolean transacaoIndependente = false;
		try
		{
		   if(manager == null || transaction == null)
		   {
			   manager = ConexaoUtil.getEntityManager();
			   transaction = manager.getTransaction();
			   transaction.begin();
			   transacaoIndependente = true;
		   }

		   try
		   {
			  NotaFiscal notaFiscal;
			
			  ItemNotaFiscal itemNotaFiscal = new ItemNotaFiscal();
			  itemNotaFiscal.setNotaFiscal(criterio);
			  List<ItemNotaFiscal> listaItemNotaFiscal = new ItemNotaFiscalService().listarPorObjetoFiltro(manager, itemNotaFiscal, null, null, null);  
			  if (listaItemNotaFiscal != null && listaItemNotaFiscal.size()>0)
				  notaFiscal = criterio;
				else
					throw new Exception("Erro ao tentar recuperar dados completos de Nota Fiscal");
			
				if (empresaFisica==null)
				{
					empresaFisica = new EmpresaFisica();
					empresaFisica.setId(notaFiscal.getEmpresa().getId());
					empresaFisica = (EmpresaFisica) new EmpresaFisicaService().recuperarPorId(manager, empresaFisica.getId());
				}
					
				Cidade cidade = new Cidade();
		
				Atividade atividade = new Atividade();
				AtividadeService atividadeService = new AtividadeService();
				
				try
				{
					atividade = atividadeService.recuperarPorId(manager, empresaFisica.getAtividade().getId());
				}
				catch(Exception exem)
				{
					throw new Exception("Atividade nao encontrada." + exem.getMessage());
				}			
	
				String cnnaeEmpr = atividade.getCnae().replaceAll("-","").replaceAll("/","");
				
				NotaFiscaComplNfe notaCompl = new NotaFiscaComplNfe();
				notaCompl.setCepEmpr(empresaFisica.getCep());
				notaCompl.setChaveAcessoNFE(new String());
				notaCompl.setCNAEEmpr(cnnaeEmpr);
				//notaCompl.setCNAEEmpr(new KscString(""+atividade.getCnae().getValue()));
				notaCompl.setCodigoMunicipioEmpr(empresaFisica.getCidade().getCodigoIbge().toString());
				notaCompl.setCodigoPaisEmpr(DsvConstante.getInstanceParametrosSistema().getCodigoPaisBrasil()); 
				notaCompl.setDataRetornoNFE(new Date());
				notaCompl.setFinalidadeNota(new Integer(1));
				notaCompl.setFlgIncideISSQN(new Integer(0));
				notaCompl.setFlgLocalEntregaDiferente(new Integer(0));
				notaCompl.setFlgLocalRetiradaDiferente(new Integer(0));
				notaCompl.setNotaFiscal(notaFiscal);
				notaCompl.setInscrMunicipalEmpr(new String());
				notaCompl.setMotivoInutilizacaoNFE(new String());
				notaCompl.setMotivoStatusCanc(new String());
				notaCompl.setNomeMunicipioEmpr(empresaFisica.getCidade().getNome());
				notaCompl.setNumProtocoloREFAZ(new String());
				notaCompl.setSiglaUFEmpr(empresaFisica.getCidade().getUf().getSigla());
				notaCompl.setStatusCancelamentoNFE(new Integer(0));
				notaCompl.setStatusImpressaoNFE(new Integer(0));
				notaCompl.setStatusInutilizacaoNFE(new Integer(0));
				notaCompl.setStatusNFE(new Integer(0));
				//notaCompl.setTelefoneEmpr(new KscString("4832443524"));
				notaCompl.setTelefoneEmpr(empresaFisica.getTelefone());
				notaCompl.setTipoNotaNFE(new Integer(1));
				notaCompl.setFlLocalEntregaDif(notaCompl.getFlgLocalEntregaDiferente());
				notaCompl.setFlLocalRetiradaDif(notaCompl.getFlgLocalRetiradaDiferente());
				notaCompl.setFlInsideISSQN(new Integer(0));
				notaCompl.setMotivoStatusNFE(new String());
				try
				{
					new NotaFiscaComplNfeService().salvar(manager, transaction, notaCompl);
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					throw new Exception("Erro ao incluir complnfe" + ex.getMessage());
				}
				
				gerarItemNotaAuxNfe(notaFiscal, empresaFisica, cliente, manager, transaction);
			
			}
		   catch(Exception e)
			{
				e.printStackTrace();
				throw new Exception(e);
			}
			   if(transacaoIndependente)
				   transaction.commit();
	   }
	   catch(Exception ex)
	   {
		   if(transacaoIndependente)
			   transaction.rollback();
		   ex.printStackTrace();
		   throw ex;
	   }
	   finally
	   {
		   if(transacaoIndependente)
			   manager.close();
	   }
	}
		   
	
	private void gerarItemNotaAuxNfe(NotaFiscal notaFiscal, EmpresaFisica empresaFisica, Cliente cliente, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
			   
	   try
	   {
		   if(manager == null || transaction == null)
		   {
			   manager = ConexaoUtil.getEntityManager();
			   transaction = manager.getTransaction();
			   transaction.begin();
			   transacaoIndependente = true;
		   }
			try
			{
				ItemNotaFiscal item= new ItemNotaFiscal();
				item.setNotaFiscal(notaFiscal);
				List<ItemNotaFiscal> itens = new ItemNotaFiscalService().listarPorObjetoFiltro(manager,item, null, null, null); 
				Produto pro = new Produto();
				
				for (int i=0;i<itens.size();i++)
				{
					ItemNotaFiscal itemNota = (ItemNotaFiscal) itens.get(i);
					
					ItemNotaAuxNfe itemAux = new ItemNotaAuxNfe();
					
					itemAux.setFlgCOFINSIncide(new Integer(1));
					gerarItemNotaCofinsNfe(itemNota, empresaFisica, cliente, manager, transaction);				
						
					itemAux.setFlgCOFINSSTIncide(new Integer(0));
					itemAux.setFlgGrupoCombIncCIDE(new Integer(0));
					
					itemAux.setFlgICMSIncide(new Integer(1));
					gerarItemNotaIcmsNfe(itemNota, empresaFisica, cliente, manager, transaction);				
					
					itemAux.setFlgIIincide(new Integer(0)); //TODO
					itemAux.setFlgIPIIncide(new Integer(0));
					itemAux.setFlgISSQNIncide(new Integer(0));
					itemAux.setFlgItemCombustivel(new Integer(0));
									
					if (itemNota.getProduto().getOrigemMercadoria()==null)
					{
						pro = new Produto();
						pro = itemNota.getProduto();
						pro = (Produto) new ProdutoService().recuperarPorId(manager, pro.getId());
					}
					else
					{
						pro = itemNota.getProduto();
					}
					
					/*if (pro.getRastreabilidade().getValue().booleanValue()==true)
					{
						itemAux.setFlgItemMedicamento(new KscBoolean(true));
						itemAux.setObservacao(new KscString("LOTE: " + itemNota.getLote().getViewValue() + " DT.FAB: " + itemNota.getDataFabricacao().getWebValue() + " DT.VAL: " + itemNota.getValidade().getViewValue()));
					}
					else
					{
						itemAux.setObservacao(new KscString());
						itemAux.setFlgItemMedicamento(new KscBoolean(false));
					}
					*/
					itemAux.setObservacao(new String());
					itemAux.setFlgItemMedicamento(new Integer(0));	
					itemAux.setFlgItemImportado(new Integer(0));				
					itemAux.setFlgItemVeiculo(new Integer(0));
					itemAux.setFlgPISIncide(new Integer(1));
					gerarItemNotaPisNfe(itemNota, empresaFisica, cliente, manager, transaction);				
					
					itemAux.setFlgPISSTIncide(new Integer(0));
					itemAux.setItemNotaFiscal(itemNota);
					try
					{
						new ItemNotaAuxNfeService().salvar(manager, transaction, itemAux);
					}
					catch(Exception exp)
					{
						exp.printStackTrace();
						throw new Exception("Erro ao gravar itemNotaAux");
					}				
				}
				
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				throw new Exception(ex);
			}
	   }
	   catch(Exception ex)
	   {
		   if(transacaoIndependente)
			   transaction.rollback();
		   ex.printStackTrace();
		   throw ex;
	   }
	   finally
	   {
		   if(transacaoIndependente)
			   manager.close();
	   }
		
	}
	private void gerarItemNotaCofinsNfe(ItemNotaFiscal itemNotaFiscal, EmpresaFisica empresaFisica, Cliente cliente, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		try
		{
			ItemNotaCofinsNfe itemCofins = new ItemNotaCofinsNfe();
			itemCofins.setAliquotaCOFINSST(new Double(0)); //ok
			itemCofins.setAliquotaCONFINSValorST(new Double(0)); //ok
			itemCofins.setCodigoCSTCOFINS(itemNotaFiscal.getCstCofins());
			/*if (itemCofins.getAliquotaCOFINS().getValue().doubleValue()>0)
				itemCofins.setCodigoCSTCOFINS(new KscInteger(1)); //TODO
			else
				itemCofins.setCodigoCSTCOFINS(new KscInteger(4));*/
			
			if (itemCofins.getCodigoCSTCOFINS().intValue()==1 || itemCofins.getCodigoCSTCOFINS().intValue()==2 || itemCofins.getCodigoCSTCOFINS().intValue()>=49)
			{
				if(itemNotaFiscal.getNotaFiscal().getModelo() == 65)
				{
					itemCofins.setValorBaseCalcCOFINS(new Double(0)); //ok
					itemCofins.setAliquotaCOFINS(new Double(0)); //ok
					itemCofins.setValorCOFINS(new Double(0)); //ok
				}
				else
				{
					itemCofins.setValorBaseCalcCOFINS(itemNotaFiscal.getValorTotal()); //ok
					itemCofins.setAliquotaCOFINS(itemNotaFiscal.getPercCofins()); //ok
					itemCofins.setValorCOFINS(new Double(itemNotaFiscal.getPercCofins().doubleValue()*itemNotaFiscal.getValorTotal().doubleValue()/100)); //ok	
				}
				
			}
			else
			{
				itemCofins.setValorBaseCalcCOFINS(new Double(0)); //ok
				itemCofins.setAliquotaCOFINS(new Double(0)); //ok
				itemCofins.setValorCOFINS(new Double(0)); //ok
			}
			if (itemCofins.getCodigoCSTCOFINS().intValue()==3)
			{
				itemCofins.setQuantVendida(itemNotaFiscal.getQuantidade()); //ok
				itemCofins.setAliquotaCOFINSValor(new Double(0));
				itemCofins.setValorCOFINS(new Double(itemNotaFiscal.getPercCofins().doubleValue()*itemNotaFiscal.getValorTotal().doubleValue()/100)); //ok
			}
			else
			{
				itemCofins.setQuantVendida(new Double(0)); //ok
				itemCofins.setAliquotaCOFINSValor(new Double(0));
			}			
			
			itemCofins.setItemNotaFiscal(itemNotaFiscal);
			//itemCofins.setId(itemNotaFiscal.getId()); //ok
			itemCofins.setQuanVendidaCOFINSST(new Double(0)); //ok
			itemCofins.setValorBaseCalcCOFINSST(new Double(0)); //ok
			itemCofins.setValorCOFINSST(new Double(0)); //ok
			try
			{
				new ItemNotaCofinsNfeService().salvar(manager, transaction, itemCofins);
		
			}
			catch(Exception exp)
			{
				exp.printStackTrace();
				throw new Exception("Erro ao gravar itemNotaCofins" + exp.getMessage());
			}			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception(e);
		}
	}
	private void gerarItemNotaIcmsNfe(ItemNotaFiscal itemNotaFiscal, EmpresaFisica empresaFisica, Cliente cliente, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		try
		{
			Produto pro;
			CodigoSituacaoTributaria cst = new CodigoSituacaoTributaria();
			//cst.setId(itemNotaFiscal.getCodigoSituacaoTributaria().getId());
			try
			{
				cst = (CodigoSituacaoTributaria) new CodigoSituacaoTributariaService().recuperarPorId(manager,itemNotaFiscal.getCodigoSituacaoTributaria().getId());
			}
			catch(Exception exc)
			{
				throw new Exception("não foi possivel recuperar código de CST do ICMS");
			}
			
			ItemNotaIcmsNfe itemIcms = new ItemNotaIcmsNfe();
			itemIcms.setAliqICMSST(itemNotaFiscal.getAliquotaICMS());
			itemIcms.setCodigoCSTICMS(new Integer(cst.getCodigo()));
			itemIcms.setItemNota(itemNotaFiscal);
			if (itemNotaFiscal.getPMC().doubleValue()>0)
			{
				itemIcms.setModBaseCalcICMSST(new Double(0)); //TODO
			}
			else
			{
				itemIcms.setModBaseCalcICMSST(new Double(4));
			}
			
			itemIcms.setModDetermBaseCalcICMS(new Integer(3));
			
			
			if (itemNotaFiscal.getProduto().getOrigemMercadoria()==null)
			{
				pro = new Produto();
				pro = itemNotaFiscal.getProduto();
				pro = (Produto) new ProdutoService().recuperarPorId(manager,pro.getId());
			}
			else
			{
				pro = itemNotaFiscal.getProduto();
			}
			try
			{
				if (pro.getOrigemMercadoria().getId().intValue() == DsvConstante.getParametrosSistema().getIdOrigemMercadoriaNacional())
					itemIcms.setOrigemMercadoria(new Integer(0));
				else if (pro.getOrigemMercadoria().getId().intValue() == DsvConstante.getParametrosSistema().getIdOrigemMercadoriaEstrangeiraImportacao())
					itemIcms.setOrigemMercadoria(new Integer(1));
				else if (pro.getOrigemMercadoria().getId().intValue() == DsvConstante.getParametrosSistema().getIdOrigemMercadoriaEstrangeiraInterna())
					itemIcms.setOrigemMercadoria(new Integer(2));
				else
					throw new Exception("Origem da mercadoria não foi informada ou não é compatível com NFe.");
			}
			catch(Exception orEx)
			{
				throw new Exception("Origem da mercadoria não foi informada no produto.");
			}
			
			//DAS
			double das=0;
			double valorDas=0;
			try
			{
				das = DsvConstante.getInstanceParametrosSistema().getPercDas();
				if (das>0)
				{
					valorDas = das * itemNotaFiscal.getValorTotal().doubleValue() /100;
				}
			}
			catch(Exception ex)
			{
				System.out.println("Não possui DAS.");
				ex.printStackTrace();//
			}			

			itemIcms.setPercMVAICMSST(itemNotaFiscal.getMargemLucro());
			itemIcms.setPercRedBaseCalcICMSST(itemNotaFiscal.getPercReducaoBaseCalcST());
			itemIcms.setValBaseCalcICMS(itemNotaFiscal.getValorBaseCalcICMS());
			itemIcms.setValBaseCalcICMSST(itemNotaFiscal.getValorBaseCalcICMSST());
			itemIcms.setValICMS(itemNotaFiscal.getValorICMS());
			itemIcms.setValICMSST(itemNotaFiscal.getValorICMSST());
			itemIcms.setUfStICMS(new String());
			itemIcms.setPbcopIcms(new Double(0));
			itemIcms.setVbcstretIcms(new Double(0));
			itemIcms.setVicmsstret(new Double(0));
			itemIcms.setMotdesicms(new Integer(0)); //Verificar o valor do inteiro
			itemIcms.setVbcstdest(new Double(0));
			itemIcms.setVicmsstdest(new Double(0));
			if (itemIcms.getCodigoCSTICMS().intValue()==40 || itemIcms.getCodigoCSTICMS().intValue()==41 || itemIcms.getCodigoCSTICMS().intValue()==50)
			{
				itemIcms.setMotdesicms(new Integer(9));
			}
			if (das>0)
			{
				itemIcms.setPcredsn(new Double(das));
				itemIcms.setVcredicmssn(new Double(valorDas));
			}
			else
			{
				itemIcms.setPcredsn(new Double(0));
				itemIcms.setVcredicmssn(new Double(0));
			}
			itemIcms.setVbcstdesticms(new Double(0));
			
			try
			{
				new ItemNotaIcmsNfeService().salvar(manager, transaction, itemIcms);
			}
			catch(Exception exp)
			{
				exp.printStackTrace();
				throw new Exception("Erro ao gravar itemNotaICMS" + exp.getMessage());
			}			
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception(e);
		}	
	}
	private void gerarItemNotaPisNfe(ItemNotaFiscal itemNotaFiscal, EmpresaFisica empresaFisica, Cliente cliente, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		try
		{
			try
			{
				ItemNotaPisNfe itemPis = new ItemNotaPisNfe();
				itemPis.setAliquotaPISST(new Double(0)); //ok
				itemPis.setAliquotaPISValorST(new Double(0));
				/*if (itemPis.getAliquotaPIS().getValue().doubleValue()>0)
					itemPis.setCodigoCSTPIS(new KscInteger(1)); //TODO
				else
					itemPis.setCodigoCSTPIS(new KscInteger(4));*/
				itemPis.setCodigoCSTPIS(itemNotaFiscal.getCstPis());
				itemPis.setItemNota(itemNotaFiscal); //ok
				
				itemPis.setQuantVendPISST(itemNotaFiscal.getQuantidade()); //ok
				if (itemPis.getCodigoCSTPIS().intValue()==1 || itemPis.getCodigoCSTPIS().intValue()==2 || itemPis.getCodigoCSTPIS().intValue()>=49)
				{
					if(itemNotaFiscal.getNotaFiscal().getModelo() == 65)
					{
						itemPis.setValorBseCalcPIS(new Double(0)); //ok
						itemPis.setAliquotaPIS(new Double(0)); //ok
						itemPis.setValorPIS(new Double(0)); //ok
					}
					else
					{
						itemPis.setValorBseCalcPIS(itemNotaFiscal.getValorTotal()); //ok
						itemPis.setAliquotaPIS(itemNotaFiscal.getPercPis()); //ok
						itemPis.setValorPIS(new Double(itemNotaFiscal.getPercPis().doubleValue()*itemNotaFiscal.getValorTotal().doubleValue()/100)); //ok	
					}
					
				}
				else
				{
					itemPis.setValorBseCalcPIS(new Double(0)); //ok
					itemPis.setAliquotaPIS(new Double(0)); //ok
					itemPis.setValorPIS(new Double(0)); //ok					
				}
				if (itemPis.getCodigoCSTPIS().intValue()==3)
				{
					itemPis.setQuatVendPIS(itemNotaFiscal.getQuantidade()); //ok
					itemPis.setAliquotaPISValor(new Double(0));
					itemPis.setValorPIS(new Double(itemNotaFiscal.getPercPis().doubleValue()*itemNotaFiscal.getValorTotal().doubleValue()/100)); //ok					
				}
				else
				{
					itemPis.setQuatVendPIS(new Double(0)); //ok
					itemPis.setAliquotaPISValor(new Double(0));
				}
				itemPis.setValorBaseCalcPISST(new Double(0)); //ok				
				itemPis.setValorPISST(new Double(0)); //ok
				try
				{
					new ItemNotaPisNfeService().salvar(manager, transaction, itemPis);
				}
				catch(Exception exp)
				{
					exp.printStackTrace();
					throw new Exception("Erro ao gravar itemNotaPis" + exp.getMessage());
				}			
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new Exception(e);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception(e);
		}		
	}
	
	public void excluirInformacoesComplementares(NotaFiscal criterio, EmpresaFisica empresa, Cliente cliente, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		try
		{
		   if(manager == null || transaction == null)
		   {
			   manager = ConexaoUtil.getEntityManager();
			   transaction = manager.getTransaction();
			   transaction.begin();
			   transacaoIndependente = true;
		   }

		   try
		   {
			  NotaFiscal notaFiscal;
			
			  ItemNotaFiscal itemNotaFiscal = new ItemNotaFiscal();
			  itemNotaFiscal.setNotaFiscal(criterio);
			  List<ItemNotaFiscal> listaItemNotaFiscal = new ItemNotaFiscalService().listarPorObjetoFiltro(manager, itemNotaFiscal, null, null, null);  
			  if (listaItemNotaFiscal != null && listaItemNotaFiscal.size()>0)
				  notaFiscal = criterio;
				else
					throw new Exception("Erro ao tentar recuperar dados completos de Nota Fiscal");
			  
			  NotaFiscaComplNfe critNf = new NotaFiscaComplNfe();
			  critNf.setNotaFiscal(notaFiscal);
			  
			  List<NotaFiscaComplNfe> listaNotaComp = new NotaFiscaComplNfeService().listarPorObjetoFiltro(manager, critNf, null, null, null);  
			  if (listaNotaComp != null && listaNotaComp.size()>0)
			  {
				  NotaFiscaComplNfe complNotaExcluir = new NotaFiscaComplNfe();
				  complNotaExcluir = listaNotaComp.get(0);
				  new NotaFiscaComplNfeService().excluir(manager,manager.getTransaction(), complNotaExcluir);
			  }
				//else
					//System.out.println("Nota Fiscal nao possui complementos");
				  
				ItemNotaCofinsNfe itCofinsCri = new ItemNotaCofinsNfe();
				ItemNotaPisNfe itPisCri = new ItemNotaPisNfe();
				ItemNotaIcmsNfe itIcmsCri = new ItemNotaIcmsNfe();
				ItemNotaAuxNfe itAuxCri = new ItemNotaAuxNfe();
				
				ItemNotaCofinsNfe itCofins = new ItemNotaCofinsNfe();
				ItemNotaPisNfe itPis = new ItemNotaPisNfe();
				ItemNotaIcmsNfe itIcms = new ItemNotaIcmsNfe();
				ItemNotaAuxNfe itAux = new ItemNotaAuxNfe();
				
				
				for (int i=0;i<listaItemNotaFiscal.size();i++)
				{
					ItemNotaFiscal itemNota = (ItemNotaFiscal) listaItemNotaFiscal.get(i);
					itCofinsCri.setItemNotaFiscal(itemNota);
					itPisCri.setItemNota(itemNota);
					itIcmsCri.setItemNota(itemNota);
					itAuxCri.setItemNotaFiscal(itemNota);
					
					List<ItemNotaCofinsNfe> listaItCofins = new ItemNotaCofinsNfeService().listarPorObjetoFiltro(manager,itCofinsCri,null, null, null);
					if(listaItCofins.size()>0)
					{
						itCofins = listaItCofins.get(0);
						new ItemNotaCofinsNfeService().excluir(manager, manager.getTransaction(),itCofins);
					}
					
					List<ItemNotaPisNfe> listaItPis = new ItemNotaPisNfeService().listarPorObjetoFiltro(manager,itPisCri, null, null, null);
					if(listaItPis.size()>0)
					{
						itPis = listaItPis.get(0);
						new ItemNotaPisNfeService().excluir(manager, manager.getTransaction(),itPis);
					}
					
					List<ItemNotaIcmsNfe> listaItIcms = new ItemNotaIcmsNfeService().listarPorObjetoFiltro(manager,itIcmsCri, null, null, null);
					if(listaItIcms.size()>0)
					{
						itIcms = listaItIcms.get(0);
						new ItemNotaIcmsNfeService().excluir(manager, manager.getTransaction(),itIcms);
					}
					
					List<ItemNotaAuxNfe> listaItAux = new ItemNotaAuxNfeService().listarPorObjetoFiltro(manager,itAuxCri, null, null, null);
					if(listaItAux.size()>0)
					{
						itAux = listaItAux.get(0);
						new ItemNotaAuxNfeService().excluir(manager, manager.getTransaction(),itAux);
					}
						
				}
					
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new Exception(e);
			}
			   if(transacaoIndependente)
					   transaction.commit();
	   }
	   catch(Exception ex)
	   {
		   if(transacaoIndependente)
			   transaction.rollback();
		   ex.printStackTrace();
		   throw ex;
	   }
	   finally
	   {
		   if(transacaoIndependente)
			   manager.close();
	   }
   }

}
	
	

