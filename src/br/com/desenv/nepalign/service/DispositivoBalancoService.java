package br.com.desenv.nepalign.service;

import java.util.List;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.DispositivoBalanco;
import br.com.desenv.nepalign.persistence.DispositivoBalancoPersistence;

public class DispositivoBalancoService extends GenericServiceIGN<DispositivoBalanco, DispositivoBalancoPersistence> {
	
	@Override
	public DispositivoBalanco salvar(DispositivoBalanco entidade) throws Exception {
		
		if (entidade.getId() != null) {
			entidade.setId(null);
		}
		
		if (entidade.getDescricao() == null || entidade.getDescricao().isEmpty()) {
			throw new Exception("Informe uma descrição válida para o Dispositivo.");
		} else if ((entidade.getOperador1() == null || entidade.getOperador1().isEmpty()) && (entidade.getOperador2() == null || entidade.getOperador2().isEmpty())) {
			throw new Exception("Identifique ao menos 1 Operador para o Dispositivo.");
		} else if (entidade.getOperador1() == null || entidade.getOperador1().isEmpty()) {
			entidade.setOperador1(entidade.getOperador2());
		} else if (entidade.getSequenciaBaseBalanco() == null || entidade.getSequenciaBaseBalanco().getSequenciaBase().equals("")) {
			throw new Exception("Informe uma Sequência Base válida para o Dispositivo.");
		}
		
		entidade.setDescricao(entidade.getDescricao().toUpperCase());
		entidade.setOperador1(entidade.getOperador1().toUpperCase());
		entidade.setOperador2(entidade.getOperador2().toUpperCase());
		List<DispositivoBalanco> dispositivoExistente = super.listarPorObjetoFiltro(new DispositivoBalanco(null, entidade.getDescricao(), null, null));
		
		if (dispositivoExistente.size() != 0) {
			throw new Exception("Dispositivo " + entidade.getDescricao() + " já está cadastrado.");
		}
		
		dispositivoExistente = super.listarPorObjetoFiltro(new DispositivoBalanco(entidade.getSequenciaBaseBalanco(), null, null, null));
		
		if (dispositivoExistente.size() != 0) {
			throw new Exception("Sequência Base " + entidade.getSequenciaBaseBalanco().getSequenciaBase().toString() + " já está sendo usada por outro Dispositivo.");
		}
		
		return super.salvar(entidade);
	}

}
