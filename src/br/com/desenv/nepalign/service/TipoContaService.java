package br.com.desenv.nepalign.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.ContaGerencial;
import br.com.desenv.nepalign.model.TipoConta;
import br.com.desenv.nepalign.model.TipoContaContaGer;
import br.com.desenv.nepalign.persistence.TipoContaPersistence;

public class TipoContaService extends GenericServiceIGN<TipoConta, TipoContaPersistence>
{
	public TipoContaService() 
	{
		super();
	}
	
	public TipoConta recuperarPorContaGerencial(EntityManager manager, EntityTransaction transaction, final ContaGerencial entidade) throws Exception
	{
		final TipoContaContaGer tipoContaContaGerFiltro = new TipoContaContaGer();
		tipoContaContaGerFiltro.setContaGerencial(entidade);
		
		List<TipoContaContaGer> listaTipoContaContaGer = new TipoContaContaGerService().listarPorObjetoFiltro(manager, tipoContaContaGerFiltro, null, null, null);
		
		if(listaTipoContaContaGer.size() == 0x00)
		{
			TipoConta tipoConta = new TipoConta();
			tipoConta.setDescricao(entidade.getDescricao());
			tipoConta.setCredito(entidade.getCreditoDebito().equals("C") ? 0x01 : 0x00);
			tipoConta.setIncideCpmf(0x00);
			
			salvar(manager, transaction, tipoConta, false);
			
			TipoContaContaGer tipoContaContaGer = new TipoContaContaGer();
			tipoContaContaGer.setContaGerencial(entidade);
			tipoContaContaGer.setTipoConta(tipoConta);
			
			new TipoContaContaGerService().salvar(manager, transaction, tipoContaContaGer, false);
			
			
			
			return tipoConta;
		}
		else
		{
			return listaTipoContaContaGer.get(0x00).getTipoConta();
		}
	}

}