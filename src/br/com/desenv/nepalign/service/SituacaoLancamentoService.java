package br.com.desenv.nepalign.service;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.SituacaoLancamento;
import br.com.desenv.nepalign.persistence.SituacaoLancamentoPersistence;

public class SituacaoLancamentoService extends GenericServiceIGN<SituacaoLancamento, SituacaoLancamentoPersistence>
{
	public static final int ABERTO = 0x01;
	public static final int BAIXADO = 0x02;
	public static final int PARCIAL = 0x03;
	
	public SituacaoLancamentoService() 
	{
		super();
	}
}