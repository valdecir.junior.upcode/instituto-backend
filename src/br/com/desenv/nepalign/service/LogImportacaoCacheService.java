package br.com.desenv.nepalign.service;

import java.util.Date;

import javax.persistence.EntityManager;

import flex.messaging.log.LogCategories;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.LogImportacaoCache;
import br.com.desenv.nepalign.model.TipoOperacaoLV;
import br.com.desenv.nepalign.persistence.LogImportacaoCachePersistence;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class LogImportacaoCacheService extends GenericServiceIGN<LogImportacaoCache, LogImportacaoCachePersistence>
{

	public LogImportacaoCacheService() 
	{
		super();
	}
	
	public void logaAe(int tipo, int tipoLog, String descricaoLog)
	{
		try
		{
			LogImportacaoCache log = new LogImportacaoCache();
			log.setDataHora(new Date());
			log.setDescricaoErro(descricaoLog);
			TipoOperacaoLV tp = new TipoOperacaoLV();
			tp.setId(tipoLog);
			log.setTipoOperacaoLV(tp);
			salvar(log);
		}
		catch(Exception e)
		{
			
		}
	}
	
	public void excluirTodosPorIdSituacao(int id) throws Exception
	{
		try
		{
			LogImportacaoCachePersistence log = new LogImportacaoCachePersistence();
			//log.excluirTodosPorIdSituacao(id);
			
		}
		catch(Exception e)
		{
			throw e;
		}
	
	}
	

}

