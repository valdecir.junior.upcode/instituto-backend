package br.com.desenv.nepalign.service;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.GrupoEmpresa;
import br.com.desenv.nepalign.model.OperadoraCartao;
import br.com.desenv.nepalign.persistence.GrupoEmpresaPersistence;

public class GrupoEmpresaService extends GenericServiceIGN<GrupoEmpresa, GrupoEmpresaPersistence>
{
	public GrupoEmpresaService() 
	{
		super();
	}
	
	public final GrupoEmpresa recuperarGrupoEmpresa(EntityManager manager, final OperadoraCartao operadoraCartao) throws Exception
	{
		return recuperarGrupoEmpresa(manager, operadoraCartao.getId());
	}
	
	public final GrupoEmpresa recuperarGrupoEmpresa(EntityManager manager, final Integer operadoraCartao) throws Exception
	{
		switch(operadoraCartao.intValue())
		{ 
			case OperadoraCartao.REDECARD:
				return recuperarPorId(manager, GrupoEmpresa.GRUPOEMPRESA_CARTAO_REDECARD);
			case OperadoraCartao.HIPERCARD:
				return recuperarPorId(manager, GrupoEmpresa.GRUPOEMPRESA_CARTAO_HIPERCARD);
			case OperadoraCartao.CIELO:
				return recuperarPorId(manager, GrupoEmpresa.GRUPOEMPRESA_CARTAO_CIELO);
			case OperadoraCartao.COOPERCRED:
				return recuperarPorId(manager, GrupoEmpresa.GRUPOEMPRESA_CARTAO_COOPER);
			case OperadoraCartao.SENFF:
				return recuperarPorId(manager, GrupoEmpresa.GRUPOEMPRESA_CARTAO_SENFF);
			case OperadoraCartao.ASSEMPAR:
				return recuperarPorId(manager, GrupoEmpresa.GRUPOEMPRESA_CARTAO_ASEMPAR);
			case OperadoraCartao.BIN:
				return recuperarPorId(manager, GrupoEmpresa.GRUPOEMPRESA_CARTAO_ASEMPAR);
			default: return null;
		}
	}
}