package br.com.desenv.nepalign.service;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.Operadora;
import br.com.desenv.nepalign.persistence.OperadoraPersistence;

public class OperadoraService extends GenericServiceIGN<Operadora, OperadoraPersistence>
{
	public OperadoraService() 
	{
		super();
	}
}

