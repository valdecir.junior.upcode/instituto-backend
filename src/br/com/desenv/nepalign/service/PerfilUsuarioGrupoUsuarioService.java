package br.com.desenv.nepalign.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.Grupo;
import br.com.desenv.nepalign.model.PerfilUsuario;
import br.com.desenv.nepalign.model.PerfilUsuarioGrupoUsuario;
import br.com.desenv.nepalign.persistence.PerfilUsuarioGrupoUsuarioPersistence;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class PerfilUsuarioGrupoUsuarioService extends GenericServiceIGN<PerfilUsuarioGrupoUsuario, PerfilUsuarioGrupoUsuarioPersistence>
{
	public PerfilUsuarioGrupoUsuarioService() 
	{
		super();
	}
	
	public List<Grupo> listarGrupos(EntityManager manager, final PerfilUsuario perfil) throws Exception
	{
		boolean managerIndependente = false;
		try
		{
			if((managerIndependente = (manager == null)))
				manager = ConexaoUtil.getEntityManager();
			
			final List<Grupo> listaGrupos = new ArrayList<Grupo>();
			final PerfilUsuarioGrupoUsuario perfilGrupoCriterio = new PerfilUsuarioGrupoUsuario();
			perfilGrupoCriterio.setPerfilUsuario(perfil);
			
			for(final PerfilUsuarioGrupoUsuario perfilGrupo : listarPorObjetoFiltro(manager, perfilGrupoCriterio, null, null, null))
				listaGrupos.add(perfilGrupo.getGrupo());
			
			return listaGrupos;
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
	
	public List<Grupo> listarGrupos(final PerfilUsuario perfil) throws Exception
	{
		return listarGrupos(null, perfil);
	}
}