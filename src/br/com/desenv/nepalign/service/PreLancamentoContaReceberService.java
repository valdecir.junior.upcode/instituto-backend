package br.com.desenv.nepalign.service;

import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.integracao.dto.ChequeRecebidoDTO;
import br.com.desenv.nepalign.integracao.dto.ClienteDTO;
import br.com.desenv.nepalign.integracao.dto.DuplicataDTO;
import br.com.desenv.nepalign.integracao.dto.DuplicataParcelaDTO;
import br.com.desenv.nepalign.integracao.dto.PagamentoCartaoDTO;
import br.com.desenv.nepalign.model.Banco;
import br.com.desenv.nepalign.model.FormaPagamento;
import br.com.desenv.nepalign.model.GrupoEmpresa;
import br.com.desenv.nepalign.model.PreLancamentoContaReceber;
import br.com.desenv.nepalign.model.TaxaCartao;
import br.com.desenv.nepalign.persistence.PreLancamentoContaReceberPersistence;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class PreLancamentoContaReceberService extends GenericServiceIGN<PreLancamentoContaReceber, PreLancamentoContaReceberPersistence>
{
	private static final TaxaCartaoService taxaCartaoService = new TaxaCartaoService();
	private static final LogOperacaoService logOperacaoService = new LogOperacaoService();
	private static final GrupoEmpresaService grupoEmpresaService = new GrupoEmpresaService();
	private static final EmpresaFisicaService empresaFisicaService = new EmpresaFisicaService();
	private static final FormaPagamentoService formaPagamentoService = new FormaPagamentoService();
	private static final EmpresaFinanceiroService empresaFinanceiroService = new EmpresaFinanceiroService();
	private static final FormaPagamentoCartaoService formaPagamentoCartaoService = new FormaPagamentoCartaoService();
	
	public PreLancamentoContaReceberService() 
	{
		super();
	}
	
	public void gerarPreLancamentoContaReceber(EntityManager manager, final ChequeRecebidoDTO chequeRecebidoDTO) throws Exception
	{
		Boolean transacaoCentralIndependente = false;
		
		try
		{
			if(manager == null)
			{
				manager = ConexaoUtil.getEntityManager();
				manager.getTransaction().begin();
				transacaoCentralIndependente = true;
			}
			
			final PreLancamentoContaReceber preLancamentoContaReceber = new PreLancamentoContaReceber();
			preLancamentoContaReceber.setEmpresaFinanceiro(empresaFinanceiroService.recuperarEmpresaFinanceiro(manager, chequeRecebidoDTO.getEmpresaFisica(), GrupoEmpresa.GRUPOEMPRESA_CHEQUE));
			preLancamentoContaReceber.setBanco(manager.find(Banco.class, chequeRecebidoDTO.getBanco().intValue()));
			preLancamentoContaReceber.setDataLancamento(chequeRecebidoDTO.getDataEmissao());
			preLancamentoContaReceber.setDataVencimento(chequeRecebidoDTO.getBomPara());
			preLancamentoContaReceber.setNumeroDocumento(chequeRecebidoDTO.getNumeroCheque());
			preLancamentoContaReceber.setCpfCnpjEmitente(chequeRecebidoDTO.getCpfCnpjPortador());
			preLancamentoContaReceber.setEmitente(chequeRecebidoDTO.getEmitente());
			preLancamentoContaReceber.setQuantidadeParcelas(0x01);
			preLancamentoContaReceber.setValor(chequeRecebidoDTO.getValor());
			preLancamentoContaReceber.setValorTotal(chequeRecebidoDTO.getValor());
			
			salvar(manager, manager.getTransaction(), preLancamentoContaReceber, false);
			
			logOperacaoService.gravarOperacaoInclusao(manager, manager.getTransaction(), preLancamentoContaReceber, "GERADO ATRAVÉS DO ENVIO DE MOVIMENTO*****");
			
			if (transacaoCentralIndependente)
				manager.getTransaction().commit();
		}
		catch(Exception ex)
		{
			if(transacaoCentralIndependente)
				manager.getTransaction().rollback();
			
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoCentralIndependente)
				manager.close();
		}
	}
	
	public void gerarPreLancamentoContaReceber(EntityManager managerCentral, EntityManager unitManager, final PagamentoCartaoDTO pagamentoCartaoDTO) throws Exception
	{
		Boolean transacaoCentralIndependente = false;
		
		try
		{
			if(managerCentral == null)
			{
				managerCentral = ConexaoUtil.getEntityManager();
				managerCentral.getTransaction().begin();
				transacaoCentralIndependente = true;
			}
			
			if(pagamentoCartaoDTO.getOperadoraCartao() == null)
				return;
			
			final Calendar vencimentoCartao = Calendar.getInstance();
			vencimentoCartao.setTime(pagamentoCartaoDTO.getDataEmissao());
			
			for(int parcela = 0x01; parcela <= pagamentoCartaoDTO.getNumeroParcelas(); parcela++)
			{
				if(unitManager.find(FormaPagamento.class, pagamentoCartaoDTO.getFormaPagamento()).getCartaoCredito() == 0x01)
					vencimentoCartao.add(Calendar.DAY_OF_YEAR, 30);
				else
					vencimentoCartao.add(Calendar.DAY_OF_YEAR, 0x03); //TODO: revisar data de vencimento cartão débito
				
				final PreLancamentoContaReceber preLancamentoContaReceber = new PreLancamentoContaReceber();
				preLancamentoContaReceber.setEmpresaFisicaOrigem(empresaFisicaService.recuperarPorId(managerCentral, pagamentoCartaoDTO.getEmpresaFisica()));
				preLancamentoContaReceber.setEmpresaFinanceiro(empresaFinanceiroService.recuperarEmpresaFinanceiro(managerCentral, pagamentoCartaoDTO.getEmpresaFisica(), grupoEmpresaService.recuperarGrupoEmpresa(managerCentral, pagamentoCartaoDTO.getOperadoraCartao()).getId()));
				preLancamentoContaReceber.setDataLancamento(pagamentoCartaoDTO.getDataEmissao());
				preLancamentoContaReceber.setDataVencimento(vencimentoCartao.getTime());
				preLancamentoContaReceber.setNumeroDocumento(String.valueOf(parcela)); 
				preLancamentoContaReceber.setEmitente(pagamentoCartaoDTO.getEmitente());
				preLancamentoContaReceber.setNumeroParcela(parcela);
				preLancamentoContaReceber.setQuantidadeParcelas(pagamentoCartaoDTO.getNumeroParcelas());
				
				final TaxaCartao taxaCartao = taxaCartaoService.buscaTaxaCartaoPorEmpresaFinanceiro(managerCentral, preLancamentoContaReceber.getEmpresaFinanceiro(), 
										formaPagamentoCartaoService.recuperarFormaPagamentoCartao(managerCentral, formaPagamentoService.recuperarPorId(unitManager, pagamentoCartaoDTO.getFormaPagamento()), 
										preLancamentoContaReceber.getQuantidadeParcelas()), preLancamentoContaReceber.getQuantidadeParcelas(), preLancamentoContaReceber.getDataLancamento());
				
				preLancamentoContaReceber.setValorTotal(pagamentoCartaoDTO.getValorOriginal());
				preLancamentoContaReceber.setValorParcelaSemDesconto(IgnUtil.roundUpDecimalPlaces(pagamentoCartaoDTO.getValorOriginal() / pagamentoCartaoDTO.getNumeroParcelas(), 0x02));
				preLancamentoContaReceber.setValor(IgnUtil.roundUpDecimalPlaces((pagamentoCartaoDTO.getValorOriginal() - (pagamentoCartaoDTO.getValorOriginal() * (taxaCartao.getTaxa() / 100))) / pagamentoCartaoDTO.getNumeroParcelas(), 0x02));
				
				salvar(managerCentral, managerCentral.getTransaction(), preLancamentoContaReceber, false);
				logOperacaoService.gravarOperacaoInclusao(managerCentral, managerCentral.getTransaction(), preLancamentoContaReceber, "GERADO ATRAVÉS DO ENVIO DE MOVIMENTO*****");
			}
			
			if (transacaoCentralIndependente)
				managerCentral.getTransaction().commit();
		}
		catch(Exception ex)
		{
			if(transacaoCentralIndependente)
				managerCentral.getTransaction().rollback();
			
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoCentralIndependente)
				managerCentral.close();
		}
	}
	
	public void gerarPreLancamentoContaReceber(EntityManager managerCentral, EntityManager unitManager, final DuplicataDTO duplicataDTO) throws Exception
	{
		Boolean transacaoCentralIndependente = false;
		
		try
		{
			if(managerCentral == null)
			{
				managerCentral = ConexaoUtil.getEntityManager();
				managerCentral.getTransaction().begin();
				transacaoCentralIndependente = true;
			}
			
			@SuppressWarnings("unchecked")
			final List<DuplicataParcelaDTO> listaDuplicataParcela = (List<DuplicataParcelaDTO>) unitManager.createNativeQuery("SELECT * FROM `duplicataParcela` WHERE `idDuplicata` = " + duplicataDTO.getId().intValue(), DuplicataParcelaDTO.class).getResultList();
			final ClienteDTO cliente = unitManager.find(ClienteDTO.class, duplicataDTO.getCliente());
			
			for(DuplicataParcelaDTO duplicataParcelaDTO : listaDuplicataParcela)
			{ 
				final PreLancamentoContaReceber preLancamentoContaReceber = new PreLancamentoContaReceber();
				preLancamentoContaReceber.setEmpresaFisicaOrigem(empresaFisicaService.recuperarPorId(managerCentral, duplicataDTO.getEmpresaFisica()));
				preLancamentoContaReceber.setEmpresaFinanceiro(empresaFinanceiroService.recuperarEmpresaFinanceiro(managerCentral, duplicataDTO.getEmpresaFisica(), GrupoEmpresa.GRUPOEMPRESA_VALE_CALCADO));
				preLancamentoContaReceber.setDataLancamento(duplicataDTO.getDataCompra());
				preLancamentoContaReceber.setDataVencimento(duplicataParcelaDTO.getDataVencimento());
				preLancamentoContaReceber.setNumeroDocumento(duplicataDTO.getNumeroDuplicata().toString().concat(" - ").concat(String.valueOf(duplicataParcelaDTO.getNumeroParcela()))); 
				preLancamentoContaReceber.setEmitente(cliente.getId().toString().concat(" - ").concat(cliente.getNome())); 
				preLancamentoContaReceber.setQuantidadeParcelas(Integer.parseInt(duplicataDTO.getNumeroTotalParcela()));
				preLancamentoContaReceber.setValor(duplicataParcelaDTO.getValorParcela());
				preLancamentoContaReceber.setValorTotal(duplicataDTO.getValorTotalCompra());
				
				salvar(managerCentral, managerCentral.getTransaction(), preLancamentoContaReceber, false);
				logOperacaoService.gravarOperacaoInclusao(managerCentral, managerCentral.getTransaction(), preLancamentoContaReceber, "GERADO ATRAVÉS DO ENVIO DE MOVIMENTO*****");
			}
			
			if (transacaoCentralIndependente)
				managerCentral.getTransaction().commit();
		}
		catch(Exception ex)
		{
			if(transacaoCentralIndependente)
				managerCentral.getTransaction().rollback();
			
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoCentralIndependente)
				managerCentral.close();
		}
	}
}