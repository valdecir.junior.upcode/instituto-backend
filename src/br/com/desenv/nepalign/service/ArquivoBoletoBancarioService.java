package br.com.desenv.nepalign.service;

import java.io.File;
import java.nio.file.Files;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao.Grupo;
import br.com.desenv.nepalign.model.ArquivoBoletoBancario;
import br.com.desenv.nepalign.persistence.ArquivoBoletoBancarioPersistence;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ArquivoBoletoBancarioService extends GenericServiceIGN<ArquivoBoletoBancario, ArquivoBoletoBancarioPersistence>
{

	public ArquivoBoletoBancarioService() 
	{
		super();
	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Crediario,Grupo.Caixa,Grupo.Gerente}, nomeAcao="Salvar")
	public ArquivoBoletoBancario salvar (String nomeDoArquivo,String tipoArquivo ) throws Exception
	{
		return this.salvar(nomeDoArquivo, tipoArquivo, null, null);
	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Crediario,Grupo.Caixa,Grupo.Gerente}, nomeAcao="Salvar")
	public ArquivoBoletoBancario salvar (String nomeDoArquivo,String tipoArquivo,EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			ArquivoBoletoBancario arquivoBoletoBancario = new ArquivoBoletoBancario();
			arquivoBoletoBancario.setDataHoraCriacao(new Date());
			arquivoBoletoBancario.setNomeArquivo(nomeDoArquivo);
			arquivoBoletoBancario.setTipoArquivo(tipoArquivo);
			arquivoBoletoBancario.setUsuario(new UsuarioService().recuperarSessaoUsuarioLogado());
			arquivoBoletoBancario = this.salvar(manager, transaction, arquivoBoletoBancario);

			if(transacaoIndependente)
				transaction.commit();
			
			return arquivoBoletoBancario;
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Crediario,Grupo.Caixa,Grupo.Gerente}, nomeAcao="Registrar Caminho Salvo")
	public void registrarCaminhoSalvo(String nomeDoArquivo,String caminhoSalvo) throws Exception
	{
		this.registrarCaminhoSalvo(nomeDoArquivo, caminhoSalvo, null, null);
	}
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Crediario,Grupo.Caixa,Grupo.Gerente}, nomeAcao="Registrar Caminho Salvo")
	public void registrarCaminhoSalvo (String nomeDoArquivo,String caminhoSalvo,EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			ArquivoBoletoBancario arquivoBoletoBancarioCriterio = new ArquivoBoletoBancario();
			arquivoBoletoBancarioCriterio.setNomeArquivo(nomeDoArquivo);
			arquivoBoletoBancarioCriterio.setTipoArquivo("E");
			List<ArquivoBoletoBancario> lista = this.listarPorObjetoFiltro(manager, arquivoBoletoBancarioCriterio, null, null, 1);
			if(lista.size()>0){
				ArquivoBoletoBancario arquivoBoletoBancario = lista.get(0);
				arquivoBoletoBancario.setCaminhoSalvo(caminhoSalvo);
				this.atualizar(manager, transaction, arquivoBoletoBancario);
			}
			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	public Map<String, Object> recuperarArquivoRemessa(ArquivoBoletoBancario arquivoBoletoBancario) throws Exception
	{
		try
		{
		if(!arquivoBoletoBancario.getTipoArquivo().equals("E"))
			throw new Exception("Só é possível recuperar arquivos de Remessa");
		
		String caminhoArquivoCNAB400 = DsvConstante.getParametrosSistema().get("CAMINHO_ARQUIVO_REMESSA_CNAB400");
		String nomeArquivo = arquivoBoletoBancario.getNomeArquivo();

		File arquivoRecuperado = new File(caminhoArquivoCNAB400+File.separatorChar+nomeArquivo);
		if(arquivoRecuperado.exists())
		{
			byte[] array = Files.readAllBytes(arquivoRecuperado.toPath());
			Map<String, Object> retorno = new HashMap<String, Object>();
			retorno.put("nomeArquivo", arquivoRecuperado.getName());
			retorno.put("data", array);
			return retorno;
		}
		else
		{
			throw new Exception("Arquivo de remessa não encontrado!");
		}

		}catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}

	}

}

