package br.com.desenv.nepalign.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.Operacao;
import br.com.desenv.nepalign.model.OperacaoMovimentacaoEstoque;
import br.com.desenv.nepalign.model.OperacaoTabelasBasicasEstoque;
import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.nepalign.persistence.OperacaoPersistence;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class OperacaoService extends GenericServiceIGN<Operacao, OperacaoPersistence>
{
	public OperacaoService() 
	{
		super();
	}
	
	public void logaAE(EntityManager manager, EntityTransaction transaction, String classe, String operacao, Date data, Object object)
	{
		byte[] obj = null;
		
		try 
		{
			obj = paraByte(object);
			logaAE(manager, transaction, classe, operacao, data, obj);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void logaAE(EntityManager manager, EntityTransaction transaction, String classe, String operacao, Date data, byte[] objeto)
	{
		boolean transacaoIndependente = false;
		
		UsuarioService usuarioService = new UsuarioService();
		Usuario usuario = new Usuario();
		usuario = usuarioService.recuperarSessaoUsuarioLogado();
		int idEmpresaFisicaLocal;

		Operacao op = new Operacao();
		op.setUsuario(usuario);
		op.setDataOperacao(data);
		op.setNomeClasse(classe);
		op.setOperacao(operacao);
		op.setObjeto(objeto);

		
		
		try 
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			
			salvar(manager, transaction, op);

			if (classe != null && classe.equalsIgnoreCase("br.com.desenv.nepalign.persistence.MovimentacaoEstoquePersistence"))
			{
        			OperacaoMovimentacaoEstoque opMovimentacaoEstoque = new OperacaoMovimentacaoEstoque();
        			opMovimentacaoEstoque.setUsuario(usuario);
        			opMovimentacaoEstoque.setDataOperacao(data);
        			opMovimentacaoEstoque.setNomeClasse(classe);
        			opMovimentacaoEstoque.setOperacao(operacao);
        			opMovimentacaoEstoque.setObjeto(objeto);
        			// 0 - NAO SINCRONIZACAO
        			// 1 - SINCRONIZADO
        			opMovimentacaoEstoque.setFlagSincronizacao("0");
        			OperacaoMovimentacaoEstoqueService operacaoMovimentacaoEstoqueService = new OperacaoMovimentacaoEstoqueService(); 
        
        			operacaoMovimentacaoEstoqueService.salvar(manager, transaction, opMovimentacaoEstoque);
			}
			else if(classe != null && classe.equalsIgnoreCase("br.com.desenv.nepalign.persistence.ItemMovimentacaoEstoquePersistence"))
			{
    			OperacaoMovimentacaoEstoque opMovimentacaoEstoque = new OperacaoMovimentacaoEstoque();
    			opMovimentacaoEstoque.setUsuario(usuario);
    			opMovimentacaoEstoque.setDataOperacao(data);
    			opMovimentacaoEstoque.setNomeClasse(classe);
    			opMovimentacaoEstoque.setOperacao(operacao);
    			opMovimentacaoEstoque.setObjeto(objeto);
    			// 0 - NAO SINCRONIZACAO
    			// 1 - SINCRONIZADO
    			opMovimentacaoEstoque.setFlagSincronizacao("0");
    			OperacaoMovimentacaoEstoqueService operacaoMovimentacaoEstoqueService = new OperacaoMovimentacaoEstoqueService(); 
    
    			operacaoMovimentacaoEstoqueService.salvar(manager, transaction, opMovimentacaoEstoque);
			}
			
			idEmpresaFisicaLocal = DsvConstante.getParametrosSistema().getInt("EMPRESA_LOCAL");
			if (idEmpresaFisicaLocal==0)
			{
				throw new Exception("Empresa local não pode ser identificada em parametros do sistema.");
			}
			
			if (DsvConstante.getParametrosSistema().get(idEmpresaFisicaLocal, "GERAR_LOG_INTEGRACAOESTOQUE") != null &&
					DsvConstante.getParametrosSistema().get(idEmpresaFisicaLocal, "GERAR_LOG_INTEGRACAOESTOQUE").equals("S"))
			{
				OperacaoTabelasBasicasEstoqueService opTabService = new OperacaoTabelasBasicasEstoqueService();
				OperacaoTabelasBasicasEstoque opTab = new OperacaoTabelasBasicasEstoque();
				
				//COR
				//ESTOQUEPRODUTO
				//FORNECEDOR
				//GRUPOPRODUTO
				//MARCA				
				//ORDEMPRODUTO
				//PRODUTO
				//TAMANHO
				//SALDOESTOQUEPRODUTO

				//SALDOESTOQUEPRODUTO				
				if(classe != null && (classe.equalsIgnoreCase("br.com.desenv.nepalign.persistence.FornecedorPersistence") 
						|| classe.equalsIgnoreCase("br.com.desenv.nepalign.persistence.MarcaPersistence")
						|| classe.equalsIgnoreCase("br.com.desenv.nepalign.persistence.GrupoPersistence")
						|| classe.equalsIgnoreCase("br.com.desenv.nepalign.persistence.CorPersistence")
						|| classe.equalsIgnoreCase("br.com.desenv.nepalign.persistence.TamanhoPersistence")
						|| classe.equalsIgnoreCase("br.com.desenv.nepalign.persistence.OrdemProdutoPersistence")
						|| classe.equalsIgnoreCase("br.com.desenv.nepalign.persistence.ProdutoPersistence")
						|| classe.equalsIgnoreCase("br.com.desenv.nepalign.persistence.EstoqueProdutoPersistence")
						|| classe.equalsIgnoreCase("br.com.desenv.nepalign.persistence.SaldoEstoqueProdutoPersistence")
						))
				{
					opTab.setUsuario(usuario);
					opTab.setDataOperacao(data);
					opTab.setNomeClasse(classe);
					opTab.setOperacao(operacao);
					opTab.setObjeto(objeto);
	    			// 0 - NAO SINCRONIZACAO
	    			// 1 - SINCRONIZADO
					opTab.setFlagSincronizacao("0");
	    			opTabService.salvar(manager, transaction, opTab);
				}
			}	
			
			if(transacaoIndependente)
				transaction.commit();
		} 
		catch (Exception e) 
		{
			if(transacaoIndependente)
				transaction.rollback();
			
			e.printStackTrace();
		}	
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	
	public byte[] paraByte(Object obj) throws IOException
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		
		try 
		{
			out = new ObjectOutputStream(bos);   
			out.writeObject(obj);
			byte[] yourBytes = bos.toByteArray();
			return yourBytes;
		} 
		finally 
		{
			out.close();
			bos.close();
		}
	}
	
	public String recuperar(int id)
	{
		Operacao op = new Operacao();
		Usuario o = new Usuario();
		
		try 
		{
			op = recuperarPorId(id);
		
			ByteArrayInputStream bis = new ByteArrayInputStream(op.getObjeto());
			ObjectInput in = null;
			
			try 
			{
				in = new ObjectInputStream(bis);
				o = (Usuario) in.readObject(); 
		 
			} 
			finally 
			{
				bis.close();
				in.close();
			}
		} 
		catch (Exception e) 
		{	
			e.printStackTrace();
		}
		return o.getLogin();
	}

	public String objetoDeserializado(byte[] byteArray) throws Exception
	{
		ByteArrayInputStream in = new ByteArrayInputStream(byteArray);
		ObjectInputStream is = new ObjectInputStream(in);
		return new IgnUtil().deserializarObjeto(is.readObject());
	}
	
	public void logaAEPersonalizadoAtualizar(EntityManager manager, EntityTransaction transaction, Object obj) throws Exception
	{
		if(		  obj.getClass().getSimpleName().equals("DuplicataParcela")
				||obj.getClass().getSimpleName().equals("PedidoVenda")
				||obj.getClass().getSimpleName().equals("MovimentoParcelaDuplicata")
				||obj.getClass().getSimpleName().equals("Duplicata")
				||obj.getClass().getSimpleName().equals("Usuario"))
		{
			if(!Thread.currentThread().getStackTrace()[5].getMethodName().equals("atualizarMovimentoConsistenciaCaixaDia")
					&&!Thread.currentThread().getStackTrace()[4].getMethodName().equals("excluirPagamento")
					&&!Thread.currentThread().getStackTrace()[4].getMethodName().equals("salvarMovimentoDuplicataParela")
					&&!Thread.currentThread().getStackTrace()[4].getMethodName().equals("excluirPagamento")
					&&!Thread.currentThread().getStackTrace()[4].getMethodName().equals("salvarPagamentos")
					&&!Thread.currentThread().getStackTrace()[4].getMethodName().equals("salvarPagamentos")
				    &&!Thread.currentThread().getStackTrace()[5].getMethodName().equals("atualizarInformacoesEntrega"))
			{
				new LogDuplicataService().gerarLog(obj, manager, transaction);
			}
			
		}
	}
	
	public void logaAEPersonalizadoExcluir(EntityManager manager, EntityTransaction transaction, Object obj) throws Exception
	{
		new LogDuplicataService().gerarLogExcluir(obj, manager, transaction);
	}
}