package br.com.desenv.nepalign.service;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.MovimentacaoEstoque;
import br.com.desenv.nepalign.model.TmpMovimentacaoEstoque;
import br.com.desenv.nepalign.persistence.TmpMovimentacaoEstoquePersistence;

public class TmpMovimentacaoEstoqueService extends GenericServiceIGN<TmpMovimentacaoEstoque, TmpMovimentacaoEstoquePersistence>
{
	public TmpMovimentacaoEstoqueService() 
	{
		super();
	}
	
	public TmpMovimentacaoEstoque getTmpMovimentacaoEstoquePorMovimentacaoEstoque(MovimentacaoEstoque movimentacaoEstoque) throws Exception
	{
		TmpMovimentacaoEstoque tmpMovimentacaoEstoque = null;
		try
		{
			tmpMovimentacaoEstoque = new TmpMovimentacaoEstoque();
			tmpMovimentacaoEstoque.setId(movimentacaoEstoque.getId());
			tmpMovimentacaoEstoque.setCliente(movimentacaoEstoque.getCliente());
			tmpMovimentacaoEstoque.setDataMovimentacao(movimentacaoEstoque.getDataMovimentacao());
			tmpMovimentacaoEstoque.setEmpresa(movimentacaoEstoque.getEmpresaFisica());
			tmpMovimentacaoEstoque.setEmpresaDestino(movimentacaoEstoque.getEmpresaDestino());
			tmpMovimentacaoEstoque.setEntradaSaida(movimentacaoEstoque.getEntradaSaida());
			tmpMovimentacaoEstoque.setEstoque(movimentacaoEstoque.getEstoque());
			tmpMovimentacaoEstoque.setFaturado(movimentacaoEstoque.getFaturado());
			tmpMovimentacaoEstoque.setForncedor(movimentacaoEstoque.getForncedor());
			tmpMovimentacaoEstoque.setIdDocumentoOrigem(movimentacaoEstoque.getIdDocumentoOrigem());
			tmpMovimentacaoEstoque.setManual(movimentacaoEstoque.getManual());
			tmpMovimentacaoEstoque.setNumeroDocumento(movimentacaoEstoque.getNumeroDocumento());
			tmpMovimentacaoEstoque.setNumeroVolume(movimentacaoEstoque.getNumeroVolume());
			tmpMovimentacaoEstoque.setObservacao(movimentacaoEstoque.getObservacao());
			tmpMovimentacaoEstoque.setPeso(movimentacaoEstoque.getPeso());
			tmpMovimentacaoEstoque.setStatusMovimentacaoEstoque(movimentacaoEstoque.getStatusMovimentacaoEstoque());
			tmpMovimentacaoEstoque.setTipoDocumentoMovimento(movimentacaoEstoque.getTipoDocumentoMovimento());
			tmpMovimentacaoEstoque.setTipoMovimentacaoEstoque(movimentacaoEstoque.getTipoMovimentacaoEstoque());
			tmpMovimentacaoEstoque.setUsuario(movimentacaoEstoque.getUsuario());
			tmpMovimentacaoEstoque.setValorFrete(movimentacaoEstoque.getValorFrete());
		}
		catch(Exception ex)
		{
			throw ex;
		}
		return tmpMovimentacaoEstoque;
	}
}