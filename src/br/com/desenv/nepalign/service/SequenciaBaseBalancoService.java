package br.com.desenv.nepalign.service;

import java.util.List;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.SequenciaBaseBalanco;
import br.com.desenv.nepalign.persistence.SequenciaBaseBalancoPersistence;

public class SequenciaBaseBalancoService extends GenericServiceIGN<SequenciaBaseBalanco, SequenciaBaseBalancoPersistence> {

	@Override
	public SequenciaBaseBalanco salvar(SequenciaBaseBalanco entidade) throws Exception {
		
		if (entidade.getSequenciaBase() == null || entidade.getSequenciaBase().equals("") || (entidade.getSequenciaBase() % 10000) != 0) {
			throw new Exception("Sequência Base deve ser multiplo de 10000.");
		}

		if (entidade.getId() != null) {
			entidade.setId(null);
		} else {
			entidade.setDescricao(entidade.getSequenciaBase().toString());
			List<SequenciaBaseBalanco> sequenciaBaseExistente = super.listarPorObjetoFiltro(entidade);
			
			if (sequenciaBaseExistente.size() != 0) {
				throw new Exception("Sequência Base " + entidade.getSequenciaBase().toString() + " já está cadastrada.");
			}
			
			return super.salvar(entidade);
		}
		
		return null;
	}
	
	public SequenciaBaseBalanco recuperar(SequenciaBaseBalanco entidade) throws Exception {
		
		if (entidade.getId() == null && entidade.getSequenciaBase() == null && entidade.getDescricao().isEmpty()) {
			throw new Exception("Informe uma Sequência Base válida.");
		} else {
			List<SequenciaBaseBalanco> sequenciaBaseExistente = super.listarPorObjetoFiltro(entidade);
			
			if (sequenciaBaseExistente.size() == 0) {
				return null;
			} else if (sequenciaBaseExistente.size() == 1) {
				return sequenciaBaseExistente.get(0);
			} else {
				throw new Exception("não foi possível recuperar a Sequência Base " + entidade.getSequenciaBase().toString() + ".");
			}
			
		}
		
	}

}
