package br.com.desenv.nepalign.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.mail.Address;
import javax.mail.internet.InternetAddress;

import br.com.desenv.nepalign.alertas.EnviarEmail;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.LogDuplicata;
import br.com.desenv.nepalign.util.DsvConstante;

public class RunnableEmailLog  implements Runnable{
	private List<LogDuplicata> lista;
	EmpresaFisica empresa;
	public RunnableEmailLog(List<LogDuplicata> lista,EmpresaFisica empresa){
		this.lista = lista;
		this.empresa = empresa;
	}
	@Override
	public void run() {
		try
		{
			String messagem = "";
			String from = DsvConstante.getParametrosSistema().get("EMAIL_LOJA");
			String password = DsvConstante.getParametrosSistema().get("SENHA_EMAIL_LOJA");
			String smtpServer = DsvConstante.getParametrosSistema().get("SMTP_LOJA");
			for (LogDuplicata logDuplicata : lista)
			{
				messagem += "<br />"+logDuplicata.getNomeUsuario()+" "+logDuplicata.getMotivo();
			}
			List<String> to = new ArrayList<String>();
			String[] emailFrom =  DsvConstante.getParametrosSistema().get("EMAIL_LOG").split(";");
			
			for (String email : emailFrom) {
				to.add(email);
			}
			
			Address[] addressTo = new Address[to.size()];
			
			for(int i = 0; i < addressTo.length; i++) {
				addressTo[i] = new InternetAddress(to.get(i));
			}
			
			if(addressTo.length > 0) 
			{
				String nomeLoja = empresa.getNomeConcatenado();
				String subject = "Log de Alteração de Cliente "+nomeLoja;
				
				EnviarEmail enviarAniversariante = new EnviarEmail();
				System.out.println("Enviando E-mail Log.");
				enviarAniversariante.enviarEmail(from, password, smtpServer, addressTo, subject, messagem, nomeLoja, "");
				System.out.println("Enviou Email Log: "+new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
			}
		
		}
		catch(Exception ex)
		{
			Logger log = Logger.getGlobal();
			log.fine("Erro Enviar E-Mail de log do cliente!");
			ex.printStackTrace();
		}
		
	}

}
