package br.com.desenv.nepalign.service;

import java.util.List;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.IgnParametrosSistema;
import br.com.desenv.nepalign.model.ParametrosSistema;
import br.com.desenv.nepalign.persistence.ParametrosSistemaPersistence;

public class ParametrosSistemaService extends GenericServiceIGN<ParametrosSistema, ParametrosSistemaPersistence>
{
	public ParametrosSistemaService() 
	{
		super();
	}
	
	public void carregarParametros() throws Exception
	{
		new IgnParametrosSistema().carregarParametros();
	}
	
	public ParametrosSistema getParametro(String chave) throws Exception
	{
		ParametrosSistema param = new ParametrosSistema();
		param.setDescricao(chave);
		
		List<ParametrosSistema> listaParametrosSistema = listarPorObjetoFiltro(param);
		
		if(listaParametrosSistema.size() < 0x01)
			return null;
		else
			return listaParametrosSistema.get(0x00);
	}
}