package br.com.desenv.nepalign.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.SimpleFormatter;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao;
import br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao.Grupo;
import br.com.desenv.nepalign.model.ParametroSistema;
import br.com.desenv.nepalign.persistence.ParametroSistemaPersistence;
import br.com.desenv.nepalign.util.DsvConstante;

public class ParametroSistemaService extends GenericServiceIGN<ParametroSistema, ParametroSistemaPersistence>
{

	public ParametroSistemaService() 
	{
		super();
	}
	
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Caixa},nomeAcao="Recuperar Data Implantacao Seralle")
	public Date recuperarDataImplantacaoSeralle(Integer idEmpresaFisica) throws Exception
	{
		String dataParametro = "";
		Date dataRecuperada = null;
		try{
			if(DsvConstante.getParametrosSistema().get("dataSeralle") != null)
				dataParametro = DsvConstante.getParametrosSistema().get("dataSeralle");
			else
				dataRecuperada = new Date();
		}
		catch(Exception ex){
			throw new Exception("Não foi possível recuperar o parametro de sistema: dataSeralle");
		}
		
		if(!dataParametro.equals(""))
			dataRecuperada =  IgnUtil.dateFormat.parse(dataParametro);
		
		return dataRecuperada;
	}
	
	

}

