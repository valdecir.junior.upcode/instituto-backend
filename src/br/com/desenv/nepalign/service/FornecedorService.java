package br.com.desenv.nepalign.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.encoding.XMLType;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.Atividade;
import br.com.desenv.nepalign.model.CategoriaFornecedor;
import br.com.desenv.nepalign.model.Cidade;
import br.com.desenv.nepalign.model.ClassificacaoFornecedor;
import br.com.desenv.nepalign.model.Fornecedor;
import br.com.desenv.nepalign.model.Marca;
import br.com.desenv.nepalign.persistence.FornecedorPersistence;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class FornecedorService extends GenericServiceIGN<Fornecedor, FornecedorPersistence>
{

	public FornecedorService() 
	{
		super();
	}
	public Fornecedor recuperarFornecedorEscritorio(Integer idFornecedor) throws Exception
	{
		return recuperarFornecedorEscritorio(idFornecedor, null, null);
	}
	public Fornecedor recuperarFornecedorEscritorio(Integer idFornecedor,EntityManager manager, EntityTransaction transaction ) throws Exception
	{
		Boolean transacaoIndependente = false;
		Fornecedor fornecedor = null;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}

			if(DsvConstante.getParametrosSistema().get("caminhoWebServiceVendaEscritorio")==null)
			{
				return fornecedor;
			}
			String endpoint = DsvConstante.getParametrosSistema().get("caminhoWebServiceVendaEscritorio");
			Service  service = new Service();
			Call call= (Call) service.createCall();
			call.setTargetEndpointAddress(new java.net.URL(endpoint));
			call.setOperationName("recuperaFornecedor");
			call.addParameter("idFornecedor", XMLType.XSD_INT, ParameterMode.IN);
			call.setReturnType(XMLType.XSD_STRING);

			String retorno = (String) call.invoke(new Object[] {idFornecedor});
			String[] arr = retorno.split(",");
			fornecedor = new Fornecedor();
			
			if(arr[0].equals("null"))
			{
				fornecedor.setAtividade(null);	
			}
			else
			{
				Atividade atividade = new AtividadeService().recuperarPorId(Integer.parseInt(arr[0]));
				fornecedor.setAtividade(atividade);
			}
			fornecedor.setBairro(arr[1]);
			if(arr[2].equals("null"))
			{
				fornecedor.setCategoriaFornecedor(null);	
			}
			else
			{
				CategoriaFornecedor categoriaFornecedor = new CategoriaFornecedorService().recuperarPorId(Integer.parseInt(arr[2]));
				fornecedor.setCategoriaFornecedor(categoriaFornecedor);
			}
		
			fornecedor.setCep(arr[3]);
			if(arr[4].equals("null"))
			{
				fornecedor.setCidade(null);	
			}
			else
			{
				Cidade cidade = new CidadeService().recuperarPorId(Integer.parseInt(arr[4]));
				fornecedor.setCidade(cidade);
			}
			if(arr[5].equals("null"))
			{
				fornecedor.setCidade(null);	
			}
			else
			{
				ClassificacaoFornecedor classificacaoFornecedor = new ClassificacaoFornecedorService().recuperarPorId(Integer.parseInt(arr[5]));
				fornecedor.setClassificacaoFornecedor(classificacaoFornecedor);
			}
			fornecedor.setCnpjCpf(arr[6]);
			fornecedor.setComentario(arr[7]);
			fornecedor.setComplementoEndereco(arr[8]);
			fornecedor.setContato(arr[9]);
			fornecedor.setEmail(arr[10]);
			fornecedor.setEnderecoFornecedor(arr[11]);
			fornecedor.setFax(arr[12]);
			fornecedor.setInscricaoEstadualRg(arr[13]);
			fornecedor.setInscricaoMunicipal(arr[14]);
			fornecedor.setNomeEtiqueta(arr[15]);
			fornecedor.setNomeFantasia(arr[16]);
			fornecedor.setObservacao(arr[17]);
			fornecedor.setRazaoSocial(arr[18]);
			fornecedor.setTelefone(arr[19]);
			fornecedor.setTipoFornecedor(arr[20]);
			System.out.println("Usou WebService de Fornecedor:"+idFornecedor);
			fornecedor = this.salvar(manager, transaction, fornecedor);
			if(transacaoIndependente)
				transaction.commit();

		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
		return fornecedor;
	}
	

}

