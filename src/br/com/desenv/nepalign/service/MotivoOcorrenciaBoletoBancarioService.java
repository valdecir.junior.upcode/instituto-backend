package br.com.desenv.nepalign.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.MotivoOcorrenciaBoletoBancario;
import br.com.desenv.nepalign.model.OcorrenciaBoletoBancario;
import br.com.desenv.nepalign.persistence.MotivoOcorrenciaBoletoBancarioPersistence;

public class MotivoOcorrenciaBoletoBancarioService extends GenericServiceIGN<MotivoOcorrenciaBoletoBancario, MotivoOcorrenciaBoletoBancarioPersistence>
{

	public MotivoOcorrenciaBoletoBancarioService() 
	{
		super();
	}
	public MotivoOcorrenciaBoletoBancario recuperarPorCodigo(String codigoMotivo) throws Exception
	{
		return this.recuperarPorCodigo(codigoMotivo, null, null);
	}
	public MotivoOcorrenciaBoletoBancario recuperarPorCodigo(String codigoMotivo,EntityManager manager, EntityTransaction transaction) throws Exception
	{
		MotivoOcorrenciaBoletoBancario motivoOcorrenciaBoletoBancario = new MotivoOcorrenciaBoletoBancario();
		motivoOcorrenciaBoletoBancario.setCodigoMotivo(codigoMotivo);
		
		List<MotivoOcorrenciaBoletoBancario> lista = listarPorObjetoFiltro(manager, motivoOcorrenciaBoletoBancario, null, null, null);
		if(lista.size() > 0)
			return lista.get(0);
		else
			return null;
	}
	

}

