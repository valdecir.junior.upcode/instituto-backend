package br.com.desenv.nepalign.service;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.TipoMovimentoCaixa;
import br.com.desenv.nepalign.persistence.TipoMovimentoCaixaPersistence;

public class TipoMovimentoCaixaService extends GenericServiceIGN<TipoMovimentoCaixa, TipoMovimentoCaixaPersistence>
{
	public TipoMovimentoCaixaService() { super(); }
}