package br.com.desenv.nepalign.service;

import java.util.TimerTask;

import br.com.desenv.nepalign.model.AlertaSenhaGerente;
import br.com.desenv.nepalign.model.Usuario;

public class AlertaSenhaGerenteServiceWorker extends TimerTask
{
	public boolean resultado;
	public Usuario usuario = null;
	private AlertaSenhaGerente alerta = null;
	public AlertaSenhaGerenteServiceWorker(AlertaSenhaGerente alerta) 
	{
		super();
		this.alerta = alerta;
	}

	@Override
	public void run() 
	{
		try 
		{
			synchronized(this)
			{
				alerta = new AlertaSenhaGerenteService().recuperarPorId(this.alerta.getId());
				if(alerta.getSituacao().equals("S"))
				{
					this.usuario = alerta.getUsuarioAutorizador();
					this.notifyAll();
				}
				else
				{
					this.usuario = null;;		
				}	
			}
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   
	}
}

