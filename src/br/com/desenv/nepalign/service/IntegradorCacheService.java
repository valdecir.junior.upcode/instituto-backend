
package br.com.desenv.nepalign.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.batik.dom.util.HashTable;
import org.apache.poi.hssf.record.crypto.Biff8DecryptingStream;

import com.kscbrasil.itemEstoqueProduto.model.ItemEstoqueProduto;

import br.com.desenv.frameworkignorante.ListaEmpresaEntidadeChave;
import br.com.desenv.frameworkignorante.Page;
import br.com.desenv.nepalign.model.Cor;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.Estoque;
import br.com.desenv.nepalign.model.EstoqueProduto;
import br.com.desenv.nepalign.model.Fornecedor;
import br.com.desenv.nepalign.model.GrupoProduto;
import br.com.desenv.nepalign.model.ItemMovimentacaoEstoque;
import br.com.desenv.nepalign.model.Marca;
import br.com.desenv.nepalign.model.MovimentacaoEstoque;
import br.com.desenv.nepalign.model.OrdemProduto;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.model.SaldoEstoqueProduto;
import br.com.desenv.nepalign.model.StatusMovimentacaoEstoque;
import br.com.desenv.nepalign.model.Tamanho;
import br.com.desenv.nepalign.model.TipoDocumentoMovimentoEstoque;
import br.com.desenv.nepalign.model.TipoMovimentacaoEstoque;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class IntegradorCacheService 
{
	
	private StringBuffer registros;
	private String sp;
	private String sn;
	
	public void gerarRegistrosParaAtualizacao(Date dataRegistros) throws Exception
	{
		registros = new StringBuffer();
		sp = "^";
		char separador[] = {22};
		sn = new String(separador);
		SimpleDateFormat sdfArquivo = new SimpleDateFormat("yyyyMMdd_HHmmss");
		
		MovimentacaoEstoque criterioMovimentacao;
		MovimentacaoEstoqueService movimentoService = new MovimentacaoEstoqueService();
		Produto criterioProduto = new Produto();
		ProdutoService pService = new ProdutoService();
		EstoqueProduto estoqueProduto;
		ItemMovimentacaoEstoqueService itemMovService = new ItemMovimentacaoEstoqueService();
		ItemMovimentacaoEstoque criterioItem = new ItemMovimentacaoEstoque();
		
		List <ItemMovimentacaoEstoque> listaItensMovimentacao;
		HashMap<Integer, Marca> listaMarcas = new HashMap<Integer, Marca>();
		HashMap<Integer, Produto> listaProdutos = new HashMap<Integer, Produto>();
		HashMap<Integer, Cor> listaCor = new HashMap<Integer, Cor>();
		HashMap<Integer, Tamanho> listaTamanho = new HashMap<Integer, Tamanho>();
		HashMap<Integer, EstoqueProduto> listaEstoqueProduto = new HashMap<Integer, EstoqueProduto>();
		HashMap<Integer, OrdemProduto> listaOrdemProduto = new HashMap<Integer, OrdemProduto>();
		HashMap<Integer, Fornecedor> listaFornecedores = new HashMap<Integer, Fornecedor>();
		HashMap<Integer, SaldoEstoqueProduto> listaSaldo = new HashMap<Integer, SaldoEstoqueProduto>();
		HashMap<Integer, GrupoProduto> listaGrupos = new HashMap<Integer, GrupoProduto>();
		
		SaldoEstoqueProduto criterioSep;
		SaldoEstoqueProdutoService sepService = new SaldoEstoqueProdutoService();
		SaldoEstoqueProduto sep;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
		//busca movimentos do dia
		
		try
		{
			criterioMovimentacao = new MovimentacaoEstoque();
			criterioMovimentacao.setDataMovimentacao(dataRegistros);
			Page<MovimentacaoEstoque> listaMovimentacao = movimentoService.listarPaginadoSqlLivreGenerico(0, 0, " dataMovimentacao >= '" + sdf.format(dataRegistros) 
					+ " 00:00:00' AND dataMovimentacao <= '" + sdf.format(dataRegistros) + " 23:59:59'");
			
			for (MovimentacaoEstoque movimentacaoEstoque : listaMovimentacao.getRecordList()) 
			{
				criterioItem = new ItemMovimentacaoEstoque();
				criterioItem.setMovimentacaoEstoque(movimentacaoEstoque);
				listaItensMovimentacao = itemMovService.listarPorObjetoFiltro(criterioItem);
				
				for (ItemMovimentacaoEstoque itemMovimentacaoEstoque : listaItensMovimentacao) 
				{
					estoqueProduto = itemMovimentacaoEstoque.getEstoqueProduto();
					listaProdutos.put(estoqueProduto.getProduto().getId(), estoqueProduto.getProduto());
					listaMarcas.put(estoqueProduto.getProduto().getMarca().getId(), estoqueProduto.getProduto().getMarca());
					listaCor.put(estoqueProduto.getCor().getId(), estoqueProduto.getCor());
					listaTamanho.put(estoqueProduto.getTamanho().getId(), estoqueProduto.getTamanho());
					listaOrdemProduto.put(estoqueProduto.getOrdemProduto().getId(), estoqueProduto.getOrdemProduto());
					listaFornecedores.put(estoqueProduto.getProduto().getFornecedor().getId(), estoqueProduto.getProduto().getFornecedor());
					listaEstoqueProduto.put(estoqueProduto.getId(), estoqueProduto);
					listaGrupos.put(estoqueProduto.getProduto().getGrupo().getId(), estoqueProduto.getProduto().getGrupo());
					//recupera saldo estoque produto
					criterioSep = new SaldoEstoqueProduto();
					criterioSep.setEstoqueProduto(estoqueProduto);
					criterioSep.setEmpresaFisica(movimentacaoEstoque.getEmpresaFisica());
					try
					{
						sep = sepService.listarPorObjetoFiltro(criterioSep).get(0);
						listaSaldo.put(sep.getId(), sep);
					}
					catch(Exception exsep)
					{
						System.out.println("preço de Venda do produto não foi encontrado. Empresa: " + movimentacaoEstoque.getEmpresaFisica() + ", Estoque produto: " + estoqueProduto.getCodigoBarras());
					}
				}
			}
			gerarRegistroProduto(new ArrayList<Produto>(listaProdutos.values()));
			gerarRegistrosCor(new ArrayList<Cor>(listaCor.values()));
			gerarRegistrosEstoqueProduto(new ArrayList<EstoqueProduto>(listaEstoqueProduto.values()));
			gerarRegistrosGrupos(new ArrayList<GrupoProduto>(listaGrupos.values()));
			gerarRegistrosMarca(new ArrayList<Marca>(listaMarcas.values()));
			gerarRegistrosOrdem(new ArrayList<OrdemProduto>(listaOrdemProduto.values()));
			gerarRegistrosSaldoEstoqueProduto(new ArrayList<SaldoEstoqueProduto>(listaSaldo.values()));
			
			FileWriter arquivo = new FileWriter(new File("c:/desenv/arquivoslojas/exportacao/registroscache"+ sdf.format(dataRegistros) + " " + sdfArquivo.format(new Date()) +".txt"));
			arquivo.append(registros.toString());
			arquivo.flush();
			arquivo.close();
			
		}
		catch(Exception ex)
		{
			throw ex;
		}
		
	}
	public void gerarRegistrosParaAtualizacaoPorSEP(Date dataInicialRegistros, Date dataFinalRegistros) throws Exception
	{
		registros = new StringBuffer();
		sp = "^";
		char separador[] = {22};
		sn = new String(separador);
		SimpleDateFormat sdfArquivo = new SimpleDateFormat("yyyyMMdd_HHmmss");
		
		MovimentacaoEstoque criterioMovimentacao;
		MovimentacaoEstoqueService movimentoService = new MovimentacaoEstoqueService();
		Produto criterioProduto = new Produto();
		ProdutoService pService = new ProdutoService();
		EstoqueProduto estoqueProduto;
		ItemMovimentacaoEstoqueService itemMovService = new ItemMovimentacaoEstoqueService();
		ItemMovimentacaoEstoque criterioItem = new ItemMovimentacaoEstoque();
		
		List <ItemMovimentacaoEstoque> listaItensMovimentacao;
		HashMap<Integer, Marca> listaMarcas = new HashMap<Integer, Marca>();
		HashMap<Integer, Produto> listaProdutos = new HashMap<Integer, Produto>();
		HashMap<Integer, Cor> listaCor = new HashMap<Integer, Cor>();
		HashMap<Integer, Tamanho> listaTamanho = new HashMap<Integer, Tamanho>();
		HashMap<Integer, EstoqueProduto> listaEstoqueProduto = new HashMap<Integer, EstoqueProduto>();
		HashMap<Integer, OrdemProduto> listaOrdemProduto = new HashMap<Integer, OrdemProduto>();
		HashMap<Integer, Fornecedor> listaFornecedores = new HashMap<Integer, Fornecedor>();
		HashMap<Integer, SaldoEstoqueProduto> listaSaldo = new HashMap<Integer, SaldoEstoqueProduto>();
		HashMap<Integer, GrupoProduto> listaGrupos = new HashMap<Integer, GrupoProduto>();
		
		SaldoEstoqueProduto criterioSep;
		SaldoEstoqueProdutoService sepService = new SaldoEstoqueProdutoService();
		SaldoEstoqueProduto sep;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
		//busca movimentos do dia
		
		try
		{
			//criterioMovimentacao = new MovimentacaoEstoque();
			//criterioMovimentacao.setDataMovimentacao(dataRegistros);
			/*Page<MovimentacaoEstoque> listaMovimentacao = movimentoService.listarPaginadoSqlLivreGenerico(0, 0, " dataMovimentacao >= '" + sdf.format(dataRegistros) 
					+ " 00:00:00' AND dataMovimentacao <= '" + sdf.format(dataRegistros) + " 23:59:59'");*/

			List<SaldoEstoqueProduto> listaSaldoEstoqueProduto = sepService.listarPaginadoSqlLivreGenerico(0, 0, " dataUltimaAlteracao >= '" + sdf.format(dataInicialRegistros) 
					+ " 00:00:00' AND dataUltimaAlteracao <= '" + sdf.format(dataFinalRegistros) + " 23:59:59'").getRecordList();
						
			for (SaldoEstoqueProduto saldoEstoqueProduto : listaSaldoEstoqueProduto) 
			{
				estoqueProduto = saldoEstoqueProduto.getEstoqueProduto();
				listaProdutos.put(estoqueProduto.getProduto().getId(), estoqueProduto.getProduto());
				listaMarcas.put(estoqueProduto.getProduto().getMarca().getId(), estoqueProduto.getProduto().getMarca());
				listaCor.put(estoqueProduto.getCor().getId(), estoqueProduto.getCor());
				listaTamanho.put(estoqueProduto.getTamanho().getId(), estoqueProduto.getTamanho());
				listaOrdemProduto.put(estoqueProduto.getOrdemProduto().getId(), estoqueProduto.getOrdemProduto());
				listaFornecedores.put(estoqueProduto.getProduto().getFornecedor().getId(), estoqueProduto.getProduto().getFornecedor());
				listaEstoqueProduto.put(estoqueProduto.getId(), estoqueProduto);
				listaGrupos.put(estoqueProduto.getProduto().getGrupo().getId(), estoqueProduto.getProduto().getGrupo());
				//recupera saldo estoque produto
				criterioSep = new SaldoEstoqueProduto();
				criterioSep.setEstoqueProduto(estoqueProduto);
				criterioSep.setEmpresaFisica(saldoEstoqueProduto.getEmpresaFisica());
				try
				{
					sep = sepService.listarPorObjetoFiltro(criterioSep).get(0);
					listaSaldo.put(sep.getId(), sep);
				}
				catch(Exception exsep)
				{
					System.out.println("preço de Venda do produto não foi encontrado. Empresa: " + saldoEstoqueProduto.getEmpresaFisica().getId() + ", Estoque produto: " + estoqueProduto.getCodigoBarras());
				}
			}
			gerarRegistroProduto(new ArrayList<Produto>(listaProdutos.values()));
			gerarRegistrosCor(new ArrayList<Cor>(listaCor.values()));
			gerarRegistrosEstoqueProduto(new ArrayList<EstoqueProduto>(listaEstoqueProduto.values()));
			gerarRegistrosGrupos(new ArrayList<GrupoProduto>(listaGrupos.values()));
			gerarRegistrosMarca(new ArrayList<Marca>(listaMarcas.values()));
			gerarRegistrosOrdem(new ArrayList<OrdemProduto>(listaOrdemProduto.values()));
			gerarRegistrosSaldoEstoqueProduto(new ArrayList<SaldoEstoqueProduto>(listaSaldo.values()));
			
			FileWriter arquivo = new FileWriter(new File("c:/desenv/arquivoslojas/exportacao/registroscacheSEP"+ sdf.format(dataInicialRegistros) +"_"+sdf.format(dataFinalRegistros) + " " + sdfArquivo.format(new Date()) +".txt"));
			arquivo.append(registros.toString());
			arquivo.flush();
			arquivo.close();
			
		}
		catch(Exception ex)
		{
			throw ex;
		}
		
	}	
	public void gerarRegistrosParaAtualizacaoGeral() throws Exception
	{
		registros = new StringBuffer();
		sp = "^";
		char separador[] = {22};
		sn = new String(separador);
		SimpleDateFormat sdfArquivo = new SimpleDateFormat("yyyyMMdd_HHmmss");
		
		MovimentacaoEstoque criterioMovimentacao;
		MovimentacaoEstoqueService movimentoService = new MovimentacaoEstoqueService();
		Produto criterioProduto = new Produto();
		ProdutoService pService = new ProdutoService();
		EstoqueProduto estoqueProduto;
		ItemMovimentacaoEstoqueService itemMovService = new ItemMovimentacaoEstoqueService();
		ItemMovimentacaoEstoque criterioItem = new ItemMovimentacaoEstoque();
		
		List <ItemMovimentacaoEstoque> listaItensMovimentacao;
		HashMap<Integer, Marca> listaMarcas = new HashMap<Integer, Marca>();
		HashMap<Integer, Produto> listaProdutos = new HashMap<Integer, Produto>();
		HashMap<Integer, Cor> listaCor = new HashMap<Integer, Cor>();
		HashMap<Integer, Tamanho> listaTamanho = new HashMap<Integer, Tamanho>();
		HashMap<Integer, EstoqueProduto> listaEstoqueProduto = new HashMap<Integer, EstoqueProduto>();
		HashMap<Integer, OrdemProduto> listaOrdemProduto = new HashMap<Integer, OrdemProduto>();
		HashMap<Integer, Fornecedor> listaFornecedores = new HashMap<Integer, Fornecedor>();
		HashMap<Integer, SaldoEstoqueProduto> listaSaldo = new HashMap<Integer, SaldoEstoqueProduto>();
		HashMap<Integer, GrupoProduto> listaGrupos = new HashMap<Integer, GrupoProduto>();
		
		SaldoEstoqueProduto criterioSep;
		SaldoEstoqueProdutoService sepService = new SaldoEstoqueProdutoService();
		SaldoEstoqueProduto sep;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		ProdutoService produtoService = new ProdutoService();
		
		//busca movimentos do dia
		//
		EmpresaFisica empresa = new EmpresaFisica();
		List<EmpresaFisica> listaEmpresaFisica = new EmpresaFisicaService().listarPaginadoSqlLivreGenerico(0, 0, " idEmpresaFisica<50 and idEmpresaFisica<>6 ").getRecordList();
		
		try
		{
			List<Produto> listaProdutosGerais = produtoService.listarPaginadoSqlLivreGenerico(0, 0, " idProduto <= 70000 ", "", "").getRecordList();
			
			for (Produto produto : listaProdutosGerais) 
			{
				for (EmpresaFisica empresaFisica : listaEmpresaFisica) 
				{
					List<SaldoEstoqueProduto> sepLista = new SaldoEstoqueProdutoService().listarPorObjetoFiltroProdutoSql(produto, empresaFisica);
				
					for (SaldoEstoqueProduto saldoEstoqueProduto : sepLista) 
					{
						estoqueProduto = saldoEstoqueProduto.getEstoqueProduto();
						sep = saldoEstoqueProduto;
						listaProdutos.put(estoqueProduto.getProduto().getId(), estoqueProduto.getProduto());
						listaMarcas.put(estoqueProduto.getProduto().getMarca().getId(), estoqueProduto.getProduto().getMarca());
						listaCor.put(estoqueProduto.getCor().getId(), estoqueProduto.getCor());
						listaTamanho.put(estoqueProduto.getTamanho().getId(), estoqueProduto.getTamanho());
						listaOrdemProduto.put(estoqueProduto.getOrdemProduto().getId(), estoqueProduto.getOrdemProduto());
						listaFornecedores.put(estoqueProduto.getProduto().getFornecedor().getId(), estoqueProduto.getProduto().getFornecedor());
						listaEstoqueProduto.put(estoqueProduto.getId(), estoqueProduto);
						listaGrupos.put(estoqueProduto.getProduto().getGrupo().getId(), estoqueProduto.getProduto().getGrupo());
						listaSaldo.put(sep.getId(), sep);
						System.out.println(sep.getId() + " " + sep.getEmpresaFisica().getId() + " " + sep.getEstoqueProduto().getProduto().getId());
					}
				}
			}
			gerarRegistroProduto(new ArrayList<Produto>(listaProdutos.values()));
			gerarRegistrosCor(new ArrayList<Cor>(listaCor.values()));
			gerarRegistrosEstoqueProduto(new ArrayList<EstoqueProduto>(listaEstoqueProduto.values()));
			gerarRegistrosGrupos(new ArrayList<GrupoProduto>(listaGrupos.values()));
			gerarRegistrosMarca(new ArrayList<Marca>(listaMarcas.values()));
			gerarRegistrosOrdem(new ArrayList<OrdemProduto>(listaOrdemProduto.values()));
			gerarRegistrosSaldoEstoqueProduto(new ArrayList<SaldoEstoqueProduto>(listaSaldo.values()));
			
			FileWriter arquivo = new FileWriter(new File("c:/desenv/arquivoslojas/exportacao/registroscacheGeral"+ sdf.format(new Date()) + " " + sdfArquivo.format(new Date()) +".txt"));
			arquivo.append(registros.toString());
			arquivo.flush();
			arquivo.close();
			
		}
		catch(Exception ex)
		{
			throw ex;
		}
		
	}	
	
	private void gerarRegistrosTamanho(List<Tamanho> listaTamanhos) throws Exception
	{
		String chaves;
		String infos;
		
		for (Tamanho tamanho : listaTamanhos) 
		{
			chaves = tamanho.getId().toString();
			infos = tamanho.getDescricao();
			adicionarRegistro("S", "CETAM", chaves, infos);
		}
	}		
	private void gerarRegistrosMarca(List<Marca> listaMarcas) throws Exception
	{
		String chaves;
		String infos;
		
		for (Marca marca : listaMarcas) 
		{
			chaves = marca.getId().toString();
			infos = marca.getDescricao();
			adicionarRegistro("S", "CEMAR", chaves, infos);
		}
	}		
	private void gerarRegistrosGrupos(List<GrupoProduto> listaGrupos) throws Exception
	{
		String chaves;
		String infos;
		
		for (GrupoProduto grupoProduto : listaGrupos) 
		{
			chaves = grupoProduto.getId().toString();
			infos = grupoProduto.getDescricao();
			adicionarRegistro("S", "CEGRU", chaves, infos);
		}
	}	
	private void gerarRegistrosOrdem(List<OrdemProduto> listaOrdens) throws Exception
	{
		String chaves;
		String infos;
		
		for (OrdemProduto ordem : listaOrdens) 
		{
			chaves = ordem.getId().toString();
			infos = ordem.getDescricao();
			adicionarRegistro("S", "CETIPM", chaves, infos);
		}
	}	
	private void gerarRegistrosCor(List<Cor> listaCores) throws Exception
	{
		String chaves;
		String infos;
		
		for (Cor cor : listaCores) 
		{
			chaves = cor.getId().toString();
			infos = cor.getDescricaoCorEmpresa();
			adicionarRegistro("S", "CECOR", chaves, infos);
		}
	}
	
	private void gerarRegistrosSaldoEstoqueProduto(List<SaldoEstoqueProduto> listaSaldos) throws Exception
	{
		String chaves;
		String infos;
		String chaves2;
		String infos2;
		
		for (SaldoEstoqueProduto saldoEstoqueProduto : listaSaldos) 
		{
			chaves = saldoEstoqueProduto.getEmpresaFisica().getId()+sn+
						saldoEstoqueProduto.getEstoqueProduto().getId();
			
			if (saldoEstoqueProduto.getVenda()==null || saldoEstoqueProduto.getVenda().doubleValue()==0)
			{
				System.out.println("Produto sem valor de venda. Codigo de Barras: " + saldoEstoqueProduto.getId() );
				infos = "0.0";
			}
			else
			{
				infos = saldoEstoqueProduto.getVenda().toString();
			}
			
			chaves2 = saldoEstoqueProduto.getEmpresaFisica().getId()+sn+
					saldoEstoqueProduto.getEstoqueProduto().getId()+sn+saldoEstoqueProduto.getEmpresaFisica().getMesAberto();
			
			if (saldoEstoqueProduto.getSaldo()==null)
			{
				System.out.println("Produto sem valor de custo e com saldo nulo. Codigo de Barras: " + saldoEstoqueProduto.getId() );
				infos2 = "0"+sp+saldoEstoqueProduto.getCusto();
			}
			else if (saldoEstoqueProduto.getCusto()==null)
			{
				System.out.println("Produto sem valor de custo e com saldo nulo. Codigo de Barras: " + saldoEstoqueProduto.getId() );
				infos2 = saldoEstoqueProduto.getSaldo().intValue()+sp+"0";				
			}
			else
			{
				infos2 = saldoEstoqueProduto.getSaldo().intValue()+sp+saldoEstoqueProduto.getCusto();
			}
			
			adicionarRegistro("S", "CEPRO", chaves, infos);
			adicionarRegistro("S", "CEPRO", chaves2, infos2);
			
		}
	}
	
	private void gerarRegistrosEstoqueProduto(List<EstoqueProduto> listaEstoqueProduto) throws Exception
	{
		String chaves1;
		String infos1;
		String chaves2;
		String infos2;		
		for (EstoqueProduto estoqueProduto : listaEstoqueProduto) 
		{
			chaves1 = estoqueProduto.getId().toString();
			infos1 = estoqueProduto.getProduto().getId()+sp+
						estoqueProduto.getOrdemProduto().getId()+sp+
						estoqueProduto.getCor().getId()+sp+
						estoqueProduto.getTamanho().getId();
			
			chaves2 = estoqueProduto.getProduto().getId()+sn+
						estoqueProduto.getOrdemProduto().getId()+sn+
						estoqueProduto.getCor().getId()+sn+
						estoqueProduto.getTamanho().getId();
			infos2 = estoqueProduto.getId().toString();
			
			adicionarRegistro("S", "CECADIDP", chaves1, infos1);
			adicionarRegistro("S", "CECADIDR", chaves2, infos2);
			
		}
	}
	
	private void gerarRegistroProduto(List<Produto> listaProdutos) throws Exception
	{
		String chaves;
		String infos;
		String ncm;
		for (Produto produto : listaProdutos) 
		{
			//trata produto sem NCM
			if (produto.getCodigoNcm()==null)
				produto.setCodigoNcm("99999999");
			
			chaves = produto.getId().toString();
			infos = produto.getNomeProduto()+sp+
					""+sp+
					produto.getMarca().getId()+sp+
					produto.getGrupo().getId()+sp+
					produto.getFornecedor().getId()+sp+
					produto.getUnidadeMedida().getSigla()+sp+
					""+sp+
					""+sp+
					""+sp+
					""+sp+
					""+sp+
					""+sp+
					""+sp+
					""+sp+
					""+sp+
					""+sp+
					""+sp+
					(produto.getCodigoNcm().trim().equals("99999999")?"0":produto.getCodigoNcm().trim())+
					"";
			adicionarRegistro("S", "CECAD", chaves, infos);
		}
	}
	private void adicionarRegistro(String tipo,String global, String chaves, String infos) throws Exception
	{
		String linha = tipo+sp+
						global+sp+
						chaves+sp+
						infos;
		registros.append(linha+"\r\n");
	}
	
	private void gerarMovimentacaoEstoqueLojaDia(int loja, String caminho, String nomeArquivo) throws Exception
	{
		FileReader reader = null;
		BufferedReader bufferedReader = null;
		String linha = null;
		MovimentacaoEstoque mov;
		MovimentacaoEstoqueService movService = new MovimentacaoEstoqueService();
		ItemMovimentacaoEstoque itMov;
		String valores[];
		int tipoRegistro;
		Date dataArquivo=null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		FileWriter fwLog = new FileWriter(caminho+"log"+sdf.format(new Date())+".txt",true);
		fwLog.write(new Date()+" ========================================================================"+"\r\n");
		fwLog.write(nomeArquivo+"\r\n");
		EntityManager entity = ConexaoUtil.getEntityManager();
		EntityTransaction transaction = entity.getTransaction();
		transaction.begin();
		File arquivo=null;
				
		try
		{
			EmpresaFisica empresa = new EmpresaFisica();
			empresa.setId(loja);
			empresa = new EmpresaFisicaService().recuperarPorId(loja);
			try
			{
				arquivo = new File(caminho+nomeArquivo);
				reader = new FileReader(arquivo);
			}
			catch(FileNotFoundException fnfe)
			{
				throw new Exception(fnfe);
			}						
			
			bufferedReader = new BufferedReader(reader);
			Estoque estoque = new Estoque();
			estoque.setId(1);
			List<MovimentacaoEstoque> movimentacoes = new ArrayList<MovimentacaoEstoque>();
			HashMap<String, List<ItemMovimentacaoEstoque>> itensMovimentacao = new HashMap<String, List<ItemMovimentacaoEstoque>>();
			HashMap<String, List<ItemMovimentacaoEstoque>> itensMovimentacaoMenos = new HashMap<String, List<ItemMovimentacaoEstoque>>();
			int seq = 0;
			int seq2 = 0;
			List<ItemMovimentacaoEstoque> itens = new ArrayList<ItemMovimentacaoEstoque>();
			List<ItemMovimentacaoEstoque> itensMenos = new ArrayList<ItemMovimentacaoEstoque>();
			
			while (bufferedReader.ready())
			{
				linha = bufferedReader.readLine();
				if (linha!=null && linha.equals("")==false)
				{
					if (linha.trim().equals("FIM"))
					{
						System.out.println("Achou registro de fim");
						fwLog.write("Achou registro de fim"+"\r\n");
					}
					else
					{
						valores = linha.split("\\^");
						tipoRegistro = Integer.parseInt(valores[1]);
						if (tipoRegistro==0) //MovimentacaoEstoque
						{
							mov = gerarObjetoMovimentacaoEstoque(empresa, estoque, valores);
							dataArquivo = mov.getDataMovimentacao();
							//se for troca muda tipo de movimentação
							if (mov.getTipoMovimentacaoEstoque().getId()==6)
							{
								mov.setEntradaSaida("E");
								mov.getTipoMovimentacaoEstoque().setId(5);
								mov.getTipoDocumentoMovimento().setId(1);
							}
							movimentacoes.add(mov);
							itens = new ArrayList<ItemMovimentacaoEstoque>();
							itensMovimentacao.put(mov.getNumeroDocumento(), itens);
							if (Integer.parseInt(valores[6])==2)
							{
								mov = gerarObjetoMovimentacaoEstoque(empresa, estoque, valores);
								mov.setNumeroDocumento(mov.getNumeroDocumento()+"S");
								movimentacoes.add(mov);
								
								itensMenos = new ArrayList<ItemMovimentacaoEstoque>();
								itensMovimentacaoMenos.put(mov.getNumeroDocumento(), itensMenos);
							}
							
						}
						else //item Movimentacao
						{
							itMov = gerarObjetoItemMovimentacaoEstoque(empresa, estoque, valores, fwLog);
							if (valores.length>7)
							{
								String maisOuMenos = valores[7];
								if (maisOuMenos.equals("-"))
									itensMenos.add(itMov);
								else
									itens.add(itMov);
							}
							else
							{
								itens.add(itMov);
							}
							//veririfica se nao tem o preco de venda e preco de custo na CEPRO
						}
					}
				}
			}
			
			//Grava Movimentações
			int qtPedidos=0;
			int qtTrocasEntrada=0;
			int qtTrocasSaida=0;
			int qtItens=0;
			double valorTotalPedidos=0.0;
			
		
			for (MovimentacaoEstoque movimentacao : movimentacoes) 
			{
				//verifica se ja existe
				mov = new MovimentacaoEstoque();
				mov.setEmpresaFisica(movimentacao.getEmpresaFisica());
				mov.setNumeroDocumento(movimentacao.getNumeroDocumento());
				mov.setDataMovimentacao(movimentacao.getDataMovimentacao());
				List<MovimentacaoEstoque> lista = movService.listarPorObjetoFiltro(entity, mov,null,null,null);
				if (lista.size()==0)
				{
					mov = movService.salvar(entity, transaction, movimentacao,false);				
					seq=0;
					if (mov.getTipoMovimentacaoEstoque().getId()==2 || mov.getTipoMovimentacaoEstoque().getId()==5)
					{
						if (mov.getTipoMovimentacaoEstoque().getId()==2)
						{
							qtPedidos++;
						}
						else
							qtTrocasEntrada++;
						itens = itensMovimentacao.get(mov.getNumeroDocumento());
						for (ItemMovimentacaoEstoque itemMovimentacaoEstoque : itens) 
						{
							qtItens++;
							seq++;
							itemMovimentacaoEstoque.setMovimentacaoEstoque(mov);
							itemMovimentacaoEstoque.setSequencialItem(seq);
							new ItemMovimentacaoEstoqueService().salvar(entity, transaction, itemMovimentacaoEstoque);
							valorTotalPedidos = valorTotalPedidos + (itemMovimentacaoEstoque.getValor()*itemMovimentacaoEstoque.getQuantidade());
						}
					}
					else if (mov.getTipoMovimentacaoEstoque().getId()==6)
					{
						qtTrocasSaida++;
						itens = itensMovimentacaoMenos.get(mov.getNumeroDocumento());
						for (ItemMovimentacaoEstoque itemMovimentacaoEstoque : itens) 
						{
							qtItens++;
							seq++;
							itemMovimentacaoEstoque.setMovimentacaoEstoque(mov);
							itemMovimentacaoEstoque.setSequencialItem(seq);
							new ItemMovimentacaoEstoqueService().salvar(entity, transaction, itemMovimentacaoEstoque);
						}					
					}
					else
					{
						transaction.rollback();
						System.out.println("Tipo de movimentação não mapeado para importação: "  + mov.getTipoMovimentacaoEstoque().getId());
						throw new Exception("Tipo de movimentação não mapeado para importação: " + mov.getTipoMovimentacaoEstoque().getId());
					}
				}
				
				else
				{
					System.out.println("Movimentação: " + movimentacao.getNumeroDocumento()+" para empresa "+ movimentacao.getEmpresaFisica().getId()+ " já existe." );
				}
			}
			System.out.println("Total de Pedidos: " + qtPedidos);
			fwLog.write("Total de Pedidos: " + qtPedidos+"\r\n");

			System.out.println("Total de Trocas - Entrada: " + qtTrocasEntrada);
			fwLog.write("Total de Trocas - Entrada: " + qtTrocasEntrada+"\r\n");
			
			System.out.println("Total de Trocas - Saida: " + qtTrocasSaida);
			fwLog.write("Total de Trocas - Saida: " + qtTrocasSaida+"\r\n");
			
			System.out.println("Total de Itens Geral: " + qtItens);
			fwLog.write("Total de Itens Geral: " + qtItens+"\r\n");
			
			System.out.println("Valor Total de Pedidos de Venda: " + valorTotalPedidos);
			fwLog.write("Valor Total de Pedidos de Venda: " + valorTotalPedidos+"\r\n");
			
			transaction.commit();
			entity.close();
			bufferedReader.close();			
			reader.close();
			reader = null;		
			
			
			renomeiaArquivoImportado(arquivo, caminho, empresa, dataArquivo);
			
		}
		catch(Exception ex)
		{
			if (transaction!=null && transaction.isActive())
				transaction.rollback();
			if (entity!=null && entity.isOpen())
				entity.close();
			ex.printStackTrace();
		}
		finally
		{
			fwLog.close();
			if (entity!=null && entity.isOpen())
				entity.close();			
		}
	}
	
	public void renomeiaArquivoImportado(File arquivo, String caminho, EmpresaFisica empresa, Date dataArquivo) throws Exception
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd");
		SimpleDateFormat sdfImp = new SimpleDateFormat("yyyyMMddHHmmss");
		String novoNome = caminho+"PROCESSADO_ESTOQUE_"+empresa.getId()+"-" +sdf.format(dataArquivo)+"-" +sdfImp.format(new Date())+".txt";
		System.out.println(novoNome);
		arquivo.renameTo(new File(novoNome));
	}
	
	private MovimentacaoEstoque gerarObjetoMovimentacaoEstoque(EmpresaFisica empresa, Estoque estoque, String valores[]) throws Exception
	{
		
		MovimentacaoEstoque mov;
		
		mov = new MovimentacaoEstoque();
		mov.setDataMovimentacao(dataCacheParaJava(Integer.parseInt(valores[2])));
		String strEmpresa = valores[34].trim();
		if (empresa.getId().intValue()!=Integer.parseInt(strEmpresa))
		{
			throw new Exception("Empresa contida no registro do arquivo\n é diferente da empresa informada para importação.");
		}
		if (valores.length>17)
		{
			
			EmpresaFisica empresaDestino = new EmpresaFisica();
			if (valores[19].trim().equals("")==false)
			{
				empresaDestino.setId(Integer.parseInt(valores[19]));
				mov.setEmpresaDestino(empresaDestino);
			}
		}
		mov.setEmpresaFisica(empresa);
		mov.setEntradaSaida("S");
		mov.setEstoque(estoque);
		mov.setFaturado("N");
		//mov.setForncedor(forncedor)
		//mov.setIdDocumentoOrigem(idDocumentoOrigem)
		mov.setManual("N");
		mov.setNumeroDocumento(valores[0].trim()); // +"CV");
		mov.setObservacao("GERADO AUTOMATICAMENTE PELO SISTEMA INTEGRADOR.");
		StatusMovimentacaoEstoque status = new StatusMovimentacaoEstoque();
		status.setId(2);
		mov.setStatusMovimentacaoEstoque(status);
		TipoMovimentacaoEstoque tipoMov = new TipoMovimentacaoEstoque();
		tipoMov.setId(defineTipoMovimentacao(Integer.parseInt(valores[6])));
		mov.setTipoMovimentacaoEstoque(tipoMov);
		TipoDocumentoMovimentoEstoque tipoDoc = new TipoDocumentoMovimentoEstoque();
		tipoDoc.setId(defineTipoDocumento(Integer.parseInt(valores[6])));
		mov.setTipoDocumentoMovimento(tipoDoc);		
		
		return mov;
	}
	
	private ItemMovimentacaoEstoque gerarObjetoItemMovimentacaoEstoque(EmpresaFisica empresa, Estoque estoque, String valores[], FileWriter fwLog) throws Exception
	{
		
		ItemMovimentacaoEstoque itMov;
		Integer codigoBarras; 

		itMov = new ItemMovimentacaoEstoque();
		codigoBarras = Integer.parseInt(valores[2]);
		EstoqueProduto ep = new EstoqueProduto();
		ep.setId(codigoBarras);
		SaldoEstoqueProduto sep = new SaldoEstoqueProduto();
		sep.setEstoqueProduto(ep);
		try
		{
			sep = new SaldoEstoqueProdutoService().listarPorObjetoFiltro(sep).get(0);
		}
		catch(Exception exsep)
		{
			//quando nao existe saldo estoque produto
			ep = new EstoqueProdutoService().recuperarPorId(ep.getId());
			if (ep==null)
			{				
				System.out.println("Codigo de barras que não existe: " + codigoBarras);
				fwLog.write("Codigo de barras que não existe: " + codigoBarras+"\r\n");
			}
			
			sep = new SaldoEstoqueProduto();
			sep.setEstoqueProduto(ep);
			sep.setCusto(0.0);
		}
		itMov = new ItemMovimentacaoEstoque();
		itMov.setCusto(sep.getCusto());
		itMov.setDocumentoOrigem(valores[0]);
		itMov.setEstoqueProduto(sep.getEstoqueProduto());
		itMov.setProduto(sep.getEstoqueProduto().getProduto());
		itMov.setQuantidade(Double.parseDouble(valores[3]));
		itMov.setSequencialItem(1);
		itMov.setUnidadeMedida(sep.getEstoqueProduto().getProduto().getUnidadeMedida());
		try
		{
			if (valores.length>13)
				itMov.setValor(Double.parseDouble(valores[14]));
			else
			{
				Double preco = new Double(valores[5])/itMov.getQuantidade();
				BigDecimal precoArred = new BigDecimal(preco);
				precoArred = precoArred.setScale(2, RoundingMode.HALF_UP);
				itMov.setValor(precoArred.doubleValue());
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return itMov;

	}
	
	private Date dataCacheParaJava(int dataHora)
	{
		// DATA ZERO DO CACHE - 12/31/1840


		Calendar calendario = Calendar.getInstance();
		calendario.set(Calendar.YEAR, 1840);
		calendario.set(Calendar.MONTH, 11);
		calendario.set(Calendar.DAY_OF_MONTH, 31);

		calendario.set(Calendar.HOUR_OF_DAY, 0);
		calendario.set(Calendar.MINUTE, 0);
		calendario.set(Calendar.SECOND, 0);
		calendario.set(Calendar.MILLISECOND, 0);

		//dataHora = dataHora.replaceAll("[^0-9,]", "");
		
		calendario.add(Calendar.DAY_OF_MONTH, dataHora);


		return calendario.getTime();

	}	

	private int defineTipoMovimentacao(int tipoCache)
	{
		// Troca
		if (tipoCache==2)
		{
			return 6;
		}
		// Saida
		else if (tipoCache==1)
		{
			// Venda
			return 2;
		}
		else if (tipoCache==3 || tipoCache==4)
		{
			return 4;
		}
		else
		{
			return 2;
		}	
	}
	private int defineTipoDocumento(int tipoCache)
	{
		// Troca
		if (tipoCache==2)
		{
			return 2;
		}
		// Saida
		else if (tipoCache==1)
		{
			// Venda
			return 2;
		}
		else if (tipoCache==3 || tipoCache==4)
		{
			return 3;
		}
		else
		{
			return 1;
		}	
	}
	
	public String retornarArquivosPendentesMovimento(String caminhoArquivos) throws Exception
	{
		StringBuilder outBuffer = new StringBuilder();
		
		File diretorio = new File(caminhoArquivos);

		try
		{
			if (diretorio.isDirectory()==false)
				throw new Exception("O caminho especificado não é um diretorio ou não existe.");
	
			String arquivos[] = diretorio.list();
	
			for (int d=0;d<arquivos.length;d++)
			{
				String nomeArquivo = arquivos[d];
				if (nomeArquivo.length()>3 && nomeArquivo.substring(0,4).indexOf("ESTO")>-1)
					outBuffer.append(nomeArquivo + " \n");
			}
		}
		catch(Exception ex)
		{
			outBuffer.append("não foi possível recuperar a lista de arquivos pendentes!");
		}
		return outBuffer.toString();
	}
	
	public String importarArquivosMovimentacaoLoja(String caminhoArquivos) throws Exception
	{
		StringBuilder outBuffer = new StringBuilder();
		
		File diretorio = new File(caminhoArquivos);
		String nomeArquivo;
		
		if (diretorio.isDirectory()==false)
			throw new Exception("O caminho especificado não é um diretorio ou não existe.");
		
		int idEmpresaFisica;
		String arquivos[] = diretorio.list();
		
		try
		{
			for (int d=0;d<arquivos.length;d++)
			{
				nomeArquivo = arquivos[d];
				if (nomeArquivo.length()>3 && nomeArquivo.substring(0,4).indexOf("ESTO")>-1)
				{
					System.out.println("INICIO importacao arquivo: " + nomeArquivo + " " + new Date());
					outBuffer.append("INICIO importacao arquivo: " + nomeArquivo + " " + new Date() + " \n");
					idEmpresaFisica = Integer.parseInt(nomeArquivo.substring(7,9));
					gerarMovimentacaoEstoqueLojaDia(idEmpresaFisica, caminhoArquivos, nomeArquivo);
					System.out.println("FINAL importacao arquivo: " + nomeArquivo + " " + new Date());
					outBuffer.append("FINAL importacao arquivo: " + nomeArquivo + " " + new Date() + " \n");
				}
			}
			outBuffer.append("Finalizado com sucesso!");
		}
		catch(Exception ex)
		{
			outBuffer.append("Erro ao baixar movimento!! \n\n\n" + ex.getMessage());
			ex.printStackTrace();
		}
		
		return outBuffer.toString();
	}
	
	public static void main(String[] args) throws Exception 
	{
		IntegradorCacheService intService = new IntegradorCacheService();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		int idEmpresaFisica = 1; 
		String dataBase = "01/10/2014";
		String dataBaseFinal = "06/10/2014";
		int tipoGeracao = 3;
		
		ListaEmpresaEntidadeChave.getInstance();
		
		if (args.length>0)
		{
			idEmpresaFisica = Integer.parseInt(args[0]);
			dataBase = args[1];
			tipoGeracao = Integer.parseInt(args[2]);
			dataBaseFinal = args[3];
		}
		
		String caminho = "c:/desenv/arquivoslojas/";
		
		if (tipoGeracao==1)
			intService.gerarRegistrosParaAtualizacao(sdf.parse(dataBase));
		else if (tipoGeracao==2)
			intService.gerarMovimentacaoEstoqueLojaDia(idEmpresaFisica, caminho, "ESTOQUE");
		else if (tipoGeracao==3)
			intService.importarArquivosMovimentacaoLoja("c:/desenv/arquivoslojas/");
		else if (tipoGeracao==4)
			intService.gerarRegistrosParaAtualizacaoGeral();		
		else if (tipoGeracao==5)
			intService.gerarRegistrosParaAtualizacaoPorSEP(sdf.parse(dataBase), sdf.parse(dataBaseFinal));		
		
		//EmpresaFisica empresa = new EmpresaFisica(sb);
		//empresa.setId(1);
		//intService.renomeiaArquivoImportado(new File("c:/TMP/ESTOQUE"),"C:/TMP/", empresa, new Date());
	}
}

