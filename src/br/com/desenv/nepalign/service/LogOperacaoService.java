package br.com.desenv.nepalign.service;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager; 
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.LogOperacao;
import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.nepalign.persistence.LogOperacaoPersistence;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class LogOperacaoService extends GenericServiceIGN<LogOperacao, LogOperacaoPersistence>
{	
	private final int TIPOLOG_INCLUSAO 	= 1;
	private final int TIPOLOG_ALTERACAO = 2;  
	private final int TIPOLOG_EXCLUSAO	= 3;
	
	public Method gerarObservacaoMethod;
	public String agrupadorFixo;
	
	public void gravarOperacaoInclusao(EntityManager manager, EntityTransaction transaction, Object objInlcuido) throws Exception
	{
		gravarOperacaoInclusao(manager, transaction, objInlcuido, null);
	}
	
	public void gravarOperacaoInclusao(EntityManager manager, EntityTransaction transaction, Object objInlcuido, String observacao) throws Exception
	{
		Boolean transacaoIndependente = false;
		
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
 
			final Integer idEmpresaFisica = recuperarEmpresaFisicaLog(manager, objInlcuido);
			final String agrupador = agrupadorFixo == null ? this.gerarCodigoAgrupador(objInlcuido.getClass().getSimpleName()) : agrupadorFixo;
			
			LogOperacao logOperacao = new LogOperacao();
			 
			logOperacao.setData(new Date());
			logOperacao.setAgrupador(agrupador);
			logOperacao.setIdDocumentoOrigem(this.recuperarIdDocumentoOrigem(objInlcuido).toString());
			logOperacao.setNomeClasse(objInlcuido.getClass().getSimpleName());
			logOperacao.setTipoLog(TIPOLOG_INCLUSAO);
			
			if(observacao != null)
				logOperacao.setObservacao(observacao);
			else if(gerarObservacaoMethod != null)
			{
				Object service = recuperarServiceObject(objInlcuido);
				logOperacao.setObservacao((String) gerarObservacaoMethod.invoke(service, objInlcuido));
				service = null;
			}
			
			if(idEmpresaFisica != null)
				logOperacao.setEmpresaFisica(new EmpresaFisicaService().recuperarPorId(manager, idEmpresaFisica));
			
			Usuario usuarioLogado = new UsuarioService().recuperarSessaoUsuarioLogado();
		    
			if(usuarioLogado != null)
				logOperacao.setUsuario(usuarioLogado);

			this.salvar(manager, transaction, logOperacao);
			
			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();
			
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}

	public void gravarOperacaoAlterar(EntityManager manager, EntityTransaction transaction, Object objAlterado) throws Exception
	{
		gravarOperacaoAlterar(manager, transaction, objAlterado, null);
	}
	
	public void gravarOperacaoAlterar(EntityManager manager, EntityTransaction transaction, Object objAlterado, String observacao) throws Exception
	{
		Boolean transacaoIndependente = false;
				
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			
			List<LogOperacao> listaLogOperacao = new ArrayList<LogOperacao>();
			
			Integer idDocumentoOrigem = this.recuperarIdDocumentoOrigem(objAlterado);
			Object objOriginal = this.recuperarObjetoOriginal(manager, objAlterado);
			Integer idEmpresaFisica = null;
			String agrupador = agrupadorFixo == null ? this.gerarCodigoAgrupador(objAlterado.getClass().getSimpleName()) : agrupadorFixo;
			idEmpresaFisica = this.recuperarEmpresaFisicaLog(manager, objOriginal);
			
			if(objOriginal == null)
			{
				LogOperacao logOperacao = new LogOperacao();
				logOperacao.setData(new Date());
				logOperacao.setIdDocumentoOrigem(idDocumentoOrigem.toString());
				logOperacao.setAgrupador(agrupador);
				logOperacao.setNomeClasse(objAlterado.getClass().getSimpleName());
				logOperacao.setTipoLog(TIPOLOG_ALTERACAO);
				
				if(gerarObservacaoMethod != null)
				{
					Object service = recuperarServiceObject(objOriginal);
					logOperacao.setObservacao((String) gerarObservacaoMethod.invoke(service, objOriginal));
					service = null;
				}
				else
					logOperacao.setObservacao(observacao);
				
				Usuario usuarioLogado = new UsuarioService().recuperarSessaoUsuarioLogado();
			    
				if(usuarioLogado != null)
					logOperacao.setUsuario(usuarioLogado);
				
				salvar(manager, transaction, logOperacao, false);
				
				System.err.println("Não foi possível encontrar o objeto original do " + objAlterado.getClass().getSimpleName() + " com ID " + idDocumentoOrigem);
			}
			else
			{
				for(Field field : objOriginal.getClass().getDeclaredFields())
				{
					if(field.getModifiers() == 26)
						continue;
					
					field.setAccessible(true);
					
					Object value1 = field.get(objOriginal);
					Object value2 = field.get(objAlterado);
					
					if(value1 == null && value2 == null)
						continue;
					
					LogOperacao logOperacao = new LogOperacao();
					
					String fieldName = field.getName();
					
					
					if(new IgnUtil().isForeignKeyField(field, objOriginal))
					{
						Field foreignKeyIdField = value1.getClass().getDeclaredField("id");
						foreignKeyIdField.setAccessible(true);
						
						if(value1 != null)
							value1 = foreignKeyIdField.get(value1);
						if(value2 != null)
							value2 = foreignKeyIdField.get(value2);
					}

					if(field.getType() == Date.class)
					{
						if(value1 != null)
							value1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format((Date) value1);
						if(value2 != null)
							value2 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format((Date) value2);
					}
					
					boolean updateFlag = false;
					
					if(value1 == null && value2 != null)
					{
						logOperacao.setValorOriginal(null);
						logOperacao.setValorAlterado(value2.toString());
						
						updateFlag = true;
					}
					else if(value1 != null && value2 == null)
					{
						logOperacao.setValorOriginal(value1.toString());
						logOperacao.setValorAlterado(null);
						
						updateFlag = true;
					}
					else if(!value1.toString().equals(value2.toString()))
					{
						logOperacao.setValorOriginal(value1.toString());
						logOperacao.setValorAlterado(value2.toString());
						
						updateFlag = true;
					}
					
					if(updateFlag)
					{
						logOperacao.setCampo(this.formatarNomeCampo(fieldName));
						logOperacao.setData(new Date());
						logOperacao.setIdDocumentoOrigem(idDocumentoOrigem.toString());
						logOperacao.setAgrupador(agrupador);
						logOperacao.setNomeClasse(objOriginal.getClass().getSimpleName());
						logOperacao.setTipoLog(TIPOLOG_ALTERACAO);
						
						if(gerarObservacaoMethod != null)
						{
							Object service = recuperarServiceObject(objOriginal);
							logOperacao.setObservacao((String) gerarObservacaoMethod.invoke(service, objOriginal));
							service = null;
						}
						else
							logOperacao.setObservacao(observacao);
							
						if(idEmpresaFisica != null)
							logOperacao.setEmpresaFisica(new EmpresaFisicaService().recuperarPorId(manager, idEmpresaFisica));
						
						Usuario usuarioLogado = new UsuarioService().recuperarSessaoUsuarioLogado();
					    
						if(usuarioLogado != null)
							logOperacao.setUsuario(usuarioLogado);
						
						listaLogOperacao.add(logOperacao);
					}
					
					field.setAccessible(false);
				}
				
				for(LogOperacao logOperacao : listaLogOperacao)
				{
					this.salvar(manager, transaction, logOperacao, false);
				}	
			}
			
			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();
			
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	
	public void gravarOperacaoExclusao(EntityManager manager, EntityTransaction transaction, Object objExcluido) throws Exception
	{
		gravarOperacaoExclusao(manager, transaction, objExcluido, null);
	}
	
	public void gravarOperacaoExclusao(EntityManager manager, EntityTransaction transaction, Object objExcluido, String observacao) throws Exception
	{
		Boolean transacaoIndependente = false;
		
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}

			Integer idEmpresaFisica = null;
			String agrupador = agrupadorFixo == null ? this.gerarCodigoAgrupador(objExcluido.getClass().getSimpleName()) : agrupadorFixo;
			idEmpresaFisica = this.recuperarEmpresaFisicaLog(manager, objExcluido);
			
			LogOperacao logOperacao = new LogOperacao();
			
			logOperacao.setData(new Date());
			logOperacao.setAgrupador(agrupador);
			logOperacao.setIdDocumentoOrigem(this.recuperarIdDocumentoOrigem(objExcluido).toString());
			logOperacao.setNomeClasse(objExcluido.getClass().getSimpleName());
			logOperacao.setTipoLog(TIPOLOG_EXCLUSAO);
			
			if(gerarObservacaoMethod != null)
			{
				Object service = recuperarServiceObject(objExcluido);
				logOperacao.setObservacao((String) gerarObservacaoMethod.invoke(service, recuperarObjetoOriginal(manager, objExcluido)));
				service = null;
			}
			else
				logOperacao.setObservacao(observacao);
			
			if(idEmpresaFisica != null)
				logOperacao.setEmpresaFisica(new EmpresaFisicaService().recuperarPorId(manager, idEmpresaFisica));
			
			Usuario usuarioLogado = new UsuarioService().recuperarSessaoUsuarioLogado();
		    
			if(usuarioLogado != null)
				logOperacao.setUsuario(usuarioLogado);

			this.salvar(manager, transaction, logOperacao);
			
			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();
			
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object recuperarObjetoOriginal(EntityManager manager, Object obj) throws Exception
	{
		Object objOriginal = null;
		
		try
		{
			Class classe = Class.forName("br.com.desenv.nepalign.service." + obj.getClass().getSimpleName() + "Service");
			Constructor objectConstructor = null;
			objectConstructor = classe.getConstructor();
			Object object = objectConstructor.newInstance();
		        
			Field id = obj.getClass().getDeclaredField("id");
			id.setAccessible(true);

			objOriginal = (object.getClass().getMethod("recuperarPorId", EntityManager.class, Integer.class).invoke(object, manager, Integer.parseInt(id.get(obj).toString())));   
			id.setAccessible(false);	
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			objOriginal = null;
		}
		
		return objOriginal;
	}
	
	public Integer recuperarIdDocumentoOrigem(Object obj) throws Exception
	{
		Integer idDocumentoOrigem = 0;
		
		try
		{
			Field id = obj.getClass().getDeclaredField("id");
			id.setAccessible(true);
			
		    idDocumentoOrigem = Integer.parseInt(id.get(obj).toString());
		    
		    id.setAccessible(false);
		}
		catch(Exception ex)
		{
			idDocumentoOrigem = 0;
		}
	    
	    return idDocumentoOrigem;
	}

	private Object recuperarServiceObject(Object obj) throws Exception
	{
		return Class.forName("br.com.desenv.nepalign.service." + obj.getClass().getSimpleName() + "Service").getConstructor().newInstance();
	}

	public String gerarCodigoAgrupador(String className) throws Exception
	{
		try
		{
			return className + "-RN-" + new IgnUtil().getRandomNumber(10);		
		}
		catch(Exception ex)
		{
			return className + "-RN-" + new Date().getTime();
		}
	}
	
	public Integer recuperarEmpresaFisicaLog(EntityManager manager, Object obj) throws Exception
	{
		Integer idEmpresaFisica = null;
		
		try
		{
			Field idEmpresaFisicaField = obj.getClass().getDeclaredField("empresaFisica");
			idEmpresaFisicaField.setAccessible(true);
			
			Field foreignKeyIdField = idEmpresaFisicaField.get(obj).getClass().getDeclaredField("id");
			foreignKeyIdField.setAccessible(true);
			
			Object idEmpresaFisicaBuffer = foreignKeyIdField.get(idEmpresaFisicaField.get(obj));
			idEmpresaFisica = Integer.parseInt(idEmpresaFisicaBuffer.toString());	
			
			idEmpresaFisicaField.setAccessible(false);
			foreignKeyIdField.setAccessible(false);
		}
		catch(NullPointerException nullp) // ????
		{
			nullp.printStackTrace();
			idEmpresaFisica = null;
		}
		catch(NoSuchFieldException nsfe)
		{
			idEmpresaFisica = null;
		}
		
		try
		{
			if(idEmpresaFisica == null)
			{
				Usuario usuarioLogado = new UsuarioService().recuperarSessaoUsuarioLogado();
			    
				if(usuarioLogado != null && usuarioLogado.getEmpresaPadrao() != null)
					idEmpresaFisica = new EmpresaFisicaService().recuperarEmpresaFisicaPorEmpresa(manager, usuarioLogado.getEmpresaPadrao()).getId();
			}	
		}
		catch(Exception ex)
		{
			idEmpresaFisica = null;
		}
		
		return idEmpresaFisica;
	}

	public String formatarNomeCampo(String nomeCampo)
	{
		return IgnUtil.firstCharUpperCase(IgnUtil.separeEveryUpperCase(nomeCampo));
	}
}