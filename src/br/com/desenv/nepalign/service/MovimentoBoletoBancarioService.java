package br.com.desenv.nepalign.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.MovimentoBoletoBancario;
import br.com.desenv.nepalign.persistence.MovimentoBoletoBancarioPersistence;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class MovimentoBoletoBancarioService extends GenericServiceIGN<MovimentoBoletoBancario, MovimentoBoletoBancarioPersistence>
{

	public MovimentoBoletoBancarioService() 
	{
		super();
	}
	public List<MovimentoBoletoBancario> listarPorNomerArquivo(String nomeArquivo) throws Exception
	{
		return listarPorNomerArquivo(nomeArquivo, null, null);
	}
	public List<MovimentoBoletoBancario> listarPorNomerArquivo(String nomeArquivo,EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		List<MovimentoBoletoBancario> lista = null;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			MovimentoBoletoBancario criterio = new MovimentoBoletoBancario();
			criterio.setNomeArquivo(nomeArquivo);
			
			 lista = listarPorObjetoFiltro(manager, criterio, null, null, null);
			
			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();

			if(transacaoIndependente)
				transaction.rollback();

			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
		return lista;
	}

}

