package br.com.desenv.nepalign.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.hibernate.ejb.criteria.expression.function.AggregationFunction.MAX;

import net.sf.ehcache.search.aggregator.Max;
import br.com.desenv.nepalign.model.Acao;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.EstoqueProduto;
import br.com.desenv.nepalign.model.Funcionalidade;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.model.SaldoEstoqueProduto;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.ProdutoService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;


public class AjusteSaldoItem {

	/**
	 * @param args
	 */
	public void ajustarSaldoEstoqueProduto (String empresa) throws Exception
	{
		EntityManager manager = null;
		EntityTransaction transaction = null;
		Boolean transacaoIndependente = false;
		   try
		   {
			   if(manager == null || transaction == null)
			   {
				   manager = ConexaoUtil.getEntityManager();
				   transaction = manager.getTransaction();
				   transaction.begin();
				   transacaoIndependente = true;
			   }
			Connection connection = ConexaoUtil.getConexaoPadrao();
			int ultimoCodigoBarras = 0;
			Date dataUltimaEntrada;
			DateFormat formataData = new SimpleDateFormat("dd/MM/yyyy");
			Double venda = null;
			Double custo = null;
			int cont = 0;
			int cont2 = 0;
			//String empGrupo1 = "1,2,3,4,7,9,13";
			//String empGrupo2 = "5,8";
			//String empresas = "";
			Statement stm = null;
			ResultSet rs = null;
				
			int anoAberto = new EmpresaFisicaService().anoAberto();
			int mesAberto = new EmpresaFisicaService().mesAberto();
			
			Boolean possuiDivergenciaVenda = false;
			Boolean possuiDivergenciaCusto = false;

			//List<Produto> listaProduto = new ProdutoService().listarPorObjetoFiltro(new ProdutoService().recuperarPorId(67611));
			
			//List<Produto> listaProduto = new ProdutoService().listarPaginadoSqlLivreGenerico(Integer.MAX_VALUE, 1, "idproduto=66067","idproduto","DESC").getRecordList();
			List<Produto> listaProduto = new ProdutoService().listar("idProduto", "DESC");
			
			File updateSaldoEstoque = new File("C:\\script\\updateSaldoEstoqueProduto_F"+empresa+".txt");
			FileWriter frupdate = new FileWriter(updateSaldoEstoque.getAbsoluteFile());
			BufferedWriter gravadorUpdateSaldoEstoque = new BufferedWriter(frupdate);
			
				File referenciaComMovimentacaoDivergente = new File("C:\\script\\referenciaComMovimentacaoDivergente_F"+empresa+".txt");
				FileWriter fr = new FileWriter(referenciaComMovimentacaoDivergente.getAbsoluteFile());
				BufferedWriter gravadorComMovimentacaoDivergente = new BufferedWriter(fr);
				if (!referenciaComMovimentacaoDivergente.exists()) 
					referenciaComMovimentacaoDivergente.createNewFile(); 
				
				File referenciaSemMovimentacaoDivergente = new File("C:\\script\\referenciaSemMovimentacaoDivergente_F"+empresa+".txt");
				FileWriter fr2 = new FileWriter(referenciaSemMovimentacaoDivergente.getAbsoluteFile());
				BufferedWriter gravadorSemMovimentacaoDivergente = new BufferedWriter(fr2);
				if (!referenciaSemMovimentacaoDivergente.exists()) 
					referenciaSemMovimentacaoDivergente.createNewFile(); 
				
				gravadorComMovimentacaoDivergente.write("******************   Empresa: " + empresa +"   *********************");
				gravadorComMovimentacaoDivergente.newLine();
				gravadorComMovimentacaoDivergente.write("****************** Referencias Com Movimentação de Entrada Com Valores de Venda Divergentes *********************");
				gravadorComMovimentacaoDivergente.newLine();
				
				
				gravadorSemMovimentacaoDivergente.write("****************** Empresa: " + empresa +"   *********************");
				gravadorSemMovimentacaoDivergente.newLine();
				gravadorSemMovimentacaoDivergente.write("****************** Referencias Sem Movimentacao de Entrada Com Valores de Venda Divergentes *********************");
				gravadorSemMovimentacaoDivergente.newLine();
				
					for (Produto produto : listaProduto) 
					{
						EstoqueProduto estoqueProdutoCriterio = new EstoqueProduto();
						estoqueProdutoCriterio.setProduto(produto);
						List<SaldoEstoqueProduto> listaSaldoEstoqueProduto = new ArrayList<>(); 
						
						String ultimaEntradaQuery = "select im.idEstoqueProduto as codigoBarras,mov.dataMovimentacao as dataMovimentacao, im.venda as venda, im.custo as custo from itemmovimentacaoestoque as im ";
						ultimaEntradaQuery += "inner join movimentacaoestoque as mov on mov.idMovimentacaoEstoque = im.idMovimentacaoEstoque ";
						ultimaEntradaQuery += "where im.idProduto = "+produto.getId()+" ";
						ultimaEntradaQuery += "and mov.idEmpresaFisica in("+empresa+") and mov.idTipoMovimentacaoEstoque=1 and mov.dataMovimentacao >= '2014-03-01 00:00:00' ";
						ultimaEntradaQuery += "order by mov.dataMovimentacao desc limit 1";
			
						System.out.println("#Referencia: " + produto.getId());
	 					ResultSet resultUltimaEntrada = connection.createStatement().executeQuery(ultimaEntradaQuery); 
						
						//se teve entrada procura o saldo deste codigo barras
						if(resultUltimaEntrada.next())
						{
							ultimoCodigoBarras = resultUltimaEntrada.getInt("codigoBarras");
							dataUltimaEntrada = resultUltimaEntrada.getDate("dataMovimentacao");
							venda = resultUltimaEntrada.getDouble("venda");
							custo = resultUltimaEntrada.getDouble("custo");
							
							
					 
							String saldoRecuperado = "SELECT venda, custo from saldoestoqueproduto where ";
							saldoRecuperado += " idestoqueproduto in (select idEstoqueProduto from estoqueProduto where idProduto = " + produto.getId().intValue() + ") ";
							saldoRecuperado += " and idempresafisica in ("+empresa+") and ano = "+anoAberto+" and mes="+mesAberto;
							 
							rs = connection.createStatement().executeQuery(saldoRecuperado.toString());
							
							while (rs.next())
							{
								SaldoEstoqueProduto sep = new SaldoEstoqueProduto();
								sep.setCusto(rs.getDouble("custo"));
								sep.setVenda(rs.getDouble("venda"));
								listaSaldoEstoqueProduto.add(sep);
							}			
							
							//stm.close(); 
							rs.close();
							 
							//listaSaldoEstoqueProduto = new SaldoEstoqueProdutoService().buscarSaldoPorListaStringEmpresaReferencia(empresas, produto, connection);
							
							possuiDivergenciaVenda = verificarPrecoVendaDiferenteProduto(listaSaldoEstoqueProduto);
							possuiDivergenciaCusto = verificarPrecoCustoDiferenteProduto(listaSaldoEstoqueProduto);
							
							if(possuiDivergenciaVenda)
							{
								cont++;	
								gravadorComMovimentacaoDivergente.write("# "+cont+" # VENDA # Referência: "+produto.getId()+" - DataUltimaEntrada: "+formataData.format(dataUltimaEntrada));
								gravadorComMovimentacaoDivergente.newLine();
								gravadorComMovimentacaoDivergente.flush();
								
								String bkpSaldo = " SELECT * FROM saldoestoqueproduto where idEstoqueProduto in ";
								bkpSaldo += "(select idEstoqueProduto from estoqueproduto where idproduto in ("+produto.getId()+"))";
								bkpSaldo += " and idempresafisica = "+empresa+" and ano="+anoAberto+" and mes="+mesAberto+" INTO OUTFILE 'C:/script/bkp/bkpRef"+produto.getId()+".txt'";
								
								connection.createStatement().executeQuery(bkpSaldo);
								//rs =  stm.executeQuery(bkpSaldo.toString());
								
								String atualizaSaldoEstoqueQuery = "update saldoestoqueproduto ";
								atualizaSaldoEstoqueQuery += "set venda = "+venda+" ";
								atualizaSaldoEstoqueQuery += "where mes="+mesAberto+" and ano="+anoAberto+" and idempresafisica="+empresa+" and idEstoqueProduto in(select idEstoqueProduto from estoqueproduto where idproduto="+produto.getId()+"); ";
								System.out.println(atualizaSaldoEstoqueQuery);
								
								gravadorUpdateSaldoEstoque.write(atualizaSaldoEstoqueQuery);
								gravadorUpdateSaldoEstoque.newLine();
								gravadorUpdateSaldoEstoque.flush();
							}
							
							if(possuiDivergenciaCusto)
							{

								cont++;	
								gravadorComMovimentacaoDivergente.write("# "+cont+" # CUSTO # Referência: "+produto.getId()+" - DataUltimaEntrada: "+formataData.format(dataUltimaEntrada));
								gravadorComMovimentacaoDivergente.newLine();
								gravadorComMovimentacaoDivergente.flush();
								
								String bkpSaldo = " SELECT * FROM saldoestoqueproduto where idEstoqueProduto in ";
								bkpSaldo += "(select idEstoqueProduto from estoqueproduto where idproduto in ("+produto.getId()+"))";
								bkpSaldo += " and idempresafisica = "+empresa+" and ano="+anoAberto+" and mes="+mesAberto+" INTO OUTFILE 'C:/script/bkp/bkpRef"+cont+"-"+produto.getId()+".txt'";
								
								connection.createStatement().executeQuery(bkpSaldo);
								//rs =  stm.executeQuery(bkpSaldo.toString());
								
								String atualizaSaldoEstoqueQuery = "update saldoestoqueproduto ";
								atualizaSaldoEstoqueQuery += "set custo = "+custo+" ";
								atualizaSaldoEstoqueQuery += ", custoMedio ="+custo+" ";
								atualizaSaldoEstoqueQuery += ", ultimoCusto ="+custo+" ";
								atualizaSaldoEstoqueQuery += "where mes="+mesAberto+" and ano="+anoAberto+" and idempresafisica="+empresa+" and idEstoqueProduto in(select idEstoqueProduto from estoqueproduto where idproduto="+produto.getId()+"); ";
								
								gravadorUpdateSaldoEstoque.write(atualizaSaldoEstoqueQuery);
								gravadorUpdateSaldoEstoque.newLine();
								gravadorUpdateSaldoEstoque.flush();
							}
						}
						else //SEM MOVIMENTAção DE ENTRADA
						{ 
							System.out.println("#Nao foi encontrado movimentação de entrada desta referencia.");
							//gravadorSemMovimentacaoDivergente.write("não foi encontrado Movimentação de Entrada da Referência: "+produto.getId());
							//gravadorSemMovimentacaoDivergente.newLine();
							
							
							String saldoRecuperado = "SELECT venda, custo from saldoestoqueproduto where ";
							saldoRecuperado += " idestoqueproduto in (select idEstoqueProduto from estoqueProduto where idProduto = " + produto.getId().intValue() + ") ";
							saldoRecuperado += " and idempresafisica in ("+empresa+") and ano = "+anoAberto+" and mes="+mesAberto;
							 
							rs = connection.createStatement().executeQuery(saldoRecuperado);
							//rs =  stm.executeQuery(saldoRecuperado.toString());
							
							while (rs.next())
							{
								SaldoEstoqueProduto sep = new SaldoEstoqueProduto();
								sep.setCusto(rs.getDouble("custo"));
								sep.setVenda(rs.getDouble("venda"));
								listaSaldoEstoqueProduto.add(sep);
							}	
							
																	
							possuiDivergenciaVenda = verificarPrecoVendaDiferenteProduto(listaSaldoEstoqueProduto);
							possuiDivergenciaCusto = verificarPrecoCustoDiferenteProduto(listaSaldoEstoqueProduto);
							
							if(possuiDivergenciaVenda)
							{
								cont2++;
								gravadorSemMovimentacaoDivergente.write("# "+cont2+" # VENDA # Referência: "+produto.getId());
								gravadorSemMovimentacaoDivergente.newLine();
								gravadorSemMovimentacaoDivergente.flush();
								
								String bkpSaldo = " SELECT * FROM saldoestoqueproduto where idEstoqueProduto in ";
								bkpSaldo += "(select idEstoqueProduto from estoqueproduto where idproduto in ("+produto.getId()+"))";
								bkpSaldo += " and idempresafisica = "+empresa+" and ano="+anoAberto+" and mes="+mesAberto+" INTO OUTFILE 'C:/script/bkp/bkpRefV"+produto.getId()+".txt'";
								
								connection.createStatement().executeQuery(bkpSaldo.toString());
								
								String ultimoSaldo = " select * from saldoestoqueproduto as s1 ";
								ultimoSaldo += "where s1.idestoqueproduto in(select idestoqueproduto from estoqueproduto where idproduto="+produto.getId()+") and s1.idempresafisica = "+empresa+" and s1.ano="+anoAberto+" and s1.mes="+mesAberto+" " ;
								ultimoSaldo += "and ultimoEvento = (SELECT MAX(ultimoEvento)  FROM saldoestoqueproduto as s2 where s2.idestoqueproduto in(select idestoqueproduto from estoqueproduto where idproduto="+produto.getId()+") and s2.idempresafisica = "+empresa+" and s2.ano="+anoAberto+" and s2.mes="+mesAberto+") ";
								ultimoSaldo += "order by dataultimaalteracao desc limit 1";
								
								rs = connection.createStatement().executeQuery(ultimoSaldo.toString());
								
								Double ultimaVenda = 0.0;
								
								if(rs.next())
									ultimaVenda = rs.getDouble("venda");
								
								if(ultimaVenda.doubleValue() != 0.0 && ultimaVenda != null)
								{
									String atualizaSaldoEstoqueQuery = "update saldoestoqueproduto ";
									atualizaSaldoEstoqueQuery += "set venda = "+ultimaVenda+" ";
									atualizaSaldoEstoqueQuery += "where mes="+mesAberto+" and ano="+anoAberto+" and idempresafisica="+empresa+" and idEstoqueProduto in(select idEstoqueProduto from estoqueproduto where idproduto="+produto.getId()+"); ";
									System.out.println(atualizaSaldoEstoqueQuery);
									
									gravadorUpdateSaldoEstoque.write(atualizaSaldoEstoqueQuery);
									gravadorUpdateSaldoEstoque.newLine();
									gravadorUpdateSaldoEstoque.flush();
								}
							}
							
							if(possuiDivergenciaCusto)
							{
								cont2++;
								gravadorSemMovimentacaoDivergente.write("# "+cont2+" # CUSTO # Referência: "+produto.getId());
								gravadorSemMovimentacaoDivergente.newLine();
								gravadorSemMovimentacaoDivergente.flush();
								
								String bkpSaldo = " SELECT * FROM saldoestoqueproduto where idEstoqueProduto in ";
								bkpSaldo += "(select idEstoqueProduto from estoqueproduto where idproduto in ("+produto.getId()+"))";
								bkpSaldo += " and idempresafisica = "+empresa+" and ano="+anoAberto+" and mes="+mesAberto+" INTO OUTFILE 'C:/script/bkp/bkpRefC"+produto.getId()+".txt'";
								
								rs = connection.createStatement().executeQuery(bkpSaldo.toString());
								
								String ultimoSaldo = " select * from saldoestoqueproduto as s1 ";
								ultimoSaldo += "where s1.idestoqueproduto in(select idestoqueproduto from estoqueproduto where idproduto="+produto.getId()+") and s1.idempresafisica = "+empresa+" and s1.ano="+anoAberto+" and s1.mes="+mesAberto+" " ;
								ultimoSaldo += "and ultimoEvento = (SELECT MAX(ultimoEvento)  FROM saldoestoqueproduto as s2 where s2.idestoqueproduto in(select idestoqueproduto from estoqueproduto where idproduto="+produto.getId()+") and s2.idempresafisica = "+empresa+" and s2.ano="+anoAberto+" and s2.mes="+mesAberto+") ";
								ultimoSaldo += "order by dataultimaalteracao desc limit 1";
								
								rs = connection.createStatement().executeQuery(ultimoSaldo.toString());
								
								Double ultimoCusto = 0.0;
								if(rs.next())
									ultimoCusto = rs.getDouble("custo");
								
								if(ultimoCusto.doubleValue() != 0.0 && ultimoCusto != null)
								{
									String atualizaSaldoEstoqueQuery = "update saldoestoqueproduto ";
									atualizaSaldoEstoqueQuery += "set custo = "+ultimoCusto+" ";
									atualizaSaldoEstoqueQuery += ", custoMedio ="+ultimoCusto+" ";
									atualizaSaldoEstoqueQuery += ", ultimoCusto ="+ultimoCusto+" ";
									atualizaSaldoEstoqueQuery += "where mes="+mesAberto+" and ano="+anoAberto+" and idempresafisica="+empresa+" and idEstoqueProduto in(select idEstoqueProduto from estoqueproduto where idproduto="+produto.getId()+"); ";
									
									gravadorUpdateSaldoEstoque.write(atualizaSaldoEstoqueQuery);
									gravadorUpdateSaldoEstoque.newLine();
									gravadorUpdateSaldoEstoque.flush();
						
								}
							}
						} 
						resultUltimaEntrada.close(); 
						
					} //Fim For Produto	

					
				gravadorComMovimentacaoDivergente.close();
				gravadorSemMovimentacaoDivergente.close();
				gravadorUpdateSaldoEstoque.close();
				cont = 0;
				cont2 = 0;
				rs.close();
				
			 if(transacaoIndependente)
				   transaction.commit();
		   }
		   catch(Exception ex)
		   {
			   if(transacaoIndependente)
				   transaction.rollback();
			   
			   
			   ex.printStackTrace();
			   throw ex;
		   }
		   finally
		   {
			   if(transacaoIndependente)
				   manager.close();
			   
			   
			   
			   System.out.println("FINALIZADO");
		   }
		
	}
	
	public Boolean verificarPrecoVendaDiferenteProduto(List<SaldoEstoqueProduto> listaSaldoEstoqueProduto) throws Exception
	{
		Boolean possuiDivergencia = false;
		List<Double> listaPrecoVenda = new ArrayList<>();
		
		for (SaldoEstoqueProduto saldoRecuperado : listaSaldoEstoqueProduto) {
			if(!listaPrecoVenda.contains(saldoRecuperado.getVenda()))
					listaPrecoVenda.add(saldoRecuperado.getVenda());
		}
		if(listaPrecoVenda.size() > 1)
			possuiDivergencia = true;
		
		return possuiDivergencia;
	}
	
	public Boolean verificarPrecoCustoDiferenteProduto(List<SaldoEstoqueProduto> listaSaldoEstoqueProduto) throws Exception
	{
		Boolean possuiDivergencia = false;
		List<Double> listaPrecoCusto = new ArrayList<>();
		
		for (SaldoEstoqueProduto saldoRecuperado : listaSaldoEstoqueProduto) {
			if(!listaPrecoCusto.contains(saldoRecuperado.getCusto()))
				listaPrecoCusto.add(saldoRecuperado.getCusto());
		}
		if(listaPrecoCusto.size() > 1)
			possuiDivergencia = true;
		
		return possuiDivergencia;
	}
	
	public void recuperarSaldoReferencia() throws Exception
	{
		try
		{
			ResultSet rs;
			Connection connection = ConexaoUtil.getConexaoPadrao();
			
			//EmpresaFisica empresa2 = new EmpresaFisica();
			List<EmpresaFisica> listaEmpresa = new EmpresaFisicaService().listarPaginadoSqlLivreGenerico(Integer.MAX_VALUE, 1, "idEmpresaFisica <= 13 and idEmpresaFisica not in (10,11,12) ", "idEmpresaFisica", "ASC").getRecordList();
			//List<EmpresaFisica> listaEmpresa = new EmpresaFisicaService().listarPorObjetoFiltro(empresa2 = new EmpresaFisicaService().recuperarPorId(8));
			
			
			for (EmpresaFisica empresa : listaEmpresa) 
			{
				List<Produto> listaProduto = buscarReferenciasArquivo(empresa.getId());
				
				File updateSaldo = new File("C:\\script\\referencias\\updateSaldo_F"+empresa.getId()+".txt");
				FileWriter file = new FileWriter(updateSaldo.getAbsoluteFile());
				BufferedWriter gravador = new BufferedWriter(file);
					
				for (Produto produto : listaProduto) 
				{
					String saldo = " select * from saldoestoqueproduto as s1 ";
					saldo += "where s1.idestoqueproduto in(select idestoqueproduto from estoqueproduto where idproduto="+produto.getId()+") and s1.idempresafisica = "+empresa.getId()+" and s1.ano="+2015+" and s1.mes="+4+" " ;
					saldo += "and venda is not null and custo is not null ";
					saldo += "order by dataultimaalteracao desc limit 1";
					
					rs = connection.createStatement().executeQuery(saldo.toString());
					
					Double custo = 0.0;
					Double venda = 0.0;
					if(rs.next())
					{
						custo = rs.getDouble("custo");
						venda = rs.getDouble("venda");
					}
					
					if(custo != 0.0 && venda != 0.0)
					{
						String atualizaSaldoEstoqueQuery = "update saldoestoqueproduto ";
						atualizaSaldoEstoqueQuery += "set custo = "+custo+" ";
						atualizaSaldoEstoqueQuery += ", custoMedio ="+custo+" ";
						atualizaSaldoEstoqueQuery += ", ultimoCusto ="+custo+" ";
						atualizaSaldoEstoqueQuery += ", venda ="+venda+" ";
						atualizaSaldoEstoqueQuery += "where mes="+4+" and ano="+2015+" and idempresafisica="+empresa.getId()+" and idEstoqueProduto in(select idEstoqueProduto from estoqueproduto where idproduto="+produto.getId()+"); ";
						
						
						gravador.write(atualizaSaldoEstoqueQuery);
						gravador.newLine();
						gravador.flush();
					}
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	
	public List<Produto> buscarReferenciasArquivo(Integer idEmpresa) throws Exception
	{
		String[] infoReferencia = null;
		String caminhoTrace = "C:/script/referencias/referenciaSemMovimentacaoDivergente_F"+idEmpresa+".txt";

		List<Produto> listaProduto = new ArrayList<>();
		List<Produto> listaProdutoFinal = new ArrayList<>();
		try
		{

			ArrayList<Produto> data = new ArrayList<Produto>();

			System.out.println("Começando escaneamento do arquivo...");

			if(!new File(caminhoTrace).exists())
			{
				System.out.println("Arquivo ["+ caminhoTrace +"] não encontrado.");
			}

			BufferedReader bufferedReader = new BufferedReader(new FileReader(caminhoTrace));

			System.out.println("Arquivo encontrado...");

			String produto = "";

			System.out.println("Lendo arquivo....");



			while((produto = bufferedReader.readLine()) != null)
			{
				if(!produto.equals("FIM"))
				{
					infoReferencia = produto.split(":");

					Produto produtoCriterio = new Produto();
					produtoCriterio.setCodigoProduto(infoReferencia[1]);

					Produto produtoRecuperado = new Produto();

					listaProduto = new ProdutoService().listarPorObjetoFiltro(produtoCriterio);

					if (listaProduto.size() > 0)
					{
						produtoRecuperado = new ProdutoService().listarPorObjetoFiltro(produtoCriterio).get(0);
						if(!listaProdutoFinal.contains(produtoRecuperado))
							listaProdutoFinal.add(produtoRecuperado);
					}
				}
				else
					break; 
			}

			System.out.println(listaProdutoFinal.size() + " Referencias...");

			return listaProdutoFinal;

		}
		catch(Exception ex)
		{
			System.out.println(infoReferencia);
			throw new Exception(ex);
		}
	}
	
	
	
	public static void main(String[] args) throws Exception
	{
		/*
		String var = "";
		if (args.length==0)
		{
			var = "8";
		}
		new AjusteSaldoItem().init(args[0]); 
		*/
		
		new AjusteSaldoItem().recuperarSaldoReferencia();
	}
	
	/*
	public void init(String args) throws Exception
	{
		String valor = args;
		new AjusteSaldoItem().ajustarSaldoEstoqueProduto(valor); 
	}
	
	*/

}
