package br.com.desenv.nepalign.service;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.FormaPagamento;
import br.com.desenv.nepalign.model.FormaPagamentoCartao;
import br.com.desenv.nepalign.persistence.FormaPagamentoCartaoPersistence;

public class FormaPagamentoCartaoService extends GenericServiceIGN<FormaPagamentoCartao, FormaPagamentoCartaoPersistence> 
{
	public FormaPagamentoCartaoService()
	{
		super();
	}
	
	public final FormaPagamentoCartao recuperarFormaPagamentoCartao(EntityManager manager, final FormaPagamento formaPagamento, final Integer quantidadeParcelas) throws Exception
	{ 
		if(formaPagamento.getCartaoCredito().intValue() == 0x01)
		{
			if(quantidadeParcelas > 0x01)
				return recuperarPorId(manager, FormaPagamentoCartao.PARCELADO);
			else
				return recuperarPorId(manager, FormaPagamentoCartao.CREDITO);
		}
		else
			return recuperarPorId(manager, FormaPagamentoCartao.DEBITO);
	}
}