package br.com.desenv.nepalign.service;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.LancamentoContaPagar;
import br.com.desenv.nepalign.model.LancamentoContaPagarRateio;
import br.com.desenv.nepalign.persistence.LancamentoContaPagarRateioPersistence;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class LancamentoContaPagarRateioService extends GenericServiceIGN<LancamentoContaPagarRateio, LancamentoContaPagarRateioPersistence>
{
	public LancamentoContaPagarRateioService() 
	{
		super();
	}
	
	public void excluir(EntityManager manager, LancamentoContaPagar lancamentoContaPagar) throws Exception
	{
		boolean transacaoIndependente = false;
		
		try
		{
			if((transacaoIndependente = (manager == null)))
			{
				manager = ConexaoUtil.getEntityManager();
				manager.getTransaction().begin();
			}
			
			final LancamentoContaPagarRateio rateioFiltro = new LancamentoContaPagarRateio();
			rateioFiltro.setLancamentoContaPagar(lancamentoContaPagar);
			
			for (final LancamentoContaPagarRateio rateio : listarPorObjetoFiltro(manager, rateioFiltro, null, null, null))
				super.excluir(manager, manager.getTransaction(), rateio, false);
			
			if(transacaoIndependente)
				manager.getTransaction().commit();
		}
		catch(Exception ex)
		{
			if(transacaoIndependente && manager.isOpen() && manager.getTransaction().isActive())
				manager.getTransaction().rollback();
			
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
}