package br.com.desenv.nepalign.service;

import br.com.desenv.frameworkignorante.GenericServiceIGN;

import br.com.desenv.nepalign.model.BandeiraCartao;
import br.com.desenv.nepalign.persistence.BandeiraCartaoPersistence;

public class BandeiraCartaoService extends GenericServiceIGN<BandeiraCartao, BandeiraCartaoPersistence>
{
	public BandeiraCartaoService() 
	{
		super();
	}
}