package br.com.desenv.nepalign.service;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.Funcionalidade;
import br.com.desenv.nepalign.model.Modulo;
import br.com.desenv.nepalign.persistence.FuncionalidadePersistence;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class FuncionalidadeService extends GenericServiceIGN<Funcionalidade, FuncionalidadePersistence>
{
	public FuncionalidadeService() 
	{
		super();
	}
	
	@SuppressWarnings("rawtypes")
	public final Funcionalidade getFuncionalidade(EntityManager manager, Class<? extends GenericServiceIGN> service) throws Exception
	{
		boolean managerIndependente = false;
		try
		{
			if((managerIndependente = (manager == null)))
				manager = ConexaoUtil.getEntityManager();
			
			Funcionalidade funcionalidade = new Funcionalidade();
			funcionalidade.setNomeServico(service.getSimpleName());
			
			try
			{
				return listarPorObjetoFiltro(manager, funcionalidade, null, null, null).get(0x00);	
			}
			catch(IndexOutOfBoundsException outOfBounds)
			{
				funcionalidade = new Funcionalidade();
				funcionalidade.setDescricao(service.getSimpleName());
				funcionalidade.setNomeServico(service.getSimpleName());
				funcionalidade.setSituacao("A");
				funcionalidade.setModulo(new ModuloService().recuperarPorId(manager, 0x01));
				
				manager.persist(funcionalidade);
				return funcionalidade;
			}
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
}