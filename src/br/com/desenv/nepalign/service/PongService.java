package br.com.desenv.nepalign.service;

public class PongService 
{
	/**
	 * Representa um método ping-pong para manter a conexão ativa entre o cliente e o servidor 
	 */
	public void pong() { }
}