package br.com.desenv.nepalign.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.OcorrenciaBoletoBancario;
import br.com.desenv.nepalign.persistence.OcorrenciaBoletoBancarioPersistence;

public class OcorrenciaBoletoBancarioService extends GenericServiceIGN<OcorrenciaBoletoBancario, OcorrenciaBoletoBancarioPersistence>
{

	public OcorrenciaBoletoBancarioService() 
	{
		super();
	}
	public OcorrenciaBoletoBancario recuperarPorCodigo(String codigoOcorrencia) throws Exception
	{
		return this.recuperarPorCodigo(codigoOcorrencia, null, null);
	}
	public OcorrenciaBoletoBancario recuperarPorCodigo(String codigoOcorrencia,EntityManager manager, EntityTransaction transaction) throws Exception
	{
		OcorrenciaBoletoBancario ocorrenciaBoletoBancario = new OcorrenciaBoletoBancario();
		ocorrenciaBoletoBancario.setCodigoOcorrencia(codigoOcorrencia);
		
		List<OcorrenciaBoletoBancario> lista = listarPorObjetoFiltro(manager, ocorrenciaBoletoBancario, null, null, null);
		if(lista.size() > 0)
			return lista.get(0);
		else
			return null;
	}
	

}

