package br.com.desenv.nepalign.service;

import java.util.List;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.ModuloSenhaGerentes;
import br.com.desenv.nepalign.persistence.ModuloSenhaGerentesPersistence;

public class ModuloSenhaGerentesService extends GenericServiceIGN<ModuloSenhaGerentes, ModuloSenhaGerentesPersistence>
{

	public ModuloSenhaGerentesService() 
	{
		super();
	}

  public ModuloSenhaGerentes recuperarPorDescricao(String modulo) throws Exception
  {
	  	 String whereModulo = " descricao = '"+modulo+"'";
		 List<ModuloSenhaGerentes> modulos = new ModuloSenhaGerentesService().listarPaginadoSqlLivreGenerico(Integer.MAX_VALUE, 1, whereModulo ).getRecordList();
		 
		 if(modulos.size()>0)
			 return modulos.get(0);
		 else
			 return null;
  }
  public List<ModuloSenhaGerentes> recuperarModulosPorDescricao(String modulos) throws Exception
  {
	  	 String whereModulo = " descricao in("+modulos+")";
	    return  new ModuloSenhaGerentesService().listarPaginadoSqlLivreGenerico(Integer.MAX_VALUE, 1, whereModulo ).getRecordList();
		
  }	

}

