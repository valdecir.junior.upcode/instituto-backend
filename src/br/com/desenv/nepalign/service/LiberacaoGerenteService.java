package br.com.desenv.nepalign.service;

import java.util.List;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.DuplicataParcela;
import br.com.desenv.nepalign.model.LiberacaoGerente;
import br.com.desenv.nepalign.persistence.LiberacaoGerentePersistence;

public class LiberacaoGerenteService extends GenericServiceIGN<LiberacaoGerente, LiberacaoGerentePersistence>
{

	public LiberacaoGerenteService() 
	{
		super();
	}
	@Override
	public LiberacaoGerente salvar(LiberacaoGerente liberacaoGerente) throws Exception{

		if(liberacaoGerente.getParcelasLiberadas().size()<1){
			List<DuplicataParcela> parcelas = new DuplicataParcelaService().listarPaginadoSqlLivreGenerico(Integer.MAX_VALUE, 1, " idDuplicata = "+liberacaoGerente.getDuplicata().getId()).getRecordList();
			liberacaoGerente.adicionarParcelasLiberadas((DuplicataParcela[]) parcelas.toArray(new DuplicataParcela[0]));
		}
		return super.salvar(liberacaoGerente);
	}


}

