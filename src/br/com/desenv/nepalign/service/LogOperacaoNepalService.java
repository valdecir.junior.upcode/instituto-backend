package br.com.desenv.nepalign.service;

import java.util.Date;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.LogOperacaoNepal;
import br.com.desenv.nepalign.model.TipoOperacaoLV;
import br.com.desenv.nepalign.persistence.LogOperacaoNepalPersistence;

public class LogOperacaoNepalService extends GenericServiceIGN<LogOperacaoNepal, LogOperacaoNepalPersistence>
{

	public LogOperacaoNepalService() 
	{
		super();
	}
	public void logaAe(int tipoLog, int idTipoOperacaoLV, String descricaoLog) throws Exception
	{
		try
		{
			LogOperacaoNepal log = new LogOperacaoNepal();
			log.setDataHora(new Date());
			log.setDesricao(descricaoLog);
			log.setTipoLog(tipoLog+"");
			if (idTipoOperacaoLV>0)
			{
				TipoOperacaoLV tp = new TipoOperacaoLV();
				tp.setId(tipoLog);
				log.setTipoOperacaoLV(idTipoOperacaoLV);
			}
			else
			{
				log.setTipoOperacaoLV(null);
			}
		
			salvar(log);
		}
		catch(Exception e)
		{
			throw e;
		}
	}	

}

