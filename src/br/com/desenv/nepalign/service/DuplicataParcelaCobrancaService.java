package br.com.desenv.nepalign.service;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao;
import br.com.desenv.nepalign.model.Cliente;
import br.com.desenv.nepalign.model.DuplicataParcela;
import br.com.desenv.nepalign.model.DuplicataParcelaCobranca;
import br.com.desenv.nepalign.persistence.DuplicataParcelaCobrancaPersistence;
import br.com.desenv.nepalign.persistence.DuplicataParcelaPersistence;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class DuplicataParcelaCobrancaService extends GenericServiceIGN<DuplicataParcelaCobranca, DuplicataParcelaCobrancaPersistence>
{

	public DuplicataParcelaCobrancaService() 
	{
		super();
	}

	@IgnSegurancaAcao
	public  DuplicataParcelaCobranca persitirPagamento (DuplicataParcela parcela) throws Exception
	{
		return persitirPagamento(parcela, null, null);

	}

	@IgnSegurancaAcao
	public  DuplicataParcelaCobranca persitirPagamento (DuplicataParcela parcela,EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		DuplicataParcelaCobranca dpc = new DuplicataParcelaCobranca();
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			dpc.setData(new Date());
			dpc.setOcorrencia("P");
			dpc.setDuplicataParcela(parcela);
			dpc = new DuplicataParcelaCobrancaService().salvar(manager, transaction, dpc, false);


			if(transacaoIndependente)
			{
				transaction.commit();
			}
			return dpc;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{

			if(transacaoIndependente)
				manager.close();
		}
	}
	@IgnSegurancaAcao
	public  DuplicataParcelaCobranca persitirCobranca (DuplicataParcela parcela) throws Exception
	{
		return persitirCobranca(parcela, null, null);

	}
	@IgnSegurancaAcao
	public  DuplicataParcelaCobranca persitirCobranca(DuplicataParcela parcela,EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		DuplicataParcelaCobranca dpc = new DuplicataParcelaCobranca();
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			dpc.setData(new Date());
			dpc.setOcorrencia("C");
			dpc.setDuplicataParcela(parcela);
			dpc = new DuplicataParcelaCobrancaService().salvar(manager, transaction, dpc, false);


			if(transacaoIndependente)
			{
				transaction.commit();
			}
			return dpc;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{

			if(transacaoIndependente)
				manager.close();
		}
	}
	@IgnSegurancaAcao
	public Date retornaDataUltimaImpressao() throws Exception
	{
		return new DuplicataParcelaCobrancaPersistence().retornaDataUltimaImpressao();
	}
	@IgnSegurancaAcao
	public byte[] gerarArquivoRecebimentoNovo() throws Exception
	{
		return this.gerarArquivoRecebimentoNovo(null,null);
	}
	public byte[] gerarArquivoRecebimentoNovo(EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			List<Long> listaContratos = new ArrayList<>();
			List<Cliente> listaClientes = new ArrayList<>();
			Date ultimaDataImpresao = this.retornaDataUltimaImpressao();
			Date hj = new Date();
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			if(ultimaDataImpresao!=null && df.parse(df.format(hj)).getTime() == ultimaDataImpresao.getTime())
			{
				throw new Exception("Arquivo já gerado hoje!. Se forma para reimpressão entrar na tela de Reimpressão!");
			}
			String where = " ocorrencia = 'P' and dataimpressao is null";

			List<DuplicataParcelaCobranca> lista = new DuplicataParcelaCobrancaService().listarPaginadoSqlLivreGenerico(manager, Integer.MAX_VALUE, 1, where, null, null).getRecordList();
			File arquivo = new File("arquivo.txt");
			if(arquivo.exists())
			{
				arquivo.delete();
			}
			FileWriter manipuladorArquivo = new FileWriter(arquivo);
			PrintWriter manipuladorTexto = new PrintWriter(manipuladorArquivo);
			manipuladorTexto.println(criarCabecalhoArquivoRecebimento(hj));
			Double valorParcelas = 0.0;
			for (int i = 0; i < lista.size(); i++) 
			{
				DuplicataParcelaCobranca duplicataParcelaCobranca = lista.get(i);
				duplicataParcelaCobranca.setDataImpressao(hj);
				DuplicataParcela par = duplicataParcelaCobranca.getDuplicataParcela();
				String linha = parcelaParaTextoArquivoRecebimento(par);
				manipuladorTexto.println(linha);
				this.atualizar(manager, transaction, duplicataParcelaCobranca);
				valorParcelas += par.getValorPago();
				if(listaContratos.size()==0)
					listaContratos.add(par.getNumeroDuplicata());
				else
				{
					boolean pode = true;
					for (int j = 0; j < listaContratos.size(); j++) 
					{
						Long l = listaContratos.get(j);
						if(l == par.getNumeroDuplicata())
						{
							pode = false;
						}
					}
					if(pode)
						listaContratos.add(par.getNumeroDuplicata());
				}

				if(listaClientes.size()==0)
					listaClientes.add(par.getCliente());
				else
				{
					boolean pode = true;
					for (int j = 0; j < listaClientes.size(); j++) 
					{
						Cliente c = listaClientes.get(j);
						if(c.getId().intValue() == par.getCliente().getId().intValue())
						{
							pode = false;
						}
					}
					if(pode)
						listaClientes.add(par.getCliente());
				}
			}
			manipuladorTexto.println(criarRodapeArquivoRecebimento(listaClientes.size(),listaContratos.size(),valorParcelas));
			manipuladorArquivo.close();

			if(transacaoIndependente)
			{
				transaction.commit();
			}
			byte[] array = Files.readAllBytes(arquivo.toPath());
			return array;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{

			if(transacaoIndependente)
				manager.close();
		}
	}
	@IgnSegurancaAcao
	public byte[] gerarArquivoCobrancaTerceiro(String dataEmissaoInicial, String dataEmissaoFinal) throws Exception
	{
		return this.gerarArquivoCobrancaTerceiro(null, null, dataEmissaoInicial, dataEmissaoFinal);
	}
	@IgnSegurancaAcao
	public byte[] gerarArquivoCobrancaTerceiro(EntityManager manager, EntityTransaction transaction, String dataEmissaoInicial, String dataEmissaoFinal) throws Exception
	{
		Boolean transacaoIndependente = false;

		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			File arquivo = new File("arquivo.csv");
			if(arquivo.exists())
			{
				arquivo.delete();
			}

			FileWriter manipuladorArquivo = new FileWriter(arquivo);
			PrintWriter manipuladorTexto = new PrintWriter(manipuladorArquivo);
			manipuladorTexto.print(new DuplicataParcelaService().criarCabecarioArquivoTeceiro());

			String dataSqlEmissaoInicial = null;
			String dataSqlEmissaoFinal = null;

			if(!dataEmissaoInicial.equals(""))
				dataSqlEmissaoInicial = IgnUtil.dateFormatSql.format(IgnUtil.dateFormat.parse(dataEmissaoInicial));
			if(!dataEmissaoFinal.equals(""))
				dataSqlEmissaoFinal = IgnUtil.dateFormatSql.format(IgnUtil.dateFormat.parse(dataEmissaoFinal));

			Integer dias  = DsvConstante.getParametrosSistema().get("diasGerarArquivoCobranca")==null?35:DsvConstante.getParametrosSistema().getInt("diasGerarArquivoCobranca");
			String sqlCliente = "";
			sqlCliente += "  SELECT dup.idcliente   "; 
			sqlCliente += "  FROM duplicataparcela p "; 
			sqlCliente += "  INNER JOIN duplicata dup on dup.idDuplicata = p.idDuplicata ";
			sqlCliente += "  WHERE p.situacao in(0,2)  "; 
			sqlCliente += "  AND if(p.situacao = 2,if(p.databaixa < p.datavencimento,datediff(current_date(),p.datavencimento),datediff(current_date(),p.databaixa)), datediff(current_date(),p.datavencimento))>=  "+dias; 
			sqlCliente += "  AND (cobranca is null or cobranca <>'C')";

			sqlCliente += "group by dup.idcliente "; 

			Connection conn = ConexaoUtil.getConexaoPadrao(); 
			ResultSet rsCliente = conn.createStatement().executeQuery(sqlCliente);
			String idsClientes = "";

			while(rsCliente.next())
			{
				if(idsClientes.equals(""))
					idsClientes += ""+rsCliente.getInt("idcliente");
				else
					idsClientes += ","+rsCliente.getInt("idcliente");
			}
			conn.close();
			rsCliente.close();

			if(!idsClientes.equals(""))
			{
				String where = "";
				where += " SELECT p.idduplicataparcela ,p.acordo,p.numeroDuplicata "; 
				where += " FROM duplicataparcela p "; 
				where += " INNER JOIN duplicata dup on dup.idDuplicata = p.idDuplicata ";
				where += " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
				where += " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
				where += " group by ac.idDuplicataNova) "; 
				where += " as acordo on acordo.idDuplicataNova=dup.idduplicata ";
				where += " WHERE p.situacao in(0,2)  "; 
				where += " AND (p.cobranca is null or p.cobranca <>'C') ";
				where += " AND p.idCliente in("+idsClientes+")";
				if(dataSqlEmissaoInicial != null)
					where += " and if(p.acordo<>'S',dup.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00',acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
				if(dataSqlEmissaoFinal != null)
					where += " and if(p.acordo<>'S',dup.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59')";

				where = where + " order by p.idcliente,p.numeroDuplicata,p.numeroParcela";

				System.out.println(where);
				conn = ConexaoUtil.getConexaoPadrao(); 
				ResultSet rs = conn.createStatement().executeQuery(where); 

				List<DuplicataParcela> listaParcelas = new ArrayList<DuplicataParcela>();
				int x = 1;
				rs.last();
				int tam = rs.getRow();

				rs.beforeFirst();


				while(rs.next())
				{
					listaParcelas.add(new DuplicataParcelaService().recuperarPorId(manager, rs.getInt("idduplicataparcela")));
					x++;
				}

				rs.close();
				conn.close();

				for (int i = 0; i < listaParcelas.size(); i++) 
				{
					DuplicataParcela par = listaParcelas.get(i);
					new DuplicataParcelaCobrancaService().persitirCobranca(par, manager, transaction);
					par.setCobranca("C");
					new DuplicataParcelaService().atualizar(manager, transaction, par, false);
				}
			}
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String dt = sdf.format(new Date());
			String sql = " data >= '"+dt+" 00:00:00' and data <= '"+dt+" 23:59:59' and ocorrencia = 'C'";
			List<DuplicataParcelaCobranca> lista = new DuplicataParcelaCobrancaService().listarPaginadoSqlLivreGenerico(manager, Integer.MAX_VALUE, 1, sql, null, null).getRecordList();
			for (int i = 0; i < lista.size(); i++) 
			{
				String linha = new DuplicataParcelaService().duplicataParcelaParaString(lista.get(i).getDuplicataParcela());
				manipuladorTexto.print(linha);
			}
			manipuladorArquivo.close();

			if(transacaoIndependente)
			{
				transaction.commit();
			}
			byte[] array = Files.readAllBytes(arquivo.toPath());
			return array;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{

			if(transacaoIndependente)
				manager.close();
		}
	}
	public byte[] gerarArquivoRecebimentoTerceiro(Date dataEmissaoInicial,Date dataEmissaoFinal) throws Exception
	{
		return this.gerarArquivoRecebimentoTerceiro(dataEmissaoInicial, dataEmissaoFinal,null,null);
	}
	public byte[] gerarArquivoRecebimentoTerceiro(Date dataEmissaoInicial,Date dataEmissaoFinal,EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");

			String dataSqlEmissaoInicial = "";
			String dataSqlEmissaoFinal = "";
			if(dataEmissaoInicial!=null ){
				dataSqlEmissaoInicial = df.format(dataEmissaoInicial);
			}
			if(dataEmissaoFinal!=null ){
				dataSqlEmissaoFinal = df.format(dataEmissaoFinal);
			}
			String where = "";
			String ids = "";
			String sql = "";
			List<DuplicataParcelaCobranca> lista = new ArrayList<>();
			
			
			sql = sql + " select cob.idDuplicataParcelaCobranca as id ";
			sql = sql + " from duplicataparcelacobranca cob ";
			sql = sql + " inner join duplicataparcela par on cob.idduplicataparcela = par.idduplicataparcela ";
			sql = sql + " inner join duplicata dupli on dupli.idduplicata = par.idduplicata ";
			sql = sql + " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
			sql = sql + " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
			sql = sql + " group by ac.idDuplicataNova) "; 
			sql = sql + " as acordo on acordo.idDuplicataNova=PAR.idduplicata ";
			sql = sql + " where dataimpressao is null and ocorrencia = 'P'  ";
			if(!dataSqlEmissaoInicial.equals(""))
				sql = sql + " and if((par.acordo<>'S' or par.acordo is null),dupli.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00',acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00' ) ";
			if(!dataSqlEmissaoFinal.equals(""))
				sql = sql +"  and if((par.acordo<>'S' or par.acordo is null),dupli.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59')";
			Connection con = ConexaoUtil.getConexaoPadrao();
			ResultSet rs = con.createStatement().executeQuery(sql);
			System.out.println(sql);
			while(rs.next()){
				if(ids.equals(""))
					ids += rs.getString("id");
				else
					ids += ","+rs.getString("id");
			}
			con.close();
			if(!ids.equals("")){
				where = " idDuplicataParcelaCobranca in("+ids+")";
				System.out.println(ids);
			 lista = new DuplicataParcelaCobrancaService().listarPaginadoSqlLivreGenerico(manager, Integer.MAX_VALUE, 1, where, null, null).getRecordList();
			}
			else
			{
				throw new Exception("Não há nenhum recebimento de cobrança!");
			}
			File arquivo = new File("arquivo.txt");
			if(arquivo.exists())
			{
				arquivo.delete();
			}
			FileWriter manipuladorArquivo = new FileWriter(arquivo);
			PrintWriter manipuladorTexto = new PrintWriter(manipuladorArquivo);
			manipuladorTexto.print(new DuplicataParcelaService().criarCabecarioArquivoTeceiro());
			for (int i = 0; i < lista.size(); i++) 
			{
				DuplicataParcelaCobranca duplicataParcelaCobranca = lista.get(i);
				duplicataParcelaCobranca.setDataImpressao(new Date());
				new DuplicataParcelaCobrancaService().atualizar(manager, transaction, duplicataParcelaCobranca, false);
				DuplicataParcela par = lista.get(i).getDuplicataParcela();
				if(!par.getSituacao().equals("3") && !par.getTipoPagamento().equals("5"))
				{
					String linha = new DuplicataParcelaService().duplicataParcelaParaString(par);
					manipuladorTexto.print(linha);
				}

			}
			manipuladorArquivo.close();

			if(transacaoIndependente)
			{
				transaction.commit();
			}
			byte[] array = Files.readAllBytes(arquivo.toPath());
			return array;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{

			if(transacaoIndependente)
				manager.close();
		}
	}
	public byte[] gerarArquivoRecebimentoTerceiroReimpressao(Date inicio,Date fim,Date dataEmissaoInicial,Date dataEmissaoFinal) throws Exception
	{
		return this.gerarArquivoRecebimentoTerceiroReimpressao(inicio, fim, dataEmissaoInicial, dataEmissaoFinal,null,null);
	}
	public byte[] gerarArquivoRecebimentoTerceiroReimpressao(Date inicio,Date fim,Date dataEmissaoInicial,Date dataEmissaoFinal,EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}

			String dataSqlEmissaoInicial = "";
			String dataSqlEmissaoFinal = "";
			if(dataEmissaoInicial!=null ){
				dataSqlEmissaoInicial = df.format(dataEmissaoInicial);
			}
			if(dataEmissaoFinal!=null ){
				dataSqlEmissaoFinal = df.format(dataEmissaoFinal);
			}
			String where = "";
			String ids = "";
			String sql = "";
			List<DuplicataParcelaCobranca> lista = new ArrayList<>();
			
			
			sql = sql + " select cob.idDuplicataParcelaCobranca as id ";
			sql = sql + " from duplicataparcelacobranca cob ";
			sql = sql + " inner join duplicataparcela par on cob.idduplicataparcela = par.idduplicataparcela ";
			sql = sql + " inner join duplicata dupli on dupli.idduplicata = par.idduplicata ";
			sql = sql + " left outer join (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac ";
			sql = sql + " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
			sql = sql + " group by ac.idDuplicataNova) "; 
			sql = sql + " as acordo on acordo.idDuplicataNova=PAR.idduplicata ";
			sql = sql + " where data >= '"+df.format(inicio)+" 00:00:00' and data <= '"+df.format(fim)+" 23:59:59' and ocorrencia = 'P'";
			if(!dataSqlEmissaoInicial.equals(""))
				sql = sql + " and if((par.acordo<>'S' or par.acordo is null),dupli.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00',acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00' ) ";
			if(!dataSqlEmissaoFinal.equals(""))
				sql = sql +"  and if((par.acordo<>'S' or par.acordo is null),dupli.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59')";
			Connection con = ConexaoUtil.getConexaoPadrao();
			ResultSet rs = con.createStatement().executeQuery(sql);
			System.out.println(sql);
			while(rs.next()){
				if(ids.equals(""))
					ids += rs.getString("id");
				else
					ids += ","+rs.getString("id");
			}
			con.close();
			if(!ids.equals("")){
				where = " idDuplicataParcelaCobranca in("+ids+")";
				System.out.println(ids);
			 lista = new DuplicataParcelaCobrancaService().listarPaginadoSqlLivreGenerico(manager, Integer.MAX_VALUE, 1, where, null, null).getRecordList();
			}
			else
			{
				throw new Exception("Não há nenhum recebimento de cobrança!");
			}

			File arquivo = new File("arquivo.txt");
			if(arquivo.exists())
			{
				arquivo.delete();
			}
			FileWriter manipuladorArquivo = new FileWriter(arquivo);
			PrintWriter manipuladorTexto = new PrintWriter(manipuladorArquivo);
			manipuladorTexto.print(new DuplicataParcelaService().criarCabecarioArquivoTeceiro());
			for (int i = 0; i < lista.size(); i++) 
			{
				DuplicataParcela par = lista.get(i).getDuplicataParcela();
				if(!par.getSituacao().equals("3") && !par.getTipoPagamento().equals("5"))
				{
					String linha = new DuplicataParcelaService().duplicataParcelaParaString(par);
					manipuladorTexto.print(linha);
				}

			}
			manipuladorArquivo.close();

			if(transacaoIndependente)
			{
				transaction.commit();
			}
			byte[] array = Files.readAllBytes(arquivo.toPath());
			return array;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{

			if(transacaoIndependente)
				manager.close();
		}
	}

	@IgnSegurancaAcao
	public byte[] reimpressaoArquivoCobrancaTerceiro(Date dataInicial,Date dataFinal) throws Exception
	{
		return reimpressaoArquivoCobrancaTerceiro(dataInicial, dataFinal,"","", null, null);
	}

	@IgnSegurancaAcao
	public byte[] reimpressaoArquivoCobrancaTerceiro(Date dataInicial,Date dataFinal, String dataEmissaoInicial, String dataEmissaoFinal) throws Exception
	{
		return reimpressaoArquivoCobrancaTerceiro(dataInicial, dataFinal,dataEmissaoInicial,dataEmissaoFinal, null, null);
	}

	@IgnSegurancaAcao
	public byte[] reimpressaoArquivoCobrancaTerceiro(Date dataInicial,Date dataFinal, String dataEmissaoInicial, String dataEmissaoFinal,EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dataSqlEmissaoInicial = null;
		String dataSqlEmissaoFinal = null;
		if(!dataEmissaoInicial.equals(""))
			dataSqlEmissaoInicial = IgnUtil.dateFormatSql.format(IgnUtil.dateFormat.parse(dataEmissaoInicial));
		if(!dataEmissaoFinal.equals(""))
			dataSqlEmissaoFinal = IgnUtil.dateFormatSql.format(IgnUtil.dateFormat.parse(dataEmissaoFinal));

		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}

			String query = "SELECT * FROM duplicataparcelacobranca dupCob ";
			query += " INNER JOIN duplicataparcela par on par.idduplicataParcela = dupCob.idDuplicataParcela ";
			query += " INNER JOIN duplicata dup on dup.idDuplicata = par.idduplicata ";
			query += " LEFT OUTER JOIN (select dupAntiga.dataCompra, ac.idDuplicataNova from duplicataacordo ac "; 
			query += " INNER JOIN duplicata dupAntiga on dupAntiga.numeroDuplicata=ac.numeroDuplicataAntiga ";
			query += " GROUP BY ac.idDuplicataNova) as acordo on acordo.idduplicatanova =dup.idduplicata ";
			query += " WHERE data >= '"+sdf.format(dataInicial)+" 00:00:00' and data <= '"+sdf.format(dataFinal)+" 23:59:59' and ocorrencia = 'C' ";
			if(dataSqlEmissaoInicial != null)
				query += " AND if(par.acordo<>'S',dup.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00',acordo.dataCompra >= '"+dataSqlEmissaoInicial+" 00:00:00') ";
			if(dataSqlEmissaoFinal != null)
				query += " AND if(par.acordo<>'S',dup.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59',acordo.dataCompra <= '"+dataSqlEmissaoFinal+" 23:59:59')";
			query += " ORDER BY par.idcliente,par.numeroDuplicata,par.numeroParcela";

			System.out.println("Reimpressao Arquivo de Cobrança: \n"+query);

			ResultSet rs = ConexaoUtil.getConexaoPadrao().createStatement().executeQuery(query);

			List<DuplicataParcelaCobranca> lista = new ArrayList<>();

			while (rs.next())
			{
				DuplicataParcelaCobranca dupCobranca = new DuplicataParcelaCobranca();
				dupCobranca = new DuplicataParcelaCobrancaService().recuperarPorId(rs.getInt("idDuplicataParcelaCobranca"));
				lista.add(dupCobranca);
			}

			File arquivo = new File("arquivo.txt");
			if(arquivo.exists())
				arquivo.delete();

			FileWriter manipuladorArquivo = new FileWriter(arquivo);
			PrintWriter manipuladorTexto = new PrintWriter(manipuladorArquivo);
			manipuladorTexto.print(new DuplicataParcelaService().criarCabecarioArquivoTeceiro());

			String linha = "";

			for (int i = 0; i < lista.size(); i++) 
			{
				DuplicataParcela par = lista.get(i).getDuplicataParcela();
				linha = new DuplicataParcelaService().duplicataParcelaParaString(par);
				manipuladorTexto.print(linha);
			}

			manipuladorArquivo.close();

			if(transacaoIndependente)
				transaction.commit();

			byte[] array = Files.readAllBytes(arquivo.toPath());
			return array;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{

			if(transacaoIndependente)
				manager.close();
		}
	}


	private String criarCabecalhoArquivoRecebimento(Date dataProcessamento)
	{
		SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
		String cabecalho ="00"+df.format(dataProcessamento);
		return cabecalho;
	}
	private String parcelaParaTextoArquivoRecebimento(DuplicataParcela parcela)
	{
		DecimalFormat decimalf = new DecimalFormat("0.00");  
		SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
		String linha ="07";
		linha+=""+(IgnUtil.zeroEsquerda(Long.parseLong(parcela.getCliente().getCpfCnpj()), 14));
		linha+=""+(IgnUtil.espacoDireita(parcela.getNumeroDuplicata().toString(), 20));
		linha+=""+(IgnUtil.espacoDireita(parcela.getNumeroParcela().toString(), 15));
		linha+=""+(IgnUtil.zeroEsquerda(decimalf.format(parcela.getValorPago()).replace(",","."), 11));
		linha+=""+(df.format(parcela.getDataBaixa()));
		linha+=""+(IgnUtil.zeroEsquerda(decimalf.format(parcela.getValorParcela()).replace(",","."), 11));
		linha+=""+(IgnUtil.zeroEsquerda(decimalf.format(parcela.getValorDesconto()).replace(",","."), 11));
		linha+=""+(IgnUtil.zeroEsquerda(decimalf.format(parcela.getJurosPago()).replace(",","."), 11));
		linha+=""+(IgnUtil.zeroEsquerda(0.0,11));
		linha+=""+(IgnUtil.zeroEsquerda(0.0,11));
		linha+=""+(IgnUtil.zeroEsquerda(0.0,11));
		linha+=""+(IgnUtil.zeroEsquerda(0.0,11));
		linha+=""+(IgnUtil.zeroEsquerda(0.0,11));
		linha+=""+(IgnUtil.zeroEsquerda(0.0,11));
		linha+=""+(IgnUtil.espacoDireita("", 420));
		return linha;

	}
	private String criarRodapeArquivoRecebimento(Integer totalDevedores,Integer totalContratos,Double valorTotalContratosVencidos)
	{
		DecimalFormat decimalf = new DecimalFormat("0.00");  
		String linha = "99";
		linha +=""+(IgnUtil.zeroEsquerda(totalDevedores.doubleValue(), 10));
		linha +=""+(IgnUtil.zeroEsquerda(totalContratos.doubleValue(), 10));
		linha+=""+(IgnUtil.zeroEsquerda(decimalf.format(valorTotalContratosVencidos).replace(",","."), 14));
		linha+=""+(IgnUtil.zeroEsquerda(0.0,14));
		linha+=""+(IgnUtil.espacoDireita("", 550));
		return linha;

	}
}