package br.com.desenv.nepalign.service;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.EmpresaBancos;
import br.com.desenv.nepalign.persistence.EmpresaBancosPersistence;

public class EmpresaBancosService extends GenericServiceIGN<EmpresaBancos, EmpresaBancosPersistence>
{
	public EmpresaBancosService() { super(); }
}