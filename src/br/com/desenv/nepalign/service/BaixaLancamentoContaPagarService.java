package br.com.desenv.nepalign.service;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.log4j.Logger;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.BaixaLancamentoContaPagar;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.LancamentoContaPagar;
import br.com.desenv.nepalign.persistence.BaixaLancamentoContaPagarPersistence;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class BaixaLancamentoContaPagarService extends GenericServiceIGN<BaixaLancamentoContaPagar, BaixaLancamentoContaPagarPersistence>
{
	private static final Logger logger = Logger.getLogger("CPAG");
	
	private static final IgnUtil ignUtil = new IgnUtil();
	private static final SimpleDateFormat simpleDateFormatMesAno = new SimpleDateFormat("MM/yyyy");
	
	private static final LancamentoContaPagarService lancamentoContaPagarService = new LancamentoContaPagarService();
	
	private static final LogOperacaoService logOperacaoService = new LogOperacaoService();

	public BaixaLancamentoContaPagarService() 
	{
		super();
	}
	
	public br.com.desenv.nepalign.model.BaixaLancamentoContaPagar salvar(List<br.com.desenv.nepalign.model.BaixaLancamentoContaPagar> listaEntidade) throws Exception
	{
		EntityManager manager = null;

		try
		{
			manager = ConexaoUtil.getEntityManager();
			manager.getTransaction().begin();

			for(final br.com.desenv.nepalign.model.BaixaLancamentoContaPagar entidade:listaEntidade)
			{	
				if(entidade.getId() != null)
				{
					logOperacaoService.gravarOperacaoAlterar(manager, manager.getTransaction(), entidade, gerarObservacaoLog(entidade));
					atualizar(manager, manager.getTransaction(), entidade);	
				}
				else
				{
					salvar(manager, manager.getTransaction(), entidade);
					logOperacaoService.gravarOperacaoInclusao(manager, manager.getTransaction(), entidade, gerarObservacaoLog(entidade));
				}					
			}
			
			manager.getTransaction().commit();
		}
		catch(Exception ex)
		{
			logger.error("Não foi possível salvar as Baixas", ex);
			
			if(manager.getTransaction().isActive())
				manager.getTransaction().rollback();
	
			throw ex;
		}
		finally
		{
			if(manager != null)
				manager.close();
		}
		return null;
	}

	public br.com.desenv.nepalign.model.BaixaLancamentoContaPagar salvar(br.com.desenv.nepalign.model.BaixaLancamentoContaPagar entidade) throws Exception
	{
		try
		{
			return salvar(null, null, entidade);	
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}

	public br.com.desenv.nepalign.model.BaixaLancamentoContaPagar salvar(EntityManager manager, EntityTransaction transaction, br.com.desenv.nepalign.model.BaixaLancamentoContaPagar entidade) throws Exception
	{
		Boolean transacaoIndependente = false;
		
		try
		{			
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			} 

			if(!lancamentoContaPagarService.verificarPermissaoManutencaoContasAPagar())
				if(!verificarPermissaoDataBaixa(entidade.getDataPagamento(), entidade.getLancamentoContaPagar().getEmpresaFisica()))
					throw new Exception("Não é possível baixar um lançamento com uma data anterior ao mês/ano aberto da Empresa.");
			
			br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagar = lancamentoContaPagarService.recuperarPorId(manager, entidade.getLancamentoContaPagar().getId());
			
			if(lancamentoContaPagar.getSituacaoLancamento().getId().equals(SituacaoLancamentoService.BAIXADO))
				throw new Exception("Não é possível fazer baixas para lançamentos já quitados.!");
			if(entidade.getValor() <= 0.0)
				throw new Exception("Valor da baixa inválido!");
			
			entidade = salvar(manager, transaction, entidade, true);
			logOperacaoService.gravarOperacaoInclusao(manager, transaction, entidade, gerarObservacaoLog(entidade));
			
			lancamentoContaPagarService.atualizarLancamentoComParcelas(manager, transaction, lancamentoContaPagar, entidade.getConsiderarBaixaIntegral() == 0x00000001);
			
			if(transacaoIndependente)
				transaction.commit();
			
			return entidade;
		}
		catch(Exception ex)
		{
			logger.error("Não foi possível Baixar o Lançamento", ex);
			
			if(transacaoIndependente)
				transaction.rollback();
			
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	
	public void baixarMultiplosLancamentos(List<br.com.desenv.nepalign.model.LancamentoContaPagar> listaLancamentoContaPagar, br.com.desenv.nepalign.model.BaixaLancamentoContaPagar estruturaBaixaLancamentoContaPagar) throws Exception
	{
		this.baixarMultiplosLancamentos(listaLancamentoContaPagar, estruturaBaixaLancamentoContaPagar, false, null, null);
	}
	
	public void baixarMultiplosLancamentos(List<br.com.desenv.nepalign.model.LancamentoContaPagar> listaLancamentoContaPagar, br.com.desenv.nepalign.model.BaixaLancamentoContaPagar estruturaBaixaLancamentoContaPagar, Boolean baixarSomenteValorSaldo) throws Exception
	{
		this.baixarMultiplosLancamentos(listaLancamentoContaPagar, estruturaBaixaLancamentoContaPagar, baixarSomenteValorSaldo, null, null);
	}
	
	public void baixarMultiplosLancamentos(List<br.com.desenv.nepalign.model.LancamentoContaPagar> listaLancamentoContaPagar, br.com.desenv.nepalign.model.BaixaLancamentoContaPagar estruturaBaixaLancamentoContaPagar, Boolean baixarSomenteValorSaldo, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false; 
		
		if(manager == null || transaction == null)
		{
			manager = ConexaoUtil.getEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			transacaoIndependente = true;
		} 
		
		try
		{
			final String codigoSeguranca = "MB".concat(ignUtil.getRandomNumberToString());
			
			for (final br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagar : listaLancamentoContaPagar)
			{
				br.com.desenv.nepalign.model.BaixaLancamentoContaPagar baixaLancamentoContaPagar = (br.com.desenv.nepalign.model.BaixaLancamentoContaPagar) estruturaBaixaLancamentoContaPagar.clone();
				baixaLancamentoContaPagar.setId(null);
				baixaLancamentoContaPagar.setLancamentoContaPagar(lancamentoContaPagar);
				baixaLancamentoContaPagar.setDataPagamento(estruturaBaixaLancamentoContaPagar.getDataPagamento());
				baixaLancamentoContaPagar.setValorOriginal(baixarSomenteValorSaldo ? lancamentoContaPagar.getValorSaldo() : lancamentoContaPagar.getValorOriginal());
				baixaLancamentoContaPagar.setValorDesconto(lancamentoContaPagar.getValorDesconto());
				baixaLancamentoContaPagar.setValorJuros(lancamentoContaPagar.getValorMulta());
				baixaLancamentoContaPagar.setValor(baixarSomenteValorSaldo ? lancamentoContaPagar.getValorSaldo() : lancamentoContaPagar.getValorOriginal());
				baixaLancamentoContaPagar.setValorMulta(lancamentoContaPagar.getValorMulta());
				baixaLancamentoContaPagar.setCodigoSeguranca(codigoSeguranca);
				
				salvar(manager, transaction, baixaLancamentoContaPagar);
				logOperacaoService.gravarOperacaoInclusao(manager, transaction, baixaLancamentoContaPagar, gerarObservacaoLog(baixaLancamentoContaPagar));
			}
			
			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			logger.error("Não foi possível Baixar Multiplos Lançamentos", ex);
			
			if(transacaoIndependente)
				transaction.rollback();
			
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	
	public void atualizarBaixa(List<br.com.desenv.nepalign.model.BaixaLancamentoContaPagar> lista, List<br.com.desenv.nepalign.model.BaixaLancamentoContaPagar> listaExclusao) throws Exception
	{
		atualizarBaixa(null, null, lista, listaExclusao);
	}
	
	public void atualizarBaixa(EntityManager manager, EntityTransaction transaction, List<br.com.desenv.nepalign.model.BaixaLancamentoContaPagar> lista, List<br.com.desenv.nepalign.model.BaixaLancamentoContaPagar> listaExclusao) throws Exception
	{
		Boolean transacaoIndependente = false;
		
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			
			LancamentoContaPagar lancamentoContaPagar = null;
			if(lista.size() > 0x00)
				lancamentoContaPagar = lista.get(0).getLancamentoContaPagar();
			else
				lancamentoContaPagar = listaExclusao.get(0).getLancamentoContaPagar();
			
			for (final br.com.desenv.nepalign.model.BaixaLancamentoContaPagar baixaLancamentoContaPagar : listaExclusao)
				excluir(manager, transaction, baixaLancamentoContaPagar, false);
			
			for (final br.com.desenv.nepalign.model.BaixaLancamentoContaPagar baixaLancamentoContaPagar : lista)
			{
				if(baixaLancamentoContaPagar.getId() == null)
				{
					salvar(manager, transaction, baixaLancamentoContaPagar);
					logOperacaoService.gravarOperacaoInclusao(manager, transaction, baixaLancamentoContaPagar, gerarObservacaoLog(baixaLancamentoContaPagar));
				}
				else
				{
					logOperacaoService.gravarOperacaoAlterar(manager, transaction, baixaLancamentoContaPagar, gerarObservacaoLog(baixaLancamentoContaPagar));
					atualizar(manager, transaction, baixaLancamentoContaPagar, false);	
				}
			}
			
			lancamentoContaPagarService.atualizarLancamentoComParcelas(manager, transaction, 
					lancamentoContaPagar, 
					lista.size() == 0x00 ? false : lista.get(lista.size() - 0x01).getConsiderarBaixaIntegral() == 0x01);
			
			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			logger.error("Não foi possível atualizar a Baixa", ex);
			
			if(transacaoIndependente)
				transaction.rollback();
			
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	
	public void excluir(EntityManager manager, EntityTransaction transaction, br.com.desenv.nepalign.model.BaixaLancamentoContaPagar entidade, boolean logar) throws Exception
	{
		excluir(null, null, entidade);
	}
	
	public void excluir(br.com.desenv.nepalign.model.BaixaLancamentoContaPagar entidade) throws Exception
	{
		excluir(null, null, entidade);
	}
	
	public void excluir(EntityManager manager, EntityTransaction transaction, br.com.desenv.nepalign.model.BaixaLancamentoContaPagar entidade) throws Exception
	{
		try
		{
			if(!lancamentoContaPagarService.verificarPermissaoManutencaoContasAPagar())
			{
				if(!lancamentoContaPagarService.verificarPermissaoMesContasAPagar(entidade.getLancamentoContaPagar()))
					throw new Exception("Não é possível excluir uma baixa com uma data anterior ao mês/ano aberto da Empresa.");
				
				Calendar dataBaixaLancamento = Calendar.getInstance();
				dataBaixaLancamento.set(Calendar.MONTH, Integer.parseInt(IgnUtil.dateFormat.format(entidade.getDataPagamento()).split("/")[0x1]) -1);
				dataBaixaLancamento.set(Calendar.YEAR, Integer.parseInt(IgnUtil.dateFormat.format(entidade.getDataPagamento()).split("/")[0x2]));
				dataBaixaLancamento.set(Calendar.DAY_OF_MONTH, dataBaixaLancamento.getActualMaximum(Calendar.DAY_OF_MONTH));
				
				Calendar dataPermitidaExclusao = Calendar.getInstance();
				dataPermitidaExclusao.setTime(new Date());
				dataPermitidaExclusao.add(Calendar.DAY_OF_YEAR, -0x00000003);
				dataPermitidaExclusao.set(Calendar.HOUR_OF_DAY, 0x00000000);
				dataPermitidaExclusao.set(Calendar.MINUTE, 		0x00000000);
				dataPermitidaExclusao.set(Calendar.SECOND, 		0x00000000);
				dataPermitidaExclusao.set(Calendar.MILLISECOND, 0x00000000);
				
				if(dataBaixaLancamento.before(dataPermitidaExclusao))
					throw new Exception("Não é possível excluir uma baixa feita à mais de 3 dias.");
			}
			
			logOperacaoService.gravarOperacaoExclusao(manager, transaction, entidade, gerarObservacaoLog(entidade));
			super.excluir(manager, transaction, entidade, false);
			
			lancamentoContaPagarService.atualizarLancamentoComParcelas(manager, transaction, entidade.getLancamentoContaPagar(), false);
		}
		catch(Exception ex)
		{
			logger.error("Não foi possível excluir a Baixa", ex);
			
			throw ex;
		}
	}
	
	public void baixaLancamentoContaPagar(br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagar) throws Exception
	{
		baixaLancamentoContaPagar(lancamentoContaPagar, false);
	}
	
	public void baixaLancamentoContaPagar(br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagar, Boolean baixaAutomatica) throws Exception
	{
		baixaLancamentoContaPagar(lancamentoContaPagar, baixaAutomatica, null, null);
	}
	
	public void baixaLancamentoContaPagar(br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagar, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		baixaLancamentoContaPagar(lancamentoContaPagar, false, manager, transaction);
	}
	
	public void baixaLancamentoContaPagar(br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagar, Boolean baixaAutomatica, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
				
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			
			final br.com.desenv.nepalign.model.BaixaLancamentoContaPagar baixaLancamentoContaPagar = new br.com.desenv.nepalign.model.BaixaLancamentoContaPagar();
			baixaLancamentoContaPagar.setContaCorrente(lancamentoContaPagar.getContaCorrente());
			baixaLancamentoContaPagar.setValorOriginal(lancamentoContaPagar.getValorOriginal());
			baixaLancamentoContaPagar.setValor(lancamentoContaPagar.getValorOriginal());
			baixaLancamentoContaPagar.setValorDesconto(lancamentoContaPagar.getValorDesconto());
			baixaLancamentoContaPagar.setConsiderarBaixaIntegral(0x00000001);
			baixaLancamentoContaPagar.setDataLancamento(lancamentoContaPagar.getDataLancamento());
			baixaLancamentoContaPagar.setDataPagamento(lancamentoContaPagar.getDataVencimento());
			baixaLancamentoContaPagar.setFormaPagamento(lancamentoContaPagar.getFormaPagamento());
			baixaLancamentoContaPagar.setLancamentoContaPagar(lancamentoContaPagar);
			
			if(baixaAutomatica)
				baixaLancamentoContaPagar.setCodigoSeguranca("BA" + (lancamentoContaPagar.getCodigoSeguranca() == null ? ignUtil.getRandomNumberToString() : "-" + lancamentoContaPagar.getCodigoSeguranca()));
			
			salvar(manager, transaction, baixaLancamentoContaPagar); 
			logOperacaoService.gravarOperacaoInclusao(manager, transaction, baixaLancamentoContaPagar, gerarObservacaoLog(baixaLancamentoContaPagar));
			
			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			logger.error("Não foi possível Baixar o Lançamento", ex);
			
			if(transacaoIndependente)
				transaction.rollback();
			
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	
	public Boolean existePagamento(br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagar) throws Exception
	{
		return this.existePagamento(null, lancamentoContaPagar);
	}
	
	public Boolean existePagamento(List<br.com.desenv.nepalign.model.LancamentoContaPagar> listaLancamentoContaPagar) throws Exception
	{
		return this.existePagamento(null, listaLancamentoContaPagar);
	}
	
	public Boolean existePagamento(EntityManager manager, List<br.com.desenv.nepalign.model.LancamentoContaPagar> listaLancamentoContaPagar) throws Exception
	{
		Boolean managerIndependente = false;
		
		try
		{
			if(manager == null)
			{
				manager = ConexaoUtil.getEntityManager();
				managerIndependente = true;
			}
			
			
			for (br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagar : listaLancamentoContaPagar) 
				if(existePagamento(manager, lancamentoContaPagar))
					return true;
			
			return false;
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
	
	public Boolean existePagamento(EntityManager manager, br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagar) throws Exception
	{
		Boolean managerIndependente = false;
		
		try
		{
			if(manager == null)
			{
				manager = ConexaoUtil.getEntityManager();
				managerIndependente = true;
			}
			
			br.com.desenv.nepalign.model.BaixaLancamentoContaPagar baixaLancamentoContaPagarCriterio = new br.com.desenv.nepalign.model.BaixaLancamentoContaPagar();
			baixaLancamentoContaPagarCriterio.setLancamentoContaPagar(lancamentoContaPagar);
			
			if(this.listarPorObjetoFiltro(manager, baixaLancamentoContaPagarCriterio, null, null, null).size() > 0x00000000)
				return true;
			else
				return false;
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
	
	public List<br.com.desenv.nepalign.model.BaixaLancamentoContaPagar> listarPagamentos(EntityManager manager, br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagar) throws Exception
	{
		Boolean managerIndependente = false;
		
		try
		{
			if(manager == null)
			{
				manager = ConexaoUtil.getEntityManager();
				managerIndependente = true;
			}
			
			br.com.desenv.nepalign.model.BaixaLancamentoContaPagar baixaLancamentoContaPagarFiltro = new br.com.desenv.nepalign.model.BaixaLancamentoContaPagar();
			baixaLancamentoContaPagarFiltro.setLancamentoContaPagar(lancamentoContaPagar);
			
			return super.listarPorObjetoFiltro(manager, baixaLancamentoContaPagarFiltro, null, null, null);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
	
	public List<br.com.desenv.nepalign.model.BaixaLancamentoContaPagar> listarPagamentos(br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagar) throws Exception
	{
		return listarPagamentos(null, lancamentoContaPagar);
	}
	
	public boolean verificarPermissaoDataBaixa(final Date dataBaixa, final EmpresaFisica empresaFisica)
	{
		return simpleDateFormatMesAno.format(dataBaixa).equals(String.format("%02d", empresaFisica.getMesAbertoFin()).concat("/").concat(String.valueOf(empresaFisica.getAnoAbertoFin())));
	}
	
	/**
	 * Metodo para geração da observação no logoperacao
	 * @param lancamentoContaPagar
	 * @return Observacao completa
	 */
	public final String gerarObservacaoLog(final BaixaLancamentoContaPagar baixaLancamentoContaPagar)
	{
		return "N. DOCUMENTO DA BAIXA ".concat(baixaLancamentoContaPagar.getLancamentoContaPagar().getNumeroDocumento()).concat(" ").
				concat("EMP. ").concat(String.valueOf(baixaLancamentoContaPagar.getLancamentoContaPagar().getEmpresaFisica().getId())).concat(" ").
				concat("DT VENC. LANC. ").concat(IgnUtil.dateFormat.format(baixaLancamentoContaPagar.getLancamentoContaPagar().getDataVencimento())).concat(" ").
				concat("DT BAIXA. ").concat(IgnUtil.dateFormat.format(baixaLancamentoContaPagar.getDataPagamento())).concat(" ").
				concat("VLR BAIXA ").concat(NumberFormat.getCurrencyInstance().format(baixaLancamentoContaPagar.getValor())).concat(" ").
				concat("BX. INTEGRAL? ").concat(String.valueOf(baixaLancamentoContaPagar.getConsiderarBaixaIntegral()));
	}
}