package br.com.desenv.nepalign.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.CaixaDia;
import br.com.desenv.nepalign.model.PagamentoCartao;
import br.com.desenv.nepalign.model.PagamentoPedidoVenda;
import br.com.desenv.nepalign.model.Parcelapagamentocartao;
import br.com.desenv.nepalign.persistence.PagamentoCartaoPersistence;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
public class PagamentoCartaoService extends GenericServiceIGN<PagamentoCartao, PagamentoCartaoPersistence>
{

	public PagamentoCartaoService() 
	{
		super();
	}

	public PagamentoCartao gerarParcelaPagamentoCartao(PagamentoCartao pag,Double valorParcela,Integer vezes,EntityManager manager,EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			this.salvar(manager, transaction, pag);
			Parcelapagamentocartao parcela = new Parcelapagamentocartao();
			List<Parcelapagamentocartao> listaPar = new ArrayList<>();
			Integer mes = new Integer(1);
			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
			Date data = new Date(); 
			Calendar cal = null;
			for (int i = 1; i <= vezes; i++) 
			{
				String dataString = formatter.format(new Date());
				data = (java.util.Date) formatter.parse(dataString);
				cal=Calendar.getInstance();
				cal.setTime(data);

				cal.add(Calendar.MONTH, mes);
				parcela = new Parcelapagamentocartao();
				parcela.setDataVencimento(cal.getTime());
				parcela.setNumero(i);
				parcela.setValor(valorParcela);
				parcela.setPagamentoCartao(pag);
				listaPar.add(parcela);
				mes += 1;
			}
			for (int i = 0; i < listaPar.size(); i++) 
			{
				new ParcelapagamentocartaoService().salvar(manager, transaction, listaPar.get(i));

			}

			if(transacaoIndependente)
			{
				transaction.commit();
			}
			return pag;
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}

	}
	public List<PagamentoCartao> recuperarPagamentosCartaoSemEmitente(CaixaDia caixa) throws Exception
	{
		return new PagamentoCartaoPersistence().recuperarPagamentosCartaoSemEmitente(caixa);
	}
	public void excluirPagamento(PagamentoCartao pag,EntityManager manager,EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}

			List<Parcelapagamentocartao> listaPar = new ArrayList<>();
			Parcelapagamentocartao parC = new Parcelapagamentocartao();
			parC.setPagamentoCartao(pag);
			listaPar.addAll(new ParcelapagamentocartaoService().listarPorObjetoFiltro(manager, parC, null, null, null)); 

			for (int i = 0; i < listaPar.size(); i++) 
			{
				new ParcelapagamentocartaoService().excluir(manager, transaction, listaPar.get(i));
			}
			this.excluir(manager, transaction, pag);
			if(transacaoIndependente)
			{
				transaction.commit();
			}

		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
}

