package br.com.desenv.nepalign.service;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.Bancos;
import br.com.desenv.nepalign.persistence.BancosPersistence;

public class BancosService extends GenericServiceIGN<Bancos, BancosPersistence>
{
	public BancosService() { super(); }
}