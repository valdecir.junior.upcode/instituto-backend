package br.com.desenv.nepalign.service;

import java.util.List;

import flex.messaging.io.ArrayList;
import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.Acao;
import br.com.desenv.nepalign.model.Funcionalidade;
import br.com.desenv.nepalign.model.Modulo;
import br.com.desenv.nepalign.persistence.ModuloPersistence;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.sun.org.apache.bcel.internal.classfile.Attribute;

public class ModuloService extends GenericServiceIGN<Modulo, ModuloPersistence>
{

	public ModuloService() 
	{
		super();
	}
	
	
	public Document GenerateXML() throws Exception
	{
	 	DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.newDocument();
		
		
		Element rootElement = doc.createElement("root");
		List existentsModules = (List)this.listarPorObjetoFiltro(new Modulo());
		for(int i = 0; i < existentsModules.size(); i++)
		{ 
			Element moduleElement = doc.createElement("node");
			Modulo module = (Modulo)existentsModules.get(i);
			
			Attr attr = doc.createAttribute("label");
			attr.setValue(module.getDescricao());
			moduleElement.setAttributeNode(attr);
			
			Funcionalidade criterioFuncionalidade = new Funcionalidade();
			criterioFuncionalidade.setModulo(module);
			List existentesFuncs = (List)new FuncionalidadeService().listarPorObjetoFiltro(criterioFuncionalidade);
			
			for(int x = 0; x < existentesFuncs.size(); x++)
			{
				criterioFuncionalidade = (Funcionalidade)existentesFuncs.get(x);
				
				Attr attrFunc = doc.createAttribute("label");
				attrFunc.setValue(criterioFuncionalidade.getDescricao());
				Element funcElement = doc.createElement("node");
				funcElement.setAttributeNode(attrFunc);
				
				Acao criterioAcao = new Acao();
				criterioAcao.setFuncionalidade(criterioFuncionalidade);
				
				List existentsActions = (List) new AcaoService().listarPorObjetoFiltro(criterioAcao);
				
				for(int y = 0; y < existentsActions.size(); y++)
				{
					criterioAcao = (Acao)existentsActions.get(y);
					
					
					Attr attrActionId = doc.createAttribute("id");
					attrActionId.setValue(criterioAcao.getId().toString());
					Attr attrAction = doc.createAttribute("label");
					attrAction.setValue(criterioAcao.getDescricao());
					Element actionElement = doc.createElement("node");
					actionElement.setAttributeNode(attrAction);
					actionElement.setAttributeNode(attrActionId);
					funcElement.appendChild(actionElement);
				}
				if(existentesFuncs.size() > 0 && funcElement.getElementsByTagName("node").getLength() > 0)
					moduleElement.appendChild(funcElement);
			}
			if(existentsModules.size() > 0 && moduleElement.getElementsByTagName("node").getLength() > 0)
				rootElement.appendChild(moduleElement);
		}
		doc.appendChild(rootElement);
		return doc;
	}
}

