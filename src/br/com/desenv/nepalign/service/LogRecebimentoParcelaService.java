package br.com.desenv.nepalign.service;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.LogRecebimentoParcela;
import br.com.desenv.nepalign.persistence.LogRecebimentoParcelaPersistence;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class LogRecebimentoParcelaService extends GenericServiceIGN<LogRecebimentoParcela, LogRecebimentoParcelaPersistence>
{

	public LogRecebimentoParcelaService() 
	{
		super();
	}
	
	public LogRecebimentoParcela salvarDataHoje(LogRecebimentoParcela log, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		   try
		   {
			   if(manager == null || transaction == null)
			   {
				   manager = ConexaoUtil.getEntityManager();
				   transaction = manager.getTransaction();
				   transaction.begin();
				   transacaoIndependente = true;
			   }
			   log.setData(new Date());
			   if(transacaoIndependente)
				   transaction.commit();
		   }
		   catch(Exception ex)
		   {
			   if(transacaoIndependente)
				   transaction.rollback();
			   
			   
			   ex.printStackTrace();
			   throw ex;
		   }
		   finally
		   {
			   if(transacaoIndependente)
				   manager.close();
		   }
		return this.salvar(manager, transaction, log);
	}

}

