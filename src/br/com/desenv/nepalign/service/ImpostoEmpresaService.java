package br.com.desenv.nepalign.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao.Grupo;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.ImpostoEmpresa;
import br.com.desenv.nepalign.persistence.ImpostoEmpresaPersistence;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ImpostoEmpresaService extends GenericServiceIGN<ImpostoEmpresa, ImpostoEmpresaPersistence>
{

	public ImpostoEmpresaService() 
	{
		super();
	}
	
	public ImpostoEmpresa recuperarImpostoEmpresa(EmpresaFisica empresaFisica)throws Exception
	{
		return recuperarImpostoEmpresa(null, empresaFisica);
	}
	
	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao(gruposAutorizados={Grupo.Faturamento}, nomeAcao="Calcular Imposto Empresa")
	public ImpostoEmpresa recuperarImpostoEmpresa( EntityManager manager, EmpresaFisica empresaFisica)throws Exception
	{
		boolean transacaoIndependente = false;
		
		try
		{
			if(manager == null)
			{
				manager = ConexaoUtil.getEntityManager();
				manager.getTransaction().begin();
				
				transacaoIndependente = true;
			}
			
			ImpostoEmpresa impostoEmpresaFiltro = new ImpostoEmpresa();
			impostoEmpresaFiltro.setEmpresaFisica(empresaFisica);
			
			ImpostoEmpresa impostoEmpresa = new ImpostoEmpresa();
			
			List<ImpostoEmpresa> listaImpostoEmpresa = new ArrayList<>();
			listaImpostoEmpresa = new ImpostoEmpresaService().listarPorObjetoFiltro(manager, impostoEmpresaFiltro, null, null, null);
			
			if(listaImpostoEmpresa.size() > 0)
				impostoEmpresa = listaImpostoEmpresa.get(listaImpostoEmpresa.size()-1);
			else
				throw new Exception("Não foi encontrado percentual de imposto para esta empresa.");
			
			if(transacaoIndependente)
				manager.getTransaction().commit();
			
			return impostoEmpresa;
		}
		catch(Exception ex)
		{
			manager.getTransaction().rollback();
			
			throw ex;
		}
	}

}

