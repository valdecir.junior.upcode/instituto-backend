package br.com.desenv.nepalign.service;

import java.lang.reflect.Method;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao;
import br.com.desenv.nepalign.model.Acao;
import br.com.desenv.nepalign.persistence.AcaoPersistence;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class AcaoService extends GenericServiceIGN<Acao, AcaoPersistence>
{
	private static final FuncionalidadeService funcionalidadeService = new FuncionalidadeService();
	
	public AcaoService() 
	{
		super();
	}
	
	public final boolean acaoCadastrada(final String nomeMetodo) throws Exception
	{
		return acaoCadastrada(null, nomeMetodo);
	}
	
	public final boolean acaoCadastrada(EntityManager manager, final String nomeMetodo) throws Exception
	{
		boolean managerIndependente = false;
		try
		{
			if((managerIndependente = (manager == null)))
				manager = ConexaoUtil.getEntityManager();
			
			final Acao acaoCriterio = new Acao();
			acaoCriterio.setNomeMetodo(nomeMetodo);
			
			return listarPorObjetoFiltro(manager, acaoCriterio, null, null, null).size() > 0x00000000;
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
	
	public final Acao cadastrarAcao(final IgnSegurancaAcao ignSegurancaAcao, final Method method) throws Exception
	{
		return cadastrarAcao(null, ignSegurancaAcao, method);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public final Acao cadastrarAcao(EntityManager manager, final IgnSegurancaAcao ignSegurancaAcao, final Method method) throws Exception
	{
		boolean managerIndependente = false;
		try
		{
			if((managerIndependente = (manager == null)))
				manager = ConexaoUtil.getEntityManager();
			
			final Acao acao = new Acao();
			acao.setFuncionalidade(funcionalidadeService.getFuncionalidade(manager, (Class<? extends GenericServiceIGN>) method.getDeclaringClass()));
			acao.setLogarAcao("S");
			acao.setSituacao("S");
			acao.setNomeMetodo(method.getName());
			acao.setDescricao(ignSegurancaAcao.nomeAcao().concat(" ").concat(acao.getNomeMetodo()));
			
			manager.persist(acao);
			
			return acao;
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
}