package br.com.desenv.nepalign.service;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.EmpresaFinanceiro;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.FormaPagamento;
import br.com.desenv.nepalign.model.TaxaPagamento;
import br.com.desenv.nepalign.persistence.TaxaPagamentoPersistence;

public class TaxaPagamentoService extends GenericServiceIGN<TaxaPagamento, TaxaPagamentoPersistence>
{
	public TaxaPagamentoService() 
	{
		super();
	}
	
	public TaxaPagamento buscarTaxaPorFormaDePagamento(EmpresaFisica empresa,FormaPagamento forma, Integer vezes) throws Exception
	{
		return new TaxaPagamentoPersistence().buscarTaxaPorFormaDePagamento(empresa,forma, vezes);
	}
	
	public TaxaPagamento buscaTaxaPagamentoPorEmpresaFinanceiro(EmpresaFinanceiro empresaFinanceiro) throws Exception
	{
		//return new TaxaPagamentoPersistence().buscarTaxaPorFormaDePagamento(empresa,forma, vezes);
		return null;
	}
}