package br.com.desenv.nepalign.service.cockpit;

import java.util.Date;

public class CockPitParametros 
{
	private String listaEmpresasFisicas;
	private Date dataInicial;
	private Date dataFinal;

	public String getListaEmpresasFisicas() {
		return listaEmpresasFisicas;
	}
	
	public void setListaEmpresasFisicas(String listaEmpresasFisicas) {
		this.listaEmpresasFisicas = listaEmpresasFisicas;
	}
	
	public Date getDataInicial() {
		return dataInicial;
	}
	
	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}
	
	public Date getDataFinal() {
		return dataFinal;
	}
	
	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}
}
