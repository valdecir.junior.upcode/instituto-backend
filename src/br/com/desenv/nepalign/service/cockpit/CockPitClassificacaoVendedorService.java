package br.com.desenv.nepalign.service.cockpit;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class CockPitClassificacaoVendedorService
{
	public static void main(String[] args) throws Exception
	{
		CockPitParametrosClassificacaoVendedor params = new CockPitParametrosClassificacaoVendedor();
		params.setDataInicial(new SimpleDateFormat("dd/MM/yyyy").parse("01/02/2015"));
		params.setDataFinal(new SimpleDateFormat("dd/MM/yyyy").parse("01/03/2015"));

		new CockPitClassificacaoVendedorService().gerar(params);
	}
	
	public List<CockPitRetornoClassificacaoVendedor> gerar(CockPitParametrosClassificacaoVendedor parametros) throws Exception 
	{ 
		Connection con = ConexaoUtil.getConexaoPadrao();    
       	Statement stm = con.createStatement();
       	SimpleDateFormat formatadorDataSql = new SimpleDateFormat("yyyy-MM-dd");
       
        int idCancelado = DsvConstante.getParametrosSistema().getInt("idSituacaoPedidoVendaCancelado");
        String idMovimentacaoEstoqueTransferencia = ""+DsvConstante.getParametrosSistema().getInt("idTipoMovimentacaoEstoqueTransferenciaEntrada")+","+DsvConstante.getParametrosSistema().getInt("idTipoMovimentacaoEstoqueTransferenciaSaida");
             

  	    String str = "";
	  	str = str + " SELECT abs(sum(ip.quantidade-if(ip.entradaSaida='E',(ip.quantidade*2 ),0))) as quantidade "; 
	  	str = str + " ,abs(sum(if(ip.entradasaida ='S',ip.custoProduto * ip.quantidade,((ip.custoProduto * ip.quantidade))*(-1)))) as custo "; 
	  	str = str + " ,(sum(if(ip.entradasaida ='S',ip.precoVenda * ip.quantidade,(ip.precoVenda * ip.quantidade)*(-1)))/sum(if(ip.entradasaida ='S',ip.custoProduto * ip.quantidade,((ip.custoProduto * ip.quantidade))*(-1)))* 100)-100 as Margem "; 
	  	str = str + " ,(sum(if(ip.entradasaida ='S',ip.precoVenda * ip.quantidade,(ip.precoVenda * ip.quantidade)*(-1)))) as venda,ven.nome,ven.idVendedor,concat(ven.codigovendedorcache,' - ',ven.nome) as codigovendedorcache, ven.codigovendedorcache as codigoVendedor, "; 
	  	str = str + " empresa.idEmpresaFisica, empresa.razaoSocial "; 
	  	str = str + "  FROM itempedidovenda ip "; 
	  	str = str + " inner join pedidovenda pv on pv.idPedidoVenda = ip.idPedidoVenda "; 
	  	str = str + " left outer join vendedor ven on ven.idVendedor = pv.idVendedor "; 
	  	str = str + " inner join empresaFisica empresa on pv.idEmpresaFisica = empresa.idEmpresaFisica ";
	  	str = str + " WHERE dataVenda between '" + formatadorDataSql.format(parametros.getDataInicial()) + " 00:00:00' AND '" + formatadorDataSql.format(parametros.getDataFinal()) + " 23:59:59' ";
	  	str = str + " and if( pv.idsituacaoPedidoVenda is null,true,pv.idsituacaoPedidoVenda not in(  "+idCancelado+"))";
		str = str + " and pv.idTipoMovimentacaoEstoque not in("+idMovimentacaoEstoqueTransferencia+")";
		str = str + " and pv.valorTotalPedido > 0 ";

		if(parametros.getListaEmpresasFisicas() != null)
			str = str + " and pv.idEmpresaFisica in (" + parametros.getListaEmpresasFisicas().replace(";", ",") + ")";

	  	str = str + " group by empresa.idEmpresaFisica, ven.idVendedor ";
	  	str = str + " order by empresa.idEmpresaFisica, abs(sum(if(ip.entradasaida ='S',ip.precoVenda * ip.quantidade,(ip.precoVenda * ip.quantidade)*(-1)))) desc ";
	  	
	  	ResultSet rs = stm.executeQuery(str);
	  	List<CockPitRetornoClassificacaoVendedor> listaRetorno = new ArrayList<CockPitRetornoClassificacaoVendedor>();

	  	double valorAcima = 0.0;
	  	double valor=0.00;
	  	
	  	while (rs.next())
	  	{
	  		valor = rs.getDouble("venda");

	  		CockPitRetornoClassificacaoVendedor retorno = new CockPitRetornoClassificacaoVendedor();
	  		retorno.setIdVendedor(rs.getInt("ven.idVendedor"));
	  		retorno.setCodigoVendedor(rs.getInt("codigoVendedor"));
	  		retorno.setColocacao(listaRetorno.size() + 1);
	  		
	  		if(valorAcima == 0.0)
	  			retorno.setDiferencaAcima(0.0);
	  		else
	  			retorno.setDiferencaAcima(IgnUtil.roundUpDecimalPlaces(valorAcima - valor, 2));
	  		
	  		retorno.setValorVenda(valor);
	  		retorno.setNomeVendedor(rs.getString("ven.nome"));
	  		listaRetorno.add(retorno);
	  		
	  		valorAcima = valor;
	  	}
	  	
	  	for(int i = listaRetorno.size() - 1; i > 0; i--)
	  	{
	  		CockPitRetornoClassificacaoVendedor retorno = listaRetorno.get(i);
	  		CockPitRetornoClassificacaoVendedor retornoAcima = listaRetorno.get(i - 1);
	  		
	  		retornoAcima.setDiferencaAbaixo(IgnUtil.roundUpDecimalPlaces(retornoAcima.getValorVenda() - retorno.getValorVenda(),2));
	  	}
	  	
	  	if(parametros.getIdVendedor() != null && parametros.getIdVendedor() > 0)
	  	{
	  		CockPitRetornoClassificacaoVendedor retornoVendedor = null;
	  		
	  		for(CockPitRetornoClassificacaoVendedor retorno : listaRetorno)
	  		{
	  			if(retorno.getIdVendedor().intValue() == parametros.getIdVendedor().intValue())
	  				retornoVendedor = retorno;
	  		}
	  		
	  		listaRetorno.clear();
	  		listaRetorno.add(retornoVendedor);
	  	}

	  	return listaRetorno;
	}
}
