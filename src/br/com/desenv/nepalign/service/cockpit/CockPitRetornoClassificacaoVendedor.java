package br.com.desenv.nepalign.service.cockpit;

import java.io.Serializable;

public class CockPitRetornoClassificacaoVendedor implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7470704765580804895L;

	public CockPitRetornoClassificacaoVendedor()
	{
		
	}
	
	private Integer idVendedor;
	private Integer codigoVendedor;
	private String nomeVendedor;
	private Double valorVenda;
	private Double diferencaAcima;
	private Double diferencaAbaixo;
	private Integer colocacao;
	
	public Integer getIdVendedor() {
		return idVendedor;
	}
	public void setIdVendedor(Integer idVendedor) {
		this.idVendedor = idVendedor;
	}
	public Integer getCodigoVendedor() {
		return codigoVendedor;
	}
	public void setCodigoVendedor(Integer codigoVendedor) {
		this.codigoVendedor = codigoVendedor;
	}
	public String getNomeVendedor() {
		return nomeVendedor;
	}
	public void setNomeVendedor(String nomeVendedor) {
		this.nomeVendedor = nomeVendedor;
	}
	public Double getValorVenda() {
		return valorVenda;
	}
	public void setValorVenda(Double valorVenda) {
		this.valorVenda = valorVenda;
	}
	public Double getDiferencaAcima() {
		return diferencaAcima;
	}
	public void setDiferencaAcima(Double diferencaAcima) {
		this.diferencaAcima = diferencaAcima;
	}
	public Double getDiferencaAbaixo() {
		return diferencaAbaixo;
	}
	public void setDiferencaAbaixo(Double diferencaAbaixo) {
		this.diferencaAbaixo = diferencaAbaixo;
	}
	public Integer getColocacao() {
		return colocacao;
	}
	public void setColocacao(Integer colocacao) {
		this.colocacao = colocacao;
	}
}