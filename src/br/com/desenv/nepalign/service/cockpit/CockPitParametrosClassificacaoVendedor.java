package br.com.desenv.nepalign.service.cockpit;

public class CockPitParametrosClassificacaoVendedor extends CockPitParametros 
{
	private Integer idVendedor;
	
	public CockPitParametrosClassificacaoVendedor()
	{
		super();
	}
	
	public Integer getIdVendedor() {
		return idVendedor;
	}
	public void setIdVendedor(Integer idVendedor) {
		this.idVendedor = idVendedor;
	}
}
