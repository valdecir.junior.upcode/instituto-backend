package br.com.desenv.nepalign.service;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.mail.Address;
import javax.mail.internet.InternetAddress;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import java.util.logging.Logger;
//import sun.org.mozilla.javascript.internal.ast.ForInLoop;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.alertas.EnviarEmail;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.LogDuplicata;
import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.nepalign.persistence.LogDuplicataPersistence;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class LogDuplicataService extends GenericServiceIGN<LogDuplicata, LogDuplicataPersistence>
{

	public LogDuplicataService() 
	{
		super();
	}
	public void gerarLog(Object objEqual,EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Object obj = null;
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			Integer idDocumentoOrigem = null;
			{
				Class classe = Class.forName("br.com.desenv.nepalign.service."+objEqual.getClass().getSimpleName() + "Service");
				Constructor objectConstructor = null;
				objectConstructor = classe.getConstructor();
				Object object = objectConstructor.newInstance();

				Field id = objEqual.getClass().getDeclaredField("id");
				id.setAccessible(true);

				obj = (object.getClass().getMethod("recuperarPorId",EntityManager.class, Integer.class).invoke(object, manager, Integer.parseInt( id.get(objEqual).toString())));   
				idDocumentoOrigem= Integer.parseInt( id.get(objEqual).toString());
				id.setAccessible(false);
			} 

			try
			{
				Integer idCliente = null;
				for (Field field : obj.getClass().getDeclaredFields())
				{
					field.setAccessible(true);
					if(field.getModifiers() != 26 && field.get(obj) != null)
					{
						Object value1 = field.get(obj);
						if(value1.getClass().getSimpleName().equals("Cliente"))
						{
							Field foreignKeyField  = field.get(obj).getClass().getDeclaredField("id");
							foreignKeyField.setAccessible(true);

							idCliente = (Integer.parseInt((foreignKeyField.get(field.get(obj)).toString())));
						}
					}
				}
				for (Field field : obj.getClass().getDeclaredFields())
				{
					field.setAccessible(true);
					LogDuplicata log = new LogDuplicata();
					log.setIdDocumentoOrigem(""+idDocumentoOrigem);
					if(idCliente!=null)
					{
						log.setIdCliente(idCliente);
					}
					if(field.getModifiers() != 26 && field.get(obj) != null)
					{

						Object value1 = field.get(obj);
						Object value2 = field.get(objEqual);

						if(value1.getClass().equals(Date.class) || value1.getClass().equals(java.sql.Timestamp.class))
						{
							value1 = value1==null?null:((Date) value1).getTime();
							value2 = value2==null?null:((Date) value2).getTime();
						}

						if(!value1.equals(value2))
						{
							log.setData(new Date());

							try
							{
								field.get(obj).getClass().getDeclaredField("id");
								Field foreignKeyField = null;
								if(field.get(objEqual)!=null)
								{

									foreignKeyField = field.get(objEqual).getClass().getDeclaredField("id");
									String descricao ="";
									for (Field f : field.get(objEqual).getClass().getDeclaredFields())
									{
										f.setAccessible(true);
										if(f.getModifiers() != 26 && f.get(field.get(objEqual)) != null)
										{
											f.getName();
											Object v1 =  f.get(field.get(objEqual));
											if(f.getName().equals("descricao"))
											{
												Field fk  = field.get(objEqual).getClass().getDeclaredField("descricao");
												fk.setAccessible(true);

												descricao = fk.get(field.get(objEqual)).toString();
											}
										}
									}
									foreignKeyField.setAccessible(true);
									try
									{
										if (foreignKeyField.get(field.get(objEqual))!=null){
											log.setValorAlterado(foreignKeyField.get(field.get(objEqual)).toString()+"-"+descricao);
											foreignKeyField.setAccessible(false);
										}
									}
									catch(Exception ex)
									{
										System.out.println("Log não pode parar a aplicação! ");
										ex.printStackTrace();
									}
								}
								else
								{
									log.setValorAlterado(""); 
								}




								foreignKeyField = field.get(obj).getClass().getDeclaredField("id");
								foreignKeyField.setAccessible(true);
								String descricao ="";
								for (Field f : field.get(obj).getClass().getDeclaredFields())
								{
									f.setAccessible(true);
									if(f.getModifiers() != 26 && f.get(field.get(obj)) != null)
									{
										f.getName();
										Object v1 =  f.get(field.get(obj));
										if(f.getName().equals("descricao"))
										{
											Field fk  = field.get(objEqual).getClass().getDeclaredField("descricao");
											fk.setAccessible(true);

											descricao = fk.get(field.get(obj)).toString();
										}
									}
								}
								try
								{
									if (foreignKeyField.get(field.get(obj)) !=null){
										log.setValorOriginal(foreignKeyField.get(field.get(obj)).toString()+"-"+descricao);
									}
								}
								catch(Exception ex)
								{
									System.out.println(">>>>>>>>>>> Log com problemas na geração.");
									ex.printStackTrace();
								}

								foreignKeyField.setAccessible(false);

								foreignKeyField = null;
							}
							catch(NoSuchFieldException ex)
							{
								if(field.get(obj).getClass().equals(Date.class) || field.get(obj).getClass().equals(java.sql.Timestamp.class))
								{
									Calendar conversaoData = Calendar.getInstance();
									if(value1!=null)
									{

										conversaoData.setTimeInMillis((long) value1);
										conversaoData.set(Calendar.HOUR, 0);

										value1 = conversaoData.getTime();	
										log.setValorOriginal(new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format((Date) value1));
									}
									if(value2!=null)
									{
										conversaoData.setTimeInMillis((long) value2);
										conversaoData.set(Calendar.HOUR, 0);

										value2 = conversaoData.getTime();

										log.setValorAlterado(new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format((Date) value2)); 
									}



								}
								else
								{
									Double valor2 = null;
									Double valor1 = null;
									if(value2.getClass().getSimpleName().equals("double")||value2.getClass().getSimpleName().equals("Double"))
									{
										valor2 = Double.parseDouble(value2.toString());
										valor2 =  new BigDecimal(valor2).setScale(2,BigDecimal.ROUND_HALF_EVEN).doubleValue();
									}
									if(value1.getClass().getSimpleName().equals("double")||value1.getClass().getSimpleName().equals("Double"))
									{
										valor1 = Double.parseDouble(value1.toString());
										valor1 =  new BigDecimal(valor1).setScale(2,BigDecimal.ROUND_HALF_EVEN).doubleValue();
									}
									log.setValorAlterado(valor2==null?value2.toString():valor2.toString());
									log.setValorOriginal(valor1==null?value1.toString():valor1.toString()); 
								}						 
							}
							Usuario usuarioLogado = new UsuarioService().recuperarSessaoUsuarioLogado();

							if(usuarioLogado != null)
							{
								log.setNomeUsuario ("" + usuarioLogado.getNome());
								log.setIdEmpresaFisica(new EmpresaFisicaService().recuperarEmpresaFisicaPorEmpresa(usuarioLogado.getEmpresaPadrao()).getId());
							}
							else
							{
								log.setNomeUsuario("[DESCONHECIDO] ");
							}

							log.setNomeClasse(IgnUtil.separeEveryUpperCase(obj.getClass().getSimpleName()));

							try
							{
								log.setMotivo(""+Thread.currentThread().getStackTrace()[3].getMethodName()+" o campo "+IgnUtil.separeEveryUpperCase(field.getName()));	 
							}
							catch(Exception ex)
							{

							}

							log.setData(new Date());
							this.salvar(manager, transaction, log);
						}
					}
				}
			}


			catch(Exception ex)
			{
				ex.printStackTrace();
				throw ex;
			}

			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}

	public void gerarLogExcluir(Object obj,EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}

			LogDuplicata log = new LogDuplicata();
			log.setNomeClasse(IgnUtil.separeEveryUpperCase(obj.getClass().getSimpleName()));

			Usuario usuarioLogado = new UsuarioService().recuperarSessaoUsuarioLogado();

			if(usuarioLogado != null)
			{
				log.setNomeUsuario ("" + usuarioLogado.getNome());
				try
				{
					log.setIdEmpresaFisica(new EmpresaFisicaService().recuperarEmpresaFisicaPorEmpresa(usuarioLogado.getEmpresaPadrao()).getId());   
				}
				catch(Exception ex)
				{

				}
			}
			else
			{
				log.setNomeUsuario("[DESCONHECIDO] ");
			}

			Field fieldid = obj.getClass().getDeclaredField("id");
			fieldid.setAccessible(true);
			String id = fieldid.get(obj).toString();
			log.setIdDocumentoOrigem(id);

			if(usuarioLogado != null)
				log.setNomeUsuario ("" + usuarioLogado.getNome());
			else
				log.setNomeUsuario("[DESCONHECIDO] ");

			log.setData(new Date());

			if(obj.getClass().getSimpleName().equals("DuplicataParcela"))
			{
				log.setMotivo("Excluiu Parcela ");
				Field fieldNum = obj.getClass().getDeclaredField("numeroDuplicata");
				fieldNum.setAccessible(true);
				String num = fieldNum.get(obj).toString();
				log.setMotivo(log.getMotivo()+". Duplicata: "+num);

				Field fieldNumPar = obj.getClass().getDeclaredField("numeroParcela");
				fieldNumPar.setAccessible(true);
				String numPar = fieldNumPar.get(obj).toString();
				log.setMotivo(log.getMotivo()+". Parcela: "+numPar);

				Field fieldcli = obj.getClass().getDeclaredField("cliente");
				fieldcli.setAccessible(true);

				Field FieldCliId = fieldcli.get(obj).getClass().getDeclaredField("id");
				FieldCliId.setAccessible(true);
				Integer idCliente = Integer.parseInt(FieldCliId.get(fieldcli.get(obj)).toString());
				log.setIdCliente(idCliente);

				Field FieldCliNome = fieldcli.get(obj).getClass().getDeclaredField("nome");
				FieldCliNome.setAccessible(true);
				String nome = FieldCliNome.get(fieldcli.get(obj)).toString();		
				log.setMotivo(log.getMotivo()+". Cliente: "+nome); 

				Field fieldValor = obj.getClass().getDeclaredField("valorParcela");
				fieldValor.setAccessible(true);
				String valor = fieldValor.get(obj).toString();
				log.setMotivo(log.getMotivo()+". Valor: "+valor);
				this.salvar(manager, transaction, log);

			}
			else if(obj.getClass().getSimpleName().equals("Duplicata"))
			{
				log.setMotivo("Excluiu Duplicata");
				Field fieldNum = obj.getClass().getDeclaredField("numeroDuplicata");
				fieldNum.setAccessible(true);
				String num = fieldNum.get(obj).toString();
				log.setMotivo(log.getMotivo()+". Duplicata: "+num);

				Field fieldcli = obj.getClass().getDeclaredField("cliente");
				fieldcli.setAccessible(true);

				Field FieldCliId = fieldcli.get(obj).getClass().getDeclaredField("id");
				FieldCliId.setAccessible(true);
				Integer idCliente = Integer.parseInt(FieldCliId.get(fieldcli.get(obj)).toString());
				log.setIdCliente(idCliente);

				Field FieldCliNome = fieldcli.get(obj).getClass().getDeclaredField("nome");
				FieldCliNome.setAccessible(true);
				String nome = FieldCliNome.get(fieldcli.get(obj)).toString();		
				log.setMotivo(log.getMotivo()+". Cliente: "+nome); 

				Field fieldValor = obj.getClass().getDeclaredField("valorTotalCompra");
				fieldValor.setAccessible(true);
				String valor = fieldValor.get(obj).toString();
				log.setMotivo(log.getMotivo()+". Valor: "+valor);
				this.salvar(manager, transaction, log);
			}
			else  if(obj.getClass().getSimpleName().equals("MovimentoParcelaDuplicata"))
			{
				log.setMotivo("Excluiu pagamento parcela:");
				Field fieldPar = obj.getClass().getDeclaredField("duplicataParcela");
				fieldPar.setAccessible(true);

				Field foreignKeyFieldCli = fieldPar.get(obj).getClass().getDeclaredField("cliente");
				foreignKeyFieldCli.setAccessible(true);

				Field FieldCliId = foreignKeyFieldCli.get(fieldPar.get(obj)).getClass().getDeclaredField("id");
				FieldCliId.setAccessible(true);
				Integer idCliente = Integer.parseInt(FieldCliId.get(foreignKeyFieldCli.get(fieldPar.get(obj))).toString());
				log.setIdCliente(idCliente);

				Field FieldCliNome = foreignKeyFieldCli.get(fieldPar.get(obj)).getClass().getDeclaredField("nome");
				FieldCliNome.setAccessible(true);
				String nome = FieldCliNome.get(foreignKeyFieldCli.get(fieldPar.get(obj))).toString();

				Field fieldNum = fieldPar.get(obj).getClass().getDeclaredField("numeroDuplicata");
				fieldNum.setAccessible(true);
				String numDuplicata = fieldNum.get(fieldPar.get(obj)).toString();

				Field fieldNumPar = fieldPar.get(obj).getClass().getDeclaredField("numeroParcela");
				fieldNumPar.setAccessible(true);
				String numParcela = fieldNumPar.get(fieldPar.get(obj)).toString();

				Field fieldValor = obj.getClass().getDeclaredField("valorRecebido");
				fieldValor.setAccessible(true);
				String valor = fieldValor.get(obj).toString();

				log.setMotivo(log.getMotivo()+" "+numParcela);
				log.setMotivo(log.getMotivo()+". Duplicata: "+numDuplicata);
				log.setMotivo(log.getMotivo()+". Cliente: "+nome);
				log.setMotivo(log.getMotivo()+". Valor: "+valor);
				this.salvar(manager, transaction, log);
			}
			else  if(obj.getClass().getSimpleName().equals("PedidoVenda"))
			{
				log.setMotivo("Excluiu Pedido Venda:"); 
				Field fieldNum = obj.getClass().getDeclaredField("numeroControle");
				fieldNum.setAccessible(true);
				String num = fieldNum.get(obj).toString();
				log.setMotivo(log.getMotivo()+". Controle: "+num);

				Field fieldcli = obj.getClass().getDeclaredField("vendedor");
				fieldcli.setAccessible(true);
				Field FieldCliNome = fieldcli.get(obj).getClass().getDeclaredField("nome");
				FieldCliNome.setAccessible(true);
				String nome = FieldCliNome.get(fieldcli.get(obj)).toString();		
				log.setMotivo(log.getMotivo()+". Vendedor: "+nome); 

				Field fieldValor = obj.getClass().getDeclaredField("valorTotalPedido");
				fieldValor.setAccessible(true);
				String valor = fieldValor.get(obj).toString();
				log.setMotivo(log.getMotivo()+". Valor: "+valor);
				this.salvar(manager, transaction, log);
			}
			else if(obj.getClass().getSimpleName().equals("MovimentoCaixadia")
					&&Thread.currentThread().getStackTrace()[6].getMethodName().equals("excluirMovimentacaoDiversa"))
			{
				log.setMotivo("Excluiu movimentacao diversas. Caixa:");  

				Field fieldcli = obj.getClass().getDeclaredField("cliente");
				fieldcli.setAccessible(true);
				String nomeCli = "";
				if(fieldcli.get(obj)!=null)
				{

					Field FieldCliId = fieldcli.get(obj).getClass().getDeclaredField("id");
					FieldCliId.setAccessible(true);
					if(FieldCliId.get(fieldcli.get(obj))!=null)
					{
						Integer idCliente = Integer.parseInt(FieldCliId.get(fieldcli.get(obj)).toString());
						log.setIdCliente(idCliente);
					}
					Field FieldCliNome = fieldcli.get(obj).getClass().getDeclaredField("nome");
					FieldCliNome.setAccessible(true);

					if(FieldCliNome.get(fieldcli.get(obj))!=null)
					{
						nomeCli = FieldCliNome.get(fieldcli.get(obj)).toString();	 
					}
				}

				Field fieldfor = obj.getClass().getDeclaredField("fornecedor");
				fieldfor.setAccessible(true);
				String nomeFor = "";
				if(fieldfor.get(obj)!=null)
				{
					Field FieldforNome = fieldfor.get(obj).getClass().getDeclaredField("razaoSocial");
					FieldforNome.setAccessible(true);

					if(FieldforNome.get(fieldfor.get(obj))!=null)
					{
						nomeFor = FieldforNome.get(fieldfor.get(obj)).toString();	 
					}
				}

				Field fieldTipo = obj.getClass().getDeclaredField("tipoMovimentoCaixaDia");
				fieldTipo.setAccessible(true);
				Field FieldDesTipo = fieldTipo.get(obj).getClass().getDeclaredField("descricao");
				FieldDesTipo.setAccessible(true);
				String descricaoTipo = FieldDesTipo.get(fieldTipo.get(obj)).toString();

				Field fieldValor = obj.getClass().getDeclaredField("valor");
				fieldValor.setAccessible(true);
				String valor = fieldValor.get(obj).toString();

				if(!nomeCli.equals(""))
				{
					log.setMotivo(log.getMotivo()+". Cliente: "+nomeCli);
				}
				if(!nomeFor.equals(""))
				{
					log.setMotivo(log.getMotivo()+". Fornecedor: "+nomeFor);
				}
				log.setMotivo(log.getMotivo()+". De "+descricaoTipo);	
				log.setMotivo(log.getMotivo()+". Valor: "+valor);
				this.salvar(manager, transaction, log);
			}


			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	public void enviarEmailLog(List<LogDuplicata> lista) throws Exception
	{
		EmpresaFisica empresa = new EmpresaFisicaService().recuperarEmpresaFisicaPorEmpresa(new UsuarioService().recuperarSessaoUsuarioLogado().getEmpresaPadrao());
		
		new Thread(new RunnableEmailLog(lista,empresa)).start();
	}
}

