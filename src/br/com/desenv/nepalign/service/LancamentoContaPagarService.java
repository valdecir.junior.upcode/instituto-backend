package br.com.desenv.nepalign.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.log4j.Logger;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.BaixaLancamentoContaPagar;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.LancamentoContaPagar;
import br.com.desenv.nepalign.model.LancamentoContaPagarRateio;
import br.com.desenv.nepalign.model.LogOperacao;
import br.com.desenv.nepalign.persistence.LancamentoContaPagarPersistence;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class LancamentoContaPagarService extends GenericServiceIGN<LancamentoContaPagar, LancamentoContaPagarPersistence>
{
	private static final Logger logger = Logger.getLogger("CPAG");
	
	private static final IgnUtil ignUtil = new IgnUtil();
	
	private static final String PARAMETRO_USUARIO_AUTORIZACAO_RELATORIO_CONTASAPAGAR = "USUARIO_AUTORIZADO_RELATORIO_CONTASPAGAR";
	private static final String PARAMETRO_USUARIO_MANUTENCAO_CONTASAPAGAR = "USUARIOS_AUTORIZACAO_MANUTENCAOCONTASAPAGAR";
	
	private static final UsuarioService usuarioService = new UsuarioService();
	private static final TipoContaService tipoContaService = new TipoContaService();
	private static final ContaCorrenteService contaCorrenteService = new ContaCorrenteService();
	private static final EmpresaFisicaService empresaFisicaService = new EmpresaFisicaService();
	private static final ContaGerencialService contaGerencialService = new ContaGerencialService();
	private static final FormaPagamentoService formaPagamentoService = new FormaPagamentoService();
	private static final CentroFinanceiroService centroFinanceiroService = new CentroFinanceiroService();
	private static final SituacaoLancamentoService situacaoLancamentoService = new SituacaoLancamentoService();
	private static final BaixaLancamentoContaPagarService baixaLancamentoContaPagarService = new BaixaLancamentoContaPagarService();
	private static final LancamentoContaPagarRateioService lancamentoContaPagarRateioService = new LancamentoContaPagarRateioService();
	
	private static final LogOperacaoService logOperacaoService = new LogOperacaoService();

	public LancamentoContaPagarService() 
	{
		super();
		
		if(logOperacaoService.gerarObservacaoMethod == null)
		{
			try 
			{
				logOperacaoService.gerarObservacaoMethod = getClass().getMethod("gerarObservacaoLog", LancamentoContaPagar.class);
			}
			catch (NoSuchMethodException | SecurityException e) 
			{
				logger.error("Não foi possível definir o método de observação do LogOperacao", e);
			}
		}
	}
	
	public br.com.desenv.nepalign.model.LancamentoContaPagar salvar(br.com.desenv.nepalign.model.LancamentoContaPagar entidade, Map<Integer, BigDecimal> listaEmpresaFisicaSelecionada) throws Exception
	{
		return this.salvar(entidade, listaEmpresaFisicaSelecionada, 0);
	}
	
	public br.com.desenv.nepalign.model.LancamentoContaPagar salvar(br.com.desenv.nepalign.model.LancamentoContaPagar entidade, Map<Integer, BigDecimal> listaEmpresaFisicaSelecionada, Integer numReplicacao) throws Exception
	{
		return this.salvar(entidade, listaEmpresaFisicaSelecionada, numReplicacao, "");
	}
	
	public br.com.desenv.nepalign.model.LancamentoContaPagar salvar(br.com.desenv.nepalign.model.LancamentoContaPagar entidade, Map<Integer, BigDecimal> listaEmpresaFisicaSelecionada, Integer numReplicacao, String parcelas) throws Exception
	{
		return this.salvar(entidade, listaEmpresaFisicaSelecionada, numReplicacao, parcelas, false);
	}

	@SuppressWarnings("unchecked")
	public br.com.desenv.nepalign.model.LancamentoContaPagar salvar(br.com.desenv.nepalign.model.LancamentoContaPagar entidade, Object listaEmpresaFisicaSelecionada, Integer numReplicacao, String parcelas, Boolean baixarAutomaticamente) throws Exception
	{
		Map<Integer, BigDecimal> listaRateio = null;
		
		try
		{
			if(listaEmpresaFisicaSelecionada instanceof Map)
				listaRateio = (Map<Integer, BigDecimal>) listaEmpresaFisicaSelecionada;
		}
		catch(ClassCastException cce)
		{
			listaRateio = null;
		}
		
		return this.salvar(entidade, listaRateio, numReplicacao, parcelas, baixarAutomaticamente, null);
	}

	public br.com.desenv.nepalign.model.LancamentoContaPagar salvar(br.com.desenv.nepalign.model.LancamentoContaPagar entidade, Map<Integer, BigDecimal> listaEmpresaFisicaSelecionada, Integer numReplicacao, String parcelas, Boolean baixarAutomaticamente, EntityManager manager) throws Exception
	{
		boolean transacaoIndependente = false;
		
		try
		{
			if((transacaoIndependente = (manager == null)))
			{
				manager = ConexaoUtil.getEntityManager();
				manager.getTransaction().begin();
			}
			
			if(entidade.getNumeroDocumento() == null || entidade.getNumeroDocumento().trim().equals(""))
				throw new Exception("Informe um número de documento válido");
			if(entidade.getEmpresaFisica() == null)
				throw new Exception("Informe uma empresa válida");
			if(entidade.getCentroFinanceiro() == null)
				throw new Exception("Informe um Centro Financeiro válido");
			if(entidade.getTipoConta() == null)
				throw new Exception("Informe um Tipo Conta válido");
			if(entidade.getValorOriginal() == null || entidade.getValorOriginal() <= 0.0)
				throw new Exception("Informe um Valor válido");
			if(entidade.getFornecedor() == null)
				throw new Exception("Informe um Fornecedor válido");
			if(entidade.getContaGerencial() == null)
				throw new Exception("Informe uma Conta Gerencial válida");
			if(listaEmpresaFisicaSelecionada != null)
			{
				BigDecimal porcentagem = new BigDecimal(0x00);
				
				for(final Map.Entry<Integer, BigDecimal> empresaRateio : listaEmpresaFisicaSelecionada.entrySet())
					porcentagem = porcentagem.add(new BigDecimal(String.valueOf(empresaRateio.getValue())));
				
				if(porcentagem.compareTo(new BigDecimal(100)) != 0x00)
					throw new Exception("Soma das porcentagems do Rateio tem que ser igual a 100!");
			}

			if(!verificarPermissaoManutencaoContasAPagar())
			{
				final Calendar dataVencimentoLancamento = Calendar.getInstance();
				dataVencimentoLancamento.set(Calendar.MONTH, Integer.parseInt(IgnUtil.dateFormat.format(entidade.getDataVencimento()).split("/")[0x00000001]));
				dataVencimentoLancamento.set(Calendar.YEAR, Integer.parseInt(IgnUtil.dateFormat.format(entidade.getDataVencimento()).split("/")[0x00000002]));
				dataVencimentoLancamento.set(Calendar.DAY_OF_MONTH, dataVencimentoLancamento.getActualMaximum(Calendar.DAY_OF_MONTH));
				
				final Calendar mesAnoAbertoEmpresa = Calendar.getInstance();
				mesAnoAbertoEmpresa.set(Calendar.MONTH, entidade.getEmpresaFisica().getMesAbertoFin());
				mesAnoAbertoEmpresa.set(Calendar.YEAR, entidade.getEmpresaFisica().getAnoAbertoFin());
				mesAnoAbertoEmpresa.set(Calendar.DAY_OF_MONTH, mesAnoAbertoEmpresa.getActualMinimum(Calendar.DAY_OF_MONTH));
				
				if(dataVencimentoLancamento.before(mesAnoAbertoEmpresa))
					throw new Exception("Não é possível incluir um lançamento com uma data anterior ao mês/ano aberto da Empresa.");
			}
			
			if(entidade.getContaGerencial().getCodigoContaNivel3() == null || entidade.getContaGerencial().getCodigoContaNivel3().intValue() == 0x00000000)
				throw new Exception("Lançamentos devem ser feitos com uma Conta Gerencia de nível 3 ou superior!");	
			
			final br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagarPesquisaNumeroDocumento = new LancamentoContaPagar();
			lancamentoContaPagarPesquisaNumeroDocumento.setNumeroDocumento(entidade.getNumeroDocumento());
			lancamentoContaPagarPesquisaNumeroDocumento.setEmpresaFisica(entidade.getEmpresaFisica());
			lancamentoContaPagarPesquisaNumeroDocumento.setFornecedor(entidade.getFornecedor());

			if(listarPorObjetoFiltro(manager, lancamentoContaPagarPesquisaNumeroDocumento, null, null, null).size() > 0x00000000)
				throw new Exception("Já existe um lançamento com esse número de documento");
			
			if(baixarAutomaticamente)
			{
				if(entidade.getContaCorrente() == null)
					throw new Exception("Para realizar a baixa automática a CONTA BANCO/CAIXA deve ser informada.");
				if(entidade.getFormaPagamento() == null)
					throw new Exception("Para realizar a baixa automática a FORMA DE PAGAMENTO deve ser informada.");
			}
			
			entidade.setId(null);
			entidade.setValorPago(null);
			entidade.setDataQuitacao(null);
			entidade.setDataLancamento(new Date());
			
			final List<LancamentoContaPagar> listaLancamentoContaPagarPersistidos = new ArrayList<LancamentoContaPagar>();
			
			if(parcelas == null || parcelas.equals(""))
			{
				// lançamento com réplicas
				if(numReplicacao != null && numReplicacao > 0x00000000)
				{
					entidade.setCodigoSeguranca("RN".concat(ignUtil.getRandomNumberToString()));
					
					final List<br.com.desenv.nepalign.model.LancamentoContaPagar> listaLancamentosReplicados = replicarLancamentoContaPagar(manager, entidade, numReplicacao);
					listaLancamentoContaPagarPersistidos.addAll(listaLancamentosReplicados);
					
					if(baixarAutomaticamente)
						for(final br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagar : listaLancamentosReplicados)
							baixaLancamentoContaPagarService.baixaLancamentoContaPagar(lancamentoContaPagar, baixarAutomaticamente, manager, manager.getTransaction());
				}
				else // lançamento único
				{
					manager.persist(entidade);
					logOperacaoService.gravarOperacaoInclusao(manager, manager.getTransaction(), entidade);
					listaLancamentoContaPagarPersistidos.add(entidade);
					
					if(baixarAutomaticamente)
						baixaLancamentoContaPagarService.baixaLancamentoContaPagar(entidade, baixarAutomaticamente, manager, manager.getTransaction());
				}
			}
			else if (parcelas != null && !parcelas.equals("")) // lançamento com parcelas
			{
				final String dias[] = parcelas.split("/");
				
				if(dias.length > 0x00000000)
				{
					entidade.setCodigoSeguranca("RP".concat(ignUtil.getRandomNumberToString()));
					entidade.setNumeroDocumento(entidade.getNumeroDocumento() + "-1/" + (dias.length + 1));
					
					manager.persist(entidade);
					listaLancamentoContaPagarPersistidos.add(entidade);
					
					for(int parcela = 0x00000000; parcela < dias.length; parcela++)
					{						
						if(!dias[parcela].equals(""))
						{
							final LancamentoContaPagar lancamentoContaPagarClone = entidade.clone();
							lancamentoContaPagarClone.setId(null);
							
							final Calendar c = GregorianCalendar.getInstance();
							c.setTime(entidade.getDataVencimento());
							c.add(Calendar.DAY_OF_MONTH, Integer.parseInt(dias[parcela]));
							
							lancamentoContaPagarClone.setDataVencimento(c.getTime());
							lancamentoContaPagarClone.setNumeroDocumento(lancamentoContaPagarClone.getNumeroDocumento() + "-" + (parcela + 0x00000002) + "/" + (dias.length + 0x00000001)); // NUMDOC-NUMPARCELA/TOTALPARCELA
							
							manager.persist(lancamentoContaPagarClone);
							logOperacaoService.gravarOperacaoInclusao(manager, manager.getTransaction(), lancamentoContaPagarClone);
							listaLancamentoContaPagarPersistidos.add(lancamentoContaPagarClone);
							
							if(baixarAutomaticamente)
								baixaLancamentoContaPagarService.baixaLancamentoContaPagar(lancamentoContaPagarClone, baixarAutomaticamente, manager, manager.getTransaction());
						}
						else
							throw new Exception("Não foi possível determinar o vencimento da condição ".concat(String.valueOf(parcela)));
					}
				}
				else
					throw new Exception("Não foi possível determinar as condições de pagamento!");
			}
			
			if(listaEmpresaFisicaSelecionada != null)
			{
				for(final LancamentoContaPagar lancamentoContaPagarPersistido : listaLancamentoContaPagarPersistidos)
				{
					final LancamentoContaPagarRateio lancamentoContaPagarRateio = new LancamentoContaPagarRateio();
					lancamentoContaPagarRateio.setLancamentoContaPagar(lancamentoContaPagarPersistido);
					
					for(final Map.Entry<Integer, BigDecimal> empresaRateio : listaEmpresaFisicaSelecionada.entrySet())
					{
						final LancamentoContaPagarRateio lancamentoContaPagarRateioEmpresa = lancamentoContaPagarRateio.clone();
						
						// Map.Entry teve que converter os valores para String e depois para o tipo definido.
						// Problema com o Casting ???
						lancamentoContaPagarRateioEmpresa.setEmpresaFisica(empresaFisicaService.recuperarPorId(manager, Integer.valueOf(String.valueOf(empresaRateio.getKey()))));
						lancamentoContaPagarRateioEmpresa.setPorcentagemRateio(new BigDecimal(String.valueOf(empresaRateio.getValue())));
						
						manager.persist(lancamentoContaPagarRateioEmpresa);
						
						final LogOperacao logOperacao = new LogOperacao();
						
						logOperacao.setEmpresaFisica(entidade.getEmpresaFisica());
						logOperacao.setCampo("RATEIO-".concat("F-".concat(String.valueOf(empresaRateio.getKey()))));
						logOperacao.setValorOriginal("?");
						logOperacao.setValorAlterado(String.valueOf(empresaRateio.getValue()));
						logOperacao.setData(new Date());
						logOperacao.setIdDocumentoOrigem(String.valueOf(entidade.getId()));
						logOperacao.setAgrupador("INCLUSAO_RATEIO_".concat(String.valueOf(entidade.getId()).concat(new Date().toString())));
						logOperacao.setNomeClasse(entidade.getClass().getSimpleName());
						logOperacao.setUsuario(usuarioService.recuperarSessaoUsuarioLogado());
						logOperacao.setObservacao("Numero Documento : ".concat(entidade.getNumeroDocumento()));
						logOperacao.setTipoLog(0x01);
						logOperacaoService.salvar(manager, manager.getTransaction(), logOperacao, false);
					}	
				}	
			}
			
			if(transacaoIndependente)
				manager.getTransaction().commit();
			
			return null;
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				manager.getTransaction().rollback();
			
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	
	public void excluir(List<br.com.desenv.nepalign.model.LancamentoContaPagar> listaEntidade) throws Exception
	{
		final EntityManager manager = ConexaoUtil.getEntityManager();
		manager.getTransaction().begin();
		
		try
		{
			for(final br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagar : listaEntidade)
				excluir(manager, manager.getTransaction(), lancamentoContaPagar);
			
			manager.getTransaction().commit();
		}
		catch(Exception ex)
		{
			manager.getTransaction().rollback();
		
			throw ex;
		}
		finally
		{
			manager.close();
		}
	}

	public void excluir(final br.com.desenv.nepalign.model.LancamentoContaPagar entidade) throws Exception
	{
		excluir(null, null, entidade);
	}
	
	public void excluir(EntityManager manager, EntityTransaction transaction, br.com.desenv.nepalign.model.LancamentoContaPagar entidade) throws Exception
	{
		boolean transacaoIndependente = false;
		try
		{
			if((transacaoIndependente = (manager == null)))
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
			}
			
			if(!verificarPermissaoManutencaoContasAPagar())
				if(!verificarPermissaoMesContasAPagar(entidade))
					throw new Exception("O Lançamento " + entidade.getNumeroDocumento() + " não pode ser apagado por que o vencimento é do mês anterior.");
			
			if(entidade.getCodigoSeguranca() != null)
			{
				br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagarNepalCriterio = new br.com.desenv.nepalign.model.LancamentoContaPagar();
				lancamentoContaPagarNepalCriterio.setCodigoSeguranca(entidade.getCodigoSeguranca().replace("%", ""));
				
				final List<br.com.desenv.nepalign.model.LancamentoContaPagar> listaLancamentoContaPagarNepal = listarPorObjetoFiltro(manager, lancamentoContaPagarNepalCriterio, null, null, null);
				
				for(final br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagarNepal : listaLancamentoContaPagarNepal)
				{
					if(baixaLancamentoContaPagarService.existePagamento(manager, lancamentoContaPagarNepal))
						throw new Exception("O Lançamento " + lancamentoContaPagarNepal.getNumeroDocumento() + " não pode ser apagado por que já está baixado! Exclua a baixa antes de continuar");
				
					lancamentoContaPagarRateioService.excluir(manager, lancamentoContaPagarNepal);
					logOperacaoService.gravarOperacaoExclusao(manager, transaction, lancamentoContaPagarNepal);
					super.excluir(manager, transaction, lancamentoContaPagarNepal, false);
				}		
			}
			else
			{
				if(baixaLancamentoContaPagarService.existePagamento(manager, entidade))
					throw new Exception("O Lançamento " + entidade.getNumeroDocumento() + " não pode ser apagado por que já está baixado! Exclua a baixa antes de continuar");
				
				lancamentoContaPagarRateioService.excluir(manager, entidade);
				logOperacaoService.gravarOperacaoExclusao(manager, transaction, entidade);
				super.excluir(manager, transaction, entidade, false);
			}
			
			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();
			
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	
	public br.com.desenv.nepalign.model.LancamentoContaPagar atualizar(EntityManager manager, EntityTransaction transaction, br.com.desenv.nepalign.model.LancamentoContaPagar entidade, Map<Integer, BigDecimal> listaEmpresaFisicaSelecionada) throws Exception
	{
		boolean transacaoIndependente = false;
		
		try
		{
			if((transacaoIndependente = (manager == null)))
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
			}
			
			if(entidade.getSituacaoLancamento().getId().equals(SituacaoLancamentoService.BAIXADO) && !verificarPermissaoManutencaoContasAPagar())
				if(!verificarPermissaoMesContasAPagar(entidade))
					throw new Exception("Não é possível alterar um lançamento com uma data anterior ao mês/ano aberto da Empresa.");
			
			if(listaEmpresaFisicaSelecionada != null)
			{
				BigDecimal porcentagem = new BigDecimal(0x00);
				
				for(final Map.Entry<Integer, BigDecimal> empresaRateio : listaEmpresaFisicaSelecionada.entrySet())
					porcentagem = porcentagem.add(new BigDecimal(String.valueOf(empresaRateio.getValue())));
				
				if(porcentagem.compareTo(new BigDecimal(100)) != 0x00)
					throw new Exception("Soma das porcentagems do Rateio tem que ser igual a 100!");
			}
			
			List<LancamentoContaPagarRateio> rateios = recuperarRateios(manager, entidade.getId());
			
			
			manager.createNativeQuery("SET UNIQUE_CHECKS=0;").executeUpdate();
			if(rateios != null && (entidade.getEmpresasRateio() == null || entidade.getEmpresasRateio().trim().equals("")))
			{
				for(LancamentoContaPagarRateio rateio : rateios)
					manager.remove(rateio);	
			}
			
			if(listaEmpresaFisicaSelecionada != null)
			{
				entidade.setEmpresasRateio(null);
				for(final Map.Entry<Integer, BigDecimal> empresaRateio : listaEmpresaFisicaSelecionada.entrySet())
				{
					final LancamentoContaPagarRateio lancamentoContaPagarRateioEmpresa = new LancamentoContaPagarRateio();
					// Map.Entry teve que converter os valores para String e depois para o tipo definido.
					// Problema com o Casting ???
					lancamentoContaPagarRateioEmpresa.setLancamentoContaPagar(entidade);
					lancamentoContaPagarRateioEmpresa.setEmpresaFisica(empresaFisicaService.recuperarPorId(manager, Integer.valueOf(String.valueOf(empresaRateio.getKey()))));
					lancamentoContaPagarRateioEmpresa.setPorcentagemRateio(new BigDecimal(String.valueOf(empresaRateio.getValue())));
					
					manager.persist(lancamentoContaPagarRateioEmpresa);
		
					final LogOperacao logOperacao = new LogOperacao();
					
					logOperacao.setEmpresaFisica(entidade.getEmpresaFisica());
					logOperacao.setCampo("RATEIO-".concat("F-".concat(String.valueOf(empresaRateio.getKey()))));
					logOperacao.setValorOriginal("?");
					logOperacao.setValorAlterado(String.valueOf(empresaRateio.getValue()));
					logOperacao.setData(new Date());
					logOperacao.setIdDocumentoOrigem(String.valueOf(entidade.getId()));
					logOperacao.setAgrupador("ALTERACAO_RATEIO_".concat(String.valueOf(entidade.getId()).concat(new Date().toString())));
					logOperacao.setNomeClasse(entidade.getClass().getSimpleName());
					logOperacao.setUsuario(usuarioService.recuperarSessaoUsuarioLogado());
					logOperacao.setObservacao("Numero Documento : ".concat(entidade.getNumeroDocumento()));
					logOperacao.setTipoLog(0x02);
					logOperacaoService.salvar(manager, transaction, logOperacao, false);
				}		
			}

			logOperacaoService.gravarOperacaoAlterar(manager, transaction, entidade);
			entidade = super.atualizar(manager, transaction, entidade, false);	
			
			if(transacaoIndependente)
				transaction.commit();
			
			return entidade;
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();
			
			logger.error(entidade.toString(), ex);
			
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public br.com.desenv.nepalign.model.LancamentoContaPagar atualizar(br.com.desenv.nepalign.model.LancamentoContaPagar entidade, Object listaEmpresaFisicaSelecionada) throws Exception
	{
		Map<Integer, BigDecimal> listaRateio = null;
		
		try
		{
			if(listaEmpresaFisicaSelecionada instanceof Map)
				listaRateio = (Map<Integer, BigDecimal>) listaEmpresaFisicaSelecionada;
		}
		catch(ClassCastException cce)
		{
			listaRateio = null;
		}
		return atualizar(entidade, listaRateio);
	}
	
	public br.com.desenv.nepalign.model.LancamentoContaPagar atualizar(br.com.desenv.nepalign.model.LancamentoContaPagar entidade, Map<Integer, BigDecimal> listaEmpresaFisicaSelecionada) throws Exception
	{
		return atualizar(null, null, entidade, listaEmpresaFisicaSelecionada);
	}
	
	public br.com.desenv.nepalign.model.LancamentoContaPagar atualizar(br.com.desenv.nepalign.model.LancamentoContaPagar entidade) throws Exception
	{
		return atualizar(null, null, entidade);	
	}
	
	public final List<br.com.desenv.nepalign.model.LancamentoContaPagar> replicarLancamentoContaPagar(EntityManager manager, final br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagar, final int numReplicacao) throws Exception
	{
		boolean transacaoIndependente = false;
		final List<br.com.desenv.nepalign.model.LancamentoContaPagar> listaLancamentosReplicados = new ArrayList<br.com.desenv.nepalign.model.LancamentoContaPagar>();
		
		try
		{
			if((transacaoIndependente = (manager == null)))
			{
				manager = ConexaoUtil.getEntityManager();
				manager.getTransaction().begin();
			}
			
			if (numReplicacao < 1 || numReplicacao > 99)
				throw new Exception("Informe um número de replicação válido. Número deve ser entre 00 e 99.");			
			
			final java.util.Date dataVencimento = lancamentoContaPagar.getDataVencimento();
			final Calendar calendarClone = Calendar.getInstance();
			calendarClone.setTime(dataVencimento);
			
			for (int i = 0x000000000; i < numReplicacao; i++)
			{
				final LancamentoContaPagar lancamentoContaPagarClone = lancamentoContaPagar.clone();
				lancamentoContaPagarClone.setDataVencimento(calendarClone.getTime());			
				lancamentoContaPagarClone.setNumeroDocumento(lancamentoContaPagar.getNumeroDocumento() + "-" + (i + 1) + "/" + numReplicacao);

				manager.persist(lancamentoContaPagarClone);
				logOperacaoService.gravarOperacaoInclusao(manager, manager.getTransaction(), lancamentoContaPagarClone);
				listaLancamentosReplicados.add(lancamentoContaPagarClone);
				
				calendarClone.add(Calendar.MONTH, 0x00000001);
			}
			
			if(transacaoIndependente)
				manager.getTransaction().commit();
		}	
		catch(SQLException sqle) 
		{
			if(transacaoIndependente)
				manager.getTransaction().rollback();
			
			throw sqle;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
		return listaLancamentosReplicados;	
	}

	public br.com.desenv.nepalign.model.LancamentoContaPagar gerarLancamentoContaPagarPagamentoDocumentoEntradaSaida(br.com.desenv.nepalign.model.PagamentoDocumentoEntradaSaida entidade) throws Exception
	{
		return this.gerarLancamentoContaPagarPagamentoDocumentoEntradaSaida(entidade, null, null);
	}
	
	public br.com.desenv.nepalign.model.LancamentoContaPagar gerarLancamentoContaPagarPagamentoDocumentoEntradaSaida(br.com.desenv.nepalign.model.PagamentoDocumentoEntradaSaida entidade, EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;

		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			
			final br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagar = new br.com.desenv.nepalign.model.LancamentoContaPagar();
			lancamentoContaPagar.setPagamentoDocumentoEntradaSaida(entidade);
			lancamentoContaPagar.setEmpresaFisica(entidade.getEmpresa());
			lancamentoContaPagar.setCentroFinanceiro(centroFinanceiroService.recuperarPorId(manager, Integer.parseInt(DsvConstante.getParametrosSistema().get("IdCentroFinanceiroCompras"))));
			lancamentoContaPagar.setContaGerencial(contaGerencialService.recuperarPorId(manager, Integer.parseInt(DsvConstante.getParametrosSistema().get("idContaGerencialCompraMercadoria"))));
			lancamentoContaPagar.setDataLancamento(new Date());
			lancamentoContaPagar.setDataLimiteDesconto(entidade.getDataVencimento());
			lancamentoContaPagar.setDataVencimento(entidade.getDataVencimento());
			lancamentoContaPagar.setFormaPagamento(entidade.getFormaPagamento());
			lancamentoContaPagar.setFornecedor(entidade.getDocumentoEntradaSaida().getFornecedor());
			lancamentoContaPagar.setNumeroDocumento(entidade.getNumeroFatura());
			lancamentoContaPagar.setDocumentoPagamento(entidade.getDocumentoEntradaSaida().getNumeroNotaFiscal());
			lancamentoContaPagar.setSituacaoLancamento(situacaoLancamentoService.recuperarPorId(manager, SituacaoLancamentoService.ABERTO));
			lancamentoContaPagar.setTipoConta(tipoContaService.recuperarPorId(manager, Integer.parseInt(DsvConstante.getParametrosSistema().get("idTipoContaCompraMercadoria"))));
			lancamentoContaPagar.setValorOriginal(entidade.getValor());
			lancamentoContaPagar.setValorPago(0.0);
			lancamentoContaPagar.setCodigoBarra(null);
			lancamentoContaPagar.setCodigoBarraConvertido(null);
			lancamentoContaPagar.setContaCorrente(null);
			lancamentoContaPagar.setDataQuitacao(null);
			lancamentoContaPagar.setPercentualJuroDiario(null);
			lancamentoContaPagar.setPercentualMulta(null);
			lancamentoContaPagar.setPrazoPagamento(null);
			lancamentoContaPagar.setSituacaoEmissaoCheque(null);
			lancamentoContaPagar.setValorDesconto(null);
			lancamentoContaPagar.setValorJuroDiario(null);
			lancamentoContaPagar.setValorMulta(null);
			lancamentoContaPagar.setObservacao(entidade.getObservacao());
			
			this.salvar(manager, transaction, lancamentoContaPagar);
			
			if(transacaoIndependente)
				transaction.commit();
			
			return null;
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();
			
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	
	public br.com.desenv.nepalign.model.LancamentoContaPagar gerarLancamentoContaPagarPreLancamentoContaPagar(EntityManager manager, EntityTransaction transaction, br.com.desenv.nepalign.model.PreLancamentoContaPagar entidade) throws Exception
	{
		Boolean transacaoIndependente = false;

		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			
			int idContaCorrente = 0x00;
			
			switch(entidade.getEmpresaFisicaOrigem().getId().intValue())
			{
				case 0x01:
					idContaCorrente = 101;
					break;
				case 0x02:
					idContaCorrente = 52;
					break;
				case 0x03:
					idContaCorrente = 33;
					break;
				case 0x04:
					idContaCorrente = 44;
					break;
				case 0x05:
					idContaCorrente = 55;
					break;
				case 0x07:
					idContaCorrente = 57;
					break;
				case 0x08:
					idContaCorrente = 58;
					break;
				case 0x09:
					idContaCorrente = 59;
					break;
				case 0x0D:
					idContaCorrente = 113;
					break;
				default:
					idContaCorrente = 52;
					break;
			}
			
			final br.com.desenv.nepalign.model.LancamentoContaPagar lancamentoContaPagar = new br.com.desenv.nepalign.model.LancamentoContaPagar(); 
			lancamentoContaPagar.setEmpresaFisica(entidade.getEmpresaFisica());
			lancamentoContaPagar.setContaCorrente(contaCorrenteService.recuperarPorId(manager, idContaCorrente));
			lancamentoContaPagar.setSituacaoLancamento(situacaoLancamentoService.recuperarPorId(manager, SituacaoLancamentoService.ABERTO));
			lancamentoContaPagar.setTipoConta(tipoContaService.recuperarPorContaGerencial(manager, transaction, entidade.getContaGerencial()));
			lancamentoContaPagar.setFormaPagamento(formaPagamentoService.recuperarPorId(manager, Integer.parseInt(DsvConstante.getParametrosSistema().get("idFormaPagamentoDinheiro"))));
			lancamentoContaPagar.setContaGerencial(entidade.getContaGerencial());
			lancamentoContaPagar.setDataLancamento(entidade.getDataLancamento());
			lancamentoContaPagar.setDataLimiteDesconto(entidade.getDataLancamento());
			lancamentoContaPagar.setDataVencimento(entidade.getDataLancamento());
			lancamentoContaPagar.setFornecedor(entidade.getFornecedor());
			lancamentoContaPagar.setNumeroDocumento(entidade.getNumeroDocumento());
			lancamentoContaPagar.setValorOriginal(entidade.getValor());
			lancamentoContaPagar.setValorPago(0.0);
			lancamentoContaPagar.setCodigoBarra(null);
			lancamentoContaPagar.setCodigoBarraConvertido(null);
			lancamentoContaPagar.setCentroFinanceiro(null); //precisa preencher?? validar depois TODO
			lancamentoContaPagar.setPercentualJuroDiario(null);
			lancamentoContaPagar.setPercentualMulta(null);
			lancamentoContaPagar.setPrazoPagamento(null);
			lancamentoContaPagar.setSituacaoEmissaoCheque(null);
			lancamentoContaPagar.setValorDesconto(null);
			lancamentoContaPagar.setValorJuroDiario(null);
			lancamentoContaPagar.setValorMulta(null);
			lancamentoContaPagar.setObservacao(entidade.getObservacao());
			
			salvar(manager, transaction, lancamentoContaPagar, false);
			logOperacaoService.gravarOperacaoInclusao(manager, manager.getTransaction(), lancamentoContaPagar);
			
			if(transacaoIndependente)
				transaction.commit();
			
			return lancamentoContaPagar;
		}
		catch(Exception ex)
		{
			logger.error("Não foi possível gerar o Lançamento Contas a Pagar", ex);
			
			if(transacaoIndependente)
				transaction.rollback();
			
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	
	public br.com.desenv.nepalign.model.LancamentoContaPagar recuperarPorNumeroDocumento(EntityManager manager, String numeroDocumento) throws Exception
	{
		try
		{
			return ((LancamentoContaPagarPersistence) super.getPersistence()).recuperarPorNumeroDocumento(manager, numeroDocumento);
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
	
	@SuppressWarnings("rawtypes")
	protected LancamentoContaPagar atualizarLancamentoComParcelas(EntityManager manager, EntityTransaction transaction, final LancamentoContaPagar entidade, boolean considerarBaixaIntegral) throws NumberFormatException, Exception
	{
		double valorPago 		= 0x00000000;
		double valorDesconto 	= 0x00000000;
		double valorJuros 		= 0x00000000;
		double valorMulta 		= 0x00000000;
	
		final BaixaLancamentoContaPagar baixaLancamentoContaPagarCriterio = new BaixaLancamentoContaPagar();
		baixaLancamentoContaPagarCriterio.setLancamentoContaPagar(entidade);
		
		final List<BaixaLancamentoContaPagar> listaBaixaLancamentoContaPagar = baixaLancamentoContaPagarService.listarPorObjetoFiltro(manager, baixaLancamentoContaPagarCriterio, " dataPagamento ", "desc", null);
		
		if (listaBaixaLancamentoContaPagar.size() > 0x000000000)
		{
			Iterator i2 = listaBaixaLancamentoContaPagar.iterator();
			while (i2.hasNext())
			{
				final BaixaLancamentoContaPagar baixaLancamentoContaPagar = (BaixaLancamentoContaPagar) i2.next();
				valorPago = valorPago + baixaLancamentoContaPagar.getValorOriginal().doubleValue();
				valorDesconto = valorDesconto + (baixaLancamentoContaPagar.getValorDesconto() == null ? 0x00000000 : baixaLancamentoContaPagar.getValorDesconto().doubleValue());
				valorJuros = valorJuros + (baixaLancamentoContaPagar.getValorJuros() == null ? 			0x00000000 : baixaLancamentoContaPagar.getValorJuros().doubleValue());
				valorMulta = valorMulta+ (baixaLancamentoContaPagar.getValorMulta() == null ? 			0x00000000 : baixaLancamentoContaPagar.getValorMulta().doubleValue());
				entidade.setDataQuitacao(baixaLancamentoContaPagar.getDataPagamento());
				
				if(baixaLancamentoContaPagar.getConsiderarBaixaIntegral() == 0x00000001)
					considerarBaixaIntegral = true;
			}
		}
		
		entidade.setValorDesconto(valorDesconto);
		entidade.setValorMulta(valorMulta);
		entidade.setValorPago(valorPago);
		
		if (considerarBaixaIntegral)
			entidade.setSituacaoLancamento(situacaoLancamentoService.recuperarPorId(manager, SituacaoLancamentoService.BAIXADO));
		else if (valorPago > 0x00000000)
			entidade.setSituacaoLancamento(situacaoLancamentoService.recuperarPorId(manager, SituacaoLancamentoService.PARCIAL));
		else
		{
			entidade.setSituacaoLancamento(situacaoLancamentoService.recuperarPorId(manager, SituacaoLancamentoService.ABERTO));
			entidade.setDataQuitacao(null);
		}
		
		logOperacaoService.gravarOperacaoAlterar(manager, transaction, entidade);
		return atualizar(manager, transaction, entidade);
	}
	
	public final List<String> verificarExistenciaRateio(List<LancamentoContaPagar> listaLancamentoContaPagar) throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		
		HashMap<String, List<LancamentoContaPagar>> lancamentos = new HashMap<String, List<LancamentoContaPagar>>();
		List<String> listaExistencias = new ArrayList<String>();
		
		for (final LancamentoContaPagar lancamentoContaPagar : listaLancamentoContaPagar)
		{
			if(lancamentos.get(lancamentoContaPagar.getCodigoSeguranca()) != null)
			{
				List<LancamentoContaPagar> listaLancamentoContaPagarCodigoSeguranca = lancamentos.get(lancamentoContaPagar.getCodigoSeguranca());
				listaLancamentoContaPagarCodigoSeguranca.add(lancamentoContaPagar);
				
				lancamentos.remove(lancamentoContaPagar.getCodigoSeguranca());
				lancamentos.put(lancamentoContaPagar.getCodigoSeguranca(), listaLancamentoContaPagarCodigoSeguranca);
			}
			else
			{
				List<LancamentoContaPagar> listaLancamentoContaPagarCodigoSeguranca = new ArrayList<LancamentoContaPagar>();
				listaLancamentoContaPagarCodigoSeguranca.add(lancamentoContaPagar);
				
				lancamentos.put(lancamentoContaPagar.getCodigoSeguranca(), listaLancamentoContaPagarCodigoSeguranca);				
			}
		}
		
		@SuppressWarnings("rawtypes")
		Iterator iterator = lancamentos.entrySet().iterator();
		
		while (iterator.hasNext())
		{
			@SuppressWarnings("unchecked")
			Map.Entry<String, List<LancamentoContaPagar>> next = (Map.Entry<String, List<LancamentoContaPagar>>) iterator.next();
			
			if(next.getKey() != null)
			{
				LancamentoContaPagar lancamentoContaPagarFiltro = new LancamentoContaPagar();
				lancamentoContaPagarFiltro.setCodigoSeguranca(next.getKey());
				
				List<LancamentoContaPagar> listaLancamentoContaPagarCodigoSeguranca = this.listarPorObjetoFiltro(manager, lancamentoContaPagarFiltro, null, null, null);
				
				if(listaLancamentoContaPagarCodigoSeguranca.size() != next.getValue().size())
					listaExistencias.add("Lançamento " + next.getValue().get(0).getNumeroDocumento() + " faz parte de um grupo de lançamentos com mais " + (listaLancamentoContaPagarCodigoSeguranca.size() - next.getValue().size()) + " registros!");
			}
		}
		
		manager.close();
		return listaExistencias;
	}
		
	public final String verificarExistenciaRateio(LancamentoContaPagar lancamentoContaPagar) throws Exception
	{
		try
		{
			final LancamentoContaPagar lancamentoContaPagarFiltro = new LancamentoContaPagar();
			lancamentoContaPagarFiltro.setCodigoSeguranca(lancamentoContaPagar.getCodigoSeguranca());
			
			final List<LancamentoContaPagar> listaLancamentoContaPagar = listarPorObjetoFiltro(lancamentoContaPagarFiltro);
			
			if(listaLancamentoContaPagar.size() > 0x0000001)
				return "Lançamento " + lancamentoContaPagar.getNumeroDocumento() + " faz parte de um grupo de lançamentos com mais " + (listaLancamentoContaPagar.size() - 0x0000001) + " registros!";
			else
				return null;
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
	
	/**
	 * Recupera a lista da LancamentoContaPagarRateio para utilizar no relatório
	 * @param manager
	 * @param idLancamentoContaPagar
	 * @return
	 */
	public final List<LancamentoContaPagarRateio> recuperarRateios(Integer idLancamentoContaPagar)
	{
		return recuperarRateios(null, idLancamentoContaPagar);
	}
	
	/**
	 * Recupera a lista da LancamentoContaPagarRateio para utilizar no relatório
	 * @param manager
	 * @param idLancamentoContaPagar
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public final List<LancamentoContaPagarRateio> recuperarRateios(EntityManager manager, Integer idLancamentoContaPagar)
	{
		boolean transacaoIndependente = false;
		
		try
		{
			if((transacaoIndependente = (manager == null)))
				manager = ConexaoUtil.getEntityManager();
			
			List<LancamentoContaPagarRateio> rateios = (List<LancamentoContaPagarRateio>) manager.createNativeQuery("SELECT * FROM `lancamentoContaPagarRateio` WHERE `idLancamentoContaPagar` = ".concat(String.valueOf(idLancamentoContaPagar)).concat(";"), LancamentoContaPagarRateio.class).getResultList();
			
			// existem lançamentos feitos de modo antigo, para compatibilizar, se nao achar os registros na tabela
			// lancamentoContaPagarRateio ele faz uma busca pela coluna empresasRateio e gera os valores de modo
			// equivalente para cada empresa
			if(rateios.size() == 0x00)
			{
				Object[] rateioAntigo = (Object[]) manager.createNativeQuery("SELECT `empresasRateio`, `idEmpresa` FROM `lancamentoContaPagar` WHERE `idLancamentoContaPagar` = ".concat(String.valueOf(idLancamentoContaPagar))).getSingleResult();
				
				// assume que nao existe rateio do modo antigo ou do modo novo
				if(rateioAntigo == null)
					return null;
				else
				{
					String empresasRateio 			= String.valueOf(rateioAntigo[0x00]);
					
					/* Alguns registros no banco de dados estão com o valor 'null' devido a integração com sirius */
					if(empresasRateio == null || empresasRateio.equals("null"))
						return new ArrayList<LancamentoContaPagarRateio>();
					
					String idEmpresaOrigemDespesa	= String.valueOf(rateioAntigo[0x01]);
					
					// engloba a empresa da origem da empresa na lista de rateios, caso não estiver ainda
					if(!new IgnUtil().contains(idEmpresaOrigemDespesa, Arrays.asList(empresasRateio.split(","))))
						empresasRateio += ",".concat(idEmpresaOrigemDespesa);
					
					String[] _rateios = empresasRateio.split(",");
					 
					// calcula a porcentagem para todas as empresas de modo equivalente
					BigDecimal porcentagem = new BigDecimal(100.0).divide(new BigDecimal(_rateios.length), 2, RoundingMode.FLOOR);
					
					for (String idEmpresaRateio : _rateios)
					{
						LancamentoContaPagarRateio rateio = new LancamentoContaPagarRateio();
						rateio.setEmpresaFisica(manager.find(EmpresaFisica.class, Integer.valueOf(idEmpresaRateio)));
						rateio.setPorcentagemRateio(porcentagem);
						rateios.add(rateio);
					}
					
					// adicionado validação para o resto da divisão de 100/quantidade de empresas do rateio
					if(porcentagem.multiply(new BigDecimal(_rateios.length)).doubleValue() < 100.0)
						rateios.get(0).setPorcentagemRateio(rateios.get(0).getPorcentagemRateio().add(new BigDecimal(100.0).subtract(porcentagem.multiply(new BigDecimal(_rateios.length)))));
				}
			}
			
			return rateios; 	
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	
	/**
	 * Verifica de acordo com mês/ano se o lançamento pode ser alterado.
	 * @param entidade {@link LancamentoContaPagar}
	 * @return false caso o lançamento estiver em um mês/ano anterior ao mês ano da empresa, caso o contrário, true
	 */
	protected final boolean verificarPermissaoMesContasAPagar(final br.com.desenv.nepalign.model.LancamentoContaPagar entidade)
	{
		final Calendar dataVencimentoLancamento = Calendar.getInstance();
		dataVencimentoLancamento.set(Calendar.MONTH, Integer.parseInt(IgnUtil.dateFormat.format(entidade.getDataVencimento()).split("/")[0x00000001]));
		dataVencimentoLancamento.set(Calendar.YEAR, Integer.parseInt(IgnUtil.dateFormat.format(entidade.getDataVencimento()).split("/")[0x00000002]));
		dataVencimentoLancamento.set(Calendar.DAY_OF_MONTH, dataVencimentoLancamento.getActualMaximum(Calendar.DAY_OF_MONTH));

		final Calendar mesAnoAbertoEmpresa = Calendar.getInstance();
		mesAnoAbertoEmpresa.set(Calendar.MONTH, entidade.getEmpresaFisica().getMesAbertoFin());
		mesAnoAbertoEmpresa.set(Calendar.YEAR, entidade.getEmpresaFisica().getAnoAbertoFin());
		mesAnoAbertoEmpresa.set(Calendar.DAY_OF_MONTH, mesAnoAbertoEmpresa.getActualMinimum(Calendar.DAY_OF_MONTH));
		
		if(dataVencimentoLancamento.before(mesAnoAbertoEmpresa))
			return false;
		
		return true;
	}
	
	/**
	 * Verifica se o usuário logado possui permissões especiais para alterar um {@link LancamentoContaPagar}
	 * @return true caso o usuário for autorizado, caso o contrário false.
	 */
	public final boolean verificarPermissaoManutencaoContasAPagar() throws Exception
	{
		return usuarioService.usuarioAutorizado(usuarioService.recuperarSessaoUsuarioLogado(), PARAMETRO_USUARIO_MANUTENCAO_CONTASAPAGAR);
	}
	
	/**
	 * Verifica se o usuário logado possui permissões especiais para visualizar relatórios completos do Contas à Pagar
	 * @param idUsuario {@link String}
	 * @return true caso o usuário for autorizado, caso o contrário false.
	 */
	public static final boolean isUsuarioRelatorioCompleto(final String idUsuario) throws Exception
	{
		if(idUsuario == null || idUsuario.equals(""))
			return false;
		
		for (final String usuario : DsvConstante.getParametrosSistema().get(PARAMETRO_USUARIO_AUTORIZACAO_RELATORIO_CONTASAPAGAR).split(","))
			if(usuario.equals(idUsuario))
				return true;
		
		return false; 
	}

	/**
	 * Metodo para geração da observação no logoperacao
	 * @param lancamentoContaPagar
	 * @return Observacao completa
	 */
	public String gerarObservacaoLog(final LancamentoContaPagar lancamentoContaPagar)
	{
		return "N. DOCUMENTO ".concat(lancamentoContaPagar.getNumeroDocumento()).concat(" ").
				concat("DT LANC. ").concat(IgnUtil.dateFormat.format(lancamentoContaPagar.getDataLancamento())).concat(" ").
				concat("DT VENC. ").concat(IgnUtil.dateFormat.format(lancamentoContaPagar.getDataVencimento())).concat(" ").
				concat("VLR ORIGINAL ").concat(NumberFormat.getCurrencyInstance().format(lancamentoContaPagar.getValorOriginal()));
	}
}