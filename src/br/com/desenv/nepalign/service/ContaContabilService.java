package br.com.desenv.nepalign.service;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.ContaContabil;
import br.com.desenv.nepalign.persistence.ContaContabilPersistence;

public class ContaContabilService extends GenericServiceIGN<ContaContabil, ContaContabilPersistence>
{
	public ContaContabilService() { super(); }
}