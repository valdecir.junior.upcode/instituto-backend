package br.com.desenv.nepalign.service;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.ItemMovimentacaoEstoque;
import br.com.desenv.nepalign.model.TmpItemMovimentacaoEstoque;
import br.com.desenv.nepalign.persistence.TmpItemMovimentacaoEstoquePersistence;

public class TmpItemMovimentacaoEstoqueService extends GenericServiceIGN<TmpItemMovimentacaoEstoque, TmpItemMovimentacaoEstoquePersistence>
{
	public TmpItemMovimentacaoEstoqueService() 
	{
		super();
	}
	
	public TmpItemMovimentacaoEstoque getTmpItemMovimentacaoEstoquePorItemMovimentacaoEstoque(ItemMovimentacaoEstoque itemMovimentacaoEstoque) throws Exception
	{
		TmpItemMovimentacaoEstoque tmpItemMovimentacaoEstoque = null;
		
		try
		{
			tmpItemMovimentacaoEstoque = new TmpItemMovimentacaoEstoque();
			tmpItemMovimentacaoEstoque.setCusto(itemMovimentacaoEstoque.getCusto());
			tmpItemMovimentacaoEstoque.setDataFabricacao(itemMovimentacaoEstoque.getDataFabricacao());
			tmpItemMovimentacaoEstoque.setDataValidade(itemMovimentacaoEstoque.getDataValidade());
			tmpItemMovimentacaoEstoque.setDocumentoOrigem(itemMovimentacaoEstoque.getDocumentoOrigem());
			tmpItemMovimentacaoEstoque.setEstoqueProduto(itemMovimentacaoEstoque.getEstoqueProduto());
			tmpItemMovimentacaoEstoque.setId(itemMovimentacaoEstoque.getId());
			tmpItemMovimentacaoEstoque.setNumeroLote(itemMovimentacaoEstoque.getNumeroLote());
			tmpItemMovimentacaoEstoque.setProduto(itemMovimentacaoEstoque.getProduto());
			tmpItemMovimentacaoEstoque.setQuantidade(itemMovimentacaoEstoque.getQuantidade());
			tmpItemMovimentacaoEstoque.setSequencialItem(itemMovimentacaoEstoque.getSequencialItem());
			tmpItemMovimentacaoEstoque.setTmpMovimentacaoEstoque(new TmpMovimentacaoEstoqueService().recuperarPorId(itemMovimentacaoEstoque.getMovimentacaoEstoque().getId()));
			tmpItemMovimentacaoEstoque.setUnidadeMedida(itemMovimentacaoEstoque.getUnidadeMedida());
			tmpItemMovimentacaoEstoque.setValor(itemMovimentacaoEstoque.getValor());
			tmpItemMovimentacaoEstoque.setVenda(itemMovimentacaoEstoque.getValor()); 
		}
		catch(Exception ex)
		{
			throw ex;
		}
		return tmpItemMovimentacaoEstoque;
	}
}