package br.com.desenv.nepalign.service;

import java.util.Date;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.LogCodigoBarras;
import br.com.desenv.nepalign.persistence.LogCodigoBarrasPersistence;

public class LogCodigoBarrasService extends GenericServiceIGN<LogCodigoBarras, LogCodigoBarrasPersistence>
{

	public LogCodigoBarrasService() 
	{
		super();
	}
	public void gerarLog(String metodo,String resultadoMetodo) throws Exception
	{
		LogCodigoBarras log = new LogCodigoBarras();
		log.setData(new Date());
		log.setMetodo(metodo);
		log.setUsuario(new UsuarioService().recuperarSessaoUsuarioLogado());
		log.setResultadoMetodo(resultadoMetodo);
		this.salvar(log, false);
	}
}
		