package br.com.desenv.nepalign.service;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.Grupo;
import br.com.desenv.nepalign.persistence.GrupoPersistence;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class GrupoService extends GenericServiceIGN<Grupo, GrupoPersistence>
{
	public GrupoService() 
	{
		super();
	}
	
	public final Grupo recuperarPorCodigo(int codigo) throws Exception
	{
		return recuperarPorCodigo(null, codigo);
	}
	
	public final Grupo recuperarPorCodigo(EntityManager manager, final int codigo) throws Exception
	{
		boolean managerIndependente = false;
		try
		{
			if((managerIndependente = (manager == null)))
				manager = ConexaoUtil.getEntityManager();
			
			Grupo grupoCriterio = new Grupo();
			grupoCriterio.setCodigo(codigo);
			
			return listarPorObjetoFiltro(manager, grupoCriterio, null, null, null).get(0x00);
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
}