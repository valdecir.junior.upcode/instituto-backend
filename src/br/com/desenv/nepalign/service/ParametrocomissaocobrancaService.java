package br.com.desenv.nepalign.service;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.Cobrador;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.FormaPagamento;
import br.com.desenv.nepalign.model.Parametrocomissaocobranca;
import br.com.desenv.nepalign.persistence.ParametrocomissaocobrancaPersistence;

public class ParametrocomissaocobrancaService extends GenericServiceIGN<Parametrocomissaocobranca, ParametrocomissaocobrancaPersistence>
{

	public ParametrocomissaocobrancaService() 
	{
		super();
	}
    public Parametrocomissaocobranca retornarParametro(FormaPagamento forma,Integer dias,EmpresaFisica empresa,Cobrador cob) throws Exception
    {
    	return new ParametrocomissaocobrancaPersistence().retornarParametro(forma, dias, empresa, cob);
    }
	

}

