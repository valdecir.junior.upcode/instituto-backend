package br.com.desenv.nepalign.service;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.EmpresaCaixa;
import br.com.desenv.nepalign.persistence.EmpresaCaixaPersistence;

public class EmpresaCaixaService extends GenericServiceIGN<EmpresaCaixa, EmpresaCaixaPersistence>
{
	public EmpresaCaixaService() { super(); }
}