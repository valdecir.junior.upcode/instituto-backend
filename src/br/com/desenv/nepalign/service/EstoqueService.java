package br.com.desenv.nepalign.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.excecoes.relatorios.EstoqueIndisponivelException;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.Estoque;
import br.com.desenv.nepalign.persistence.EstoquePersistence;
import br.com.desenv.nepalign.util.DsvConstante;

public class EstoqueService extends GenericServiceIGN<Estoque, EstoquePersistence>
{
	private static final SimpleDateFormat simpleDateFormatMesAno = new SimpleDateFormat("MM/yyyy");
	private static final UsuarioService usuarioService = new UsuarioService();
	
	public EstoqueService() 
	{
		super();
	}
	
	/**
	 * Verifica se não existe nenhum processo de ajuste de estoque funcionando.
	 * 
	 * Processos possíveis (RECONSTROI SALDO)
	 * @throws EstoqueIndisponivelException
	 */
	public static final void verificarEstoqueDisponivel() throws EstoqueIndisponivelException
	{
		for(final Object _threadRunning : Thread.getAllStackTraces().keySet().toArray())
		{
			if(((Thread) _threadRunning).getName().startsWith("RECONSTROI_SALDO"))
				throw new EstoqueIndisponivelException("O sistema está reconstruindo o saldo da F-" + ((Thread) _threadRunning).getName().split("_")[0x00000002] + "!");
		}
	}
	
	/**
	 * MM/yyyy da Data de Entrada tem que ser igual ao MM/yyyy aberto da EmpresaFisica
	 * @param dataEntrada Data da Movimentação Estoque
	 * @param empresaFisica EmpresaFisica da Movimentação de Estoque
	 * @return true se MM/yyyy for igual, false se for diferente
	 */
	public boolean verificarDataEntrada(final Date dataEntrada, final EmpresaFisica empresaFisica)
	{
		return simpleDateFormatMesAno.format(dataEntrada).equals(String.format("%02d", empresaFisica.getMesAberto()).concat("/").concat(String.valueOf(empresaFisica.getAnoAberto())));
	}
	
	/**
	 * Verifica de acordo com o ParametrosSistema BLOQUEAR_ESTOQUE_idEmpresaFisica se o Estoque da EmpresaFisica está bloqueado
	 * 
	 * Se EmpresaFisica for 60 (PUBLICIDADE) verifica se o idUsuario está no ParametrosSistema USUARIOS_AUTORIZACAO_MANUTENCAOESTOQUE_60
	 * @param empresaFisica
	 * @return true se estiver bloqueado, false se não estiver
	 */
	public boolean estoqueBloqueado(final EmpresaFisica empresaFisica)
	{
		try
		{
			final String bloquearMovimentacoes = empresaFisica == null ? DsvConstante.getParametrosSistema().get("BLOQUEAR_ESTOQUE") : DsvConstante.getParametrosSistema().get(empresaFisica.getId(), "BLOQUEAR_ESTOQUE");
			
			if(bloquearMovimentacoes != null && bloquearMovimentacoes.equals("S"))
				return true;
			else if(bloquearMovimentacoes != null && bloquearMovimentacoes.equals("N") && empresaFisica != null && empresaFisica.getId().intValue() == 60)
				return !usuarioService.usuarioAutorizado(usuarioService.recuperarSessaoUsuarioLogado(), "USUARIOS_AUTORIZACAO_MANUTENCAOESTOQUE_60");
			
			return false;	
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return true;
		}
	}
}