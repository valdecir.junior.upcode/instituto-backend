package br.com.desenv.nepalign.service;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.DispositivoBalanco;
import br.com.desenv.nepalign.model.EstoqueProduto;
import br.com.desenv.nepalign.model.ItemContagemBalanco;
import br.com.desenv.nepalign.persistence.ItemContagemBalancoPersistence;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ItemContagemBalancoService extends GenericServiceIGN<ItemContagemBalanco, ItemContagemBalancoPersistence> {

	@Override
	public ItemContagemBalanco salvar(ItemContagemBalanco entidade) throws Exception 
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		
		try
		{
			manager.getTransaction().begin();
			
			if (entidade == null) 
				throw new Exception("Item não pode ser nulo.");
			else if (entidade.getDispositivoBalanco() == null) 
				throw new Exception("Informe um Dispositivo válido.");
			else if (entidade.getEstoqueProduto() == null || entidade.getEstoqueProduto().getCodigoBarras().equals("")) 
				throw new Exception("Informe um Código de Barras válido.");
			else if (entidade.getQuantidade() == null || entidade.getQuantidade() < 1) 
				throw new Exception("Informe uma quantidade válida.");
			
			try 
			{
				List<EstoqueProduto> estoqueProdutoExistente = new EstoqueProdutoService().listarPorObjetoFiltro(manager, entidade.getEstoqueProduto(), null, null, null);
				
				if (estoqueProdutoExistente.size() < 1) 
					throw new Exception("Código de Barras não cadastrado.");
				else 
					entidade.setEstoqueProduto(estoqueProdutoExistente.get(0));
			} 
			catch (Exception e) 
			{
				throw new Exception("Não foi possível verificar o produto " + entidade.getEstoqueProduto().getCodigoBarras() + ".");
			}
			
			List<ItemContagemBalanco> itensExistentes = super.listarPorObjetoFiltro(manager, new ItemContagemBalanco(null, entidade.getDispositivoBalanco(), null, null), "itemContagem", "ASC", null);
			entidade.setItemContagem(itensExistentes.size() < 1 ? (entidade.getDispositivoBalanco().getSequenciaBaseBalanco().getSequenciaBase() + 1) : (itensExistentes.get(itensExistentes.size() - 1).getItemContagem() + 1));
			return super.salvar(manager, manager.getTransaction(), entidade);
		}
		catch(Exception ex)
		{
			if(manager != null)
			{
				if(manager.getTransaction().isActive())
					manager.getTransaction().rollback();
			}
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			manager.close();
		}
	}
	
	@Override
	public ItemContagemBalanco atualizar(ItemContagemBalanco entidade) throws Exception {
		
		if (entidade == null) {
			throw new Exception("Item não pode ser nulo.");
		} else if (entidade.getDispositivoBalanco() == null) {
			throw new Exception("Informe um Dispositivo válido.");
		} else if (entidade.getEstoqueProduto() == null || entidade.getEstoqueProduto().getCodigoBarras().equals("")) {
			throw new Exception("Informe um código de Barras válido.");
		} else if (entidade.getQuantidade() == null || entidade.getQuantidade() < 1) {
			throw new Exception("Informe uma quantidade válida");
		}
		
		if (entidade.getEstoqueProduto().getId() != Integer.parseInt(entidade.getEstoqueProduto().getCodigoBarras())) {
			EstoqueProduto novoEstoqueProduto =  new EstoqueProduto();
			novoEstoqueProduto.setCodigoBarras(entidade.getEstoqueProduto().getCodigoBarras());
			entidade.setEstoqueProduto(novoEstoqueProduto);
		}
		
		try {
			List<EstoqueProduto> estoqueProdutoExistente = new EstoqueProdutoService().listarPorObjetoFiltro(entidade.getEstoqueProduto());
			
			if (estoqueProdutoExistente.size() < 1) {
				throw new Exception("código de Barras não cadastrado.");
			} else {
				entidade.setEstoqueProduto(estoqueProdutoExistente.get(0));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("não foi possível verificar o produto " + entidade.getEstoqueProduto().getCodigoBarras() + ".");
		}
		
		return super.atualizar(entidade);
	}
	
	public void excluir(List<ItemContagemBalanco> listaEntidade) throws Exception {
		
		if (listaEntidade == null || listaEntidade.isEmpty()) {
			throw new Exception("Informe um intervalo válido.");
		}
		
		for (ItemContagemBalanco itemContagemBalanco : listaEntidade) {
			
			try {
				super.excluir(itemContagemBalanco);
			} catch (Exception e) {
				e.printStackTrace();
				throw new Exception("não foi possível excluir o Item " + itemContagemBalanco.getItemContagem() + ".");
			}
			
		}
		
	}
	
	@Deprecated
	public void exportarParaArquivo(DispositivoBalanco entidade) throws Exception {
		
		if (entidade == null || entidade.getId() == null || entidade.getDescricao().isEmpty()) {
			throw new Exception("Informe um Dispositivo válido.");
		} else if (entidade.getSequenciaBaseBalanco() == null || entidade.getSequenciaBaseBalanco().getId() == null) {
			throw new Exception("Sequência Base inválida.");
		}
		
		Path path = Paths.get("C:/nepalignbalanco/" + entidade.getDescricao() + "" + entidade.getSequenciaBaseBalanco().getSequenciaBase().toString());
        Files.createDirectories(path.getParent());

        try {
            Files.createFile(path);
        } catch (FileAlreadyExistsException e) {
            System.err.println(e.getMessage() + " foi sobrescrito.");
        }
		
		FileWriter writer = new FileWriter(new File("" + path));
		PrintWriter out = new PrintWriter(writer);
		
		List<ItemContagemBalanco> listaItemContagemBalanco = new ItemContagemBalancoService().listarPorObjetoFiltro(new ItemContagemBalanco(null, entidade, null, null));
		
		for (ItemContagemBalanco itemContagemBalanco : listaItemContagemBalanco) {
			out.println(itemContagemBalanco.getEstoqueProduto().getCodigoBarras() + ";" + itemContagemBalanco.getQuantidade());
		}
		
		out.close();
		writer.close();
	}

	public byte[] gerarArquivo(DispositivoBalanco entidade) throws Exception
	{
		if (entidade == null || entidade.getId() == null || entidade.getDescricao().isEmpty())
			throw new Exception("Informe um Dispositivo válido.");
		else if (entidade.getSequenciaBaseBalanco() == null || entidade.getSequenciaBaseBalanco().getId() == null)
			throw new Exception("Sequência Base inválida.");
		
		final StringBuilder output = new StringBuilder();
		
		@SuppressWarnings("unchecked")
		List<ItemContagemBalanco> listaItemContagemBalanco = ConexaoUtil.getEntityManager().createNativeQuery(
				"select idItemContagemBalanco, itemContagem, sum(quantidade) as quantidade, idDispositivoBalanco, idEstoqueProduto from itemcontagembalanco " + 
				"group by idEstoqueProduto;", ItemContagemBalanco.class).getResultList();
		
		for (ItemContagemBalanco itemContagemBalanco : listaItemContagemBalanco) 
		{
			//TODO: 50; feito para balanço do indenizado
			output.append("50".
					concat(";").
					concat(itemContagemBalanco.getEstoqueProduto().getCodigoBarras()).
					concat(";").
					concat(String.valueOf(itemContagemBalanco.getQuantidade())));
			
			output.append("\r\n");
		}
		
		return output.toString().getBytes(Charset.forName("UTF-8"));
	}
}
