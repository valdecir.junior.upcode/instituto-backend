package br.com.desenv.nepalign.service;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.TipoMovimentoCaixaDiaGrupoEmpresa;
import br.com.desenv.nepalign.persistence.TipoMovimentoCaixaDiaGrupoEmpresaPersistence;

public class TipoMovimentoCaixaDiaGrupoEmpresaService extends GenericServiceIGN<TipoMovimentoCaixaDiaGrupoEmpresa, TipoMovimentoCaixaDiaGrupoEmpresaPersistence>
{
	
	public TipoMovimentoCaixaDiaGrupoEmpresaService() 
	{ 
		super();
	}

	
}