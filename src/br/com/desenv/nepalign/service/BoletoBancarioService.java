package br.com.desenv.nepalign.service;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.commons.lang.time.DateUtils;
import org.jrimum.bopepo.BancosSuportados;
import org.jrimum.bopepo.Boleto;
import org.jrimum.bopepo.parametro.ParametroBancoSicredi;
import org.jrimum.bopepo.view.BoletoViewer;
import org.jrimum.domkee.comum.pessoa.endereco.CEP;
import org.jrimum.domkee.comum.pessoa.endereco.Endereco;
import org.jrimum.domkee.comum.pessoa.endereco.UnidadeFederativa;
import org.jrimum.domkee.financeiro.banco.ParametrosBancariosMap;
import org.jrimum.domkee.financeiro.banco.febraban.Agencia;
import org.jrimum.domkee.financeiro.banco.febraban.Carteira;
import org.jrimum.domkee.financeiro.banco.febraban.Cedente;
import org.jrimum.domkee.financeiro.banco.febraban.ContaBancaria;
import org.jrimum.domkee.financeiro.banco.febraban.NumeroDaConta;
import org.jrimum.domkee.financeiro.banco.febraban.Sacado;
import org.jrimum.domkee.financeiro.banco.febraban.TipoDeCobranca;
import org.jrimum.domkee.financeiro.banco.febraban.TipoDeTitulo;
import org.jrimum.domkee.financeiro.banco.febraban.Titulo;
import org.jrimum.utilix.ClassLoaders;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.frameworkignorante.Page;
import br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao;
import br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao.Grupo;
import br.com.desenv.nepalign.model.BoletoBancario;
import br.com.desenv.nepalign.model.Cliente;
import br.com.desenv.nepalign.model.ContaCorrente;
import br.com.desenv.nepalign.model.DuplicataParcela;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.LogDuplicata;
import br.com.desenv.nepalign.model.MotivoMovimentoBoleto;
import br.com.desenv.nepalign.model.MotivoOcorrenciaBoletoBancario;
import br.com.desenv.nepalign.model.MovimentoBoletoBancario;
import br.com.desenv.nepalign.model.OcorrenciaBoletoBancario;
import br.com.desenv.nepalign.model.ParametroSistema;
import br.com.desenv.nepalign.model.ParametrosBoletoConta;
import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.nepalign.persistence.BoletoBancarioPersistence;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import com.upcode.cnab.sicredicnab400.DetalheRemessaSicrediCNAB400;
import com.upcode.cnab.sicredicnab400.GerarArquivoRemessaSicrediCNAB400;
import com.upcode.cnab.sicredicnab400.HeaderRemessaSicrediCNAB400;
import com.upcode.cnab.sicredicnab400.TrailerRemessaSicrediCNAB400;

public class BoletoBancarioService extends GenericServiceIGN<BoletoBancario, BoletoBancarioPersistence> {

	public BoletoBancarioService() {
		super();
	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Gerar Boleto Bancário SICREDI")
	public BoletoBancario gerarBoletoBancarioSICREDI(Cliente cliente, DuplicataParcela duplicataParcela, EmpresaFisica empresaFisica, ContaCorrente contaCorrente, ParametrosBoletoConta parametros) throws Exception {

		double custoEmissaoBoleto = 0.0;
		try
		{
			custoEmissaoBoleto = Double.parseDouble(DsvConstante.getParametrosSistema().get("CUSTO_EMISSAO_BOLETO"));
		}
		catch(Exception paramException)
		{
			throw new Exception("Erro ao recuperar parametros do sistema para geração de boletos. PERCENTUAL_JUROS_MENSAL_BOLETO, PERCENTUAL_MULTA_BOLETO, GERAR_BOLETO_NEPAL, CUSTO_EMISSAO_BOLETO podem não existir. ");
		}		
		try
		{
			BoletoBancario boleto = new BoletoBancario();
			boleto.setCliente(cliente);
			boleto.setClienteCache(null);
			boleto.setContaCorrente(contaCorrente);
			boleto.setCodigoCedente(parametros.getCodigoCedente().toString());
			boleto.setCodigoPosto(parametros.getPosto().toString());
			boleto.setCupomFiscal(null);
			boleto.setDataGeracao(new Date());
			boleto.setDataPagamento(null);
			boleto.setDataVencimento(duplicataParcela.getDataVencimento());
			boleto.setDuplicataCache(null);
			boleto.setDuplicataParcela(duplicataParcela);
			boleto.setLinhaDigitavel(null);
			boleto.setNossoNumero(null);

			boleto.setNotaFiscal(null);
			boleto.setNumeroDocumento(Integer.parseInt(duplicataParcela.getDuplicata().getNumeroDuplicata().toString() + (duplicataParcela.getNumeroParcela() < 10 ? ("0" + duplicataParcela.getDuplicata().getNumeroDuplicata()) : "" + duplicataParcela.getDuplicata().getNumeroDuplicata())));
			boleto.setParcelaDuplicataCache(null);
			boleto.setValorOriginal(duplicataParcela.getValorParcela());
			boleto.setValorAcrescimos(custoEmissaoBoleto);
			boleto.setValorPago(null);
			boleto.setSequencialBoleto(buscarProximoSequencialBoleto(parametros));
			boleto.setNossoNumero(Integer.parseInt(gerarNossoNumeroSICREDI(boleto, parametros)));

			return boleto;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Gerar Boleto Bancário SICREDI (CACHE)")
	public ArrayList<BoletoBancario> gerarBoletosSICREDIBaseCACHE(Integer idContaCorrente, Integer codigoClienteCache, Integer numeroDuplicataCache, Integer numeroParcelaCache) throws Exception {
		try
		{
			EntityManager manager = null;
			EntityTransaction transaction = null;
			Integer cepSacado;

			ArrayList<BoletoBancario> boletosGerados = new ArrayList<BoletoBancario>();
			SimpleDateFormat sdfAno = new SimpleDateFormat("yyyy");
			ContaCorrente contaCorrente = new ContaCorrenteService().recuperarPorId(idContaCorrente);

			ParametrosBoletoConta criterio = new ParametrosBoletoConta();
			criterio.setAno(Integer.parseInt(sdfAno.format(new Date())));
			criterio.setContaCorrente(contaCorrente);
			ParametrosBoletoConta parametros = null;
			double percentualJurosMensal = 0.0;
			double percentualMulta = 0.0;
			double custoEmissaoBoleto = 0.0;
			int diasVencimentoBoleto = 0;
			String gerarPeloNepal = null;

			try
			{
				percentualJurosMensal = Double.parseDouble(DsvConstante.getParametrosSistema().get("PERCENTUAL_JUROS_MENSAL_BOLETO"));
				percentualMulta = Double.parseDouble(DsvConstante.getParametrosSistema().get("PERCENTUAL_MULTA_BOLETO"));
				custoEmissaoBoleto = Double.parseDouble(DsvConstante.getParametrosSistema().get("CUSTO_EMISSAO_BOLETO"));
				diasVencimentoBoleto = Integer.parseInt(DsvConstante.getParametrosSistema().get("DIAS_VENCIMENTO_BOLETO"));
				gerarPeloNepal = DsvConstante.getParametrosSistema().get("GERAR_BOLETO_NEPAL") == null?null:DsvConstante.getParametrosSistema().get("GERAR_BOLETO_NEPAL");
			}
			catch(Exception paramException)
			{
				throw new Exception("Erro ao recuperar parametros do sistema para geração de boletos. PERCENTUAL_JUROS_MENSAL_BOLETO, PERCENTUAL_MULTA_BOLETO, GERAR_BOLETO_NEPAL, CUSTO_EMISSAO_BOLETO podem não existir. ");
			}
			try {
				parametros = new ParametrosBoletoContaService().listarPorObjetoFiltro(criterio).get(0);
			} catch (Exception exParam) {
				throw new Exception("Erro ao recuperar parametros de boleto por conta", exParam);
			}

			ResultSet rs =  null;
			Connection con = null;
			Statement stm = null;
			String strSQL = "";
			if(gerarPeloNepal !=null && gerarPeloNepal.equals("S"))
			{
				strSQL = strSQL + " SELECT  ";
				strSQL = strSQL + " 	par.dataVencimento,par.numeroDuplicata,par.numeroParcela,par.valorParcela, par.idDuplicataParcela, ";
				strSQL = strSQL + " 	dup.dataCompra,dup.idCliente,dup.idEmpresaFisica AS idEmpresa,dup.numeroFatura, ";
				strSQL = strSQL + " 	cli.cep AS CEP, cid.nome AS cidade,cli.logradouro AS endereco,uf.sigla AS estado,cli.nome,cli.cpfCnpj ";
				strSQL = strSQL + " FROM duplicataparcela par ";
				strSQL = strSQL + " INNER JOIN duplicata dup ON dup.idDuplicata = par.idDuplicata ";
				strSQL = strSQL + " INNER JOIN cliente cli ON cli.idCliente = dup.idCliente ";
				strSQL = strSQL + " INNER JOIN cidade cid ON cid.idCidade = cli.idCidadeEndereco ";
				strSQL = strSQL + " INNER JOIN uf uf ON uf.idUf = cid.idUf ";
				strSQL = strSQL + " WHERE cli.idCliente = " + codigoClienteCache + " ";
				strSQL = strSQL + "   AND dup.numeroDuplicata = " + numeroDuplicataCache + " ";
				strSQL = strSQL + "   AND par.numeroparcela > 0  and situacao = 0 ";

				if (numeroParcelaCache != null && numeroParcelaCache > 0)
					strSQL = strSQL + "   AND par.numeroParcela = " + numeroParcelaCache + " ";

				strSQL = strSQL + " ORDER BY ";
				strSQL = strSQL + " 	dup.idEmpresaFisica, dup.idCliente, dup.numeroDuplicata, par.numeroParcela ";
				con = ConexaoUtil.getConexaoPadrao();
				stm = con.createStatement(); 
				rs = stm.executeQuery(strSQL);
			}
			else
			{
				strSQL = " SELECT ";
				strSQL = strSQL + " CRPD.dataVencimento, CRPD.numeroDuplicata, CRPD.numeroParcela, CRPD.valorParcela, ";
				strSQL = strSQL + " CRD.dataCompra, CRD.idCliente, CRD.idEmpresa, CRD.numeroFatura, ";
				strSQL = strSQL + " CRCLI.CEP, CRCLI.cidade, CRCLI.endereco, CRCLI.estado, CRCLI.nome, CRCLI.cpfCnpj ";
				strSQL = strSQL + " FROM ";
				strSQL = strSQL + " ((CRParcelaDuplicata AS CRPD ";
				strSQL = strSQL + " INNER JOIN CRDuplicata AS CRD ON CRPD.idEmpresa = CRD.idEmpresa AND CRPD.idCliente = CRD.idCliente AND CRPD.numeroDuplicata = CRD.numeroDuplicata) ";
				strSQL = strSQL + " INNER JOIN CRCliente AS CRCLI ON CRCLI.idCliente = CRD.idCliente) ";
				strSQL = strSQL + " WHERE ";
				strSQL = strSQL + " CRPD.idCliente = CRD.idCliente AND ";
				strSQL = strSQL + " CRPD.idEmpresa = CRD.idEmpresa AND ";
				strSQL = strSQL + " CRPD.numeroDuplicata = CRD.numeroDuplicata AND ";
				strSQL = strSQL + " CRD.idCliente = CRCLI.idCliente AND ";
				strSQL = strSQL + " CRD.idCliente = " + codigoClienteCache + " AND ";
				strSQL = strSQL + " CRD.numeroDuplicata = " + numeroDuplicataCache + " AND ";
				strSQL = strSQL + " CRPD.numeroParcela > 0 AND CRPD.situacao = 0 ";

				if (numeroParcelaCache != null && numeroParcelaCache > 0) {
					strSQL = strSQL + " AND CRPD.numeroParcela = " + numeroParcelaCache + " ";
				}

				strSQL = strSQL + " ORDER BY ";
				strSQL = strSQL + " CRD.idEmpresa, CRD.idCliente, CRD.numeroDuplicata, CRPD.numeroParcela ";
				System.out.println(strSQL);
				con = ConexaoUtil.getConexaoPadraoCACHE();
				stm = con.createStatement();
				rs = stm.executeQuery(strSQL);
			}

			manager = ConexaoUtil.getEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();

			while (rs.next()) {
				// calacula juros de um dia
				BigDecimal jurosDeUmDia = new BigDecimal((percentualJurosMensal * rs.getDouble("valorParcela") / 100) / 30).setScale(2, BigDecimal.ROUND_UP);
				// calcula Multa Mensal
				BigDecimal multaMensal = new BigDecimal(percentualMulta * rs.getDouble("valorParcela") / 100).setScale(2, BigDecimal.ROUND_UP);

				BoletoBancario boleto = new BoletoBancario();
				if(gerarPeloNepal !=null && gerarPeloNepal.equals("S"))
				{
					boleto.setCliente(new ClienteService().recuperarPorId(manager, rs.getInt("idCliente")));
					boleto.setDuplicataParcela(new DuplicataParcelaService().recuperarPorId(manager, rs.getInt("idDuplicataParcela")));
				}
				else
				{
					boleto.setDuplicataParcela(null);
					boleto.setCliente(null);
				}

				boleto.setClienteCache(codigoClienteCache + "");
				boleto.setContaCorrente(contaCorrente);
				boleto.setCodigoCedente(parametros.getCodigoCedente().toString());
				boleto.setCodigoPosto(parametros.getPosto().toString());
				boleto.setCupomFiscal(null);
				boleto.setDataGeracao(new Date());
				boleto.setDataPagamento(null);
				boleto.setDataVencimento(rs.getDate("dataVencimento"));
				boleto.setDuplicataCache(rs.getInt("numeroDuplicata"));
				boleto.setParcelaDuplicataCache(rs.getInt("numeroParcela"));
				boleto.setLinhaDigitavel(null);
				boleto.setNotaFiscal(null);
				String numeroDocumento = String.format("%07d", rs.getInt("numeroDuplicata")) + String.format("%02d", rs.getInt("numeroParcela"));
				boleto.setNumeroDocumento(Integer.parseInt(numeroDocumento));
				boleto.setValorOriginal(rs.getDouble("valorParcela")+custoEmissaoBoleto);
				boleto.setValorPago(null);
				boleto.setSequencialBoleto(buscarProximoSequencialBoleto(parametros));
				boleto.setNossoNumero(Integer.parseInt(gerarNossoNumeroSICREDI(boleto, parametros)));
				boleto.setNomeSacado(IgnUtil.removerCatacterEspeciais(rs.getString("nome")).toUpperCase());
				boleto.setCpfCnpjSacado(rs.getString("cpfCnpj"));
				if(boleto.getCpfCnpjSacado() != null || boleto.getCpfCnpjSacado().equals(""))
					throw new Exception("Cliente sem Cpf!");
				boleto.setLogradouroSacado(IgnUtil.removerCatacterEspeciais(rs.getString("endereco").toUpperCase()));
				boleto.setNumeroLogradouroSacado(null);
				boleto.setComplementoLogradoruSacado(null);

				try
				{
					cepSacado = rs.getString("CEP") == null ? 0 : Integer.parseInt(rs.getString("CEP").replaceAll("-", "").replaceAll(".", "").replaceAll(" ", ""));
					if (cepSacado==0)
						cepSacado = DsvConstante.getParametrosSistema().getInt("BOLETO_CEP_PADRAO");
				}
				catch(Exception exCep)
				{
					Logger.getGlobal().fine("Cliente sem CEP " + codigoClienteCache + ". Pode gerar problemas na geração do boleto.");
					cepSacado = DsvConstante.getParametrosSistema().getInt("BOLETO_CEP_PADRAO");
				}

				boleto.setCepSacado(cepSacado);
				boleto.setObservacao1("Após vencimento cobrar juros de R$ " + jurosDeUmDia.toString()  + " ao dia.");
				boleto.setObservacao2("Atenção Sr(a). Caixa: Por favor não receber após "+diasVencimentoBoleto+" dias do vencimento.");
				boleto.setObservacao3("Valor do boleto acrescido de R$"+custoEmissaoBoleto+" de taxa");
				boleto.setValorDesconto(0.00);
				boleto.setValorJurosDiario(jurosDeUmDia.doubleValue());
				boleto.setCidadeSacado(rs.getString("cidade"));
				boleto.setEstadoSacado(rs.getString("estado"));
				boleto.setBairroSacado("");
				boleto.setSituacaoEnvio(0);

				// validar data limite para envio de arquivos remessa
				Integer dataLimiteVencimento = Integer.parseInt(DsvConstante.getParametrosSistema().get("DATA_LIMITE_ENVIO_ARQUIVO_REMESSA_CNAB400"));
				Date dataLimite = DateUtils.addDays(new Date(), dataLimiteVencimento);

				if(boleto.getDataGeracao().getTime() > boleto.getDataVencimento().getTime())
					throw new Exception("Data de vencimento não pode ser menor que a data de geração do boleto!");
				//
				//				if (boleto.getDataVencimento().compareTo(dataLimite) > 0) {
				boletosGerados.add(boleto);
				//				} else {
				//					throw new Exception("A data limite para vencimento do boleto deve ser superior a 7 dias");
				//				}

			}
			if(boletosGerados.size()<1)
			{
				throw new Exception("Não foi encontrada nenhuma parcela em aberto!");
			}
			for (BoletoBancario boleto : boletosGerados) {
				new BoletoBancarioService().salvar(manager, transaction, boleto);
				if(gerarPeloNepal == null || gerarPeloNepal.equals("N"))
				{
					exportarBoletoParaCache(boleto, con);
				}
			}

			transaction.commit();
			rs.close();
			stm.close();
			con.close();

			return boletosGerados;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Gerar Boleto Bancário SICREDI (CACHE - SEM CUSTO)")
	public ArrayList<BoletoBancario> gerarBoletosSICREDIBaseCACHESemCusto(Integer idContaCorrente, Integer codigoClienteCache, Integer numeroDuplicataCache, Integer numeroParcelaCache) throws Exception {
		try
		{
			EntityManager manager = null;
			EntityTransaction transaction = null;
			Integer cepSacado;

			ArrayList<BoletoBancario> boletosGerados = new ArrayList<BoletoBancario>();
			SimpleDateFormat sdfAno = new SimpleDateFormat("yyyy");
			ContaCorrente contaCorrente = new ContaCorrenteService().recuperarPorId(idContaCorrente);

			ParametrosBoletoConta criterio = new ParametrosBoletoConta();
			criterio.setAno(Integer.parseInt(sdfAno.format(new Date())));
			criterio.setContaCorrente(contaCorrente);
			ParametrosBoletoConta parametros = null;
			double percentualJurosMensal = 0.0;
			double percentualMulta = 0.0;
			double custoEmissaoBoleto = 0.0;
			int diasVencimentoBoleto = 0;
			String gerarPeloNepal = null;

			try
			{
				percentualJurosMensal = Double.parseDouble(DsvConstante.getParametrosSistema().get("PERCENTUAL_JUROS_MENSAL_BOLETO"));
				percentualMulta = Double.parseDouble(DsvConstante.getParametrosSistema().get("PERCENTUAL_MULTA_BOLETO"));
				diasVencimentoBoleto = Integer.parseInt(DsvConstante.getParametrosSistema().get("DIAS_VENCIMENTO_BOLETO"));
				gerarPeloNepal = DsvConstante.getParametrosSistema().get("GERAR_BOLETO_NEPAL") == null?null:DsvConstante.getParametrosSistema().get("GERAR_BOLETO_NEPAL");
			}
			catch(Exception paramException)
			{
				throw new Exception("Erro ao recuperar parametros do sistema para geração de boletos. PERCENTUAL_JUROS_MENSAL_BOLETO, PERCENTUAL_MULTA_BOLETO, GERAR_BOLETO_NEPAL, CUSTO_EMISSAO_BOLETO podem não existir. ");
			}
			try {
				parametros = new ParametrosBoletoContaService().listarPorObjetoFiltro(criterio).get(0);
			} catch (Exception exParam) {
				throw new Exception("Erro ao recuperar parametros de boleto por conta", exParam);
			}

			ResultSet rs =  null;
			Connection con = null;
			Statement stm = null;
			String strSQL = "";
			if(gerarPeloNepal !=null && gerarPeloNepal.equals("S"))
			{
				strSQL = strSQL + " SELECT  ";
				strSQL = strSQL + " 	par.dataVencimento,par.numeroDuplicata,par.numeroParcela,par.valorParcela, par.idDuplicataParcela, ";
				strSQL = strSQL + " 	dup.dataCompra,dup.idCliente,dup.idEmpresaFisica AS idEmpresa,dup.numeroFatura, ";
				strSQL = strSQL + " 	cli.cep AS CEP, cid.nome AS cidade,cli.logradouro AS endereco,uf.sigla AS estado,cli.nome,cli.cpfCnpj ";
				strSQL = strSQL + " FROM duplicataparcela par ";
				strSQL = strSQL + " INNER JOIN duplicata dup ON dup.idDuplicata = par.idDuplicata ";
				strSQL = strSQL + " INNER JOIN cliente cli ON cli.idCliente = dup.idCliente ";
				strSQL = strSQL + " INNER JOIN cidade cid ON cid.idCidade = cli.idCidadeEndereco ";
				strSQL = strSQL + " INNER JOIN uf uf ON uf.idUf = cid.idUf ";
				strSQL = strSQL + " WHERE cli.idCliente = " + codigoClienteCache + " ";
				strSQL = strSQL + "   AND dup.numeroDuplicata = " + numeroDuplicataCache + " ";
				strSQL = strSQL + "   AND par.numeroparcela > 0  and situacao = 0 ";

				if (numeroParcelaCache != null && numeroParcelaCache > 0)
					strSQL = strSQL + "   AND par.numeroParcela = " + numeroParcelaCache + " ";

				strSQL = strSQL + " ORDER BY ";
				strSQL = strSQL + " 	dup.idEmpresaFisica, dup.idCliente, dup.numeroDuplicata, par.numeroParcela ";
				con = ConexaoUtil.getConexaoPadrao();
				stm = con.createStatement(); 
				rs = stm.executeQuery(strSQL);
			}
			else
			{
				strSQL = " SELECT ";
				strSQL = strSQL + " CRPD.dataVencimento, CRPD.numeroDuplicata, CRPD.numeroParcela, CRPD.valorParcela, ";
				strSQL = strSQL + " CRD.dataCompra, CRD.idCliente, CRD.idEmpresa, CRD.numeroFatura, ";
				strSQL = strSQL + " CRCLI.CEP, CRCLI.cidade, CRCLI.endereco, CRCLI.estado, CRCLI.nome, CRCLI.cpfCnpj ";
				strSQL = strSQL + " FROM ";
				strSQL = strSQL + " ((CRParcelaDuplicata AS CRPD ";
				strSQL = strSQL + " INNER JOIN CRDuplicata AS CRD ON CRPD.idEmpresa = CRD.idEmpresa AND CRPD.idCliente = CRD.idCliente AND CRPD.numeroDuplicata = CRD.numeroDuplicata) ";
				strSQL = strSQL + " INNER JOIN CRCliente AS CRCLI ON CRCLI.idCliente = CRD.idCliente) ";
				strSQL = strSQL + " WHERE ";
				strSQL = strSQL + " CRPD.idCliente = CRD.idCliente AND ";
				strSQL = strSQL + " CRPD.idEmpresa = CRD.idEmpresa AND ";
				strSQL = strSQL + " CRPD.numeroDuplicata = CRD.numeroDuplicata AND ";
				strSQL = strSQL + " CRD.idCliente = CRCLI.idCliente AND ";
				strSQL = strSQL + " CRD.idCliente = " + codigoClienteCache + " AND ";
				strSQL = strSQL + " CRD.numeroDuplicata = " + numeroDuplicataCache + " AND ";
				strSQL = strSQL + " CRPD.numeroParcela > 0 AND CRPD.situacao = 0 ";

				if (numeroParcelaCache != null && numeroParcelaCache > 0) {
					strSQL = strSQL + " AND CRPD.numeroParcela = " + numeroParcelaCache + " ";
				}

				strSQL = strSQL + " ORDER BY ";
				strSQL = strSQL + " CRD.idEmpresa, CRD.idCliente, CRD.numeroDuplicata, CRPD.numeroParcela ";
				System.out.println(strSQL);
				con = ConexaoUtil.getConexaoPadraoCACHE();
				stm = con.createStatement();
				rs = stm.executeQuery(strSQL);
			}

			manager = ConexaoUtil.getEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();

			while (rs.next()) {
				// calacula juros de um dia
				BigDecimal jurosDeUmDia = new BigDecimal((percentualJurosMensal * rs.getDouble("valorParcela") / 100) / 30).setScale(2, BigDecimal.ROUND_UP);
				// calcula Multa Mensal
				BigDecimal multaMensal = new BigDecimal(percentualMulta * rs.getDouble("valorParcela") / 100).setScale(2, BigDecimal.ROUND_UP);

				BoletoBancario boleto = new BoletoBancario();
				if(gerarPeloNepal !=null && gerarPeloNepal.equals("S"))
				{
					boleto.setCliente(new ClienteService().recuperarPorId(manager, rs.getInt("idCliente")));
					boleto.setDuplicataParcela(new DuplicataParcelaService().recuperarPorId(manager, rs.getInt("idDuplicataParcela")));
				}
				else
				{
					boleto.setDuplicataParcela(null);
					boleto.setCliente(null);
				}

				boleto.setClienteCache(codigoClienteCache + "");
				boleto.setContaCorrente(contaCorrente);
				boleto.setCodigoCedente(parametros.getCodigoCedente().toString());
				boleto.setCodigoPosto(parametros.getPosto().toString());
				boleto.setCupomFiscal(null);
				boleto.setDataGeracao(new Date());
				boleto.setDataPagamento(null);
				boleto.setDataVencimento(rs.getDate("dataVencimento"));
				boleto.setDuplicataCache(rs.getInt("numeroDuplicata"));
				boleto.setParcelaDuplicataCache(rs.getInt("numeroParcela"));
				boleto.setLinhaDigitavel(null);
				boleto.setNotaFiscal(null);
				String numeroDocumento = String.format("%07d", rs.getInt("numeroDuplicata")) + String.format("%02d", rs.getInt("numeroParcela"));
				boleto.setNumeroDocumento(Integer.parseInt(numeroDocumento));
				boleto.setValorOriginal(rs.getDouble("valorParcela"));
				boleto.setValorAcrescimos(custoEmissaoBoleto);
				boleto.setValorPago(null);
				boleto.setSequencialBoleto(buscarProximoSequencialBoleto(parametros));
				boleto.setNossoNumero(Integer.parseInt(gerarNossoNumeroSICREDI(boleto, parametros)));
				boleto.setNomeSacado(IgnUtil.removerCatacterEspeciais(rs.getString("nome")).toUpperCase());
				boleto.setCpfCnpjSacado(rs.getString("cpfCnpj"));
				boleto.setLogradouroSacado(IgnUtil.removerCatacterEspeciais(rs.getString("endereco").toUpperCase()));
				boleto.setNumeroLogradouroSacado(null);
				boleto.setComplementoLogradoruSacado(null);

				try
				{
					cepSacado = rs.getString("CEP") == null ? 0 : Integer.parseInt(rs.getString("CEP").replaceAll("-", "").replaceAll(".", "").replaceAll(" ", ""));
					if (cepSacado==0)
						cepSacado = DsvConstante.getParametrosSistema().getInt("BOLETO_CEP_PADRAO");
				}
				catch(Exception exCep)
				{
					Logger.getGlobal().fine("Cliente sem CEP " + codigoClienteCache + ". Pode gerar problemas na geração do boleto.");
					cepSacado = DsvConstante.getParametrosSistema().getInt("BOLETO_CEP_PADRAO");
				}

				boleto.setCepSacado(cepSacado);
				boleto.setObservacao1("Após vencimento cobrar juros de R$ " + jurosDeUmDia.toString()  + " ao dia.");
				boleto.setObservacao2("Atenção Sr(a). Caixa: Por favor não receber após "+diasVencimentoBoleto+" dias do vencimento.");
				boleto.setObservacao3("Valor do boleto acrescido de R$"+custoEmissaoBoleto+" de taxa");
				boleto.setValorDesconto(0.00);
				boleto.setValorJurosDiario(jurosDeUmDia.doubleValue());
				boleto.setCidadeSacado(rs.getString("cidade"));
				boleto.setEstadoSacado(rs.getString("estado"));
				boleto.setBairroSacado("");
				boleto.setSituacaoEnvio(0);

				// validar data limite para envio de arquivos remessa
				Integer dataLimiteVencimento = Integer.parseInt(DsvConstante.getParametrosSistema().get("DATA_LIMITE_ENVIO_ARQUIVO_REMESSA_CNAB400"));
				Date dataLimite = DateUtils.addDays(new Date(), dataLimiteVencimento);

				if(boleto.getDataGeracao().getTime() > boleto.getDataVencimento().getTime())
					throw new Exception("Data de vencimento não pode ser menor que a data de geração do boleto!");
				//
				//				if (boleto.getDataVencimento().compareTo(dataLimite) > 0) {
				boletosGerados.add(boleto);
				//				} else {
				//					throw new Exception("A data limite para vencimento do boleto deve ser superior a 7 dias");
				//				}

			}
			if(boletosGerados.size()<1)
			{
				throw new Exception("Não foi encontrada nenhuma parcela em aberto!");
			}
			for (BoletoBancario boleto : boletosGerados) {
				new BoletoBancarioService().salvar(manager, transaction, boleto);
				if(gerarPeloNepal == null || gerarPeloNepal.equals("N"))
				{
					exportarBoletoParaCache(boleto, con);
				}
			}

			transaction.commit();
			rs.close();
			stm.close();
			con.close();

			return boletosGerados;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Gerar Nosso Numero SICREDI")
	private String gerarNossoNumeroSICREDI(BoletoBancario boleto, ParametrosBoletoConta parametros) {
		SimpleDateFormat sdfAnoGeracaoAA = new SimpleDateFormat("yy");
		StringBuilder nossoNumero = new StringBuilder();
		nossoNumero.append(sdfAnoGeracaoAA.format(boleto.getDataGeracao()));
		nossoNumero.append("2");// Byte (2 ~ 9)
		nossoNumero.append(String.format("%05d", boleto.getSequencialBoleto()));
		int dvSicredi = calcularDVNossoNumeroSICREDI(parametros.getContaCorrente().getNumeroAgencia(), parametros.getPosto(), parametros.getCodigoCedente(), nossoNumero.toString());
		nossoNumero.append(String.format("%01d", dvSicredi));

		return nossoNumero.toString();
	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Calcular DV Nosso Numero SICREDI")
	private Integer calcularDVNossoNumeroSICREDI(String agencia, Integer posto, Integer codigoCedente, String numeroSequencial) {
		StringBuilder composicao = new StringBuilder();
		composicao.append(String.format("%04d", Integer.parseInt(agencia)));
		composicao.append(String.format("%02d", posto));
		composicao.append(String.format("%05d", codigoCedente));
		composicao.append(numeroSequencial);

		return getMod11(composicao.toString(), 9, 1);
	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Buscar Proximo Sequencial Boleto")
	public Integer buscarProximoSequencialBoleto(ParametrosBoletoConta criterio) throws Exception {
		ParametrosBoletoConta param = new ParametrosBoletoContaService().recuperarPorId(criterio.getId());
		param.setSequencial((param.getSequencial().intValue() + 1));
		new ParametrosBoletoContaService().atualizar(param);

		return param.getSequencial();
	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Get MODDL")
	public static int getMod11(String num, int base, int r) {
		/**
		 * Autor: Douglas Tybel <dtybel@yahoo.com.br>
		 * 
		 * Função: Calculo do Modulo 11 para geracao do digito verificador de
		 * boletos bancarios conforme documentos obtidos da Febraban -
		 * www.febraban.org.br
		 * 
		 * Entrada: $num: string numérica para a qual se deseja calcularo digito
		 * verificador; $base: valor maximo de multiplicacao [2-$base] $r:
		 * quando especificado um devolve somente o resto
		 * 
		 * Saída: Retorna o Digito verificador.
		 * 
		 * Observações: - Script desenvolvido sem nenhum reaproveitamento de
		 * código existente. - Script original de Pablo Costa
		 * <pablo@users.sourceforge.net> - Transportado de php para java -
		 * Exemplo de uso: getMod11(nossoNumero, 9,1) - 9 e 1 são fixos de
		 * acordo com a base - Assume-se que a verificação do formato das
		 * variáveis de entrada é feita antes da execução deste script.
		 */

		/*
		 * int fator = 2; 
		 * int soma = 0; 
		 * int[] numeros = new int[num.length()];
		 * 
		 * // soma das multiplicações por peso (2 ~ 9) 
		 * for (int i = numeros.length; i > 1; i--) { 
		 * 	numeros[i - 1] = Integer.parseInt(num.substring(i - 1, i)) * fator; soma += numeros[i - 1];
		 * 	fator = fator == base ? 2 : fator++;
		 * }
		 * 
		 * int dv = 11 - (soma % 11); return dv < 10 ? dv : 0;
		 */

		base = 9;
		r = 0;

		int soma = 0;
		int fator = 2;
		String[] numeros, parcial;
		numeros = new String[num.length() + 1];
		parcial = new String[num.length() + 1];

		// Separacao dos numeros
		for (int i = num.length(); i > 0; i--) {
			// pega cada numero isoladamente
			numeros[i] = num.substring(i - 1, i);
			// Efetua multiplicacao do numero pelo falor
			parcial[i] = String.valueOf(Integer.parseInt(numeros[i]) * fator);
			// Soma dos digitos
			soma += Integer.parseInt(parcial[i]);

			if (fator == base) {
				// restaura fator de multiplicacao para 2
				fator = 1;
			}

			fator++;
		}

		// Calculo do modulo 11
		if (r == 0) {
			soma *= 10;
			int digito = soma % 11;

			if (digito == 10) {
				digito = 0;
			}

			return digito;
		} else {
			int resto = soma % 11;

			return resto;
		}

	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Gerar Impressao Boletos")
	public byte[] gerarImpressaoBoletos(ArrayList<BoletoBancario> boletosBancarios) throws Exception {
		try
		{
			String nossoNumeroSemDV = null;
			String dvNossoNumero = null;
			EmpresaFisica empresa = null;
			Cedente cedente = null;
			Sacado sacado = null;
			Endereco end = null;
			CEP cep = null;
			ContaBancaria contaBancaria = null;
			Titulo titulo = null;
			Boleto bolbepo = null;
			List<Boleto> boletosImpressao = new ArrayList<Boleto>();

			for (BoletoBancario boleto : boletosBancarios) {
				nossoNumeroSemDV = boleto.getNossoNumero().toString().substring(0, boleto.getNossoNumero().toString().length() - 1);
				dvNossoNumero = boleto.getNossoNumero().toString().substring(boleto.getNossoNumero().toString().length() - 1);
				//empresa = new EmpresaFisicaService().recuperarPorId(2); /* hardcoded! */
				empresa = new EmpresaFisicaService().recuperarPorId(boleto.getContaCorrente().getEmpresaFisica().getId());

				cedente = new Cedente(empresa.getRazaoSocial(), empresa.getCnpj());
				sacado = new Sacado(boleto.getNomeSacado(), boleto.getCpfCnpjSacado());

				end = new Endereco();
				cep = new CEP(boleto.getCepSacado() == null ? "" : boleto.getCepSacado() + "");
				end.setCep(cep);

				end.setLogradouro(boleto.getLogradouroSacado() == null ? "" : boleto.getLogradouroSacado().toUpperCase());
				end.setBairro(boleto.getBairroSacado() == null ? "" : boleto.getBairroSacado().toUpperCase());
				end.setComplemento(boleto.getComplementoLogradoruSacado() == null ? "" : boleto.getComplementoLogradoruSacado().toUpperCase());
				end.setLocalidade(boleto.getCidadeSacado() == null ? "" : boleto.getCidadeSacado().toUpperCase());
				end.setNumero(boleto.getNumeroLogradouroSacado() == null ? "" : boleto.getNumeroLogradouroSacado() + "");
				end.setPais("BRASIL");
				end.setUF(new BoletoBancarioService().retornaUfBobepo(boleto.getEstadoSacado()));

				sacado.addEndereco(end);

				contaBancaria = new ContaBancaria(BancosSuportados.BANCO_SICREDI.create());
				contaBancaria.setNumeroDaConta(new NumeroDaConta(Integer.parseInt(boleto.getContaCorrente().getNumeroConta()), boleto.getContaCorrente().getNumeroConta()));
				contaBancaria.setCarteira(new Carteira(1, TipoDeCobranca.COM_REGISTRO));
				contaBancaria.setAgencia(new Agencia(Integer.parseInt(boleto.getContaCorrente().getNumeroAgencia())));

				titulo = new Titulo(contaBancaria, sacado, cedente);

				titulo.setNumeroDoDocumento(boleto.getNumeroDocumento().toString());
				titulo.setNossoNumero(nossoNumeroSemDV);
				titulo.setDigitoDoNossoNumero(dvNossoNumero);
				titulo.setValor(BigDecimal.valueOf(boleto.getValorOriginal()));
				titulo.setDataDoDocumento(boleto.getDataGeracao());
				titulo.setDataDoVencimento(boleto.getDataVencimento());
				titulo.setTipoDeDocumento(TipoDeTitulo.DM_DUPLICATA_MERCANTIL);
				//titulo.setAceite();
				titulo.setDesconto(new BigDecimal(boleto.getValorDesconto()));
				titulo.setDeducao(BigDecimal.ZERO);
				titulo.setMora(BigDecimal.ZERO);
				//adicionado por Anderson
				titulo.setAcrecimo(BigDecimal.valueOf(boleto.getValorAcrescimos()==null?0.0:boleto.getValorAcrescimos()));
				Double valorCobrado = boleto.getValorOriginal()+(boleto.getValorAcrescimos()==null?0.0:boleto.getValorAcrescimos());
				titulo.setValorCobrado(BigDecimal.valueOf(valorCobrado));

				ParametrosBancariosMap param = new ParametrosBancariosMap(ParametroBancoSicredi.POSTO_DA_AGENCIA, boleto.getContaCorrente().getPostoAgencia());
				titulo.setParametrosBancarios(param);

				bolbepo = new Boleto(titulo);

				bolbepo.setLocalPagamento("Pagaver preferencialmente nas agencias do SICREDI ou em qualquer Banco até o Vencimento.");
				bolbepo.setInstrucao1(boleto.getObservacao1());
				bolbepo.setInstrucao2(boleto.getObservacao2());
				bolbepo.setInstrucao3(boleto.getObservacao3());
				bolbepo.setInstrucao4("ATENÇÃO: Após a data de vencimento cobrar R$ " + boleto.getValorJurosDiario().toString() + " ao dia.");

				String strNumeroAgencia = boleto.getContaCorrente().getNumeroAgencia().length()<4?"0"+boleto.getContaCorrente().getNumeroAgencia():""+boleto.getContaCorrente().getNumeroAgencia();
				String strPostoAgencia = boleto.getContaCorrente().getPostoAgencia()<10?"0"+boleto.getContaCorrente().getPostoAgencia():""+boleto.getContaCorrente().getPostoAgencia();


				String campoLivre = strNumeroAgencia + "." + strPostoAgencia + "." + boleto.getContaCorrente().getNumeroConta();



				bolbepo.addTextosExtras("txtFcAgenciaCodigoCedentePosto", campoLivre);
				bolbepo.addTextosExtras("txtFcNossoNumero", titulo.getNossoNumero() + "-" + titulo.getDigitoDoNossoNumero());
				bolbepo.addTextosExtras("txtRsNossoNumero", titulo.getNossoNumero() + "-" + titulo.getDigitoDoNossoNumero());
				bolbepo.addTextosExtras("txtFcAgenciaCodigoCedente", campoLivre);
				bolbepo.addTextosExtras("txtRsAgenciaCodigoCedente", campoLivre);
				bolbepo.addTextosExtras("txtFcCodBanco", "748-X");

				boletosImpressao.add(bolbepo);
			}

			//byte[] boletosPdf = BoletoViewer.groupInOnePDF(boletosImpressao);

			File templatePersonalizado = new File(ClassLoaders.getResource("/BoletoCarne3PorPagina.pdf").getFile());
			byte[] boletosPdf = groupInPages(boletosImpressao, templatePersonalizado);

			return boletosPdf;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Gerar Impressao Boletos 2")
	public File gerarImpressaoBoletos(ArrayList<BoletoBancario> boletosBancarios, String nomeArquivo) throws Exception {
		File arquivoBoletos = new File(nomeArquivo);
		FileOutputStream fos = new FileOutputStream(arquivoBoletos);
		fos.write(gerarImpressaoBoletos(boletosBancarios));
		fos.flush();
		fos.close();

		return arquivoBoletos;
	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Gerar Arquivo CNAB")
	public String gerarArquivoCNAB400Sicredi(Integer idContaCorrente, Date dataInicial, Date dataFinal) throws Exception {
		try
		{
			verificarBoletoSemRetorno();
			SimpleDateFormat sdfAnoMesDia = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder whereLivre = new StringBuilder();
			int contDetalhe = 0;

			if (idContaCorrente == null) {
				throw new Exception("Conta Corrente não pode ser nula");
			} else if (dataInicial == null || dataFinal == null) {
				throw new Exception("Data não pode ser nula");
			}

			// DEFINE CLAUSULA WHERE
			whereLivre.append(" idContaCorrente = " + idContaCorrente + " ");
//			whereLivre.append(" AND dataGeracao >= '" + sdfAnoMesDia.format(dataInicial) + "' ");
//			whereLivre.append(" AND dataGeracao <= '" + sdfAnoMesDia.format(dataFinal) + "' ");
			whereLivre.append(" AND situacaoEnvio = 0 ");

			// LISTAR BOLETOS
			Page<BoletoBancario> resultado = new BoletoBancarioService().listarPaginadoSqlLivreGenerico(0, 0, whereLivre.toString());

			List<BoletoBancario> listaBoleto = null;

			if (resultado.getTotalRecords() > 0) {
				listaBoleto = resultado.getRecordList();
			} else {
				throw new Exception("Não foram localizados boletos a enviar nesse período");
			}

			// INSTANCIAR DEPENDENCIAS
			ContaCorrente contaCorrente = new ContaCorrenteService().recuperarPorId(idContaCorrente);
			EmpresaFisica empresaFisica = new EmpresaFisicaService().recuperarPorId(contaCorrente.getEmpresaFisica().getId());
			Calendar cal = Calendar.getInstance();
			if((cal.get(Calendar.MONDAY)+1)!= empresaFisica.getMesAberto()){
				empresaFisica.setMesAberto((cal.get(Calendar.MONDAY)+1));
			}
			ParametrosBoletoConta parametrosBoletoConta = new ParametrosBoletoConta();
			parametrosBoletoConta.setContaCorrente(contaCorrente);
			List<ParametrosBoletoConta> listValidacaoDuplicidade = new ParametrosBoletoContaService().listarPorObjetoFiltro(parametrosBoletoConta);

			if (listValidacaoDuplicidade.size() < 1) {
				throw new Exception("Nenhum parâmetro encontrado para essa Conta Corrente");
			} else if (listValidacaoDuplicidade.size() > 1) {
				throw new Exception("Há mais de 1 parâmetro para essa Conta Corrente");
			} else {
				parametrosBoletoConta = (ParametrosBoletoConta) new ParametrosBoletoContaService().listarPorObjetoFiltro(parametrosBoletoConta).get(0);
			}

			int numeroSequencial = 0;

			// INSTANCIAR HEADER
			Integer agencia;
			//Integer dvAgencia;/* SICREDI não usa dvAgencia */

			if (parametrosBoletoConta.getContaCorrente().getNumeroAgencia().contains("-")) {
				String fullString = parametrosBoletoConta.getContaCorrente().getNumeroAgencia();
				String[] partString = fullString.split("-");
				agencia = Integer.parseInt(partString[0]);
				//dvAgencia = Integer.parseInt(partString[1]);/* SICREDI não usa dvAgencia */
			} else {
				agencia = Integer.parseInt(parametrosBoletoConta.getContaCorrente().getNumeroAgencia());
				//dvAgencia = null;/* SICREDI não usa dvAgencia */
			}

			Integer conta;
			String dac;

			if (parametrosBoletoConta.getContaCorrente().getNumeroConta().contains("-")) {
				String fullString = parametrosBoletoConta.getContaCorrente().getNumeroConta();
				String[] partString = fullString.split("-");
				conta = Integer.parseInt(partString[0]);
				dac = partString[1];
			} else {
				conta = Integer.parseInt(parametrosBoletoConta.getContaCorrente().getNumeroConta());
				dac = contaCorrente.getDvContaCorrente().toString();
			}

			numeroSequencial++;
			HeaderRemessaSicrediCNAB400 header = new HeaderRemessaSicrediCNAB400(
					agencia, 
					conta, 
					dac, 
					0, 
					empresaFisica.getRazaoSocial(),
					new Date(), 
					numeroSequencial,
					parametrosBoletoConta.getSequencial(),
					parametrosBoletoConta.getCodigoCedente(),
					empresaFisica.getCnpj()
					);

			// POPULAR LISTA DE DETALHES
			DetalheRemessaSicrediCNAB400[] listaDetalhes = new DetalheRemessaSicrediCNAB400[listaBoleto.size()];
			String nossoNumero;/* TODO verificar esse warning */
			Integer digitoVerificadorNossoNumero;

			Integer dataLimiteVencimento = Integer.parseInt(DsvConstante.getParametrosSistema().get("DATA_LIMITE_ENVIO_ARQUIVO_REMESSA_CNAB400"));

			for (BoletoBancario boleto : listaBoleto) {
				// validar data limite para envio de arquivos remessa
				Date dataLimite = DateUtils.addDays(new Date(), dataLimiteVencimento);

				//				if (boleto.getDataVencimento().compareTo(dataLimite) <= 0 ) {
				//					throw new Exception("A data limite para vencimento do boleto deve ser superior a 7 dias");
				//				}

				numeroSequencial++;
				nossoNumero = boleto.getNossoNumero().toString().substring(0, boleto.getNossoNumero().toString().length() - 1);
				digitoVerificadorNossoNumero = Integer.parseInt(boleto.getNossoNumero().toString().substring(boleto.getNossoNumero().toString().length() - 1));

				DetalheRemessaSicrediCNAB400 detalhe = new DetalheRemessaSicrediCNAB400(
						header.getConta() + "",
						header.getConta() + "",
						header.getAgencia(),
						header.getDvAgencia(),
						Long.parseLong(header.getConta() + ""),
						header.getDac(),
						Long.parseLong(header.getNumeroConvenio() + ""),
						numeroSequencial + "",
						boleto.getNossoNumero() + "",
						digitoVerificadorNossoNumero.intValue(),
						"",
						"",
						0,
						0,
						"A",
						0,
						0,
						boleto.getNumeroDocumento() + "",
						boleto.getDataVencimento(),
						boleto.getValorOriginal().doubleValue(),
						"A",
						boleto.getDataGeracao(),
						"01",
						"00",
						boleto.getValorJurosDiario().doubleValue(),
						0.00,
						"",
						boleto.getValorDesconto().doubleValue(),
						0.00,
						0.00,
						"",
						Long.parseLong("0"),
						boleto.getNomeSacado(),
						boleto.getLogradouroSacado(),
						(boleto.getCepSacado() == null ? 0 : boleto.getCepSacado()),
						boleto.getBairroSacado(), boleto.getCidadeSacado(), 
						boleto.getEstadoSacado(), 
						contaCorrente.getInstrucaoBoleto(), 
						0, 
						Long.parseLong(boleto.getSequencialBoleto().toString()), 
						"A", 
						"A",
						boleto.getCpfCnpjSacado()
						);

				listaDetalhes[contDetalhe] = detalhe;
				contDetalhe++;
			}

			// INSTANCIAR TRAILER
			TrailerRemessaSicrediCNAB400 trailer = new TrailerRemessaSicrediCNAB400();
			trailer.setTipoRegistro('9');

			// INSTANCIAR GERADOR
			ParametroSistemaService parametroSistemaService = new ParametroSistemaService();
			ParametroSistema parametroSistema = new ParametroSistema();
			parametroSistema.setChave("ULTIMO_ARQUIVO_REMESSA_CNAB400");
			parametroSistema.setEmpresaFisica(empresaFisica);
			parametroSistema = parametroSistemaService.listarPorObjetoFiltro(parametroSistema).get(0);

			GerarArquivoRemessaSicrediCNAB400 gerador = new GerarArquivoRemessaSicrediCNAB400(
					header, 
					listaDetalhes, 
					trailer, 
					Integer.parseInt(parametroSistema.getValor())
					);

			// CRIAR ARQUIVO REMESSA
			// formatar código do cedente ( CCCCC )
			String CCCCC = parametrosBoletoConta.getCodigoCedente().toString();

			for (int i = parametrosBoletoConta.getCodigoCedente().toString().length(); i < 5; i++) {
				CCCCC = "0" + CCCCC;
			}

			// codificar mês ( M )
			String M = defineMesPadraoSicredi(empresaFisica.getMesAberto());

			// formatar dia ( DD )
			Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
			String DD = String.format("%02d", calendar.get(Calendar.DATE));

			String caminhoArquivoCNAB400 = DsvConstante.getParametrosSistema().get("CAMINHO_ARQUIVO_REMESSA_CNAB400");
			File folder = new File(caminhoArquivoCNAB400);
			File[] listOfFiles = folder.listFiles();
			Integer controleArquivoRemessa = 0;

			for (File file : listOfFiles) {

				if (file.isFile() && (file.getName().startsWith(CCCCC + M + DD) || file.getName().startsWith(CCCCC + M + DD + ".RM"))) {
					controleArquivoRemessa++;
				}

			}

			File arquivoGerado;

			if (controleArquivoRemessa > 9) {
				throw new Exception("O limite diário de 10 arquivos remessa foi atingido");
			} else if (controleArquivoRemessa == 0) {
				arquivoGerado = gerador.gerar(caminhoArquivoCNAB400 + "/" + CCCCC + M + DD + ".CRM");
			} else {

				if (controleArquivoRemessa < 9) {
					arquivoGerado = gerador.gerar(caminhoArquivoCNAB400 + "/" + CCCCC + M + DD + ".RM" + (controleArquivoRemessa + 1));
				} else {
					arquivoGerado = gerador.gerar(caminhoArquivoCNAB400 + "/" + CCCCC + M + DD + ".RM0");// 0 (zero) é a 10ª remessa
				}

			}

			// Atualizar número do ultimo arquivo remessa CNAB400 
			parametroSistema.setValor("" + (Integer.parseInt(parametroSistema.getValor()) + 1));
			parametroSistemaService.atualizar(parametroSistema);

			EntityManager manager = ConexaoUtil.getEntityManager();
			EntityTransaction transaction = manager.getTransaction();
			transaction.begin();
			BoletoBancarioService boletoBancarioService = new BoletoBancarioService();

			for (BoletoBancario boletoBancario : listaBoleto) {
				boletoBancario.setSituacaoEnvio(1);// seta a situação como enviado 
				boletoBancarioService.atualizar(manager, transaction, boletoBancario);
			}
			new ArquivoBoletoBancarioService().salvar(arquivoGerado.getName(), "E", manager, transaction);
			transaction.commit();
			manager.close();

			return arquivoGerado.getAbsolutePath();
		}
		catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}
	}

	private void verificarBoletoSemRetorno() throws Exception {
		try {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Integer diasVerificarSemRetorno = DsvConstante.getParametrosSistema().get("DIAS_VERIFICAR_BOLETOS_SEM_RETORNO")==null
					?5
							:DsvConstante.getParametrosSistema().getInt("DIAS_VERIFICAR_BOLETOS_SEM_RETORNO");
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, diasVerificarSemRetorno * (-1));
			String where = " dataGeracao <= '"+df.format(cal.getTime())+"' and idOcorrenciaBoletoBancario is null";
			System.out.println(where);
			List<BoletoBancario> boletos = listarPaginadoSqlLivreGenerico(Integer.MAX_VALUE, 1, where).getRecordList();
			if(boletos.size()>0)
				throw new Exception("Há boletos sem retorno a mais de "+diasVerificarSemRetorno+" dias, Por favor baixar o arquivo de retorno no sistema!");
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		
	}
	public static void main(String args[]) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		//BoletoBancario boleto = new BoletoBancario();
		Cliente cliente = new Cliente();
		cliente.setId(1);
		//ContaCorrente conta = new ContaCorrente();
		//conta = new ContaCorrenteService().recuperarPorId(5);

		BoletoBancarioService boletoService = new BoletoBancarioService();
		// ArrayList<BoletoBancario> boletos = boletoService.gerarBoletosSICREDIBaseCACHE(5, 27509, 239165, null);
		// List<BoletoBancario> boletos = boletoService.listar();
		// boletoService.gerarImpressaoBoletos(new ArrayList<BoletoBancario>(boletos), DsvConstante.getParametrosSistema().get("CAMINHO_ARQUIVO_BOLETO_PDF") + "/" + new SimpleDateFormat("yyyyMMddhhmmss").format(new Date()) + ".pdf");

		boletoService.gerarArquivoCNAB400Sicredi(5, sdf.parse("01/10/2015"), sdf.parse("30/10/2015"));

		/*
		 * boleto.setCepSacado(87710220); boleto.setCliente(cliente);
		 * boleto.setClienteCache("1"); boleto.setCodigoBarras(null);
		 * boleto.setCodigoCedente("12345"); boleto.setCodigoPosto("13");
		 * boleto.setComplementoLogradoruSacado("CASA 1");
		 * boleto.setContaCorrente(conta);
		 * boleto.setCpfCnpjSacado("24595931808"); boleto.setCupomFiscal(null);
		 * boleto.setDataGeracao(new Date());
		 * boleto.setDataVencimento(sdf.parse("07/07/2015"));
		 * boleto.setDataPagamento(null); boleto.setDuplicataCache(234567);
		 * boleto.setDuplicataParcela(null); boleto.setLinhaDigitavel(null);
		 * boleto.setLogradouroSacado("RUA ANGELO VIGO");
		 * boleto.setLinhaDigitavel(null);
		 * boleto.setNomeSacado("ANDERSON PARADELAS RIBEIRO FIGUERIEDO");
		 * boleto.setNossoNumero(15212345); boleto.setNotaFiscal(null);
		 * boleto.setNumeroDocumento(234567);
		 * boleto.setNumeroLogradouroSacado(47);
		 * boleto.setObservacao1("OBSERVACAO 1");
		 * boleto.setObservacao2("BOSERVACAO 2");
		 * boleto.setObservacao3("BOSERVACAO 3");
		 * boleto.setParcelaDuplicataCache(1); boleto.setSequencialBoleto(1);
		 * boleto.setValorDesconto(0.00); boleto.setValorJurosDiario(0.23);
		 * boleto.setValorPago(0.00); boleto.setValorOriginal(30.00);
		 * boleto.setEstadoSacado("PR"); boleto.setCidadeSacado("CURITIBA");
		 * boleto.setBairroSacado("PILARZINHO"); new
		 * BoletoBancarioService().salvar(boleto);
		 */
	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Group In Pages")
	private static byte[] groupInPages(List<Boleto> boletos, File templatePersonalizado) {

		BoletoViewer boletoViewer = new BoletoViewer(boletos.get(0));
		boletoViewer.setTemplate(templatePersonalizado);

		List<byte[]> boletosEmBytes = new ArrayList<byte[]>(boletos.size());

		for (Boleto bop : boletos) {
			boletosEmBytes.add(boletoViewer.setBoleto(bop).getPdfAsByteArray());
		}

		try {
			//arq = Files.bytesToFile(filePath, mergeFilesInPages(boletosEmBytes));
			return mergeFilesInPages(boletosEmBytes);
		} catch (Exception e) {
			throw new IllegalStateException("Erro durante a geração do PDF! Causado por " + e.getLocalizedMessage(), e);
		}

	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Merge Files")
	public static byte[] mergeFilesInPages(List<byte[]> pdfFilesAsByteArray) throws DocumentException, IOException {

		Document document = new Document();
		ByteArrayOutputStream byteOS = new ByteArrayOutputStream();

		PdfWriter writer = PdfWriter.getInstance(document, byteOS);

		document.open();

		PdfContentByte cb = writer.getDirectContent();
		float positionAnterior = 0;

		// Para cada arquivo da lista, cria-se um PdfReader, responsável por ler
		// o arquivo PDF e recuperar informações dele.
		for (byte[] pdfFile : pdfFilesAsByteArray) {
			PdfReader reader = new PdfReader(pdfFile);

			// Faz o processo de mesclagem por página do arquivo, começando pela
			// de número 1.
			for (int i = 1; i <= reader.getNumberOfPages(); i++) {
				float documentHeight = cb.getPdfDocument().getPageSize().getHeight();

				// Importa a página do PDF de origem
				PdfImportedPage page = writer.getImportedPage(reader, i);
				float pagePosition = positionAnterior;

				/*
				 * Se a altura restante no documento de destino form menor que a
				 * altura do documento, cria-se uma nova página no documento de
				 * destino.
				 */
				if ((documentHeight - positionAnterior) <= page.getHeight()) {
					document.newPage();
					pagePosition = 0;
					positionAnterior = 0;
				}

				// Adiciona a página ao PDF destino
				cb.addTemplate(page, 0, pagePosition);

				positionAnterior += page.getHeight();
			}

		}

		byteOS.flush();
		document.close();

		byte[] arquivoEmBytes = byteOS.toByteArray();
		byteOS.close();

		return arquivoEmBytes;
	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Retorna UF Bobepo")
	public UnidadeFederativa retornaUfBobepo(String sigla) 
	{
		if (sigla.equals("PR"))
			return UnidadeFederativa.PR;
		else if (sigla.equals("SP"))
			return UnidadeFederativa.SP;
		else if (sigla.equals("MS"))
			return UnidadeFederativa.MS;
		else if (sigla.equals("MT"))
			return UnidadeFederativa.MT;
		else if (sigla.equals("RJ"))
			return UnidadeFederativa.RJ;		
		else if (sigla.equals("MG"))
			return UnidadeFederativa.MG;
		else if (sigla.equals("RS"))
			return UnidadeFederativa.RS;
		else if (sigla.equals("SC"))
			return UnidadeFederativa.SC;		
		else
			return UnidadeFederativa.DESCONHECIDO;
	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Define Mes Padrão SICREDI")
	private String defineMesPadraoSicredi(int mes) {

		if (mes < 10)
			return mes + "";
		else if (mes == 10)
			return "O";
		else if (mes == 11)
			return "N";
		else// mes 12
			return "D";
	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Apagar Boletos")
	public void apagarBoletos(List<BoletoBancario> lista,LogDuplicata logEnviados) throws Exception 
	{
		this.apagarBoletos(lista,logEnviados, null, null);
	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Apagar Boletos 2")
	public void apagarBoletos(List<BoletoBancario> lista, LogDuplicata logEnviados,EntityManager manager, EntityTransaction transaction) throws Exception 
	{
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			Usuario usuarioLogado = new UsuarioService().recuperarSessaoUsuarioLogado();
			for (BoletoBancario boletoBancario : lista)
			{
				Integer idCliente = null;
				if(boletoBancario.getClienteCache()!=null && boletoBancario.getClienteCache()!="")
				{
					idCliente = Integer.parseInt(boletoBancario.getClienteCache());
				}
				else if(boletoBancario.getCliente() != null) 
				{
					idCliente = boletoBancario.getCliente().getId();
				}
				MotivoMovimentoBoletoService motivoBoletoService = new MotivoMovimentoBoletoService();
				MovimentoBoletoBancario movimentoBoletoBancarioCriterio = new MovimentoBoletoBancario();
				MovimentoBoletoBancarioService movimentoBoletoBancarioService = new MovimentoBoletoBancarioService();
				movimentoBoletoBancarioCriterio.setBoletoBancario(boletoBancario);
				List<MovimentoBoletoBancario> listaMovimentos = movimentoBoletoBancarioService.listarPorObjetoFiltro(manager, movimentoBoletoBancarioCriterio, null, null, null);
				for (MovimentoBoletoBancario movimentoBoletoBancario : listaMovimentos) {
					MotivoMovimentoBoleto motivoMovimentoBoletoCriterio = new MotivoMovimentoBoleto();
					motivoMovimentoBoletoCriterio.setMovimentoBoletoBancario(movimentoBoletoBancario);
					List<MotivoMovimentoBoleto> motivos = motivoBoletoService.listarPorObjetoFiltro(manager, motivoMovimentoBoletoCriterio, null, null, null);
					for (MotivoMovimentoBoleto motivoMovimentoBoleto : motivos) {
						motivoBoletoService.excluir(manager, transaction, motivoMovimentoBoleto);
					}
					movimentoBoletoBancarioService.excluir(manager, transaction, movimentoBoletoBancario);
				}
				this.excluir(manager, transaction, boletoBancario);
				LogDuplicata log = new LogDuplicata();
				log.setData(new Date());
				log.setIdCliente(idCliente);
				log.setIdDocumentoOrigem(boletoBancario.getNossoNumero().toString());
				log.setIdEmpresaFisica(boletoBancario.getContaCorrente().getEmpresaFisica().getId());
				log.setMotivo("Apagou o Boleto da duplicata: "+boletoBancario.getDuplicataCache()+" do cliente :"+boletoBancario.getClienteCache() +" parcela: "+boletoBancario.getParcelaDuplicataCache());
				log.setNomeClasse("BoletoBancario");
				log.setNomeUsuario(usuarioLogado.getId()+" - "+usuarioLogado.getNome());
				new LogDuplicataService().salvar(manager, transaction, log);
			}

			if(logEnviados != null)
			{
				BoletoBancario boletoBancario = lista.get(0);
				Integer idCliente = null;
				if(boletoBancario.getClienteCache()!=null && boletoBancario.getClienteCache()!="")
				{
					idCliente = Integer.parseInt(boletoBancario.getClienteCache());
				}
				else if(boletoBancario.getCliente() != null) 
				{
					idCliente = boletoBancario.getCliente().getId();
				}
				logEnviados.setData(new Date());
				logEnviados.setIdCliente(idCliente); 
				new LogDuplicataService().salvar(manager, transaction, logEnviados);
			}

			if(transacaoIndependente)
			{
				transaction.commit();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();

			if(transacaoIndependente)
				transaction.rollback();

			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}

	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Baixar Arquivo Retorno SICREDI")
	public void baixarArquivoRetornoSicredi(byte[] arquivoRetorno,String nomeArquivo,String cnpj,Date data) throws Exception
	{
		this.baixarArquivoRetornoSicredi(arquivoRetorno, nomeArquivo,cnpj,data, null, null);
	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Baixar Arquivos Retorno SICREDI 2")
	public void baixarArquivoRetornoSicredi(byte[] arquivoRetorno,String nomeArquivo ,String cnpj,Date data,EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			List<MovimentoBoletoBancario> lista = new MovimentoBoletoBancarioService().listarPorNomerArquivo(nomeArquivo, manager, transaction);
			if(lista != null && lista.size()>0 )
				throw new Exception("Arquivo já foi abaixado no sistema!");

			String caminhoArquivo = DsvConstante.getParametrosSistema().get("CAMINHO_ARQUIVO_RETORNO_CNAB400");
			File pasta = new File(caminhoArquivo);
			File arquivo = null;
			if(pasta.exists())
			{
				arquivo = new File(caminhoArquivo+File.separator+nomeArquivo);

			}
			else
			{
				if(pasta.mkdirs())
				{
					arquivo = new File(caminhoArquivo+File.separator+nomeArquivo);
				}
				else
				{
					throw new Exception("Não foi possivel salvar o arquivo no servidor!");
				}
			}
			FileOutputStream stream = new FileOutputStream(arquivo);
			try 
			{
				stream.write(arquivoRetorno);
			} 
			finally
			{
				stream.close();
				try {
					FileReader arq = new FileReader(arquivo);
					BufferedReader lerArq = new BufferedReader(arq);
					String linha = lerArq.readLine(); 
					verficiarCabecalhoArquivoRetorno(linha, cnpj, data);
					String dataArquivo = linha.substring(94, 102);
					SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
					Date dateArquivo = sf.parse(dataArquivo);
					while (linha != null)
					{
						linha = lerArq.readLine(); 
						System.out.printf("%s\n", linha);
						if( linha != null) 
						{
							if(linha.substring(0,1) != null && linha.substring(0,1).equals("1"))
							{
								inserirRegistroArquivoRetorno(linha, nomeArquivo, dateArquivo,manager, transaction);
							}
						}
					}
					arq.close();
					new ArquivoBoletoBancarioService().salvar(nomeArquivo, "R", manager, transaction);
				} 
				catch (IOException e)
				{
					System.err.printf("Erro na abertura do arquivo: %s.\n",e.getMessage());
				}

			}
			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();

			if(transacaoIndependente)
				transaction.rollback();

			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	private void verficiarCabecalhoArquivoRetorno(String linha,String cnpj,Date data) throws Exception
	{
		String cnpjArquivo = linha.substring(31, 45);
		String dataArquivo = linha.substring(94, 102);
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
		Date dateArquivo = sf.parse(dataArquivo);
		if(!cnpj.toUpperCase().trim().replaceAll("[^0-9]", "").equals(cnpjArquivo))  
			throw new Exception("CNPJ informada é diferente do CNPJ do arquivo de retorno!");
		if(data.getTime() != dateArquivo.getTime())
			throw new Exception("Data informada é difrente da Data do arquivo de retorno!");
	}
	private void inserirRegistroArquivoRetorno(String linha,String nomeArquivo,Date dateArquivo,EntityManager manager, EntityTransaction transaction) throws Exception
	{

		String tipoCobrancaArquivo = linha.substring(13, 14); 
		String nossoNumeroArquivo = linha.substring(47, 62);
		String valorTituloArquivo = linha.substring(152, 165);
		String valorPagoArquivo = linha.substring(253, 266);
		String ocorrenciaArquivo = linha.substring(108, 110);
		String motivoArquivo = linha.substring(318, 328);
		Usuario usuario = new UsuarioService().recuperarSessaoUsuarioLogado();
		String decimaisTitulo = valorTituloArquivo.substring(valorTituloArquivo.length()-2);
		String inteiroTitulo = valorTituloArquivo.substring(0,valorTituloArquivo.length()-2);
		double valorTitulo = Double.parseDouble(inteiroTitulo+"."+decimaisTitulo);
		String decimaisPago = valorPagoArquivo.substring(valorTituloArquivo.length()-2);
		String inteiroPago = valorPagoArquivo.substring(0,valorTituloArquivo.length()-2);
		double valorPago = Double.parseDouble(inteiroPago+"."+decimaisPago);

		BoletoBancario boleto = new BoletoBancario();
		boleto.setNossoNumero(Integer.parseInt(nossoNumeroArquivo.trim()));
		List<BoletoBancario> listaBoleto = listarPorObjetoFiltro(manager, boleto, null, null, null);
		boleto = listaBoleto.size() > 0 ? listaBoleto.get(0) : null;

		System.out.println(ocorrenciaArquivo); 
		OcorrenciaBoletoBancario ocorrencia = new OcorrenciaBoletoBancarioService().recuperarPorCodigo(ocorrenciaArquivo, manager, transaction);
		MovimentoBoletoBancario movimentoBoletoBancario = new MovimentoBoletoBancario();
		movimentoBoletoBancario.setData(dateArquivo);
		movimentoBoletoBancario.setDataImportado(new Date()); 
		movimentoBoletoBancario.setBoletoBancario(boleto);
		movimentoBoletoBancario.setOcorrenciaBoleto(ocorrencia);
		movimentoBoletoBancario.setTipoBoleto(tipoCobrancaArquivo);
		movimentoBoletoBancario.setNomeArquivo(nomeArquivo);
		movimentoBoletoBancario.setUsuario(usuario);
		movimentoBoletoBancario.setValorTitulo(valorTitulo);
		movimentoBoletoBancario.setNossoNumero(Integer.parseInt(nossoNumeroArquivo.trim()));
		movimentoBoletoBancario.setValorPago(valorPago);
		movimentoBoletoBancario = new MovimentoBoletoBancarioService().salvar(manager, transaction, movimentoBoletoBancario, false);

		if(boleto != null)
		{
			boleto.setOcorrenciaBoletoBancario(ocorrencia);
			boleto.setValorPago(valorPago);
		}
		for (int i = 0; i < motivoArquivo.length(); i = i+2)
		{
			String codigoMotivo = motivoArquivo.substring(i,i+2); 
			if(!codigoMotivo.equals("00") && !codigoMotivo.trim().equals("")) 
			{
				MotivoOcorrenciaBoletoBancario motivoOcorrenciaBoletoBancario = new MotivoOcorrenciaBoletoBancarioService().recuperarPorCodigo(codigoMotivo, manager, transaction);
				MotivoMovimentoBoleto motivoMovimentoBoleto = new MotivoMovimentoBoleto();
				motivoMovimentoBoleto.setMotivoOcorrenciaBoletoBancario(motivoOcorrenciaBoletoBancario);
				motivoMovimentoBoleto.setMovimentoBoletoBancario(movimentoBoletoBancario);
				motivoMovimentoBoleto.setOcorrenciaBoletoBancario(ocorrencia);
				new MotivoMovimentoBoletoService().salvar(manager, transaction, motivoMovimentoBoleto);
			}
		}
		if(boleto != null && valorTitulo != boleto.getValorOriginal())
		{
			OcorrenciaBoletoBancario ocorrenciaValorErrado = new OcorrenciaBoletoBancarioService().recuperarPorCodigo("999", manager, transaction);
			boleto.setOcorrenciaBoletoBancario(ocorrenciaValorErrado);
			MotivoOcorrenciaBoletoBancario motivoOcorrenciaBoletoBancario = new MotivoOcorrenciaBoletoBancarioService().recuperarPorCodigo("999", manager, transaction);
			MotivoMovimentoBoleto motivoMovimentoBoleto = new MotivoMovimentoBoleto();
			motivoMovimentoBoleto.setMotivoOcorrenciaBoletoBancario(motivoOcorrenciaBoletoBancario);
			motivoMovimentoBoleto.setMovimentoBoletoBancario(movimentoBoletoBancario);
			motivoMovimentoBoleto.setOcorrenciaBoletoBancario(ocorrenciaValorErrado);
			new MotivoMovimentoBoletoService().salvar(manager, transaction, motivoMovimentoBoleto);
		}
		if(boleto != null)
			atualizar(manager, transaction, boleto);
	}

	public void exportarBoletoParaCache(BoletoBancario boletoBancario, Connection conCache) throws Exception
	{
		boolean conIndependente = true;
		String sql;
		Connection con = null;
		try
		{
			if (conCache== null)
			{
				con = ConexaoUtil.getConexaoPadraoCACHE();
			}
			else
			{
				conIndependente=false;
				con = conCache;
			}
			//verifica se boleto já existe
			sql = "SELECT * FROM CRBoletoDuplicata WHERE numeroDuplicata = '" + boletoBancario.getDuplicataCache();
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery(sql);
			Double valorAcrecimo = boletoBancario.getValorAcrescimos()==null?0.0:boletoBancario.getValorAcrescimos();
			if (rs.next()) //Boleto existe, apaga os registros
			{
				LogDuplicata log = new LogDuplicata();
				log.setData(new Date());
				log.setIdCliente(rs.getInt("idCliente"));
				log.setIdDocumentoOrigem(boletoBancario.getDuplicataCache().toString());
				log.setMotivo("Gerou boleto no nepal que já exite no cache");
				log.setValorAlterado(""+boletoBancario.getNossoNumero());
				log.setValorOriginal(""+rs.getInt("nossoNumero"));

				sql = "delete from  CRBoletoDuplicata WHERE numeroDuplicata = '" + boletoBancario.getDuplicataCache() +"'";
				stm.execute(sql);

				new LogDuplicataService().salvar(log);

			}
		
				sql = "INSERT INTO CRBoletoDuplicata SET codigoBarrasDuplicata = '" + (boletoBancario.getCodigoBarras()==null?"":boletoBancario.getCodigoBarras()) + "'" + 
						", idCliente =  " + boletoBancario.getClienteCache() + 
						", idEmpresa =  " + DsvConstante.getParametrosSistema().getInt("EMPRESA_LOCAL") +
						", linhaDigitavelDuplicata = '" + (boletoBancario.getLinhaDigitavel()==null?"":boletoBancario.getLinhaDigitavel()) + "'" +
						", nossoNumero = '" + boletoBancario.getNossoNumero() + "'" + 
						", numeroDuplicata = '" + boletoBancario.getDuplicataCache() + "'" + 
						", numeroParcela = '" + boletoBancario.getParcelaDuplicataCache() + "'" +
						", valorParcela = " + boletoBancario.getValorOriginal() + " " +
						", valorBoleto = " + (boletoBancario.getValorOriginal() + valorAcrecimo) + " ";;


						stm.execute(sql);

			if(con != null && !con.isClosed() && conIndependente==true)
				con.close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			if(con != null && !con.isClosed() && conIndependente==true)
				con.close();
			throw ex;
		}
	}
	/**
	 * Metodo que gerar o arquivo de remessa sicredi, que retorna um Map que cotem o nome do arquivo e Byte[] com os dados do arquivo
	 * @param idContaCorrente
	 * @param dataInicial
	 * @param dataFinal
	 * @return Map<String,Object> 
	 * @throws Exception
	 */
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Faturamento, Grupo.Gerente}, nomeAcao="Gerar Arqivo CNAB 2")
	public Map<String, Object> gerarArquivoCNAB400SicrediRetornaArquivo(Integer idContaCorrente, Date dataInicial, Date dataFinal) throws Exception {
		try
		{
			String caminhoArquivo = gerarArquivoCNAB400Sicredi(idContaCorrente, dataInicial, dataFinal);
			File arquivoGerado = new File(caminhoArquivo);
			byte[] array = Files.readAllBytes(arquivoGerado.toPath());
			Map<String, Object> retorno = new HashMap<String, Object>();
			retorno.put("nomeArquivo", arquivoGerado.getName());
			retorno.put("data", array);
			return retorno;
		}
		catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}
	}
	public boolean verficarBoletosSemRemessa() throws Exception{
		String data = IgnUtil.dateFormatSql.format(new Date());
		String where = " situacaoEnvio = 0 and dataGeracao <'"+data+"'";
		List<BoletoBancario> lista = this.listarPaginadoSqlLivreGenerico(Integer.MAX_VALUE, 1, where).getRecordList();
		return lista.size()>0;
	}
}
