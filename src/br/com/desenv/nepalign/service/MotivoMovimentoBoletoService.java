package br.com.desenv.nepalign.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao;
import br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao.Grupo;
import br.com.desenv.nepalign.model.BoletoBancario;
import br.com.desenv.nepalign.model.MotivoMovimentoBoleto;
import br.com.desenv.nepalign.model.MovimentoBoletoBancario;
import br.com.desenv.nepalign.persistence.MotivoMovimentoBoletoPersistence;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class MotivoMovimentoBoletoService extends GenericServiceIGN<MotivoMovimentoBoleto, MotivoMovimentoBoletoPersistence>
{

	public MotivoMovimentoBoletoService() 
	{
		super();
	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Crediario, Grupo.Gerente}, nomeAcao="Listar Motivos por Boleto")
	public List<MotivoMovimentoBoleto> listarMotivos(BoletoBancario boleto) throws Exception
	{
		return listarMotivos(boleto, null, null);
	}
	@IgnSegurancaAcao(gruposAutorizados={Grupo.Administrador, Grupo.Crediario, Grupo.Gerente}, nomeAcao="Listar Motivos por Boleto")
	public List<MotivoMovimentoBoleto> listarMotivos(BoletoBancario boleto,EntityManager manager, EntityTransaction transaction) throws Exception
	{
		Boolean transacaoIndependente = false;
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			MovimentoBoletoBancario movimentoCriterio = new MovimentoBoletoBancario();
			movimentoCriterio.setBoletoBancario(boleto);
			List<MovimentoBoletoBancario> listaDeMovimentos = new MovimentoBoletoBancarioService().listarPorObjetoFiltro(manager, movimentoCriterio, null, null, null);
			List<MotivoMovimentoBoleto> listadeMotivos = new ArrayList<MotivoMovimentoBoleto>();
			for (MovimentoBoletoBancario movimentoBoletoBancario : listaDeMovimentos)
			{
				MotivoMovimentoBoleto motivoCriterio = new MotivoMovimentoBoleto();
				motivoCriterio.setMovimentoBoletoBancario(movimentoBoletoBancario);
				listadeMotivos.addAll(new MotivoMovimentoBoletoService().listarPorObjetoFiltro(manager, motivoCriterio, null, null, null));

			}
			if(transacaoIndependente)
			{
				transaction.commit();
			}

			return listadeMotivos;

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			if(transacaoIndependente)
				transaction.rollback();


			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}

	}


}

