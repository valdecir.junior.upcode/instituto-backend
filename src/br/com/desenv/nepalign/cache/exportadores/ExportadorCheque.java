package br.com.desenv.nepalign.cache.exportadores;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.cache.constantes.Util;
import br.com.desenv.nepalign.model.ChequeRecebido;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ExportadorCheque 
{
	private static final StringBuffer linhas = new StringBuffer();
	
	public static void main(String[] args) throws Exception
	{
		//new ExportadorCheque().exportarParaArquivo(8, IgnUtil.dateFormat.parse("10/11/2016"), IgnUtil.dateFormat.parse("10/11/2016"));
		//new ExportadorCheque().exportarParaArquivo(Integer.parseInt(args[0x000]), IgnUtil.dateFormat.parse(args[0x001]), IgnUtil.dateFormat.parse(args[0x002]));
		
		System.out.println(TimeZone.getDefault().inDaylightTime(new Date()));
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			
			@Override
			public void run() 
			{
				Logger.getRootLogger().info("Finalizado!");
			}
		}));
	}
	
	@SuppressWarnings("unchecked")
	public byte[] exportarParaByte(final int idEmpresaFisica, final Date dataInicial, final Date dataFinal)
	{
		final EntityManager manager = ConexaoUtil.getEntityManager();
		
		try
		{
			String queryPedidoVenda = " SELECT chequeRecebido.* from `chequeRecebido` "; 
			queryPedidoVenda = queryPedidoVenda + " WHERE `chequeRecebido`.`idEmpresaFisica` = " + idEmpresaFisica + " AND `chequeRecebido`.`dataEmissao` BETWEEN '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' AND '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59';";

			final List<ChequeRecebido> listaChequeRecebido = manager.createNativeQuery(queryPedidoVenda, ChequeRecebido.class).getResultList();

			for(final ChequeRecebido chequeRecebido : listaChequeRecebido)
				registrarCheque(chequeRecebido);

			linhas.append("FIM");
			
			return linhas.toString().getBytes(Charset.forName("UTF-8"));
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			linhas.delete(0x000, linhas.length());
			manager.close();
		}
	}

	public void exportarParaArquivo(final int idEmpresaFisica, final Date dataInicial, final Date dataFinal)
	{ 
		try 
		{
			final FileWriter arquivo = new FileWriter(new File("F:/CHEQUE".concat(String.format("%02d", idEmpresaFisica)).concat(new SimpleDateFormat("ddMMYY").format(dataInicial))));
			arquivo.append(new String(exportarParaByte(idEmpresaFisica, dataInicial, dataFinal)));
			arquivo.flush();
			arquivo.close();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	private void registrarCheque(final ChequeRecebido chequeRecebido)
	{	
		linhas.append(String.valueOf(chequeRecebido.getId()).concat(Util.CACHE_SP)
				.concat(String.valueOf(chequeRecebido.getEmpresaFisica().getId())).concat(Util.CACHE_SP)
				.concat(chequeRecebido.getNumeroCheque()).concat(Util.CACHE_SP)
				.concat(String.valueOf(chequeRecebido.getValor())).concat(Util.CACHE_SP)
				.concat(String.valueOf(Util.dateToCache(chequeRecebido.getDataEmissao()))).concat(Util.CACHE_SP)
				.concat(String.valueOf(Util.dateToCache(chequeRecebido.getBomPara()))).concat(Util.CACHE_SP)
				.concat(chequeRecebido.getEmitente().contains("-") ? chequeRecebido.getEmitente().split("-")[0x00000001] : chequeRecebido.getEmitente()).concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(String.valueOf(chequeRecebido.getBanco().getCodigo().intValue())).concat(Util.CACHE_SP)
				.concat("0").concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat("0").concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(chequeRecebido.getCpfCnpjPortador().replace("\\.", "").replace(".", "").replace("-", "")).concat("\r\n"));
	}
}