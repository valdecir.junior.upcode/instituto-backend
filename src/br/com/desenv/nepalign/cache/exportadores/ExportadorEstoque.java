package br.com.desenv.nepalign.cache.exportadores;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager; 

import org.apache.log4j.Logger;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.cache.constantes.Globais;
import br.com.desenv.nepalign.cache.constantes.Util;
import br.com.desenv.nepalign.model.Cor;
import br.com.desenv.nepalign.model.EstoqueProduto;
import br.com.desenv.nepalign.model.Fornecedor;
import br.com.desenv.nepalign.model.GrupoProduto;
import br.com.desenv.nepalign.model.Marca;
import br.com.desenv.nepalign.model.OrdemProduto;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.model.SaldoEstoqueProduto;
import br.com.desenv.nepalign.model.Tamanho;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ExportadorEstoque 
{
	private static final StringBuffer linhas = new StringBuffer();
	
	public static void main(String[] args) throws Exception
	{
		new ExportadorEstoque().exportarParaArquivo(IgnUtil.dateFormat.parse(args[0x00]), IgnUtil.dateFormat.parse(args[0x01]));
		
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			
			@Override
			public void run() 
			{
				Logger.getRootLogger().info("Finalizado!");
			}
		}));
	}
	
	public void exportarParaArquivo(final Date dataInicial, final Date dataFinal)
	{ 
		final EntityManager manager = ConexaoUtil.getEntityManager();
		final HashMap<Integer, Marca> listaMarcas = new HashMap<Integer, Marca>();
		final HashMap<Integer, Produto> listaProdutos = new HashMap<Integer, Produto>();
		final HashMap<Integer, Cor> listaCor = new HashMap<Integer, Cor>();
		final HashMap<Integer, Tamanho> listaTamanho = new HashMap<Integer, Tamanho>();
		final HashMap<Integer, EstoqueProduto> listaEstoqueProduto = new HashMap<Integer, EstoqueProduto>();
		final HashMap<Integer, OrdemProduto> listaOrdemProduto = new HashMap<Integer, OrdemProduto>();
		final HashMap<Integer, Fornecedor> listaFornecedores = new HashMap<Integer, Fornecedor>();
		final HashMap<Integer, SaldoEstoqueProduto> listaSaldoEstoqueProduto = new HashMap<Integer, SaldoEstoqueProduto>();
		final HashMap<Integer, GrupoProduto> listaGrupos = new HashMap<Integer, GrupoProduto>();
		
		String querySaldoEstoqueProduto = " SELECT `saldoEstoqueProduto`.`*` from `saldoestoqueproduto` "; 
		querySaldoEstoqueProduto = querySaldoEstoqueProduto + " INNER JOIN `empresaFisica` ON `empresaFisica`.`idEmpresaFisica` = `saldoEstoqueProduto`.`idEmpresaFisica` and `saldoEstoqueProduto`.`mes` = `empresaFisica`.`mesAberto` and `saldoEstoqueProduto`.`ano` = `empresaFisica`.`anoAberto` ";
		querySaldoEstoqueProduto = querySaldoEstoqueProduto + " WHERE `saldoEstoqueProduto`.`ultimoEvento` BETWEEN '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' AND '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59';";
		
		@SuppressWarnings("unchecked")
		final List<SaldoEstoqueProduto> listaSaldoEstoqueProdutos = manager.createNativeQuery(querySaldoEstoqueProduto, SaldoEstoqueProduto.class).getResultList();
		
		Logger.getRootLogger().debug("Found " + listaSaldoEstoqueProdutos.size() + " entries.");
		
		for (final SaldoEstoqueProduto saldoEstoqueProduto : listaSaldoEstoqueProdutos) 
		{
			listaProdutos.put(saldoEstoqueProduto.getEstoqueProduto().getProduto().getId(), saldoEstoqueProduto.getEstoqueProduto().getProduto());
			listaMarcas.put(saldoEstoqueProduto.getEstoqueProduto().getProduto().getMarca().getId(), saldoEstoqueProduto.getEstoqueProduto().getProduto().getMarca());
			listaCor.put(saldoEstoqueProduto.getEstoqueProduto().getCor().getId(), saldoEstoqueProduto.getEstoqueProduto().getCor());
			listaTamanho.put(saldoEstoqueProduto.getEstoqueProduto().getTamanho().getId(), saldoEstoqueProduto.getEstoqueProduto().getTamanho());
			listaOrdemProduto.put(saldoEstoqueProduto.getEstoqueProduto().getOrdemProduto().getId(), saldoEstoqueProduto.getEstoqueProduto().getOrdemProduto());
			listaFornecedores.put(saldoEstoqueProduto.getEstoqueProduto().getProduto().getFornecedor().getId(), saldoEstoqueProduto.getEstoqueProduto().getProduto().getFornecedor());
			listaEstoqueProduto.put(saldoEstoqueProduto.getEstoqueProduto().getId(), saldoEstoqueProduto.getEstoqueProduto());
			listaSaldoEstoqueProduto.put(saldoEstoqueProduto.getId(), saldoEstoqueProduto);
			listaGrupos.put(saldoEstoqueProduto.getEstoqueProduto().getProduto().getGrupo().getId(), saldoEstoqueProduto.getEstoqueProduto().getProduto().getGrupo());
		}
		
		try 
		{
			gerarRegistroProduto(new ArrayList<Produto>(listaProdutos.values()));
			gerarRegistrosCor(new ArrayList<Cor>(listaCor.values()));
			gerarRegistrosEstoqueProduto(new ArrayList<EstoqueProduto>(listaEstoqueProduto.values()));
			gerarRegistrosGrupos(new ArrayList<GrupoProduto>(listaGrupos.values()));
			gerarRegistrosMarca(new ArrayList<Marca>(listaMarcas.values()));
			gerarRegistrosOrdem(new ArrayList<OrdemProduto>(listaOrdemProduto.values()));
			gerarRegistrosSaldoEstoqueProduto(new ArrayList<SaldoEstoqueProduto>(listaSaldoEstoqueProduto.values()));
		} 
		catch (Exception e1) 
		{
			e1.printStackTrace();
		}

		try 
		{
			final FileWriter arquivo = new FileWriter(new File("c:/registroscache_".concat(new SimpleDateFormat("dd MM yyyy hh mm ss").format(new Date())).concat(".txt")));
			arquivo.append(linhas.toString());
			arquivo.flush();
			arquivo.close();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unused")
	private void gerarRegistrosTamanho(final List<Tamanho> listaTamanhos) throws Exception
	{
		for (final Tamanho tamanho : listaTamanhos) 
			addRegistry(Util.CACHE_ACTION_INSERT, Globais.TAMANHO, String.valueOf(tamanho.getId()), tamanho.getDescricao());
	}	
	
	private void gerarRegistrosMarca(final List<Marca> listaMarcas) throws Exception
	{
		for (final Marca marca : listaMarcas) 
			addRegistry(Util.CACHE_ACTION_INSERT, Globais.MARCA, String.valueOf(marca.getId()), marca.getDescricao());
	}
	
	private void gerarRegistrosGrupos(final List<GrupoProduto> listaGrupos) throws Exception
	{
		for (final GrupoProduto grupoProduto : listaGrupos) 
			addRegistry(Util.CACHE_ACTION_INSERT, Globais.GRUPOPRODUTO, String.valueOf(grupoProduto.getId()), grupoProduto.getDescricao());
	}	
	
	private void gerarRegistrosOrdem(final List<OrdemProduto> listaOrdens) throws Exception
	{
		for (final OrdemProduto ordem : listaOrdens) 
			addRegistry(Util.CACHE_ACTION_INSERT, Globais.ORDEMPRODUTO, String.valueOf(ordem.getId()), ordem.getDescricao());
	}	
	
	private void gerarRegistrosCor(final List<Cor> listaCores) throws Exception
	{
		for (final Cor cor : listaCores) 
			addRegistry(Util.CACHE_ACTION_INSERT, Globais.COR, String.valueOf(cor.getId()), cor.getDescricaoCorEmpresa());
	}
	
	private void gerarRegistrosSaldoEstoqueProduto(List<SaldoEstoqueProduto> listaSaldos) throws Exception
	{
		String chaves;
		String infos;
		String chaves2;
		String infos2;
		
		for (final SaldoEstoqueProduto saldoEstoqueProduto : listaSaldos) 
		{
			chaves = String.valueOf(saldoEstoqueProduto.getEmpresaFisica().getId()).concat(Util.CACHE_SN).concat(String.valueOf(saldoEstoqueProduto.getEstoqueProduto().getId()));
			infos = saldoEstoqueProduto.getVenda() == null || saldoEstoqueProduto.getVenda().doubleValue() == 0  ? "0.0" : saldoEstoqueProduto.getVenda().toString();
			
			chaves2 = String.valueOf(saldoEstoqueProduto.getEmpresaFisica().getId()).concat(Util.CACHE_SN).concat(String.valueOf(saldoEstoqueProduto.getEstoqueProduto().getId())).concat(Util.CACHE_SN).concat(String.valueOf(saldoEstoqueProduto.getEmpresaFisica().getMesAberto()));
			
			if (saldoEstoqueProduto.getSaldo()==null)
				infos2 = "0".concat(Util.CACHE_SP).concat(String.valueOf(saldoEstoqueProduto.getCusto()));
			else if (saldoEstoqueProduto.getCusto()==null)
				infos2 = String.valueOf(saldoEstoqueProduto.getSaldo()).concat(Util.CACHE_SP).concat("0");				
			else
				infos2 = String.valueOf(saldoEstoqueProduto.getSaldo()).concat(Util.CACHE_SP).concat(String.valueOf(saldoEstoqueProduto.getCusto()));
			
			addRegistry(Util.CACHE_ACTION_INSERT, Globais.SALDOESTOQUEPRODUTO, chaves, infos);
			addRegistry(Util.CACHE_ACTION_INSERT, Globais.SALDOESTOQUEPRODUTO, chaves2, infos2);
		}
	}
	
	private void gerarRegistrosEstoqueProduto(List<EstoqueProduto> listaEstoqueProduto) throws Exception
	{
		for (final EstoqueProduto estoqueProduto : listaEstoqueProduto) 
		{
			addRegistry(Util.CACHE_ACTION_INSERT, Globais.ESTOQUEPRODUTO_P, estoqueProduto.getId().toString(), estoqueProduto.getProduto().getId() + Util.CACHE_SP + estoqueProduto.getOrdemProduto().getId() + Util.CACHE_SP + estoqueProduto.getCor().getId() + Util.CACHE_SP + estoqueProduto.getTamanho().getId());
			addRegistry(Util.CACHE_ACTION_INSERT, Globais.ESTOQUEPRODUTO_R, estoqueProduto.getProduto().getId() + Util.CACHE_SN + estoqueProduto.getOrdemProduto().getId() + Util.CACHE_SN + estoqueProduto.getCor().getId() + Util.CACHE_SN + estoqueProduto.getTamanho().getId(), estoqueProduto.getId().toString());
		}
	}
	
	private void gerarRegistroProduto(List<Produto> listaProdutos) throws Exception
	{
		String chaves;
		String infos;

		for (final Produto produto : listaProdutos) 
		{
			chaves = produto.getId().toString();
			infos = produto.getNomeProduto().concat(Util.CACHE_SP)
					.concat(Util.CACHE_SP)
					.concat(String.valueOf(produto.getMarca().getId())).concat(Util.CACHE_SP)
					.concat(String.valueOf(produto.getGrupo().getId())).concat(Util.CACHE_SP)
					.concat(String.valueOf(produto.getFornecedor().getId())).concat(Util.CACHE_SP)
					.concat(String.valueOf(produto.getUnidadeMedida().getSigla())).concat(Util.CACHE_SP)
					.concat(Util.CACHE_SP)
					.concat(Util.CACHE_SP)
					.concat(Util.CACHE_SP)
					.concat(Util.CACHE_SP)
					.concat(Util.CACHE_SP)
					.concat(Util.CACHE_SP)
					.concat(Util.CACHE_SP)
					.concat(Util.CACHE_SP)
					.concat(Util.CACHE_SP)
					.concat(Util.CACHE_SP)
					.concat(Util.CACHE_SP)
					.concat(produto.getCodigoNcm() == null || produto.getCodigoNcm().trim().equals("99999999") ? "0" : produto.getCodigoNcm().trim());
			addRegistry(Util.CACHE_ACTION_INSERT, Globais.PRODUTO, chaves, infos);
		}
	}
	
	private void addRegistry(final String tipo, final String global, final String chaves, final String infos) throws Exception
	{
		linhas.append(tipo.concat(Util.CACHE_SP).concat(global).concat(Util.CACHE_SP).concat(chaves).concat(Util.CACHE_SP).concat(infos).concat("\r\n"));
	}
}
