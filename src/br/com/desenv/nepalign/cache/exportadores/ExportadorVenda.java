package br.com.desenv.nepalign.cache.exportadores;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.cache.constantes.Util;
import br.com.desenv.nepalign.model.ItemPedidoVenda;
import br.com.desenv.nepalign.model.PedidoVenda;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ExportadorVenda 
{
	private static final StringBuffer linhas = new StringBuffer();
	
	public static void main(String[] args) throws Exception
	{
		//new ExportadorVenda().exportarParaArquivo(5, IgnUtil.dateFormat.parse("09/09/2016"), IgnUtil.dateFormat.parse("09/09/2016"));
		new ExportadorVenda().exportarParaArquivo(Integer.parseInt(args[0x000]), IgnUtil.dateFormat.parse(args[0x001]), IgnUtil.dateFormat.parse(args[0x002]));
		
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			
			@Override
			public void run() 
			{
				Logger.getRootLogger().info("Finalizado!");
			}
		}));
	}
	
	@SuppressWarnings("unchecked")
	public byte[] exportarParaByte(final int idEmpresaFisica, final Date dataInicial, final Date dataFinal)
	{
		final EntityManager manager = ConexaoUtil.getEntityManager();
		
		try
		{
			String queryPedidoVenda = " SELECT pedidoVenda.* from `pedidoVenda` "; 
			queryPedidoVenda = queryPedidoVenda + " WHERE `pedidoVenda`.`idSituacaoPedidoVenda` <> 5 AND `pedidoVenda`.`idEmpresaFisica` = " + idEmpresaFisica + " AND `pedidoVenda`.`dataVenda` BETWEEN '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' AND '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59';";

			final List<PedidoVenda> listaPedidoVenda = manager.createNativeQuery(queryPedidoVenda, PedidoVenda.class).getResultList();

			for(final PedidoVenda pedidoVenda : listaPedidoVenda)
			{
				registrarPedidoVenda(pedidoVenda);
				
				for(final ItemPedidoVenda itemPedidoVenda : (List<ItemPedidoVenda>) manager.createNativeQuery("SELECT * FROM `itemPedidoVenda` WHERE `itemPedidoVenda`.`idPedidoVenda` = " + pedidoVenda.getId(), ItemPedidoVenda.class).getResultList())
					registrarItemPedidoVenda(itemPedidoVenda);
			}
			
			linhas.append("FIM");
			
			return linhas.toString().getBytes(Charset.forName("UTF-8"));
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			linhas.delete(0x000, linhas.length());
			manager.close();
		}
	}

	public void exportarParaArquivo(final int idEmpresaFisica, final Date dataInicial, final Date dataFinal)
	{ 
		try 
		{
			final FileWriter arquivo = new FileWriter(new File("F:/ESTOQUE".concat(String.format("%02d", idEmpresaFisica)).concat(new SimpleDateFormat("ddMMYY").format(dataInicial))));
			arquivo.append(new String(exportarParaByte(idEmpresaFisica, dataInicial, dataFinal)));
			arquivo.flush();
			arquivo.close(); 
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	private void registrarPedidoVenda(final PedidoVenda pedidoVenda)
	{	
		linhas.append(pedidoVenda.getNumeroControle().concat(Util.CACHE_SP)
				.concat("0").concat(Util.CACHE_SP)
				.concat(String.valueOf(Util.dateToCache(pedidoVenda.getDataVenda()))).concat(Util.CACHE_SP)
				.concat(String.valueOf(pedidoVenda.getVendedor().getCodigoVendedorCache())).concat(Util.CACHE_SP)
				.concat("0").concat(Util.CACHE_SP)
				.concat(String.valueOf(pedidoVenda.getValorTotalPedido())).concat(Util.CACHE_SP)
				.concat(pedidoVenda.getTipoMovimentacaoEstoque().getId() == 0x002 ? "1" : "2").concat(Util.CACHE_SP)
				.concat(pedidoVenda.getPlanoPagamento().getCodigoPlano()).concat(Util.CACHE_SP)
				.concat("0").concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat("").concat(Util.CACHE_SP) // % desconto
				.concat(pedidoVenda.getCliente().getNome())
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(String.valueOf(pedidoVenda.getEmpresaFisica().getId())).concat("\r\n"));
	}

	private void registrarItemPedidoVenda(final ItemPedidoVenda itemPedidoVenda)
	{	
		linhas.append(itemPedidoVenda.getPedidoVenda().getNumeroControle().concat(Util.CACHE_SP)
				.concat(String.valueOf(itemPedidoVenda.getItemSequencial())).concat(Util.CACHE_SP)
				.concat(String.valueOf(itemPedidoVenda.getEstoqueProduto().getId())).concat(Util.CACHE_SP)
				.concat(String.valueOf(itemPedidoVenda.getQuantidade().intValue())).concat(Util.CACHE_SP)
				.concat(String.valueOf(itemPedidoVenda.getPrecoTabela())).concat(Util.CACHE_SP)
				.concat(String.valueOf(IgnUtil.roundUpDecimalPlaces(itemPedidoVenda.getPrecoVenda() * itemPedidoVenda.getQuantidade(), 0x002))).concat(Util.CACHE_SP)
				.concat(String.valueOf(itemPedidoVenda.getCustoProduto()).replace("null", "0.0")).concat(Util.CACHE_SP)
				.concat(itemPedidoVenda.getPedidoVenda().getTipoMovimentacaoEstoque().getId() == 0x0005 ? (itemPedidoVenda.getEntradaSaida().equalsIgnoreCase("E") ? "+" : "-") : "").concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP) 
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(Util.CACHE_SP)
				.concat(itemPedidoVenda.getEstoqueProduto().getProduto().getNomeProduto()).concat(Util.CACHE_SP)
				.concat(itemPedidoVenda.getEstoqueProduto().getProduto().getMarca().getDescricao()).concat(Util.CACHE_SP)
				.concat(itemPedidoVenda.getEstoqueProduto().getProduto().getGrupo().getDescricao()));
		
		linhas.append("\r\n");
	}

	public void exportarParaBase(final PedidoVenda pedidoVenda) throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		Connection conn = ConexaoUtil.getConexaoPadraoCACHE();
		
		@SuppressWarnings("unchecked")
		List<ItemPedidoVenda> listaItemPedidoVenda = (List<ItemPedidoVenda>) manager.createNativeQuery("SELECT * FROM itemPedidoVenda WHERE idPedidoVenda = " + pedidoVenda.getId(), ItemPedidoVenda.class).getResultList();
		
		PreparedStatement ps = conn.prepareStatement("INSERT INTO IMPORTACAO.TBPEDIDOVENDA "
				+ " (ANOMES, CODIGOCLIENTE, CODIGOEMPRESA, CODIGOPLANO, CODIGOVENDEDOR, DATAVENDA, TIPO, VALOTOTALVENDA, NUMEROCONTROLE) "
				+ " values(?, ?, ?, ?, ?, ?, ?, ?, ?)");
		
		ps.setString(1, IgnUtil.dateFormatAnoMes.format(pedidoVenda.getDataVenda()));
		ps.setString(2, "0");
		ps.setInt(3, pedidoVenda.getEmpresaFisica().getId());
		ps.setString(4, pedidoVenda.getPlanoPagamento().getCodigoPlano());
		ps.setString(5, String.valueOf(pedidoVenda.getVendedor().getCodigoVendedorCache()));
		ps.setString(6, String.valueOf(Util.dateToCache(pedidoVenda.getDataVenda())));
		ps.setString(7, pedidoVenda.getTipoMovimentacaoEstoque().getId() == 5 ? "2" : "1"); //venda, troca?
		ps.setString(8, String.valueOf(pedidoVenda.getValorTotalPedido()));
		ps.setString(9, pedidoVenda.getNumeroControle());
		ps.executeUpdate();
		
		/*conn.createStatement().executeUpdate("INSERT INTO IMPORTACAO.TBPEDIDOVENDA "
				+ " (ANOMES, CODIGOCLIENTE, CODIGOEMPRESA, CODIGOPLANO, CODIGOVENDEDOR, DATAVENDA, TIPO, VALOTOTALVENDA, NUMEROCONTROLE) "
				+ " values('201707', '0', 1, '1', '101', '63201', '1', '50.00', '1710515454') ");*/
		
		for(final ItemPedidoVenda itemPedidoVenda:listaItemPedidoVenda)
		{
			PreparedStatement psItem = conn.prepareStatement("INSERT INTO IMPORTACAO.TBITEMPEDIDOVENDA (EMPRESA, ANOMES, NUMEROCONTROLE, ITEM, CODIGOBARRAS, QUANTIDADE, VALORTABELAUNITARIO, VALORVENDATOTALITEM, VALORUNITARIOVENDA) "
					+ " values(?, ?, ?, ?, ?, ?, ?, ?, ?) ");
			
			psItem.setInt(1, pedidoVenda.getEmpresaFisica().getId());
			psItem.setString(2, IgnUtil.dateFormatAnoMes.format(pedidoVenda.getDataVenda()));
			psItem.setString(3, pedidoVenda.getNumeroControle());
			psItem.setString(4, String.valueOf(itemPedidoVenda.getItemSequencial()));
			psItem.setString(5, String.valueOf(itemPedidoVenda.getEstoqueProduto().getId()));
			psItem.setString(6, String.valueOf(itemPedidoVenda.getQuantidade().intValue()));
			psItem.setString(7, String.valueOf(itemPedidoVenda.getPrecoTabela()));
			psItem.setString(8, String.valueOf(itemPedidoVenda.getPrecoVenda()));
			psItem.setString(9, String.valueOf(itemPedidoVenda.getPrecoVenda() / itemPedidoVenda.getQuantidade()));
			psItem.executeUpdate();
		}
		
		/*conn.createStatement().executeUpdate("INSERT INTO IMPORTACAO.TBITEMPEDIDOVENDA (EMPRESA, ANOMES, NUMEROCONTROLE, ITEM, CODIGOBARRAS, QUANTIDADE, VALORTABELAUNITARIO, VALORVENDATOTALITEM, VALORUNITARIOVENDA) "
				+ " values(1, '201707', '1710515454', '1', '130652', '1', '150', '100', '100') ");*/
		
		manager.close();
		conn.close();
	}
}