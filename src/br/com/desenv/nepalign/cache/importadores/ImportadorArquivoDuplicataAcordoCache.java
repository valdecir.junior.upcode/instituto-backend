package br.com.desenv.nepalign.cache.importadores;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import br.com.desenv.nepalign.cache.constantes.Util;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ImportadorArquivoDuplicataAcordoCache extends ImportadorCache
{	
	public static void main(String[] args)
	{
		final String diretorioBase = args.length > 0x00 ? args[0x00] : "D:\\Desenv\\ARQ_CONV\\";
		
		new ImportadorArquivoDuplicataAcordoCache().run(diretorioBase);
	}
	
	public static HashMap<String, Integer> deParaNumero = new HashMap<>();
	
	public static synchronized Integer getNumeroDuplicata(Connection connection, String numero, Integer idCliente) throws Exception
	{
		synchronized (connection) 
		{
			PreparedStatement ps = connection.prepareStatement("SELECT numeroNovo FROM deparaduplicata where numeroAntigo = ? and idCliente = ?");
			ps.setString(1, numero);
			ps.setInt(2, idCliente);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next())
			{
				Integer numeroNovo = rs.getInt("numeroNovo");
				
				rs.close();
				ps.close();
				return numeroNovo;
			}
		}
		throw new Exception("Não foi possível encontrar o numero de documento novo para " + numero);
	}
	
	enum FILE_METADATA_DUPLICATAACORDO
	{
		UNK((short) 0),
		EMPRESA((short) 1),
		CLIENTE((short) 2),
		DUPLICATA_NOVA((short) 3),
		DUPLICATA_ANTIGA((short) 4),
		PARCELA_ANTIGA((short) 5);
		
		private final short index;
		
		FILE_METADATA_DUPLICATAACORDO(final short index)
		{
			this.index = index;
		}
		
		short getIndex()
		{
			return index;
		}
	}
	
	public void run(final String diretorioBase)
	{
		EntityManager manager = ConexaoUtil.getFactory("importacao", "importacao!123").createEntityManager();
		try 
		{
			manager.getTransaction().begin();
			clean(manager);
			manager.getTransaction().commit();
			
			importarAgenda(manager, readFile(diretorioBase.concat("DUPLACORDOSAI.TXT")));
		}
		catch(InterruptedException ie)
		{
			ie.printStackTrace();
			manager.getTransaction().rollback();
		}
		catch (IOException e) 
		{
			manager.getTransaction().rollback();
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			manager.close();
		}
	}
	
	private void clean(final EntityManager manager)
	{
		manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=0;").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE duplicataacordo;").executeUpdate();
		manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=1;").executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	public void importarAgenda(final EntityManager manager, final String data[]) throws InterruptedException
	{
		Logger.getLogger(ImportadorArquivoDuplicataAcordoCache.this.getClass()).info("Iniciando bloco de importação de Duplicata Acordo");
		
		try
		{
			Connection connection = ConexaoUtil.getConexaoPadrao("importacao", "importacao!123");
			PreparedStatement ps = connection.prepareStatement("INSERT INTO duplicataacordo (idDuplicataNova, idDuplicataAntigaParcela, idEmpresaFisica, idCliente, numeroNovaDuplicata," + 
					"numeroDuplicataAntiga, numeroParcelaDuplicataAntiga)" +
					"VALUES((select idduplicata from duplicata where numeroduplicata = ?), (select idduplicataparcela from duplicataparcela where numeroduplicata = ? and numeroParcela = ?), ?, ?, ?, ?, ?); ");
			
			PreparedStatement psAcordo = connection.prepareStatement("UPDATE duplicataparcela set acordo = 'S' WHERE idDuplicata = (SELECT idDuplicata FROM duplicata WHERE numeroDuplicata = ? and idCliente = ?)");
			 
			for(int index = 1; index < data.length; index++)
			{
				final String[] metadata = data[index].split(Util.CACHE_FILE_SEPARATOR, -0x01);
				
				Integer duplicataNova;
				Integer duplicataAntiga;
				Integer idCliente = Integer.parseInt(metadata[FILE_METADATA_DUPLICATAACORDO.CLIENTE.getIndex()].trim());
				Integer parcela = Integer.parseInt(metadata[FILE_METADATA_DUPLICATAACORDO.PARCELA_ANTIGA.getIndex()].trim());
				
				try
				{
					duplicataNova = Integer.parseInt(metadata[FILE_METADATA_DUPLICATAACORDO.DUPLICATA_NOVA.getIndex()].trim());
				}
				catch(Exception ex)
				{
					duplicataNova = getNumeroDuplicata(connection, metadata[FILE_METADATA_DUPLICATAACORDO.DUPLICATA_NOVA.getIndex()].trim(), idCliente);
				}
				
				try
				{
					duplicataAntiga = Integer.parseInt(metadata[FILE_METADATA_DUPLICATAACORDO.DUPLICATA_ANTIGA.getIndex()].trim());
				}
				catch(Exception ex)
				{
					duplicataAntiga = getNumeroDuplicata(connection, metadata[FILE_METADATA_DUPLICATAACORDO.DUPLICATA_ANTIGA.getIndex()].trim(), idCliente);	
				}
				
				ps.setInt(1, duplicataNova);
				ps.setInt(2, duplicataAntiga);
				ps.setInt(3, Integer.parseInt(metadata[FILE_METADATA_DUPLICATAACORDO.PARCELA_ANTIGA.getIndex()].trim()));
				ps.setInt(4, Integer.parseInt(metadata[FILE_METADATA_DUPLICATAACORDO.EMPRESA.getIndex()].trim()));
				ps.setInt(5, idCliente);
				ps.setInt(6, duplicataNova);
				ps.setInt(7, duplicataAntiga);
				ps.setInt(8, parcela);
				
				ps.executeUpdate();
				
				psAcordo.setInt(1, duplicataNova);
				psAcordo.setInt(2, idCliente);
				psAcordo.executeUpdate();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

	    Logger.getLogger(ImportadorArquivoDuplicataAcordoCache.this.getClass()).info("Bloco de importação de Duplicata Acordo finalizado!");
	}
}