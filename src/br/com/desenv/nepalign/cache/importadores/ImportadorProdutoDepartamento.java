package br.com.desenv.nepalign.cache.importadores;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.desenv.nepalign.model.Departamento;
import br.com.desenv.nepalign.model.DepartamentoProduto;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.model.SituacaoEnvioLV;
import br.com.desenv.nepalign.service.DepartamentoProdutoService;
import br.com.desenv.nepalign.util.*;

import br.com.desenv.nepalign.service.*;

public class ImportadorProdutoDepartamento 
{
	public static void main(String[] args) throws Exception{
		ImportadorProdutoDepartamento i = new ImportadorProdutoDepartamento();
		
		i.init(null);
		
	}
	public void init(Connection conCache) throws Exception
	{
 
		try 
		{
			if (conCache==null)
			{
				try 
				{
					Class.forName("com.intersys.jdbc.CacheDriver");
					conCache = DriverManager.getConnection(
							DsvConstante.getInstanceParametrosSistema().getStrConexaoCache(), DsvConstante.getInstanceParametrosSistema().getUsuarioCache(), DsvConstante.getInstanceParametrosSistema().getSenhaCache());
				} 
				catch (Exception chEx) 
				{
					throw new Exception("Falha na conexão com o caché."
							+ chEx.getMessage());
				}
			}
			//Faz as chamadas
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			System.out.println("ImportadorProdutoDepartamento - Hora inicial: " + sdf.format(new Date()));
			System.out.println("Produtos Departamento: "  + importarProdutoDepartamento(conCache));
			System.out.println("ImportadorProdutoDepartamento - Hora inicial: " + sdf.format(new Date()));
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			try
			{if (conCache!=null)
				conCache.close();
			}catch(Exception ex)
			{ex.printStackTrace();
				throw ex;}		
		}	
	}
	
	private int importarProdutoDepartamento(Connection conCache) throws Exception
	{
		Statement stm = null;
		ResultSet rs = null;
		DepartamentoProdutoService produtoDepartamentoService = new DepartamentoProdutoService();
		DepartamentoProduto produtoDepartamento;
		ProdutoService produtoService = new ProdutoService();
		Produto produto;
		Departamento departamento = null;
		Departamento criterio = null;
		DepartamentoProduto pdCriterio = null;
		DepartamentoService depService = new DepartamentoService();	
		SituacaoEnvioLV situacaoEnvio = new SituacaoEnvioLV();
		
		DepartamentoProduto criterioDP = new DepartamentoProduto();
		Produto criterioProduto = new Produto();
		Departamento criterioDepartamento = new Departamento();
		
		int cont=0;
		
		try
		{
			stm = conCache.createStatement();
			rs = stm.executeQuery("SELECT * FROM CEREFSUBDEP ");
			while(rs.next())
			{
				int subDep = rs.getInt("SUBDEPAR");
				int idProduto = rs.getInt("REFERENCIA");
				
				produto = produtoService.recuperarPorId(idProduto);
				if (produto!=null)
				{
					criterio = new Departamento();
					criterio.setIdSubDepCache(subDep);
					
					List<Departamento> deps = depService.listarPorObjetoFiltro(criterio);

					if (deps.isEmpty()==false)
					{
						for (int contDeps=0; contDeps<deps.size();contDeps++)
						{
							try
							{
								departamento = deps.get(contDeps);
								produtoDepartamento = new DepartamentoProduto();
								produtoDepartamento.setDepartamento(departamento.getDepartamentoPai());
								produtoDepartamento.setProduto(produto);
								//produtoDepartamento.setUltimaAlteracao(null);
								situacaoEnvio.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVProntoEnvio());
								produtoDepartamento.setSituacaoEnvioLV(situacaoEnvio);
			
								criterioProduto.setId(produtoDepartamento.getProduto().getId());
								criterioDepartamento.setId(produtoDepartamento.getDepartamento().getId());
								criterioDP.setProduto(criterioProduto);
								criterioDP.setDepartamento(criterioDepartamento);
								
								if (produtoDepartamentoService.listarPorObjetoFiltro(criterioDP)==null)
								{
									produtoDepartamentoService.salvar(produtoDepartamento);
									cont++;
									//para descobriri quais departamentos serao mesmo usados
									departamento.setSituacaoEnvioLV(situacaoEnvio);
									departamento.setAtivo("S");								
									depService.salvar(departamento);
								}
							}
							catch(Exception e)
							{
								e.printStackTrace();
								System.out.println("Erro ao inserir produto no departamento " + rs.getInt("REFERENCIA") + " "  + rs.getInt("SUBDEPAR"));
							}
						}
					}
				}
			}
			//System.out.println("Produtos departamento: " + cont);
			return cont;
		}
		catch(Exception e)
		{
			throw e;
		}
		finally
		{
			try
			{if (rs!=null)
				rs.close();
			if (stm!=null)
				stm.close();
			}catch(Exception ex)
			{ex.printStackTrace();
				throw ex;}		
		}				
		
	}
}
