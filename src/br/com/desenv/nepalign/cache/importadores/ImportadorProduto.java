package br.com.desenv.nepalign.cache.importadores;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;


import br.com.desenv.nepalign.model.Cor;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.EstoqueProduto;
import br.com.desenv.nepalign.model.Genero;
import br.com.desenv.nepalign.model.GrupoProduto;
import br.com.desenv.nepalign.model.LogImportacaoCache;
import br.com.desenv.nepalign.model.Marca;
import br.com.desenv.nepalign.model.OrdemProduto;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.model.RegimeTributario;
import br.com.desenv.nepalign.model.SaldoEstoqueProduto;
import br.com.desenv.nepalign.model.SituacaoEnvioLV;
import br.com.desenv.nepalign.model.SituacaoTributaria;
import br.com.desenv.nepalign.model.Tamanho;
import br.com.desenv.nepalign.model.TipoItem;
import br.com.desenv.nepalign.model.UnidadeMedida;
import br.com.desenv.nepalign.service.CorService;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.EstoqueProdutoService;
import br.com.desenv.nepalign.service.GrupoProdutoService;
import br.com.desenv.nepalign.service.LogImportacaoCacheService;
import br.com.desenv.nepalign.service.MarcaService;
import br.com.desenv.nepalign.service.OrdemProdutoService;
import br.com.desenv.nepalign.service.ProdutoService;
import br.com.desenv.nepalign.service.SaldoEstoqueProdutoService;
import br.com.desenv.nepalign.service.TamanhoService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.util.DsvProcessamentoLVException;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;


public class ImportadorProduto 
{
	public static void main(String[] args) throws Exception
	{
		ImportadorProduto i = new ImportadorProduto();
		i.init(null,1);
	}
	
	private Hashtable<Integer,SaldoEstoqueProduto> listaProdutos = new Hashtable<Integer, SaldoEstoqueProduto>();
	private ArrayList<SaldoEstoqueProduto> listaEstoque = new ArrayList<SaldoEstoqueProduto>();
	private ArrayList<EstoqueProduto> listaCodigoBarras = new ArrayList<EstoqueProduto>();
	private LogImportacaoCacheService logCache = null;
	private Hashtable<Integer,Integer> listaProdutosComErro = new Hashtable<Integer, Integer>();
	
	public static int CODIGOBARRAS_PRODUTO = 1;
	public static int PRODUTO = 2;
	
	int contProdutosNovos = 0;
	int contProdutosAlterados = 0;
		
	public void init(Connection conCache, int tipoImportacao) throws Exception
	{
		logCache = new LogImportacaoCacheService();
		LogImportacaoCache log = new LogImportacaoCache();
		logCache.excluirTodosPorIdSituacao(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoImportProdutos());	
		logCache.excluirTodosPorIdSituacao(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoImportCodBarras());
		try 
		{
			if (conCache==null)
			{
				try 
				{
					Class.forName("com.intersys.jdbc.CacheDriver");
					conCache = DriverManager.getConnection(
							DsvConstante.getInstanceParametrosSistema().getStrConexaoCache(), DsvConstante.getInstanceParametrosSistema().getUsuarioCache(), DsvConstante.getInstanceParametrosSistema().getSenhaCache());
				} 
				catch (Exception chEx) 
				{
					throw new Exception("Falha na conexão com o caché."
							+ chEx.getMessage());
				}
			}
			//Faz as chamadas
			
			if (tipoImportacao==1)
			{
				importarEstoqueCodigoBarras(conCache);
			}
			else if (tipoImportacao==2)
			{
				importarProdutosAlterados(conCache);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			try
			{if (conCache!=null)
				conCache.close();
			}catch(Exception ex)
			{ex.printStackTrace();
				throw ex;}		
		}	
	}
	
	public void importarEstoqueCodigoBarras(Connection conCache) throws Exception
	{
		Statement stm = null;
		ResultSet rs = null;
		SaldoEstoqueProdutoService ecbService = new SaldoEstoqueProdutoService();
		EstoqueProdutoService codigoBarrasService = new EstoqueProdutoService();
		SaldoEstoqueProduto ecb;
		SaldoEstoqueProduto ecbExistente;
		SaldoEstoqueProduto critEcb;
		EstoqueProduto codBarras;
		EstoqueProduto critCodBarras;
		EmpresaFisicaService empresaService = new EmpresaFisicaService();
		ProdutoService produtoService = new ProdutoService();
		Produto p = null;
		EmpresaFisica empresa = null;
		String sql = "";
		String campoQT;
		String campoCusto;
		Integer referencia;
		String idProduct;
		Produto produto = new Produto();
		Cor cor = new Cor();
		CorService corService = new CorService();
		Tamanho tamanho = new Tamanho();
		TamanhoService tamanhoService = new TamanhoService();
		SituacaoEnvioLV situacaoEnvio = new SituacaoEnvioLV();
		OrdemProduto ordem = new OrdemProduto();
		OrdemProdutoService ordemService = new OrdemProdutoService();
		EstoqueProduto estoqueProduto = new EstoqueProduto();
		List<SaldoEstoqueProduto> resEcb;
		EstoqueProduto criterioEp = new EstoqueProduto();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		System.out.println("importarEstoqueCodigoBarras - Hora inicial: " + sdf.format(new Date()));
		try
		{
			//DsvUtilImagens utilImagens = new DsvUtilImagens();			
			empresa = empresaService.recuperarPorId(DsvConstante.getInstanceParametrosSistema().getIdEmpresaLV());
			//Hashtable<String, ArrayList<Integer>> refCorArquivos = utilImagens.buscaReferenciaFotosCores(DsvConstante.getInstanceParametrosSistema().getCaminhoAbsolutoFotos());
			
			campoCusto = "Custo"+ajustaStr(empresa.getMesAberto()+"");
			campoQT = "Qt"+ajustaStr(empresa.getMesAberto()+"");
			
			sql = sql + "select ref.codigo codigo, ref.tamCodigo tamCodigo, ref.corCodigo corCodigo, ref.tipCodigo tipCodigo,"; 
			sql = sql + "ref.codigoID codigoID, est."+campoCusto+ " " + campoCusto + ", ";
			sql = sql +"est.EmpCodigo empCodigo, est.precoVenda precoVenda, est."+ campoQT+ " " + campoQT + " ";
			sql = sql + "FROM CEProdutoRef ref ";
			sql = sql + "INNER JOIN CeProdutoPV est ON est.CodigoID = ref.codigoID ";
			sql = sql + "where est.EmpCodigo = " + empresa.getId() + " AND est."+ campoQT+ ">=0 and ref.corCodigo<>0";
			//sql = sql + " AND ref.codigo=56207";
			
			stm = conCache.createStatement();
			rs = stm.executeQuery(sql);
			int cont=0;
			int contSalva=0;
			while(rs.next())
			{
				//para os casos de inconsistencia
				if (rs.getBigDecimal("precoVenda")==null || rs.getBigDecimal("precoVenda").doubleValue()==0)
				{
					if (rs.getInt(campoQT)>0)
					{
						logCache.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoImportProdutos(), 1, "Produto com estoque e preço zerado. CodigoBarras: " + rs.getInt("codigoID") + " Referencia: " + rs.getInt("codigo"));
					}
				}
				else
				{
					try
					{
						criterioEp = new EstoqueProduto();
						Produto pCriterio = new Produto();
						cor = new Cor();
						cor.setCodigo(rs.getInt("corCodigo"));
						tamanho = new Tamanho();
						tamanho.setCodigo(rs.getInt("tamCodigo"));
						ordem = new OrdemProduto();
						ordem.setCodigo(rs.getInt("tipCodigo"));
						pCriterio.setCodigoProduto(""+rs.getInt("codigo"));
						pCriterio = produtoService.listarPorObjetoFiltro(pCriterio).get(0);
						
						criterioEp.setProduto(pCriterio);
						
						codBarras = codigoBarrasService.listarPorObjetoFiltro(criterioEp).get(0);
					}
					catch(Exception exCod)
					{
						codBarras=null; 
					}
					if (codBarras==null)
					{
						produto = new Produto();
						produto.setCodigoProduto(rs.getInt("codigo")+"");
						//produto = produtoService.listarPorObjetoFiltro(produto).get(0);
						cor = corService.listarPorObjetoFiltro(cor).get(0);
						tamanho = tamanhoService.listarPorObjetoFiltro(tamanho).get(0);
						ordem = ordemService.listarPorObjetoFiltro(ordem).get(0);
						situacaoEnvio.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVAguardandoAprov());
						codBarras = new EstoqueProduto();
						//codBarras.setId(rs.getInt("codigoID"));
						codBarras.setCodigoBarras(""+rs.getInt("codigoID"));
						codBarras.setCor(cor);
						codBarras.setOrdemProduto(ordem);
						codBarras.setProduto(produto);
						codBarras.setSituacaoEnvioLV(situacaoEnvio);
						codBarras.setTamanho(tamanho);
						//codBarras.set(new Date());
					}				
					ecb=null;
					critEcb = new SaldoEstoqueProduto();
					estoqueProduto = new EstoqueProduto();
					estoqueProduto.setCodigoBarras(rs.getInt("codigoID")+"");
					critEcb.setEstoqueProduto(estoqueProduto);
					critEcb.setEmpresaFisica(empresa);
					critEcb.setMes(empresa.getMesAberto());
					critEcb.setAno(empresa.getAnoAberto());
					try
					{
						resEcb = ecbService.listarPorObjetoFiltro(critEcb);
						if (resEcb.isEmpty()==false)
						{
							ecbExistente = resEcb.get(0);
							if (rs.getInt(campoQT)!=ecbExistente.getSaldo().intValue() || 
							rs.getDouble("precoVenda")!=ecbExistente.getVenda().doubleValue() ||
							rs.getDouble(campoCusto)!=ecbExistente.getCusto().doubleValue())
							{
								ecb = ecbExistente;
								ecb.setSaldo(rs.getDouble(campoQT));
								ecb.setVenda(rs.getDouble("precoVenda"));
								ecb.setCusto(rs.getDouble(campoCusto));
								ecb.setEstoqueProduto(estoqueProduto);
								situacaoEnvio.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVProntoEnvio());
								ecb.setSituacaoEnvioLV(situacaoEnvio);
							}
						}
						else
						{
							ecb = new SaldoEstoqueProduto();
							ecb.setAno(empresa.getAnoAberto());
							ecb.setId(rs.getInt("codigoID"));
							ecb.setCusto(rs.getDouble(campoCusto));
							ecb.setDataUltimaAlteracao(null);
							ecb.setEmpresaFisica(empresa);
							//ecb.setIdProdutoscv(0);
							ecb.setMes(empresa.getMesAberto());
							ecb.setSaldo(rs.getDouble(campoQT));
							ecb.setVenda(rs.getDouble("precoVenda"));
							ecb.setEstoqueProduto(estoqueProduto);
							situacaoEnvio.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVProntoEnvio());
							ecb.setSituacaoEnvioLV(situacaoEnvio);
						}
					}
					catch(Exception execb)
					{
						execb.printStackTrace();
						throw new Exception("Erro ao criar estoque produto", execb);
					}
					if (ecb!=null)
					{
						//codBarras.setUltimaAlteracao(null);
						situacaoEnvio.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVAguardandoAprov());
						codBarras.setSituacaoEnvioLV(situacaoEnvio);
						listaCodigoBarras.add(codBarras);
						listaEstoque.add(ecb);
						if (listaProdutos.containsKey(Integer.parseInt(codBarras.getProduto().getCodigoProduto()))==false)
							listaProdutos.put(Integer.parseInt(codBarras.getProduto().getCodigoProduto()), ecb);
						cont++;
						contSalva++;
						if (contSalva>1000)
						{
							importarProdutos(listaProdutos, conCache);
							salvarEstoqueProduto(conCache);							
							listaEstoque = new ArrayList<SaldoEstoqueProduto>();
							listaCodigoBarras = new ArrayList<EstoqueProduto>();
							contSalva=0;
							System.out.println(cont);
						}
						//System.out.println(cont);
					}	
				}
			}
			importarProdutos(listaProdutos, conCache);
			salvarEstoqueProduto(conCache);
			System.out.println("Produto Estoque: " + cont);
			System.out.println("Produtos novos: " + contProdutosNovos);
			System.out.println("Produtos Alterados: " + contProdutosAlterados);
			System.out.println("importarEstoqueCodigoBarras - Hora Final: " + sdf.format(new Date()));
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		finally
		{
			try
			{if (rs!=null)
				rs.close();
			if (stm!=null)
				stm.close();
			}catch(Exception ex)
			{ex.printStackTrace();
				throw ex;}		
		}		
	}
	
	public void importarProdutos(Hashtable<Integer, SaldoEstoqueProduto> produtos, Connection conCache) throws Exception
	{
		Statement stm = null;
		ResultSet rs = null;
		Produto produto;
		Produto produtoExists;
		ProdutoService pService = new ProdutoService();
		Integer idProduto=0;
		GrupoProdutoService grupoProdutoService = new GrupoProdutoService(); 
		MarcaService marcaService = new MarcaService();		

		stm = conCache.createStatement();
		try
		{
			//Hashtable<Integer , ArrayList> refsArquivos = new DsvUtilImagens().buscaReferenciaFotos(DsvConstante.getInstanceParametrosSistema().getCaminhoAbsolutoFotos());
			Enumeration<Integer> eProd = produtos.keys(); 
			while (eProd.hasMoreElements())
			{		
				idProduto = eProd.nextElement();
				/*if (refsArquivos.containsKey(idProduto)==false)
				{
					logCache.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoImportProdutos(), 1, "Referenci do produto não possuem imagens: " + idProduto);
				}*/
				produtoExists = new Produto();
				produtoExists.setCodigoProduto(idProduto+"");
				try
				{
					produtoExists = pService.listarPorObjetoFiltro(produtoExists).get(0);
				}
				catch(Exception e)
				{
					produtoExists = null;
				}
				
				SaldoEstoqueProduto ecb = produtos.get(idProduto);
				rs = stm.executeQuery("SELECT pro.Codigo codigo, pro.Descricao descricaoInterna, pro.DtUltimaCompra DtUltimaCompra, " +
						"pro.DtUltimaVenda dtUltimaVenda, pro.Fornecedor as fornecedor, pro.GrupoCodigo grupoCodigo, pro.MarcaCodigo marcaCodigo, " +
						"des.descricao1 descricao1, des.descricao2 descricao2, des.descricao3 descricao3 " +
						"FROM CEProduto pro left join CeProdutoCompl des on des.codigo = pro.codigo WHERE pro.codigo = " + idProduto);
				if(rs.next())
				{
					if (rs.getString("descricao1")!=null && rs.getString("descricao1").equals("")==false)
					{
						if (produtoExists==null)
						{
							produto = new Produto();
							//produto.setId(rs.getInt("codigo"));
							produto.setCodigoProduto(rs.getInt("codigo")+"");
							produto.setCodigoEan("");
							produto.setNomeProduto(new String(rs.getBytes("descricaoInterna"),"ISO-8859-1"));
							produto.setCodigoNcm("99999999");
							produto.setExptipi("");
							Genero genero = new Genero();
							genero.setId(1); //Genero 1 padrao
							//produto.setGenero(genero);
							produto.setGenero(null);
							UnidadeMedida un = new UnidadeMedida();
							un.setId(1);
							produto.setUnidadeMedida(un);
							TipoItem tipoItem = new TipoItem();
							tipoItem.setId(1);
							produto.setTipoItem(tipoItem);
							//RegimeTributario regimeTributario = new RegimeTributario();
							//regimeTributario.setId(1);
							produto.setRegimeTributario(null);
							SituacaoTributaria sitTrib = new SituacaoTributaria();
							sitTrib.setId(1);
							produto.setSituacaoTributaria(sitTrib);
							produto.setOrigemMercadoria(null);
							produto.setMva(null);
							produto.setAliquotaIcms(0.0);
							produto.setListaServico(null);
							GrupoProduto grupoProduto = new GrupoProduto();
							grupoProduto.setCodigo(rs.getInt("grupoCodigo"));
							grupoProduto = grupoProdutoService.listarPorObjetoFiltro(grupoProduto).get(0);
							produto.setGrupo(grupoProduto);
							Marca marca = new Marca();
							marca.setCodigo(rs.getInt("marcaCodigo"));
							marca = marcaService.listarPorObjetoFiltro(marca).get(0);
							produto.setMarca(marca);
							produto.setSubstitutoTributario("N");
							produto.setAplicacao("");
							produto.setAtivoInativo("A");
							produto.setFornecedor(null); //TODO: Importar fornecedor
							produto.setPesoProduto(new Double("0"));
							produto.setCodigoCatalogo("");
							produto.setCodigoFabricante("");
							produto.setNome1Produto(rs.getString("descricao1"));
							produto.setNome2Produto(rs.getString("descricao2"));
							produto.setNome3Produto(rs.getString("descricao3"));
							produto.setNome4Produto("");
							produto.setNome5Produto("");
							produto.setValoresDescritivo1("");
							produto.setValoresDescritivo2("");
							produto.setValoresDescritivo3("");
							produto.setGarantia(1);
							produto.setAltura(20);
							produto.setLargura(30);
							produto.setProfundidade(50);
							produto.setGenero(null); //TODO: Importar Generos
							produto.setOcasiaoUso(null); //TODO: Importar ocasião de uso
							produto.setMaterial(null);
							produto.setLocalfoto("");
							//produto.setIdGruposub(null);
							produto.setUltimaAlteracao(new Date());
							produto.setProdutoLV("S");
							produto.setUltAlteracaoPreco(new Date());
							SituacaoEnvioLV situacaoEnvioLV = new SituacaoEnvioLV();
							situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVAguardandoAprov());
							produto.setSituacaoEnvioLV(situacaoEnvioLV);
							produto.setPrecoPromocao(0.0);
							try
							{
								pService.salvar(produto);
								contProdutosNovos++;
							}
							catch(Exception ex)
							{
								ex.printStackTrace();
								throw new DsvProcessamentoLVException("Erro ao salvar produto " + produto.getCodigoProduto());
								
							}
						}
						else
						{
							produto = new Produto();
							produto.setId(produtoExists.getId());
							produto.setNomeProduto(new String(rs.getBytes("descricaoInterna"),"ISO-8859-1"));
							produto.setAliquotaIcms(0.0);
							GrupoProduto grupoProduto = new GrupoProduto();
							grupoProduto.setCodigo(rs.getInt("grupoCodigo"));
							grupoProduto = grupoProdutoService.listarPorObjetoFiltro(grupoProduto).get(0);
							produto.setGrupo(grupoProduto);
							Marca marca = new Marca();
							marca.setCodigo(rs.getInt("marcaCodigo"));
							marca = marcaService.listarPorObjetoFiltro(marca).get(0);
							produto.setMarca(marca);
							produto.setFornecedor(null);
							produto.setPesoProduto(new Double("0"));
							produto.setNome1Produto(rs.getString("descricao1"));
							produto.setNome2Produto(rs.getString("descricao2"));
							produto.setNome3Produto(rs.getString("descricao3"));
							if (produtoExists.equals(produto)==false)
							{
								produto = produtoExists;
								produto.setUltimaAlteracao(new Date());
								produto.setProdutoLV("S");
								SituacaoEnvioLV situacaoEnvioLV = new SituacaoEnvioLV();
								situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVAguardandoAprov());
								produto.setSituacaoEnvioLV(situacaoEnvioLV);
								pService.salvar(produto);
								contProdutosAlterados++;
							}
						}
					}
					else
					{
						if (ecb.getSaldo()!=null && ecb.getSaldo()>0)
						{
							logCache.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoImportProdutos(), 1, "Descricao para a Loja Virtual nao existe mas possui saldo: " + idProduto);
						}
						listaProdutosComErro.put(rs.getInt("codigo"), 1);
					}
				}

				if (rs!=null)
					rs.close();
			}
		}
		catch(Exception e)
		{
			logCache.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoImportProdutos(), 1, "Erro ao gerar produto" + idProduto);
			throw e;
		}
		finally
		{
			try
			{if (rs!=null)
				rs.close();
			if (stm!=null)
				stm.close();
			}catch(Exception ex)
			{ex.printStackTrace();
				throw ex;}		
		}
	}
	
	
	public void importarProdutosAlterados(Connection conCache) throws Exception
	{
		Statement stm = null;
		ResultSet rs = null;
		Produto produto;
		Produto produtoExists;
		ProdutoService pService = new ProdutoService();
		Integer idProduto=0;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		System.out.println("importarProdutosAlterados - Hora inicial: " + sdf.format(new Date()));
		GrupoProdutoService grupoProdutoService = new GrupoProdutoService(); 
		MarcaService marcaService = new MarcaService();
		
		stm = conCache.createStatement();
		try
		{				
			rs = stm.executeQuery("SELECT pro.Codigo codigo, pro.Descricao descricaoInterna, pro.DtUltimaCompra DtUltimaCompra, " +
					"pro.DtUltimaVenda dtUltimaVenda, pro.Fornecedor as fornecedor, pro.GrupoCodigo grupoCodigo, pro.MarcaCodigo marcaCodigo, " +
					"des.descricao1 descricao1, des.descricao2 descricao2, des.descricao3 descricao3 " +
					"FROM CEProduto pro left join CeProdutoCompl des on des.codigo = pro.codigo");
			while(rs.next())
			{
				produtoExists = new Produto();
				produtoExists.setCodigoProduto(rs.getInt("codigo")+"");
				try
				{
					produtoExists = pService.listarPorObjetoFiltro(produtoExists).get(0);
				}
				catch(Exception e)
				{
					produtoExists = null;
				}				
				if (produtoExists!=null)
				{
					if (rs.getString("descricao1")!=null && rs.getString("descricao1").equals("")==false)
					{					
							produto = new Produto();
							produto.setId(produtoExists.getId());
							produto.setNomeProduto(new String(rs.getBytes("descricaoInterna"),"ISO-8859-1"));
							produto.setAliquotaIcms(0.0);
							GrupoProduto grupoProduto = new GrupoProduto();
							grupoProduto.setCodigo(rs.getInt("grupoCodigo"));
							grupoProduto = grupoProdutoService.listarPorObjetoFiltro(grupoProduto).get(0);
							produto.setGrupo(grupoProduto);
							Marca marca = new Marca();
							marca.setCodigo(rs.getInt("marcaCodigo"));
							marca = marcaService.listarPorObjetoFiltro(marca).get(0);
							produto.setMarca(marca);
							produto.setFornecedor(null);
							produto.setPesoProduto(0.0);
							try
							{
								produto.setNome1Produto(new String(rs.getBytes("descricao1"),"ISO-8859-1"));
							}
							catch(Exception exDesc1)
							{
								produto.setNome1Produto("");
							}
							try
							{
								produto.setNome2Produto(new String(rs.getBytes("descricao2"),"ISO-8859-1"));
							}
							catch(Exception exDesc1)
							{
								produto.setNome2Produto("");
							}
							try
							{
								produto.setNome3Produto(new String(rs.getBytes("descricao3"),"ISO-8859-1"));
							}
							catch(Exception exDesc1)
							{
								produto.setNome3Produto("");
							}							
							if (produtoExists.equals(produto)==false)
							{
								produto = produtoExists;
								produto.setId(produtoExists.getId());
								produto.setNomeProduto(new String(rs.getBytes("descricaoInterna"),"ISO-8859-1"));
								produto.setAliquotaIcms(0.0);
								grupoProduto = new GrupoProduto();
								grupoProduto.setCodigo(rs.getInt("grupoCodigo"));
								grupoProduto = grupoProdutoService.listarPorObjetoFiltro(grupoProduto).get(0);
								produto.setGrupo(grupoProduto);
								marca = new Marca();
								marca.setCodigo(rs.getInt("marcaCodigo"));
								marca = marcaService.listarPorObjetoFiltro(marca).get(0);
								produto.setMarca(marca);
								produto.setFornecedor(null);
								produto.setPesoProduto(0.0);
								try
								{
									produto.setNome1Produto(new String(rs.getBytes("descricao1"),"ISO-8859-1"));
								}
								catch(Exception exDesc1)
								{
									produto.setNome1Produto("");
								}
								try
								{
									produto.setNome2Produto(new String(rs.getBytes("descricao2"),"ISO-8859-1"));
								}
								catch(Exception exDesc1)
								{
									produto.setNome2Produto("");
								}
								try
								{
									produto.setNome3Produto(new String(rs.getBytes("descricao3"),"ISO-8859-1"));
								}
								catch(Exception exDesc1)
								{
									produto.setNome3Produto("");
								}								
								produto.setUltimaAlteracao(new Date());
								produto.setProdutoLV("S");
								SituacaoEnvioLV situacaoEnvioLV = new SituacaoEnvioLV();
								situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVAguardandoAprov());
								produto.setSituacaoEnvioLV(situacaoEnvioLV);
								pService.salvar(produto);
							}
					}
					else
					{
						logCache.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoImportProdutos(), 1, "Descricao para a Loja Virtual nao existe " + idProduto);
					}
				}
			}
			if (rs!=null)
			rs.close();
			
			System.out.println("importarProdutosAlterados - Hora Final: " + sdf.format(new Date()));

		}
		catch(Exception e)
		{
			logCache.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoImportProdutos(), 1, "Erro ao gerar produto" + idProduto);
			throw e;
		}
		finally
		{
			try
			{if (rs!=null)
				rs.close();
			if (stm!=null)
				stm.close();
			}catch(Exception ex)
			{ex.printStackTrace();
				throw ex;}		
		}
	}	
	
	public void salvarEstoqueProduto(Connection conCache) throws Exception
	{

		SaldoEstoqueProdutoService ecbService = new SaldoEstoqueProdutoService();
		EstoqueProdutoService codigoBarrasService = new EstoqueProdutoService();
		SaldoEstoqueProduto ecb;
		EstoqueProduto codigoBarras;
		SaldoEstoqueProduto criterioSaldo;
		EstoqueProduto criterioEp;
		Hashtable<Integer,EstoqueProduto> listaCbErro = new Hashtable<Integer, EstoqueProduto>();
		ProdutoService produtoService = new ProdutoService();
		List<EstoqueProduto> resultadoEp;
		List<SaldoEstoqueProduto> resultadoSaldo;

		EntityManager manager = ConexaoUtil.getEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		transaction.begin();
		try
		{

			for (int contCod=0;contCod<listaCodigoBarras.size();contCod++)
			{
				codigoBarras = listaCodigoBarras.get(contCod);
				criterioEp = new EstoqueProduto();
				criterioEp.setCodigoBarras(codigoBarras.getCodigoBarras());
				if (listaProdutosComErro.containsKey(codigoBarras.getCodigoBarras())==false)
				{
					resultadoEp = codigoBarrasService.listarPorObjetoFiltro(criterioEp);
					if (resultadoEp.isEmpty())
					{
						try
						{
							codigoBarras.setProduto(produtoService.listarPorObjetoFiltro(codigoBarras.getProduto()).get(0));
							codigoBarrasService.salvar(codigoBarras);
						}
						catch(Exception e)
						{
							System.out.println(codigoBarras.getCodigoBarras()+ " "+codigoBarras.getProduto().getId()+" "+codigoBarras.getCor().getId()+" "+codigoBarras.getTamanho().getId()+" "+codigoBarras.getOrdemProduto().getId());
						}
					}
					else
					{
						//criterioEp = resultadoEp.get(0);
						//codigoBarras.setId(criterioEp.getId());
						//codigoBarrasService.atualizar(codigoBarras);
					}
				}
				else
				{
					listaCbErro.put(new Integer(codigoBarras.getCodigoBarras()), codigoBarras);
				}
					
			}
			for (int contEst=0;contEst<listaEstoque.size();contEst++)
			{
				ecb = listaEstoque.get(contEst);
				criterioSaldo = new SaldoEstoqueProduto();
				criterioSaldo.setAno(ecb.getAno());
				criterioSaldo.setMes(ecb.getMes());
				criterioSaldo.setEmpresaFisica(ecb.getEmpresaFisica());
				criterioSaldo.setEstoqueProduto(ecb.getEstoqueProduto());
				try
				{
					if (!listaCbErro.containsKey(ecb.getId()))
					{
						criterioEp = new EstoqueProduto();
						criterioEp.setCodigoBarras(ecb.getEstoqueProduto().getCodigoBarras());
						resultadoEp = codigoBarrasService.listarPorObjetoFiltro(criterioEp);
						if (!resultadoEp.isEmpty())
						{
							resultadoSaldo = ecbService.listarPorObjetoFiltro(criterioSaldo);
							if (resultadoSaldo.isEmpty())
							{
								ecb.setEstoqueProduto(resultadoEp.get(0));
								ecb.setDataUltimaAlteracao(new Date());
								ecbService.salvar(ecb);
							}
	
							else
							{
								//criterioSaldo = resultadoSaldo.get(0);
								//ecb.setId(criterioSaldo.getId());
								//ecbService.atualizar(ecb);
							}
						}
					}
				}
				catch(Exception exEcb)
				{
					System.out.println(ecb.getId()+ " " + ecb.getAno() + ecb.getMes());
					throw exEcb;
				}
			}
			transaction.commit();
		}
		catch(Exception e)
		{
			transaction.rollback();
			throw e;
		}
	}
	
	private String ajustaStr(String codigo)
	{
		if (codigo.length()==1)
		{
			return "0"+codigo;
		}
		else 
		{
			return ""+codigo;
		}
	}
	
}
