package br.com.desenv.nepalign.cache.importadores;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.apache.log4j.Logger;

import br.com.desenv.frameworkignorante.Formatador;
import br.com.desenv.nepalign.cache.constantes.Util;
import br.com.desenv.nepalign.model.Cliente;
import br.com.desenv.nepalign.model.EstoqueProduto;
import br.com.desenv.nepalign.model.PedidoVenda;
import br.com.desenv.nepalign.model.PlanoPagamento;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.model.SituacaoPedidoVenda;
import br.com.desenv.nepalign.model.StatusItemPedido;
import br.com.desenv.nepalign.model.TipoMovimentacaoEstoque;
import br.com.desenv.nepalign.model.Vendedor;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ImportadorArquivoVendaCache extends ImportadorCache
{
	public static void main(String[] args)
	{
		final String diretorioBase = args.length > 0x00 ? args[0x00] : "D:\\Desenv\\ARQ_CONV\\";
		
		new ImportadorArquivoVendaCache().run(diretorioBase);
	}
	
	enum FILE_METADATA_PV
	{
		LOJA((short) 0),
		ANOMES((short) 1),
		NUMEROCONTROLE((short) 2),
		DATAVENDA((short) 3),
		VENDEDOR((short) 5),
		VALORVENDA((short) 6),
		PLANO((short) 7),
		TIPO((short) 8);
		
		private final short index;
		
		FILE_METADATA_PV(final short index)
		{
			this.index = index;
		}
		
		short getIndex()
		{
			return index;
		}
	}
	
	enum FILE_METADATA_IPV
	{
		LOJA((short) 0),
		ANOMES((short) 1),
		NUMEROCONTROLE((short) 2),
		SEQUENCIA((short) 3),
		REFERENCIA((short) 4),
		CODIGOBARRAS((short) 5),
		VALORVENDA((short) 6),
		QUANTIDADE((short) 7),
		ENTRADASAIDA((short) 8);
		
		private final short index;
		
		FILE_METADATA_IPV(final short index)
		{
			this.index = index;
		}
		
		short getIndex()
		{
			return index;
		}
	}
	
	enum ENTIDADE_PADRAO
	{
		CLIENTE(0),
		TIPOMOVIMENTACAOESTOQUE_VENDA(2),
		TIPOMOVIMENTACAOESTOQUE_TROCA(5),
		SITUACAOPEDIDOVENDA(3),
		STATUSITEMPEDIDO(1);
		
		private final int codigo;
		
		ENTIDADE_PADRAO(final int codigo)
		{
			this.codigo = codigo;
		}
		
		int getCodigo()
		{
			return codigo;
		}
	}
	
	private void validateCliente(final EntityManager manager) throws NoResultException
	{
		if(manager.find(Cliente.class, ENTIDADE_PADRAO.CLIENTE.getCodigo()) == null)
			throw new NoResultException(String.format("Não foi possível encontrar o Cliente PADRÃO com código %d no banco de dados!", ENTIDADE_PADRAO.CLIENTE.getCodigo()));
	}
	
	private void validateTipoMovimentacaoEstoque(final EntityManager manager) throws NoResultException
	{
		if(manager.find(TipoMovimentacaoEstoque.class, ENTIDADE_PADRAO.TIPOMOVIMENTACAOESTOQUE_VENDA.getCodigo()) == null)
			throw new NoResultException(String.format("Não foi possível encontrar o TipoMovimentacaoEstoque de Venda com código %d no banco de dados!", ENTIDADE_PADRAO.TIPOMOVIMENTACAOESTOQUE_VENDA.getCodigo()));
		
		if(manager.find(TipoMovimentacaoEstoque.class, ENTIDADE_PADRAO.TIPOMOVIMENTACAOESTOQUE_TROCA.getCodigo()) == null)
			throw new NoResultException(String.format("Não foi possível encontrar o TipoMovimentacaoEstoque de Troca com código %d no banco de dados!", ENTIDADE_PADRAO.TIPOMOVIMENTACAOESTOQUE_TROCA.getCodigo()));
	}
	
	private void validateSituacaoPedidoVenda(final EntityManager manager) throws NoResultException
	{
		if(manager.find(SituacaoPedidoVenda.class, ENTIDADE_PADRAO.SITUACAOPEDIDOVENDA.getCodigo()) == null)
			throw new NoResultException(String.format("Não foi possível encontrar a SituacaoPedidoVenda com código %d no banco de dados!", ENTIDADE_PADRAO.SITUACAOPEDIDOVENDA.getCodigo()));
	}
	
	private void validateStatusItemPedido(final EntityManager manager) throws NoResultException
	{
		if(manager.find(StatusItemPedido.class, ENTIDADE_PADRAO.STATUSITEMPEDIDO.getCodigo()) == null)
			throw new NoResultException(String.format("Não foi possível encontrar o StatusItemPedido com código %d no banco de dados!", ENTIDADE_PADRAO.STATUSITEMPEDIDO.getCodigo()));
	}
	
	public void run(final String diretorioBase)
	{
		EntityManager manager = ConexaoUtil.getFactory("importacao", "importacao!123").createEntityManager();
		try 
		{
			manager.getTransaction().begin();
			clean(manager);
			manager.getTransaction().commit();
			
			Logger.getLogger(ImportadorArquivoVendaCache.this.getClass()).info("Validando Entidades...");
			validateCliente(manager);
			validateTipoMovimentacaoEstoque(manager);
			
			validateStatusItemPedido(manager);
			validateSituacaoPedidoVenda(manager);
			Logger.getLogger(ImportadorArquivoVendaCache.this.getClass()).info("Concluído");
			
			importarVendas(manager, readFile(diretorioBase.concat("MOVIMENTO.TXT")));
			importarItens(manager, readFile(diretorioBase.concat("MOVIMENTOIT.TXT")));
		}
		catch(InterruptedException ie)
		{
			ie.printStackTrace();
			manager.getTransaction().rollback();
		}
		catch (IOException e) 
		{
			manager.getTransaction().rollback();
			e.printStackTrace();
		}
		finally
		{
			manager.close();
		}
	}
	
	private void clean(final EntityManager manager)
	{
		Logger.getLogger(ImportadorArquivoVendaCache.this.getClass()).info("Apagando dados antigos...");
		manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=0;").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE itempedidovenda;").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE pedidovenda;").executeUpdate();
		manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=1;").executeUpdate();
		Logger.getLogger(ImportadorArquivoVendaCache.this.getClass()).info("Concluído");
	}

	final List<PedidoVenda> listaPedidoVenda = new ArrayList<PedidoVenda>();
	
	@SuppressWarnings("unchecked")
	public void importarVendas(final EntityManager manager, final String data[]) throws InterruptedException
	{
		Logger.getLogger(ImportadorArquivoVendaCache.this.getClass()).info("Iniciando bloco de importação de PedidoVenda");
		
		final List<Vendedor> vendedores = new ArrayList<Vendedor>();
		final List<PlanoPagamento> planos = new ArrayList<PlanoPagamento>();
		
		vendedores.addAll(manager.createNativeQuery("SELECT * FROM VENDEDOR", Vendedor.class).getResultList());
		planos.addAll(manager.createNativeQuery("SELECT * FROM planoPagamento where idEmpresaFisica = " + data[0x01].split(Util.CACHE_FILE_SEPARATOR, -1)[0x00], PlanoPagamento.class).getResultList());
		
		final Counter counter = new Counter();
		
	    class RunBlock implements Runnable 
	    {
	        int startIndex, endIndex;
	        Connection connection;
	        final String[] data;
	        
	        final Integer getUsuario(final String codigoVendedor) throws Exception
	        {
	        	for(final Vendedor vendedor : vendedores)
	        		if(vendedor.getCodigoVendedorCache().equals(Integer.parseInt(codigoVendedor)))
	        			return vendedor.getUsuario().getId();
	        	
	        	throw new Exception(String.format("Não foi encontrado Usuario do Vendedor com o código %s", codigoVendedor));
	        }
	        
	        final Integer getVendedor(final String codigoVendedor) throws Exception
	        {
	        	for(final Vendedor vendedor : vendedores)
	        		if(vendedor.getCodigoVendedorCache().equals(Integer.parseInt(codigoVendedor)))
	        			return vendedor.getId();
	        	
	        	throw new Exception(String.format("Não foi encontrado Vendedor com o código %s", codigoVendedor));
	        }
	        
	        final Integer getPlanoPagamento(final String codigoPlano) throws Exception
	        {
	        	for(final PlanoPagamento planoPagamento : planos)
	        		if(planoPagamento.getCodigoPlano().equals(codigoPlano))
	        			return planoPagamento.getId();
	        	
	        	throw new Exception(String.format("Não foi encontrado Plano Pagamento com o código %s", codigoPlano));
	        }
	        
	        RunBlock(String[] data, int startIndex, int endIndex) 
	        { 
	        	this.connection = ConexaoUtil.getConexaoPadrao("importacao", "importacao!123");
	        	this.startIndex = startIndex;
	        	this.endIndex = endIndex;
	        	this.data = data;
	        }

			@Override
			public void run() 
			{
				try 
				{
					PreparedStatement psPedidoVenda = connection.prepareStatement("INSERT INTO pedidoVenda (idTipoMovimentacaoEstoque, idEmpresaFisica, idVendedor, idCliente, idUsuario, idSituacaoPedidoVenda, idPlanoPagamento," +
							"dataVenda, numeroControle, valorTotalPedido) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);",
                            Statement.RETURN_GENERATED_KEYS); 
					
    	    		for(int index = startIndex; index < endIndex; index++)
    	    		{ 
    	    			try
    	    			{
        	    			final String[] metadata = data[index].split(Util.CACHE_FILE_SEPARATOR, -1);
        	    			
        	    			if(metadata[FILE_METADATA_PV.TIPO.getIndex()].equals("1")) // venda
        	    				psPedidoVenda.setInt(1, ENTIDADE_PADRAO.TIPOMOVIMENTACAOESTOQUE_VENDA.getCodigo());
        	    			else // troca
        	    				psPedidoVenda.setInt(1, ENTIDADE_PADRAO.TIPOMOVIMENTACAOESTOQUE_TROCA.getCodigo());
        	    			
        	    			
        	    			psPedidoVenda.setInt(2, Integer.parseInt(metadata[FILE_METADATA_PV.LOJA.getIndex()].trim()));
        	    			psPedidoVenda.setInt(3, getVendedor(metadata[FILE_METADATA_PV.VENDEDOR.getIndex()].trim()));
        	    			psPedidoVenda.setInt(4, ENTIDADE_PADRAO.CLIENTE.getCodigo());
        	    			psPedidoVenda.setInt(5, getUsuario(metadata[FILE_METADATA_PV.VENDEDOR.getIndex()].trim()));
        	    			psPedidoVenda.setInt(6, ENTIDADE_PADRAO.SITUACAOPEDIDOVENDA.getCodigo());
        	    			psPedidoVenda.setInt(7, getPlanoPagamento(metadata[FILE_METADATA_PV.PLANO.getIndex()]));
        	    			synchronized (sqlDateFormat) 
        	    			{
            	    			psPedidoVenda.setString(8, sqlDateFormat.format(dateFormat.parse(metadata[FILE_METADATA_PV.DATAVENDA.getIndex()])));	
							}
        	    			psPedidoVenda.setString(9, metadata[FILE_METADATA_PV.NUMEROCONTROLE.getIndex()]);
        	    			psPedidoVenda.setDouble(10, Double.parseDouble(metadata[FILE_METADATA_PV.VALORVENDA.getIndex()]));
        	    			
        	    			psPedidoVenda.executeUpdate();
        	    			
        	    			PedidoVenda pedidoVenda = new PedidoVenda();
        	    			pedidoVenda.setNumeroControle(metadata[FILE_METADATA_PV.NUMEROCONTROLE.getIndex()].trim());
        	    			
        	    			try (ResultSet generatedKeys = psPedidoVenda.getGeneratedKeys()) 
        	    			{
        	    	            if (generatedKeys.next()) 
        	    	            	pedidoVenda.setId(generatedKeys.getInt(1));
        	    	            else 
        	    	                throw new Exception("Creating PedidoVenda failed, no ID obtained.");
        	    	        }
        	    			
        	    			synchronized (listaPedidoVenda) 
        	    			{
            	    			listaPedidoVenda.add(pedidoVenda);
							}

        	    			
        	    			counter.increment();
	    	    		}
	    				catch (Exception e) 
	    				{
	    					Logger.getLogger(ImportadorArquivoVendaCache.class).error(String.format("Erro ao processar a linha %s", data[index]), e);
	    					throw e;
	    				}
    	    		}
				}
				catch (Exception e) 
				{
					
				}
			}
	    }
	    
	    List<Thread> runBlockList = new ArrayList<Thread>();
	    
	    Thread firstBlock = new Thread(new RunBlock( data, 0x01, (int) (data.length * 0.01)));
	    firstBlock.setName(String.format("PedidoVenda - Unit Block %d to %d", 1, (int) (data.length * 0.01)));
	    firstBlock.start();
	    runBlockList.add(firstBlock);
	     
	    for(double threadPart = 0.01; threadPart < 0x01; threadPart += 0.01)
	    {
	    	Thread thread = new Thread(new RunBlock( data, (int) (data.length * threadPart), (int) (data.length * (threadPart + 0.01))));
	    	thread.setName(String.format("PedidoVenda - Unit Block %d to %d", (int) (data.length * threadPart),  (int) (data.length * (threadPart + 0.01))));
	    	thread.start();
	    	runBlockList.add(thread);
	    }
	    
	    Thread counterThread  = new Thread(new Runnable() 
	    {
			@Override
			public void run() 
			{
			    while(!Thread.currentThread().isInterrupted())
			    {
			    	try 
			    	{
						synchronized (counter) 
						{
							Logger.getLogger(ImportadorArquivoVendaCache.this.getClass()).info(String.format("%s PedidoVenda processados", counter));
						}
						Thread.sleep(1500);
			    	}
			    	catch (InterruptedException e) 
			    	{
			    		Thread.currentThread().interrupt();
					}
			    }
			}
		});
	    counterThread.start();
	    
	    for(final Thread unitBlock : runBlockList)
	    	unitBlock.join();
	    
	    counterThread.interrupt();
	    Logger.getLogger(ImportadorArquivoVendaCache.this.getClass()).info("Bloco de importação de Pedido Venda finalizado!");
	}
	
	public void importarItens(final EntityManager manager, final String data[]) throws InterruptedException
	{
		Logger.getLogger(ImportadorArquivoVendaCache.this.getClass()).info("Iniciando bloco de importação de ItemPedidoVenda");
		final List<EstoqueProduto> listaEstoqueProduto = new ArrayList<EstoqueProduto>();
		
		final Counter counter = new Counter();
		
	    class RunBlock implements Runnable 
	    {
	        int startIndex, endIndex;
	        Connection connection;
	        final String[] data;
	        
	        final Integer getPedidoVenda(final String numeroControle) throws Exception
	        {
	        	synchronized (listaPedidoVenda) 
	        	{
		        	for(final PedidoVenda pedidoVenda : listaPedidoVenda)
		        	{
		        		if(pedidoVenda.getNumeroControle().equals(numeroControle))
		        			return pedidoVenda.getId();	
		        	}
				}
	        	throw new Exception(String.format("Não foi encontrado PedidoVenda com o Número Controle %s", numeroControle));	
	        }
	        
	        @SuppressWarnings("unused")
			final Integer getProduto(final String codigoBarras) throws Exception
	        {
	        	synchronized (listaEstoqueProduto) 
	        	{
		        	for(final EstoqueProduto estoqueProduto : listaEstoqueProduto)
		        	{
		        		if(estoqueProduto.getCodigoBarras().equals(codigoBarras))
		        			return estoqueProduto.getProduto().getId();
		        	}

				}

	        	try
	        	{
	        		Produto produto = (Produto) manager.createNativeQuery("SELECT * FROM produto WHERE idProduto = (SELECT idProduto FROM estoqueProduto WHERE idEstoqueProduto = " + codigoBarras + " LIMIT 1)", Produto.class).getResultList().get(0);
	        		
	        		EstoqueProduto estoqueProduto = new EstoqueProduto();
	        		estoqueProduto.setCodigoBarras(codigoBarras);
	        		estoqueProduto.setProduto(produto);
	        		
	        		synchronized (listaEstoqueProduto)
	        		{
	        			listaEstoqueProduto.add(estoqueProduto);
					}
	        		
	        		return produto.getId();
	        	}
	        	catch(Exception ex)
	        	{
	        		ex.printStackTrace();
		        	throw new Exception(String.format("Não foi encontrado EstoqueProduto com o Código de Barras %s", codigoBarras));
	        	}
	        }
	        
	        RunBlock(String[] data, int startIndex, int endIndex) 
	        { 
	        	this.connection = ConexaoUtil.getConexaoPadrao("importacao", "importacao!123");
	        	this.startIndex = startIndex;
	        	this.endIndex = endIndex;
	        	this.data = data;
	        }

			@Override
			public void run() 
			{
				try 
				{
					PreparedStatement psItemPedidoVenda = connection.prepareStatement("INSERT INTO itemPedidoVenda (idPedidoVenda, itemSequencial, entradaSaida, quantidade, precoTabela, precoVenda, idEstoqueProduto," +
							"idProduto, idStatusItemPedido) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);"); 
					
    	    		for(int index = startIndex; index < endIndex; index++)
    	    		{ 
    	    			try
    	    			{
        	    			final String[] metadata = data[index].split(Util.CACHE_FILE_SEPARATOR, -1);
    	    				
    	    				final boolean itemSaida = metadata[FILE_METADATA_IPV.ENTRADASAIDA.getIndex()].equals("") || metadata[FILE_METADATA_IPV.ENTRADASAIDA.getIndex()].equals("-");
    	    				
    	    				double valorVenda = Double.parseDouble(metadata[FILE_METADATA_IPV.VALORVENDA.getIndex()]);
    	    				double quantidade = Double.parseDouble(metadata[FILE_METADATA_IPV.QUANTIDADE.getIndex()]);
    	    				
        	    			psItemPedidoVenda.setInt(1, getPedidoVenda(metadata[FILE_METADATA_IPV.NUMEROCONTROLE.getIndex()].trim()));
        	    			psItemPedidoVenda.setInt(2, Integer.parseInt(metadata[FILE_METADATA_IPV.SEQUENCIA.getIndex()]));
        	    			psItemPedidoVenda.setString(3, itemSaida ? "S" : "E");
        	    			psItemPedidoVenda.setDouble(4, quantidade);
        	    			psItemPedidoVenda.setDouble(5, Formatador.round(valorVenda / quantidade, 2));
        	    			psItemPedidoVenda.setDouble(6, Formatador.round(valorVenda / quantidade, 2));
        	    			psItemPedidoVenda.setInt(7, Integer.parseInt(metadata[FILE_METADATA_IPV.CODIGOBARRAS.getIndex()].trim()));
        	    			psItemPedidoVenda.setInt(8, Integer.parseInt(metadata[FILE_METADATA_IPV.REFERENCIA.getIndex()].trim()));
        	    			//psItemPedidoVenda.setInt(8, getProduto(metadata[FILE_METADATA_IPV.CODIGOBARRAS.getIndex()].trim())); 
        	    			psItemPedidoVenda.setInt(9, ENTIDADE_PADRAO.STATUSITEMPEDIDO.getCodigo());
        	    			
        	    			synchronized (connection) 
        	    			{
            	    			psItemPedidoVenda.executeUpdate();
            	    			
            	    			counter.increment();
							}
	    	    		}
	    				catch (Exception e) 
	    				{
	    					Logger.getLogger(ImportadorArquivoVendaCache.class).error(String.format("Erro ao processar a linha %s. Mensagem: %s", data[index], e.getMessage()));
	    					throw e;
	    				}
    	    		}
				}
				catch (Exception e) 
				{
					
				}
			}
	    }
	    
	    List<Thread> runBlockList = new ArrayList<Thread>();
	    
	    Thread firstBlock = new Thread(new RunBlock( data, 0x01, (int) (data.length * 0.01)));
	    firstBlock.setName(String.format("ItemPedidoVenda - Unit Block %d to %d", 1, (int) (data.length * 0.01)));
	    firstBlock.start();
	    runBlockList.add(firstBlock);
	     
	    for(double threadPart = 0.01; threadPart < 0x01; threadPart += 0.01)
	    {
	    	Thread thread = new Thread(new RunBlock( data, (int) (data.length * threadPart), (int) (data.length * (threadPart + 0.01))));
	    	thread.setName(String.format("ItemPedidoVenda - Unit Block %d to %d", (int) (data.length * threadPart),  (int) (data.length * (threadPart + 0.01))));
	    	thread.start();
	    	runBlockList.add(thread);
	    }
	    
	    Thread counterThread  = new Thread(new Runnable() 
	    {
			@Override
			public void run() 
			{
			    while(!Thread.currentThread().isInterrupted())
			    {
			    	try 
			    	{
						synchronized (counter) 
						{
							Logger.getLogger(ImportadorArquivoVendaCache.this.getClass()).info(String.format("%s ItemPedidoVenda processados", counter));
						}
						Thread.sleep(1500);
			    	}
			    	catch (InterruptedException e) 
			    	{
			    		Thread.currentThread().interrupt();
					}
			    }
			}
		});
	    counterThread.start();
	    
	    for(final Thread unitBlock : runBlockList)
	    	unitBlock.join();
	    
	    counterThread.interrupt();
	    Logger.getLogger(ImportadorArquivoVendaCache.this.getClass()).info("Bloco de importação de Item Pedido Venda finalizado!");
	}
}