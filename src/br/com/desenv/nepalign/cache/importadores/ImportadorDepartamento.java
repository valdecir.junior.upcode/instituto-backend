package br.com.desenv.nepalign.cache.importadores;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import com.sun.org.apache.xml.internal.security.Init;

import br.com.desenv.nepalign.model.Departamento;
import br.com.desenv.nepalign.model.SituacaoEnvioLV;
import br.com.desenv.nepalign.service.DepartamentoService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.util.DsvUtilText;

public class ImportadorDepartamento 
{
	public static void main(String[] args) throws Exception{
		ImportadorDepartamento i = new ImportadorDepartamento();
		
		i.init(null);
		
	}
	
	public void init(Connection conCache) throws Exception
	{
		String sql = "";
		Statement stm=null;
		ResultSet rs=null; 
		try 
		{
			Departamento departamento = null;
			Departamento criterio = null;
			DepartamentoService depService = new DepartamentoService();
			int idPai;
			int idFilho;
			Departamento depPai = null;
			Departamento depFilho = null;
			String strCod;
			SituacaoEnvioLV sitEnvio = new SituacaoEnvioLV();
			
			
			sitEnvio.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVAguardandoEvSistema());			
			
			
			if (conCache==null)
			{
				try 
				{
					Class.forName("com.intersys.jdbc.CacheDriver");
					conCache = DriverManager.getConnection(
							DsvConstante.getInstanceParametrosSistema().getStrConexaoCache(), DsvConstante.getInstanceParametrosSistema().getUsuarioCache(), DsvConstante.getInstanceParametrosSistema().getSenhaCache());
				} 
				catch (Exception chEx) 
				{
					throw new Exception("Falha na conexão com o caché."
							+ chEx.getMessage());
				}
			}
			sql = "SELECT * FROM CESUBDEPDEP";
			stm = conCache.createStatement();
			rs = stm.executeQuery(sql);
			ArrayList<Departamento> listaFilhos = new ArrayList<Departamento>();
			Hashtable<Integer, Departamento> listaPais = new Hashtable<Integer, Departamento>();
			while(rs.next())
			{
				if (rs.getString("SubDeparDescricao").equalsIgnoreCase("Porta Chuteira")==false)
				{
					idPai = rs.getInt("DeparCodigo");
					idFilho = rs.getInt("SubDeparCodigo");
					depPai = retornaDepartamento(conCache, idPai);
					depFilho = retornaSubDepartamento(conCache, idFilho);
					criterio = new Departamento();
					criterio.setIdDepCache(idPai);
					criterio.setIdSubDepCache(idFilho);
					listaPais.put(idPai, depPai);
					
					try
					{
						departamento = depService.listarPorObjetoFiltro(criterio).get(0);
						listaFilhos.add(departamento);
					}
					catch(Exception exDep)
					{
						strCod = DsvUtilText.addZerosLeft(idPai, 3)+DsvUtilText.addZerosLeft(idFilho, 3);
						departamento = new Departamento();
						departamento.setAtivo("S");
						departamento.setCodigo(strCod);
						departamento.setDescricao(new String(depFilho.getDescricao().getBytes(),"ISO-8859-1"));
						//departamento.setDescricaoCompleta(depFilho.getDescricao());
						//departamento.setIdDepartamento(Integer.parseInt(strCod));
						departamento.setDepartamentoPai(depPai);
						departamento.setIdDepCache(idPai);
						departamento.setIdSubDepCache(idFilho);
						departamento.setSituacaoEnvioLV(sitEnvio);
						/*departamento.setImagemDepartamento("");
						departamento.setLinkExterno(false);
						departamento.setOrdemDisposicao(0);
						departamento.setUrlExterna("");
						departamento.setSituacaoEnvioLV(null);*/
						listaFilhos.add(departamento);
					}
				}
			}
			//Salva Pais
			Departamento existsDep;
			Enumeration<Departamento> e = listaPais.elements();
			while (e.hasMoreElements())
			{
				depPai = e.nextElement();
				departamento = new Departamento();
				criterio = new Departamento();
				criterio.setIdDepCache(depPai.getIdDepCache());
				criterio.setIdSubDepCache(0);
				try
				{
					departamento = depService.listarPorObjetoFiltro(criterio).get(0);
					departamento.setDescricao(depPai.getDescricao());
					
					existsDep = depService.recuperarPorId(departamento.getId());
					if (existsDep==null || departamento.equals(existsDep)==false)
					{
						depService.salvar(departamento);
					}
				}
				catch(Exception exDep)
				{
					strCod = DsvUtilText.addZerosLeft(depPai.getIdDepCache(), 3);
					departamento = new Departamento();
					departamento.setAtivo("S");
					departamento.setCodigo(strCod);
					departamento.setDescricao(new String(depPai.getDescricao().getBytes(), "ISO-8859-1"));
					departamento.setDescricaoCompleta(depPai.getDescricaoCompleta());
					//departamento.setIdDepartamento(Integer.parseInt(strCod));
					departamento.setDepartamentoPai(null);
					departamento.setIdDepCache(depPai.getIdDepCache());
					departamento.setIdSubDepCache(0);
					departamento.setSituacaoEnvioLV(sitEnvio);
					//departamento.setImagemDepartamento("");
					//departamento.setLinkExterno(false);
					departamento.setOrdemDisposicao(0);
					//departamento.setUrlExterna("");
					departamento.setSituacaoEnvioLV(null);
					departamento = depService.salvar(departamento);
					depPai.setId(departamento.getId());
				} 
			}
			for (int contDep=0; contDep<listaFilhos.size();contDep++)
			{
				departamento = listaFilhos.get(contDep);
				departamento.setAtivo("S");
				depPai = new Departamento();
				depPai.setIdDepCache(departamento.getIdDepCache());
				depPai.setIdSubDepCache(0);
				depPai = depService.listarPorObjetoFiltro(depPai).get(0) ;
				Departamento criterioDp = new Departamento();
				criterioDp.setIdSubDepCache(departamento.getIdSubDepCache());
				try
				{
					departamento.setDepartamentoPai(depPai);
					if (depService.listarPorObjetoFiltro(criterioDp).isEmpty())
					{
						departamento = depService.salvar(departamento);
					}					
				}
				catch(Exception exDep)
				{
					System.out.println("Erro ao Importar departamento " + exDep.getMessage());
					exDep.printStackTrace();
				}
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if (rs!=null)
					rs.close();
				if (stm!=null)
					stm.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				throw ex;
			}
				
		}
	}
	private Departamento retornaSubDepartamento(Connection conCache, int codigo) throws Exception
	{
		String sql = "";
		Statement stm=null;
		ResultSet rs=null;
		Departamento dep = new Departamento();
		SituacaoEnvioLV sitEnvio2 = new SituacaoEnvioLV();
		sitEnvio2.setId(2);
		
		if (conCache==null)
		{
			try 
			{
				Class.forName("com.intersys.jdbc.CacheDriver");
				conCache = DriverManager.getConnection(
						DsvConstante.getInstanceParametrosSistema().getStrConexaoCache(), DsvConstante.getInstanceParametrosSistema().getUsuarioCache(), DsvConstante.getInstanceParametrosSistema().getSenhaCache());
			} 
			catch (Exception chEx) 
			{
				throw new Exception("Falha na conexão com o caché."
						+ chEx.getMessage());
			}
		}
		
		sql = "SELECT * FROM CESUBDEPAR WHERE SubDeparCodigo = " + codigo;
		stm = conCache.createStatement();
		rs = stm.executeQuery(sql);
		while (rs.next())
		{
			if (rs.getString("SubDeparDescricao").equalsIgnoreCase("Porta Chuteira")==false)
			{
				dep.setAtivo("S");
				dep.setIdSubDepCache(codigo);
				try
				{
					dep.setDescricao(DsvUtilText.firstUpperCase(rs.getString("SubDeparDescricao")));
				}
				catch(Exception exDep)
				{
					exDep.printStackTrace();
				}
				dep.setDepartamentoPai(null);
				dep.setDescricaoCompleta(DsvUtilText.firstUpperCase(new String(rs.getBytes("SubDeparDescricao"),"ISO-8859-1")));
				//dep.setId(-1);
				//dep.setDepartamentoPai(0);
				dep.setIdDepCache(0);
				dep.setIdSubDepCache(codigo);
				dep.setSituacaoEnvioLV(sitEnvio2);
				//dep.setImagemDepartamento("");
				//dep.setLinkExterno(false);
				dep.setOrdemDisposicao(1);
				//dep.setUrlExterna("");
				dep.setSituacaoEnvioLV(null);
			}
		}
		rs.close();
		stm.close();
		
		return dep;
	}
	private Departamento retornaDepartamento(Connection conCache, int codigo) throws Exception
	{
		String sql = "";
		Statement stm=null;
		ResultSet rs=null;
		Departamento dep = new Departamento();
		SituacaoEnvioLV sitEnvio2 = new SituacaoEnvioLV();
		sitEnvio2.setId(2);
		
		if (conCache==null)
		{
			try 
			{
				Class.forName("com.intersys.jdbc.CacheDriver");
				conCache = DriverManager.getConnection(
						DsvConstante.getInstanceParametrosSistema().getStrConexaoCache(), DsvConstante.getInstanceParametrosSistema().getUsuarioCache(), DsvConstante.getInstanceParametrosSistema().getSenhaCache());
			} 
			catch (Exception chEx) 
			{
				throw new Exception("Falha na conexão com o caché."
						+ chEx.getMessage());
			}
		}
		
		sql = "SELECT * FROM CEDEPAR WHERE DeparCodigo = " + codigo;
		stm = conCache.createStatement();
		rs = stm.executeQuery(sql);
		
		while (rs.next())
		{
			
			dep.setAtivo("S");
			dep.setIdSubDepCache(codigo);
			dep.setDescricao(DsvUtilText.firstUpperCase(new String(rs.getBytes("DeparDescricao"),"ISO-8859-1")));
			dep.setDepartamentoPai(null);
			dep.setDescricaoCompleta(DsvUtilText.firstUpperCase(rs.getString("DeparDescricao")));
			//dep.setId(-1);
			//dep.setDepartamentoPai(0);
			dep.setIdDepCache(codigo);
			dep.setIdSubDepCache(0);
			dep.setSituacaoEnvioLV(sitEnvio2);
			//dep.setImagemDepartamento("");
			//dep.setLinkExterno(false);
			dep.setOrdemDisposicao(1);
			//dep.setUrlExterna("");
			dep.setSituacaoEnvioLV(null);
		}
		rs.close();
		stm.close();
		
		return dep;
	}
	
	
	
}
