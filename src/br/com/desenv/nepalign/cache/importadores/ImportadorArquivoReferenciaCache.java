package br.com.desenv.nepalign.cache.importadores;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.hibernate.exception.SQLGrammarException;

import br.com.desenv.nepalign.cache.constantes.Util;
import br.com.desenv.nepalign.model.Cor;
import br.com.desenv.nepalign.model.Fornecedor;
import br.com.desenv.nepalign.model.GrupoProduto;
import br.com.desenv.nepalign.model.Marca;
import br.com.desenv.nepalign.model.OrdemProduto;
import br.com.desenv.nepalign.model.SituacaoEnvioLV;
import br.com.desenv.nepalign.model.Tamanho;
import br.com.desenv.nepalign.model.UnidadeMedida;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;


public class ImportadorArquivoReferenciaCache extends ImportadorCache
{
	public static void main(String[] args)
	{
		final String diretorioBase = args.length > 0x00 ? args[0x00] : "D:\\Desenv\\ARQ_CONV\\";
		
		new ImportadorArquivoReferenciaCache().run(diretorioBase);
	}
	
	enum FILE_METADATA
	{
		REFERENCIA((short) 0),
		COD_BARRAS((short) 1),
		DESCRICAO((short) 2),
		UNIDADE((short) 3),
		TAMANHO((short) 4),
		GRUPO((short) 5),
		MARCA((short) 6),
		DESCRICAO_REDUZIDA((short) 7);
		
		private final short index;
		
		FILE_METADATA(final short index)
		{
			this.index = index;
		}
		
		short getIndex()
		{
			return index;
		}
	}
	
	enum FILE_METADATA_PRECO
	{
		EMPRESA((short) 0),
		COD_BARRAS((short) 2),
		PRECO_VENDA((short) 3);
		
		private final short index;
		
		FILE_METADATA_PRECO(final short index)
		{
			this.index = index;
		}
		
		short getIndex()
		{
			return index;
		}
	}
	
	public void run(final String diretorioBase)
	{
		EntityManager manager = ConexaoUtil.getFactory("importacao", "importacao!123").createEntityManager();
		
		try 
		{
			manager.getTransaction().begin();
			clean(manager);
			manager.getTransaction().commit();
			
			Logger.getLogger(ImportadorArquivoReferenciaCache.this.getClass()).info(String.format("Começando importação com diretório base dos arquivos: %s", diretorioBase));
			
			importarReferencias(manager, readFile(diretorioBase.concat("CADPRODUTO.TXT")));
			importarPrecos(manager, readFile(diretorioBase.concat("SALDOPRO.TXT")));
		}
		catch(InterruptedException ie)
		{
			ie.printStackTrace();
			manager.getTransaction().rollback();
		}
		catch (IOException e) 
		{
			manager.getTransaction().rollback();
			e.printStackTrace();
		}
		finally
		{
			manager.close();
		}
	}
	
	private void clean(final EntityManager manager)
	{
		Logger.getLogger(ImportadorArquivoReferenciaCache.this.getClass()).info("Apagando dados existentes...");
		manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=0;").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE marca;").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE grupoProduto;").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE unidadeMedida;").executeUpdate();
		manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=1;").executeUpdate();
		Logger.getLogger(ImportadorArquivoReferenciaCache.this.getClass()).info("Concluído");
	}
	
	private Cor cor = null;
	private Tamanho tamanho = null;
	private Fornecedor fornecedor = null;
	private OrdemProduto ordemProduto = null;
	private SituacaoEnvioLV situacaoEnvioLV = null;
	
	private void validateCor(final EntityManager manager) throws NoResultException
	{
		cor = manager.find(Cor.class, 0x00);
		
		if(cor == null)
			throw new NoResultException("Não foi possível encontrar a Cor PADRÃO com código 0 no banco de dados!");
	}
	
	private void validateTamanho(final EntityManager manager) throws NoResultException
	{
		tamanho = manager.find(Tamanho.class, 0x00);
		
		if(tamanho == null)
			throw new NoResultException("Não foi possível encontrar o Tamanho PADRÃO com código 0 no banco de dados!");
	}
	
	private void validateOrdemProduto(final EntityManager manager) throws NoResultException
	{
		ordemProduto = manager.find(OrdemProduto.class, 0x00);
		
		if(ordemProduto == null)
			throw new NoResultException("Não foi possível encontrar a OrdemProduto PADRÃO com código 0 no banco de dados!");
	}
	
	private void validateFornecedor(final EntityManager manager) throws NoResultException
	{
		fornecedor = manager.find(Fornecedor.class, 0x00);
		
		if(fornecedor == null)
			throw new NoResultException("Não foi possível encontrar o Fornecedor PADRÃO com código 0 no banco de dados!");
	}
	
	private void validateSituacaoEnvioLV(final EntityManager manager) throws NoResultException
	{
		situacaoEnvioLV = manager.find(SituacaoEnvioLV.class, 0x01);
		
		if(situacaoEnvioLV == null)
			throw new NoResultException("Não foi possível encontrar a Situação de Envio LV PADRÃO com código 0 no banco de dados!");
	}
	
	@SuppressWarnings("unchecked")
	public void importarReferencias(final EntityManager manager, final String data[]) throws InterruptedException
	{
		Logger.getLogger(ImportadorArquivoReferenciaCache.this.getClass()).info("Validando Entidades...");
		validateCor(manager);
		validateTamanho(manager);
		validateOrdemProduto(manager);
		
		validateFornecedor(manager);
		validateSituacaoEnvioLV(manager);
		Logger.getLogger(ImportadorArquivoReferenciaCache.this.getClass()).info("Concluído");
		
		Logger.getLogger(ImportadorArquivoReferenciaCache.this.getClass()).info("Iniciando bloco de importação de Referências");
		
		final List<Marca> listaMarca = (List<Marca>) manager.createNativeQuery("SELECT * FROM marca;", Marca.class).getResultList();
		final List<GrupoProduto> listaGrupoProduto = (List<GrupoProduto>) manager.createNativeQuery("SELECT * FROM grupoProduto;", GrupoProduto.class).getResultList();
		final List<UnidadeMedida> listaUnidadeMedida = (List<UnidadeMedida>) manager.createNativeQuery("SELECT * FROM unidadeMedida;", UnidadeMedida.class).getResultList();
		
		
		class MarcaRunBlock implements Runnable
		{
	        final EntityManager manager;
	        final String[] data;
	        
	        final List<Marca> listaMarcaBuffer;
	        
	    	private void validateMarca(final EntityManager manager, final String metadata[])
	    	{
	    		Marca marca = new Marca();
	    		marca.setDescricao(metadata[FILE_METADATA.MARCA.getIndex()]);
	    		marca.setUltimaAlteracao(new Date());
	    		marca.setFranquia(0x00); // não é franquia
	    		
	    		try
	    		{
	    			synchronized (listaMarcaBuffer) 
	    			{
		    			if(!listaMarca.contains(marca) && !listaMarcaBuffer.contains(marca))
		    				marca = (Marca) manager.createNativeQuery("SELECT * FROM marca WHERE descricao = '" + marca.getDescricao() + "';", Marca.class).getSingleResult();	
					}
	    		}
	    		catch(PersistenceException peException)
	    		{
	    			if(peException.getClass() == NoResultException.class)
	    			{
	    				marca.setCodigo(marca.getId());
	    				listaMarcaBuffer.add(marca);
	    				
	    				//Logger.getLogger(this.getClass()).debug(String.format("Marca [%s] não encontrada! Persistindo objeto e adicionando ao buffer", marca.getDescricao()));
	    			}
	    			else if(peException.getCause().getClass() == SQLGrammarException.class)
	    			{
	    				Logger.getLogger(this.getClass()).error(String.format("Não foi possível localizar a Marca [%s] SQL Utilizado : ", marca.getDescricao(), ((SQLGrammarException) peException.getCause()).getSQL()));
	    				throw peException;
	    			}
	    			else
	    				throw peException;
	    		}
	    	}
	    	
	    	MarcaRunBlock(EntityManager manager, String[] data) 
	        { 
	        	this.manager = ConexaoUtil.getFactory("importacao", "importacao!123").createEntityManager();
	        	this.data = data;
	        	
	        	this.listaMarcaBuffer = new ArrayList<Marca>();
	        }
	    	
	    	@Override
	        public void run() 
	        {
	    		Logger.getLogger(ImportadorArquivoReferenciaCache.this.getClass()).info("Iniciando bloco de importação de Marca");
	    		manager.getTransaction().begin();
	    		
	    		try
	    		{
	    			
	    			class MarcaUnitRunBlock implements Runnable
	    			{
	    		        int startIndex, endIndex;
	    		        final String[] data;
	    		    
	    		        MarcaUnitRunBlock(String[] data, int startIndex, int endIndex)
	    		        {
	    		        	this.data = data;
	    		        	this.startIndex = startIndex;
	    		        	this.endIndex = endIndex;
	    		        }
	    		        
	    				@Override
	    				public void run()
	    				{
	    		    		for(int index = startIndex; index < endIndex; index++)
	    		    		{ 
	    		    			final String metadata = data[index];
	    		    			validateMarca(manager, metadata.split(Util.CACHE_FILE_SEPARATOR));
	    		    		}
	    				}
	    			}
	    			
	    			final List<Thread> marcaUnitsBlock = new ArrayList<Thread>();
	    			
	    		    for(double threadPart = 0.0; threadPart < 0x01; threadPart += 0.01)
	    		    {
	    		    	Thread thread = new Thread(new MarcaUnitRunBlock(data, (int) (data.length * threadPart), (int) (data.length * (threadPart + 0.01))));
	    		    	thread.setName(String.format("Marca - Unit Block %d to %d", (int) (data.length * threadPart),  (int) (data.length * (threadPart + 0.01))));
	    		    	marcaUnitsBlock.add(thread);
	    		    }
	    		    
	    		    for(final Thread unitBlock : marcaUnitsBlock)
	    		    	unitBlock.start();
	    		    
	    		    for(final Thread unitBlock : marcaUnitsBlock)
	    		    	unitBlock.join();
	    		    
	    		    for(final Marca marca : listaMarcaBuffer)
	    		    	manager.persist(marca);	
	    		    
	    		    listaMarca.addAll(listaMarcaBuffer);
		    		
		    		manager.getTransaction().commit();
	    		}
	    		catch(Exception ex)
	    		{
	    			manager.getTransaction().rollback();
	    			ex.printStackTrace();
	    		}
	    		finally
	    		{
	    			manager.close();
		    		Logger.getLogger(ImportadorArquivoReferenciaCache.this.getClass()).info("Bloco de importação de Marca finalizado!");
	    		}
	        }
		}
		
		class GrupoProdutoRunBlock implements Runnable
		{
	        final EntityManager manager;
	        final String[] data;
	        
	    	private void validateGrupoProduto(final EntityManager manager, final String metadata[])
	    	{
	    		GrupoProduto grupoProduto = new GrupoProduto();
	    		grupoProduto.setDescricao(metadata[FILE_METADATA.GRUPO.getIndex()]);
	    		grupoProduto.setUltimaAlteracao(new Date());
	    		try
	    		{
	    			synchronized (listaGrupoProduto)
	    			{
	    				if(!listaGrupoProduto.contains(grupoProduto))
	    					grupoProduto = (GrupoProduto) manager.createNativeQuery("SELECT * FROM grupoProduto WHERE descricao = '" + metadata[FILE_METADATA.GRUPO.getIndex()] + "';", GrupoProduto.class).getSingleResult();
	    			}
	    		}
	    		catch(NoResultException nrException)
	    		{
	    			manager.persist(grupoProduto);
	    			grupoProduto.setCodigo(grupoProduto.getId());
	    			listaGrupoProduto.add(grupoProduto);
	    			
	    			//Logger.getLogger(this.getClass()).debug(String.format("Grupo Produto [%s] não encontrado! Persistindo objeto e adicionando ao buffer", grupoProduto.getDescricao()));
	    		}
	    	}
	    	
	    	GrupoProdutoRunBlock(EntityManager manager, String[] data) 
	        { 
	        	this.manager = ConexaoUtil.getFactory("importacao", "importacao!123").createEntityManager();
	        	this.data = data;
	        }
	    	
	    	@Override
	        public void run() 
	        {
	    		Logger.getLogger(ImportadorArquivoReferenciaCache.this.getClass()).info("Iniciando bloco de importação de GrupoProduto");
	    		manager.getTransaction().begin();
	    		
	    		try
	    		{
		    		for(final String metadata : data)
		    			validateGrupoProduto(manager, metadata.split(Util.CACHE_FILE_SEPARATOR));
		    		manager.getTransaction().commit();
	    		}
	    		catch(Exception ex)
	    		{
	    			manager.getTransaction().rollback();
	    			ex.printStackTrace();
	    		}
	    		finally
	    		{
	    			manager.close();
		    		Logger.getLogger(ImportadorArquivoReferenciaCache.this.getClass()).info("Bloco de importação de GrupoProduto finalizado!");
	    		}
	        }
		}
		
		class UnidadeMedidaRunBlock implements Runnable
		{
	        final EntityManager manager;
	        final String[] data;
	        
	    	private void validateUnidadeMedida(final EntityManager manager, final String metadata[])
	    	{
	    		UnidadeMedida unidadeMedida = new UnidadeMedida();
	    		unidadeMedida.setDescricao(metadata[FILE_METADATA.UNIDADE.getIndex()]);
	    		unidadeMedida.setSigla(metadata[FILE_METADATA.UNIDADE.getIndex()].substring(0, Math.min(3, metadata[FILE_METADATA.UNIDADE.getIndex()].length())));
	    		
	    		try
	    		{
    				synchronized (listaUnidadeMedida) 
    				{
    	    			if(!listaUnidadeMedida.contains(unidadeMedida))
    	    				unidadeMedida = (UnidadeMedida) manager.createNativeQuery("SELECT * FROM unidadeMedida WHERE descricao = '" + unidadeMedida.getDescricao() + "';", UnidadeMedida.class).getSingleResult();
    				}
	    		}
	    		catch(PersistenceException peException)
	    		{
	    			if(peException.getClass() == NoResultException.class)
	    			{
	    				manager.persist(unidadeMedida);
	    				listaUnidadeMedida.add(unidadeMedida);
	    				
	    				Logger.getLogger(this.getClass()).debug(String.format("UnidadeMedida [%s] não encontrada! Persistindo objeto e adicionando ao buffer", unidadeMedida.getDescricao()));
	    			}
	    			else if(peException.getCause().getClass() == SQLGrammarException.class)
	    			{
	    				Logger.getLogger(this.getClass()).error(String.format("Não foi possível localizar a Marca [%s] SQL Utilizado : ", unidadeMedida.getDescricao(), ((SQLGrammarException) peException.getCause()).getSQL()));
	    				throw peException;
	    			}
	    			else
	    				throw peException;
	    		}
	    	}
	    	
	    	UnidadeMedidaRunBlock(EntityManager manager, String[] data) 
	        { 
	        	this.manager = ConexaoUtil.getFactory("importacao", "importacao!123").createEntityManager();
	        	this.data = data;
	        }
	    	
	    	@Override
	        public void run() 
	        {
	    		Logger.getLogger(ImportadorArquivoReferenciaCache.this.getClass()).info("Iniciando bloco de importação de UnidadeMedida");
	    		manager.getTransaction().begin();
	    		
	    		try
	    		{
		    		for(final String metadata : data)
		    			validateUnidadeMedida(manager, metadata.split(Util.CACHE_FILE_SEPARATOR));
		    		manager.getTransaction().commit();
	    		}
	    		catch(Exception ex)
	    		{
	    			manager.getTransaction().rollback();
	    			ex.printStackTrace();
	    		}
	    		finally
	    		{
	    			manager.close();
		    		Logger.getLogger(ImportadorArquivoReferenciaCache.this.getClass()).info("Bloco de importação de UnidadeMedida finalizado!");
	    		}
	        }
		}
		
		final Thread marcaBlockThread = new Thread(new MarcaRunBlock(manager, data));
		marcaBlockThread.setName("Importação Marca");
		marcaBlockThread.start();
		
		final Thread grupoProdutoBlockThread = new Thread(new GrupoProdutoRunBlock(manager, data));
		grupoProdutoBlockThread.setName("Importação GrupoProduto");
		grupoProdutoBlockThread.start();
		
		final Thread unidadeMedidaBlockThread = new Thread(new UnidadeMedidaRunBlock(manager, data));
		unidadeMedidaBlockThread.setName("Importação UnidadeMedida");
		unidadeMedidaBlockThread.start();
		
		unidadeMedidaBlockThread.join();
		grupoProdutoBlockThread.join();
		marcaBlockThread.join();
		
		manager.getTransaction().begin();
		manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=0;").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE produto;").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE estoqueProduto;").executeUpdate();
		manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=1;").executeUpdate();
		manager.getTransaction().commit();
		
		final Counter counter = new Counter();
		
	    class RunBlock implements Runnable 
	    {
	        int startIndex, endIndex;
	        Connection connection;
	        final String[] data;
	        
	        final Marca recuperarMarca(final String descricaoMarca) throws Exception
	        {
	        	for(final Marca marca : listaMarca)
	        	{
	        		if(marca.getDescricao().equalsIgnoreCase(descricaoMarca))
	        			return marca;
	        	}
	        	throw new Exception("Não existe Marca para a descrição " + descricaoMarca);
	        }
	        
	        final GrupoProduto recuperarGrupoProduto(final String descricaoGrupoProduto) throws Exception
	        {
	        	for(final GrupoProduto grupoProduto : listaGrupoProduto)
	        	{
	        		if(grupoProduto.getDescricao().equalsIgnoreCase(descricaoGrupoProduto))
	        			return grupoProduto;
	        	}
	        	throw new Exception("Não existe GrupoProduto para a descrição " + descricaoGrupoProduto);
	        }
	        
	        final UnidadeMedida recuperarUnidadeMedida(final String descricaoUnidadeMedida) throws Exception
	        {
	        	for(final UnidadeMedida unidadeMedida : listaUnidadeMedida)
	        	{
	        		if(unidadeMedida.getDescricao().equalsIgnoreCase(descricaoUnidadeMedida))
	        			return unidadeMedida;
	        	}
	        	throw new Exception("Não existe UnidadeMedida para a descrição " + descricaoUnidadeMedida);
	        }
	    
	        RunBlock(EntityManager manager, String[] data, int startIndex, int endIndex) 
	        { 
	        	this.connection = ConexaoUtil.getConexaoPadrao("importacao", "importacao!123");
	        	this.startIndex = startIndex;
	        	this.endIndex = endIndex;
	        	this.data = data;
	        }
	        
	        public void run() 
	        {   			
    			try
    			{
	    			PreparedStatement ps = connection.prepareStatement("INSERT INTO produto (idProduto, codigoProduto, nomeProduto, idMarca, idGrupoProduto, idUnidadeMedida," +
	    					"idFornecedor, idSituacaoEnvioLV, ativoInativo, ultimaAteracao, produtoLV) VALUES (?, ?, ?, ?, ?, ?," +
	    					"?, ?, ?, ?, ?);");
	    			
	    			PreparedStatement psEstoqueProduto = connection.prepareStatement("INSERT INTO estoqueProduto (idEstoqueProduto, codigoBarras, idProduto, idCor, idTamanho, idOrdemProduto," +
	    					"idSituacaoEnvioLV) VALUES (?, ?, ?, ?, ?, ?, ?); ");
	    			
    	    		for(int index = startIndex; index < endIndex; index++)
    	    		{ 
    	    			final String[] metadata = data[index].split(Util.CACHE_FILE_SEPARATOR);
    	    			
    	    			ps.setInt(1, Integer.parseInt(metadata[FILE_METADATA.REFERENCIA.getIndex()]));
    	    			ps.setString(2, metadata[FILE_METADATA.REFERENCIA.getIndex()]);
    	    			ps.setString(3, metadata[FILE_METADATA.DESCRICAO.getIndex()]);
    	    			ps.setInt(4, recuperarMarca(metadata[FILE_METADATA.MARCA.getIndex()].trim()).getId());
    	    			ps.setInt(5, recuperarGrupoProduto(metadata[FILE_METADATA.GRUPO.getIndex()].trim()).getId());
    	    			ps.setInt(6, recuperarUnidadeMedida(metadata[FILE_METADATA.UNIDADE.getIndex()].trim()).getId());
    	    			ps.setInt(7, 0);
    	    			ps.setInt(8, 1);
    	    			ps.setString(9, "A");
    	    			ps.setString(10, "2017-03-21 00:00:00");
    	    			ps.setString(11, "N");
    	    			
    	    			psEstoqueProduto.setInt(1, Integer.parseInt(metadata[FILE_METADATA.COD_BARRAS.getIndex()]));
    	    			psEstoqueProduto.setString(2, metadata[FILE_METADATA.COD_BARRAS.getIndex()]);
    	    			psEstoqueProduto.setInt(3, Integer.parseInt(metadata[FILE_METADATA.REFERENCIA.getIndex()]));
    	    			psEstoqueProduto.setInt(4, 0);
    	    			psEstoqueProduto.setInt(5, 0);
    	    			psEstoqueProduto.setInt(6, 0);
    	    			psEstoqueProduto.setInt(7, 1);
    	    			
    	    			ps.executeUpdate();
    	    			psEstoqueProduto.executeUpdate();
    	    			
    	    			synchronized (counter)
    	    			{
    	    				counter.increment();
						}
    	    		}
    			}
    			catch(Exception ex)
    			{
    				ex.printStackTrace();
    			}
    			finally
    			{
    				try 
    				{
						connection.close();
					} 
    				catch (SQLException e) 
    				{
						e.printStackTrace();
					}
    			}
	        } 
	    }   
	    
	    List<Thread> runBlockList = new ArrayList<Thread>();
	    
	    Thread firstBlock = new Thread(new RunBlock(null, data, 0x01, (int) (data.length * 0.01)));
	    firstBlock.setName(String.format("Referencia - Unit Block %d to %d", 1, (int) (data.length * 0.01)));
	    firstBlock.start();
	    runBlockList.add(firstBlock);
	     
	    for(double threadPart = 0.01; threadPart < 0x01; threadPart += 0.01)
	    {
	    	Thread thread = new Thread(new RunBlock(null, data, (int) (data.length * threadPart), (int) (data.length * (threadPart + 0.01))));
	    	thread.setName(String.format("Referencia - Unit Block %d to %d", (int) (data.length * threadPart),  (int) (data.length * (threadPart + 0.01))));
	    	thread.start();
	    	runBlockList.add(thread);
	    }
	    
	    Thread counterThread  = new Thread(new Runnable() 
	    {
			@Override
			public void run() 
			{
			    while(!Thread.currentThread().isInterrupted())
			    {
			    	try 
			    	{
						Thread.sleep(1500);
						
						synchronized (counter) 
						{
							Logger.getLogger(ImportadorArquivoReferenciaCache.this.getClass()).info(String.format("%s referencias processadas", counter));
						}
			    	}
			    	catch (InterruptedException e) 
			    	{
			    		Thread.currentThread().interrupt();
					}
			    }
			}
		});
	    counterThread.start();
	    
	    for(final Thread unitBlock : runBlockList)
	    	unitBlock.join();
	    
	    counterThread.interrupt();
	    
	    Logger.getLogger(ImportadorArquivoReferenciaCache.this.getClass()).info("Bloco de importação de Referencia finalizado!");
	}
	
	private final Calendar calendar = Calendar.getInstance();
	
	private void importarPrecos(final EntityManager manager, final String data[]) throws InterruptedException
	{
		Logger.getLogger(ImportadorArquivoReferenciaCache.this.getClass()).info("Iniciando bloco de importação de Preços!");
		
		manager.getTransaction().begin();
		manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=0;").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE saldoEstoqueProduto;").executeUpdate();
		manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=1;").executeUpdate();
		manager.getTransaction().commit();
		
		calendar.setTime(new Date());
		
		final Counter counter = new Counter();
		class SaldoEstoqueProdutoRunBlock implements Runnable 
	    {
	        int startIndex, endIndex;
	        Connection connection;
	        final String[] data;
	    
	        SaldoEstoqueProdutoRunBlock(EntityManager manager, String[] data, int startIndex, int endIndex) 
	        { 
	        	this.connection = ConexaoUtil.getConexaoPadrao("importacao", "importacao!123");
	        	this.startIndex = startIndex;
	        	this.endIndex = endIndex;
	        	this.data = data;
	        }
	        
	        public void run() 
	        {   			
    			try
    			{
	    			PreparedStatement psSaldoEstoqueProduto = connection.prepareStatement("INSERT INTO saldoEstoqueProduto (idEmpresaFisica, idEstoqueProduto, idSituacaoEnvioLV, mes, ano, venda," +
	    					"dataUltimaAlteracao) VALUES (?, ?, ?, ?, ?, ?, ?); ");
	    			
    	    		for(int index = startIndex; index < endIndex; index++)
    	    		{ 
    	    			final String[] metadata = data[index].split(Util.CACHE_FILE_SEPARATOR, -1);
    	    			
    	    			final String precoVenda = metadata[FILE_METADATA_PRECO.PRECO_VENDA.getIndex()];
    	    			
    	    			try
    	    			{
        	    			psSaldoEstoqueProduto.setInt(1, Integer.parseInt(metadata[FILE_METADATA_PRECO.EMPRESA.getIndex()]));
        	    			psSaldoEstoqueProduto.setInt(2, Integer.parseInt(metadata[FILE_METADATA_PRECO.COD_BARRAS.getIndex()]));
        	    			psSaldoEstoqueProduto.setInt(3, situacaoEnvioLV.getId());
        	    			psSaldoEstoqueProduto.setInt(4, calendar.get(Calendar.MONTH + 0x01));
        	    			psSaldoEstoqueProduto.setInt(5, calendar.get(Calendar.YEAR));
        	    			psSaldoEstoqueProduto.setDouble(6, precoVenda.equals("") ? 0.0 : Double.parseDouble(precoVenda));
        	    			psSaldoEstoqueProduto.setString(7, sqlDateFormat.format(new Date()));
        	    			
        	    			psSaldoEstoqueProduto.executeUpdate();        	    			
    	    			}
    	    			catch(Exception ex)
    	    			{
    	    				Logger.getLogger(ImportadorArquivoReferenciaCache.class).error(String.format("Não foi possível criar preço de venda para o código de barras %s", metadata[FILE_METADATA_PRECO.COD_BARRAS.getIndex()]), ex);
    	    			}
    	    			counter.increment();
    	    		}
    			}
    			catch(Exception ex)
    			{
    				ex.printStackTrace();
    			}
    			finally
    			{
    				try 
    				{
						connection.close();
					} 
    				catch (SQLException e) 
    				{
						e.printStackTrace();
					}
    			}
	        } 
	    }   
	    
	    List<Thread> runBlockList = new ArrayList<Thread>();
	    
	    Thread firstBlock = new Thread(new SaldoEstoqueProdutoRunBlock(null, data, 0x01, (int) (data.length * 0.01)));
	    firstBlock.setName(String.format("SaldoEstoqueProduto - Unit Block %d to %d", 1, (int) (data.length * 0.01)));
	    firstBlock.start();
	    runBlockList.add(firstBlock);
	     
	    for(double threadPart = 0.01; threadPart < 0x01; threadPart += 0.01)
	    {
	    	Thread thread = new Thread(new SaldoEstoqueProdutoRunBlock(null, data, (int) (data.length * threadPart), (int) (data.length * (threadPart + 0.01))));
	    	thread.setName(String.format("SaldoEstoqueProduto - Unit Block %d to %d", (int) (data.length * threadPart),  (int) (data.length * (threadPart + 0.01))));
	    	thread.start();
	    	runBlockList.add(thread);
	    }
	    
	    Thread counterThread  = new Thread(new Runnable() 
	    {
			@Override
			public void run() 
			{
			    while(!Thread.currentThread().isInterrupted())
			    {
			    	try 
			    	{
						Thread.sleep(1500);
						
						synchronized (counter) 
						{
							Logger.getLogger(ImportadorArquivoReferenciaCache.this.getClass()).info(String.format("%s preços processadas", counter));
						}
			    	}
			    	catch (InterruptedException e) 
			    	{
			    		Thread.currentThread().interrupt();
					}
			    }
			}
		});
	    counterThread.start();
	    
	    for(final Thread unitBlock : runBlockList)
	    	unitBlock.join();
	    
	    counterThread.interrupt();
	    
	    Logger.getLogger(ImportadorArquivoReferenciaCache.this.getClass()).info("Bloco de importação de Preços finalizado!");
	}
}