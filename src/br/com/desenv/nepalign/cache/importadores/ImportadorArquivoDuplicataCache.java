package br.com.desenv.nepalign.cache.importadores;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.apache.log4j.Logger;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.cache.constantes.Util;
import br.com.desenv.nepalign.model.Cliente;
import br.com.desenv.nepalign.model.Duplicata;
import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.nepalign.model.Vendedor;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ImportadorArquivoDuplicataCache extends ImportadorCache
{
	public static void main(String[] args)
	{
		final String diretorioBase = args.length > 0x00 ? args[0x00] : "D:\\Desenv\\ARQ_CONV\\";
		
		new ImportadorArquivoDuplicataCache().run(diretorioBase);
	}
	
	public static List<Object[]> deParaNumero = new ArrayList<Object[]>();
	
	public static synchronized void addDePara(String antigo, Integer novo, Integer idCliente, PreparedStatement ps) throws SQLException
	{
		synchronized (deParaNumero) 
		{
			deParaNumero.add(new Object[] { antigo, novo, idCliente });
			synchronized (ps) 
			{
				ps.setString(1, antigo);
				ps.setInt(2, novo);
				ps.setInt(3, idCliente);
				ps.executeUpdate();
			}
		}
	}
	
	public static synchronized Integer getNumeroDuplicata(String numeroAntigo, Integer idCliente) throws Exception
	{
		synchronized (deParaNumero) 
		{
			for(Object[] value : deParaNumero)
			{
				if(((String) value[0]).equals(numeroAntigo) && ((Integer) value[2]).equals(idCliente))
					return ((Integer) value[1]);
			}
		}
		throw new Exception("Não foi possível encontrar o numero de documento novo para " + numeroAntigo);
	}
	
    private static Object numDupLocker = new Object();
    
    private static int numeroDuplicataNova = 10000000;
    
    public static synchronized int getNovoNumeroDuplicata()
    {
    	synchronized(numDupLocker)
    	{
    		numeroDuplicataNova++;
    		return numeroDuplicataNova;
    	}
    }
	
	enum FILE_METADATA_DUP
	{
		LOJA((short) 0),
		CLIENTE((short) 1),
		NUMERO_DUPLICATA((short) 2),
		DATA_COMPRA((short) 3),
		NOTA_FISCAL((short) 4),
		VENDEDOR((short) 5),
		VALOR_TOTAL_COMPRA((short) 6),
		VALOR_ENTRADA((short) 7),
		TOTAL_PRESTACOES((short) 8);
		
		private final short index;
		
		FILE_METADATA_DUP(final short index)
		{
			this.index = index;
		}
		
		short getIndex()
		{
			return index;
		}
	}
	
	enum FILE_METADATA_DUPPARC
	{
		LOJA((short) 0),
		CLIENTE((short) 1),
		NUMERO_DUPLICATA((short) 2),
		NUMERO_PARCELA((short) 3),
		DATA_VENCIMENTO((short) 4),
		VALOR_PARCELA((short) 5),
		SITUACAO_PARCELA((short) 6),
		VALOR_PAGO((short) 7),
		DATA_BAIXA((short) 8),
		SEGUNDO_VENCIMENTO((short) 9);
		
		private final short index;
		
		FILE_METADATA_DUPPARC(final short index)
		{
			this.index = index;
		}
		
		short getIndex()
		{
			return index;
		}
	}
	
	enum ENTIDADE_PADRAO
	{
		CLIENTE(0),
		USUARIO(1),
		VENDEDOR(0);
		
		private final int codigo;
		
		ENTIDADE_PADRAO(final int codigo)
		{
			this.codigo = codigo;
		}
		
		int getCodigo()
		{
			return codigo;
		}
	}
	
	private void validateCliente(final EntityManager manager) throws NoResultException
	{
		if(manager.find(Cliente.class, ENTIDADE_PADRAO.CLIENTE.getCodigo()) == null)
			throw new NoResultException(String.format("Não foi possível encontrar o Cliente PADRÃO com código %d no banco de dados!", ENTIDADE_PADRAO.CLIENTE.getCodigo()));
	}
	
	private void validateUsuario(final EntityManager manager) throws NoResultException
	{
		if(manager.find(Usuario.class, ENTIDADE_PADRAO.USUARIO.getCodigo()) == null)
			throw new NoResultException(String.format("Não foi possível encontrar o Usuario com código %d no banco de dados!", ENTIDADE_PADRAO.USUARIO.getCodigo()));
	}
	
	private void validateVendedor(final EntityManager manager) throws NoResultException
	{
		if(manager.find(Vendedor.class, ENTIDADE_PADRAO.VENDEDOR.getCodigo()) == null)
			throw new NoResultException(String.format("Não foi possível encontrar o Vendedor com código %d no banco de dados!", ENTIDADE_PADRAO.VENDEDOR.getCodigo()));
	}
	
	public void run(final String diretorioBase)
	{
		EntityManager manager = ConexaoUtil.getFactory("importacao", "importacao!123").createEntityManager();
		try 
		{
			manager.getTransaction().begin();
			clean(manager);
			manager.getTransaction().commit();
			
			Logger.getLogger(ImportadorArquivoDuplicataCache.this.getClass()).info("Validando Entidades...");
			validateCliente(manager);
			validateUsuario(manager);
			validateVendedor(manager);
			Logger.getLogger(ImportadorArquivoDuplicataCache.this.getClass()).info("Concluído");
			
			Logger.getLogger(ImportadorArquivoDuplicataCache.this.getClass()).info(String.format("Começando importação com diretório base dos arquivos: %s", diretorioBase));
			
			importarDuplicata(manager, readFile(diretorioBase.concat("DUPLICATAp1.TXT")));
			importarDuplicataParcela(manager, readFile(diretorioBase.concat("DUPLICATAp2.TXT")));
		}
		catch(InterruptedException ie)
		{
			ie.printStackTrace();
			manager.getTransaction().rollback();
		}
		catch (IOException e) 
		{
			manager.getTransaction().rollback();
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			manager.close();
		}
	}
	
	private void clean(final EntityManager manager)
	{
		Logger.getLogger(ImportadorArquivoDuplicataCache.this.getClass()).info("Apagando dados existentes...");
		manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=0;").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE movimentoparceladuplicata;").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE duplicataparcela;").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE duplicata;").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE deparaduplicata;").executeUpdate();
		manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=1;").executeUpdate();
		Logger.getLogger(ImportadorArquivoDuplicataCache.this.getClass()).info("Concluído");
	}

	final List<Duplicata> listaDuplicata = new ArrayList<Duplicata>();
	
	@SuppressWarnings("unchecked")
	public void importarDuplicata(final EntityManager manager, final String data[]) throws InterruptedException
	{
		Logger.getLogger(ImportadorArquivoDuplicataCache.this.getClass()).info("Iniciando bloco de importação de Duplicata");
		
		final List<Vendedor> listaVendedor = manager.createNativeQuery("SELECT * FROM VENDEDOR", Vendedor.class).getResultList();
		
		final Counter counter = new Counter();
		
	    class RunBlock implements Runnable 
	    {
	        int startIndex, endIndex;
	        Connection connection;
	        final String[] data;
	        
	        RunBlock(String[] data, int startIndex, int endIndex) 
	        { 
	        	this.connection = ConexaoUtil.getConexaoPadrao("importacao", "importacao!123");
	        	this.startIndex = startIndex;
	        	this.endIndex = endIndex;
	        	this.data = data;
	        }
	        
	        final Vendedor getVendedor(final String codigoVendedorCache) throws Exception
	        {
	        	synchronized (listaVendedor) 
	        	{
					for(final Vendedor vendedor : listaVendedor)
					{
						if(vendedor.getCodigoVendedorCache().equals(Integer.parseInt(codigoVendedorCache)))
							return vendedor;
					}
				}
	        	throw new Exception(String.format("Não foi possível encontrar o vendedor com o código %s", codigoVendedorCache));
	        }

			@Override
			public void run() 
			{
				try 
				{
					PreparedStatement ps = connection.prepareStatement("INSERT INTO duplicata (idEmpresaFisica, idCliente, numeroDuplicata, numeroFatura, dataCompra, dataHoraInclusao, numeroNotaFiscal, idUsuario, idVendedor," +
							"valorTotalCompra, valorEntrada, numeroTotalParcela, semPedidoVenda, observacao, jorrovi) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);",
                            Statement.RETURN_GENERATED_KEYS);
					
					PreparedStatement deParaPS = connection.prepareStatement("INSERT INTO deparaduplicata (numeroAntigo, numeroNovo, idCliente) VALUES (?, ?, ?);");
					
    	    		for(int index = startIndex; index < endIndex; index++)
    	    		{ 
    	    			try
    	    			{
        	    			final String[] metadata = data[index].split(Util.CACHE_FILE_SEPARATOR, -1);
        	    			
        	    			int numeroDuplicata = 0;
        	    			int idCliente = Integer.parseInt(metadata[FILE_METADATA_DUP.CLIENTE.getIndex()].trim());
        	    			String dataCompra = metadata[FILE_METADATA_DUP.DATA_COMPRA.getIndex()].trim();
        	    			
        	    			if(!dataCompra.equals("31/08/2017"))
        	    				continue;
        	    			
        	    			String observacao = "IMPORTADA DO CACHE ";
        	    			
        	    			try
        	    			{
        	    				numeroDuplicata = Integer.parseInt(metadata[FILE_METADATA_DUP.NUMERO_DUPLICATA.getIndex()].trim());
        	    			}
        	    			catch(Exception ex)
        	    			{
        	    				numeroDuplicata = getNovoNumeroDuplicata();
        	    				observacao += " NÚMERO ANTIGO " + metadata[FILE_METADATA_DUP.NUMERO_DUPLICATA.getIndex()].trim();
        	    				addDePara(metadata[FILE_METADATA_DUP.NUMERO_DUPLICATA.getIndex()].trim(), numeroDuplicata, idCliente, deParaPS);
        	    			}
        	    		
        	    			ps.setInt(1, Integer.parseInt(metadata[FILE_METADATA_DUP.LOJA.getIndex()].trim()));
        	    			ps.setInt(2, idCliente);
        	    			ps.setInt(3, numeroDuplicata);
        	    			ps.setInt(4, numeroDuplicata);
        	    			
        	    			synchronized (sqlDateFormat)
        	    			{
        	    				if(dataCompra.equals(""))
        	    					dataCompra = "01/01/1990";
        	    				
        	    				ps.setString(5, sqlDateFormat.format(dateFormat.parse(dataCompra)));
        	    				ps.setString(6, sqlDateFormat.format(dateFormat.parse(dataCompra)));
        	    			}
        	    			
        	    			String notaFiscal = metadata[FILE_METADATA_DUP.NOTA_FISCAL.getIndex()].trim();
        	    			if(notaFiscal == null || notaFiscal.isEmpty() || notaFiscal.equals("."))
        	    				ps.setNull(7, Types.INTEGER);
        	    			else
        	    			{
        	    				try
        	    				{
        	    					ps.setInt(7, Integer.parseInt(notaFiscal));	
        	    				}
        	    				catch(Exception ex)
        	    				{
        	    					ps.setNull(7, Types.INTEGER);
        	    					observacao += " NOTA FISCAL ANTIGA " + notaFiscal;
        	    				}
        	    			}
        	    			
    	    				ps.setInt(8, ENTIDADE_PADRAO.USUARIO.getCodigo());
    	    				
    	    				String codigoVendedorCache = metadata[FILE_METADATA_DUP.VENDEDOR.getIndex()];
    	    				if(codigoVendedorCache == null || codigoVendedorCache.isEmpty())
    	    					ps.setInt(9, ENTIDADE_PADRAO.VENDEDOR.getCodigo());
    	    				else
        	    				ps.setInt(9, getVendedor(codigoVendedorCache).getId());
    	    				
        	    			ps.setDouble(10, Double.parseDouble(metadata[FILE_METADATA_DUP.VALOR_TOTAL_COMPRA.getIndex()]));
        	    			
        	    			String valorEntrada = metadata[FILE_METADATA_DUP.VALOR_ENTRADA.getIndex()];
        	    			if(valorEntrada == null || valorEntrada.isEmpty())
        	    				ps.setNull(11, Types.DOUBLE);
        	    			else
        	    				ps.setDouble(11, Double.parseDouble(valorEntrada));
        	    			
        	    			ps.setInt(12, Integer.parseInt(metadata[FILE_METADATA_DUP.TOTAL_PRESTACOES.getIndex()]));
        	    			ps.setString(13, "S");
        	    			ps.setString(14, observacao);
        	    			ps.setInt(15, 0);
        	    			
        	    			synchronized (connection) 
        	    			{
            	    			ps.executeUpdate();
							}

        	    			Duplicata duplicata = new Duplicata();
        	    			duplicata.setNumeroDuplicata((long) numeroDuplicata);
        	    			duplicata.setDataCompra(dateFormat.parse(dataCompra));
        	    			
        	    			try (ResultSet generatedKeys = ps.getGeneratedKeys()) 
        	    			{
        	    	            if (generatedKeys.next()) 
        	    	            	duplicata.setId(generatedKeys.getInt(1));
        	    	            else 
        	    	                throw new Exception("Creating Duplicata failed, no ID obtained.");
        	    	        }
        	    			
        	    			synchronized (listaDuplicata) 
        	    			{
        	    				listaDuplicata.add(duplicata);
            	    			counter.increment();
							}

	    	    		}
	    				catch (Exception e) 
	    				{
	    					Logger.getLogger(ImportadorArquivoDuplicataCache.class).error(String.format("Erro ao processar a linha %s. Mensagem: %s", data[index], e.getMessage()), e);
	    					//throw e;
	    				}
    	    		}
				}
				catch (Exception e) 
				{
					
				} 
			}
	    }
	    
	    List<Thread> runBlockList = new ArrayList<Thread>();
	    
	    Thread firstBlock = new Thread(new RunBlock( data, 0x01, (int) (data.length * 0.01)));
	    firstBlock.setName(String.format("Duplicata - Unit Block %d to %d", 1, (int) (data.length * 0.01)));
	    firstBlock.start();
	    runBlockList.add(firstBlock);
	     
	    for(double threadPart = 0.01; threadPart < 0x01; threadPart += 0.01)
	    {
	    	Thread thread = new Thread(new RunBlock( data, (int) (data.length * threadPart), (int) (data.length * (threadPart + 0.01))));
	    	thread.setName(String.format("Duplicata - Unit Block %d to %d", (int) (data.length * threadPart),  (int) (data.length * (threadPart + 0.01))));
	    	thread.start();
	    	runBlockList.add(thread);
	    }
	    
	    Thread counterThread  = new Thread(new Runnable() 
	    {
			@Override
			public void run() 
			{
			    while(!Thread.currentThread().isInterrupted())
			    {
			    	try 
			    	{
						synchronized (counter) 
						{
							Logger.getLogger(ImportadorArquivoDuplicataCache.this.getClass()).info(String.format("%s duplicatas processadas", counter));
						}
						Thread.sleep(1500);
			    	}
			    	catch (InterruptedException e) 
			    	{
			    		Thread.currentThread().interrupt();
					}
			    }
			}
		});
	    counterThread.start();
	    
	    for(final Thread unitBlock : runBlockList)
	    	unitBlock.join();
	    
	    counterThread.interrupt();
	    Logger.getLogger(ImportadorArquivoDuplicataCache.this.getClass()).info("Bloco de importação de Duplicatas finalizado!");
	}
	
	public void importarDuplicataParcela(final EntityManager manager, final String data[]) throws InterruptedException
	{
		Logger.getLogger(ImportadorArquivoDuplicataCache.this.getClass()).info("Iniciando bloco de importação de Duplicata Parcela");
		
		final Counter counter = new Counter();
		
	    class RunBlock implements Runnable 
	    {
	        int startIndex, endIndex;
	        Connection connection;
	        final String[] data;
	        
	        RunBlock(String[] data, int startIndex, int endIndex) 
	        { 
	        	this.connection = ConexaoUtil.getConexaoPadrao("importacao", "importacao!123");
	        	this.startIndex = startIndex;
	        	this.endIndex = endIndex;
	        	this.data = data;
	        }
	        
	        final Duplicata getDuplicata(final String numeroDuplicata) throws Exception
	        {
	        	synchronized (listaDuplicata) 
	        	{
					for(final Duplicata duplicata : listaDuplicata)
					{
						if(duplicata.getNumeroDuplicata().equals(Long.parseLong(numeroDuplicata)))
							return duplicata;
					}
				}
	        	throw new Exception(String.format("Não foi possível encontrar a duplicata com o número %s", numeroDuplicata));
	        }
	        
	        final void gerarMovimentoParcelaDuplicata(PreparedStatement ps, Duplicata duplicata, Integer idDuplicataParcela, String[] metadata, String situacao) throws Exception
	        {
	        	ps.setInt(1, duplicata.getId());
	        	ps.setInt(2, idDuplicataParcela);
    			ps.setInt(3, Integer.parseInt(metadata[FILE_METADATA_DUPPARC.LOJA.getIndex()].trim()));
    			ps.setInt(4, Integer.parseInt(metadata[FILE_METADATA_DUPPARC.CLIENTE.getIndex()].trim()));
    			ps.setInt(5, ENTIDADE_PADRAO.USUARIO.getCodigo());
    			ps.setInt(6, duplicata.getNumeroDuplicata().intValue());
    			ps.setInt(7, Integer.parseInt(metadata[FILE_METADATA_DUPPARC.NUMERO_PARCELA.getIndex()].trim()));
    			
    			synchronized (sqlDateFormat)
    			{
    				String dataBaixa = metadata[FILE_METADATA_DUPPARC.DATA_BAIXA.getIndex()].trim();
    				if(dataBaixa.equals(""))
    					dataBaixa = "01/01/1990";
    				ps.setString(8, sqlDateFormat.format(dateFormat.parse(dataBaixa)));
    			}
    			
    			ps.setInt(9, Integer.parseInt(situacao));
    			ps.setDouble(10, Double.parseDouble(metadata[FILE_METADATA_DUPPARC.VALOR_PARCELA.getIndex()]));
    			ps.setDouble(11, Double.parseDouble(metadata[FILE_METADATA_DUPPARC.VALOR_PARCELA.getIndex()]));
    			ps.setDouble(12, Double.parseDouble(metadata[FILE_METADATA_DUPPARC.VALOR_PARCELA.getIndex()]));
    			ps.executeUpdate();
	        }

			@Override
			public void run() 
			{
				try 
				{
					PreparedStatement ps = connection.prepareStatement("INSERT INTO duplicataParcela (idDuplicata, idEmpresaFisica, idCliente, idUsuario, numeroDuplicata, numeroParcela, dataVencimento, valorParcela, situacao," +
							"valorPago, dataBaixa, dataVencimento02Acordado, tipoPagamento, jurosPago) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, 0.00)",
                            Statement.RETURN_GENERATED_KEYS); 
					PreparedStatement psMovimento = connection.prepareStatement("INSERT INTO movimentoparceladuplicata (idDuplicata, idDuplicataParcela, idEmpresaFisica, idCliente, idUsuario, numeroDuplicata, numeroParcela, dataMovimento," +
							"tipoPagamento, valorRecebido, valorCompraPago, valorParcela, valorentradajuros) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0.00);");
					PreparedStatement psAtualizaDuplicata = connection.prepareStatement("UPDATE duplicata set dataVencimentoPrimeiraPrestacao = ? WHERE idDuplicata = ?");
					
    	    		for(int index = startIndex; index < endIndex; index++)
    	    		{ 
    	    			try
    	    			{
        	    			final String[] metadata = data[index].split(Util.CACHE_FILE_SEPARATOR, -1);
        	    			
        	    			String numeroDuplicata;
        	    			Integer idCliente = Integer.parseInt(metadata[FILE_METADATA_DUPPARC.CLIENTE.getIndex()].trim());
        	    			
        	    			try
        	    			{
        	    				numeroDuplicata = String.valueOf(Integer.parseInt(metadata[FILE_METADATA_DUPPARC.NUMERO_DUPLICATA.getIndex()].trim()));
        	    			}
        	    			catch(Exception ex)
        	    			{
        	    				numeroDuplicata = getNumeroDuplicata(metadata[FILE_METADATA_DUPPARC.NUMERO_DUPLICATA.getIndex()].trim(), idCliente).toString();
        	    			}
        	    			Duplicata duplicata = null;
        	    			try
        	    			{
        	    				duplicata = getDuplicata(numeroDuplicata);	
        	    			}
        	    			catch(Exception ex)
        	    			{
        	    				
        	    			}
        	    			
        	    			if(duplicata == null)
        	    				continue;
        	    			
        	    			
        	    			if(!IgnUtil.dateFormat.format(duplicata.getDataCompra()).equals("31/08/2017"))
        	    				continue;
        	    			
        	    			ps.setInt(1, duplicata.getId());
        	    			ps.setInt(2, Integer.parseInt(metadata[FILE_METADATA_DUPPARC.LOJA.getIndex()].trim()));
        	    			ps.setInt(3, idCliente);
        	    			ps.setInt(4, ENTIDADE_PADRAO.USUARIO.getCodigo());
        	    			ps.setInt(5, Integer.parseInt(numeroDuplicata));
        	    			ps.setInt(6, Integer.parseInt(metadata[FILE_METADATA_DUPPARC.NUMERO_PARCELA.getIndex()].trim()));
        	    			
        	    			String dataVencimentoParcela = null;
        	    			synchronized (sqlDateFormat)
        	    			{
        	    				dataVencimentoParcela = sqlDateFormat.format(dateFormat.parse(metadata[FILE_METADATA_DUPPARC.DATA_VENCIMENTO.getIndex()].trim()));
        	    				
        	    				ps.setString(7, dataVencimentoParcela);
        	    				
        	    				String dataBaixa = metadata[FILE_METADATA_DUPPARC.DATA_BAIXA.getIndex()].trim();
        	    				if(dataBaixa == null || dataBaixa.isEmpty())
        	    					ps.setNull(11, Types.DATE);
        	    				else
        	    					ps.setString(11, sqlDateFormat.format(dateFormat.parse(dataBaixa)));
        	    				
        	    				String dataSegundoVencimento = metadata[FILE_METADATA_DUPPARC.SEGUNDO_VENCIMENTO.getIndex()].trim();
        	    				if(dataSegundoVencimento == null || dataSegundoVencimento.isEmpty())
        	    					ps.setNull(12, Types.DATE);
        	    				else
        	    					ps.setString(12, sqlDateFormat.format(dateFormat.parse(dataSegundoVencimento)));
        	    			}

        	    			ps.setDouble(8, Double.parseDouble(metadata[FILE_METADATA_DUPPARC.VALOR_PARCELA.getIndex()]));
        	    			
        	    			String situacaoParcela = metadata[FILE_METADATA_DUPPARC.SITUACAO_PARCELA.getIndex()].trim();
        	    			String valorPago = metadata[FILE_METADATA_DUPPARC.VALOR_PAGO.getIndex()].trim();
        	    			
        	    			if(valorPago.isEmpty())
        	    				valorPago = "0";

        	    			if(situacaoParcela.equals("0") && Double.parseDouble(valorPago) > 0) // se tiver em aberto mais pago, é parcial
        	    				situacaoParcela = "2";

        	    			ps.setInt(9, Integer.parseInt(situacaoParcela));
        	    			
        	    			if(valorPago == null || valorPago.isEmpty())
        	    				ps.setNull(10, Types.DOUBLE);
        	    			else
        	    				ps.setDouble(10, Double.parseDouble(valorPago));
        	    			
        	    			synchronized (connection) 
        	    			{
        	    				ps.executeUpdate();
        	    				
        	    				int idDuplicataParcela = 0;
        	    				
            	    			try (ResultSet generatedKeys = ps.getGeneratedKeys()) 
            	    			{
            	    	            if (generatedKeys.next()) 
            	    	            	idDuplicataParcela = generatedKeys.getInt(1);
            	    	            else 
            	    	                throw new Exception("Creating DuplicataParcela failed, no ID obtained.");
            	    	        }
            	    			
            	    			if(!situacaoParcela.equals("0") || Double.parseDouble(valorPago) > 0)
            	    				gerarMovimentoParcelaDuplicata(psMovimento, duplicata, idDuplicataParcela, metadata, situacaoParcela);
            	    			
            	    			if(metadata[FILE_METADATA_DUPPARC.NUMERO_PARCELA.getIndex()].trim().equals("1"))
            	    			{
            	    				psAtualizaDuplicata.setString(1, dataVencimentoParcela);
            	    				psAtualizaDuplicata.setInt(2, duplicata.getId());
            	    				psAtualizaDuplicata.executeUpdate();
            	    			}
            	    			
            	    			counter.increment();
							}
	    	    		}
	    				catch (Exception e) 
	    				{
	    					Logger.getLogger(ImportadorArquivoDuplicataCache.class).error(String.format("Erro ao processar a linha %s. Mensagem: %s", data[index], e.getMessage()), e);
	    					//throw e;
	    				}
    	    		}
				}
				catch (Exception e) 
				{
					
				} 
			}
	    }
	    
	    List<Thread> runBlockList = new ArrayList<Thread>();
	    
	    Thread firstBlock = new Thread(new RunBlock( data, 0x01, (int) (data.length * 0.01)));
	    firstBlock.setName(String.format("DuplicataParcela - Unit Block %d to %d", 1, (int) (data.length * 0.01)));
	    firstBlock.start();
	    runBlockList.add(firstBlock);
	     
	    for(double threadPart = 0.01; threadPart < 0x01; threadPart += 0.01)
	    {
	    	Thread thread = new Thread(new RunBlock( data, (int) (data.length * threadPart), (int) (data.length * (threadPart + 0.01))));
	    	thread.setName(String.format("DuplicataParcela - Unit Block %d to %d", (int) (data.length * threadPart),  (int) (data.length * (threadPart + 0.01))));
	    	thread.start();
	    	runBlockList.add(thread);
	    }
	    
	    Thread counterThread  = new Thread(new Runnable() 
	    {
			@Override
			public void run() 
			{
			    while(!Thread.currentThread().isInterrupted())
			    {
			    	try 
			    	{
						synchronized (counter) 
						{
							Logger.getLogger(ImportadorArquivoDuplicataCache.this.getClass()).info(String.format("%s duplicatas parcela processadas", counter));
						}
						Thread.sleep(1500);
			    	}
			    	catch (InterruptedException e) 
			    	{
			    		Thread.currentThread().interrupt();
					}
			    }
			}
		});
	    counterThread.start();
	    
	    for(final Thread unitBlock : runBlockList)
	    	unitBlock.join();
	    
	    counterThread.interrupt();
	    Logger.getLogger(ImportadorArquivoDuplicataCache.this.getClass()).info("Bloco de importação de DuplicataParcela finalizado!");
	}
}