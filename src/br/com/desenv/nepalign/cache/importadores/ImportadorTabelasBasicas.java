package br.com.desenv.nepalign.cache.importadores;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet; 
import java.sql.Statement;
import java.util.Date;

import br.com.desenv.nepalign.model.Cor;
import br.com.desenv.nepalign.model.Grupo;
import br.com.desenv.nepalign.model.GrupoProduto;
import br.com.desenv.nepalign.model.Marca;
import br.com.desenv.nepalign.model.OrdemProduto;
import br.com.desenv.nepalign.model.Tamanho;
import br.com.desenv.nepalign.service.CorService;
import br.com.desenv.nepalign.service.GrupoProdutoService;
import br.com.desenv.nepalign.service.GrupoService;
import br.com.desenv.nepalign.service.MarcaService;
import br.com.desenv.nepalign.service.OrdemProdutoService;
import br.com.desenv.nepalign.service.TamanhoService;
import br.com.desenv.nepalign.util.DsvConstante;



public class ImportadorTabelasBasicas 
{
	public static void main(String[] args) throws Exception{
		ImportadorTabelasBasicas i = new ImportadorTabelasBasicas();
		
		i.init(null);
		
	}

	public void init(Connection conCache) throws Exception
	{
 
		try 
		{
			if (conCache==null)
			{
				try 
				{
					Class.forName("com.intersys.jdbc.CacheDriver");
					conCache = DriverManager.getConnection(
							DsvConstante.getInstanceParametrosSistema().getStrConexaoCache(), DsvConstante.getInstanceParametrosSistema().getUsuarioCache(), DsvConstante.getInstanceParametrosSistema().getSenhaCache());
				} 
				catch (Exception chEx) 
				{
					throw new Exception("Falha na conexão com o caché."
							+ chEx.getMessage());
				}
			}
			//Faz as chamadas
			System.out.println("Cores: "  + importarCor(conCache));
			System.out.println("Grupos Produtos: "  + importarGrupoProduto(conCache));
			System.out.println("Marcas: "  + importarMarca(conCache));
			System.out.println("Tamanhos: "  + importarTamanho(conCache));
			System.out.println("Ordens Produto: "  + importarOrdemProduto(conCache));

		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			try
			{if (conCache!=null)
				conCache.close();
			}catch(Exception ex)
			{ex.printStackTrace();
				throw ex;}		
		}	
	}
	
	private int importarCor(Connection conCache) throws Exception
	{
		Statement stm = null;
		ResultSet rs = null;
		CorService corService = new CorService();
		Cor cor;
		Cor existingCor;
		int cont=0;
		Cor criterioCor;
		
		try
		{
			stm = conCache.createStatement();
			rs = stm.executeQuery("SELECT * FROM CECor");
			
			while(rs.next())
			{
				cor = new Cor();
				cor.setDescricaoCorEmpresa(rs.getString("CorDescricao"));
				cor.setDescricaoCorFabrica(new String(rs.getString("CorDescricao").getBytes(),"ISO-8859-1"));
				cor.setId(rs.getInt("CorCodigo"));
				cor.setUltimaAlteracao(new Date());
				cor.setCodigo(rs.getInt("CorCodigo"));
				
				criterioCor = new Cor();
				criterioCor.setCodigo(cor.getCodigo());			
				
				
				if (corService.listarPorObjetoFiltro(criterioCor).isEmpty())
				{
					corService.salvar(cor);
					cont++;
				}
				else
				{
					 existingCor = corService.listarPorObjetoFiltro(criterioCor).get(0);
					 if (existingCor.getDescricaoCorEmpresa().equals(cor.getDescricaoCorEmpresa())==false)
					 {
						 corService.atualizar(cor);
					 }
				}
				
			
			}
			return cont;
		}
		catch(Exception e)
		{
			throw e;
		}
		finally
		{
			try
			{if (rs!=null)
				rs.close();
			if (stm!=null)
				stm.close();
			}catch(Exception ex)
			{ex.printStackTrace();
				throw ex;}	
		}				
		
	}
	private int importarTamanho(Connection conCache) throws Exception
	{
	
		Statement stm = null;
		ResultSet rs = null;
		TamanhoService tamanhoService = new TamanhoService();
		Tamanho tamanho;
		Tamanho existingTamanho;
		int cont=0;
		
		try
		{
			stm = conCache.createStatement();
			rs = stm.executeQuery("SELECT * FROM CETamanho");
			
			while(rs.next())
			{
				tamanho = new Tamanho();
				tamanho.setId(rs.getInt("CodTamanho"));
				tamanho.setDescricao(rs.getString("DesTamanho"));
				tamanho.setUltimaAlteracao(new Date());
				tamanho.setCodigo(rs.getInt("CodTamanho"));
				
				Tamanho criterioTamanho = new Tamanho();
								
				criterioTamanho.setCodigo(tamanho.getCodigo());
				
				if (tamanhoService.listarPorObjetoFiltro(criterioTamanho).isEmpty())
				{
					tamanhoService.salvar(tamanho);
					cont++;
				}
				else
				{
					existingTamanho = tamanhoService.listarPorObjetoFiltro(criterioTamanho).get(0);
					if (existingTamanho.getDescricao().equals(tamanho.getDescricao()) == false)  
					{
						tamanhoService.atualizar(tamanho);
					}
				}
			}
			return cont;
		}
		catch(Exception e)
		{
			throw e;
		}
		finally
		{
			try
			{if (rs!=null)
				rs.close();
			if (stm!=null)
				stm.close();
			}catch(Exception ex)
			{ex.printStackTrace();
				throw ex;}		
		}		
	}
	
	private int importarMarca(Connection conCache) throws Exception
	{
	
		Statement stm = null;
		ResultSet rs = null;
		MarcaService marcaService = new MarcaService();
		Marca marca = new Marca();
		Marca criterioMarca = new Marca();
		Marca existingMarca;
		int cont=0;
		
		try
		{
			stm = conCache.createStatement();
			rs = stm.executeQuery("SELECT * FROM CEMarca");
			
			while(rs.next())
			{
				marca = new Marca();
				marca.setId(rs.getInt("MarCodigo"));
				marca.setCodigo(rs.getInt("MarCodigo"));
				marca.setDescricao(rs.getString("MarDescricao"));
				marca.setUltimaAlteracao(new Date());
				
				criterioMarca.setCodigo(marca.getCodigo());
				
				if (marcaService.listarPorObjetoFiltro(criterioMarca).isEmpty())
				{
					marcaService.salvar(marca);
					cont++;
				}
				else
				{
					existingMarca = marcaService.listarPorObjetoFiltro(criterioMarca).get(0);
					if(existingMarca.getDescricao().equals(marca.getDescricao()) == false)
					{
						marcaService.atualizar(marca);
					}
				}
				
			}
			return cont;
		}
		catch(Exception e)
		{
			throw e;
		}
		finally
		{
			try
			{if (rs!=null)
				rs.close();
			if (stm!=null)
				stm.close();
			}catch(Exception ex)
			{ex.printStackTrace();
				throw ex;}		
		}		
	}	

	private int importarGrupoProduto(Connection conCache) throws Exception
	{
	
		Statement stm = null;
		ResultSet rs = null;
		GrupoProdutoService grupoProdutoService = new GrupoProdutoService();
		GrupoProduto grupoProduto;
		GrupoProduto existingGrupo;
		int cont=0;
		
		try
		{
			stm = conCache.createStatement();
			rs = stm.executeQuery("SELECT * FROM CEGrupo");
			
			
			
			while(rs.next())
			{
				grupoProduto = new GrupoProduto();
				grupoProduto.setCodigo(rs.getInt("GruCodigo"));
				grupoProduto.setId(rs.getInt("GruCodigo"));
				grupoProduto.setDescricao(rs.getString("GruDescricao"));
				grupoProduto.setUltimaAlteracao(new Date());;
				
				GrupoProduto criterioGrupo = new GrupoProduto();
				
				
				criterioGrupo.setCodigo(grupoProduto.getCodigo());	
				
				
				if (grupoProdutoService.listarPorObjetoFiltro(criterioGrupo).isEmpty())
						//existingGrupo==null || grupo.equals(existingGrupo)==false)
				{
					grupoProdutoService.salvar(grupoProduto);
					cont++;
				}
				else
				{
					existingGrupo = grupoProdutoService.listarPorObjetoFiltro(criterioGrupo).get(0);
					if (existingGrupo.getDescricao().equals(grupoProduto.getDescricao())==false)
					{
						grupoProdutoService.atualizar(grupoProduto);
					}
				}
					
			}
			return cont;
		}
		catch(Exception e)
		{
			throw e;
		}
		finally
		{
			try
			{if (rs!=null)
				rs.close();
			if (stm!=null)
				stm.close();
			}catch(Exception ex)
			{ex.printStackTrace();
				throw ex;}		
		}		
	}
	
	private int importarOrdemProduto(Connection conCache) throws Exception
	{
	
		Statement stm = null;
		ResultSet rs = null;
		OrdemProdutoService ordemService = new OrdemProdutoService();
		OrdemProduto ordemProduto;
		OrdemProduto existingOrdem;
		int cont=0;
		
		try
		{
			stm = conCache.createStatement();
			rs = stm.executeQuery("SELECT * FROM CETipoMaterial");
			
			while(rs.next())
			{
				ordemProduto = new OrdemProduto();
				ordemProduto.setId(rs.getInt("TipCodigo"));
				ordemProduto.setDescricao(rs.getString("TipDescricao"));
				ordemProduto.setCodigo(rs.getInt("TipCodigo"));
				
				OrdemProduto criterioOrdem = new OrdemProduto();
								
				criterioOrdem.setCodigo(ordemProduto.getCodigo());
				
				if (ordemService.listarPorObjetoFiltro(criterioOrdem).isEmpty())
				{
					ordemService.salvar(ordemProduto);
					cont++;
				}
				else
				{
					existingOrdem = ordemService.listarPorObjetoFiltro(criterioOrdem).get(0);
					if (existingOrdem.getDescricao().equals(ordemProduto.getDescricao()) == false)  
					{
						ordemService.atualizar(ordemProduto);
					}
				}
			}
			return cont;
		}
		catch(Exception e)
		{
			throw e;
		}
		finally
		{
			try
			{if (rs!=null)
				rs.close();
			if (stm!=null)
				stm.close();
			}catch(Exception ex)
			{ex.printStackTrace();
				throw ex;}		
		}		
	}
	
}
