package br.com.desenv.nepalign.cache.importadores;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.apache.log4j.Logger;

import br.com.desenv.nepalign.cache.constantes.Util;
import br.com.desenv.nepalign.model.Cliente;
import br.com.desenv.nepalign.model.EstoqueProduto;
import br.com.desenv.nepalign.model.PedidoVenda;
import br.com.desenv.nepalign.model.PlanoPagamento;
import br.com.desenv.nepalign.model.SituacaoPedidoVenda;
import br.com.desenv.nepalign.model.StatusItemPedido;
import br.com.desenv.nepalign.model.TipoMovimentacaoEstoque;
import br.com.desenv.nepalign.model.Vendedor;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;


public class ImportadorArquivoPlanoCache extends ImportadorCache
{
	public static void main(String[] args)
	{
		new ImportadorArquivoPlanoCache().run();
	}
	
	private static int IDEMPRESAFISICA = 3;
	
	enum FILE_METADATA_PLANO
	{
		CODIGO((short) 0),
		DESCRICAO((short) 1),
		PERCENTUAL((short) 2),
		TIPO((short) 3);
		
		private final short index;
		
		FILE_METADATA_PLANO(final short index)
		{
			this.index = index;
		}
		
		short getIndex()
		{
			return index;
		}
	}
	
	
	public void run()
	{
		EntityManager manager = ConexaoUtil.getFactory("importacao", "importacao!123").createEntityManager();
		try 
		{
			manager.getTransaction().begin();
			clean(manager);
			manager.getTransaction().commit();
			
			importarPlanos(manager, readFile("D:\\Desenv\\ARQ_CONV\\PLANO.TXT"));
		}
		catch(InterruptedException ie)
		{
			ie.printStackTrace();
			manager.getTransaction().rollback();
		}
		catch (IOException e) 
		{
			manager.getTransaction().rollback();
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			manager.close();
		}
	}
	
	private void clean(final EntityManager manager)
	{
		manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=0;").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE planopagamento;").executeUpdate();
		manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=1;").executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	public void importarPlanos(final EntityManager manager, final String data[]) throws InterruptedException
	{
		Logger.getLogger(ImportadorArquivoPlanoCache.this.getClass()).info("Iniciando bloco de importação de PlanoPagamento");
		
		try
		{
			Connection connection = ConexaoUtil.getConexaoPadrao("importacao", "importacao!123");
			PreparedStatement ps = connection.prepareStatement("INSERT INTO planoPagamento (descricao, tipoPercentual, percentual, quantParcelas, codigoPlano, idEmpresaFisica)" +
					"VALUES(?, ?, ?, ?, ?, ?); ");
			
			for(final String _metadata : data)
			{
				final String[] metadata = _metadata.split(Util.CACHE_FILE_SEPARATOR, -0x01);
				
				ps.setString(1, metadata[FILE_METADATA_PLANO.DESCRICAO.getIndex()]);
				ps.setString(2, metadata[FILE_METADATA_PLANO.TIPO.getIndex()]);
				ps.setInt(3, Integer.parseInt(metadata[FILE_METADATA_PLANO.PERCENTUAL.getIndex()]));
				ps.setInt(4, 10);
				ps.setString(5, metadata[FILE_METADATA_PLANO.CODIGO.getIndex()]);
				ps.setInt(6, IDEMPRESAFISICA);

				ps.executeUpdate();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

		
	    Logger.getLogger(ImportadorArquivoPlanoCache.this.getClass()).info("Bloco de importação de Pedido Venda finalizado!");
	}
}