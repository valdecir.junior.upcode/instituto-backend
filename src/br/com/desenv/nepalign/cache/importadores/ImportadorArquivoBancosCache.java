package br.com.desenv.nepalign.cache.importadores;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.google.common.primitives.Ints;

import br.com.desenv.nepalign.model.ContaContabil;
import br.com.desenv.nepalign.model.TipoMovimentoBancos;
import br.com.desenv.nepalign.service.ContaContabilService;
import br.com.desenv.nepalign.service.TipoMovimentoBancosService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ImportadorArquivoBancosCache 
{
	private static final int[] bancosPaulo = new int[] { 24, 127, 286 };
	
	public static void main(String[] args) throws Exception
	{
		final ImportadorArquivoBancosCache integrar = new ImportadorArquivoBancosCache();

		System.out.println("Essa importação irá importar somente os bancos do Paulo. Deseja continuar?");
		Thread.sleep(10000);
		System.in.read();
		
		//integrar.integrarBanco(ConexaoUtil.getConexaoPadrao(), integrar.readFile("F:/ARQUIVOS/bancos/BANCO"));
		integrar.integrarSaldoBancos(ConexaoUtil.getConexaoPadrao(), integrar.readFile("F:/ARQUIVOS/bancos/SALDO"));
		//integrar.integrarHistorico(ConexaoUtil.getConexaoPadrao(), integrar.readFile("F:/ARQUIVOS/bancos/HISTORICO"));
		//integrar.integrarConta(ConexaoUtil.getConexaoPadrao(), integrar.readFile("F:/ARQUIVOS/bancos/CONTA"));
		integrar.integrarRegistroBancos(ConexaoUtil.getConexaoPadrao(), integrar.readFile("F:/ARQUIVOS/bancos/MOV"));
	}

	public String[] readFile(String fileName) throws IOException
	{
		List<String> result = new ArrayList<String>();
		
		BufferedReader br = null;

		try 
		{
			String sCurrentLine;

			br = new BufferedReader(new FileReader(fileName));

			while ((sCurrentLine = br.readLine()) != null) 
			{
				if(sCurrentLine.equals("FIM")) break;
				
				result.add(sCurrentLine);	
			}
		} 
		catch (IOException e) 
		{
			throw e;
		}
		finally 
		{
			try 
			{
				if (br != null) br.close();
			}
			catch (IOException ex) 
			{
				ex.printStackTrace();
			}
		}
		return result.toArray(new String[result.size()]);
	}

	public void integrarBanco(Connection connection, String[] data) throws SQLException
	{
		connection.createStatement().execute("SET FOREIGN_KEY_CHECKS=0;");
		connection.createStatement().execute("TRUNCATE TABLE bancos;");
		PreparedStatement statement = connection.prepareStatement("INSERT INTO bancos(idBancos, nomeBanco, idEmpresaBancos, mesAberto, anoAberto) VALUES (?, ?, 2, MONTH(now()), YEAR(now())); ");
		
		for(short index = 0x01; index < data.length; index++)
		{ 
			statement.setInt(0x01, Integer.parseInt(data[index].split("\\|")[0x00]));
			statement.setString(0x02, data[index].split("\\|")[0x01]);
			//TODO : check bancosRelacionados
			
			statement.execute();
		}

		statement.close();
	}

	public void integrarConta(Connection connection, String[] data) throws SQLException
	{
		connection.createStatement().execute("SET FOREIGN_KEY_CHECKS=0;");
		connection.createStatement().execute("TRUNCATE TABLE contaContabil;");
		
		PreparedStatement statement = connection.prepareStatement("INSERT INTO contaContabil(idEmpresa, descricao, codigoContabil, codigoContabilNivel1, codigoContabilNivel2, codigoContabilNivel3, creditoDebito) VALUES (?, ?, ?, ?, ?, ?, ?); ");
		
		for(short index = 0x01; index < data.length; index++)
		{ 
			statement.setInt(0x01, Integer.parseInt(data[index].split("\\|")[0x00]));
			statement.setString(0x02, data[index].split("\\|")[0x02]);
			statement.setString(0x03, data[index].split("\\|")[0x01]);
			statement.setString(0x04, data[index].split("\\|")[0x01].split("\\.")[0x00]);
			statement.setString(0x05, data[index].split("\\|")[0x01].split("\\.")[0x01]);
			statement.setString(0x06, data[index].split("\\|")[0x01].split("\\.")[0x02]);
			statement.setInt(0x07, Integer.parseInt(data[index].split("\\|")[0x03]));
			
			statement.execute();
		}

		statement.close();
	}
	
	public void integrarSaldoBancos(Connection connection, String[] data) throws SQLException
	{
		connection.createStatement().execute("SET FOREIGN_KEY_CHECKS=0;");
		//connection.createStatement().execute("TRUNCATE TABLE saldoBancos;");
		
		connection.createStatement().execute("DELETE FROM saldoBancos WHERE idBancos in (24, 127, 286);");
		
		final PreparedStatement statement = connection.prepareStatement("INSERT INTO saldoBancos(idBancos, mes, ano, saldo) VALUES (?, ?, ?, ?); ");
		final PreparedStatement statementBanco = connection.prepareStatement("UPDATE bancos SET mesAberto = ?, anoAberto = ? WHERE idBancos = ?");
		
		for(short index = 0x01; index < data.length; index++)
		{ 
			final short mesAberto = Short.parseShort(data[index].split("\\|")[0x0F]);
			final short anoAberto = (short) (Short.parseShort(data[index].split("\\|")[0x10]) + 2000);
			
			short ano = anoAberto;
			short mes = 0x00;
			
			final int idBancos = Integer.parseInt(data[index].split("\\|")[0x01]);
			
			if(!Ints.contains(bancosPaulo, idBancos))
				continue;
			
			for(short saldoIndex = mesAberto; saldoIndex > (-0x0C + mesAberto); saldoIndex--)
			{
				if(saldoIndex < 0x01)
				{
					mes = (short) ((short) saldoIndex + (short) 0x0C);
					
					if(saldoIndex == 0x00)
						ano--;
				}
				else
					mes = saldoIndex;
				
				statement.setInt(0x01, idBancos);
				statement.setInt(0x02, mes);
				statement.setInt(0x03, ano); 
				statement.setDouble(0x04, Double.parseDouble(data[index].split("\\|")[0x01 + mes]));
				
				statement.execute();
			}
			
			statementBanco.setInt(0x01, mesAberto);
			statementBanco.setInt(0x02, anoAberto);
			statementBanco.setInt(0x03, Integer.parseInt(data[index].split("\\|")[0x01]));
			statementBanco.execute();
		}

		statement.close();
		statementBanco.close();
	}
	
	public void integrarHistorico(Connection connection, String[] data) throws SQLException
	{
		connection.createStatement().execute("SET FOREIGN_KEY_CHECKS=0;");
		connection.createStatement().execute("TRUNCATE TABLE tipoMovimentoBancos;");
		PreparedStatement statement = connection.prepareStatement("INSERT INTO tipoMovimentoBancos(codigo, descricao, creditoDebito) VALUES (?, ?, ?); ");
		
		for(short index = 0x01; index < data.length; index++)
		{ 
			statement.setString(0x01, data[index].split("\\|")[0x00]);
			statement.setString(0x02, data[index].split("\\|")[0x01]);
			statement.setInt(0x03, Integer.parseInt(data[index].split("\\|")[0x02]));

			statement.execute();
		}

		statement.close();
	}
	
	public void integrarContaContabil(Connection connection, String[] data) throws SQLException
	{
		connection.createStatement().execute("SET FOREIGN_KEY_CHECKS=0;");
		connection.createStatement().execute("TRUNCATE TABLE contaContabil;");
		PreparedStatement statement = connection.prepareStatement("INSERT INTO contacontabil(codigo, descricao, creditoDebito) VALUES (?, ?, ?); ");
		
		for(short index = 0x01; index < data.length; index++)
		{ 
			statement.setString(0x01, data[index].split("\\|")[0x00]);
			statement.setString(0x02, data[index].split("\\|")[0x01]);
			statement.setInt(0x03, Integer.parseInt(data[index].split("\\|")[0x02]));

			statement.execute();
		}

		statement.close();
	}
	
	public void integrarRegistroBancos(Connection connection, String[] data) throws Exception
	{
		connection.createStatement().execute("SET FOREIGN_KEY_CHECKS=0;");
		//connection.createStatement().execute("TRUNCATE TABLE lancamentoBanco;");
		connection.createStatement().execute("DELETE FROM lancamentoBanco WHERE idBancos in (24, 127, 286);");
		
		final List<TipoMovimentoBancos> listaTipoMovimentoBancos = new TipoMovimentoBancosService().listar();
		final List<ContaContabil> listaContaContabil = new ContaContabilService().listar();
		
	    class RunBlock implements Runnable 
	    {
	        int startIndex, endIndex;
	        Connection connection;
	        final String[] data;
	        final List<TipoMovimentoBancos> listaTipoMovimentoBancos;
	        final List<ContaContabil> listaContaContabil;

	        final TipoMovimentoBancos getTipoMovimentoBancos(String codigo)
	        {
	        	for(TipoMovimentoBancos tipoMovimentoBancos : listaTipoMovimentoBancos)
	        	{
	        		if(tipoMovimentoBancos.getCodigo().equals(codigo))
	        			return tipoMovimentoBancos;
	        	}
	        	
	        	return null;
	        }
	        
	        final ContaContabil getContaContabil(int idEmpresa, String codigoContabil)
	        {
        		for(ContaContabil conta : listaContaContabil)
        		{
        			if(conta.getIdEmpresa().intValue() == idEmpresa && conta.getCodigoContabil().equals(codigoContabil))
        				return conta;
        		}
        		
        		return null;
	        }
	        
	        RunBlock(Connection connection, String[] data, int startIndex, int endIndex, List<TipoMovimentoBancos> listaTipoMovimentoBancos, List<ContaContabil> listaContaContabil) 
	        { 
	        	this.connection = connection;
	        	this.startIndex = startIndex;
	        	this.endIndex = endIndex;
	        	this.data = data;
	        	
	        	this.listaContaContabil = listaContaContabil;
	        	this.listaTipoMovimentoBancos = listaTipoMovimentoBancos;
	        }
	        
	        public void run() 
	        {
	        	try
	        	{ 
	        		connection = ConexaoUtil.getConexaoPadrao();
	        		connection.createStatement().execute("SET FOREIGN_KEY_CHECKS=0;");	
	        		
		        	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		    		
		    		PreparedStatement statement = connection.prepareStatement("INSERT INTO lancamentoBanco(idLancamentoBanco, idEmpresaBancos, idBancos, idTipoMovimentoBancos, numeroDocumento, datalancamento, valor, observacao, idContaContabil, dataCompensacao, dataRecebimento) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); ");
		    		
		    		for(int index = startIndex; index < endIndex; index++)
		    		{ 
		    			if(data[index].split("\\|")[0x00].equals("#")) continue;
		    			
		    			String codigoEmpresa = data[index].split("\\|")[0x00];
		    			String codigoBanco = data[index].split("\\|")[0x01];
		    			String numLanc = data[index].split("\\|")[0x02];
		    			String dataEmissao = data[index].split("\\|")[0x03];
		    			String codigoHistorico = data[index].split("\\|")[0x04]; 
		    			String observacao = data[index].split("\\|")[0x05];
		    			String valor = data[index].split("\\|")[0x07]; 
		    			
		    			if(!Ints.contains(bancosPaulo, Integer.parseInt(codigoBanco)))
		    				continue;

		    			statement.setInt(0x01, Integer.parseInt(codigoBanco + (String.format("%06d", Integer.parseInt(numLanc)))));
		    			statement.setInt(0x02, Integer.parseInt(codigoEmpresa));
		    			statement.setInt(0x03, Integer.parseInt(codigoBanco)); 
		    			statement.setInt(0x04, getTipoMovimentoBancos(codigoHistorico).getId());
		    			statement.setString(0x05, numLanc);
		    			statement.setDate(0x06, dataEmissao.equals("") ? new java.sql.Date(0x00) : new java.sql.Date(formatter.parse(dataEmissao).getTime()));
		    			statement.setDouble(0x07, Double.parseDouble(valor));
		    			statement.setString(0x08, observacao);

		    			if(data[index].split("\\|").length > 0x08)
		    				statement.setInt(0x09, getContaContabil(0x02, data[index].split("\\|")[0x08]).getId());
		    			else
			    			statement.setNull(0x09, Types.INTEGER);
		    			
		    			if(data[index].split("\\|").length > 0x09)
		    			{
		    				String dataCompensacao = data[index].split("\\|")[0x09];
		    				statement.setDate(0x0A, dataCompensacao.equals("") ? new java.sql.Date(0x00) : new java.sql.Date(formatter.parse(dataCompensacao).getTime()));
		    			}
		    			else
		    				statement.setDate(0x0A, new java.sql.Date(0x00));
		    			
		    			if(data[index].split("\\|").length > 0x0A)
		    			{
		    				String dataDocumento = data[index].split("\\|")[0x0A];
		    				statement.setDate(0x0B, dataDocumento.equals("") ? new java.sql.Date(0x00) : new java.sql.Date(formatter.parse(dataDocumento).getTime()));
		    			}
		    			else
		    				statement.setDate(0x0B, new java.sql.Date(0x00));
		    			
		    			statement.execute(); 
		    		}
		    		statement.close();
	        	} 
	        	catch(Exception ex)
	        	{
	        		System.out.println("Exception at block " + startIndex + " " + endIndex + " :: " + ex.getMessage());
	        		ex.printStackTrace();
	        	}
	        } 
	    }   
	    
	    new Thread(new RunBlock(connection, data, 0x01, (int) (data.length * 0.01), listaTipoMovimentoBancos, listaContaContabil)).start();
	     
	    for(double threadPart = 0.01; threadPart < 0x01; threadPart += 0.01)
	    	new Thread(new RunBlock(connection, data, (int) (data.length * threadPart), (int) (data.length * (threadPart + 0.01)), listaTipoMovimentoBancos, listaContaContabil)).start();
	}
}