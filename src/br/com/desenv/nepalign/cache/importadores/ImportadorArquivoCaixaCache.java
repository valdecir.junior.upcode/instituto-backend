package br.com.desenv.nepalign.cache.importadores;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.desenv.nepalign.model.ContaContabil;
import br.com.desenv.nepalign.model.TipoMovimentoCaixa;
import br.com.desenv.nepalign.service.ContaContabilService;
import br.com.desenv.nepalign.service.TipoMovimentoCaixaService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ImportadorArquivoCaixaCache 
{
	public static void main(String[] args) throws Exception
	{
		final ImportadorArquivoCaixaCache integrar = new ImportadorArquivoCaixaCache();

		//integrar.integrarCaixa(ConexaoUtil.getConexaoPadrao(), integrar.readFile("F:/ARQUIVOS/caixas/EMP"));
		integrar.integrarSaldoCaixas(ConexaoUtil.getConexaoPadrao(), integrar.readFile("F:/ARQUIVOS/caixas/SALDO"));
		//integrar.integrarHistorico(ConexaoUtil.getConexaoPadrao(), integrar.readFile("F:/ARQUIVOS/caixas/HISTORICO"));
		//integrar.integrarConta(ConexaoUtil.getConexaoPadrao(), integrar.readFile("F:/ARQUIVOS/caixas/CONTA"));
		integrar.integrarRegistroCaixas(ConexaoUtil.getConexaoPadrao(), integrar.readFile("F:/ARQUIVOS/caixas/MOV"));
	} 

	public String[] readFile(String fileName) throws IOException
	{
		List<String> result = new ArrayList<String>();
		
		BufferedReader br = null;

		try 
		{
			String sCurrentLine;

			br = new BufferedReader(new FileReader(fileName));

			while ((sCurrentLine = br.readLine()) != null) 
			{
				if(sCurrentLine.equals("FIM")) break;
				
				result.add(sCurrentLine);	
			}
		} 
		catch (IOException e) 
		{
			throw e;
		}
		finally 
		{
			try 
			{
				if (br != null) br.close();
			}
			catch (IOException ex) 
			{
				ex.printStackTrace();
			}
		}
		return result.toArray(new String[result.size()]);
	}

	public void integrarCaixa(Connection connection, String[] data) throws SQLException
	{
		connection.createStatement().execute("SET FOREIGN_KEY_CHECKS=0;");
		connection.createStatement().execute("TRUNCATE TABLE caixa;");
		PreparedStatement statement = connection.prepareStatement("INSERT INTO caixa(idCaixa, descricao, mesAberto, anoAberto) VALUES (?, ?, MONTH(now()), YEAR(now())); ");
		
		for(short index = 0x01; index < data.length; index++)
		{ 
			statement.setInt(0x01, Integer.parseInt(data[index].split("\\|")[0x00]));
			statement.setString(0x02, data[index].split("\\|")[0x01]);

			statement.execute();
		}

		statement.close();
	}

	public void integrarConta(Connection connection, String[] data) throws SQLException
	{
		connection.createStatement().execute("SET FOREIGN_KEY_CHECKS=0;");
		connection.createStatement().execute("TRUNCATE TABLE contaContabil;");
		
		PreparedStatement statement = connection.prepareStatement("INSERT INTO contaContabil(idEmpresa, descricao, codigoContabil, codigoContabilNivel1, codigoContabilNivel2, codigoContabilNivel3, creditoDebito) VALUES (?, ?, ?, ?, ?, ?, ?); ");
		
		for(short index = 0x01; index < data.length; index++)
		{ 
			statement.setInt(0x01, Integer.parseInt(data[index].split("\\|")[0x00]));
			statement.setString(0x02, data[index].split("\\|")[0x02]);
			statement.setString(0x03, data[index].split("\\|")[0x01]);
			statement.setString(0x04, data[index].split("\\|")[0x01].split("\\.")[0x00]);
			statement.setString(0x05, data[index].split("\\|")[0x01].split("\\.")[0x01]);
			statement.setString(0x06, data[index].split("\\|")[0x01].split("\\.")[0x02]);
			statement.setInt(0x07, Integer.parseInt(data[index].split("\\|")[0x03]));
			
			statement.execute();
		}

		statement.close();
	}
	
	public void integrarSaldoCaixas(Connection connection, String[] data) throws SQLException
	{
		connection.createStatement().execute("SET FOREIGN_KEY_CHECKS=0;");
		connection.createStatement().execute("TRUNCATE TABLE saldoCaixa;");
		 
		final PreparedStatement statement = connection.prepareStatement("INSERT INTO saldoCaixa(idCaixa, idEmpresaCaixa, mes, ano, saldo) VALUES (?, 2, ?, ?, ?); ");
		final PreparedStatement statementBanco = connection.prepareStatement("UPDATE caixa SET mesAberto = ?, anoAberto = ? WHERE idCaixa = ?");
		
		for(short index = 0x01; index < data.length; index++)
		{ 
			final short mesAberto = Short.parseShort(data[index].split("\\|")[0x0E]);
			final short anoAberto = (short) (Short.parseShort(data[index].split("\\|")[0x0F]) + 2000);
			
			short ano = anoAberto;
			short mes = 0x00;
			
			for(short saldoIndex = mesAberto; saldoIndex > (-0x0C + mesAberto); saldoIndex--)
			{
				if(saldoIndex < 0x01)
				{
					mes = (short) ((short) saldoIndex + (short) 0x0C);
					
					if(saldoIndex == 0x00)
						ano--;
				}
				else
					mes = saldoIndex;
				
				statement.setInt(0x01, Integer.parseInt(data[index].split("\\|")[0x00]));
				statement.setInt(0x02, mes);
				statement.setInt(0x03, ano); 
				
				try
				{
					statement.setDouble(0x04, Double.parseDouble(data[index].split("\\|")[mes]));	
				}
				catch(Exception ex)
				{
					statement.setDouble(0x04, 0.0);
				}
				
				statement.execute();
			}
			
			statementBanco.setInt(0x01, mesAberto);
			statementBanco.setInt(0x02, anoAberto);
			statementBanco.setInt(0x03, Integer.parseInt(data[index].split("\\|")[0x00]));
			statementBanco.execute();
		}

		statement.close();
		statementBanco.close();
	}
	
	public void integrarHistorico(Connection connection, String[] data) throws SQLException
	{
		connection.createStatement().execute("SET FOREIGN_KEY_CHECKS=0;");
		connection.createStatement().execute("TRUNCATE TABLE tipoMovimentoCaixa;");
		PreparedStatement statement = connection.prepareStatement("INSERT INTO tipoMovimentoCaixa(codigo, descricao, creditoDebito) VALUES (?, ?, ?); ");
		
		for(short index = 0x01; index < data.length; index++)
		{ 
			statement.setString(0x01, data[index].split("\\|")[0x00]);
			statement.setString(0x02, data[index].split("\\|")[0x01]);
			statement.setInt(0x03, Integer.parseInt(data[index].split("\\|")[0x02]));

			statement.execute();
		}

		statement.close();
	}
	
	public void integrarContaContabil(Connection connection, String[] data) throws SQLException
	{
		connection.createStatement().execute("SET FOREIGN_KEY_CHECKS=0;");
		connection.createStatement().execute("TRUNCATE TABLE contaContabil;");
		PreparedStatement statement = connection.prepareStatement("INSERT INTO contacontabil(codigo, descricao, creditoDebito) VALUES (?, ?, ?); ");
		
		for(short index = 0x01; index < data.length; index++)
		{ 
			statement.setString(0x01, data[index].split("\\|")[0x00]);
			statement.setString(0x02, data[index].split("\\|")[0x01]);
			statement.setInt(0x03, Integer.parseInt(data[index].split("\\|")[0x02]));

			statement.execute();
		}

		statement.close();
	}
	
	public void integrarRegistroCaixas(Connection connection, String[] data) throws Exception
	{
		connection.createStatement().execute("SET FOREIGN_KEY_CHECKS=0;");
		connection.createStatement().execute("TRUNCATE TABLE lancamentoCaixa;");
		
		final List<TipoMovimentoCaixa> listaTipoMovimentoCaixa = new TipoMovimentoCaixaService().listar();
		final List<ContaContabil> listaContaContabil = new ContaContabilService().listar();
		
	    class RunBlock implements Runnable 
	    {
	        int startIndex, endIndex;
	        Connection connection;
	        final String[] data;
	        final List<TipoMovimentoCaixa> listaTipoMovimentoCaixa;
	        final List<ContaContabil> listaContaContabil;

	        final TipoMovimentoCaixa getTipoMovimentoCaixa(String codigo)
	        {
	        	for(TipoMovimentoCaixa tipoMovimentoCaixa : listaTipoMovimentoCaixa)
	        	{
	        		if(tipoMovimentoCaixa.getCodigo().equals(codigo))
	        			return tipoMovimentoCaixa;
	        	}
	        	
	        	return null;
	        }
	        
	        final ContaContabil getContaContabil(int idEmpresa, String codigoContabil)
	        {
        		for(ContaContabil conta : listaContaContabil)
        		{
        			if(conta.getIdEmpresa().intValue() == idEmpresa && conta.getCodigoContabil().equals(codigoContabil))
        				return conta;
        		}
        		
        		return null;
	        }
	        
	        RunBlock(Connection connection, String[] data, int startIndex, int endIndex, List<TipoMovimentoCaixa> listaTipoMovimentoCaixa, List<ContaContabil> listaContaContabil) 
	        { 
	        	this.connection = connection;
	        	this.startIndex = startIndex;
	        	this.endIndex = endIndex;
	        	this.data = data;
	        	
	        	this.listaContaContabil = listaContaContabil;
	        	this.listaTipoMovimentoCaixa = listaTipoMovimentoCaixa;
	        }
	        
	        public void run() 
	        {
	        	try
	        	{ 
	        		connection = ConexaoUtil.getConexaoPadrao();
	        		connection.createStatement().execute("SET FOREIGN_KEY_CHECKS=0;");	
	        		
		        	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		    		
		    		PreparedStatement statement = connection.prepareStatement("INSERT INTO lancamentoCaixa(idLancamentoCaixa, idCaixa, idEmpresaCaixa, idTipoMovimentoCaixa, datalancamento, valor, observacao, idContaContabil) VALUES (?, ?, 2, ?, ?, ?, ?, ?); ");
		    		
		    		for(int index = startIndex; index < endIndex; index++)
		    		{ 
		    			if(data[index].split("\\|")[0x00].equals("#")) continue;
		    			
		    			final String codigoEmpresa = data[index].split("\\|")[0x00];
		    			final String dataEmissao = data[index].split("\\|")[0x02];
		    			
		    			statement.setInt(0x01, Integer.parseInt(codigoEmpresa + (String.format("%06d", Integer.parseInt(data[index].split("\\|")[0x01])))));
		    			statement.setInt(0x02, Integer.parseInt(codigoEmpresa));
		    			
		    			try
		    			{
		    				statement.setInt(0x03, getTipoMovimentoCaixa(data[index].split("\\|")[0x03]).getId());	
		    			}
		    			catch(Exception ex)
		    			{
		    				statement.setNull(0x03, Types.INTEGER);
		    			}
		    			
		    			try
		    			{
			    			statement.setDate(0x04, dataEmissao.equals("") ? new java.sql.Date(0x00) : new java.sql.Date(formatter.parse(dataEmissao).getTime()));
			    			statement.setDouble(0x05, Double.parseDouble(data[index].split("\\|")[0x06]));
			    			statement.setString(0x06, data[index].split("\\|")[0x04]);
		    			}
		    			catch(Exception ex)
		    			{
		    				System.out.println("Erro na conversão de dados da linha " + data[index] + " " + ex.getMessage());
		    			}

		    			
		    			try
		    			{
		    				statement.setInt(0x07, getContaContabil(Integer.parseInt(codigoEmpresa), data[index].split("\\|")[0x07]).getId());
		    			}
		    			catch(Exception ex)
		    			{
		    				statement.setNull(0x07, Types.INTEGER);	
		    			}

		    			statement.execute(); 
		    		}
		    		statement.close();
	        	} 
	        	catch(Exception ex)
	        	{
	        		System.out.println("Exception at block " + startIndex + " " + endIndex + " :: " + ex.getMessage());
	        		ex.printStackTrace();
	        	}
	        } 
	    }   
	    
	    new Thread(new RunBlock(connection, data, 0x01, (int) (data.length * 0.01), listaTipoMovimentoCaixa, listaContaContabil)).start();
	     
	    for(double threadPart = 0.01; threadPart < 0x01; threadPart += 0.01)
	    	new Thread(new RunBlock(connection, data, (int) (data.length * threadPart), (int) (data.length * (threadPart + 0.01)), listaTipoMovimentoCaixa, listaContaContabil)).start();
	}
}