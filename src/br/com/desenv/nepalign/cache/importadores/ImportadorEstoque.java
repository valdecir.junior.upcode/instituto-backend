package br.com.desenv.nepalign.cache.importadores;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import br.com.desenv.nepalign.cache.constantes.Globais;
import br.com.desenv.nepalign.cache.constantes.Util;
import br.com.desenv.nepalign.model.Cor;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.EstoqueProduto;
import br.com.desenv.nepalign.model.Fornecedor;
import br.com.desenv.nepalign.model.GrupoProduto;
import br.com.desenv.nepalign.model.Marca;
import br.com.desenv.nepalign.model.OrdemProduto;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.model.SaldoEstoqueProduto;
import br.com.desenv.nepalign.model.SituacaoEnvioLV;
import br.com.desenv.nepalign.model.Tamanho;
import br.com.desenv.nepalign.model.UnidadeMedida;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ImportadorEstoque 
{
	private static final Logger logger = Logger.getLogger("IMPORTADOR.CACHE.ESTOQUE");
	
	public ImportadorEstoque()
	{
		Runtime.getRuntime().addShutdownHook(new Thread() 
		{
		    public void run() 
		    {
		    	
		    }
		});
	}
	
	public static void main(String[] args) throws Exception
	{ 
		final ImportadorEstoque integrar = new ImportadorEstoque();
		
		integrar.integrarRegistrosEstoque(ConexaoUtil.getEntityManager(), Files.readAllBytes(Paths.get("D:/BARRAS")));
	}

	private void registroMarca(final EntityManager manager, final String[] data)
	{ 
    	/*Marca marca = manager.find(Marca.class, Integer.parseInt(data[0x0002]));
     	
     	if(marca == null)
     	{
     		marca = new Marca();
     		marca.setId(Integer.parseInt(data[0x0002]));
     		marca.setCodigo(Integer.parseInt(data[0x0002]));
     		marca.setDescricao(data[0x0003]);
     		
     		manager.merge(marca);
     	}
     	else
     	{
     		marca.setDescricao(data[0x0003]);
     		manager.merge(marca);
     	}*/
	}

	private void registroGrupoProduto(final EntityManager manager, final String[] data) { } //NÃO UTILIZADO POR ENQUANTO
     
    private void registroCor(final EntityManager manager, final String[] data)
    {
    	Cor cor = manager.find(Cor.class, Integer.parseInt(data[0x0002]));
     	
    	if(cor == null)
    	{
     		cor = new Cor();
     		cor.setId(Integer.parseInt(data[0x0002]));
     		cor.setDescricaoCorEmpresa(data[0x0003]);
     		cor.setDescricaoCorFabrica(data[0x0003]);
     		cor.setCodigo(Integer.parseInt(data[0x0002]));
     		
     		manager.merge(cor);
     	}
    	else
    	{
     		cor.setDescricaoCorEmpresa(data[0x0003]);
     		manager.merge(cor);
    	}
    }

    private void registroProduto(final EntityManager manager, final String[] data)
    {
    	Produto produto = manager.find(Produto.class, Integer.parseInt(data[0x0002]));
			
    	if(produto == null)
    	{
    		produto = new Produto();
    		produto.setId(Integer.parseInt(data[0x0002]));
    		produto.setCodigoProduto(data[0x0002]);
    		produto.setNomeProduto(data[0x0003]);
    		produto.setUnidadeMedida(manager.find(UnidadeMedida.class, UnidadeMedida.UNIDADEMEDIDA_PAR));
    		produto.setGrupo(manager.find(GrupoProduto.class, 0x0000));
    		produto.setAtivoInativo("1");
    		produto.setMarca(manager.find(Marca.class, 0x0000));
    		produto.setFornecedor(manager.find(Fornecedor.class, Fornecedor.FORNECEDOR_NAOINFORMADO));
    		produto.setUltimaAlteracao(new Date());
    		produto.setProdutoLV("0");
    		produto.setSituacaoEnvioLV(manager.find(SituacaoEnvioLV.class, SituacaoEnvioLV.SITUACAOENVIOLV_PENDENTE));
				
    		manager.merge(produto);
    	}
    	else
    	{
    		produto.setNomeProduto(data[0x0003]);
    		manager.merge(produto);
    	}
    }
     
    private void registroTamanho(final EntityManager manager, final String[] data)
    {
    	Tamanho tamanho = manager.find(Tamanho.class, Integer.parseInt(data[0x0002]));
     	
     	if(tamanho == null)
     	{
     		tamanho = new Tamanho();
     		tamanho.setId(Integer.parseInt(data[0x0002]));
     		tamanho.setCodigo(Integer.parseInt(data[0x0002]));
     		tamanho.setDescricao(data[0x0003]);
     		
     		manager.merge(tamanho);
     	}
    }
     
    private void registroOrdemProduto(final EntityManager manager, final String[] data)
    {
    	OrdemProduto ordemProduto = manager.find(OrdemProduto.class, Integer.parseInt(data[0x0002]));
     	
     	if(ordemProduto == null)
     	{
     		ordemProduto = new OrdemProduto();
     		ordemProduto.setId(Integer.parseInt(data[0x0002]));
     		ordemProduto.setCodigo(Integer.parseInt(data[0x0002]));
     		ordemProduto.setDescricao(data[0x0003]);
     		
     		manager.merge(ordemProduto);
     	}
     	else
     	{
     		ordemProduto.setDescricao(data[0x0003]);
     		manager.merge(ordemProduto);
     	}
    }
     
    private void registroEstoqueProduto(final EntityManager manager, final String[] data)
    {
     	EstoqueProduto estoqueProduto = manager.find(EstoqueProduto.class, Integer.parseInt(data[0x0002]));
     	
     	@SuppressWarnings("unchecked")
		List<EstoqueProduto> listaEstoqueProduto = (List<EstoqueProduto>) manager.createNativeQuery("SELECT * FROM estoqueProduto WHERE " +
     			" idProduto = " + data[0x0003] + " and idTamanho = 0 and idOrdemProduto = 0 and idCor = 0;", EstoqueProduto.class).getResultList();
     	
     	for(final EstoqueProduto estoqueProdutoAntigo : listaEstoqueProduto)
     	{
     		manager.remove(estoqueProdutoAntigo);
     		manager.flush();
     	}
     	
     	if(estoqueProduto == null) 
     	{
     		estoqueProduto = new EstoqueProduto();
 			estoqueProduto.setId(Integer.parseInt(data[0x0002]));
 			estoqueProduto.setCodigoBarras(data[0x0002]);
 			estoqueProduto.setCor(manager.find(Cor.class, Integer.parseInt(data[0x0005]))); 
 			estoqueProduto.setOrdemProduto(manager.find(OrdemProduto.class, Integer.parseInt(data[0x0004])));
 			estoqueProduto.setProduto(manager.find(Produto.class, Integer.parseInt(data[0x0003])));
 			estoqueProduto.setSituacaoEnvioLV(manager.find(SituacaoEnvioLV.class, SituacaoEnvioLV.SITUACAOENVIOLV_PENDENTE));
 			estoqueProduto.setTamanho(manager.find(Tamanho.class, Integer.parseInt(data[0x0006])));
     		
     		manager.merge(estoqueProduto);
     	}
     	else
     	{
 			estoqueProduto.setCor(manager.find(Cor.class, Integer.parseInt(data[0x0005]))); 
 			estoqueProduto.setOrdemProduto(manager.find(OrdemProduto.class, Integer.parseInt(data[0x0004])));
 			estoqueProduto.setProduto(manager.find(Produto.class, Integer.parseInt(data[0x0003])));
 			estoqueProduto.setTamanho(manager.find(Tamanho.class, Integer.parseInt(data[0x0006])));
     		
     		manager.merge(estoqueProduto);
     	}
    }
     
    private void registroSaldoEstoqueProduto(final EntityManager manager, final String[] data) throws Exception
    {
     	if(data[0x0002].split("\\" + Util.CACHE_SN).length == 0x0003)
     		return;
     	
     	final EmpresaFisica empresaFisica = manager.find(EmpresaFisica.class, Integer.parseInt(data[0x0002].split("\\" + Util.CACHE_SN)[0x0000]));
     	
     	if(empresaFisica == null)
     		return;

     	@SuppressWarnings("unchecked")
			List<SaldoEstoqueProduto> listaSaldoEstoqueProduto = manager.createNativeQuery("select * from saldoestoqueproduto where idestoqueproduto = " + data[0x0002].split("\\" + Util.CACHE_SN)[0x0001]
     			+ " and idEmpresaFisica = " + empresaFisica.getId().intValue() + " and mes = " + empresaFisica.getMesAberto() + " and ano = " + empresaFisica.getAnoAberto(), SaldoEstoqueProduto.class).getResultList();
     	
     	final SaldoEstoqueProduto saldoEstoqueProduto;
     	
     	if(listaSaldoEstoqueProduto.size() == 0x00)
     	{
 			saldoEstoqueProduto = new SaldoEstoqueProduto();
 			saldoEstoqueProduto.setEstoqueProduto(manager.find(EstoqueProduto.class, Integer.parseInt(data[0x0002].split("\\" + Util.CACHE_SN)[0x0001])));
 			saldoEstoqueProduto.setEmpresaFisica(empresaFisica);
 			saldoEstoqueProduto.setAno(empresaFisica.getAnoAberto());
 			saldoEstoqueProduto.setMes(empresaFisica.getMesAberto());
 			saldoEstoqueProduto.setCusto(null);
 			saldoEstoqueProduto.setCustoMedio(null);
 			saldoEstoqueProduto.setUltimoCusto(null);
 			saldoEstoqueProduto.setSaldo(0.0);
 			saldoEstoqueProduto.setVenda(Double.parseDouble(data[0x0003]));
 			saldoEstoqueProduto.setSituacaoEnvioLV(manager.find(SituacaoEnvioLV.class, SituacaoEnvioLV.SITUACAOENVIOLV_PENDENTE));
 			saldoEstoqueProduto.setDataUltimaAlteracao(new Date());
 			
 			manager.persist(saldoEstoqueProduto);
     	}
     	else if(listaSaldoEstoqueProduto.size() == 0x0001)
     	{
     		saldoEstoqueProduto = listaSaldoEstoqueProduto.get(0x00000);
 			saldoEstoqueProduto.setCusto(null);
 			saldoEstoqueProduto.setCustoMedio(null);
 			saldoEstoqueProduto.setUltimoCusto(null);
 			saldoEstoqueProduto.setSaldo(0.0);
 			saldoEstoqueProduto.setVenda(Double.parseDouble(data[0x0003]));
 			
 			manager.merge(saldoEstoqueProduto);
     	}
     	else
     		throw new Exception("Existe mais de um registro de Saldo para Código de Barras " + data[0x0002].split("\\" + Util.CACHE_SN)[0x0001] + " Empresa " + data[0x0002].split("\\" + Util.CACHE_SN)[0x0000]);
    }
	
    public void integrarRegistrosEstoque(final EntityManager manager, byte[] data) throws Exception
	{
		try
		{
			logger.info("Tentativa de execução do importador de estoque do caché. Total de Bytes {" + data.length + "}. Iniciando leitura");
			
		    manager.getTransaction().begin();
		    manager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0;").executeUpdate();
		    manager.createNativeQuery("SET UNIQUE_CHECKS=0;").executeUpdate();
		    
		    for(String _data : new String(data).split("\r\n"))
		    {
				final String[] sdata = _data.split("\\".concat(Util.CACHE_SP), -0x01);
				
				if(sdata[0x0000].equals("FIM")) break;
				if(sdata[0x0000].equals("#")) continue;
				if(sdata[0x0000].equals("K")) continue; // exclude
				
				if(sdata[0x0001].equals(Globais.PRODUTO_INDEX)) continue;
				else if(sdata[0x0001].equals(Globais.MARCA_INDEX)) continue;
				else if(sdata[0x0001].equals(Globais.ESTOQUEPRODUTO_R)) continue;
				else if(sdata[0x01].equals(Globais.COR))
					registroCor(manager, sdata);
				else if(sdata[0x0001].equals(Globais.MARCA))
					registroMarca(manager, sdata);
				else if(sdata[0x0001].equals(Globais.PRODUTO))
					registroProduto(manager, sdata);
				else if(sdata[0x0001].equals(Globais.TAMANHO))
					registroTamanho(manager, sdata);
				else if(sdata[0x0001].equals(Globais.GRUPOPRODUTO))
					registroGrupoProduto(manager, sdata);
				else if(sdata[0x0001].equals(Globais.ORDEMPRODUTO))
					registroOrdemProduto(manager, sdata);
				else if(sdata[0x0001].equals(Globais.ESTOQUEPRODUTO_P))
					registroEstoqueProduto(manager, sdata);
				else if(sdata[0x0001].equals(Globais.SALDOESTOQUEPRODUTO))
					registroSaldoEstoqueProduto(manager, sdata);
				else
					throw new Exception("Global not identified " + sdata[0x01]);
				

		    	//logger.info(" Dados " + Arrays.toString(data) + " registrados ");
		    }
		    
		    manager.createNativeQuery("SET UNIQUE_CHECKS=1;").executeUpdate();
		    manager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 1;").executeUpdate();
		    
	    	manager.getTransaction().commit();	
	    	
	    	logger.info("Leitura finalizada. Banco de dados atualizado.");
		}
		catch(Exception ex)
		{
			logger.info("Tentativa de execução do importador foi interrompida com um erro!", ex);
			manager.getTransaction().rollback();
			
			throw ex;
		}
		finally
		{
	    	manager.close();
		}
	}
}
