package br.com.desenv.nepalign.cache.importadores;

import gov.tjpr.migracao.persistence.Conexao;
import gov.tjpr.migracao.service.impl.CorCacheService;
import gov.tjpr.migracao.service.impl.EstoqueProdutoCacheService;
import gov.tjpr.migracao.service.impl.FornecedorCacheService;
import gov.tjpr.migracao.service.impl.GrupoProdutoCacheService;
import gov.tjpr.migracao.service.impl.MarcaCacheService;
import gov.tjpr.migracao.service.impl.OrdemProdutoCacheService;
import gov.tjpr.migracao.service.impl.ProdutoCacheService;
import gov.tjpr.migracao.service.impl.SaldoEstoqueProdutoCacheService;
import gov.tjpr.migracao.service.impl.TamanhoCacheService;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ImportadorCacheDiario 
{
	Logger log;

	public void init() throws Exception
	{
		log = Logger.getLogger("ImportadorCache");
		try
		{
			try
			{
				log.log(Level.INFO, "Inicio da importacao de Forencedor");
				importarFornecedor();
				log.log(Level.INFO, "Final da importacao de Fornecedor");
			}
			catch(Exception exForn)
			{
				log.log(Level.SEVERE, "Erro na importacao de Forencedor");
				exForn.printStackTrace();
			}
			try
			{
				log.log(Level.INFO, "Inicio da importacao de Marcas");
				importarMarca();
				log.log(Level.INFO, "Final da importacao de Marcas");
			}
			catch(Exception exMarca)
			{
				log.log(Level.SEVERE, "Erro na importacao de marcas");
				exMarca.printStackTrace();
			}
			try
			{
				log.log(Level.INFO, "Inicio da importacao de Tamanhos");
				importarTamanho();
				log.log(Level.INFO, "Final da importacao de Tamanhos");
			}
			catch(Exception exTamanho)
			{
				log.log(Level.SEVERE, "Erro na importacao de Tamanhos");
				exTamanho.printStackTrace();
			}			
			try
			{
				log.log(Level.INFO, "Inicio da importacao de Cores");
				importarCor();
				log.log(Level.INFO, "Final da importacao de Cores");
			}
			catch(Exception exCor)
			{
				log.log(Level.SEVERE, "Erro na importacao de Cores");
				exCor.printStackTrace();
			}
			try
			{
				log.log(Level.INFO, "Inicio da importacao de Ordem");
				importarOrdemProduto();
				log.log(Level.INFO, "Final da importacao de Ordem");
			}
			catch(Exception exOrdem)
			{
				log.log(Level.SEVERE, "Erro na importacao de Ordens");
				exOrdem.printStackTrace();
			}			
			try
			{
				log.log(Level.INFO, "Inicio da importacao de Grupo");
				importarGrupoProduto();
				log.log(Level.INFO, "Final da importacao de Grupo");
			}
			catch(Exception exGrupo)
			{
				log.log(Level.SEVERE, "Erro na importacao de Grupos");
				exGrupo.printStackTrace();
			}
			try
			{
				log.log(Level.INFO, "Inicio da importacao de Produto");
				importarProduto();
				log.log(Level.INFO, "Final da importacao de Produto");
			}
			catch(Exception exProduto)
			{
				log.log(Level.SEVERE, "Erro na importacao de Produtos");
				exProduto.printStackTrace();
			}
			try
			{
				log.log(Level.INFO, "Inicio da importacao de Estoque Produto");
				importarEstoque();
				log.log(Level.INFO, "Final da importacao de Estoque Produto");
			}
			catch(Exception exEstoque)
			{
				log.log(Level.SEVERE, "Erro na importacao de Estoque");
				exEstoque.printStackTrace();
			}
			try
			{
				log.log(Level.INFO, "Inicio da importacao de Saldo Estoque Produto");
				importarSaldoEstoque();
				log.log(Level.INFO, "Final da importacao de Saldo Estoque Produto");
			}
			catch(Exception exSaldoEstoque)
			{
				log.log(Level.SEVERE, "Erro na importacao de Saldo Estoque Produto");
				exSaldoEstoque.printStackTrace();
			}

			try
			{
				log.log(Level.INFO, "Inicio da procedure de atualizacao.");
				finalizaImportacao();
				log.log(Level.INFO, "Final das procedure de atualizacao.");
			}
			catch(Exception exForn)
			{
				log.log(Level.SEVERE, "Erro na Procedure de atualizacao.");
				exForn.printStackTrace();
			}			
		}
		catch(Exception ex)
		{
			throw ex; 
		}
		
	}
	
	public void finalizaImportacao() throws Exception
	{
		try
		{
			Connection con = Conexao.getConexaoMySql();
			CallableStatement proc = con.prepareCall("{ call TransfereDadosTempNepal()}");
			proc.execute();
			proc.close();
			con.close();
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
	
	public void importarProduto() throws Exception
	{
		ProdutoCacheService obj = new ProdutoCacheService();
		obj.migrarCacheMigracao();
		System.out.println("FIM.");
	}
	public void importarEstoque() throws Exception
	{
		EstoqueProdutoCacheService obj = new EstoqueProdutoCacheService();
		obj.migrarCacheMigracao();
		System.out.println("FIM.");
	}
	public void importarSaldoEstoque() throws Exception
	{
		SaldoEstoqueProdutoCacheService obj = new SaldoEstoqueProdutoCacheService();
		obj.migrarCacheMigracao();
		System.out.println("FIM.");
	}	
	public void importarMarca() throws Exception
	{
		MarcaCacheService obj = new MarcaCacheService();
		obj.migrarCacheMigracao();
		System.out.println("FIM.");
	}
	public void importarCor() throws Exception
	{
		CorCacheService obj = new CorCacheService ();
		obj.migrarCacheMigracao();
		System.out.println("FIM.");
	}
	public void importarGrupoProduto() throws Exception
	{
		GrupoProdutoCacheService obj = new GrupoProdutoCacheService();
		obj.migrarCacheMigracao();
		System.out.println("FIM.");
	}	
	public void importarOrdemProduto() throws Exception
	{
		OrdemProdutoCacheService obj = new OrdemProdutoCacheService();
		obj.migrarCacheMigracao();
		System.out.println("FIM.");
	}
	public void importarTamanho() throws Exception
	{
		TamanhoCacheService obj = new TamanhoCacheService();
		obj.migrarCacheMigracao();
		System.out.println("FIM.");
	}

	public void importarFornecedor() throws Exception
	{
		FornecedorCacheService obj = new FornecedorCacheService();
		obj.migrarCacheMigracao();
		System.out.println("FIM.");	
	}
	
	public static void main(String[] args) throws Exception
	{
		ImportadorCacheDiario imp = new ImportadorCacheDiario();
		imp.init();
	}	
}
