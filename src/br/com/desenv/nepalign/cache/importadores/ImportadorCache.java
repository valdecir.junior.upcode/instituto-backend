package br.com.desenv.nepalign.cache.importadores;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

abstract class ImportadorCache 
{
	protected static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	protected static SimpleDateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	class Counter
	{
		private int count;
		
		Counter()
		{
			count++;
		}
		
		public void increment()
		{
			count++;
		}
		
		@Override
		public String toString()
		{
			return String.valueOf(count);
		}
	}
	
	protected String[] readFile(String fileName) throws IOException
	{
		Logger.getLogger(ImportadorCache.this.getClass()).info("Fazendo leitura do arquivo: ".concat(fileName));
		List<String> result = new ArrayList<String>();
		
		BufferedReader br = null;
		try 
		{
			String sCurrentLine;

			br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "Cp1252"));

			while ((sCurrentLine = br.readLine()) != null) 
			{
				if(sCurrentLine.startsWith("//")) continue;
				if(sCurrentLine.equals("FIM")) break;
				
				sCurrentLine = sCurrentLine.replace("\'", " ");
				
				result.add(sCurrentLine);	
			}
		} 
		catch (IOException e) 
		{
			throw e;
		}
		finally 
		{
			try 
			{
				if (br != null) br.close();
			}
			catch (IOException ex) 
			{
				ex.printStackTrace();
			}
		}
		Logger.getLogger(ImportadorCache.this.getClass()).info("Leitura concluída. Tamanho dos dados: ".concat(String.valueOf(result.size())));
		return result.toArray(new String[result.size()]);
	}
}
