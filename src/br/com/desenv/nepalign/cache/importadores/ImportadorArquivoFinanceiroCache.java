package br.com.desenv.nepalign.cache.importadores;

import hu.ssh.progressbar.ProgressBar;
import hu.ssh.progressbar.console.ConsoleProgressBar;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import br.com.desenv.nepalign.model.Banco;
import br.com.desenv.nepalign.model.BandeiraCartao;
import br.com.desenv.nepalign.model.FormaPagamentoCartao;
import br.com.desenv.nepalign.model.TaxaCartao;
import br.com.desenv.nepalign.service.BancoService;
import br.com.desenv.nepalign.service.BandeiraCartaoService;
import br.com.desenv.nepalign.service.FormaPagamentoCartaoService;
import br.com.desenv.nepalign.service.TaxaCartaoService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

import com.google.common.primitives.Ints;
import com.mysql.jdbc.Statement;

public class ImportadorArquivoFinanceiroCache 
{
	/** DADOS TIPO EMPRESA
	    [1] EMPRESAS_FINANCEIRO_CHEQUE      1,2,3,4,5,6,7,8,9,10,13,11,17,18,19,50,52,53,54,55,56,131
	    [2] EMPRESAS_FINANCEIRO_CARTÃO      NOT IN(CHEQUE, CARTAO)
	    [3] EMPRESAS_FINANCEIRO_VALECALCADO 20,31,37,47,57,72,82,92,130,310,336
	    [4] ???								???
	**/
	
	/* 6,56,106,136,206,236,256,336,406,506,806,836 */
	
	private static final int[] empresasVale = new int[] { 20, 31, 37, 47, 57, 72, 82, 92, 130, 310, 336 };
	private static final int[] empresasCheque = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 13, 11, 17, 18, 19, 50, 52, 53, 54, 55, 56, 131 };
	
	private static final int[] empresasPaulo = new int[] { 6, 56, 106, 136, 206, 236, 256, 336, 406, 506, 806, 836 };
	
	private final int tipoEmpresa(final int idEmpresaFinanceiro)
	{
		for(final int idEmpresaVale : empresasVale)
		{
			if(idEmpresaVale == idEmpresaFinanceiro)
				return 0x03; // VALE
		}
		
		for(final int idEmpresaVale : empresasCheque)
		{
			if(idEmpresaVale == idEmpresaFinanceiro)
				return 0x01; // CHEQUE
		}
		
		return 0x02; // CARTÃO
	}
	
	private final void log(String value)
	{
		System.out.println("[" + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()) + "] " + value);
	}
	
	public ImportadorArquivoFinanceiroCache()
	{
		Runtime.getRuntime().addShutdownHook(new Thread() 
		{
		    public void run() 
		    {
		    	log("integração concluída com sucesso!");
		    }
		});
	}
	
	public static void main(String[] args) throws Exception
	{ 
		final ImportadorArquivoFinanceiroCache integrar = new ImportadorArquivoFinanceiroCache();
		
		System.out.println("Essa importação irá importar somente as empresas Paulo. Deseja continuar?");
		Thread.sleep(10000);
		System.in.read();

		//integrar.integrarEmpresa(ConexaoUtil.getConexaoPadrao(), integrar.readFile("F:/ARQUIVOS/fin/EMP"));
		integrar.integrarRegistroCRec(ConexaoUtil.getConexaoPadrao(), integrar.readFile("F:/ARQUIVOS/fin/MOV"));
	}

	public String[] readFile(String fileName) throws IOException
	{
		List<String> result = new ArrayList<String>();
		
		BufferedReader br = null;

		try  
		{
			String sCurrentLine;

			br = new BufferedReader(new FileReader(fileName));

			while ((sCurrentLine = br.readLine()) != null) 
			{
				if(sCurrentLine.equals("FIM")) break; 
				
				result.add(sCurrentLine);	
			}
		} 
		catch (IOException e) 
		{
			throw e;
		}
		finally 
		{
			try 
			{
				if (br != null) br.close();
			}
			catch (IOException ex) 
			{
				ex.printStackTrace();
			}
		}
		return result.toArray(new String[result.size()]);
	}

	public void integrarEmpresa(Connection connection, String[] data) throws SQLException, ParseException
	{
		@SuppressWarnings("rawtypes")
		ProgressBar progressBar = ConsoleProgressBar.on(System.out).withFormat("[:bar] :percent% :elapsed").withTotalSteps(data.length);
		
		log("Integrando Empresas...");
		log("Desabilitando chaves estrangeiras e limpando dados antigos...");
		
		connection.createStatement().execute("SET FOREIGN_KEY_CHECKS=0;"); 
		//connection.createStatement().execute("TRUNCATE TABLE taxaCartao;");
		//connection.createStatement().execute("TRUNCATE TABLE empresaFinanceiro;");
		
		//PreparedStatement statementEmpresa = connection.prepareStatement("INSERT INTO empresaFinanceiro(idEmpresaFinanceiro, nomeEmpresa, mesAberto, anoAberto, idEmpresaFisica, tipoEmpresa) VALUES (?, ?, ?, ?, 2, ?); ");
		PreparedStatement statementTaxaCartao = connection.prepareStatement("INSERT INTO taxacartao(idEmpresaFinanceiro, parcela, taxa, idFormaPagamentoCartao, dataVigencia) VALUES (?, ?, ?, ?, DATE(NOW())); ");
		
		log("Iniciando persistencia dos dados... [" + data.length + "] registros!");
		
		for(int index = 0x01; index < data.length; index++)
		{ 
			progressBar.tickOne();
			
			final String[] splittedData = data[index].split("\\|", -0x01); 
			
			final String codigoEmpresa = splittedData[0x00];
			final String nomeEmpresa = splittedData[0x01];
			final String mesAberto = splittedData[0x04];
			final String anoAberto = new SimpleDateFormat("yyyy").format(new SimpleDateFormat("yy").parse(splittedData[0x05]));

			/*statementEmpresa.setInt(0x01, Integer.parseInt(codigoEmpresa));
			statementEmpresa.setString(0x02, nomeEmpresa);
			statementEmpresa.setInt(0x03, Integer.parseInt(mesAberto));
			statementEmpresa.setInt(0x04, Integer.parseInt(anoAberto));
			statementEmpresa.setInt(0x05, tipoEmpresa(Integer.parseInt(codigoEmpresa)));
			
			statementEmpresa.execute();*/
			 
			if(!splittedData[0x06].equals("")) // taxa debito
			{
				statementTaxaCartao.setInt(0x01, Integer.parseInt(codigoEmpresa));
				statementTaxaCartao.setInt(0x02, 0x01); // debito = null??? //TODO
				statementTaxaCartao.setDouble(0x03, Double.parseDouble(splittedData[0x06])); // taxa
				statementTaxaCartao.setInt(0x04, 0x01); // forma pagamento debito
				
				statementTaxaCartao.execute();
			}
			
			if(!splittedData[0x07].equals("")) // taxa credito
			{
				statementTaxaCartao.setInt(0x01, Integer.parseInt(codigoEmpresa));
				statementTaxaCartao.setInt(0x02, 0x01); // parcela
				statementTaxaCartao.setDouble(0x03, Double.parseDouble(splittedData[0x07])); // taxa
				statementTaxaCartao.setInt(0x04, 0x02); // forma pagamento credito
				
				statementTaxaCartao.execute();	
			}
			
			if(!splittedData[0x08].equals("")) // taxa parcela 1 a 6
			{
				for(int parcela = 0x01; parcela <= 6; parcela++)
				{
					statementTaxaCartao.setInt(0x01, Integer.parseInt(codigoEmpresa));
					statementTaxaCartao.setInt(0x02, parcela); // parcela
					statementTaxaCartao.setDouble(0x03, Double.parseDouble(splittedData[0x08])); // taxa
					statementTaxaCartao.setInt(0x04, 0x03); // forma pagamento credito
					
					statementTaxaCartao.execute();	
				}
			}
			
			if(!splittedData[0x09].equals("")) // taxa parcela 1 a 6
			{
				for(int parcela = 0x07; parcela <= 12; parcela++)
				{
					statementTaxaCartao.setInt(0x01, Integer.parseInt(codigoEmpresa));
					statementTaxaCartao.setInt(0x02, parcela); // parcela
					statementTaxaCartao.setDouble(0x03, Double.parseDouble(splittedData[0x09])); // taxa
					statementTaxaCartao.setInt(0x04, 0x03); // forma pagamento credito
					
					statementTaxaCartao.execute();	
				}
			}
		}
		
		log("Concluído!");

		statementTaxaCartao.close();
		//statementEmpresa.close();
	}
	
	public void integrarRegistroCRec(Connection connection, String[] data) throws Exception
	{
		log("Integrando Lançamentos...");
		log("Desabilitando chaves estrangeiras e limpando dados antigos...");
		connection.createStatement().execute("SET FOREIGN_KEY_CHECKS=0;");
		connection.createStatement().execute("DELETE FROM baixalancamentocontareceber where idLancamentoContaReceber in "
				+ " (select idLancamentoContaReceber from lancamentocontareceber where idEmpresaFinanceiro in " +
				" (6,56,106,136,206,236,256,336,406,506,806,836));");
		
		connection.createStatement().execute("delete from lancamentocontareceber where idEmpresaFinanceiro in "
				+ " (6,56,106,136,206,236,256,336,406,506,806,836); ");


		//connection.createStatement().execute("TRUNCATE TABLE baixaLancamentoContaReceber;");
		//connection.createStatement().execute("TRUNCATE TABLE lancamentoContaReceber;");
		
		log("Registrando objetos necessários...");
		final List<Banco> listaBanco = new BancoService().listar();
		final List<TaxaCartao> listaTaxaCartao = new TaxaCartaoService().listar();
		final List<BandeiraCartao> listaBandeiraCartao = new BandeiraCartaoService().listar();
		final List<FormaPagamentoCartao> listaFormaPagamentoCartao = new FormaPagamentoCartaoService().listar();
		
		log("Registrando Workclass...");
		
	    class RunBlock implements Runnable 
	    {
	        Connection connection;
	        
	        final int startIndex, endIndex;
	        final String[] data;
	        
	        final List<Banco> listaBanco;
	        final List<TaxaCartao> listaTaxaCartao;
	        final List<BandeiraCartao> listaBandeiraCartao;
	        
	        @SuppressWarnings("unused")
			final List<FormaPagamentoCartao> listaFormaPagamentoCartao;
	        
	        final PreparedStatement statementBanco;
	        
	        final Integer getBanco(String codigo)
	        {
	        	synchronized (listaBanco) 
	        	{
		        	for(Banco banco : listaBanco)
		        	{
		        		if(banco.getCodigo().equals(Double.parseDouble(codigo)))
		        			return banco.getId();
		        	}
		        	
		        	try 
		        	{
						statementBanco.setDouble(0x01, Double.parseDouble(codigo));
						statementBanco.setString(0x02, "BANCO não CADASTRADO");
						statementBanco.execute();
						
						final ResultSet generatedKeys = statementBanco.getGeneratedKeys();
						
						if(generatedKeys.next())
						{
							Banco banco = new Banco();
							banco.setId(generatedKeys.getInt(0x01));
							banco.setCodigo(Double.parseDouble(codigo));
							 
							listaBanco.add(banco);
							
							return banco.getId();	 
						} 
					} 
		        	catch (NumberFormatException e) 
					{
						e.printStackTrace();
					} 
		        	catch (SQLException e) 
		        	{
						e.printStackTrace();
					}	
				}

	        	return null;
	        }
	        
	        final Integer getTaxaCartao(int idEmpresaFinanceiro, int idFormaPagamentoCartao, double taxa)
	        {
	        	for(TaxaCartao taxaCartao : listaTaxaCartao)
	        	{
	        		if(taxaCartao.getEmpresaFinanceiro().getId().intValue() == idEmpresaFinanceiro &&
	        				taxaCartao.getFormaPagamentoCartao().getId().intValue() == idFormaPagamentoCartao && taxaCartao.getTaxa().doubleValue() == taxa)
	        			return taxaCartao.getId();
	        	}
	        	return null;
	        }
	        
	        final Integer getFormaPagamentoCartao(final String tipo)
	        {
	        	if(tipo.equals("D1"))
	        		return 0x01;
	        	else if(tipo.equals("C1"))
	        		return 0x02;
	        	else
	        		return 0x03;
	        }
	        
	        final Integer getFormaPagamentoCartao(String emitente, String observacao, int parcela)
	        {
	        	for(String tmp : emitente.split(" "))
	        	{
		        	if((tmp.toLowerCase().contains("debito") || tmp.toLowerCase().contains("débito")) && parcela == 0x01)
		        		return 0x01;
		        	else if((tmp.toLowerCase().contains("credito") || tmp.toLowerCase().contains("crédito") || tmp.toLowerCase().contains("parcelado")) && parcela == 0x01)
		        		return 0x02;
		        	else if((tmp.toLowerCase().contains("credito") || tmp.toLowerCase().contains("crédito") || tmp.toLowerCase().contains("parcelado")) && parcela > 0x01)
		        		return 0x03;	
	        	}
	        	for(String tmp : observacao.split(" "))
	        	{
		        	if((tmp.toLowerCase().contains("debito") || tmp.toLowerCase().contains("débito")) && parcela == 0x01)
		        		return 0x01;
		        	else if((tmp.toLowerCase().contains("credito") || tmp.toLowerCase().contains("crédito") || tmp.toLowerCase().contains("parcelado")) && parcela == 0x01)
		        		return 0x02;
		        	else if((tmp.toLowerCase().contains("credito") || tmp.toLowerCase().contains("crédito") || tmp.toLowerCase().contains("parcelado")) && parcela > 0x01)
		        		return 0x03;	
	        	}
	        	return null;
	        }
	        
	        final Integer getBandeiraCartao(String emitente, String observacao, Integer idEmpresaFinanceiro)
	        {
	        	String pesquisa = "";
	        	
	        	for(String tmp : emitente.split(" "))
	        	{
	        		if(tmp.toLowerCase().equals("master"))
	        			pesquisa = "master";
	        		else if(tmp.toLowerCase().equals("visa"))
	        			pesquisa = "visa";
	        		else if(tmp.toLowerCase().equals("elo"))
	        			pesquisa = "elo";
	        		else if(tmp.toLowerCase().equals("cielo"))
	        			pesquisa = "cielo";
	        		else if(tmp.toLowerCase().equals("sicredi"))
	        			pesquisa = "sicredi";
	        		else if(tmp.toLowerCase().equals("hiper"))
	        			pesquisa = "hipercard";
	        		else if(tmp.toLowerCase().equals("hipercard"))
	        			pesquisa = "hipercard";
	        	}
	        	
	        	for(String tmp : observacao.split(" "))
	        	{
	        		if(tmp.toLowerCase().equals("master"))
	        			pesquisa = "master";
	        		else if(tmp.toLowerCase().equals("visa"))
	        			pesquisa = "visa";
	        		else if(tmp.toLowerCase().equals("elo"))
	        			pesquisa = "elo";
	        		else if(tmp.toLowerCase().equals("cielo"))
	        			pesquisa = "cielo";
	        		else if(tmp.toLowerCase().equals("sicredi"))
	        			pesquisa = "sicredi";
	        		else if(tmp.toLowerCase().equals("hiper"))
	        			pesquisa = "hipercard";
	        		else if(tmp.toLowerCase().equals("hipercard"))
	        			pesquisa = "hipercard";
	        	}
	        	
	        	if(pesquisa.equals(""))
	        		return null;
	        	else if(pesquisa.equals("") && tipoEmpresa(idEmpresaFinanceiro) != 0x02)
	        		return null;
	        	
	        	for(BandeiraCartao bandeira : listaBandeiraCartao)
	        	{
	        		if(bandeira.getDescricao().equalsIgnoreCase(pesquisa))
	        			return bandeira.getId();
	        	}
	        	
	        	return null;
	        }

	        RunBlock(final Connection connection, final String[] data, final int startIndex, final int endIndex, final List<Banco> listaBanco, final List<TaxaCartao> listaTaxaCartao,
	        		final List<BandeiraCartao> listaBandeiraCartao, final List<FormaPagamentoCartao> listaFormaPagamentoCartao) throws SQLException 
	        { 
	        	this.connection = connection;
	        	this.startIndex = startIndex;
	        	this.endIndex = endIndex;
	        	this.data = data;
	        	
	        	this.listaBanco = listaBanco;
	        	this.listaTaxaCartao = listaTaxaCartao;
	        	this.listaBandeiraCartao = listaBandeiraCartao;
	        	this.listaFormaPagamentoCartao = listaFormaPagamentoCartao;
	        	this.statementBanco = connection.prepareStatement("INSERT INTO banco (codigo, descricao) VALUES (?, ?);", Statement.RETURN_GENERATED_KEYS);
	        }
	        
	        public void run() 
	        {
	        	try
	        	{ 
	        		connection = ConexaoUtil.getConexaoPadrao();
	        		connection.createStatement().execute("SET FOREIGN_KEY_CHECKS=0;");	
	        		
		        	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		    		
		    		final PreparedStatement statement = connection.prepareStatement("INSERT INTO lancamentoContaReceber(idLancamentoContaReceber, idEmpresaFinanceiro, idBanco, numeroDocumento, dataLancamento, dataVencimento, valorLancamento, valorParcelaSemDesconto, valorTotal, quantidadeParcelas, idSituacaoLancamento, emitente, cpfCnpjEmitente, observacao, percentualCartao, idBandeiraCartao, idTaxaCartao, idFormaPagamentoCartao, numeroParcela, dataQuitacao) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); ", Statement.RETURN_GENERATED_KEYS);
		    		final PreparedStatement statementBaixa = connection.prepareStatement("INSERT INTO baixaLancamentoContaReceber(idLancamentoContaReceber, valor, dataBaixa, tipoBaixa, observacao) VALUES (?, ?, ?, ?, ?); ");
		    		
		    		for(int index = startIndex; index < endIndex; index++)
		    		{  
		    			if(data[index].split("\\|", -0x01)[0x00].equals("#")) continue;
		    			
		    			final String[] splitteData = data[index].split("\\|", -0x01);

		    			final String codigoEmpresa = splitteData[0x00];
		    			
		    			if(!Ints.contains(empresasPaulo, Integer.parseInt(codigoEmpresa)))
		    				continue;
		    			
		    			@SuppressWarnings("unused")
						final String numLanc = splitteData[0x01];
		    			final String numDoc = splitteData[0x02];
		    			final String dataEmissao = splitteData[0x03]; 
		    			final String dataVencimento = splitteData[0x04];
		    			final String situacaoLancamento = splitteData[0x06].equals("0") ? "1" : "2";
		    			final String dataBaixa = splitteData[0x07];
		    			
		    			//statement.setInt(0x01, Integer.parseInt(codigoEmpresa + (String.format("%06d", Integer.parseInt(numLanc))))); // idLancamentoContaReceber
		    			statement.setNull(0x01, Types.INTEGER);
		    			statement.setInt(0x02, Integer.parseInt(codigoEmpresa)); // idEmpresaFinanceiro
		    			
		    			if(!splitteData[0x0B].trim().equals(""))
		    			{
			    			final Integer codigoBanco = getBanco(splitteData[0x0B]);
			    			
			    			//idBanco
			    			if(codigoBanco == null)
			    				statement.setNull(0x03, Types.INTEGER);
			    			else
			    				statement.setInt(0x03, codigoBanco);		
		    			}
		    			 
		    			//numeroDocumento
		    			statement.setString(0x04, numDoc);
		    			statement.setDate(0x05, dataEmissao.equals("") ? new java.sql.Date(0x00) : new java.sql.Date(formatter.parse(dataEmissao).getTime())); // datalancamento
		    			statement.setDate(0x06, dataVencimento.equals("") ? new java.sql.Date(0x00) : new java.sql.Date(formatter.parse(dataVencimento).getTime())); // datavencimento
		    			statement.setDouble(0x07, Double.parseDouble(splitteData[0x05])); // valorLancamento
		    			
		    			// valor parcela sem desconto
		    			if(splitteData[0x0D].equals("")) 
		    				statement.setNull(0x08, Types.DOUBLE);
		    			else
		    				statement.setDouble(0x08, Double.parseDouble(splitteData[0x0D]));
		    			
		    			// valor total
		    			if(splitteData[0x0F].equals(""))
		    				statement.setNull(0x09, Types.DOUBLE);
		    			else
		    				statement.setDouble(0x09, Double.parseDouble(splitteData[0x0F]));

		    			// quantidade parcelas
		    			if(splitteData[0x0E].equals(""))
		    				statement.setNull(0x0A, Types.INTEGER);
		    			else
		    				statement.setInt(0x0A, Integer.parseInt(splitteData[0x0E]));
		    			
		    			statement.setInt(0x0B, Integer.parseInt(situacaoLancamento)); // situacao
		    			
		    			if(splitteData[0x0A].equals(""))
		    				statement.setNull(0x0C, Types.VARCHAR);
		    			else
		    				statement.setString(0x0C, splitteData[0x0A]); // emitente
		    			
		    			if(splitteData[0x09].equals(""))
		    				statement.setNull(0x0D, Types.VARCHAR);
		    			else
		    				statement.setString(0x0D, splitteData[0x09]); // cpf
		    			
		    			//observacao
		    			if(splitteData[0x08].trim().equals(""))
		    				statement.setNull(0x0E, Types.VARCHAR);
		    			else
		    				statement.setString(0x0E, splitteData[0x08]);
		    			
		    			if(splitteData[0x0C].trim().equals(""))
		    				statement.setNull(0x0F, Types.VARCHAR);
		    			else
		    				statement.setString(0x0F, splitteData[0x0C]);
		    			
		    			final Integer bandeiraCartao = getBandeiraCartao(splitteData[0x0A], splitteData[0x08], Integer.parseInt(codigoEmpresa));
		    			
		    			if(bandeiraCartao == null)
		    				statement.setNull(0x10, Types.INTEGER);
		    			else
		    				statement.setInt(0x10, bandeiraCartao);
		    			
		    			if(bandeiraCartao != null)
		    			{
		    				if(splitteData[0x0E].equals(""))
		    				{
			    				statement.setNull(0x11, Types.INTEGER);
			    				statement.setNull(0x12, Types.INTEGER);
		    				}
		    				else
		    				{
		    					Integer formaPagamentoCartao;
		    					
		    					if(splitteData[0x13] != null && !splitteData[0x13].equals(""))
		    						formaPagamentoCartao = getFormaPagamentoCartao(splitteData[0x13]);
		    					else
		    						formaPagamentoCartao = getFormaPagamentoCartao(splitteData[0x0A], splitteData[0x08], Integer.parseInt(splitteData[0x0E]));
				    			
				    			if(formaPagamentoCartao == null)
				    			{
				    				statement.setNull(0x11, Types.INTEGER);
				    				statement.setNull(0x12, Types.INTEGER);
				    			}
				    			else
				    			{
				    				if(!splitteData[0x0C].equals(""))
				    				{
						    			Integer taxaCartao = getTaxaCartao(Integer.parseInt(codigoEmpresa), formaPagamentoCartao, Double.parseDouble(splitteData[0x0C]));
						    			
						    			if(taxaCartao == null)
						    				statement.setNull(0x11, Types.INTEGER);
						    			else
						    				statement.setInt(0x11, taxaCartao);	
				    				}
					    			
				    				statement.setInt(0x12, formaPagamentoCartao);
				    			}		
		    				}
		    				
		    				try
		    				{
		    					statement.setInt(0x13, Integer.parseInt(numDoc));	
		    				}
		    				catch(Exception numdocexc)
		    				{
		    					statement.setNull(0x13, Types.INTEGER);
		    				}
		    			}
		    			else
		    			{
		    				statement.setNull(0x11, Types.INTEGER);
		    				statement.setNull(0x12, Types.INTEGER);
		    				statement.setNull(0x13, Types.INTEGER);
		    			}
		    			
		    			if(dataBaixa.equals(""))
		    				statement.setNull(0x14, Types.DATE);
		    			else 
		    				statement.setDate(0x14, new java.sql.Date(formatter.parse(dataBaixa).getTime()));
		    			
		    			statement.execute();
		    			
		    			final ResultSet resultSet = statement.getGeneratedKeys();
		    			
		    			if(resultSet != null && resultSet.next())
		    			{
			    			if(Integer.parseInt(situacaoLancamento) == 0x02)
			    			{
			    				statementBaixa.setInt(0x01, resultSet.getInt(0x01));
			    				statementBaixa.setDouble(0x02, Double.parseDouble(splitteData[0x05]));
			    				statementBaixa.setDate(0x03, dataBaixa.equals("") ? new java.sql.Date(0x00) : new java.sql.Date(formatter.parse(dataBaixa).getTime()));
			    				statementBaixa.setInt(0x04, 0x01); //BAIXA SEMPRE INTEGRAL
			    				
			    				if(splitteData[0x08].equals(""))
			    					statementBaixa.setNull(0x05, Types.VARCHAR);
			    				else
			    					statementBaixa.setString(0x05, splitteData[0x08]);
			    				
			    				statementBaixa.execute();
			    			}	
		    			}
		    			else
		    				throw new Exception();
		    		}
		    		statement.close(); 
	        	} 
	        	catch(Exception ex)
	        	{
	        		ex.printStackTrace();
	        		System.out.println("Exception at block " + startIndex + " " + endIndex + " :: " + ex.getMessage());
	        	}
	        } 
	    }   
	    
	    log("Iniciando blocos de execução...");
	    
	    new Thread(new RunBlock(connection, data, 0x01, (int) (data.length * 0.01), listaBanco, listaTaxaCartao, listaBandeiraCartao, listaFormaPagamentoCartao)).start();
	     
	    for(double threadPart = 0.01; threadPart < 0x01; threadPart += 0.01)
	    	new Thread(new RunBlock(connection, data, (int) (data.length * threadPart), (int) (data.length * (threadPart + 0.01)), listaBanco, listaTaxaCartao, listaBandeiraCartao, listaFormaPagamentoCartao)).start();
	    
	    log("Blocos iniciados com sucesso!");
	}
}
