package br.com.desenv.nepalign.cache.importadores;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import br.com.desenv.nepalign.cache.constantes.Util;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ImportadorArquivoAgendaCache extends ImportadorCache
{	
	public static void main(String[] args)
	{
		final String diretorioBase = args.length > 0x00 ? args[0x00] : "D:\\Desenv\\ARQ_CONV\\";
		
		new ImportadorArquivoAgendaCache().run(diretorioBase);
	}
	
	enum FILE_METADATA_AGENDA
	{
		CODIGO((short) 0),
		NOME((short) 1),
		CIDADE((short) 2),
		TELEFONE1((short) 3),
		TELEFONE2((short) 4);
		
		private final short index;
		
		FILE_METADATA_AGENDA(final short index)
		{
			this.index = index;
		}
		
		short getIndex()
		{
			return index;
		}
	}
	
	public void run(final String diretorioBase)
	{
		EntityManager manager = ConexaoUtil.getFactory("importacao", "importacao!123").createEntityManager();
		try 
		{
			manager.getTransaction().begin();
			clean(manager);
			manager.getTransaction().commit();
			
			importarAgenda(manager, readFile(diretorioBase.concat("AGENDA.TXT")));
		}
		catch(InterruptedException ie)
		{
			ie.printStackTrace();
			manager.getTransaction().rollback();
		}
		catch (IOException e) 
		{
			manager.getTransaction().rollback();
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			manager.close();
		}
	}
	
	private void clean(final EntityManager manager)
	{
		manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=0;").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE agenda;").executeUpdate();
		manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=1;").executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	public void importarAgenda(final EntityManager manager, final String data[]) throws InterruptedException
	{
		Logger.getLogger(ImportadorArquivoAgendaCache.this.getClass()).info("Iniciando bloco de importação de Agenda");
		
		try
		{
			Connection connection = ConexaoUtil.getConexaoPadrao("importacao", "importacao!123");
			PreparedStatement ps = connection.prepareStatement("INSERT INTO agenda (nome, telefoneComecial, telefoneResidencial, cidade)" +
					"VALUES(?, ?, ?, ?); ");
			
			for(int index = 1; index < data.length; index++)
			{
				final String[] metadata = data[index].split(Util.CACHE_FILE_SEPARATOR, -0x01);
				
				ps.setString(1, metadata[FILE_METADATA_AGENDA.NOME.getIndex()]);
				ps.setString(2, metadata[FILE_METADATA_AGENDA.TELEFONE1.getIndex()]);
				ps.setString(3, metadata[FILE_METADATA_AGENDA.TELEFONE2.getIndex()]);
				ps.setString(4, metadata[FILE_METADATA_AGENDA.CIDADE.getIndex()]);
				
				ps.executeUpdate();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

	    Logger.getLogger(ImportadorArquivoAgendaCache.this.getClass()).info("Bloco de importação de Agenda finalizado!");
	}
}