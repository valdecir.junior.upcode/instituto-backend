package br.com.desenv.nepalign.cache.importadores;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import br.com.desenv.nepalign.cache.constantes.Util;
import br.com.desenv.nepalign.model.Cidade;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;


public class ImportadorArquivoClientesCache extends ImportadorCache
{
	private static BufferedWriter cidadeNaoEncontradaWriter;
	
	public static void main(String[] args) throws Exception
	{
		final String diretorioBase = args.length > 0x00 ? args[0x00] : "D:\\Desenv\\ARQ_CONV\\";
		
		new ImportadorArquivoClientesCache(diretorioBase).run(diretorioBase);
	}
	
	public ImportadorArquivoClientesCache(final String diretorioBase)
	{
		final File arquivoLogCidades = new File(diretorioBase.concat("cidade_nao_encontrada.txt"));
		try
		{
			if(arquivoLogCidades.exists())
			{
				Logger.getLogger(ImportadorArquivoClientesCache.class).warn("Apagando arquivo de Logs de Cidade antigo....");
				arquivoLogCidades.delete();	
			}
			
			Logger.getLogger(ImportadorArquivoClientesCache.class).info("Criando arquivo de Logs de Cidades não encontradas. Caminho : " + arquivoLogCidades.getAbsolutePath());
			arquivoLogCidades.createNewFile();
			
			Logger.getLogger(ImportadorArquivoClientesCache.class).info("Concluído");
			cidadeNaoEncontradaWriter = new BufferedWriter(new FileWriter(arquivoLogCidades));	
		}
		catch(Exception ex)
		{
			Logger.getLogger(ImportadorArquivoClientesCache.class).error("Não foi possível escrever o arquivo " + arquivoLogCidades, ex);
		}
	}
	
	enum FILE1_METADATA
	{
		CODIGO_CLIENTE((short) 0),
		NOME((short) 1),
		ENDERECO((short) 2),
		CEP((short) 3),
		CIDADE((short) 4),
		ESTADO((short) 5),
		CPF((short) 6),
		BAIRRO((short) 7),
		CONJUGE((short) 8),
		DATA_NASCIMENTO((short) 9),
		TELEFONE((short) 10),
		RENDA((short) 11);
		
		private final short index;
		
		FILE1_METADATA(final short index)
		{
			this.index = index;
		}
		
		short getIndex()
		{
			return index;
		}
	}
	
	enum FILE2_METADATA
	{
		CODIGO_CLIENTE((short) 0),
		ESTADO_CIVIL((short) 1),
		RG((short) 2),
		EMPREGO((short) 3),
		CIDADE_EMPREGO((short) 4),
		ENDERECO_COMERCIAL((short) 5),
		TELEFONE_COMERCIAL((short) 6),
		PROFISSAO((short) 8),
		OUTRA_RENDA((short) 9),
		PAI((short) 10),
		MAE((short) 11);
		
		private final short index;
		
		FILE2_METADATA(final short index)
		{
			this.index = index;
		}
		
		short getIndex()
		{
			return index;
		}
	}
	
	enum FILE3_METADATA
	{
		CODIGO_CLIENTE((short) 0),
		OBSERVACAO1((short) 1),
		OBSERVACAO2((short) 2),
		OBSERVACAO3((short) 3),
		OBSERVACAO4((short) 4),
		OBSERVACAO5((short) 5),
		RG_CONJUGE((short) 6),
		DATA_NASCIMENTO_CONJUGE((short) 7),
		EMPRESA_TRABALHO_CONJUGE((short) 8),
		PROFISSAO_CONJUGE((short) 9),
		ENDERECO_TRABALHO_CONJUGE((short) 10),
		TELEFONE_COMERCIAL_CONJUGE((short) 11),
		SALARIO_CONJUGE((short) 12);
		
		private final short index;
		
		FILE3_METADATA(final short index)
		{
			this.index = index;
		}
		
		short getIndex()
		{
			return index;
		}
	}
	
	enum FILE4_METADATA
	{
		CODIGO_CLIENTE((short) 0),
		EMAIL1((short) 1),
		EMAIL2((short) 2),
		EMAIL3((short) 3),
		NATURAL((short) 4),
		ORGAO_EXP_RG((short) 5);
		
		private final short index;
		
		FILE4_METADATA(final short index)
		{
			this.index = index;
		}
		
		short getIndex()
		{
			return index;
		}
	}
	
	enum ENTIDADE_PADRAO
	{
		CIDADE(1475); //bataguassu 
		
		private final int codigo;
		
		ENTIDADE_PADRAO(final int codigo) 
		{
			this.codigo = codigo;
		}
		
		public final int getCodigo()
		{
			return codigo;
		}
	}
	
	public void run(final String diretorioBase)
	{
		EntityManager manager = ConexaoUtil.getFactory("importacao", "importacao!123").createEntityManager();
		
		try 
		{
			manager.getTransaction().begin();
			clean(manager);
			manager.getTransaction().commit();
			
			/*importarClientes(manager, 
					new String[][] { readFile(diretorioBase.concat("CADCLIENTEp1.TXT")), 
					readFile(diretorioBase.concat("CADCLIENTEp2.TXT")),
					readFile(diretorioBase.concat("CADCLIENTEp3.TXT")),
					readFile(diretorioBase.concat("CADCLIENTEp4.TXT")) });*/
			
			correcaoNomePaiMae(manager, 
				new String[][] { readFile(diretorioBase.concat("CADCLIENTEp1.TXT")), 
				readFile(diretorioBase.concat("CADCLIENTEp2.TXT")),
				readFile(diretorioBase.concat("CADCLIENTEp3.TXT")),
				readFile(diretorioBase.concat("CADCLIENTEp4.TXT")) });
		}
		catch(InterruptedException ie)
		{
			ie.printStackTrace();
			manager.getTransaction().rollback();
		}
		catch (IOException e) 
		{
			manager.getTransaction().rollback();
			e.printStackTrace();
		}
		finally
		{
			manager.close();
		}
	}
	
	/**
	 * apaga menos os padrões
	 * @param manager
	 */
	private void clean(final EntityManager manager)
	{
		manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=0;").executeUpdate();
		manager.createNativeQuery("SET SESSION sql_mode='NO_AUTO_VALUE_ON_ZERO';").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE CONJUGE;").executeUpdate();
		manager.createNativeQuery("INSERT INTO CONJUGE (idConjuge, nome) VALUES(0, '**NÃO INFORMADO**');").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE CLIENTE;").executeUpdate();
		manager.createNativeQuery("INSERT INTO CLIENTE (idCliente, nome) VALUES(0, 'CONSUMIDOR');").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE historicoCobranca").executeUpdate();
		manager.createNativeQuery("TRUNCATE TABLE emailcliente;").executeUpdate(); 
		manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=1;").executeUpdate();
	}
	
	private void registraCidadeNaoEncontrada(final Integer idCliente, final String nomeCidade)
	{
		synchronized (cidadeNaoEncontradaWriter) 
		{
			try 
			{
				cidadeNaoEncontradaWriter.write(String.format("Código do Cliente : %d Cidade não encontrada: %s\n", idCliente, nomeCidade));
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	private void salvaArquivoLogCidade()
	{
		synchronized (cidadeNaoEncontradaWriter) 
		{
			try 
			{
				cidadeNaoEncontradaWriter.flush();
				cidadeNaoEncontradaWriter.close();
			}
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	public void correcaoNomePaiMae(final EntityManager manager, final String data[][]) throws InterruptedException
	{	
	    class RunBlock implements Runnable 
	    {
	        int startIndex, endIndex;
	        final String[] data2;
	        
	        RunBlock(EntityManager manager, String[][] data, int startIndex, int endIndex) 
	        { 
	        	this.startIndex = startIndex;
	        	this.endIndex = endIndex;
	        	this.data2 = data[0x01];
	        }
	      
	        public void run() 
	        {
	    		for(int index = startIndex; index < endIndex; index++)
	    		{
	    			final String[] metadata2 = data2[index].split(Util.CACHE_FILE_SEPARATOR, -1);
	    			
	    			String nomePai = metadata2[FILE2_METADATA.PAI.getIndex()].equals("") ? "null" : "'" + metadata2[FILE2_METADATA.PAI.getIndex()] + "'";
	    			String nomeMae = metadata2[FILE2_METADATA.MAE.getIndex()].equals("") ? "null" : "'" + metadata2[FILE2_METADATA.MAE.getIndex()] + "'";
	    			
	    			if(nomePai.equals("null") && nomeMae.equals("null"))
	    				continue;
	    			
	    			System.out.println("UPDATE cliente SET nomePai = " + nomePai + ""
	    					+ ", nomeMae = " + nomeMae + " WHERE idCliente = " + metadata2[FILE2_METADATA.CODIGO_CLIENTE.getIndex()] + ";");	
	    		}
	        }
	    }   
	    
	    List<Thread> runBlockList = new ArrayList<Thread>();
	    
	    Thread firstBlock = new Thread(new RunBlock(null, data, 0x00, (int) (data[0x00].length * 0.01)));
	    firstBlock.setName(String.format("Cliente - Unit Block %d to %d", 1, (int) (data[0x00].length * 0.01)));
	    firstBlock.start();
	    runBlockList.add(firstBlock);
	     
	    for(double threadPart = 0.01; threadPart < 0x01; threadPart += 0.01)
	    {
	    	Thread thread = new Thread(new RunBlock(null, data, (int) (data[0x00].length * threadPart), (int) (data[0x00].length * (threadPart + 0.01))));
	    	thread.setName(String.format("Cliente - Unit Block %d to %d", (int) (data[0x00].length * threadPart),  (int) (data[0x00].length * (threadPart + 0.01))));
	    	thread.start();
	    	runBlockList.add(thread);
	    }
	    
	    for(final Thread unitBlock : runBlockList)
	    	unitBlock.join();
	}
	
	@SuppressWarnings("unchecked")
	public void importarClientes(final EntityManager manager, final String data[][]) throws InterruptedException
	{
		final List<Cidade> listaCidade = manager.createNativeQuery("SELECT * FROM cidade WHERE idUf IN (11,12,13,16,25)", Cidade.class).getResultList();
		final Counter counter = new Counter();
		
	    class RunBlock implements Runnable 
	    {
	        int startIndex, endIndex;
	        Connection connection;
	        final String[] data1;
	        final String[] data2;
	        final String[] data3;
	        final String[] data4;
	        
	        
	        RunBlock(EntityManager manager, String[][] data, int startIndex, int endIndex) 
	        { 
	        	this.connection = ConexaoUtil.getConexaoPadrao("importacao", "importacao!123");
	        	this.startIndex = startIndex;
	        	this.endIndex = endIndex;
	        	this.data1 = data[0x00];
	        	this.data2 = data[0x01];
	        	this.data3 = data[0x02];
	        	this.data4 = data[0x03];
	        }
	        
	        final Cidade getCidade(String nomeCidade) throws Exception
	        {
	        	if(nomeCidade.equals("PRES.EPITACIO") || nomeCidade.contains("EPITÁCIO") || nomeCidade.contains("TACIO"))
	        		nomeCidade = "PRESIDENTE EPITACIO";
	        	else if(nomeCidade.startsWith("BATAGU") || nomeCidade.equals("BATAGASSU") || nomeCidade.equals("BSTAGUASSU") || nomeCidade.equals("BATAGIUASSU") || nomeCidade.equals("BATGUASSU") ||
	        			nomeCidade.equals("BATAGUASU") || nomeCidade.contains("BATAGUASSU") || nomeCidade.equals("BATAGUESSU") || nomeCidade.equals("BATYAGUASSU"))
	        		nomeCidade = "BATAGUASSU";
	        	else if(nomeCidade.equals("PARANAPOLIS"))
	        		nomeCidade = "ANDRADINA";
	        	else if(nomeCidade.equals("PRES.VENCESLAU") || nomeCidade.contains("VENCESLAU") || nomeCidade.equals("CAMPINAL"))
	        		nomeCidade = "PRESIDENTE VENCESLAU";
	        	else if(nomeCidade.equals("STA RITA DO PARDO"))
	        		nomeCidade = "SANTA RITA DO PARDO";
	        	else if(nomeCidade.equals("GUARAÇAI"))
	        		nomeCidade = "GUARACAI";
	        	else if(nomeCidade.equals("MURUTINGA"))
	        		nomeCidade = "MURUTINGA DO SUL";
	        	else if(nomeCidade.equals("PARANAVAI PR"))
	        		nomeCidade = "PARANAVAI";
	        	else if(nomeCidade.contains("PORTO XV") || nomeCidade.equals("DEBRASA") || nomeCidade.equals("POTO XV"))
	        		nomeCidade = "BATAGUASSU";
	        	else if(nomeCidade.equals("ABDRADINA"))
	        		nomeCidade = "ANDRADINA";
	        	else if(nomeCidade.equals("NOVA IDEPEDENCIA") || nomeCidade.equals("NOVA IDEPENDENCIA") || nomeCidade.equals("N.INDEPENDENCIA") || nomeCidade.equals("NOVA INDEPENDECIA") ||
	        			nomeCidade.equals("NOVA INDEP.") || nomeCidade.equals("NOVA IDEPENDECIA"))
	        		nomeCidade = "NOVA INDEPENDENCIA";
	        	else if(nomeCidade.equals("MURUTINGA DO SUL.") || nomeCidade.equals("MURUTINGA DO SIL") || nomeCidade.equals("MUTINGA DO SUL") || nomeCidade.equals("MURITINGA DO SUL"))
	        		nomeCidade = "MURUTINGA DO SUL";
	        	else if(nomeCidade.contains("FABRICIANO"))
	        		nomeCidade = "CORONEL FABRICIANO";
	        	else if(nomeCidade.equals("PERREIRA BARETO") || nomeCidade.startsWith("PERREIRA BARRETO") || nomeCidade.equals("PEREIRA BARRETOS"))
	        		nomeCidade = "PEREIRA BARRETO";
	        	else if(nomeCidade.startsWith("CASTILHO") || nomeCidade.equals("CATILHO"))
	        		nomeCidade = "CASTILHO";
	        	else if(nomeCidade.equals("MIRADA DO SOL"))
	        		nomeCidade = "Mirandopolis";
	        	else if(nomeCidade.equals("SUSANAPOLIS"))
	        		nomeCidade = "Suzanapolis";
	        	else if(nomeCidade.startsWith("SUD ") || nomeCidade.equals("SULDIMENUCI"))
	        		nomeCidade = "SUD MENNUCCI";
	        	else if(nomeCidade.contains("PRUDENTE"))
	        		nomeCidade = "PRESIDENTE PRUDENTE";
	        	else if(nomeCidade.equals("CAUIA") || nomeCidade.equals("MUNICIPIO DE CAIUA"))
	        		nomeCidade = "CAIUA";
	        	else if(nomeCidade.startsWith("TRES L"))
	        		nomeCidade = "TRES LAGOAS";
	        	else if(nomeCidade.startsWith("SANTA RITA"))
	        		nomeCidade = "SANTA RITA DO PARDO";
	        	
	        	synchronized (listaCidade) 
	        	{
	        		for(Cidade cidade : listaCidade)
	        		{
	        			if(cidade.getNome().equalsIgnoreCase(nomeCidade) || cidade.getNomeSemAcento().equalsIgnoreCase(nomeCidade))
	        				return cidade;
	        		}
				}
	        	
	        	throw new Exception(String.format("Não foi possível encontrar a cidade %s", nomeCidade));
	        }
	        
	        void salvaHistoricoCobranca(PreparedStatement ps, Integer idCliente, String historico) throws Exception
	        {
	        	if(historico == null || historico.isEmpty())
	        		return;
	        	
	        	ps.setInt(1, idCliente);
	        	ps.setString(2, historico);
	        	ps.executeUpdate();
	        }
	        
	        void salvaEmailCliente(PreparedStatement ps, Integer idCliente, String email) throws Exception
	        {
	        	if(email == null || email.isEmpty())
	        		return;
	        	
	        	ps.setInt(1, idCliente);
	        	ps.setString(2, email);
	        	ps.executeUpdate();
	        }
	        
	        public void run() 
	        {   			
    			try
    			{
	    			PreparedStatement ps = connection.prepareStatement("INSERT INTO cliente (idCliente, nome, logradouro, cep, idCidadeEndereco, dataHoraCadastro," +
	    					"cpfCnpj, bairro, idConjuge, dataNascimento, telefoneResidencial, salario, estadoCivil, rgInscricaoEstadual, empresaTrabalha, cidadeComercialCache, telefoneComercial, profissao," +
	    					"outrarenda, pai, mae) VALUES (?, ?, ?, ?, ?, ?," +
	    					"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
	    			
	    			PreparedStatement psConjuge = connection.prepareStatement("INSERT INTO conjuge (nome, cpfCnpj, rg, dataNascimento, empresaTrabalha, profissao, logradouroComercial, cidadeComercialCache, salario) VALUES" +
	    					"(?, ?, ?, ?, ?, ?, ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS);
	    			
	    			PreparedStatement psHistorico = connection.prepareStatement("INSERT INTO historicocobranca (idCliente, observacao, idTipoContato, alerta, dataCobranca) VALUES (?, ?, 1, '0', now());");
	    			
	    			PreparedStatement psEmail = connection.prepareStatement("INSERT INTO emailCliente (idCliente, email) VALUES (?, ?);");
	    			
    	    		for(int index = startIndex; index < endIndex; index++)
    	    		{ 
    	    			final String[] metadata1 = data1[index].split(Util.CACHE_FILE_SEPARATOR, -1);
    	    			final String[] metadata2 = data2[index].split(Util.CACHE_FILE_SEPARATOR, -1);
    	    			final String[] metadata3 = data3[index].split(Util.CACHE_FILE_SEPARATOR, -1);
    	    			final String[] metadata4 = data4[index].split(Util.CACHE_FILE_SEPARATOR, -1);
    	    			
    	    			try
    	    			{
        	    			if(!metadata1[FILE1_METADATA.CODIGO_CLIENTE.getIndex()].equals(metadata2[FILE2_METADATA.CODIGO_CLIENTE.getIndex()]) ||
        	    					!metadata2[FILE2_METADATA.CODIGO_CLIENTE.getIndex()].equals(metadata3[FILE3_METADATA.CODIGO_CLIENTE.getIndex()]) ||
        	    					!metadata3[FILE3_METADATA.CODIGO_CLIENTE.getIndex()].equals(metadata4[FILE4_METADATA.CODIGO_CLIENTE.getIndex()]))
        	    				throw new Exception("Arquivos com divergência! Código do Cliente nas linhas não são compatíveis!");
        	    			
    	    				Integer idCliente = Integer.parseInt(metadata1[FILE1_METADATA.CODIGO_CLIENTE.getIndex()]);
        	    			ps.setInt(1, idCliente);
        	    			ps.setString(2, metadata1[FILE1_METADATA.NOME.getIndex()]);
        	    			ps.setString(3, metadata1[FILE1_METADATA.ENDERECO.getIndex()]);
        	    			ps.setString(4, metadata1[FILE1_METADATA.CEP.getIndex()].trim().replace("-", ""));
        	    			
        	    			String cidade = new String(metadata1[FILE1_METADATA.CIDADE.getIndex()].trim().getBytes(Charset.forName("UTF8")));
        	    			if(cidade == null || cidade.isEmpty())
        	    				ps.setNull(5, Types.INTEGER);
        	    			else
        	    			{
        	    				try
        	    				{
        	    					ps.setInt(5, getCidade(cidade).getId());
        	    				}
        	    				catch(Exception ex)
        	    				{
        	    					ps.setInt(5, ENTIDADE_PADRAO.CIDADE.getCodigo());
        	    					registraCidadeNaoEncontrada(idCliente, cidade);
            	    				Logger.getLogger(ImportadorArquivoClientesCache.class).warn(ex.getMessage());	
        	    				}
        	    			}
        	    				 // idCidade
        	    			
        	    			ps.setNull(6, Types.DATE);
        	    			
        	    			String cpf = metadata1[FILE1_METADATA.CPF.getIndex()].trim();
        	    			if(cpf == null || cpf.isEmpty())
        	    				ps.setNull(7, Types.VARCHAR);
        	    			else
        	    				ps.setString(7, cpf);
        	    			
        	    			ps.setString(8, metadata1[FILE1_METADATA.BAIRRO.getIndex()].trim());
        	    			
        	    			
        	    			String dataNascimento = metadata1[FILE1_METADATA.DATA_NASCIMENTO.getIndex()].trim();
        	    			
        	    			synchronized (dateFormat) 
        	    			{
            	    			if(dataNascimento == null || dataNascimento.isEmpty())
            	    				ps.setNull(10, Types.DATE);
            	    			else
            	    				ps.setString(10, sqlDateFormat.format(dateFormat.parse(dataNascimento))); // datanascimento	
							}
        	    			
        	    			ps.setString(11, metadata1[FILE1_METADATA.TELEFONE.getIndex()].trim()); //telefone
        	    			
        	    			String renda = metadata1[FILE1_METADATA.RENDA.getIndex()].trim();
        	    			if(renda == null || renda.isEmpty())
        	    				ps.setNull(12,Types.DOUBLE);
        	    			else
        	    				ps.setDouble(12, Double.parseDouble(metadata1[FILE1_METADATA.RENDA.getIndex()].replace(",", ".").trim()));
        	    			
        	    			ps.setString(13, metadata2[FILE2_METADATA.ESTADO_CIVIL.getIndex()]);
        	    			
        	    			String rg = metadata2[FILE2_METADATA.RG.getIndex()].trim();
        	    			if(rg == null || rg.isEmpty())
        	    				ps.setNull(14, Types.VARCHAR);
        	    			else
        	    				ps.setString(14, rg);
        	    			
        	    			String emprego = metadata2[FILE2_METADATA.EMPREGO.getIndex()].trim();
        	    			if(emprego == null || emprego.isEmpty())
        	    				ps.setNull(15, Types.VARCHAR);
        	    			else
        	    				ps.setString(15, emprego);

        	    			String cidadeEmprego = metadata2[FILE2_METADATA.CIDADE_EMPREGO.getIndex()].trim();
        	    			if(cidadeEmprego == null || cidadeEmprego.isEmpty())
        	    				ps.setNull(16, Types.VARCHAR);
        	    			else
        	    				ps.setString(16, cidadeEmprego);

        	    			String telefoneComercial = metadata2[FILE2_METADATA.TELEFONE_COMERCIAL.getIndex()].trim();
        	    			if(telefoneComercial == null || telefoneComercial.isEmpty())
        	    				ps.setNull(17, Types.VARCHAR);
        	    			else
        	    				ps.setString(17, telefoneComercial);
        	    			
        	    			String profissao = metadata2[FILE2_METADATA.PROFISSAO.getIndex()].trim();
        	    			if(profissao == null || profissao.isEmpty())
        	    				ps.setNull(18, Types.VARCHAR);
        	    			else
        	    				ps.setString(18, profissao);
        	    			
        	    			String outraRenda = metadata2[FILE2_METADATA.OUTRA_RENDA.getIndex()].trim();
        	    			if(outraRenda == null || outraRenda.isEmpty())
        	    				ps.setNull(19, Types.DOUBLE);
        	    			else
        	    				ps.setDouble(19, Double.parseDouble(outraRenda));
        	    			
        	    			ps.setString(20, metadata2[FILE2_METADATA.PAI.getIndex()]);
        	    			ps.setString(21, metadata2[FILE2_METADATA.MAE.getIndex()]);

        	    			if(!metadata1[FILE1_METADATA.CONJUGE.getIndex()].isEmpty())
        	    			{
        	    				psConjuge.setString(1, metadata1[FILE1_METADATA.CONJUGE.getIndex()].trim());
        	    				psConjuge.setNull(2, Types.VARCHAR);
        	    				
        	    				String rgConjuge = metadata3[FILE3_METADATA.RG_CONJUGE.getIndex()];
        	    				if(rgConjuge == null || rgConjuge.isEmpty())
        	    					psConjuge.setNull(3, Types.VARCHAR);
        	    				else
        	    					psConjuge.setString(3, rgConjuge.trim().substring(0, Math.min(8, rgConjuge.trim().length())).replace("-", "").replace("\\.", ""));
        	    				
        	    				
            	    			synchronized (dateFormat) 
            	    			{
            	    				dataNascimento = metadata3[FILE3_METADATA.DATA_NASCIMENTO_CONJUGE.getIndex()].trim();
                	    			if(dataNascimento == null || dataNascimento.isEmpty())
                	    				psConjuge.setNull(4, Types.DATE);
                	    			else
                	    				psConjuge.setString(4, sqlDateFormat.format(dateFormat.parse(dataNascimento))); // datanascimento	
    							}
            	    			
            	    			String trabalhoConjuge = metadata3[FILE3_METADATA.EMPRESA_TRABALHO_CONJUGE.getIndex()].trim();
            	    			if(trabalhoConjuge == null ||trabalhoConjuge.isEmpty())
            	    				psConjuge.setNull(5, Types.VARCHAR);
            	    			else
            	    				psConjuge.setString(5, trabalhoConjuge);
        	    				
            	    			String profissaoConjuge = metadata3[FILE3_METADATA.PROFISSAO_CONJUGE.getIndex()].trim();
            	    			if(profissaoConjuge == null ||profissaoConjuge.isEmpty())
            	    				psConjuge.setNull(6, Types.VARCHAR);
            	    			else
            	    				psConjuge.setString(6, profissaoConjuge);
        	    				
            	    			String enderecoTrabalhoConjuge = metadata3[FILE3_METADATA.ENDERECO_TRABALHO_CONJUGE.getIndex()].trim();
            	    			if(enderecoTrabalhoConjuge == null ||enderecoTrabalhoConjuge.isEmpty())
            	    				psConjuge.setNull(7, Types.DOUBLE);
            	    			else
            	    				psConjuge.setString(7, enderecoTrabalhoConjuge);
        	    				
            	    			psConjuge.setNull(8, Types.DOUBLE);
        	    				
            	    			String salarioConjuge = metadata3[FILE3_METADATA.SALARIO_CONJUGE.getIndex()].trim();
            	    			if(salarioConjuge == null ||salarioConjuge.isEmpty())
            	    				psConjuge.setNull(9, Types.DOUBLE);
            	    			else
            	    				psConjuge.setDouble(9, Double.parseDouble(salarioConjuge));
            	    			
            	    			try
            	    			{
                	    			psConjuge.executeUpdate();	
            	    			}
            	    			catch(Exception ex)
            	    			{
            	    				ex.printStackTrace();
            	    			}
            	    			
            	    			try (ResultSet generatedKeys = psConjuge.getGeneratedKeys()) 
            	    			{
            	    	            if (generatedKeys.next()) 
            	    	            	ps.setInt(9, generatedKeys.getInt(1));
            	    	            else 
            	    	                throw new Exception("Creating PedidoVenda failed, no ID obtained.");
            	    	        }
        	    			}
        	    			else
        	    				ps.setNull(9, Types.INTEGER);
        	    			
        	    			synchronized (counter)
        	    			{
              	    			ps.executeUpdate();
        	    				counter.increment();
    						}
        	    			
           	    			synchronized (psHistorico) 
        	    			{
            	    			salvaHistoricoCobranca(psHistorico, idCliente, metadata3[FILE3_METADATA.OBSERVACAO1.getIndex()].trim());
            	    			salvaHistoricoCobranca(psHistorico, idCliente, metadata3[FILE3_METADATA.OBSERVACAO2.getIndex()].trim());
            	    			salvaHistoricoCobranca(psHistorico, idCliente, metadata3[FILE3_METADATA.OBSERVACAO3.getIndex()].trim());
            	    			salvaHistoricoCobranca(psHistorico, idCliente, metadata3[FILE3_METADATA.OBSERVACAO4.getIndex()].trim());
            	    			salvaHistoricoCobranca(psHistorico, idCliente, metadata3[FILE3_METADATA.OBSERVACAO5.getIndex()].trim());
							}
           	    			synchronized (psEmail) 
           	    			{
           	    				salvaEmailCliente(psEmail, idCliente, metadata4[FILE4_METADATA.EMAIL1.getIndex()].trim());
           	    				salvaEmailCliente(psEmail, idCliente, metadata4[FILE4_METADATA.EMAIL2.getIndex()].trim());
           	    				salvaEmailCliente(psEmail, idCliente, metadata4[FILE4_METADATA.EMAIL3.getIndex()].trim());
							}
    	    			}
    	    			catch(Exception ex)
    	    			{
    	    				Logger.getLogger(ImportadorArquivoClientesCache.this.getClass()).error(String.format("Erro ao processar linha %s. Mensagem: %s", data1[index], ex.getMessage()));
    	    				//throw ex;
    	    			}

    	    		}
    			}
    			catch(Exception ex)
    			{
    				ex.printStackTrace();
    			}
    			finally
    			{
    				try 
    				{
						connection.close();
					} 
    				catch (SQLException e) 
    				{
						e.printStackTrace();
					}
    			}
	        } 
	    }   
	    
	    List<Thread> runBlockList = new ArrayList<Thread>();
	    
	    Thread firstBlock = new Thread(new RunBlock(null, data, 0x00, (int) (data[0x00].length * 0.01)));
	    firstBlock.setName(String.format("Cliente - Unit Block %d to %d", 1, (int) (data[0x00].length * 0.01)));
	    firstBlock.start();
	    runBlockList.add(firstBlock);
	     
	    for(double threadPart = 0.01; threadPart < 0x01; threadPart += 0.01)
	    {
	    	Thread thread = new Thread(new RunBlock(null, data, (int) (data[0x00].length * threadPart), (int) (data[0x00].length * (threadPart + 0.01))));
	    	thread.setName(String.format("Cliente - Unit Block %d to %d", (int) (data[0x00].length * threadPart),  (int) (data[0x00].length * (threadPart + 0.01))));
	    	thread.start();
	    	runBlockList.add(thread);
	    }
	    
	    Thread counterThread  = new Thread(new Runnable() 
	    {
			@Override
			public void run() 
			{
			    while(!Thread.currentThread().isInterrupted())
			    {
			    	try 
			    	{
						Thread.sleep(1500);
						
						synchronized (counter) 
						{
							Logger.getLogger(ImportadorArquivoClientesCache.this.getClass()).info(String.format("%s clientes processadas", counter));
						}
			    	}
			    	catch (InterruptedException e) 
			    	{
			    		Thread.currentThread().interrupt();
					}
			    }
			}
		});
	    counterThread.start();
	    
	    for(final Thread unitBlock : runBlockList)
	    	unitBlock.join();
	    
	    counterThread.interrupt();
	    
	    salvaArquivoLogCidade();
	    
	    Logger.getLogger(ImportadorArquivoClientesCache.this.getClass()).info("Bloco de importação de Clientes finalizado!");
	}
}