package br.com.desenv.nepalign.cache.constantes;

public class Globais 
{
	public static final String COR                 = "CECOR";
	public static final String MARCA               = "CEMAR";
	public static final String MARCA_INDEX         = "CEMARALFA";
	public static final String PRODUTO             = "CECAD";
	public static final String PRODUTO_INDEX       = "CECADI";
	public static final String TAMANHO             = "CETAM";
	public static final String ORDEMPRODUTO        = "CETIPM";
	public static final String GRUPOPRODUTO        = "CEGRU";
	public static final String ESTOQUEPRODUTO_P    = "CECADIDP";
	public static final String ESTOQUEPRODUTO_R    = "CECADIDR";
	public static final String SALDOESTOQUEPRODUTO = "CEPRO";
}