package br.com.desenv.nepalign.cache.constantes;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Util 
{
	public static final Calendar CACHE_DATA_BASE = Calendar.getInstance();
	
	static
	{
		/**
		 * CACHE DATA BASE 12/31/1840
		 */
		CACHE_DATA_BASE.set(Calendar.YEAR, 0x00000730);
		CACHE_DATA_BASE.set(Calendar.MONTH, 0x0000000B);
		CACHE_DATA_BASE.set(Calendar.DAY_OF_MONTH, 0x0000001F);
		
		CACHE_DATA_BASE.set(Calendar.HOUR_OF_DAY, 0x00000000);
		CACHE_DATA_BASE.set(Calendar.MINUTE, 0x0000000);
		CACHE_DATA_BASE.set(Calendar.SECOND, 0x00000000);
		CACHE_DATA_BASE.set(Calendar.MILLISECOND, 0x00000000);
	}
	
	public static final String CACHE_FILE_SEPARATOR = String.valueOf(new char[] { 0x0000005C, 0x0000007C });
	
	public static final String CACHE_SP = String.valueOf(new char[] { 0x0000005E });
	public static final String CACHE_SN = String.valueOf(new char[] { 0x00000016 });
	public static final String CACHE_NP = String.valueOf(new char[] { 0x00000022 });
	
	public static final String CACHE_ACTION_INSERT = "S";
	public static final String CACHE_ACTION_DELETE = "K";
	
	public static long dateToCache(final Date date)
	{
		final Calendar calendario = Calendar.getInstance();
		calendario.setTime(date);
		
		if (calendario.get(Calendar.HOUR_OF_DAY) == 0x00000000)
			calendario.add(Calendar.HOUR_OF_DAY, 0x00000001);
		
		return TimeUnit.DAYS.convert(calendario.getTimeInMillis() - CACHE_DATA_BASE.getTimeInMillis(), TimeUnit.MILLISECONDS);
	}	
	
	public static Date cacheToDate(int days)
	{
		Calendar calendario = (Calendar) CACHE_DATA_BASE.clone();
		calendario.add(Calendar.DAY_OF_YEAR, days);

		return calendario.getTime();
	}	
}
