package br.com.desenv.nepalign.integracao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ConsistenciaMovimento 
{
	public int getQuantidadeMovimentacoes(Connection connection, Integer idEmpresaFisica, Date dataInicial, Date dataFinal, Boolean closeConnection) throws Exception
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		String sqlQuery = "select count(0) from movimentacaoestoque where dataMovimentacao between '" + format.format(dataInicial) + " 00:00:00' and '" + format.format(dataFinal) + " 23:59:59'"; 
		sqlQuery += " and idTipoMovimentacaoEstoque in (2,5,6) and idEmpresaFisica = " + idEmpresaFisica + " group by idEmpresaFisica";
		
		ResultSet rs = connection.createStatement().executeQuery(sqlQuery);
		
		int quantidadeMovimentacao = 0x00;
		
		if(rs.first())
			quantidadeMovimentacao = rs.getInt(0x01); 
		
		if(closeConnection)
			connection.close();
		
		return quantidadeMovimentacao;
	} 
	
	public int getQuantidadePedidoVenda(Connection connection, Integer idEmpresaFisica, Date dataInicial, Date dataFinal, boolean closeConnection) throws Exception
	{
		String databaseName = Util.getDatabaseName(String.format("%02d", idEmpresaFisica));
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		String sqlQueryVenda = "select count(0) from " + databaseName + ".pedidoVenda where dataVenda between '" + format.format(dataInicial) + " 00:00:00' and '" + format.format(dataFinal) + " 23:59:59'"; 
		sqlQueryVenda += " and idTipoMovimentacaoEstoque in (2) and idEmpresaFisica = " + idEmpresaFisica + " and idSituacaoPedidoVenda <> 5 group by idEmpresaFisica";
		
		String sqlQueryTroca = "select count(0) from " + databaseName + ".pedidoVenda where dataVenda between '" + format.format(dataInicial) + " 00:00:00' and '" + format.format(dataFinal) + " 23:59:59'"; 
		sqlQueryTroca += " and idTipoMovimentacaoEstoque in (5) and idEmpresaFisica = " + idEmpresaFisica + " and idSituacaoPedidoVenda <> 5 group by idEmpresaFisica";
		
		int quantidadePedidoVenda = 0x00; 
		int quantidadePedidoTroca = 0x00;
		 
		ResultSet resultSetVenda = connection.createStatement().executeQuery(sqlQueryVenda);
		ResultSet resultSetTroca = connection.createStatement().executeQuery(sqlQueryTroca);
		
		quantidadePedidoVenda = !resultSetVenda.first() ? 0x00 : resultSetVenda.getInt(0x01);
		quantidadePedidoTroca = !resultSetTroca.first() ? 0x00 : resultSetTroca.getInt(0x01) * 0x02;
		
		if(closeConnection)
			connection.close();
		
		return quantidadePedidoVenda + quantidadePedidoTroca;
	}
	
	public HashMap<String, Entry<Integer, Integer>> comparativoValoresMovimento(EmpresaFisica empresaFisica, Date data) throws Exception
	{
		final int quantidadeVendaEsc = getQuantidadePedidoVenda(ConexaoUtil.getConexaoPadrao(), empresaFisica.getId(), data, data, true);
		final int quantidadeMovimentacoesEsc = getQuantidadeMovimentacoes(ConexaoUtil.getConexaoPadrao(), empresaFisica.getId(), data, data, true);
		
		final int quantidadeVendaLoja = -0x01;
		
		try
		{
			getQuantidadePedidoVenda(DriverManager.getConnection(Util.getDatabaseUrl(empresaFisica.getId()), "desenv", "desenv@3377"), empresaFisica.getId(), data, data, true);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		} 
		
		HashMap<String, Entry<Integer, Integer>> valores = new HashMap<String, Entry<Integer, Integer>>();
		valores.put("PEDIDOVENDA", new AbstractMap.SimpleEntry<Integer, Integer>(quantidadeVendaEsc, quantidadeVendaLoja));
		valores.put("MOVIMENTACAOESTOQUE", new AbstractMap.SimpleEntry<Integer, Integer>(quantidadeMovimentacoesEsc, null));
		 
		return valores;
	}
	
	public HashMap<String, Entry<Integer, Integer>> comparativoValoresMovimentoArquivado(EmpresaFisica empresaFisica, Date data) throws Exception
	{
		String query = "";
		query = query + " SELECT  ";
		query = query + "     entidade, quantidadeEscritorio, quantidadeLoja ";
		query = query + " FROM ";
		query = query + "     consistenciaMovimento ";
		query = query + " WHERE ";
		query = query + "     dataMovimento = '" + new SimpleDateFormat("yyyy-MM-dd").format(data) + "'  and idEmpresaFisica = " + empresaFisica.getId().intValue();
		query = query + " GROUP BY idEmpresaFisica , dataMovimento , entidade ";
		query = query + " ORDER BY dataProcessado DESC ";
		
		Connection connection = ConexaoUtil.getConexaoPadrao();
		
		ResultSet rs = connection.createStatement().executeQuery(query);
		
		HashMap<String, Entry<Integer, Integer>> valores = new HashMap<String, Entry<Integer, Integer>>();
		
		while(rs.next())
		{
			valores.put(rs.getString("entidade"), new AbstractMap.SimpleEntry<Integer, Integer>(rs.getInt("quantidadeEscritorio"), (Integer) rs.getObject("quantidadeLoja")));	
		}
		
		connection.close();

		return valores;
	}
	
	public void atualizarValoresConsistenciaMovimento(EmpresaFisica empresaFisica, Date data) throws Exception
	{
		SimpleDateFormat dateFormatSQL = new SimpleDateFormat("yyyy-MM-dd");
		
		try
		{
			for(Entry<String, Entry<Integer, Integer>> entry : comparativoValoresMovimento(empresaFisica, data).entrySet())
			{
				ConexaoUtil.getConexaoPadrao().createStatement().
				executeUpdate("INSERT INTO consistenciaMovimento (entidade, idEmpresaFisica,quantidadeescritorio, quantidadeloja, dataMovimento, dataProcessado) VALUES('" + entry.getKey() + "', " + empresaFisica.getId().intValue() + ", " + entry.getValue().getKey()  + ", " + entry.getValue().getValue()  + ", '" + dateFormatSQL.format(data) + "', NOW());");	
			}	
		}
		catch(Exception ex)
		{
			throw ex;
		}
	} 
	
	public static void main(String[] args) throws Exception
	{	
		final ConsistenciaMovimento consistencia = new ConsistenciaMovimento();
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.DAY_OF_YEAR, Integer.parseInt(args[0x01]));
		
		consistencia.atualizarValoresConsistenciaMovimento(new EmpresaFisicaService().recuperarPorId(Integer.parseInt(args[0x00])), calendar.getTime());
	}
	
	public ConsistenciaMovimento() { }
}
