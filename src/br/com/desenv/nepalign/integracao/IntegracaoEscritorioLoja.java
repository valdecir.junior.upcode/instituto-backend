package br.com.desenv.nepalign.integracao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.util.FtpUtil;
import br.com.desenv.nepalign.util.UtilDatabase;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class IntegracaoEscritorioLoja 
{
	private static Logger log = Logger.getLogger(IntegracaoEscritorioLoja.class);
	
	
	public String gerarDadosOrigem() throws Exception
	{		
		Date instanteFinal;
		Date instanteInicial;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdfArquivo = new SimpleDateFormat("yyyyMMddHHmmss");
		Connection con = null;
		Properties prop;
		String strInstanteFinal = null;
		String strInstanteInicial = null;
		log.info("Iniciando o processo de integração...");
		StringBuilder retorno = new StringBuilder("Iniciando o processo de integração...");
		String strWherePadrao;
		String arquivoTabelasGerais;
		String arquivoTabelasGeraisRar;
		EmpresaFisica empresaFisica;

		try
		{
			List<EmpresaFisica> empresas = new EmpresaFisicaService().listar();
			if (empresas.isEmpty())
			{
				log.error("Nenhuma empresaFisica foi encontrada.");
				retorno.append("\n Nenhuma empresaFisica foi encontrada.");
				throw new Exception("Nenhuma empresaFisica foi encontrada");
			}
			empresaFisica = empresas.get(0);
			
			instanteFinal = new Date();

			prop = buscaPropertiesFTP();
			log.info("Arquivo de propriedades recuperado do servidor.");
			retorno.append("\n Arquivo de propriedades recuperado do servidor.");
			strInstanteInicial = prop.getProperty("INSTANTE_ULTIMAGERACAO");
						
			strInstanteFinal = sdf.format(instanteFinal);
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(instanteFinal);
			//dia atual - 75% do que já passou do mês. EX. Se está no dia 10, ele vai buscar do dia 10-70% de 10 = 7. Ou seja, do dia 03 ao dia 10
			calendar.add(Calendar.DAY_OF_YEAR, - ((int)(calendar.get(Calendar.DAY_OF_MONTH) * 0.75)));
			
			instanteInicial = calendar.getTime();
			strInstanteInicial = sdf.format(instanteInicial);
			
			log.info("Instante Inicial para recuperação das informações. " + strInstanteInicial);
			retorno.append("\n Instante Final para recuperação das informações.");
			log.info("Instante Final para recuperação das informações. " + strInstanteFinal);
			retorno.append("Instante Final para recuperação das informações.");
			
			strWherePadrao = " ultimoEvento >= '" + strInstanteInicial + "' AND ultimoEvento <= '" + strInstanteFinal + "'";
			
			log.info("Recupera conexão padrão.");
			retorno.append("\n Recupera conexão padrão.");
			con = ConexaoUtil.getConexaoPadrao();

			Statement stm = con.createStatement();
			try
			{
				log.info("Gera tabela de sincronismo para entidade Cor");
				retorno.append("\n Gera tabela de sincronismo para entidade Cor");
				stm.execute("DROP TABLE IF EXISTS sinc_Cor;");
				stm.execute("CREATE TABLE sinc_Cor SELECT * FROM COR ");
				
				log.info("Gera tabela de sincronismo para entidade Marca");
				retorno.append("\n Gera tabela de sincronismo para entidade Marca");
				stm.execute("DROP TABLE IF EXISTS sinc_Marca;");
				stm.execute("CREATE TABLE sinc_Marca SELECT * FROM Marca ");
				
				log.info("Gera tabela de sincronismo para entidade Tamanho");
				retorno.append("\n Gera tabela de sincronismo para entidade Tamanho");
				stm.execute("DROP TABLE IF EXISTS sinc_Tamanho;");
				stm.execute("CREATE TABLE sinc_Tamanho SELECT * FROM Tamanho");
				
				log.info("Gera tabela de sincronismo para entidade OrdemProduto");
				retorno.append("\n Gera tabela de sincronismo para entidade OrdemProduto");
				stm.execute("DROP TABLE IF EXISTS sinc_OrdemProduto;");
				stm.execute("CREATE TABLE sinc_OrdemProduto SELECT * FROM OrdemProduto ");
				
				log.info("Gera tabela de sincronismo para entidade GrupoProduto");
				retorno.append("\n Gera tabela de sincronismo para entidade GrupoProduto");
				stm.execute("DROP TABLE IF EXISTS sinc_GrupoProduto;");
				stm.execute("CREATE TABLE sinc_GrupoProduto SELECT * FROM GrupoProduto ");
				
				log.info("Gera tabela de sincronismo para entidade Fornecedor");
				retorno.append("\n Gera tabela de sincronismo para entidade Fornecedor");
				stm.execute("DROP TABLE IF EXISTS sinc_Fornecedor;");
				stm.execute("CREATE TABLE sinc_Fornecedor SELECT * FROM Fornecedor ");
				
				log.info("Gera tabela de sincronismo para entidade Produto");
				retorno.append("\n Gera tabela de sincronismo para entidade Produto");
				stm.execute("DROP TABLE IF EXISTS sinc_Produto ;");
				stm.execute("CREATE TABLE sinc_Produto  SELECT * FROM Produto WHERE idProduto in (SELECT idProduto FROM EstoqueProduto WHERE idEstoqueProduto in (SELECT idEstoqueProduto FROM SaldoEstoqueProduto WHERE mes = " + empresaFisica.getMesAberto().toString() 
						+ " and ano = "+ empresaFisica.getAnoAberto() + " AND " + strWherePadrao + "))");
				
				log.info("Gera tabela de sincronismo para entidade EstoqueProduto");
				retorno.append("\n Gera tabela de sincronismo para entidade EstoqueProduto");
				stm.execute("DROP TABLE IF EXISTS sinc_EstoqueProduto;");
				stm.execute("CREATE TABLE sinc_EstoqueProduto SELECT * FROM EstoqueProduto WHERE idEstoqueProduto in (SELECT idEstoqueProduto FROM SaldoEstoqueProduto WHERE mes = " + empresaFisica.getMesAberto().toString() 
						+ " and ano = "+ empresaFisica.getAnoAberto() + " AND " + strWherePadrao + ")");
				
				log.info("Gera tabela de sincronismo para entidade SaldoEstoqueProduto");
				retorno.append("\n Gera tabela de sincronismo para entidade SaldoEstoqueProduto");
				stm.execute("DROP TABLE IF EXISTS sinc_SaldoEstoqueProduto;");
				stm.execute("CREATE TABLE sinc_SaldoEstoqueProduto SELECT * FROM SaldoEstoqueProduto WHERE mes = " + empresaFisica.getMesAberto().toString() 
						+ " and ano = "+ empresaFisica.getAnoAberto() + " AND " + strWherePadrao);
				
			}
			catch(Exception exPrep)
			{
				log.error("Erro na execuçãoo de transferÊncia de dados para tabelas temporárias auxiliares..", exPrep);
				retorno.append("\n Erro na execução de transferência de dados para tabelas temporárias auxiliares..");
				throw exPrep;
			}
			
			try
			{
				String tables = "sinc_cor sinc_Marca sinc_Tamanho sinc_OrdemProduto sinc_GrupoProduto sinc_Fornecedor " +
						"sinc_produto sinc_EstoqueProduto sinc_SaldoEstoqueProduto ";
				log.info("Efetua DUMP de Tabelas auxiliares " + tables);
				retorno.append("\n Efetua DUMP de Tabelas auxiliares " + tables);
				arquivoTabelasGerais = "tabelasAuxilaresEstoque_" + sdfArquivo.format(instanteFinal)+".sql";
				arquivoTabelasGeraisRar = "tabelasAuxilaresEstoque_" + sdfArquivo.format(instanteFinal)+".rar";
				
				String baseUrl = String.valueOf(ConexaoUtil.getFactory().getProperties().get("javax.persistence.jdbc.url"));
				baseUrl = baseUrl.split("//")[0x00000001].split("/")[0x00000001];
				
				new UtilDatabase().generateBackupFileEscritorioLoja(baseUrl, tables, DsvConstante.getParametrosSistema().get("INTEGRACAO_BARRAS_CAMINHO_LOCAL")+arquivoTabelasGerais);
			}
			catch(Exception exDump)
			{
				log.error("Erro na execução de DUMP de tabelas auxiliares de sincronização.", exDump);
				retorno.append("\n Erro na execução de DUMP de tabelas auxiliares de sincronização.");
				throw exDump;				
			}
			try
			{
				log.info("Efetua o FTP do arquivo.");
				retorno.append("\n Efetua o FTP do arquivo.");
				IgnUtil.compactarArquivo(DsvConstante.getParametrosSistema().get("INTEGRACAO_BARRAS_CAMINHO_LOCAL")+arquivoTabelasGerais,DsvConstante.getParametrosSistema().get("INTEGRACAO_BARRAS_CAMINHO_LOCAL")+arquivoTabelasGeraisRar);
				new FtpUtil().uploadFile(DsvConstante.getParametrosSistema().get("INTEGRACAO_BARRAS_CAMINHO_LOCAL")+arquivoTabelasGerais.replace("sql","rar"),DsvConstante.getParametrosSistema().get("INTEGRACAO_BARRAS_CAMINHO_FTP")+arquivoTabelasGeraisRar ,  DsvConstante.getParametrosSistema().get("INTEGRACAO_IP_FTP"),  DsvConstante.getParametrosSistema().get("INTEGRACAO_USUARIO_FTP"),  DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_FTP"));				
			}
			catch(Exception exFTP)
			{
				log.error("Erro na execução do FTP das tabelas auxiliares de sincronização.", exFTP);
				retorno.append("\n Erro na execução do FTP das tabelas auxiliares de sincronização.");
				throw exFTP;				
			}
			try
			{
				prop.put("INSTANTE_ULTIMAGERACAO", strInstanteFinal);
				prop.put("NOME_ARQUIVO_ESTOQUE", arquivoTabelasGeraisRar);
				salvarArquivoProperties(prop);
			}
			catch(Exception exProp)
			{
				log.error("Erro ao Salvar arquivo de propriedades.", exProp);
				retorno.append("\n Erro ao Salvar arquivo de propriedades.");
				throw exProp;
			}
			
			log.info("Arquivo de atualização de estoque foi salvo e enviado. " + arquivoTabelasGerais + " " + strInstanteInicial + " " + strInstanteFinal);
			retorno.append("\n Arquivo de atualização de estoque foi salvo e enviado. " + arquivoTabelasGerais + " " + strInstanteInicial + " " + strInstanteFinal);
			return retorno.toString();
		}
		catch(Exception ex)
		{
			log.error("Não foi possível sincronizar", ex);
			throw ex;
		}
	}
	
	public String atualizarDadosDestino() throws Exception
	{
		Connection con = null;
		Properties prop;
		log.info("Iniciando o processo de integraçãoo...");
		StringBuffer retorno = new StringBuffer("Iniciando o processo de integraçãoo...") ;		
		String arquivoTabelasGerais;
		String strDataGeracaoArquivo;
		String strNomeArquivoExtraido;
		int idEmpresaFisica;
		
		try
		{
			con = ConexaoUtil.getConexaoPadrao();
			
			idEmpresaFisica = DsvConstante.getParametrosSistema().getInt("EMPRESA_LOCAL");
			String empresaFisica = idEmpresaFisica < 10 ? "0" + idEmpresaFisica+"" : idEmpresaFisica+"";
			
			prop = buscaPropertiesFTP();
			log.info("Arquivo de propriedades recuperado do servidor.");			
			log.info("Recupera nome do arquivo para baixar.");
			retorno.append("\n Arquivo de propriedades recuperado do servidor.");
			retorno.append("\n Recupera nome do arquivo para baixar.");
			arquivoTabelasGerais = prop.getProperty("NOME_ARQUIVO_ESTOQUE");
			strDataGeracaoArquivo = prop.getProperty("INSTANTE_ULTIMAGERACAO");
			strNomeArquivoExtraido = arquivoTabelasGerais.replace("rar", "sql");
			retorno.append("\n Arquivo: " + arquivoTabelasGerais + " Gerado em: " + strDataGeracaoArquivo);
			//descompacta arquivo
			log.info("Efetua download do arquivo " + arquivoTabelasGerais);
			retorno.append("\n Efetua download do arquivo " + arquivoTabelasGerais);
			new FtpUtil().downloadFileAlternativo(DsvConstante.getParametrosSistema().get("INTEGRACAO_BARRAS_CAMINHO_FTP")+arquivoTabelasGerais, DsvConstante.getParametrosSistema().get("INTEGRACAO_BARRAS_CAMINHO_LOCAL") + arquivoTabelasGerais, DsvConstante.getParametrosSistema().get("INTEGRACAO_IP_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_USUARIO_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_FTP"));			
			log.info("Extraindo.");	
			retorno.append("\n Extraindo.");
			IgnUtil.descompactarArquivo(DsvConstante.getParametrosSistema().get("INTEGRACAO_BARRAS_CAMINHO_LOCAL") + arquivoTabelasGerais,DsvConstante.getParametrosSistema().get("INTEGRACAO_BARRAS_CAMINHO_LOCAL"));	
			log.info("Restaurando arquivos temporários");
			retorno.append("\n Restaurando arquivos temporários");
			new UtilDatabase().restoreBackupFile(DsvConstante.getParametrosSistema().get("INTEGRACAO_BARRAS_CAMINHO_LOCAL") + strNomeArquivoExtraido, "nepalignloja" + empresaFisica);
			log.info("Restauração completada!");
			retorno.append("\n Restauração completada! ");
			log.info("Iniciando processo de restauração das tabelas.");	
			retorno.append("\n Iniciando processo de restauração das tabelas.");
			CallableStatement proc = con.prepareCall("{ call nepalignloja" + empresaFisica +  ".SincronizacaoDadosEstoqueEscLoja()}");

			if (!proc.execute())
			{
				log.info("Restauração terminada...");
				retorno.append("\n Restauração terminada...");
				proc.close();
				con.close();				
				return retorno.toString();
			}
			else
			{
				log.info("Ocorreu um erro na restauração das tabelas...");
				retorno.append("\n Ocorreu um erro na restauração das tabelas...");
				return retorno.toString();
			}
		}
		catch(Exception ex)
		{
			log.error("Não foi possível sincronizar", ex);
			throw ex;
		}
	}
	
	public Properties buscaPropertiesFTP() throws Exception
	{
		new FtpUtil().downloadFile(DsvConstante.getParametrosSistema().get("INTEGRACAO_BARRAS_CAMINHO_PROPERTIES_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_BARRAS_CAMINHO_PROPERTIES_LOCAL"), DsvConstante.getParametrosSistema().get("INTEGRACAO_IP_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_USUARIO_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_FTP"));
		Properties properties = new Properties();
		properties.load(new FileInputStream(new File(DsvConstante.getParametrosSistema().get("INTEGRACAO_BARRAS_CAMINHO_PROPERTIES_LOCAL"))));
		return properties;
	}
	
	public void salvarArquivoProperties(Properties properties) throws Exception
	{
		properties.store(new FileOutputStream(DsvConstante.getParametrosSistema().get("INTEGRACAO_BARRAS_CAMINHO_PROPERTIES_LOCAL")), new Date().toString());
		new FtpUtil().uploadFile(DsvConstante.getParametrosSistema().get("INTEGRACAO_BARRAS_CAMINHO_PROPERTIES_LOCAL"), DsvConstante.getParametrosSistema().get("INTEGRACAO_BARRAS_CAMINHO_PROPERTIES_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_IP_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_USUARIO_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_FTP"));
	}
}