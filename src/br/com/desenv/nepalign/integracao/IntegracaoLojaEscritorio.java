package br.com.desenv.nepalign.integracao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.util.FtpUtil;
import br.com.desenv.nepalign.util.MailUtil;
import br.com.desenv.nepalign.util.UtilDatabase;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class IntegracaoLojaEscritorio 
{ 
	public IntegracaoLojaEscritorio()
	{

	}
	
	public static void main(String[] args)  throws Exception
	{
		DsvConstante.getParametrosSistema();
		ConexaoUtil.getConexaoPadrao();
		
		if(args[0].equals("1"))
			new IntegracaoLojaEscritorio().integrarEscritorio(Integer.parseInt(args[1]), Integer.parseInt(args[2]));	
		else
			new IntegracaoLojaEscritorio().integrarLoja(Integer.parseInt(args[1]));
	}
	
	public void integrarLoja(Integer sequencial) throws Exception
	{	
		Exception processException = null;
		boolean processOk = false;
		
    	Date dataInicioProcesso = new Date();	
    	String numeroEmpresaFisica = String.format ("%02d", DsvConstante.getParametrosSistema().getInt("EMPRESA_LOCAL"));
		
		try
		{
			Logger.getGlobal().info("Initializing sync to F" + numeroEmpresaFisica);
			
			String nomeArquivoDump = "dump_TG_" + numeroEmpresaFisica + "_" + new SimpleDateFormat("yyyyMMdd").format(new Date()) + "_" + sequencial.toString() + ".NEPALDUMP";
			String diretorioArquivoDump = DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump;
			
			Logger.getGlobal().info("Initializing Dump [" + nomeArquivoDump + "] in " + diretorioArquivoDump);
			
			String tables = "avalista boletobancario caixadia cliente chequeRecebido comissaocobrador condicional conjuge duplicata duplicataacordo duplicataparcela emailcliente itemcondicional itemnotaauxnfe itemnotacofinsnfe itemnotafiscal itemnotaicmsnfe itemnotaiinfe itemnotapisnfe itempedidovenda " +
					"movimentocaixadia movimentoparceladuplicata notafiscal notafiscalcomplnfe notafiscallancamentocontareceber operacaorecebimentoduplicata pagamentopedidovenda parcelapagamentocartao pedidovenda valorfechamentocaixadia vendedor ";
			
			Logger.getGlobal().info("Tables to Dump " + tables);

			new UtilDatabase().generateBackupFile("nepalignloja" + numeroEmpresaFisica, tables, diretorioArquivoDump);
			
			Logger.getGlobal().info("Dump created. Compressing.");
			
			IgnUtil.compactarArquivo(diretorioArquivoDump, diretorioArquivoDump.replace("NEPALDUMP", "rar"));
			
			Logger.getGlobal().info("Compressed. Uploading");
			 
			new FtpUtil().uploadFile(diretorioArquivoDump.replace("NEPALDUMP", "rar"), nomeArquivoDump.replace("NEPALDUMP", "rar"), DsvConstante.getParametrosSistema().get("INTEGRACAO_IP_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_USUARIO_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_FTP"));
			
			Logger.getGlobal().info("Process complete.");
			
			processOk = true;
		}
		catch(Exception ex)
		{
			processOk = false;
			processException = ex;
			
			ex.printStackTrace();
		}
		finally
		{
    		Date dataFimProcesso = new Date();
    		Date dataTerminoProcesso = new Date();
    		dataTerminoProcesso.setTime(dataFimProcesso.getTime() - dataInicioProcesso.getTime());
    		
			if(processOk)
			{
        		new MailUtil().sendMail(DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_ORIGEM_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_EMAIL_ORIGEM_NOTIFICACOES"), "Sistema Integrador Nepal", DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_DESTINO_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("SMPT_EMAIL_ORIGEM_NOTIFICACOES"), "integração Nepal. Envio TG's da Loja " + numeroEmpresaFisica + " para FTP", 
        				"Olá, a integração de envio das TG's da Loja " + numeroEmpresaFisica + " para o FTP foi concluída.<br><br>Iniciado em " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataInicioProcesso) + " e finalizado em " + 
        				new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataFimProcesso) + " o processo durou " + new SimpleDateFormat("mm:ss").format(dataTerminoProcesso) + " (minutos:segundos).");	
			}
			else
			{
				new MailUtil().sendMail(DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_ORIGEM_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_EMAIL_ORIGEM_NOTIFICACOES"), "Sistema Integrador Nepal", DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_DESTINO_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("SMPT_EMAIL_ORIGEM_NOTIFICACOES"), "integração Nepal. Envio TG's da Loja " + numeroEmpresaFisica + " para FTP FALHOU!!!", 
        				"Olá, a integração de envio das TG's da Loja " + numeroEmpresaFisica + " para o FTP não foi concluída.<br><br>Iniciado em " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataInicioProcesso) + " e finalizado em " + 
        				new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataFimProcesso) + " o processo durou " + new SimpleDateFormat("mm:ss").format(dataTerminoProcesso) + " (minutos:segundos) antes de ser abortado!!!!" + 
        				"<br><br>ERRO : " + processException.toString());
				
				System.exit(-1);
			}	
		}
	}
	
	public void integrarEscritorio(Integer idEmpresaFisica, Integer sequencial) throws Exception
	{
		Exception processException = null;
		boolean processOk = false;
		
    	Date dataInicioProcesso = new Date();	
    	String numeroEmpresaFisica = String.format ("%02d", idEmpresaFisica);
		
		try
		{ 
			Logger.getGlobal().info("Initializing Sync to F" + numeroEmpresaFisica);
			
			String nomeArquivoDump = "dump_TG_" + numeroEmpresaFisica + "_" + new SimpleDateFormat("yyyyMMdd").format(new Date()) + "_" + sequencial.toString() + ".NEPALDUMP";
			
			Logger.getGlobal().info("Initializing download of " + nomeArquivoDump);
			
			new FtpUtil().downloadFile(nomeArquivoDump.replace("NEPALDUMP", "rar"), DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump.replace("NEPALDUMP", "rar"), DsvConstante.getParametrosSistema().get("INTEGRACAO_IP_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_USUARIO_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_FTP"));
			
			Logger.getGlobal().info("Downloaded. Extracting."); 
			
			IgnUtil.descompactarArquivo(DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump.replace("NEPALDUMP", "rar"), DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump.split("\\.")[0]);
			
			Logger.getGlobal().info("Extract complete. Initializing Restore");
			
			new UtilDatabase().restoreBackupFile(DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump.split("\\.")[0] + "/" + nomeArquivoDump, "nepalignloja" + numeroEmpresaFisica);
			
			Logger.getGlobal().info("Restore complete. Finish!");	
			
			processOk = true;
		}
		catch(Exception ex)
		{
			processOk = false;
			processException = ex;
			
			ex.printStackTrace();
		}
		finally
		{
    		Date dataFimProcesso = new Date();
    		Date dataTerminoProcesso = new Date();
    		dataTerminoProcesso.setTime(dataFimProcesso.getTime() - dataInicioProcesso.getTime());
    		
			if(processOk)
			{
				new MailUtil().sendMail(DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_ORIGEM_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_EMAIL_ORIGEM_NOTIFICACOES"), "Sistema Integrador Nepal", DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_DESTINO_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("SMPT_EMAIL_ORIGEM_NOTIFICACOES"), "integração Nepal. Baixa das TG's da Loja " + numeroEmpresaFisica + " no Escritório", 
        				"Olá, a integração de baixa das TG's da Loja " + numeroEmpresaFisica + " no Escritório foi concluída.<br><br>Iniciado em " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataInicioProcesso) + " e finalizado em " + 
        				new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataFimProcesso) + " o processo durou " + new SimpleDateFormat("mm:ss").format(dataTerminoProcesso) + " (minutos:segundos).");	
			}
			else
			{
				new MailUtil().sendMail(DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_ORIGEM_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_EMAIL_ORIGEM_NOTIFICACOES"), "Sistema Integrador Nepal", DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_DESTINO_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("SMPT_EMAIL_ORIGEM_NOTIFICACOES"), "integração Nepal. Baixa das TG's da Loja " + numeroEmpresaFisica + " no Escritório FALHOU!!!", 
        				"Olá, a integração de baixa das TG's da Loja " + numeroEmpresaFisica + " no Escritório não foi concluída.<br><br>Iniciado em " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataInicioProcesso) + " e finalizado em " + 
        				new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataFimProcesso) + " o processo durou " + new SimpleDateFormat("mm:ss").format(dataTerminoProcesso) + " (minutos:segundos) antes de ser abortado!!!!" + 
        				"<br><br>ERRO : " + processException.toString());
				
				System.exit(-1);
			}
		}
	}
}