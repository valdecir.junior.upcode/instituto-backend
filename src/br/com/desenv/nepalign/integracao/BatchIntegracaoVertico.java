package br.com.desenv.nepalign.integracao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.desenv.nepalign.alertas.Loader;
import br.com.desenv.nepalign.service.AlertaPedidoVendaService;
import br.com.desenv.nepalign.util.DsvConstante;

public class BatchIntegracaoVertico extends Thread
{
	private IntegracaoPedido integracaoPedido;
	private AlertaPedidoVendaService alertaPedidoVendaService;
	
	public void run()
	{
		try
		{
			new Loader();
			this.integracaoPedido = new IntegracaoPedido();
			this.alertaPedidoVendaService = new AlertaPedidoVendaService();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
			Logger.getGlobal().log(Level.INFO, "Thread iniciada. Horário atual : " + simpleDateFormat.format(new Date()));
			do
			{
				this.integracaoPedido.integrarPedidos();
				Thread.sleep(DsvConstante.getInstanceParametrosSistema().getSleepTimeThreadVerificarAlertas());
				this.alertaPedidoVendaService.verificarAlteracoesAlertaPedidoVenda();
				Thread.sleep(DsvConstante.getInstanceParametrosSistema().getSleepTimeThreadIntegrarPedidos());
			}
			while(true);
		}
		catch(Exception ex)
		{
			Logger.getGlobal().log(Level.INFO, "Erro na integração Nepal/Vertis");
			Logger.getGlobal().log(Level.SEVERE, "Detalhes : " + ex.getMessage() + " \n\nData/Hora : " + new SimpleDateFormat("dd/MM/yyy hh:mm:ss").format(new Date()));
			ex.printStackTrace();
		}
	}
	
	public static void main(String[] args)
	{
		
		Thread checkThread = (new BatchIntegracaoVertico());
		checkThread.start();
	}
}
