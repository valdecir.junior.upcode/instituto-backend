package br.com.desenv.nepalign.integracao;

import br.com.desenv.nepalign.model.AtributoVisualizacaoLv;
import br.com.desenv.nepalign.model.AtributoVisualizacaoLvProduto;
import br.com.desenv.nepalign.service.AtributoVisualizacaoLvProdutoService;
import br.com.desenv.nepalign.service.AtributoVisualizacaoLvService;

import com.desenv.nepal.catalog.wsdl.Catalog;
import com.desenv.nepal.catalog.wsdl.CatalogSoap;
import com.desenv.nepal.catalog.xmlData.DisplayAttributeAddUpdate;
import com.desenv.nepal.catalog.xmlData.DisplayAttributeRemove;
import com.desenv.nepal.catalog.xmlData.ProductDisplayAttributeAdd;
import com.desenv.nepal.catalog.xmlData.ProductDisplayAttributeRemove;
import com.desenv.nepal.catalog.xmlData.ReturnData;
import com.desenv.nepal.integracao.util.DesenvXMLUtil;

public class IntegracaoAtributoVisualizacao 
{
	private Catalog catalog;
	private CatalogSoap catalogSoap;
	
	private String user;
	private String password;
	
	public IntegracaoAtributoVisualizacao(String user, String password)
	{
		this.catalog = new Catalog();
		this.catalogSoap = this.catalog.getCatalogSoap();
		
		this.user = user;
		this.password = password;
	}
	
	public void incluirAtributoVisualizacao(AtributoVisualizacaoLv atributoVisualizacaoLv) throws Exception
	{
		try
		{
			DisplayAttributeAddUpdate displayAttributeAdd = new DisplayAttributeAddUpdate();
			displayAttributeAdd.setDsImage(atributoVisualizacaoLv.getImagemListaProduto());
			displayAttributeAdd.setDsImageProduct(atributoVisualizacaoLv.getImagemDetalheProduto());
			displayAttributeAdd.setDsName(atributoVisualizacaoLv.getNome());
			displayAttributeAdd.setDsUrl(atributoVisualizacaoLv.getUrl());
			displayAttributeAdd.setIdDisplayAttribute(atributoVisualizacaoLv.getId());
			displayAttributeAdd.setNuOrder(atributoVisualizacaoLv.getOrdemExibicao());
			
			String xmlToSend = new DesenvXMLUtil().objectToXML(displayAttributeAdd, DisplayAttributeAddUpdate.class);
			String receivedXml = this.catalogSoap.displayAttributeAddUpdate(xmlToSend, user, password);
			
			ReturnData returnData = new DesenvXMLUtil().XMLToObjectReturnData(receivedXml);
			
			if(returnData.getIdRet() == 0)
				throw new Exception(returnData.getDsErr());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}
	
	public void excluirAtributoVisualizacao(AtributoVisualizacaoLv atributoVisualizacaoLv) throws Exception
	{
		try
		{
			DisplayAttributeRemove displayAttributeRemove = new DisplayAttributeRemove();
			displayAttributeRemove.setIdDisplayAttribute(atributoVisualizacaoLv.getId());
			
			String xmlToSend = new DesenvXMLUtil().objectToXML(displayAttributeRemove, DisplayAttributeRemove.class);
			String receivedXml = this.catalogSoap.displayAttributeRemove(xmlToSend, user, password);
			
			ReturnData returnData = new DesenvXMLUtil().XMLToObjectReturnData(receivedXml);
			
			if(returnData.getIdErr() == 0)
				throw new Exception(returnData.getDsErr());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}
	
	public void incluirAtributoVisualizacaoProduto(AtributoVisualizacaoLvProduto atributoVisualizacaoLvProduto) throws Exception
	{
		try
		{
			ProductDisplayAttributeAdd productDisplayAttributeAdd = new ProductDisplayAttributeAdd();
			productDisplayAttributeAdd.setIdDisplayAttribute(atributoVisualizacaoLvProduto.getAtributoVisualizacaoLv().getId());
			/*productDisplayAttributeAdd.setIdProduct(atributoVisualizacaoLvProduto.getEstoqueProduto().getProduto().getCodigoProduto() + "_" + 
					atributoVisualizacaoLvProduto.getEstoqueProduto().getOrdemProduto().getCodigo().toString() + "_" +
					atributoVisualizacaoLvProduto.getEstoqueProduto().getCor().getCodigo());*/
			productDisplayAttributeAdd.setIdProduct("53032_296_96");
			
			String xmlToSend = new DesenvXMLUtil().objectToXML(productDisplayAttributeAdd, ProductDisplayAttributeAdd.class);
			String receivedXml = this.catalogSoap.productDisplayAttributeAdd(xmlToSend, user, password);
			
			ReturnData returnData = new DesenvXMLUtil().XMLToObjectReturnData(receivedXml);
			
			if(returnData.getIdErr() == 0)
				throw new Exception(returnData.getDsErr());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}
	
	public static void main(String[] args) throws Exception
	{
		new IntegracaoAtributoVisualizacao("integracao", "woo5#7").incluirAtributoVisualizacao(new AtributoVisualizacaoLvService().recuperarPorId(5));
		new IntegracaoAtributoVisualizacao("integracao", "woo5#7").incluirAtributoVisualizacaoProduto(new AtributoVisualizacaoLvProdutoService().recuperarPorId(3));
		//new IntegracaoCliente().integrarClientesPorId(2808);
	}
	
	public void excluirAtributoVisualizacaoProduto(AtributoVisualizacaoLvProduto atributoVisualizacaoLvProduto) throws Exception
	{
		try
		{
			ProductDisplayAttributeRemove productDisplayAttributeRemove = new ProductDisplayAttributeRemove();
			productDisplayAttributeRemove.setIdDisplayAttribute(atributoVisualizacaoLvProduto.getAtributoVisualizacaoLv().getId());
			/*productDisplayAttributeRemove.setIdProduct(atributoVisualizacaoLvProduto.getEstoqueProduto().getProduto().getCodigoProduto() + "_" + 
					atributoVisualizacaoLvProduto.getEstoqueProduto().getOrdemProduto().getCodigo().toString() + "_" +
					atributoVisualizacaoLvProduto.getEstoqueProduto().getCor().getCodigo());*/
			productDisplayAttributeRemove.setIdProduct("44802_1617_226");
			
			String xmlToSend = new DesenvXMLUtil().objectToXML(productDisplayAttributeRemove, ProductDisplayAttributeRemove.class);
			String receivedXml = this.catalogSoap.productDisplayAttributeAdd(xmlToSend, user, password);
			
			ReturnData returnData = new DesenvXMLUtil().XMLToObjectReturnData(receivedXml);
			
			if(returnData.getIdErr() == 0)
				throw new Exception(returnData.getDsErr());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}
}
