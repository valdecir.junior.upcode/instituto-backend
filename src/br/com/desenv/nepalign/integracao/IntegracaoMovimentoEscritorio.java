package br.com.desenv.nepalign.integracao;

import java.io.File;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.security.sasl.AuthenticationException;

import org.hibernate.proxy.pojo.javassist.JavassistLazyInitializer;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

import flex.messaging.io.amf.client.AMFConnection;
import flex.messaging.io.amf.client.exceptions.ClientStatusException;
import flex.messaging.io.amf.client.exceptions.ServerStatusException;

import br.com.desenv.frameworkignorante.DsvException;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.frameworkignorante.ListaEmpresaEntidadeChave;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.ItemMovimentacaoEstoque;
import br.com.desenv.nepalign.model.MovimentacaoEstoque;
import br.com.desenv.nepalign.model.TipoMovimentacaoEstoque;
import br.com.desenv.nepalign.persistence.ItemMovimentacaoEstoquePersistence;
import br.com.desenv.nepalign.persistence.MovimentacaoEstoquePersistence;
import br.com.desenv.nepalign.service.ClienteService;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.EstoqueProdutoService;
import br.com.desenv.nepalign.service.EstoqueService;
import br.com.desenv.nepalign.service.FornecedorService;
import br.com.desenv.nepalign.service.ItemMovimentacaoEstoqueService;
import br.com.desenv.nepalign.service.MovimentacaoEstoqueService;
import br.com.desenv.nepalign.service.ProdutoService;
import br.com.desenv.nepalign.service.StatusMovimentacaoEstoqueService;
import br.com.desenv.nepalign.service.TipoDocumentoMovimentoEstoqueService;
import br.com.desenv.nepalign.service.TipoMovimentacaoEstoqueService;
import br.com.desenv.nepalign.service.TmpMovimentacaoEstoqueService;
import br.com.desenv.nepalign.service.UnidadeMedidaService;
import br.com.desenv.nepalign.service.UsuarioService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.util.FtpUtil;
import br.com.desenv.nepalign.util.MailUtil;
import br.com.desenv.nepalign.util.UtilDatabase;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class IntegracaoMovimentoEscritorio 
{
	public static PrintWriter LogFileWriter;
	
	public static void main(String[] args) throws Exception 
	{
		ConexaoUtil.getConexaoPadrao();
		
		IntegracaoMovimentoEscritorio integracaoEscritorio = new IntegracaoMovimentoEscritorio();
		
		if(args[0x00].equals("MV_NV"))
		{
			Integer idEmpresaFisica = Integer.parseInt(args[0x01]);
			
			Date dataInicial = null;
			
			if(args[0x02].equals("0"))
				dataInicial = new Date();
			else if(args[0x02].startsWith("-"))
			{
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, Integer.parseInt(args[0x02]));
				
				dataInicial = calendar.getTime();
			}
			else 
				dataInicial = new SimpleDateFormat("dd/MM/yyyy").parse(args[0x02]);
			
			Date dataFinal = null;
			
			if(args[0x03].equals("0"))
				dataFinal = new Date();
			else if(args[0x03].startsWith("-"))
			{
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, Integer.parseInt(args[0x03]));
				
				dataFinal = calendar.getTime();
			}
			else 
				dataFinal = new SimpleDateFormat("dd/MM/yyyy").parse(args[0x03]);
			
			new IntegracaoMovimentoEscritorio().sincronizarDadosLoja(idEmpresaFisica, dataInicial, dataFinal);
		}
		else if(args[0x0].equals("INTEGRACAO_INDIVIDUAL_OPME"))
		{
			Integer idEmpresaFisica = Integer.parseInt(args[0x1]);
			Integer sequencial = Integer.parseInt(args[0x2]);
			String dataBase = "";
			if(args[0x3].equals("0"))
				dataBase = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
			else
				dataBase = args[0x3];
			
			String dataInicial = "";
			if(args[0x4].equals("0"))
				dataInicial = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			else if(args[0x4].startsWith("-"))
			{
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, Integer.parseInt(args[0x4]));
				
				dataInicial = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
			}
			else 
				dataInicial = args[0x4];
			
			String dataFinal = "";
			if(args[0x5].equals("0"))
				dataFinal = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			else if(args[0x5].startsWith("-"))
			{
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, Integer.parseInt(args[0x5]));
				
				dataFinal = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
			}
			else 
				dataFinal = args[0x5];
			
			integracaoEscritorio.iniciarSincronizacaoOperacoes(idEmpresaFisica, sequencial, dataBase);
			integracaoEscritorio.converterOPME(args[0x1], dataInicial, dataFinal);
			
			LogFileWriter = new PrintWriter("F:\\desenv\\Log_Movimento\\MOVIMENTO_LOG_" + (new SimpleDateFormat("dd-MM-yyyy_hh-mm-ss").format(new Date())) + "_F" + idEmpresaFisica);	
			LogFileWriter.println("== PROCESSO INICIADO AS " + (new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date())) + " ==");
			LogFileWriter.println("===================");			
			LogFileWriter.println("[PARAMS]");
			LogFileWriter.println("[0x0] " + args[0x0]);
			LogFileWriter.println("[0x1] " + args[0x1]);
			LogFileWriter.println("[0x2] " + args[0x2]);
			LogFileWriter.println("[0x3] " + args[0x3]);
			LogFileWriter.println("[0x4] " + args[0x4]);
			LogFileWriter.println("[0x5] " + args[0x5]);
			LogFileWriter.println("[0x6] " + args[0x6]);
			LogFileWriter.println("===================");

			if(args[0x6].equals("1"))
			{
				integracaoEscritorio.persistirMETemporarias(idEmpresaFisica);		
				
				Statement stPedidoVenda = ConexaoUtil.getConexaoPadrao().createStatement();
				
				int quantidadeVenda = stPedidoVenda.executeQuery(" select count(*) from pedidoVenda where dataVenda between '" + dataInicial + " 00:00:00' and '" + dataFinal + " 23:59:59' and idTipoMovimentacaoEstoque in (2);").getInt(0);
				int quantidadeTroca = stPedidoVenda.executeQuery(" select count(*) from pedidoVenda where dataVenda between '" + dataInicial + " 00:00:00' and '" + dataFinal + " 23:59:59' and idTipoMovimentacaoEstoque in (5,6);").getInt(0) * 2; //2 MOVS POR TROCA (E/S)

				LogFileWriter.println("===================");
				LogFileWriter.println("TOTAL VENDAS > " + quantidadeVenda);
				LogFileWriter.println("TOTAL TROCAS > " + quantidadeTroca);
				LogFileWriter.println("TOTAL MOV'S  > " + (quantidadeVenda + quantidadeTroca));
				LogFileWriter.println("===================");
			}	
		}
		else if(args[0x0].equals("INTEGRACAO_PERSISTIR_TODAS_OPME"))
		{
			integracaoEscritorio.persistirMETemporarias();
		}
		else if(args[0x0].equals("INTEGRACAO_BAIXAR_TG"))
		{
			Integer idEmpresaFisica = Integer.parseInt(args[0x1]);
			Integer sequencial = Integer.parseInt(args[0x2]);
			String data = args[0x3];
			
			integracaoEscritorio.baixarTabelasGerais(idEmpresaFisica, data, sequencial);
		}
		else if(args[0x0].equals("INTEGRACAO_ATUALIZAR_BASE_PRINCIPAL"))
		{
			integracaoEscritorio.callStoredProcedure("atualizarDadosBasePrincipal");
		}
		else if(args[0x0].equals("INTEGRACAO_ATUALIZAR_ESTOQUE_LOJA00"))
		{
			integracaoEscritorio.callStoredProcedure("atualizarDadosEstoqueBase00");
		}
		else
			throw new Exception("Operação inválida!");
			
		LogFileWriter.flush();
		LogFileWriter.close();
	}

	/*
	 * Sincronizar base de dados de operações
	 */
	public void iniciarSincronizacaoOperacoes(Integer idEmpresaFisica, Integer sequencial, String data) throws Exception
    { 
		Exception processException = null;
		boolean processOk = false;
		
    	Date dataInicioProcesso = new Date();	
    	String numeroEmpresaFisica = String.format ("%02d", idEmpresaFisica);
    	
    	try
    	{
    		Date dataIntegracao = null;
    		if(data.equals("0"))
    			dataIntegracao = new Date();
    		else
    			dataIntegracao = new SimpleDateFormat("dd/MM/yyyy").parse(data);

    		lerArquivoServidorFtpECarregaEscritorio(idEmpresaFisica, sequencial, dataIntegracao);
        	
        	processOk = true;
    	}
    	catch(Exception ex)
    	{
			processOk = false; 
			processException = ex;
    	}
    	finally
    	{
    		if(processException != null)
    			processException.printStackTrace();
    		
    		Date dataFimProcesso = new Date();
    		Date dataTerminoProcesso = new Date();
    		dataTerminoProcesso.setTime(dataFimProcesso.getTime() - dataInicioProcesso.getTime());
    		
			if(processOk)
			{
				/*new MailUtil().sendMail(DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_ORIGEM_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_EMAIL_ORIGEM_NOTIFICACOES"), "Sistema Integrador Nepal", DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_DESTINO_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("SMPT_EMAIL_ORIGEM_NOTIFICACOES"), "integração Nepal. Baixa das OPME's da Loja " + numeroEmpresaFisica + " no Escritório", 
        				"Olá, a integração de baixa das OPME's da Loja " + numeroEmpresaFisica + " no Escritório foi concluída.<br><br>Iniciado em " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataInicioProcesso) + " e finalizado em " + 
        				new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataFimProcesso) + " o processo durou " + new SimpleDateFormat("mm:ss").format(dataTerminoProcesso) + " (minutos:segundos).");*/	
			}
			else
			{ 
				/*new MailUtil().sendMail(DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_ORIGEM_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_EMAIL_ORIGEM_NOTIFICACOES"), "Sistema Integrador Nepal", DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_DESTINO_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("SMPT_EMAIL_ORIGEM_NOTIFICACOES"), "integração Nepal. Baixa das OPME's da Loja " + numeroEmpresaFisica + " no Escritório FALHOU!!!", 
        				"Olá, a integração de baixa das OPME's da Loja " + numeroEmpresaFisica + " no Escritório não foi concluída.<br><br>Iniciado em " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataInicioProcesso) + " e finalizado em " + 
        				new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataFimProcesso) + " o processo durou " + new SimpleDateFormat("mm:ss").format(dataTerminoProcesso) + " (minutos:segundos) antes de ser abortado!!!!" + 
        				"<br><br>ERRO : " + processException.toString());*/
			}	
    	}
    }
    
    private void lerArquivoServidorFtpECarregaEscritorio(Integer idEmpresaFisica, Integer sequencial, Date data) throws Exception
    {
    	try
    	{
        	String numeroEmpresaFisicaFormatado = String.format("%02d", idEmpresaFisica);
        	String nomeArquivoDump = "dump_OPME_" + numeroEmpresaFisicaFormatado + "_" + new SimpleDateFormat("yyyyMMdd").format(data) + "_" + sequencial.toString() + ".NEPALDUMP";
        	
        	Logger.getGlobal().info("Initializing Download of file " + nomeArquivoDump + " for F" + numeroEmpresaFisicaFormatado);

        	new FtpUtil().downloadFile(nomeArquivoDump.replace("NEPALDUMP", "rar"), DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump.replace("NEPALDUMP", "rar"), DsvConstante.getParametrosSistema().get("INTEGRACAO_IP_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_USUARIO_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_FTP"));
        	
        	Logger.getGlobal().info("Downloaded! Extracting"); 
        	
        	IgnUtil.descompactarArquivo(DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump.replace("NEPALDUMP", "rar"), DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump.split("\\.")[0]);
        	
        	Logger.getGlobal().info("Done! Restoring " + (DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump.split("\\.")[0] + "/" + nomeArquivoDump) + " in Database " + ("nepalignloja" + numeroEmpresaFisicaFormatado));

        	new UtilDatabase().restoreBackupFile(DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump.split("\\.")[0] + "/" + nomeArquivoDump, "nepalignloja" + numeroEmpresaFisicaFormatado);
    	
        	Logger.getGlobal().info("Restore complete!");
        	
        	atualizarArquivosTransferidos();	
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    		throw ex;
    	}
    }
    
    private void atualizarArquivosTransferidos() throws Exception
    {
    	Logger.getGlobal().info("Updating name of files already handled");
    	
    	File directory = new File(DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS"));
	
    	File[] myFiles = directory.listFiles(new FilenameFilter() 
    	{
    		public boolean accept(File directory, String fileName) 
    		{
    			return fileName.endsWith(".rar");
    		}
    	});
	
    	for (File file: myFiles)
    	{
			Calendar dataLimite = Calendar.getInstance();
			dataLimite.setTime(new Date());
			dataLimite.add(Calendar.DAY_OF_YEAR, 4 * (-1));
			
			Calendar dataUltimaAlteracao = Calendar.getInstance();
			dataUltimaAlteracao.setTimeInMillis(file.lastModified());
			
			if(dataLimite.compareTo(dataUltimaAlteracao) > 0)
			{
	    		Logger.getGlobal().info(file.getName() + " will be deleted");
	    		
				file.delete();
			}
			else
			{
				File novoArquivo = new File(DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + file.getName().replace(".rar", ".ok"));
				file.renameTo(novoArquivo);
				
				novoArquivo = null;
				
	    		Logger.getGlobal().info(file.getName() + " updated to " + file.getName().replace(".rar", ".ok"));
			}
    		
    		file = null;
    	}
    	
    	Logger.getGlobal().info("Finish filename updates");
    }
    
    /*
     * Transformar dados das operações em movimentações temporárias.
     */
    public void converterOPME(String idEmpresaFisica, String dataInicial, String dataFinal) throws Exception 
	{ 
		Connection con = null;
		
		EntityManager manager = null;
		EntityTransaction transaction = null;
		
    	try
    	{    		
    		Logger.getGlobal().info("Iniciando conexão com o banco de dados.");
    		con = ConexaoUtil.getConexaoPadrao();
    		manager = ConexaoUtil.getEntityManager();
    		transaction = manager.getTransaction();
    		transaction.begin();
    		
    		Logger.getGlobal().info("Criando definições.");
    		con.setAutoCommit(false);
    		IgnUtil ignUtil = new IgnUtil();
    		String numeroEmpresaFisica = String.format ("%02d", Integer.parseInt(idEmpresaFisica));
    		String databaseName = "nepalignloja" + numeroEmpresaFisica;
    		
    		Logger.getGlobal().info("Iniciando processo de integração para a empresa " + numeroEmpresaFisica + " no banco de dados " + databaseName);
    		  
    		String sqlQueryOperacoes = "select operacao, objeto, idoperacao, nomeClasse from " + databaseName + ".operacaomovimentacaoestoque where dataoperacao between '" + dataInicial + " 00:00:00' and '" + dataFinal + " 23:59:59'";    
    		   
    		Statement st = con.createStatement();  
    		Statement stAtualizacao = con.createStatement();    
        	   
    		ResultSet rs = st.executeQuery(sqlQueryOperacoes);   
    		
    		boolean hasOp = rs.next(); 
    		
    		if(!hasOp) 
    			Logger.getGlobal().info("Nenhuma operação pendente encontrada.");  
    		else  
    		{  
    			rs.afterLast();
    			Logger.getGlobal().info("Encontradas " + rs.getRow() + " operações pendentes!");  
    			rs.beforeFirst();	
    		}
    	
    		Logger.getGlobal().info("Apagando dados temporários antigos.");
    		
    		stAtualizacao.executeUpdate("delete from " + databaseName + ".tmpitemmovimentacaoestoque;");
    		stAtualizacao.executeUpdate("delete from " + databaseName + ".tmpmovimentacaoestoque;");
    		
    		Logger.getGlobal().info("Tudo pronto. Iniciando processo!");
    		
    		while (rs.next()) 
    		{
    			String operacao = rs.getString(1); 
    			byte[] objeto = rs.getBytes(2);
        		int idOperacaoMovimentacaoEstoque = rs.getInt(3);
        		String classe = rs.getString(4);
        		 
        		if(classe.equalsIgnoreCase(MovimentacaoEstoquePersistence.class.getName()))
        			tratarMovimentacaoEstoque(databaseName, (MovimentacaoEstoque) ignUtil.recuperarObjectoPorByteArray(objeto), operacao, idOperacaoMovimentacaoEstoque, stAtualizacao, manager, transaction);		
        		else if(classe.equalsIgnoreCase(ItemMovimentacaoEstoquePersistence.class.getName()))
        		{  
            		ItemMovimentacaoEstoque itemMovimentacaoEstoque = (ItemMovimentacaoEstoque) ignUtil.recuperarObjectoPorByteArray(objeto);

            		try
            		{
                		StringBuilder sqlQuery = new StringBuilder();
                		
            			if(operacao != null && operacao.equalsIgnoreCase("SALVAR"))
            			{
            				tratarMovimentacaoEstoque(databaseName, itemMovimentacaoEstoque.getMovimentacaoEstoque(), "SALVAR", idOperacaoMovimentacaoEstoque, stAtualizacao, manager, transaction);
            				
                    		sqlQuery.append(" INSERT INTO `" + databaseName + "`.`tmpitemmovimentacaoestoque` ");
                    		sqlQuery.append(" (`idTmpItemMovimentacaoEstoque`, ");
                    		sqlQuery.append(" `idEstoqueProduto`, ");
                    		sqlQuery.append(" `idTmpMovimentacaoEstoque`, ");
                    		sqlQuery.append(" `idUnidadeMedida`, ");
                    		sqlQuery.append(" `idProduto`, ");
                    		sqlQuery.append(" `idDocumentoOrigem`, ");
                    		sqlQuery.append(" `sequencialItem`, ");
                    		sqlQuery.append(" `quantidade`, ");
                    		sqlQuery.append(" `valor`, ");
                    		sqlQuery.append(" `custo`, ");
                    		sqlQuery.append(" `numeroLote`, ");
                    		sqlQuery.append(" `dataValidade`, ");
                    		sqlQuery.append(" `dataFabricacao`, ");
                    		sqlQuery.append(" `venda`) ");
                    		sqlQuery.append(" VALUES ");
                    		sqlQuery.append(" (" + itemMovimentacaoEstoque.getId() + ", ");
                    		sqlQuery.append(" " + itemMovimentacaoEstoque.getEstoqueProduto().getId() + ", ");
                    		sqlQuery.append(" " + itemMovimentacaoEstoque.getMovimentacaoEstoque().getId() + ", ");
                    		sqlQuery.append(" " + (itemMovimentacaoEstoque.getUnidadeMedida() == null ? "null" : itemMovimentacaoEstoque.getUnidadeMedida().getId()) + ", ");
                    		sqlQuery.append(" " + itemMovimentacaoEstoque.getProduto().getId() + ", ");
                    		sqlQuery.append(" '" + (itemMovimentacaoEstoque.getDocumentoOrigem() == null ? "null" : itemMovimentacaoEstoque.getDocumentoOrigem()) + "', ");
                    		sqlQuery.append(" " + itemMovimentacaoEstoque.getSequencialItem() + ", ");
                    		sqlQuery.append(" " + itemMovimentacaoEstoque.getQuantidade() + ", ");
                    		sqlQuery.append(" " + itemMovimentacaoEstoque.getValor() + ", ");
                    		sqlQuery.append(" " + itemMovimentacaoEstoque.getCusto() + ", ");
                    		sqlQuery.append(" " + (itemMovimentacaoEstoque.getNumeroLote() == null ? "null" : itemMovimentacaoEstoque.getNumeroLote()) + ", ");
                    		sqlQuery.append(" '" + (itemMovimentacaoEstoque.getDataValidade() == null ? "2014-01-01 00:00:00" : new SimpleDateFormat().format(itemMovimentacaoEstoque.getDataValidade())) + "', ");
                    		sqlQuery.append(" '" + (itemMovimentacaoEstoque.getDataFabricacao() == null ? "2014-01-01 00:00:00" : new SimpleDateFormat().format(itemMovimentacaoEstoque.getDataFabricacao())) + "', ");
                    		sqlQuery.append(" " + itemMovimentacaoEstoque.getVenda() + "); ");
                    		
                    		Logger.getGlobal().info("OP [SALVAR] IME CBAR> " + itemMovimentacaoEstoque.getEstoqueProduto().getCodigoBarras() + " ID. " + itemMovimentacaoEstoque.getId() + " N. Mov. " + itemMovimentacaoEstoque.getMovimentacaoEstoque().getNumeroDocumento() + " ID Op." + idOperacaoMovimentacaoEstoque);	
            			} 
                		else if (operacao != null && operacao.equalsIgnoreCase("EXCLUIR"))
                		{
                			sqlQuery.append("DELETE FROM " + databaseName + ".tmpitemmovimentacaoestoque where idtmpitemmovimentacaoestoque = " + itemMovimentacaoEstoque.getId());

                			Logger.getGlobal().info("OP [EXCLUIR] IME CBAR> " + itemMovimentacaoEstoque.getEstoqueProduto().getCodigoBarras() + " ID. " + itemMovimentacaoEstoque.getId() + " N. Mov. " + itemMovimentacaoEstoque.getMovimentacaoEstoque().getNumeroDocumento() + " ID Op." + idOperacaoMovimentacaoEstoque);	
                		}
            			
        				stAtualizacao.executeUpdate(sqlQuery.toString());
        				stAtualizacao.executeUpdate("update "  + databaseName + ".operacaomovimentacaoestoque set flagsincronizacao = 1 where idoperacao = " + idOperacaoMovimentacaoEstoque + ";");
            		}
            		catch(Exception ex)
            		{
            			Logger.getGlobal().severe("Fail on IME to ME " + (itemMovimentacaoEstoque.getMovimentacaoEstoque() == null ? "null" : itemMovimentacaoEstoque.getMovimentacaoEstoque().getId().toString()) + " " + ex.getMessage());
            			throw ex;
            		}
        		}
        		else
        			Logger.getGlobal().warning("Invalid OP Class : " + classe);
    		}
    		transaction.commit();
    		con.commit();
    		Logger.getGlobal().info("Finalizado. Processo Concluído!");
    	}
    	catch(Exception ex)
    	{
    		con.rollback();
    		transaction.rollback();
    		manager.close();
    	}
	}
	
	private boolean tratarMovimentacaoEstoque(String databaseName, MovimentacaoEstoque movimentacaoEstoque, String operacao, Integer idOperacaoMovimentacaoEstoque, Statement stAtualizacao, EntityManager manager, EntityTransaction transaction) throws Exception
	{    
		try
		{
			StringBuilder sqlQuery = new StringBuilder();
			
    		if (operacao != null && operacao.equalsIgnoreCase("SALVAR")) 
    		{ 
    			sqlQuery.append(" INSERT INTO `" + databaseName + "`.`tmpmovimentacaoestoque` ");
    			sqlQuery.append(" (`idTmpMovimentacaoEstoque`, ");
    			sqlQuery.append(" `idEmpresaFisica`, ");
    			sqlQuery.append(" `idEstoque`, ");
    			sqlQuery.append(" `idTipoMovimentacaoEstoque`, ");
    			sqlQuery.append(" `idTipoDocumentoMovimentoEstoque`, ");
    			sqlQuery.append(" `idCliente`, ");
    			sqlQuery.append(" `idUsuario`, "); 
    			sqlQuery.append(" `entradaSaida`, ");
    			sqlQuery.append(" `numeroDocumento`, ");
    			sqlQuery.append(" `dataMovimentacao`, ");
    			sqlQuery.append(" `idDocumentoOrigem`, ");
    			sqlQuery.append(" `observacao`, ");
    			sqlQuery.append(" `faturado`, ");
    			sqlQuery.append(" `manual`, ");
    			sqlQuery.append(" `numeroVolume`, ");
    			sqlQuery.append(" `peso`, ");
    			sqlQuery.append(" `valorFrete`, ");
    			sqlQuery.append(" `idFornecedor`, "); 
    			sqlQuery.append(" `idStatusMovimentacaoEstoque`, ");
    			sqlQuery.append(" `idEmpresaDestino`) ");
    			sqlQuery.append(" VALUES "); 
    			sqlQuery.append(" (" + movimentacaoEstoque.getId() + ", ");
    			sqlQuery.append(" " + movimentacaoEstoque.getEmpresaFisica().getId() + ", ");
    			sqlQuery.append(" " + movimentacaoEstoque.getEstoque().getId() + ", ");
    			sqlQuery.append(" " + movimentacaoEstoque.getTipoMovimentacaoEstoque().getId() + ", ");
    			sqlQuery.append(" " + movimentacaoEstoque.getTipoDocumentoMovimento().getId() + ", ");
    			sqlQuery.append(" " + (movimentacaoEstoque.getCliente() == null ? "null" : movimentacaoEstoque.getCliente().getId()) + ", ");
    			sqlQuery.append(" " + (movimentacaoEstoque.getUsuario() == null ? "null" : movimentacaoEstoque.getUsuario().getId()) + ", ");
    			sqlQuery.append(" '" + movimentacaoEstoque.getEntradaSaida() + "', ");
    			sqlQuery.append(" '" + movimentacaoEstoque.getNumeroDocumento() + "', ");
    			sqlQuery.append(" '" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(movimentacaoEstoque.getDataMovimentacao()) + "', ");
    			sqlQuery.append(" '" + (movimentacaoEstoque.getIdDocumentoOrigem() == null ? "0" : movimentacaoEstoque.getIdDocumentoOrigem()) + "', ");
    			sqlQuery.append(" '" + movimentacaoEstoque.getObservacao() + "', ");
    			sqlQuery.append(" '" + movimentacaoEstoque.getFaturado() + "', ");
    			sqlQuery.append(" '" + movimentacaoEstoque.getManual() + "', ");
    			sqlQuery.append(" '" + (movimentacaoEstoque.getNumeroVolume() == null ? "0" : movimentacaoEstoque.getNumeroVolume()) + "', ");
    			sqlQuery.append(" " + movimentacaoEstoque.getPeso() + ", ");
    			sqlQuery.append(" " + movimentacaoEstoque.getValorFrete() + ", ");
    			sqlQuery.append(" " + (movimentacaoEstoque.getForncedor() == null ? "null" :  movimentacaoEstoque.getForncedor().getId()) + ", ");
    			sqlQuery.append(" " + movimentacaoEstoque.getStatusMovimentacaoEstoque().getId() + ", ");
    			sqlQuery.append(" " + (movimentacaoEstoque.getEmpresaDestino() == null ? "null" : movimentacaoEstoque.getEmpresaDestino().getId()) + "); ");
    			 
    			Logger.getGlobal().info("OP [SALVAR] ME NDOC > " + movimentacaoEstoque.getNumeroDocumento() + " ID. " + movimentacaoEstoque.getId() + " ID Op." + idOperacaoMovimentacaoEstoque);
    		}
    		else if (operacao != null && operacao.equalsIgnoreCase("EXCLUIR")) 
    		{
    			ResultSet rs = stAtualizacao.executeQuery("SELECT idtmpmovimentacaoestoque FROM " + databaseName + ".tmpmovimentacaoestoque where idtmpmovimentacaoestoque = " + movimentacaoEstoque.getId());
    			
    			if(rs.next())
    			{
        			sqlQuery.append("DELETE FROM " + databaseName + ".tmpmovimentacaoestoque where idtmpmovimentacaoestoque = " + movimentacaoEstoque.getId());
        			
        			Logger.getGlobal().info("OP [EXCLUIR] ME NDOC > " + movimentacaoEstoque.getNumeroDocumento() + " ID. " + movimentacaoEstoque.getId() + " ID Op. " + idOperacaoMovimentacaoEstoque);
        			
        			rs.close();
    			}
    			else
	    		{ 
    				rs.close();
    				rs = stAtualizacao.executeQuery("SELECT idmovimentacaoestoque FROM nepalign.movimentacaoestoque where numeroDocumento = '" + movimentacaoEstoque.getNumeroDocumento() + "' and idEmpresaFisica = " + movimentacaoEstoque.getEmpresaFisica().getId().toString());
    				
    				if(rs.next())
    				{
    					new MovimentacaoEstoqueService().excluirMovimentacaoEstoque(new MovimentacaoEstoqueService().recuperarPorId(manager, rs.getInt("idmovimentacaoestoque")), manager, transaction);
    					
        				Logger.getGlobal().info("OP [EXCLUIR] ME NDOC > " + movimentacaoEstoque.getNumeroDocumento() + " ID. " + movimentacaoEstoque.getId() + " ID Op. " + idOperacaoMovimentacaoEstoque);
    				}
    				else
        				Logger.getGlobal().info("OP [EXCLUIR] ME NDOC > " + movimentacaoEstoque.getNumeroDocumento() + " ID. " + movimentacaoEstoque.getId() + " ID Op. " + idOperacaoMovimentacaoEstoque + " não conseguiu encontrar movimentação");    				
    				
    				sqlQuery = null; 
    				rs.close();
	    		}
    		}
    		
    		if(sqlQuery != null)
    			stAtualizacao.executeUpdate(sqlQuery.toString());
    		
    		stAtualizacao.executeUpdate("update "  + databaseName + ".operacaomovimentacaoestoque set flagsincronizacao = 1 where idoperacao = " + idOperacaoMovimentacaoEstoque + ";");
			
    		return true;
		}
		catch(MySQLIntegrityConstraintViolationException e)
		{
			return false;
		}
		catch(Exception ex)
		{
			Logger.getGlobal().severe("Fail ME " + (movimentacaoEstoque == null ? "null" : movimentacaoEstoque.getId().toString()) + " " + ex.getMessage());
			throw ex;
		}
	}
	
	/*
	 * Persistir movimentações temporárias.
	 */
	public void persistirMETemporarias() throws Exception
	{
		Logger.getGlobal().info("Carregando chaves primárias das entidades.");
		ListaEmpresaEntidadeChave.getInstance();  

		Logger.getGlobal().info("Inicializando banco de dados.");
		
		Connection conn = null;
		EntityManager manager = null;  
		EntityTransaction transaction = null;
		
		try
		{
			manager = ConexaoUtil.getEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			
			conn = ConexaoUtil.getConexaoPadrao();
			
			Logger.getGlobal().info("Conectado! Transação ativa.");
		}
		catch(Exception conException)
		{
			throw new Exception("não foi possível iniciar a conexão com o banco de dados!");
		}
		
		Integer quantidadeMovimentacoesPersistidas = 0;

		Logger.getGlobal().info("Iniciando Services.");
		UsuarioService usuarioService = new UsuarioService();
		EstoqueService estoqueService = new EstoqueService();
		ClienteService clienteservice = new ClienteService();
		ProdutoService produtoService = new ProdutoService();
		FornecedorService fornecedorService = new FornecedorService();
		EmpresaFisicaService empresaFisicaService = new EmpresaFisicaService();
		UnidadeMedidaService unidadeMedidaService = new UnidadeMedidaService();
		EstoqueProdutoService estoqueProdutoService = new EstoqueProdutoService();
		MovimentacaoEstoqueService movimentacaoEstoqueService = new MovimentacaoEstoqueService();
		ItemMovimentacaoEstoqueService itemMovimentacaoEstoqueService = new ItemMovimentacaoEstoqueService();
		TipoMovimentacaoEstoqueService tipoMovimentacaoEstoqueService = new TipoMovimentacaoEstoqueService();
		StatusMovimentacaoEstoqueService statusMovimentacaoEstoqueService = new StatusMovimentacaoEstoqueService();
		TipoDocumentoMovimentoEstoqueService tipoDocumentoMovimentoEstoqueService = new TipoDocumentoMovimentoEstoqueService();
		
		try
		{
			if(DsvConstante.getParametrosSistema().get("EMPRESAS_INTEGRACAO_ESTOQUE") == null && DsvConstante.getParametrosSistema().get("EMPRESAS_INTEGRACAO_ESTOQUE").isEmpty())
				throw new Exception("parâmetro EMPRESAS_INTEGRACAO_ESTOQUE não encontrado! Processo abortado.");
			
			String[] empresasIntegracao = DsvConstante.getParametrosSistema().get("EMPRESAS_INTEGRACAO_ESTOQUE").split(",");
			
			Logger.getGlobal().info("Empresas informadas para a integração " + DsvConstante.getParametrosSistema().get("EMPRESAS_INTEGRACAO_ESTOQUE"));

			for(String empresa : empresasIntegracao)
			{
				String numeroEmpresaFisica = String.format ("%02d", Integer.parseInt(empresa));
				String databaseName = "nepalignloja" + numeroEmpresaFisica;
				
				Logger.getGlobal().info("Iniciando processo de integração para a empresa " + numeroEmpresaFisica + " no banco de dados " + databaseName);

				String tmpQuery = "SELECT * FROM " + databaseName + ".tmpMovimentacaoEstoque;";

				ResultSet rsTmpMovimentacaoEstoque = conn.createStatement().executeQuery(tmpQuery);
				
				if(!rsTmpMovimentacaoEstoque.next())
					throw new Exception("não existem movimentações a serem integradas!");
				
				rsTmpMovimentacaoEstoque.last();
				
				Logger.getGlobal().info("Encontradas " + rsTmpMovimentacaoEstoque.getRow() + " movimentações!");
				
				rsTmpMovimentacaoEstoque.beforeFirst();

				while(rsTmpMovimentacaoEstoque.next())
				{
					MovimentacaoEstoque movimentacaoEstoque = new MovimentacaoEstoque();
					movimentacaoEstoque.setCliente(clienteservice.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idCliente")));
					movimentacaoEstoque.setDataMovimentacao(rsTmpMovimentacaoEstoque.getDate("dataMovimentacao"));
					movimentacaoEstoque.setEmpresaDestino(empresaFisicaService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idEmpresaDestino")));
					movimentacaoEstoque.setEmpresaFisica(empresaFisicaService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idEmpresaFisica")));
					movimentacaoEstoque.setEntradaSaida(rsTmpMovimentacaoEstoque.getString("entradaSaida"));
					movimentacaoEstoque.setEstoque(estoqueService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idEstoque")));
					movimentacaoEstoque.setFaturado(rsTmpMovimentacaoEstoque.getString("faturado"));
					movimentacaoEstoque.setForncedor(fornecedorService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idFornecedor")));
					movimentacaoEstoque.setIdDocumentoOrigem(rsTmpMovimentacaoEstoque.getInt("idDocumentoOrigem"));
					movimentacaoEstoque.setManual(rsTmpMovimentacaoEstoque.getString("manual"));
					movimentacaoEstoque.setNumeroDocumento(rsTmpMovimentacaoEstoque.getString("numeroDocumento"));
					movimentacaoEstoque.setNumeroVolume(rsTmpMovimentacaoEstoque.getInt("numeroVolume"));
					movimentacaoEstoque.setObservacao(rsTmpMovimentacaoEstoque.getString("observacao"));
					movimentacaoEstoque.setPeso(rsTmpMovimentacaoEstoque.getDouble("peso"));
					movimentacaoEstoque.setStatusMovimentacaoEstoque(statusMovimentacaoEstoqueService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idStatusMovimentacaoEstoque")));
					movimentacaoEstoque.setTipoDocumentoMovimento(tipoDocumentoMovimentoEstoqueService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idTipoDocumentoMovimentoEstoque")));
					movimentacaoEstoque.setTipoMovimentacaoEstoque(tipoMovimentacaoEstoqueService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idTipoMovimentacaoEstoque")));
					movimentacaoEstoque.setUsuario(usuarioService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idUsuario")));
					movimentacaoEstoque.setValorFrete(rsTmpMovimentacaoEstoque.getDouble("valorFrete"));
					
					Logger.getGlobal().info("Persistindo movimentação N. " + movimentacaoEstoque.getNumeroDocumento() + " do Tipo " + movimentacaoEstoque.getTipoMovimentacaoEstoque().getDescricao());

					movimentacaoEstoqueService.salvar(manager, transaction, movimentacaoEstoque, false);

					List<ItemMovimentacaoEstoque> listaItemMovimentacaoEstoque = new ArrayList<ItemMovimentacaoEstoque>();

					String tmpItemQuery = "SELECT * FROM " + databaseName + ".tmpItemMovimentacaoEstoque WHERE idTmpMovimentacaoEstoque = " + rsTmpMovimentacaoEstoque.getInt("idTmpMovimentacaoEstoque") + ";";

					ResultSet rsTmpItemMovimentacaoEstoque = conn.createStatement().executeQuery(tmpItemQuery);

					while(rsTmpItemMovimentacaoEstoque.next())
					{
						ItemMovimentacaoEstoque itemMovimentacaoEstoque = new ItemMovimentacaoEstoque();
						itemMovimentacaoEstoque.setCusto(rsTmpItemMovimentacaoEstoque.getDouble("custo"));
						itemMovimentacaoEstoque.setDataFabricacao(rsTmpItemMovimentacaoEstoque.getDate("dataFabricacao"));
						itemMovimentacaoEstoque.setDataValidade(rsTmpItemMovimentacaoEstoque.getDate("dataValidade"));
						itemMovimentacaoEstoque.setDocumentoOrigem(rsTmpItemMovimentacaoEstoque.getString("documentoOrigem"));
						itemMovimentacaoEstoque.setEstoqueProduto(estoqueProdutoService.recuperarPorId(manager, rsTmpItemMovimentacaoEstoque.getInt("idEstoqueProduto")));
						itemMovimentacaoEstoque.setMovimentacaoEstoque(movimentacaoEstoque);
						itemMovimentacaoEstoque.setNumeroLote(rsTmpItemMovimentacaoEstoque.getString("numeroLote"));
						itemMovimentacaoEstoque.setProduto(produtoService.recuperarPorId(manager, rsTmpItemMovimentacaoEstoque.getInt("idProduto")));
						itemMovimentacaoEstoque.setQuantidade(rsTmpItemMovimentacaoEstoque.getDouble("quantidade"));
						itemMovimentacaoEstoque.setSequencialItem(rsTmpItemMovimentacaoEstoque.getInt("sequencialItem"));
						itemMovimentacaoEstoque.setUnidadeMedida(unidadeMedidaService.recuperarPorId(manager, rsTmpItemMovimentacaoEstoque.getInt("idUnidadeMedida")));
						itemMovimentacaoEstoque.setValor(rsTmpItemMovimentacaoEstoque.getDouble("valor"));
						itemMovimentacaoEstoque.setVenda(rsTmpItemMovimentacaoEstoque.getDouble("venda"));

						Logger.getGlobal().info("Adicionado novo item com código de barras " + itemMovimentacaoEstoque.getEstoqueProduto().getCodigoBarras() + " com Quantidade " + itemMovimentacaoEstoque.getQuantidade());
						listaItemMovimentacaoEstoque.add(itemMovimentacaoEstoque);
					}
					Logger.getGlobal().info("Total de " + listaItemMovimentacaoEstoque.size() + " items! Persistindo.");
					itemMovimentacaoEstoqueService.salvarItems(listaItemMovimentacaoEstoque, false, manager, transaction);
					Logger.getGlobal().info("Movimentação salva com sucesso!");
					
					quantidadeMovimentacoesPersistidas++;
				}
			}

			Logger.getGlobal().info("Movimentações integradas com sucesso! " + quantidadeMovimentacoesPersistidas + " ME's Persistidas!");
			Logger.getGlobal().info("Finalizando Transação.");
			transaction.commit();
			Logger.getGlobal().info("Transação finalizada. Dados salvos!");
			
			Logger.getGlobal().info("Fechando conexão com o banco de dados e finalizando os Services.");
			conn.close();
			manager.close();
			
			usuarioService = null;
			estoqueService = null;
			clienteservice = null;
			produtoService = null;
			fornecedorService = null;
			empresaFisicaService = null;
			unidadeMedidaService = null;
			estoqueProdutoService = null;
			movimentacaoEstoqueService = null;
			itemMovimentacaoEstoqueService = null;
			tipoMovimentacaoEstoqueService = null;
			statusMovimentacaoEstoqueService = null;
			tipoDocumentoMovimentoEstoqueService = null;
			Logger.getGlobal().info("Finalizado. Processo Concluído!");
		}
		catch(Exception ex)
		{
			if (transaction!=null && transaction.isActive())
				transaction.rollback();
			
			ex.printStackTrace();
			throw ex;
						
		}
	}
	
	public void persistirMETemporarias(Integer idEmpresaFisica) throws Exception
	{
		Logger.getGlobal().info("Carregando chaves primárias das entidades.");
		ListaEmpresaEntidadeChave.getInstance();  

		Logger.getGlobal().info("Inicializando banco de dados.");
		
		Connection conn = null;
		EntityManager manager = null;  
		EntityTransaction transaction = null;
		
		try
		{
			manager = ConexaoUtil.getEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			
			conn = ConexaoUtil.getConexaoPadrao();
			
			Logger.getGlobal().info("Conectado! Transação ativa.");
		}
		catch(Exception conException)
		{
			throw new Exception("não foi possível iniciar a conexão com o banco de dados!");
		}
		
		Integer quantidadeMovimentacoesPersistidas = 0;

		Logger.getGlobal().info("Iniciando Services.");
		UsuarioService usuarioService = new UsuarioService();
		EstoqueService estoqueService = new EstoqueService();
		ClienteService clienteservice = new ClienteService();
		ProdutoService produtoService = new ProdutoService();
		FornecedorService fornecedorService = new FornecedorService();
		EmpresaFisicaService empresaFisicaService = new EmpresaFisicaService();
		UnidadeMedidaService unidadeMedidaService = new UnidadeMedidaService();
		EstoqueProdutoService estoqueProdutoService = new EstoqueProdutoService();
		MovimentacaoEstoqueService movimentacaoEstoqueService = new MovimentacaoEstoqueService();
		ItemMovimentacaoEstoqueService itemMovimentacaoEstoqueService = new ItemMovimentacaoEstoqueService();
		TipoMovimentacaoEstoqueService tipoMovimentacaoEstoqueService = new TipoMovimentacaoEstoqueService();
		StatusMovimentacaoEstoqueService statusMovimentacaoEstoqueService = new StatusMovimentacaoEstoqueService();
		TipoDocumentoMovimentoEstoqueService tipoDocumentoMovimentoEstoqueService = new TipoDocumentoMovimentoEstoqueService();
		
		try
		{
			String numeroEmpresaFisica = String.format ("%02d", idEmpresaFisica);
			String databaseName = "nepalignloja" + numeroEmpresaFisica;
			
			Logger.getGlobal().info("Iniciando processo de integração para a empresa " + numeroEmpresaFisica + " no banco de dados " + databaseName);

			String tmpQuery = "SELECT * FROM " + databaseName + ".tmpMovimentacaoEstoque;";

			ResultSet rsTmpMovimentacaoEstoque = conn.createStatement().executeQuery(tmpQuery);
			
			if(!rsTmpMovimentacaoEstoque.next())
				throw new Exception("não existem movimentações a serem integradas!");
			
			rsTmpMovimentacaoEstoque.last();
			
			Logger.getGlobal().info("Encontradas " + rsTmpMovimentacaoEstoque.getRow() + " movimentações!");
			
			rsTmpMovimentacaoEstoque.beforeFirst();

			while(rsTmpMovimentacaoEstoque.next())
			{
				MovimentacaoEstoque movimentacaoEstoque = new MovimentacaoEstoque();
				movimentacaoEstoque.setCliente(clienteservice.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idCliente")));
				movimentacaoEstoque.setDataMovimentacao(rsTmpMovimentacaoEstoque.getDate("dataMovimentacao"));
				movimentacaoEstoque.setEmpresaDestino(empresaFisicaService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idEmpresaDestino")));
				movimentacaoEstoque.setEmpresaFisica(empresaFisicaService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idEmpresaFisica"))); 
				movimentacaoEstoque.setEntradaSaida(rsTmpMovimentacaoEstoque.getString("entradaSaida"));
				movimentacaoEstoque.setEstoque(estoqueService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idEstoque")));
				movimentacaoEstoque.setFaturado(rsTmpMovimentacaoEstoque.getString("faturado"));
				movimentacaoEstoque.setForncedor(fornecedorService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idFornecedor")));
				movimentacaoEstoque.setIdDocumentoOrigem(rsTmpMovimentacaoEstoque.getInt("idDocumentoOrigem"));
				movimentacaoEstoque.setManual(rsTmpMovimentacaoEstoque.getString("manual"));
				movimentacaoEstoque.setNumeroDocumento(rsTmpMovimentacaoEstoque.getString("numeroDocumento"));
				movimentacaoEstoque.setNumeroVolume(rsTmpMovimentacaoEstoque.getInt("numeroVolume"));
				movimentacaoEstoque.setObservacao(rsTmpMovimentacaoEstoque.getString("observacao"));
				movimentacaoEstoque.setPeso(rsTmpMovimentacaoEstoque.getDouble("peso"));
				movimentacaoEstoque.setStatusMovimentacaoEstoque(statusMovimentacaoEstoqueService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idStatusMovimentacaoEstoque")));
				movimentacaoEstoque.setTipoDocumentoMovimento(tipoDocumentoMovimentoEstoqueService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idTipoDocumentoMovimentoEstoque")));
				movimentacaoEstoque.setTipoMovimentacaoEstoque(tipoMovimentacaoEstoqueService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idTipoMovimentacaoEstoque")));
				movimentacaoEstoque.setUsuario(usuarioService.recuperarPorId(manager, rsTmpMovimentacaoEstoque.getInt("idUsuario")));
				movimentacaoEstoque.setValorFrete(rsTmpMovimentacaoEstoque.getDouble("valorFrete"));
				
				Logger.getGlobal().info("P. ME N. > " + movimentacaoEstoque.getNumeroDocumento() + " do Tipo " + movimentacaoEstoque.getTipoMovimentacaoEstoque().getDescricao());
				
				if(!verificarMovimentacaoExiste(manager, movimentacaoEstoque.getNumeroDocumento(), movimentacaoEstoque.getEmpresaFisica(), movimentacaoEstoque.getDataMovimentacao(), movimentacaoEstoque.getTipoMovimentacaoEstoque()))
				{
					Logger.getGlobal().info("ERR - P. ME N. > " + movimentacaoEstoque.getNumeroDocumento() + " do Tipo " + movimentacaoEstoque.getTipoMovimentacaoEstoque().getDescricao());	
					continue;	
				}

				movimentacaoEstoqueService.salvar(manager, transaction, movimentacaoEstoque, false);

				List<ItemMovimentacaoEstoque> listaItemMovimentacaoEstoque = new ArrayList<ItemMovimentacaoEstoque>();

				String tmpItemQuery = "SELECT * FROM " + databaseName + ".tmpItemMovimentacaoEstoque WHERE idTmpMovimentacaoEstoque = " + rsTmpMovimentacaoEstoque.getInt("idTmpMovimentacaoEstoque") + ";";

				ResultSet rsTmpItemMovimentacaoEstoque = conn.createStatement().executeQuery(tmpItemQuery);
				
				LogFileWriter.println("[ME] " + movimentacaoEstoque.getNumeroDocumento());

				while(rsTmpItemMovimentacaoEstoque.next())
				{
					ItemMovimentacaoEstoque itemMovimentacaoEstoque = new ItemMovimentacaoEstoque();
					itemMovimentacaoEstoque.setCusto(rsTmpItemMovimentacaoEstoque.getDouble("custo"));
					itemMovimentacaoEstoque.setDataFabricacao(rsTmpItemMovimentacaoEstoque.getDate("dataFabricacao"));
					itemMovimentacaoEstoque.setDataValidade(rsTmpItemMovimentacaoEstoque.getDate("dataValidade"));
					itemMovimentacaoEstoque.setDocumentoOrigem(rsTmpItemMovimentacaoEstoque.getString("idDocumentoOrigem"));
					itemMovimentacaoEstoque.setEstoqueProduto(estoqueProdutoService.recuperarPorId(manager, rsTmpItemMovimentacaoEstoque.getInt("idEstoqueProduto")));
					itemMovimentacaoEstoque.setMovimentacaoEstoque(movimentacaoEstoque);
					itemMovimentacaoEstoque.setNumeroLote(rsTmpItemMovimentacaoEstoque.getString("numeroLote"));
					itemMovimentacaoEstoque.setProduto(produtoService.recuperarPorId(manager, rsTmpItemMovimentacaoEstoque.getInt("idProduto")));
					itemMovimentacaoEstoque.setQuantidade(rsTmpItemMovimentacaoEstoque.getDouble("quantidade"));
					itemMovimentacaoEstoque.setSequencialItem(rsTmpItemMovimentacaoEstoque.getInt("sequencialItem"));
					itemMovimentacaoEstoque.setUnidadeMedida(unidadeMedidaService.recuperarPorId(manager, rsTmpItemMovimentacaoEstoque.getInt("idUnidadeMedida")));
					itemMovimentacaoEstoque.setValor(rsTmpItemMovimentacaoEstoque.getDouble("valor"));
					itemMovimentacaoEstoque.setVenda(rsTmpItemMovimentacaoEstoque.getDouble("venda"));

					Logger.getGlobal().info("--ITEM > " + itemMovimentacaoEstoque.getEstoqueProduto().getCodigoBarras() + " com Quantidade " + itemMovimentacaoEstoque.getQuantidade());
					listaItemMovimentacaoEstoque.add(itemMovimentacaoEstoque);
					
					LogFileWriter.println(" [IME] " + itemMovimentacaoEstoque.getEstoqueProduto().getCodigoBarras());
				}
				
				Logger.getGlobal().info("Total de " + listaItemMovimentacaoEstoque.size() + " items! Persistindo.");
				itemMovimentacaoEstoqueService.salvarItems(listaItemMovimentacaoEstoque, false, manager, transaction);
				Logger.getGlobal().info("Movimentação salva com sucesso!");

				quantidadeMovimentacoesPersistidas++;
			}

			Logger.getGlobal().info("Movimentações integradas com sucesso! " + quantidadeMovimentacoesPersistidas + " ME's persistidas!");
			Logger.getGlobal().info("Finalizando Transação.");
			transaction.commit();
			Logger.getGlobal().info("Transação finalizada. Dados salvos!");
			
			Logger.getGlobal().info("Fechando conexão com o banco de dados e finalizando os Services.");
			conn.close();
			manager.close();
			
			LogFileWriter.println("==================");
			LogFileWriter.println("QTD TOTAL ME > " + quantidadeMovimentacoesPersistidas);
			
			usuarioService = null;
			estoqueService = null;
			clienteservice = null;
			produtoService = null;
			fornecedorService = null;
			empresaFisicaService = null;
			unidadeMedidaService = null;
			estoqueProdutoService = null;
			movimentacaoEstoqueService = null;
			itemMovimentacaoEstoqueService = null;
			tipoMovimentacaoEstoqueService = null;
			statusMovimentacaoEstoqueService = null;
			tipoDocumentoMovimentoEstoqueService = null;
			Logger.getGlobal().info("Finalizado. Processo Concluído!");
		}
		catch(Exception ex)
		{
			if (transaction!=null && transaction.isActive())
				transaction.rollback();
			
			ex.printStackTrace();
			throw ex;
						
		}
	}
	
	public Boolean verificarMovimentacaoExiste(EntityManager manager, String numeroDocumento, EmpresaFisica empresaFisica, Date dataMovimentacao, TipoMovimentacaoEstoque tipoMovimentacaEstoque) throws Exception
	{
		MovimentacaoEstoque movimentacaoEstoqueFiltro = new MovimentacaoEstoque();
		
		movimentacaoEstoqueFiltro.setNumeroDocumento(numeroDocumento);
		movimentacaoEstoqueFiltro.setEmpresaFisica(empresaFisica);
		movimentacaoEstoqueFiltro.setDataMovimentacao(dataMovimentacao);
		movimentacaoEstoqueFiltro.setTipoMovimentacaoEstoque(tipoMovimentacaEstoque);
		
		return new MovimentacaoEstoqueService().listarPorObjetoFiltro(manager, movimentacaoEstoqueFiltro, null, null, 1).size() == 0;
		
	}

	/*
	 * Baixa das Tabelas Gerais
	 */
	public void baixarTabelasGerais(Integer idEmpresaFisica, String data, Integer sequencial) throws Exception
	{
		boolean processOk = false;
		
    	Date dataInicioProcesso = new Date();	
    	String numeroEmpresaFisica = String.format ("%02d", idEmpresaFisica);
    	
    	if(data.equals("0"))
    		data = new SimpleDateFormat("yyyyMMdd").format(new Date());
		
		try
		{ 
			Logger.getGlobal().info("Initializing Sync to F" + numeroEmpresaFisica);
			
			String nomeArquivoDump = "dump_TG_" + numeroEmpresaFisica + "_" + data + "_" + sequencial.toString() + ".NEPALDUMP";
			
			Logger.getGlobal().info("Initializing download of " + nomeArquivoDump);
			
			new FtpUtil().downloadFile(nomeArquivoDump.replace("NEPALDUMP", "rar"), DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump.replace("NEPALDUMP", "rar"), DsvConstante.getParametrosSistema().get("INTEGRACAO_IP_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_USUARIO_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_FTP"));
			
			Logger.getGlobal().info("Downloaded. Extracting."); 
			
			IgnUtil.descompactarArquivo(DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump.replace("NEPALDUMP", "rar"), DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump.split("\\.")[0]);
			
			Logger.getGlobal().info("Extract complete. Initializing Restore");
			
			new UtilDatabase().restoreBackupFile(DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump.split("\\.")[0] + "/" + nomeArquivoDump, "nepalignloja" + numeroEmpresaFisica);
			
			Logger.getGlobal().info("Restore complete. Finish!");	
			
			processOk = true;
		}
		catch(Exception ex)
		{
			processOk = false;
			
			ex.printStackTrace();
		}
		finally
		{
    		Date dataFimProcesso = new Date();
    		Date dataTerminoProcesso = new Date();
    		dataTerminoProcesso.setTime(dataFimProcesso.getTime() - dataInicioProcesso.getTime());
    		
			if(processOk)
			{
				/*new MailUtil().sendMail(DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_ORIGEM_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_EMAIL_ORIGEM_NOTIFICACOES"), "Sistema Integrador Nepal", DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_DESTINO_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("SMPT_EMAIL_ORIGEM_NOTIFICACOES"), "integração Nepal. Baixa das TG's da Loja " + numeroEmpresaFisica + " no Escritório", 
        				"Olá, a integração de baixa das TG's da Loja " + numeroEmpresaFisica + " no Escritório foi concluída.<br><br>Iniciado em " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataInicioProcesso) + " e finalizado em " + 
        				new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataFimProcesso) + " o processo durou " + new SimpleDateFormat("mm:ss").format(dataTerminoProcesso) + " (minutos:segundos).");*/	
			}
			else
			{
				/*new MailUtil().sendMail(DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_ORIGEM_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_EMAIL_ORIGEM_NOTIFICACOES"), "Sistema Integrador Nepal", DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_DESTINO_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("SMPT_EMAIL_ORIGEM_NOTIFICACOES"), "integração Nepal. Baixa das TG's da Loja " + numeroEmpresaFisica + " no Escritório FALHOU!!!", 
        				"Olá, a integração de baixa das TG's da Loja " + numeroEmpresaFisica + " no Escritório não foi concluída.<br><br>Iniciado em " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataInicioProcesso) + " e finalizado em " + 
        				new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataFimProcesso) + " o processo durou " + new SimpleDateFormat("mm:ss").format(dataTerminoProcesso) + " (minutos:segundos) antes de ser abortado!!!!" + 
        				"<br><br>ERRO : " + processException.toString());*/
			}
		}
	}
	
	public void callStoredProcedure(String procedureName) throws Exception
	{
		Connection conn = ConexaoUtil.getConexaoPadrao();
		try
		{
			CallableStatement callableStatement = conn.prepareCall("{ call " + procedureName + "() }");
			callableStatement.execute();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			if(conn != null && !conn.isClosed())
				conn.close();
		}
	}

	public void sincronizarDadosLoja(final Integer idEmpresaFisica, final Date dataInicial, final Date dataFinal) throws Exception
	{
		if(Util. isMovSyncRunning(String.format("%02d", idEmpresaFisica)))
			throw new Exception("já existe uma integração rodando!");

		ThreadMovimento thread = new ThreadMovimento(new RunnableMovimentoExportar(idEmpresaFisica, dataInicial, dataFinal));
		thread.setName("THREAD-SYNC-MOV-".concat(String.format("%02d", idEmpresaFisica)));
		thread.setPriority(Thread.MAX_PRIORITY);
		
		thread.start();
	}
}