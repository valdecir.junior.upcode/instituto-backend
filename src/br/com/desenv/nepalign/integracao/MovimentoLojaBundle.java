package br.com.desenv.nepalign.integracao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.integracao.dto.CaixaDiaDTO;
import br.com.desenv.nepalign.integracao.dto.ChequeRecebidoDTO;
import br.com.desenv.nepalign.integracao.dto.DataPackage;
import br.com.desenv.nepalign.integracao.dto.ItemNotaFiscalDTO;
import br.com.desenv.nepalign.integracao.dto.MovimentoCaixaDiaDTO;
import br.com.desenv.nepalign.integracao.dto.MovimentoParcelaDuplicataDTO;
import br.com.desenv.nepalign.integracao.dto.NotaFiscalDTO;
import br.com.desenv.nepalign.integracao.dto.PagamentoCartaoDTO;
import br.com.desenv.nepalign.integracao.dto.PagamentoPedidoVendaDTO;
import br.com.desenv.nepalign.integracao.dto.ValorFechamentoCaixaDiaDTO;
import br.com.desenv.nepalign.model.CaixaDia;
import br.com.desenv.nepalign.model.MovimentoCaixadia;
import br.com.desenv.nepalign.model.MovimentoParcelaDuplicata;
import br.com.desenv.nepalign.model.PagamentoPedidoVenda;
import br.com.desenv.nepalign.model.ValorFechamentoCaixaDia;
import flex.messaging.io.ArrayCollection;
import flex.messaging.io.amf.client.AMFConnection;
import flex.messaging.io.amf.client.exceptions.ClientStatusException;

public class MovimentoLojaBundle implements DataBundle
{
	public HashMap<Integer, MovimentoParcelaDuplicata> listaMovimentoParcelaDuplicata = new HashMap<Integer, MovimentoParcelaDuplicata>();
	public HashMap<Integer, ChequeRecebidoDTO> listaChequeRecebido = new HashMap<Integer, ChequeRecebidoDTO>();
	public HashMap<Integer, CaixaDia> listaCaixaIntegrado = new HashMap<Integer, CaixaDia>();
	
	@SuppressWarnings("unchecked")
	public void importar(AMFConnection con, EntityManager manager, EntityTransaction transaction, String idEmpresaFisica, Date dataInicial, Date dataFinal) throws Exception
	{
		final String databaseName = Util.getDatabaseName(idEmpresaFisica);
				
		try
		{
			do
			{
				CaixaDia caixaDiaFiltro = new CaixaDia(); 
				caixaDiaFiltro.setDataFechamento(dataInicial);

				ArrayCollection listaCaixaDia = (ArrayCollection) con.call(Util.SERVICE_GET_PB, caixaDiaFiltro);
				
				caixaDiaFiltro = new CaixaDia();
				caixaDiaFiltro.setDataAbertura(dataInicial);
				
				for (Object caixaDiaBuffer : ((ArrayCollection) con.call(Util.SERVICE_GET_PB, caixaDiaFiltro)))
				{
					boolean caixaListado = false;
					
					for (Object caixaDiaBuffer_ : listaCaixaDia)
					{
						if(((CaixaDia) caixaDiaBuffer_).getId().intValue() == ((CaixaDia)caixaDiaBuffer).getId().intValue())
							caixaListado = true;	
					}
					
					if(!caixaListado)
						listaCaixaDia.add(caixaDiaBuffer);
				}
				
				for(Object caixa : listaCaixaDia)
				{					
					CaixaDia caixaDia = ((CaixaDia) caixa).clone();
					
					if(listaCaixaIntegrado.containsKey(caixaDia.getId().intValue()))
						continue;
					
					if(!Util.pedidoVendaBundle.dataPedidosIntegrados.contains(Integer.parseInt(new SimpleDateFormat("yyyyMMdd").format(caixaDia.getDataAbertura()))))
					{
						Util.pedidoVendaBundle.importar(con, manager, transaction, idEmpresaFisica, caixaDia.getDataAbertura(), caixaDia.getDataAbertura());
						transaction.commit();
						transaction.begin();
					}
					
					if(!Util.duplicataBundle.dataDuplicatasIntegradas.contains(Integer.parseInt(new SimpleDateFormat("yyyyMMdd").format(caixaDia.getDataAbertura()))))
					{
						Util.duplicataBundle.importar(con, manager, transaction, idEmpresaFisica, caixaDia.getDataAbertura(), caixaDia.getDataAbertura());
						transaction.commit();
						transaction.begin();
					}
					
					manager.createNativeQuery("DELETE FROM " + databaseName + ".pagamentoPedidoVenda WHERE idCaixaDia = " + caixaDia.getId().intValue() + ";").executeUpdate();
					manager.createNativeQuery("DELETE FROM " + databaseName + ".movimentoParcelaDuplicata WHERE dataMovimento BETWEEN '" + new SimpleDateFormat("yyyy-MM-dd").format(caixaDia.getDataAbertura()) + " 00:00:00' AND '" + new SimpleDateFormat("yyyy-MM-dd").format(caixaDia.getDataAbertura()) + " 23:59:59';").executeUpdate();
					manager.createNativeQuery("DELETE FROM " + databaseName + ".movimentoCaixaDia WHERE idCaixaDia = " + caixaDia.getId().intValue() + ";").executeUpdate();
					manager.createNativeQuery("DELETE FROM " + databaseName + ".valorFechamentoCaixaDia WHERE idCaixaDia = " + caixaDia.getId().intValue() + ";").executeUpdate();
					manager.createNativeQuery("DELETE FROM " + databaseName + ".caixaDia WHERE idCaixaDia = " + caixaDia.getId().intValue() + ";").executeUpdate();
					
					Util.vendedorBundle.importarUsuario(con, manager, transaction, idEmpresaFisica, caixaDia.getUsuario());
					
					manager.merge(Util.<CaixaDiaDTO> copy(CaixaDiaDTO.class, caixaDia));
					
					System.out.println(">Importando Movimento Caixa! Abertura [" + new SimpleDateFormat("dd/MM/yyyy").format(caixaDia.getDataAbertura()) + "] Fechamento [" + (caixaDia.getDataFechamento() == null ? "ABERTO!" : new SimpleDateFormat("dd/MM/yyyy").format(caixaDia.getDataFechamento())) + "]");
					
					ValorFechamentoCaixaDia valorFechamentoCaixaDiaFiltro = new ValorFechamentoCaixaDia();
					valorFechamentoCaixaDiaFiltro.setCaixadia(caixaDia);
					
					ArrayCollection listaValorFechamentoCaixaDia = (ArrayCollection) con.call(Util.SERVICE_GET_PB_VALUES, valorFechamentoCaixaDiaFiltro);
					
					System.out.println(">>" + listaValorFechamentoCaixaDia.size() + " Valores Informados...");
					
					if(!Util.DEBUG_MODE)
						System.out.print(">>Importando Valores 		1/" + listaValorFechamentoCaixaDia.size());
					
					int currentIndex = 0x00;
					
					for(Object valorFechamento : listaValorFechamentoCaixaDia)
					{
						currentIndex++;
					
						if(!Util.DEBUG_MODE)
						{
				            for (int j = 0; j < (listaValorFechamentoCaixaDia.size() + "").length() + 0x01; j++) 
				                System.out.print('\b');
				            for (int j = 0; j < (currentIndex + "").length(); j++)
				                System.out.print('\b');
				            
				            System.out.print(currentIndex + "/" + listaValorFechamentoCaixaDia.size());
						}
					
						manager.merge(Util.<ValorFechamentoCaixaDiaDTO> copy(ValorFechamentoCaixaDiaDTO.class, ((ValorFechamentoCaixaDia) valorFechamento).clone()));
					}
					
					if(!Util.DEBUG_MODE)
						System.out.println();
					
					System.out.println(">>>Valores importados!");
					
					PagamentoPedidoVenda pagamentoPedidoVendaFiltro = new PagamentoPedidoVenda();
					pagamentoPedidoVendaFiltro.setCaixaDia(caixaDia);
					
					System.out.println(">Importando pagamentos!");
					
					ArrayCollection listaPagamentoPedidoVenda = (ArrayCollection) con.call(Util.SERVICE_GET_PB_SALE_PAYMENT, pagamentoPedidoVendaFiltro);
					 
					System.out.println(">>" + listaPagamentoPedidoVenda.size() + " pagamentos encontrados!");
					
					currentIndex = 0x00;
					
					if(!Util.DEBUG_MODE)
						System.out.print(">>Importando Pagamentos 		1/" + listaPagamentoPedidoVenda.size());
					
					for(Object pagamento : listaPagamentoPedidoVenda)
					{
						PagamentoPedidoVenda pagamentoPedidoVenda = ((PagamentoPedidoVenda) pagamento).clone();
						
						currentIndex++;
						
						if(!Util.DEBUG_MODE)
						{
				            for (int j = 0x00; j < (listaPagamentoPedidoVenda.size() + "").length() + 0x01; j++) 
				                System.out.print('\b');
				            for (int j = 0x00; j < (currentIndex + "").length(); j++)
				                System.out.print('\b');	
						}
						
						if(!Util.DEBUG_MODE)
							System.out.print(currentIndex + "/" + listaPagamentoPedidoVenda.size());

						Util.chequeRecebidoBundle.importar(con, manager, transaction, idEmpresaFisica, pagamentoPedidoVenda.getChequeRecebido());
						
						manager.merge(Util.<PagamentoPedidoVendaDTO> copy(PagamentoPedidoVendaDTO.class, ((PagamentoPedidoVenda) pagamento).clone()));
					}
					
					if(!Util.DEBUG_MODE)
						System.out.println();
					
					System.out.println(">>>incluídos " + listaPagamentoPedidoVenda.size() + " Pagamentos");
					
					System.out.println(">Importando Parcela Movimento Duplicata!");
					
					MovimentoParcelaDuplicata movimentoParcelaDuplicataFiltro = new MovimentoParcelaDuplicata();
					movimentoParcelaDuplicataFiltro.setDataMovimento(new SimpleDateFormat("dd/MM/yyyy").parse(new SimpleDateFormat("dd/MM/yyyy").format(caixaDia.getDataAbertura())));

					ArrayCollection listMovimentoParcelaDuplicata = (ArrayCollection) con.call(Util.SERVICE_GET_PB_TERM_SALE_ACTIVITY, movimentoParcelaDuplicataFiltro);
					
					System.out.println(">>" + listMovimentoParcelaDuplicata.size() + " Movimento Parcela Duplicata Encontrados.");
					
					currentIndex = 0x00;
					
					if(!Util.DEBUG_MODE)
						System.out.print(">>Importando Movimento Parcela Duplicata 		1/" + listMovimentoParcelaDuplicata.size());
					
					for(Object movimento : listMovimentoParcelaDuplicata)
					{
						MovimentoParcelaDuplicata  movimentoParcelaDuplicata = ((MovimentoParcelaDuplicata) movimento).clone();
						
						currentIndex++;
						
						if(!Util.DEBUG_MODE)
						{
				            for (int j = 0x00; j < (listMovimentoParcelaDuplicata.size() + "").length() + 0x01; j++) 
				                System.out.print('\b');
				            for (int j = 0x00; j < (currentIndex + "").length(); j++)
				                System.out.print('\b');
				             
				            System.out.print(currentIndex + "/" + listMovimentoParcelaDuplicata.size());	
						}
						
						Util.chequeRecebidoBundle.importar(con, manager, transaction, idEmpresaFisica, movimentoParcelaDuplicata.getChequeRecebido());
						Util.clienteBundle.importar(con, manager, transaction, idEmpresaFisica, movimentoParcelaDuplicata.getCliente());
						
						manager.merge(Util.<MovimentoParcelaDuplicataDTO> copy(MovimentoParcelaDuplicataDTO.class, ((MovimentoParcelaDuplicata) movimento).clone()));
					}
					
					if(!Util.DEBUG_MODE)
						System.out.println();
					
					System.out.println(">>>incluídos " + listMovimentoParcelaDuplicata.size() + " Movimento Parcela Duplicata");
					
					System.out.println(">Importando Movimento Caixa Dia!");
					
					MovimentoCaixadia movimentoCaixadiaFiltro = new MovimentoCaixadia();
					movimentoCaixadiaFiltro.setCaixaDia(caixaDia);
					
					ArrayCollection listaMovimentoCaixaDia = (ArrayCollection) con.call(Util.SERVICE_GET_PB_ACTIVITY, movimentoCaixadiaFiltro);
					
					System.out.println(">>" + listaMovimentoCaixaDia.size() + " Movimento Caixa Dia Encontrados.");

					currentIndex = 0x00;
					
					if(!Util.DEBUG_MODE)
						System.out.print(">>Importando Movimento Caixa Dia  		1/" + listaMovimentoCaixaDia.size());
					
					for(Object movimento : listaMovimentoCaixaDia)
					{
						MovimentoCaixadia movimentoCaixadia = ((MovimentoCaixadia) movimento).clone();
						
						currentIndex++;
						
						if(!Util.DEBUG_MODE)
						{
				            for (int j = 0x00; j < (listaMovimentoCaixaDia.size() + "").length() + 0x01; j++) 
				                System.out.print('\b');
				            for (int j = 0x00; j < (currentIndex + "").length(); j++)
				                System.out.print('\b');
				            
				            System.out.print(currentIndex + "/" + listaMovimentoCaixaDia.size());	
						}
						
						Util.clienteBundle.importar(con, manager, transaction, idEmpresaFisica, movimentoCaixadia.getCliente());
						
						manager.merge(Util.<MovimentoCaixaDiaDTO> copy(MovimentoCaixaDiaDTO.class, ((MovimentoCaixadia) movimento).clone()));
					}					
					
					if(!Util.DEBUG_MODE)
						System.out.println();
					
					System.out.println(">>>incluídos " + listaMovimentoCaixaDia.size() + " Movimento Caixa Dia");
					 
					transaction.commit();
					transaction.begin();
					
					listaCaixaIntegrado.put(caixaDia.getId().intValue(), caixaDia);
				}
				
				dataInicial = Util.addDay(dataInicial, 0x01);
			}
			while(dataInicial.before(dataFinal) || dataInicial.equals(dataFinal));
		}
		catch(ClientStatusException cExc)
		{
			cExc.printStackTrace();
			throw cExc;
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<Date> listaDatasExportacao(final EntityManager manager, final Date dataInicial, final Date dataFinal)
	{
		final String caixaDiaFechadosQuery = "SELECT `caixad`.`dataAbertura` FROM `caixaDia` `caixad` WHERE (`caixad`.`dataFechamento` BETWEEN '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' AND '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59' OR " + 
		" `caixad`.`dataAbertura` BETWEEN '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' AND '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59') ";
		 
		return manager.createNativeQuery(caixaDiaFechadosQuery).getResultList();
		/*try
		{
			return Arrays.asList(IgnUtil.dateFormat.parse("26/06/2017"));	
		}
		catch(Exception ex)
		{
			return new ArrayList<>();
		}*/
	}
	
	@SuppressWarnings("unchecked")
	public DataPackage exportar(final EntityManager manager, final DataPackage dataPackage, final Date dataInicial, final Date dataFinal) throws Exception
	{
		for(final Date dataExportacao : listaDatasExportacao(manager, dataInicial, dataFinal))
		{
			final String caixaDiaQuery = "SELECT * FROM `caixaDia` `caixad` WHERE `caixad`.`dataFechamento` BETWEEN '" + IgnUtil.dateFormatSql.format(dataExportacao) + " 00:00:00' AND '" + IgnUtil.dateFormatSql.format(dataExportacao) + " 23:59:59'";
			
			final String movimentoCaixaDiaQuery = " SELECT `mov`.* FROM `movimentoCaixaDia` `mov` " +
			" INNER JOIN `caixaDia` `caixad` ON `caixad`.`idCaixaDia` = `mov`.`idCaixaDia` " + 
			" WHERE `caixad`.`dataAbertura` BETWEEN '" +  IgnUtil.dateFormatSql.format(dataExportacao) + " 00:00:00' AND '" + IgnUtil.dateFormatSql.format(dataExportacao) + " 23:59:59'";
			
			final String movimentoDuplicataParcelaQuery = "SELECT * FROM `movimentoparceladuplicata` WHERE `dataMovimento` BETWEEN '" + IgnUtil.dateFormatSql.format(dataExportacao) + " 00:00:00' AND '" + IgnUtil.dateFormatSql.format(dataExportacao) + " 23:59:59';";
			
			final String valorFechamentoCaixaDiaQuery = " SELECT `val`.* FROM `valorFechamentoCaixaDia` `val` " +
			" INNER JOIN `caixaDia` `caixad` ON `caixad`.`idCaixaDia` = `val`.`idCaixaDia` " +
			" WHERE `caixad`.`dataAbertura` BETWEEN '" + IgnUtil.dateFormatSql.format(dataExportacao) + " 00:00:00' AND '" + IgnUtil.dateFormatSql.format(dataExportacao) + " 23:59:59'";
			
			final String pagamentoPedidoVendaQuery = " SELECT `pag`.* FROM `pagamentopedidovenda` `pag` " + 
			" INNER JOIN `caixaDia` `caixa` ON `caixa`.`idCaixaDia` = `pag`.`idCaixaDia` "+
			" WHERE `caixa`.`dataAbertura` BETWEEN '" + IgnUtil.dateFormatSql.format(dataExportacao) + " 00:00:00' AND '" + IgnUtil.dateFormatSql.format(dataExportacao) + " 23:59:59'";
			
			final String pagamentoCartaoQuery = " SELECT `pag`.* FROM `pagamentoCartao` `pag` " + 
			" INNER JOIN `caixaDia` `caixa` ON `caixa`.`idCaixaDia` = `pag`.`idCaixaDia` "+
			" WHERE `caixa`.`dataAbertura` BETWEEN '" + IgnUtil.dateFormatSql.format(dataExportacao) + " 00:00:00' AND '" + IgnUtil.dateFormatSql.format(dataExportacao) + " 23:59:59'; ";
			
			final String notaFiscalQuery = " SELECT `notaFiscal`.* FROM `notaFiscal` WHERE `notaFiscal`.`dataEmissao` BETWEEN '" + IgnUtil.dateFormatSql.format(dataExportacao) + " 00:00:00' AND '" + IgnUtil.dateFormatSql.format(dataExportacao) + " 23:59:59'; ";
			
			final String itemNotaFiscalQuery = "SELECT `itemNotaFiscal`.* FROM `itemNotaFiscal` WHERE `idNotaFiscal` IN (" +
			" SELECT `notaFiscal`.`idNotaFiscal` FROM `notaFiscal` WHERE `notaFiscal`.`dataEmissao` BETWEEN '" + IgnUtil.dateFormatSql.format(dataExportacao) + " 00:00:00' AND '" + IgnUtil.dateFormatSql.format(dataExportacao) + " 23:59:59');";
			
			/*System.out.println(caixaDiaQuery);
			System.out.println(movimentoCaixaDiaQuery);
			System.out.println(movimentoDuplicataParcelaQuery);
			System.out.println(valorFechamentoCaixaDiaQuery);
			System.out.println(pagamentoPedidoVendaQuery);
			System.out.println(pagamentoCartaoQuery);
			System.out.println(notaFiscalQuery);
			System.out.println(itemNotaFiscalQuery);*/
			
			dataPackage. <CaixaDiaDTO> put(CaixaDiaDTO.class, (List<CaixaDiaDTO>) manager.createNativeQuery(caixaDiaQuery, CaixaDiaDTO.class).getResultList());
			dataPackage. <MovimentoCaixaDiaDTO> put(MovimentoCaixaDiaDTO.class, (List<MovimentoCaixaDiaDTO>) manager.createNativeQuery(movimentoCaixaDiaQuery, MovimentoCaixaDiaDTO.class).getResultList());		
			dataPackage. <MovimentoParcelaDuplicataDTO> put(MovimentoParcelaDuplicataDTO.class, (List<MovimentoParcelaDuplicataDTO>) manager.createNativeQuery(movimentoDuplicataParcelaQuery, MovimentoParcelaDuplicataDTO.class).getResultList());
			dataPackage. <PagamentoCartaoDTO> put(PagamentoCartaoDTO.class, (List<PagamentoCartaoDTO>) manager.createNativeQuery(pagamentoCartaoQuery, PagamentoCartaoDTO.class).getResultList());
			dataPackage. <PagamentoPedidoVendaDTO> put(PagamentoPedidoVendaDTO.class, (List<PagamentoPedidoVendaDTO>) manager.createNativeQuery(pagamentoPedidoVendaQuery, PagamentoPedidoVendaDTO.class).getResultList());
			dataPackage. <ValorFechamentoCaixaDiaDTO> put(ValorFechamentoCaixaDiaDTO.class, (List<ValorFechamentoCaixaDiaDTO>) manager.createNativeQuery(valorFechamentoCaixaDiaQuery, ValorFechamentoCaixaDiaDTO.class).getResultList());
			 
			dataPackage. <NotaFiscalDTO> put(NotaFiscalDTO.class, (List<NotaFiscalDTO>) manager.createNativeQuery(notaFiscalQuery, NotaFiscalDTO.class).getResultList());
			dataPackage. <ItemNotaFiscalDTO> put(ItemNotaFiscalDTO.class, (List<ItemNotaFiscalDTO>) manager.createNativeQuery(itemNotaFiscalQuery, NotaFiscalDTO.class).getResultList());	
		}		
		return dataPackage;
	}
}