package br.com.desenv.nepalign.integracao;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.desenv.nepal.catalog.xmlData.ReturnData;
import com.desenv.nepal.integracao.util.DesenvXMLUtil;
import com.desenv.nepal.order.wsdl.Order;
import com.desenv.nepal.order.wsdl.OrderSoap;
import com.desenv.nepal.order.xmlData.OrderData;
import com.desenv.nepal.order.xmlData.OrderDataRet;
import com.desenv.nepal.order.xmlData.OrderItemsRet.OrderItemRet;

import br.com.desenv.nepalign.model.Cor;
import br.com.desenv.nepalign.model.EstoqueProduto;
import br.com.desenv.nepalign.model.ItemPedidoVenda;
import br.com.desenv.nepalign.model.OrdemProduto;
import br.com.desenv.nepalign.model.PedidoVenda;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.model.StatusItemPedido;
import br.com.desenv.nepalign.model.Tamanho;
import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.nepalign.service.AlertaPedidoVendaService;
import br.com.desenv.nepalign.service.CorService;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.EstoqueProdutoService;
import br.com.desenv.nepalign.service.ItemPedidoVendaService;
import br.com.desenv.nepalign.service.LogAlertaService;
import br.com.desenv.nepalign.service.LogOperacaoNepalService;
import br.com.desenv.nepalign.service.OrdemProdutoService;
import br.com.desenv.nepalign.service.PedidoVendaService;
import br.com.desenv.nepalign.service.ProdutoService;
import br.com.desenv.nepalign.service.TamanhoService;
import br.com.desenv.nepalign.util.DsvConstante;

public class IntegracaoItemPedidoVenda
{
	private AlertaPedidoVendaService alertaPedidoVendaService = null;
	private ItemPedidoVendaService itemPedidoVendaService = null;
	private EstoqueProdutoService estoqueProdutoService = null;
	private EmpresaFisicaService empresaFisicaService = null;
	private OrdemProdutoService ordemProdutoService = null;
	private PedidoVendaService pedidoVendaService = null;
	private LogAlertaService logAlertaService = null;
	private ProdutoService produtoService  = null;
	private TamanhoService tamanhoService = null;
	private CorService corService = null;
	
	private Order order = null;
	private OrderSoap orderSoap = null;
	
	private PedidoVenda pedidoPai;
	
	private Usuario usuarioIntegracao;
	
	private String webServiceUser = DsvConstante.getInstanceParametrosSistema().getUsuarioWebServiceOrder();
	private String webServicePassword = DsvConstante.getInstanceParametrosSistema().getSenhaWebServiceOrder();
	
	public IntegracaoItemPedidoVenda() throws Exception
	{
		this.order = new Order();
		this.orderSoap = this.order.getOrderSoap();
		this.corService = new CorService();
		this.produtoService = new ProdutoService();
		this.tamanhoService = new TamanhoService();
		this.logAlertaService = new LogAlertaService();
		this.pedidoVendaService = new PedidoVendaService();
		this.ordemProdutoService = new OrdemProdutoService();
		this.empresaFisicaService = new EmpresaFisicaService();
		this.estoqueProdutoService = new EstoqueProdutoService();
		this.itemPedidoVendaService = new ItemPedidoVendaService();
		this.alertaPedidoVendaService = new AlertaPedidoVendaService();
	}
	
	public void integrarItemPedidoVendaPorPedidoVenda(OrderData order) throws Exception
	{
		try
		{
			this.montarPedidoPai(order.getIdOrder());
		
			String xmlToSend = (String) new DesenvXMLUtil().objectToXML(order, OrderData.class);
			String xmlOrderRet = this.orderSoap.getOrder(xmlToSend, this.webServiceUser, this.webServicePassword);
			OrderDataRet orderDataRet = (OrderDataRet)new DesenvXMLUtil().XMLToObject(new StringBuffer(xmlOrderRet), OrderDataRet.class);
			String xmlReceived = this.orderSoap.getOrderItems(xmlToSend, this.webServiceUser, this.webServicePassword);
			
			try
			{
				ReturnData returnData = new DesenvXMLUtil().XMLToObjectReturnData(new StringBuffer(xmlReceived));
				throw new Exception(returnData.getDsErr() + " - Item do pedido venda número : " + order.getIdOrder());
			}
			catch(Exception ex)
			{
				if(ex.getMessage().contains("- Item do pedido venda número"))
				{
					new LogOperacaoNepalService().logaAe(3, DsvConstante.getInstanceParametrosSistema().getTipoOperacaoImportPedidoVenda(), ex.getMessage());
					throw ex;
				}
			}
			ArrayList<OrderItemRet> receivedOrderItems = this.convertXMLToArrayObject(xmlReceived);
			
			for(int i = 0; i < receivedOrderItems.size(); i++)
			{
				for(int itemIndex = 1; itemIndex <= Integer.parseInt(receivedOrderItems.get(i).getNuQuantity()); itemIndex++)
				{
					ItemPedidoVenda novoItemPedidoVenda = this.montarObjetoItemPedidoVenda(receivedOrderItems.get(i), (itemIndex + i));
					
					novoItemPedidoVenda.setQuantidade(1.0);
					this.itemPedidoVendaService.salvar(novoItemPedidoVenda);
					
					if(orderDataRet.getIdOrderStatus() == DsvConstante.getInstanceParametrosSistema().getPedidoVenda_statusParaGerarAlertaLV())
					{
						this.alertaPedidoVendaService.gerarAlerta(novoItemPedidoVenda, 10);
					}
				}
			}
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
	
	public ArrayList<OrderItemRet> convertXMLToArrayObject(String XML) throws Exception
	{
		try
		{
			XML = XML.replace("\n", "").replace("\r", "").trim();
	        DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
	        DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
	        Document doc = docBuilder.parse(new InputSource(new StringReader(XML)));
	        doc.getDocumentElement().normalize();
	        
	        ArrayList<OrderItemRet> arrayListDeOrderItemRet = new ArrayList<OrderItemRet>();
	        
	        NodeList nodeList = doc.getElementsByTagName("ReceiptItem");
	        
	        for(int i = 0; i < nodeList.getLength(); i++)
	        {
	        	if(nodeList.item(i).getChildNodes().getLength() > 0)
	        	{
	        		NodeList childNodeList = nodeList.item(i).getChildNodes();
	        		OrderItemRet orderItemRet = new OrderItemRet();
	        		for(int x = 0; x < childNodeList.getLength(); x++)
	        		{
	        			Node getterNode = childNodeList.item(x);
	        			if(!getterNode.getNodeName().contains("#text") && !getterNode.getNodeName().equals(""))
	        			{
		        			if(getterNode.getNodeName().equals("IdOrder"))
		        				orderItemRet.setIdOrder(Integer.parseInt(getterNode.getFirstChild().getNodeValue()));
		        			if(getterNode.getNodeName().equals("IdProduct"))
		        				orderItemRet.setIdProduct(getterNode.getFirstChild().getNodeValue());
		        			if(getterNode.getNodeName().equals("DsAttrib"))
		        				orderItemRet.setDsAttrib(Integer.parseInt(getterNode.getFirstChild().getNodeValue()));
		        			if(getterNode.getNodeName().equals("NuQuantity"))
		        				orderItemRet.setNuQuantity(getterNode.getFirstChild().getNodeValue());
		        			if(getterNode.getNodeName().equals("VlOriginalSalePrice"))
		        				orderItemRet.setVlOriginalSalePrice(Integer.parseInt(getterNode.getFirstChild().getNodeValue()));
		        			if(getterNode.getNodeName().equals("VlFinalSalePrice"))
		        				orderItemRet.setVlFinalSalePrice(Integer.parseInt(getterNode.getFirstChild().getNodeValue()));
		        			if(getterNode.getNodeName().equals("FgGift"))
		        				orderItemRet.setFgGift(Integer.parseInt(getterNode.getFirstChild().getNodeValue()));
	        			}
	        		
	        		}
        			arrayListDeOrderItemRet.add(orderItemRet);
	        	}
	        }
	        
			return arrayListDeOrderItemRet;
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
	

	
	public void montarPedidoPai(int idOrder) throws Exception
	{
		try
		{
			PedidoVenda pedidoVenda = new PedidoVenda();
			pedidoVenda.setCodigoExterno(idOrder);
			List<PedidoVenda> resultadoPedidoVenda = this.pedidoVendaService.listarPorObjetoFiltro(pedidoVenda);
			
			if(resultadoPedidoVenda.size() > 0)
				pedidoVenda = (PedidoVenda)resultadoPedidoVenda.get(0);
			else
			{
				throw new Exception("PedidoVenda não encontrado para código externo = " + idOrder);
			}
			this.pedidoPai = pedidoVenda;
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
	
	public ItemPedidoVenda montarObjetoItemPedidoVenda(OrderItemRet orderItem, int sequencia) throws Exception
	{
		try
		{
			ItemPedidoVenda it = montarObjetoItemPedidoVenda(orderItem);
			it.setItemSequencial(sequencia);
			return it;
		}
		catch(Exception ex)
		{
			new LogOperacaoNepalService().logaAe(1, DsvConstante.getInstanceParametrosSistema().getTipoOperacaoImportItemPedidoVenda(), ex.getMessage());
			throw ex;
		}
	}
	
	public ItemPedidoVenda montarObjetoItemPedidoVenda(OrderItemRet orderItem) throws Exception
	{
		try
		{
			String[] idProductDetached = orderItem.getIdProduct().split("_");
			
			OrdemProduto ordemProduto = new OrdemProduto();
			ordemProduto.setCodigo(Integer.parseInt(idProductDetached[1]));
			
			List<OrdemProduto> resultOrdemProduto = this.ordemProdutoService.listarPorObjetoFiltro(ordemProduto);
			if(resultOrdemProduto.size() > 0)
			{
				ordemProduto = resultOrdemProduto.get(0);
			}
			else
			{
				throw new Exception("Não foi encontrado nenhuma ordemProduto com código : " + ordemProduto.getCodigo());
			}
		

			Cor cor = new Cor();
			cor.setCodigo(Integer.parseInt(idProductDetached[2]));
			List<Cor> resultCor = this.corService.listarPorObjetoFiltro(cor);
			if(resultCor.size() > 0)
			{
				cor = resultCor.get(0);
			}
			else
			{
				throw new Exception("Não foi encontrado nenhuma cor com código : " + cor.getCodigo());
			}
			
			Tamanho tamanho = new Tamanho();
			tamanho.setCodigo(Integer.parseInt(idProductDetached[3]));
			List<Tamanho> resultTamanho = this.tamanhoService.listarPorObjetoFiltro(tamanho);
			if(resultTamanho.size() > 0)
			{
				tamanho = resultTamanho.get(0);
			}
			else
			{
				throw new Exception("Não foi encontrado nenhum tamanho com código : " + tamanho.getCodigo());
			}
			
			Produto produto = new Produto();
			produto.setCodigoProduto(idProductDetached[0]);
			List<Produto> resultProduto = this.produtoService.listarPorObjetoFiltro(produto);
			if(resultProduto.size() > 0)
			{
				produto = resultProduto.get(0);
			}
			else
			{
				throw new Exception("Não foi encontrado nenhum produto com código : " + produto.getCodigoProduto());
			}
			
			
			EstoqueProduto estoqueProduto = new EstoqueProduto();
			estoqueProduto.setCor(cor);
			estoqueProduto.setTamanho(tamanho);
			estoqueProduto.setOrdemProduto(ordemProduto);
			estoqueProduto.setProduto(produto);
			
			List<EstoqueProduto> resultEstoqueProduto = this.estoqueProdutoService.listarPorObjetoFiltro(estoqueProduto);
			
			if(resultEstoqueProduto.size() > 0)
				estoqueProduto = resultEstoqueProduto.get(0);
			else
			{
				throw new Exception("Não foi encontrado nenhum estoque produto para o produto : " + produto.getId() + 
						" para a ordem " + ordemProduto.getId() + 
						" cor " + cor.getId() + 
						" e tamanho " + tamanho.getId());
			}
			
			ItemPedidoVenda itemPedidoVenda = new ItemPedidoVenda();
			
			itemPedidoVenda.setPedidoVenda(this.pedidoPai);
			itemPedidoVenda.setProduto(produto);
			itemPedidoVenda.setEstoqueProduto(estoqueProduto);
			itemPedidoVenda.setQuantidade(Double.parseDouble(orderItem.getNuQuantity()));
			itemPedidoVenda.setCustoProduto(new Double((double)orderItem.getVlOriginalSalePrice()/100));
			itemPedidoVenda.setPrecoVenda((new Double((double)orderItem.getVlFinalSalePrice())/100));
			itemPedidoVenda.setPrecoTabela(new Double((double) orderItem.getVlOriginalSalePrice())/100);
			
			StatusItemPedido staItemPedido = new StatusItemPedido();
			staItemPedido.setId(DsvConstante.getInstanceParametrosSistema().getItemPedidoVenda_statusInicialItemLojaVirtual());
			
			itemPedidoVenda.setStatusItemPedido(staItemPedido);
			
			itemPedidoVenda.setEntradaSaida("S");
			
			return itemPedidoVenda;
		}
		catch(Exception ex)
		{
			new LogOperacaoNepalService().logaAe(2, DsvConstante.getInstanceParametrosSistema().getTipoOperacaoImportItemPedidoVenda(), ex.getMessage());
			throw ex;
		}
	}
}
