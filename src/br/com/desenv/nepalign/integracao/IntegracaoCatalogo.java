package br.com.desenv.nepalign.integracao;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.transaction.Transaction;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import com.desenv.nepal.catalog.wsdl.Catalog;
import com.desenv.nepal.catalog.xmlData.DeptData;
import com.desenv.nepal.catalog.xmlData.DeptProductData;
import com.desenv.nepal.catalog.xmlData.DeptProductRemoveData;
import com.desenv.nepal.catalog.xmlData.ProductAddUpdateData;
import com.desenv.nepal.catalog.xmlData.ProductBrandData;
import com.desenv.nepal.catalog.xmlData.ProductInventoryUpdateData;
import com.desenv.nepal.catalog.xmlData.ProductSKUAddData;
import com.desenv.nepal.catalog.xmlData.ReturnData;
import com.desenv.nepal.integracao.util.DesenvXMLUtil;
import com.sun.msv.datatype.xsd.datetime.BigDateTimeValueType;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;


import br.com.desenv.nepalign.cache.importadores.ImportadorDepartamento;
import br.com.desenv.nepalign.cache.importadores.ImportadorProduto;
import br.com.desenv.nepalign.cache.importadores.ImportadorProdutoDepartamento;
import br.com.desenv.nepalign.cache.importadores.ImportadorTabelasBasicas;
import br.com.desenv.nepalign.model.Cor;
import br.com.desenv.nepalign.model.Departamento;
import br.com.desenv.nepalign.model.DepartamentoProduto;
import br.com.desenv.nepalign.model.DsvErrosProcessamento;
import br.com.desenv.nepalign.model.Empresa;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.EstoqueProduto;
import br.com.desenv.nepalign.model.LogOperacaoNepal;
import br.com.desenv.nepalign.model.Marca;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.model.SaldoEstoqueProduto;
import br.com.desenv.nepalign.model.SituacaoEnvioLV;
import br.com.desenv.nepalign.persistence.DepartamentoProdutoPersistence;
import br.com.desenv.nepalign.persistence.ProdutoPersistence;
import br.com.desenv.nepalign.persistence.SaldoEstoqueProdutoPersistence;
import br.com.desenv.nepalign.service.CorService;
import br.com.desenv.nepalign.service.DepartamentoProdutoService;
import br.com.desenv.nepalign.service.DepartamentoService;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.EmpresaService;
import br.com.desenv.nepalign.service.EstoqueProdutoService;
import br.com.desenv.nepalign.service.LogOperacaoNepalService;
import br.com.desenv.nepalign.service.MarcaService;
import br.com.desenv.nepalign.service.ProdutoService;
import br.com.desenv.nepalign.service.SaldoEstoqueProdutoService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.util.DsvProcessamentoLVException;
import br.com.desenv.nepalign.util.DsvUtilText;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class IntegracaoCatalogo {


	private ProdutoPersistence produtoDAO;
	private LogOperacaoNepalService logOperacaoLVService;
	private SaldoEstoqueProdutoPersistence estoqueProdutoDAO;			
	private DepartamentoProdutoPersistence produtoDepartamentoDAO;		
	private DesenvXMLUtil xmlUtil;		
	private SituacaoEnvioLV situacaoEnvioLV;
	//recupera dados da empresa para ver Mes e Ano Abertos
	private Vector arquivosFTP;	
	
	public IntegracaoCatalogo() throws Exception
	{
		xmlUtil = new DesenvXMLUtil();
		logOperacaoLVService = new LogOperacaoNepalService();
	}
	
	public static void main(String[] args)
	{
		try
		{
			IntegracaoCatalogo ic = new IntegracaoCatalogo();
			//ic.enviaProdutosAlterados();
			//ic.enviaEstoqueProduto();
			//ic.enviaProdutosEstoquePrecoAlterado(); //<<====
			ic.enviaDepartamentos();
			//ic.testeWebService();
			ic.enviaDepartamentoProduto();
			//ic.verificaProdutosSiteVer2();
			//ic.recuperaListaArquivosFTP();
			//Produto p = new Produto();
			//p.setId(44802);
			//System.out.println(ic.recuperarPrecoProduto(p, null));
			//ic.atualizaSaldoEstoqueProduto();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public Set<DsvErrosProcessamento> autorizarEnvioProdutosAltPrecos(List<Produto> produtos) 
	{//TODO Implement this method
		Produto produto = new Produto();
		EstoqueProdutoService epService = null;
		EstoqueProduto estoqueProduto = null;
		HashSet<DsvErrosProcessamento> erros = new HashSet<DsvErrosProcessamento>();
		DsvErrosProcessamento erro;
		List<EstoqueProduto> listaCodigos;
				
		epService = new EstoqueProdutoService();
		for (int i = 0; i < produtos.size();i++)
		{
			produto = produtos.get(i);
			try
			{
				estoqueProduto = new EstoqueProduto();
				estoqueProduto.setProduto(produto);
				listaCodigos = epService.listarPorObjetoFiltro(estoqueProduto);
				for (int c=0; c<listaCodigos.size();c++)
				{
					estoqueProduto = listaCodigos.get(c);
					
					situacaoEnvioLV = new SituacaoEnvioLV();
					situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVProntoEnvio());
					estoqueProduto.setSituacaoEnvioLV(situacaoEnvioLV);
					epService.atualizar(estoqueProduto);
				}
			}
			catch(Exception e)
			{
				erro = new DsvErrosProcessamento();
				erro.setDescricaoErro(e.getMessage() + " " + e.getLocalizedMessage());
				erro.setObjeto(e);
				erros.add(erro);
			}
		}
		return erros;
	}

	/**
	 * Autoriza o envio dos produtos para o site
	 * Muda o Status dos produtos pra serem enviados e efetua validacao.
	 *
	 * 91020281 01
	 * @ModelReference [platform:/resource/nepalDSL/.springDSL/com/desenv/nepal/service/DsvManageProdutosLVService/autorizarEnvioProdutosNovos%7Ba3a7da5f-63a8-4d45-b2f1-a5cc4270ee50%7D/.properties.swoperation]
	 */
	public Set<DsvErrosProcessamento> autorizarEnvioProdutosNovos(List<Produto> produtos) throws Exception 
	{
		Produto produto = new Produto();
		ProdutoService pService;
		HashSet<DsvErrosProcessamento> erros = new HashSet<DsvErrosProcessamento>();
		DsvErrosProcessamento erro;
		
		pService = new ProdutoService();
		for (int i = 0; i < produtos.size();i++)
		{
			produto = produtos.get(i);
			situacaoEnvioLV = new SituacaoEnvioLV();
			situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVProntoEnvio());			
			produto.setSituacaoEnvioLV(situacaoEnvioLV);
			try
			{		
				pService.atualizar(produto);
			}
			catch(Exception e)
			{
				erro = new DsvErrosProcessamento();
				erro.setDescricaoErro(e.getMessage() + " " + e.getLocalizedMessage());
				erro.setObjeto(e);
				erros.add(erro);
			}
		}
		return erros;
	}

	/**
	 * Autoriza a atualizacao da tabela produto departamento
	 * 
	 * @ModelReference [platform:/resource/nepalDSL/.springDSL/com/desenv/nepal/service/DsvManageProdutosLVService/autorizaEnvioProdutosAltDepartamento%7Bc75b8474-0eee-4f9f-a619-f663207541f4%7D/.properties.swoperation]
	 */
	public Set<DsvErrosProcessamento> autorizaEnvioProdutosAltDepartamento(List<Produto> produtos) 
	{//TODO Implement this method
		return null;
	}
	
	public Set<DsvErrosProcessamento> importarDadosCache() 
	{
		Produto produto = new Produto();
		ProdutoService pService;
		HashSet<DsvErrosProcessamento> erros = new HashSet<DsvErrosProcessamento>();
		DsvErrosProcessamento erro;
		
		try
		{
			new ImportadorDepartamento().init(null);
			new ImportadorTabelasBasicas().init(null);
			new ImportadorProduto().init(null, 1);
			new ImportadorProdutoDepartamento().init(null);
			System.out.println("Finalizado");
		}
		catch(Exception e)
		{
			erro = new DsvErrosProcessamento();
			erro.setDescricaoErro("Ocorreram erros durante a importaÃƒÂ§ÃƒÂ£o. Consulte o log de erros." + " " + e.getMessage() + " " + e.getLocalizedMessage());
			erro.setObjeto(e);
			erros.add(erro);
			e.printStackTrace();
		}
		return erros;
	}
	
	public Set<DsvErrosProcessamento> importarDadosProdutoDepartamentoCache() 
	{
		Produto produto = new Produto();
		ProdutoService pService;
		HashSet<DsvErrosProcessamento> erros = new HashSet<DsvErrosProcessamento>();
		DsvErrosProcessamento erro;
		
		try
		{
			new ImportadorProdutoDepartamento().init(null);
			System.out.println("Finalizado");
		}
		catch(Exception e)
		{
			erro = new DsvErrosProcessamento();
			erro.setDescricaoErro("Ocorreram erros durante a importaÃƒÂ§ÃƒÂ£o. Consulte o log de erros." + " " + e.getMessage() + " " + e.getLocalizedMessage());
			erro.setObjeto(e);
			erros.add(erro);
			e.printStackTrace();
		}
		return erros;
	}
	
	public Set<DsvErrosProcessamento> importarDadosProdutosAlteradosCache() 
	{
		Produto produto = new Produto();
		ProdutoService pService;
		HashSet<DsvErrosProcessamento> erros = new HashSet<DsvErrosProcessamento>();
		DsvErrosProcessamento erro;
		
		try
		{
			new ImportadorProduto().init(null, 2);
			System.out.println("Finalizado");
		}
		catch(Exception e)
		{
			erro = new DsvErrosProcessamento();
			erro.setDescricaoErro("Ocorreram erros durante a importaÃƒÂ§ÃƒÂ£o. Consulte o log de erros." + " " + e.getMessage() + " " + e.getLocalizedMessage());
			erro.setObjeto(e);
			erros.add(erro);
			e.printStackTrace();
		}
		return erros;
	}	
	
	public void enviaProdutosAlterados() throws Exception
	{
		List<Produto> listaProdutos;
		Produto produto = null;
		Marca marca = null;
		ProdutoService produtoService = new ProdutoService();
		EstoqueProduto estoqueProduto= null;
		ProductSKUAddData productSKU = null;
		List <EstoqueProduto> listaEstoqueProduto = null;
		HashMap<String, DsvProdutoAux> listaProductPai = new HashMap<String, DsvProdutoAux>();
		DsvEstoqueProdutoSKU estoqueProdutoSKU = new DsvEstoqueProdutoSKU();
		int estoqueTotal;
		EstoqueProdutoService estoqueProdutoService = new EstoqueProdutoService();
		EmpresaFisicaService empresaService = new EmpresaFisicaService();
		CorService corService = new CorService();
		EmpresaFisica empresa;
		SaldoEstoqueProduto saldoEstoqueProduto;
		Set <DsvEstoqueProdutoSKU> listaEstoqueProdutoSKU;
		String idProduct = null;
		DsvProdutoAux prodAux = null;
		Cor cor=null;
		int contSKU = 0;
		
		try
		{
			
			empresa = empresaService.recuperarPorId(DsvConstante.getInstanceParametrosSistema().getIdEmpresaLV());
			 
			situacaoEnvioLV = new SituacaoEnvioLV();
			situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVProntoEnvio());
			produto = new Produto();
			produto.setSituacaoEnvioLV(situacaoEnvioLV);
			listaProdutos = produtoService.listarPorObjetoFiltro(produto,"idProduto","ASC");
			//listaProdutos = produtoService.findProdutoByField("idProduto", "55634");
			Iterator<Produto> itProdo = listaProdutos.iterator();
			
			arquivosFTP = recuperaListaArquivosFTP();			
			Connection con = ConexaoUtil.getConexaoPadrao();
			EntityManager manager = ConexaoUtil.getEntityManager();
			EntityTransaction transaction = manager.getTransaction();
			transaction.begin();
			Double valorProduto;
			
			
			while (itProdo.hasNext())
			{
				produto = itProdo.next();
				acionaMarcaWebService(produto.getMarca());
				estoqueProduto = new EstoqueProduto();
				estoqueProduto.setProduto(produto);
				listaEstoqueProduto = estoqueProdutoService.listarPorObjetoFiltro(estoqueProduto);
				saldoEstoqueProduto=null;
				for (int contCod=0; contCod<listaEstoqueProduto.size();contCod++)
				{
					estoqueProduto = (EstoqueProduto) listaEstoqueProduto.get(contCod);
					if (estoqueProduto.getCor()!=null &&  estoqueProduto.getCor().getId()!=null && estoqueProduto.getCor().getId()>0)
					{
						saldoEstoqueProduto = recuperarSaldoEstoqueProduto(empresa, estoqueProduto, manager);
						valorProduto = recuperarPrecoProduto(produto, produto.getMarca(), con);
						if (saldoEstoqueProduto!=null)
						{
							idProduct = estoqueProduto.getProduto().getCodigoProduto().toString()+"_"+estoqueProduto.getOrdemProduto().getCodigo().toString()+"_"+estoqueProduto.getCor().getCodigo().toString();
							//verifica se produto possui ao menos algumas imagens no site; Se nao possui gera erro.
							if (existeImagemProdutoFTP(idProduct)==false)
							{
								logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltProduto(), 1, "Produto nÃƒÂ£o possui imagem no site: " + idProduct + " " + produto.getNomeProduto());
								System.out.println("Produto nÃƒÂ£o possui imagem no site: " + idProduct + " " + produto.getNomeProduto());
							}							
							else if (existeDepartamentoProduto(produto)==false)
							{
								logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltProduto(), 1, "Produto deve possuir ao menos um departamento vinculado: " + idProduct + " " + produto.getNomeProduto());
								System.out.println("Produto nÃƒÂ£o possui imagem no site: " + idProduct + " " + produto.getNomeProduto());
							}							
							else
							{						
								if (listaProductPai.get(idProduct)!=null)
								{
									prodAux = listaProductPai.get(idProduct);
								}
								else
								{
									prodAux = new DsvProdutoAux();
								}
								//prodAux.setSaldoEstoqueProduto(SaldoEstoqueProduto);
								prodAux.setValorProduto(valorProduto);
								prodAux.setQuantidadeTotal(saldoEstoqueProduto.getSaldo().intValue()+prodAux.getQuantidadeTotal());
								prodAux.setProduto(produto);
								cor = corService.recuperarPorId(estoqueProduto.getCor().getId()); //TODO: VERIFICAR
								prodAux.setCor(cor);
								listaProductPai.put(idProduct, prodAux);
								System.out.println(contSKU++);								
							}
						}
					}
				}
				//acionaMarcaWebService(produto.getMarca());
			}
			//varre produtos Pai que foram criados e aciona webService
			Iterator<String> itKeysProducts = listaProductPai.keySet().iterator();
			while (itKeysProducts.hasNext())
			{
				idProduct = itKeysProducts.next();
				prodAux = (DsvProdutoAux) listaProductPai.get(idProduct);
				Produto produtoNew = prodAux.getProduto();
				try
				{
					ProductAddUpdateData p = criarProdutoWS(prodAux, idProduct);
					acionaProductWebService(p);
					situacaoEnvioLV = new SituacaoEnvioLV();
					situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVEnvioCompleto());
					produtoNew.setSituacaoEnvioLV(situacaoEnvioLV);
					produtoService.atualizar(produtoNew);
				}
				catch (Exception exPro)
				{
					situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVErroEnvio());
					produtoNew.setSituacaoEnvioLV(situacaoEnvioLV);
					System.out.println(produto.getCodigoProduto()+ " " + exPro.getMessage());
					produtoService.atualizar(produtoNew);
					//throw new DsvProcessamentoLVException("Erro ao salvar produto Pai", exPro);
				}
			}
			System.out.print("Fim");
			if (con!=null)
				con.close();
		}
		catch(Exception e)
		{
			logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltProduto(), 1, e.getMessage());
			e.printStackTrace();
			throw e;
		}
	}
	
	public void enviaProdutosEstoquePrecoAlterado() throws Exception
	{
		List<Produto> listaProdutos;
		Produto produto = null;
		Marca marca = null;
		ProdutoService produtoService = new ProdutoService();
		EstoqueProduto estoqueProduto = null;
		ProductSKUAddData productSKU = null;
		List <EstoqueProduto> listaEstoqueProduto = null;
		HashMap<String, DsvProdutoAux> listaProductPai = new HashMap<String, DsvProdutoAux>();
		DsvEstoqueProdutoSKU estoqueProdutoSKU = new DsvEstoqueProdutoSKU();
		int estoqueTotal;
		EstoqueProdutoService estoqueProdutoService = new EstoqueProdutoService();
		EmpresaFisicaService empresaService = new EmpresaFisicaService();
		EmpresaFisica empresa;
		SaldoEstoqueProduto saldoEstoqueProduto;
		Set <DsvEstoqueProdutoSKU> listaEstoqueProdutoSKU = null;
		String idProduct = null;
		DsvProdutoAux prodAux = null;
		boolean validarFoto = false;
		int contProduto = 0;
		EstoqueProduto criterioEP;
		Vector<Integer> marcasAdicionadas;
		
		try
		{
			Connection con = ConexaoUtil.getConexaoPadrao();
			EntityManager manager = ConexaoUtil.getEntityManager();
			//EntityTransaction transaction = manager.getTransaction();
			//transaction.begin();
			
			marcasAdicionadas = new Vector<Integer>();
			//recupera dados da empresa para ver Mes e Ano Abertos
			empresa = empresaService.recuperarPorId(DsvConstante.getInstanceParametrosSistema().getIdEmpresaLV());
			//recupera fotos
			Hashtable<String, ArrayList<Integer>> refCorArquivos = new Hashtable<String, ArrayList<Integer>>();
			//new DsvUtilImagens().buscaReferenciaFotosCores(DsvConstante.getInstanceParametrosSistema().getCaminhoAbsolutoFotos());
			Hashtable<Integer , ArrayList> refsArquivos = null; 
			//= new DsvUtilImagens().buscaReferenciaFotos(DsvConstante.getInstanceParametrosSistema().getCaminhoAbsolutoFotos());
			arquivosFTP = recuperaListaArquivosFTP();	
			//JtaTransactionManager txManager = ((JtaTransactionManager) ctx.getBean("transactionManager"));
			//TransactionStatus status = txManager.getTransaction(new DefaultTransactionDefinition()); 
			situacaoEnvioLV = new SituacaoEnvioLV();
			situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVProntoEnvio());
			estoqueProduto = new EstoqueProduto();
			estoqueProduto.setSituacaoEnvioLV(situacaoEnvioLV);
			//versao antiga que precisava da situacao do estoque produto. Agora nao precisa mais 11/10/2013 - Loja virtual nao tem mais estoque proprio.
			//listaProdutos = estoqueProdutoService.listarProdutoPorSituacaoEstoque(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVProntoEnvio());
			/////===>>>>>
			situacaoEnvioLV = new SituacaoEnvioLV();
			situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVProntoEnvio());
			produto = new Produto();
			produto.setSituacaoEnvioLV(situacaoEnvioLV);
			listaProdutos = produtoService.listarPorObjetoFiltro(produto,"idProduto","ASC");			
			
			//listaProdutos = produtoService.findProdutoByField("idProduto", "51406");
			
			double valorProduto;
			Iterator<Produto> itProdo = listaProdutos.iterator();
			listaEstoqueProdutoSKU = new HashSet<DsvEstoqueProdutoSKU>();
			while (itProdo.hasNext())
			{
				try
				{
					produto = itProdo.next();
					estoqueTotal = 0;
					criterioEP = new EstoqueProduto();
					criterioEP.setProduto(produto);
					listaEstoqueProduto = estoqueProdutoService.listarPorObjetoFiltro(manager, criterioEP, null, null, null);
					Iterator<EstoqueProduto> itEstoqueProduto = listaEstoqueProduto.iterator();
					while (itEstoqueProduto.hasNext())
					{
						try
						{
							estoqueProduto = itEstoqueProduto.next();
							if (estoqueProduto.getCor()!=null && estoqueProduto.getCor().getId()!=null && estoqueProduto.getCor().getId()!=0)
							{
								try
								{
										idProduct = estoqueProduto.getProduto().getCodigoProduto().toString()+"_"+estoqueProduto.getOrdemProduto().getCodigo()+"_"+estoqueProduto.getCor().getCodigo().toString();
								}
								catch(Exception ex)
								{
									System.out.println("Erro desconhecido");
								}
								saldoEstoqueProduto = null;
								//verifica se produto possui ao menos algumas imagens no site; Se nao possui gera erro.
								if (existeImagemProdutoFTP(idProduct)==false)
								{
									logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltProduto(), 1, "Produto nÃƒÂ£o possui imagem no site: " + idProduct + " " + produto.getNomeProduto());
									System.out.println("Produto nÃƒÂ£o possui imagem no site: " + idProduct + " " + produto.getNomeProduto());
								}							
								else if (existeDepartamentoProduto(produto)==false)
								{
									logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltProduto(), 1, "Produto deve possuir ao menos um departamento vinculado: " + idProduct + " " + produto.getNomeProduto());
									System.out.println("Produto deve porssuir ao menos um departamento: " + idProduct + " " + produto.getNomeProduto());
								}
								else
								{
									try
									{
										saldoEstoqueProduto = recuperarSaldoEstoqueProduto(empresa, estoqueProduto, manager);
										
										if (saldoEstoqueProduto==null)
										{
											logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltProduto(), 1, "Estoque nao existe para o produto. Ref: " + estoqueProduto.getProduto().getCodigoProduto() + " Cor: " + estoqueProduto.getCor().getCodigo() + " Tamanho: " + estoqueProduto.getTamanho().getCodigo());
										}
										else
										{
											valorProduto = recuperarPrecoProduto(produto, produto.getMarca(), con);
											saldoEstoqueProduto.setVenda(valorProduto);

											estoqueTotal = estoqueTotal+saldoEstoqueProduto.getSaldo().intValue();
											productSKU = criarProdutoSKUWS(produto, estoqueProduto, saldoEstoqueProduto);
											estoqueProdutoSKU = new DsvEstoqueProdutoSKU();
											estoqueProdutoSKU.setProductSKU(productSKU);
											estoqueProdutoSKU.setEstoqueProduto(estoqueProduto);
											listaEstoqueProdutoSKU.add(estoqueProdutoSKU);
											if (listaProductPai.get(idProduct)==null)
												prodAux = new DsvProdutoAux();
											else
												prodAux = listaProductPai.get(idProduct);
											//prodAux.setQuantidadeTotal(estoqueTotal);
											prodAux.setQuantidadeTotal(saldoEstoqueProduto.getSaldo().intValue()+prodAux.getQuantidadeTotal());
											prodAux.setProduto(produto);
											prodAux.setCor(estoqueProduto.getCor());
											prodAux.setValorProduto(valorProduto);
											listaProductPai.put(idProduct, prodAux);
											System.out.println(contProduto++);									
										}
									}
									catch(Exception exEstq)
									{
										//logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIndefinida(), 1, "Nao foi encontrado estoque para este Produto: " + produto.getCodigoProduto()+ " Referencia :" + EstoqueProduto.getCodigobarra() + " "  + exEstq.getMessage());
										throw new DsvProcessamentoLVException(exEstq);								
									}
								}
							}
						}
						catch(DsvProcessamentoLVException dsve)
						{
							dsve.printStackTrace();
							throw dsve;
						}
						catch(Exception dsvE)
						{
							//logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIndefinida(), 1, "Produto: " + produto.getCodigoProduto()+ "  " + dsvE.getMessage());
							dsvE.printStackTrace();
							throw new DsvProcessamentoLVException("Produto: " + produto.getCodigoProduto()+ "  " + dsvE.getMessage(), dsvE);
						}
						
					}
				}
				catch(DsvProcessamentoLVException err)
				{
					logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIndefinida(), 1,  err.getMessage());
				}				
			}
			
			try
			{
					//varre produtos Pai que foram criados e aciona webService
					Iterator<String> itKeysProducts = listaProductPai.keySet().iterator();
					while (itKeysProducts.hasNext())
					{
						idProduct = itKeysProducts.next();
						prodAux = (DsvProdutoAux) listaProductPai.get(idProduct);
						Produto produtoNew = prodAux.getProduto();
						marca = produtoNew.getMarca();
						//se marca ja foi adicionada nao chama mais o webservice
						if (marcasAdicionadas.contains(marca.getId())==false){
							acionaMarcaWebService(marca);
							marcasAdicionadas.add(marca.getId());}						
						if (prodAux.getValorProduto()==0)
						{
							situacaoEnvioLV = new SituacaoEnvioLV();
							situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVErroEnvio());
							produtoNew.setSituacaoEnvioLV(situacaoEnvioLV);
							System.out.println(produto.getCodigoProduto()+ " sem valor de venda");
							produtoService.atualizar(produtoNew);
							throw new DsvProcessamentoLVException("Erro ao salvar produto Pai " + produto.getCodigoProduto()+ " sem valor de venda");								
						}
						else
						{
							try
							{
								if (prodAux.getProduto().getNome1Produto()==null || (prodAux.getProduto().getNome2Produto()==null || prodAux.getProduto().getNome3Produto()==null))
								{
									throw new Exception("Produto nÃ£o possui descriÃ§Ãµes para o site." + prodAux.getProduto().getCodigoProduto());
								}
								else
								{
									ProductAddUpdateData p = criarProdutoWS(prodAux, idProduct);
									acionaProductWebService(p);
									situacaoEnvioLV = new SituacaoEnvioLV();
									situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVEnvioCompleto());
									produtoNew.setSituacaoEnvioLV(situacaoEnvioLV);
									produtoService.atualizar(produtoNew);
								}
							}
							catch (Exception exPro)
							{
								exPro.printStackTrace();
								situacaoEnvioLV = new SituacaoEnvioLV();
								situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVErroEnvio());
								produtoNew.setSituacaoEnvioLV(situacaoEnvioLV);
								System.out.println(produto.getCodigoProduto()+ " " + exPro.getMessage());
								produtoService.atualizar(produtoNew);
								throw new DsvProcessamentoLVException("Erro ao salvar produto Pai", exPro);
							}
						}
					}
					Iterator<DsvEstoqueProdutoSKU> itProcuctSKU = listaEstoqueProdutoSKU.iterator();
					while (itProcuctSKU.hasNext())
					{
						estoqueProdutoSKU = itProcuctSKU.next();
						EstoqueProduto codNew=null;
						try
						{
							acionaProductSKUWebService(estoqueProdutoSKU.getProductSKU());
							situacaoEnvioLV = new SituacaoEnvioLV();
							situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVEnvioCompleto());								
							codNew = estoqueProdutoSKU.getEstoqueProduto();
							codNew.setSituacaoEnvioLV(situacaoEnvioLV);
							//estoqueProdutoService.atualizar(manager, transaction, codNew);

						}
						catch(Exception exPrSKU)
						{
							codNew = estoqueProdutoSKU.getEstoqueProduto();
							situacaoEnvioLV = new SituacaoEnvioLV();
							situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVErroEnvio());								
							codNew.setSituacaoEnvioLV(situacaoEnvioLV);
							System.out.println(codNew.getCodigoBarras()+ " " + exPrSKU.getMessage());
							//estoqueProdutoService.atualizar(manager, transaction, codNew);
							throw new DsvProcessamentoLVException("Erro ao Salvar Produto SKU", exPrSKU);
							
						}	
					}
					
					//transaction.commit();
					//transaction.begin();
					//manager.flush();
				}					
			catch(DsvProcessamentoLVException err)
			{
				logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIndefinida(), 1,  err.getMessage());
			}	
			//manager.flush();
			//manager.close();
			con.close();
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	private void acionaMarcaWebService(Marca marca) throws Exception
	{
		try
		{
			ProductBrandData pb = new ProductBrandData();
			
			pb.setIdProductBrand(marca.getId()+"");
			pb.setDsName(marca.getDescricao());
			pb.setDsImage(marca.getDescricao()+".jpg");
			pb.setDsCustomHTML("");
			String xml = xmlUtil.objectToXML(pb, pb.getClass());
			String res = new Catalog().getCatalogSoap().productBrandAddUpdate(xml, "integracao", "woo5#7");
			ReturnData objRet = xmlUtil.XMLToObjectReturnData(new StringBuffer(res));
			if (objRet.getIdRet()==0)
			{
				logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltMarca(), 2, res);
			}
			
		}
		catch(Exception e)
		{
			logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltMarca(), 1, e.getMessage());
		}
	
	}
	
	private void acionaProductWebService(ProductAddUpdateData o) throws Exception
	{
		try
		{
			String xml = xmlUtil.objectToXML(o, o.getClass());
			String res = new Catalog().getCatalogSoap().productAddUpdate(xml, "integracao", "woo5#7");
			//System.out.println(xml);
			ReturnData objRet = xmlUtil.XMLToObjectReturnData(new StringBuffer(res));
			if (objRet.getIdRet()==0)
			{
				logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltProduto(), 2, res+"\n"+objRet.getIdErr()+" "+objRet.getDsErr());
				throw new Exception("Erro ao adicionar produto no Site " + o.getIdProduct() + " " + o.getDsDescription()+"\n"+objRet.getIdErr()+" "+objRet.getDsErr());
				//System.out.println("Erro ao adicionar produto no Site " + o.getIdProduct() + " " + o.getDsDescription()+"\n"+objRet.getIdErr()+" "+objRet.getDsErr());
			}
		}
		catch(Exception e)
		{
			//e.printStackTrace();
			logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltProduto(), 1, e.getMessage());
			throw new Exception("Erro ao adicionar produto no Site " + o.getIdProduct() + " " + o.getDsDescription(), e);
		}
	
	}	
	
	private void acionaProductSKUWebService(ProductSKUAddData o) throws Exception
	{
		try
		{
			String xml = xmlUtil.objectToXML(o, o.getClass());
			String res = new Catalog().getCatalogSoap().productSKUAdd(xml, "integracao", "woo5#7");
			ReturnData objRet = xmlUtil.XMLToObjectReturnData(new StringBuffer(res));
			if (objRet.getIdRet()==0)
			{
				logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltProdutoSKU(), 2, res);
				throw new Exception("Erro ao adicionar produto SKU no Site");
			}	
		}
		catch(Exception e)
		{
			logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltProdutoSKU(), 1, e.getMessage());
			throw new Exception("Erro ao adicionar produto SKU no Site", e);
		}
	
	}	
	
	private ProductAddUpdateData criarProdutoWS(DsvProdutoAux prodAux, String idProduct)
	{
		Produto produto = prodAux.getProduto();
		double valorVeda = prodAux.getValorProduto();
		ProductAddUpdateData o = new ProductAddUpdateData();
		Cor cor = prodAux.getCor();
		
		String descricaoCompleta = ((produto.getNome2Produto()==null||produto.getNome2Produto().equals(""))?"":(produto.getNome2Produto())+"\n\n")+(produto.getNome3Produto()==null?"":produto.getNome3Produto())+"\n\n"+(produto.getNome4Produto()==null?"":produto.getNome4Produto())+"\n\n"+(produto.getNome5Produto()==null?"":produto.getNome5Produto());
		descricaoCompleta = descricaoCompleta.replaceAll("(\r\n|\n\r|\r|\n)", "<br />");
		o.setDsDescription(descricaoCompleta);
		o.setDsName(DsvUtilText.firstUpperCase(produto.getNome1Produto())+" - "+cor.getDescricaoCorEmpresa().toLowerCase());
		o.setDsPartNumber("");
		o.setDtSalePriceEnd("");
		o.setDtSalePriceHourEnd("");
		o.setDtSalePriceStart("");
		o.setDtSalePriceHourStart("");
		if (prodAux.getQuantidadeTotal()>0)
			o.setFgActive(1);
		else
			o.setFgActive(0);
		o.setIdProduct(idProduct);
		o.setIdProductBrand(produto.getMarca().getCodigo()+"");
		o.setIdProductCatalog("1");
		o.setIdProductLine("1");
		o.setIdProductSector("1");
		o.setIdProductSupplier("1");
		o.setIdProductType("1");
		o.setNuDeliveryTime(0);
		o.setNuExternalDeliveryTime(0);
		o.setNuExternalInventory(0);
		o.setNuLocalInventory(0);
		o.setNuMinimumInventory(0);
		o.setNuSupplierDeliveryTime(0);
		Integer novoValor = new Integer(new Double(valorVeda*100).intValue());
		Integer valorPromocao = novoValor;
		o.setVlListPrice(novoValor);
		if (produto.getPrecoPromocao()!=null && produto.getPrecoPromocao().doubleValue()>0)
		{
			valorPromocao = new Integer(new Double(produto.getPrecoPromocao().doubleValue()*100).intValue());
		}
		o.setVlSalePrice(valorPromocao);
		o.setVlWeight(2000);		
		
		return o;
		
	}	
	
	private ProductSKUAddData criarProdutoSKUWS(Produto produto, EstoqueProduto estoqueProduto, SaldoEstoqueProduto saldoEstoqueProduto)
	{
		ProductSKUAddData o = new ProductSKUAddData();
		o.setDsAttrib1(trataTamanho(estoqueProduto.getTamanho().getDescricao()));
		o.setDsAttrib2("");
		o.setDsAttrib3("");
		o.setDsPartNumber(estoqueProduto.getCodigoBarras()+"");
		o.setIdProduct(estoqueProduto.getProduto().getCodigoProduto().toString()+"_"+estoqueProduto.getOrdemProduto().getCodigo().toString()+"_"+estoqueProduto.getCor().getCodigo().toString());
		o.setIdSKU(estoqueProduto.getProduto().getCodigoProduto().toString()+"_"+estoqueProduto.getOrdemProduto().getCodigo().toString()+"_"+estoqueProduto.getCor().getCodigo().toString()+"_"+estoqueProduto.getTamanho().getCodigo());
		o.setNuDeliveryTime(1);
		o.setNuExternalDeliveryTime(0);
		o.setNuExternalInventory(0);
		o.setNuDepth(33); //Profundidade
		o.setNuHeight(12);//Altura
		o.setNuWidth(21); //largura
		o.setVlWeight(1500);//peso
		o.setNuLocalInventory(saldoEstoqueProduto.getSaldo().intValue());
		o.setNuMinimumInventory(0);
		if (saldoEstoqueProduto.getSaldo()>0)
			o.setFgActive(1);
		else
			o.setFgActive(0);
		o.setFgActive(1);
		o.setNuSupplierDeliveryTime(0);
	
		return o;
	}
	
	/*private SaldoEstoqueProduto recuperarSaldoEstoqueProduto(EmpresaFisica empresa, EstoqueProduto estoqueProduto) throws Exception
	{
		SaldoEstoqueProduto saldoEstoqueProduto = new SaldoEstoqueProduto();
		
		SaldoEstoqueProdutoService saldoEstoqueProdutoService = new SaldoEstoqueProdutoService();
		try
		{
			saldoEstoqueProduto.setEstoqueProduto(estoqueProduto);
			saldoEstoqueProduto.setEmpresaFisica(empresa);
			saldoEstoqueProduto.setAno(empresa.getAnoAberto());
			saldoEstoqueProduto.setMes(empresa.getMesAberto());
			List<SaldoEstoqueProduto> lista = saldoEstoqueProdutoService.listarPorObjetoFiltro(saldoEstoqueProduto);
			if (lista.isEmpty()==false)
				return saldoEstoqueProdutoService.listarPorObjetoFiltro(saldoEstoqueProduto).get(0);
			else
				return null;
			
		}
		catch(Exception e)
		{
			throw new Exception("Erro ao recuperar objeto de estoque de codigo de barras", e);
		}
	}*/
	private SaldoEstoqueProduto recuperarSaldoEstoqueProduto(EmpresaFisica empresa, EstoqueProduto estoqueProduto, EntityManager manager) throws Exception
	{
		SaldoEstoqueProduto saldoEstoqueProduto = new SaldoEstoqueProduto();
		SaldoEstoqueProduto criterioSEP = new SaldoEstoqueProduto();
		
		SaldoEstoqueProdutoService saldoEstoqueProdutoService = new SaldoEstoqueProdutoService();
		try
		{
			//recupera objeto da empresa que vai ter o preÃƒÂ§o do produto
			EmpresaFisica emprPreco = new EmpresaFisica();
			emprPreco.setId(new Integer(2)); //TODO
			saldoEstoqueProduto.setEstoqueProduto(estoqueProduto);
			saldoEstoqueProduto.setEmpresaFisica(emprPreco);
			saldoEstoqueProduto.setAno(empresa.getAnoAberto());
			saldoEstoqueProduto.setMes(empresa.getMesAberto());
			List<SaldoEstoqueProduto> lista = saldoEstoqueProdutoService.listarPorObjetoFiltro(manager, saldoEstoqueProduto, null, null, null);
			int quantidadeTotal = 0;
			int quantidadeSite;
			double valorPreco = 0;
			boolean achouPrecoMandatorio=false;
			
			if (estoqueProduto.getCor()!=null && estoqueProduto.getCor().getId()>0)
			{
				//tenta se basear no preÃƒÂ§o da empresa 2 inicialmente
				//se nao acha preco na empresa 2 busca o menor preÃƒÂ§o nas filais que
				saldoEstoqueProduto=null;
				if (lista.isEmpty()==false)
				{
					saldoEstoqueProduto = lista.get(0);
					achouPrecoMandatorio=true;
				}
	
				criterioSEP = new SaldoEstoqueProduto();
				criterioSEP.setEstoqueProduto(estoqueProduto);
				criterioSEP.setAno(empresa.getAnoAberto());
				criterioSEP.setMes(empresa.getMesAberto());
				lista = saldoEstoqueProdutoService.listarPorObjetoFiltro(manager, criterioSEP, null, null, null);
				if (lista.isEmpty()==false)
				{	
					for (SaldoEstoqueProduto saldoEstoqueProduto2 : lista) 
					{
						if (saldoEstoqueProduto2.getSaldo()!=null && saldoEstoqueProduto2.getVenda()!=null && saldoEstoqueProduto2.getVenda()>0)
						{
							quantidadeTotal = quantidadeTotal + saldoEstoqueProduto2.getSaldo().intValue();
							//disputa de preÃƒÂ§o
							if (achouPrecoMandatorio==false)
							{
								if (saldoEstoqueProduto==null)
								{
									saldoEstoqueProduto = saldoEstoqueProduto2;
								}
								else
								{
									if (saldoEstoqueProduto2.getVenda().doubleValue()>saldoEstoqueProduto.getVenda().doubleValue())
									{
										saldoEstoqueProduto = saldoEstoqueProduto2;
									}
								}
							}
						}
					}
					if (quantidadeTotal<3)
					{
						quantidadeSite = 0;
						saldoEstoqueProduto.setEmpresaFisica(empresa);
						saldoEstoqueProduto.setSaldo(new Double(quantidadeSite));
						return saldoEstoqueProduto;					
					}
					else if (saldoEstoqueProduto.getVenda()==null || saldoEstoqueProduto.getVenda()==0)
					{
						throw new Exception("PreÃƒÂ§o de produto nÃƒÂ£o foi cadastrado: " + saldoEstoqueProduto.getEstoqueProduto().getProduto().getId());
					}
					else
					{
						if (quantidadeTotal>0 && quantidadeTotal<4)
							quantidadeSite = 1;
						else if(quantidadeTotal==4)
							quantidadeSite=2;
						else
							quantidadeSite = quantidadeTotal * 80 / 100;
						saldoEstoqueProduto.setEmpresaFisica(empresa);
						saldoEstoqueProduto.setSaldo(new Double(quantidadeSite));
						return saldoEstoqueProduto;
					}
				}
				else
				{
					return null;
				}				
			}
			else
			{
				return null;
			}
		}
		catch(Exception e)
		{
			throw new Exception("Erro ao recuperar objeto de estoque de codigo de barras. " + e.getMessage(), e);
		}
	}
	private Double recuperarPrecoProduto(Produto produto, Marca marca, Connection con) throws Exception
	{
		SaldoEstoqueProdutoService saldoEstoqueProdutoService = new SaldoEstoqueProdutoService();
		Double maiorPreco = saldoEstoqueProdutoService.buscarMaiorPrecoProduto(produto, con);
		//determina valor do produto
		if (!(produto.getMarca().getId()==2581 || produto.getMarca().getId()==2582 || produto.getMarca().getId()==977 ||
				produto.getMarca().getId()==2407 || produto.getMarca().getId()==329 || produto.getMarca().getId()==289))
		{
			Double preco = maiorPreco - (maiorPreco*15/100);
			return new BigDecimal(preco).setScale(2, RoundingMode.HALF_UP).doubleValue();
		}		
		else
		{
			return maiorPreco;
		}

	}	

	public void forcarEnvioSite() throws Exception
	{
		try
		{
			System.out.println("Inicio - ForÃƒÂ§ar Envio");
			IntegracaoCatalogo integracaoCatalog = new IntegracaoCatalogo();
			integracaoCatalog.enviaProdutosEstoquePrecoAlterado();
			integracaoCatalog.enviaProdutosAlterados();
			integracaoCatalog.enviaDepartamentoProduto();
			System.out.println("Final - ForÃƒÂ§ar Envio");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}

	public void enviaDepartamentos() throws Exception, DsvProcessamentoLVException
	{
		DepartamentoService departamentoService = new DepartamentoService();
		Departamento departamento = null;
		Departamento criterio = null;
		
		try
		{	
			criterio= new Departamento();
			situacaoEnvioLV = new SituacaoEnvioLV();
			situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVProntoEnvio());
			criterio.setSituacaoEnvioLV(situacaoEnvioLV);
			//List<Departamento> listaDepartamentos = departamentoService.listarPorObjetoFiltro(criterio);
			List<Departamento> departamentos = departamentoService.listarPorObjetoFiltro(criterio,"idDepartamentoPai", "ASC");
			//ArrayList<Departamento> departamentos = new DsvDepartamentoAux(listaDepartamentos).varreOrdenandoDepartamentos();
			for (int i=0;i<departamentos.size();i++)
			{
				departamento = departamentos.get(i);
				try
				{
					DeptData deptAddUpdate = criaDepartamentoWS(departamento);
					acionaDepartamentoWebService(deptAddUpdate);
					situacaoEnvioLV = new SituacaoEnvioLV();
					situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVEnvioCompleto());
					departamento.setSituacaoEnvioLV(situacaoEnvioLV);
					departamentoService.atualizar(departamento);
				}
				catch(Exception e)
				{
					situacaoEnvioLV = new SituacaoEnvioLV();
					situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVErroEnvio());
					departamento.setSituacaoEnvioLV(situacaoEnvioLV);
					departamentoService.atualizar(departamento);
					throw new DsvProcessamentoLVException(e);
				}
				
			}
		}
		catch(DsvProcessamentoLVException dsve)
		{
			logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltDepartamento(), 2, dsve.getMessage());
			throw dsve;
		}
		catch(Exception e)
		{
			logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIndefinida(), 1, e.getMessage());
			throw e;
		}
	}
	
	private DeptData criaDepartamentoWS(Departamento departamento) throws DsvProcessamentoLVException
	{
		DeptData dept = new DeptData();
		dept.setDsDeptImg("");
		if (departamento.getDescricaoCompleta()!=null && departamento.getDescricaoCompleta().equals("")==false)
			dept.setDsDescription(departamento.getDescricaoCompleta().substring(0,1).toUpperCase()+departamento.getDescricaoCompleta().substring(1).toLowerCase());
		else
			dept.setDsDescription("");
		dept.setDsManualShortcut("");
		dept.setDsMenuImg("");
		dept.setDsName(departamento.getDescricao().substring(0,1).toUpperCase()+departamento.getDescricao().substring(1).toLowerCase());
		dept.setFgManualShortcut(0);
		dept.setIdActiveFlag(departamento.getAtivo().equals("S")?1:0);
		dept.setIdDept(departamento.getId());
		dept.setIdParent((departamento.getDepartamentoPai()==null || departamento.getDepartamentoPai().getId()==null)?0:departamento.getDepartamentoPai().getId());
		dept.setNuDispOrder(departamento.getOrdemDisposicao()==null?0:1);
		
		return dept;
	}
	
	private void acionaDepartamentoWebService(DeptData o) throws DsvProcessamentoLVException
	{
		try
		{
			String xml = xmlUtil.objectToXML(o, o.getClass());
			//System.out.println(xml);
			String res = new Catalog().getCatalogSoap().deptAddUpdate(xml, "integracao", "woo5#7");
			//System.out.println(xml);
			ReturnData objRet = xmlUtil.XMLToObjectReturnData(new StringBuffer(res));
			if (objRet.getIdRet()==0)
			{
				throw new DsvProcessamentoLVException("Erro ao adicionar Departamento no Site");
			}
		}
		catch(Exception e)
		{
			throw new DsvProcessamentoLVException("Erro ao adicionar departamento no Site", e);
		}
	
	}
	
	public void enviaDepartamentoProduto() throws Exception, DsvProcessamentoLVException
	{
		DepartamentoProdutoService pdService = new DepartamentoProdutoService(); 
		DepartamentoProduto departamentoProduto;
		List<EstoqueProduto> listaEstoqueProduto = null;
		Hashtable<String, DepartamentoProduto> listaProdutosSite = new Hashtable<String, DepartamentoProduto>();
		Hashtable<String, Departamento> listaProdutosDepPaiSite = new Hashtable<String, Departamento>();
		EstoqueProduto estoqueProduto;
		String idProduct;
		String idProductDept;
		String idDept;
		Produto produto;//99961810
		Departamento depPai;
		Iterator<String> itKeysProducts = null;
		DepartamentoProduto criterioDept;
		EstoqueProdutoService epService = new EstoqueProdutoService();
		EstoqueProduto criterioEP;
		
		try
		{
			EntityManager manager = ConexaoUtil.getEntityManager();
			EntityTransaction transaction = manager.getTransaction();
			transaction.begin();			
			
			situacaoEnvioLV = new SituacaoEnvioLV();
			situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVProntoEnvio());
			criterioDept = new DepartamentoProduto();
			criterioDept.setSituacaoEnvioLV(situacaoEnvioLV);
			
			List<DepartamentoProduto> listaProdDept = pdService.listarPorObjetoFiltro(manager, criterioDept, null, null, null);
			Iterator<DepartamentoProduto> i = listaProdDept.iterator();
			while(i.hasNext())
			{	
				departamentoProduto = i.next();
				if (departamentoProduto.getProduto().getSituacaoEnvioLV().getId()==3)
				{
					criterioEP = new EstoqueProduto();
					criterioEP.setProduto(departamentoProduto.getProduto());
					
					listaEstoqueProduto = epService.listarPorObjetoFiltro(manager, criterioEP, null, null, null);
					Iterator<EstoqueProduto> i2 = listaEstoqueProduto.iterator();
					
					while (i2.hasNext()) 
					{
						estoqueProduto = i2.next();
						idProductDept = estoqueProduto.getProduto().getCodigoProduto().toString()+"_"+estoqueProduto.getOrdemProduto().getCodigo().toString()+"_"+estoqueProduto.getCor().getCodigo().toString()+"-"+departamentoProduto.getDepartamento().getCodigo();
						if (listaProdutosSite.containsKey(idProductDept)==false)
						{
							listaProdutosSite.put(idProductDept, departamentoProduto);
						}
						depPai = departamentoProduto.getDepartamento().getDepartamentoPai();
						if (depPai!=null)
						{
							idProductDept = estoqueProduto.getProduto().getCodigoProduto().toString()+"_"+estoqueProduto.getOrdemProduto().getCodigo().toString()+"_"+estoqueProduto.getCor().getCodigo().toString()+"-"+depPai.getCodigo();
							if (listaProdutosDepPaiSite.containsKey(idProductDept)==false){
								listaProdutosDepPaiSite.put(idProductDept, depPai);}						
						}
					}
				}
			}
			//varre produtos por departamento
			itKeysProducts = listaProdutosSite.keySet().iterator();
			while (itKeysProducts.hasNext())
			{
				idProductDept = itKeysProducts.next();
				String[] args = idProductDept.split("-");
				idProduct = args[0];
				idDept = args[1];
				departamentoProduto = listaProdutosSite.get(idProductDept);
				
				try
				{
					DeptProductData o = criaProdutoDepartamentoWS(departamentoProduto, idProduct, 0);
					acionaProdutoDepartamentoWebService(o);
					situacaoEnvioLV = new SituacaoEnvioLV();
					situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVEnvioCompleto());
					departamentoProduto.setSituacaoEnvioLV(situacaoEnvioLV);
					pdService.atualizar(manager, transaction, departamentoProduto);
					
				}
				catch(DsvProcessamentoLVException dsve)
				{
					situacaoEnvioLV = new SituacaoEnvioLV();
					situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVErroEnvio());
					departamentoProduto.setSituacaoEnvioLV(situacaoEnvioLV);
					pdService.atualizar(manager, transaction, departamentoProduto);
					logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltDepartamento(), 2, dsve.getMessage());
				}					
				catch(Exception ex)
				{
					situacaoEnvioLV = new SituacaoEnvioLV();
					situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVErroEnvio());
					departamentoProduto.setSituacaoEnvioLV(situacaoEnvioLV);
					pdService.atualizar(manager, transaction, departamentoProduto);
					logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltDepartamento(), 2, ex.getMessage());
				}
				transaction.commit();
				transaction.begin();
			}
			//varre produtos Pai e adiciona
			/*itKeysProducts = listaProdutosDepPaiSite.keySet().iterator();
			while (itKeysProducts.hasNext())
			{
				idProductDept = itKeysProducts.next();
				String[] args = idProductDept.split("-");
				idProduct = args[0];
				idDept = args[1];
				depPai = listaProdutosDepPaiSite.get(idProductDept);
				try
				{
					DeptProductData o = criaProdutoDepartamentoWS(depPai, idProduct, 0);
					acionaProdutoDepartamentoWebService(o);
				}
				catch(DsvProcessamentoLVException dsve)
				{
					logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltDepartamento(), 2, "Inclusao de departamento pai: " + dsve.getMessage());
				}					
				catch(Exception ex)
				{
					logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltDepartamento(), 2, "Inclusao de departamento pai: " + ex.getMessage());
				}
			}*/			
		}
		catch(Exception e)
		{
			logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIndefinida(), 1, e.getMessage());
			throw e;
		}
	}
	
	public void removeDepartamentoProduto() throws Exception, DsvProcessamentoLVException
	{
		DepartamentoProdutoService pdService = new DepartamentoProdutoService();
		DepartamentoProduto departamentoProduto;
		List<EstoqueProduto> listaEstoqueProduto = null;
		Hashtable<String, DepartamentoProduto> listaProdutosSite = new Hashtable<String, DepartamentoProduto>();
		Hashtable<String, Departamento> listaProdutosDepPaiSite = new Hashtable<String, Departamento>();
		EstoqueProduto estoqueProduto;
		EstoqueProdutoService epService = new EstoqueProdutoService();
		String idProduct;
		String idProductDept;
		String idDept;
		Produto produto;//99961810
		Departamento depPai;
		Iterator<String> itKeysProducts = null;
		DepartamentoProduto criterioDP = new DepartamentoProduto();
		
		try
		{
			situacaoEnvioLV = new SituacaoEnvioLV();
			situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVErroEnvio());
			criterioDP.setSituacaoEnvioLV(situacaoEnvioLV);
			List<DepartamentoProduto> listaProdDept = pdService.listarPorObjetoFiltro(criterioDP);
			Iterator<DepartamentoProduto> i = listaProdDept.iterator();
			while(i.hasNext())
			{
				departamentoProduto = i.next();
				estoqueProduto = new EstoqueProduto();
				estoqueProduto.setProduto(departamentoProduto.getProduto());
				listaEstoqueProduto = epService.listarPorObjetoFiltro(estoqueProduto);
				Iterator<EstoqueProduto> i2 = listaEstoqueProduto.iterator();
				while (i2.hasNext()) 
				{
					estoqueProduto = i2.next();
					idProductDept = estoqueProduto.getProduto().getCodigoProduto().toString()+"_"+estoqueProduto.getOrdemProduto().getCodigo().toString()+"_"+estoqueProduto.getCor().getCodigo().toString()+"-"+departamentoProduto.getDepartamento().getCodigo();
					if (listaProdutosSite.containsKey(idProductDept)==false){
						listaProdutosSite.put(idProductDept, departamentoProduto);}
					depPai = departamentoProduto.getDepartamento().getDepartamentoPai();
					if (depPai!=null)
					{
						idProductDept = estoqueProduto.getProduto().getCodigoProduto().toString()+"_"+estoqueProduto.getOrdemProduto().getCodigo().toString()+"_"+estoqueProduto.getCor().getCodigo().toString()+"-"+departamentoProduto.getDepartamento().getCodigo();
						if (listaProdutosDepPaiSite.containsKey(idProductDept)==false){
							listaProdutosDepPaiSite.put(idProductDept, depPai);}						
					}
				}
			}
			//varre produtos por departamento
			itKeysProducts = listaProdutosSite.keySet().iterator();
			while (itKeysProducts.hasNext())
			{
				idProductDept = itKeysProducts.next();
				String[] args = idProductDept.split("-");
				idProduct = args[0];
				idDept = args[1];
				departamentoProduto = listaProdutosSite.get(idProductDept);
				
				try
				{
					DeptProductRemoveData o = criaProdutoDepartamentoRemoveWS(departamentoProduto, idProduct);
					acionaProdutoDepartamentoRemoveWebService(o);
					situacaoEnvioLV = new SituacaoEnvioLV();
					situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVRemovidoSite());
					departamentoProduto.setSituacaoEnvioLV(situacaoEnvioLV);
					pdService.atualizar(departamentoProduto);
				}
				catch(DsvProcessamentoLVException dsve)
				{
					situacaoEnvioLV = new SituacaoEnvioLV();
					situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVErroRemocao());
					departamentoProduto.setSituacaoEnvioLV(situacaoEnvioLV);
					pdService.atualizar(departamentoProduto);
					logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltDepartamento(), 2, dsve.getMessage());
				}					
				catch(Exception ex)
				{
					situacaoEnvioLV = new SituacaoEnvioLV();
					situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVErroRemocao());
					departamentoProduto.setSituacaoEnvioLV(situacaoEnvioLV);
					pdService.atualizar(departamentoProduto);
					logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltDepartamento(), 2, ex.getMessage());
				}
			}			
		}
		catch(Exception e)
		{
			logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIndefinida(), 1, e.getMessage());
			throw e;
		}
	}	
	
	private DeptProductData criaProdutoDepartamentoWS(DepartamentoProduto produtoDepartamento, String idProduct, int isRoot) throws DsvProcessamentoLVException
	{
		DeptProductData o = new DeptProductData(); //TODO
		o.setIdDept(produtoDepartamento.getDepartamento().getId());
		o.setIdProduct(idProduct);
		o.setIsRoot(isRoot);

		return o;
	}
	private DeptProductData criaProdutoDepartamentoWS(Departamento departamento, String idProduct, int isRoot) throws DsvProcessamentoLVException
	{
		DeptProductData o = new DeptProductData(); //TODO
		o.setIdDept(departamento.getId());
		o.setIdProduct(idProduct);
		o.setIsRoot(isRoot);

		return o;
	}	
	
	private void acionaProdutoDepartamentoWebService(DeptProductData o) throws DsvProcessamentoLVException
	{
		try
		{
			String xml = xmlUtil.objectToXML(o, o.getClass());
			String res = new Catalog().getCatalogSoap().deptProductAdd(xml, "integracao", "woo5#7");
			ReturnData objRet = xmlUtil.XMLToObjectReturnData(new StringBuffer(res));
			if (objRet.getIdRet()==0)
			{
				throw new DsvProcessamentoLVException("Erro ao adicionar produto Departamento no Site " + res);
			}
		}
		catch(Exception e)
		{
			throw new DsvProcessamentoLVException("Erro ao adicionar produto departamento no Site. ", e);
		}
	
	}	
	
	private DeptProductRemoveData criaProdutoDepartamentoRemoveWS(DepartamentoProduto produtoDepartamento, String idProduct) throws DsvProcessamentoLVException
	{
		DeptProductRemoveData o = new DeptProductRemoveData(); //TODO
		o.setIdDept(produtoDepartamento.getId());
		o.setIdProduct(idProduct);

		return o;
	}
	
	private void acionaProdutoDepartamentoRemoveWebService(DeptProductRemoveData o) throws DsvProcessamentoLVException
	{
		try
		{
			String xml = xmlUtil.objectToXML(o, o.getClass());
			String res = new Catalog().getCatalogSoap().deptProductRemove(xml, "integracao", "woo5#7");
			ReturnData objRet = xmlUtil.XMLToObjectReturnData(new StringBuffer(res));
			if (objRet.getIdRet()==0)
			{
				throw new DsvProcessamentoLVException("Erro ao remover Produto em Departamento no Site");
			}
		}
		catch(Exception e)
		{
			throw new DsvProcessamentoLVException("Erro ao remover Produto em departamento no Site", e);
		}
	
	}
	
	/**
	* 
	*/
	public Set <Produto> listaProdutosNovosEAlteradosLV() throws Exception
	{
		//Novos e Alterados
		Produto criterio = new Produto();
		situacaoEnvioLV = new SituacaoEnvioLV();
		situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVAguardandoAprov());
		criterio.setSituacaoEnvioLV(situacaoEnvioLV);
		criterio.setProdutoLV("S");
		List<Produto> lista = produtoDAO.listarPorObjetoFiltro(criterio);
		//Com erro de processamento
		//Novos e Alterados
		criterio = new Produto();
		situacaoEnvioLV = new SituacaoEnvioLV();
		situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVErroEnvio());
		criterio.setSituacaoEnvioLV(situacaoEnvioLV);
		criterio.setProdutoLV("S");
		List<Produto> listaComErro = produtoDAO.listarPorObjetoFiltro(criterio);
		lista.addAll(listaComErro);
		//Sem foto
		criterio = new Produto();
		situacaoEnvioLV = new SituacaoEnvioLV();
		situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVAguardandoImagens());
		criterio.setSituacaoEnvioLV(situacaoEnvioLV);
		criterio.setProdutoLV("S");
		List<Produto> listaSemFoto = produtoDAO.listarPorObjetoFiltro(criterio);
		lista.addAll(listaSemFoto);

		return new HashSet<Produto>(lista);

	}
	/**
	* 
	*/
	/*public Set <Produto> listaProdutosPrecoEstoqueAlteradoLV(Integer idEmpresa)
	{
		Set<Produto> aguardando = EstoqueProdutoDAO.listarPro(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVAguardandoAprov());	
		Set<Produto> semFoto = EstoqueProdutoDAO.findProdutosEstoqueProdutoBySituacaoEnvioLV(4);
		Set<Produto> comErro = EstoqueProdutoDAO.findProdutosEstoqueProdutoBySituacaoEnvioLV(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVErroEnvio());
		
		aguardando.addAll(semFoto);
		aguardando.addAll(comErro);
		
		return aguardando;
	}
	/**
	* 
	*/
	/*public Set <ProdutoDepartamento> listaProdutosDepartamentoAlteradoLV()
	{
		Set<ProdutoDepartamento> alterados =  produtoDepartamentoDAO.findProdutoDepartamentoByIdSituacaoEnvioLV(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVProntoEnvio());
		Set<ProdutoDepartamento> comErro =  produtoDepartamentoDAO.findProdutoDepartamentoByIdSituacaoEnvioLV(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVErroEnvio());
		alterados.addAll(comErro);
		return alterados;
	}*/	
	
	private String trataTamanho(String str)
	{
		String arr[] = str.split("/");
		if (arr!=null && arr.length>1)
			return arr[0];
		else
			return str;
	}
	

	
	public void enviaEstoqueProduto() throws Exception
	{
		SaldoEstoqueProdutoService eepService = new SaldoEstoqueProdutoService();
		EstoqueProdutoService epService = new EstoqueProdutoService();
		EmpresaFisicaService empresaFisicaService = new EmpresaFisicaService();
		SaldoEstoqueProduto saldoEstoqueProduto;
		EstoqueProduto ep;
		ProductInventoryUpdateData product;
		
		EmpresaFisica empresa = empresaFisicaService.recuperarPorId(DsvConstante.getInstanceParametrosSistema().getIdEmpresaLV());
		
		saldoEstoqueProduto = new SaldoEstoqueProduto();
		saldoEstoqueProduto.setMes(empresa.getMesAberto());
		saldoEstoqueProduto.setAno(empresa.getAnoAberto());

		situacaoEnvioLV = new SituacaoEnvioLV();
		situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVProntoEnvio());
		saldoEstoqueProduto.setSituacaoEnvioLV(situacaoEnvioLV);
		
		List<SaldoEstoqueProduto> listaEstoqueCodigo = eepService.listarPorObjetoFiltro(saldoEstoqueProduto);
			
		for (int i=0; i<listaEstoqueCodigo.size();i++)
		{
			try
			{
				saldoEstoqueProduto = listaEstoqueCodigo.get(i);
				ep = saldoEstoqueProduto.getEstoqueProduto();
				if (ep.getCor().getCodigo()!=0)
				{
					//cria  Produtct SKU caso nao exista
					ProductSKUAddData productSKU = criarProdutoSKUWS(ep.getProduto(), ep, saldoEstoqueProduto);
					acionaProductSKUWebService(productSKU);					
					//cb = epService.findEstoqueProdutoByField("codigobarra", ecb.getCodigobarra().toString()).iterator().next();
					product = criarEstoqueProdutoWS(ep, saldoEstoqueProduto);
					acionaEstoqueUpdateWebService(product);
					//
					situacaoEnvioLV = new SituacaoEnvioLV();
					situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVEnvioCompleto());
					saldoEstoqueProduto.setSituacaoEnvioLV(situacaoEnvioLV);
					eepService.atualizar(saldoEstoqueProduto);
				}
			}
			catch(Exception e)
			{
				logOperacaoLVService.logaAe(26, 1, "Erro ao autualizar estoque produto. Codigo de Barras: " + (saldoEstoqueProduto!=null?saldoEstoqueProduto.getEstoqueProduto().getCodigoBarras()+"":""));
			}			
		}
		System.out.println("AtualizaÃƒÂ§ÃƒÂ£o de Estoque Produtos Finalizada");
	}
	
	public void atualizaSaldoEstoqueProduto()
	{
		try
		{
			Produto p = new Produto();
			ProdutoService pService = new ProdutoService();
			SaldoEstoqueProduto objSaldo;
			EstoqueProdutoService epService = new EstoqueProdutoService();
			EstoqueProduto epCriterio;
			List<EstoqueProduto> listaEstoque;
			
			//define parametros
			SituacaoEnvioLV sit = new SituacaoEnvioLV();
			sit.setId(26);
			EmpresaFisica empresaFisica = new EmpresaFisicaService().recuperarPorId(2);

			SituacaoEnvioLV sitNova = new SituacaoEnvioLV();
			sitNova.setId(3);

			
			p.setSituacaoEnvioLV(sit);
			List<Produto> listaProdutos = pService.listarPorObjetoFiltro(p, "codigoProduto", "DESC");
			EntityManager manager = ConexaoUtil.getEntityManager();
			EntityTransaction transaction = manager.getTransaction();
			double saldoProduto;
			List<Produto> produtosSemSaldo = new ArrayList<>();
			
			for (Produto produto : listaProdutos) 
			{
				saldoProduto = 0;
				epCriterio = new EstoqueProduto();
				epCriterio.setProduto(produto);
				listaEstoque = epService.listarPorObjetoFiltro(epCriterio);
				for (EstoqueProduto estoqueProduto : listaEstoque) 
				{
					objSaldo = recuperarSaldoEstoqueProduto(empresaFisica, estoqueProduto, manager);
					if (objSaldo!=null)
					{
						objSaldo.setEstoqueProduto(estoqueProduto);
						saldoProduto = saldoProduto + objSaldo.getSaldo().doubleValue();
						enviaEstoqueProduto(objSaldo);
					}
				}
				if (saldoProduto==0)
					produtosSemSaldo.add(produto);
				
				produto.setSituacaoEnvioLV(sitNova);
				pService.atualizar(produto);
				System.out.println(produto.getCodigoProduto());
			}
			manager.close();
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public void enviaEstoqueProduto(SaldoEstoqueProduto saldoEstoqueProduto) throws Exception
	{
		SaldoEstoqueProdutoService eepService = new SaldoEstoqueProdutoService();
		EstoqueProdutoService epService = new EstoqueProdutoService();
		EmpresaFisicaService empresaFisicaService = new EmpresaFisicaService();
		EstoqueProduto ep;
		ProductInventoryUpdateData product;
		
		EmpresaFisica empresa = empresaFisicaService.recuperarPorId(DsvConstante.getInstanceParametrosSistema().getIdEmpresaLV());
					
		try
		{
			ep = saldoEstoqueProduto.getEstoqueProduto();
			if (ep.getCor().getCodigo()!=0)
			{
				//so descomente se nao tiver certeza que Produto esta criado 
				//cria  Produtct SKU caso nao exista
				//ProductSKUAddData productSKU = criarProdutoSKUWS(ep.getProduto(), ep, saldoEstoqueProduto);
				//acionaProductSKUWebService(productSKU);					
				//cb = epService.findEstoqueProdutoByField("codigobarra", ecb.getCodigobarra().toString()).iterator().next();
				product = criarEstoqueProdutoWS(ep, saldoEstoqueProduto);
				acionaEstoqueUpdateWebService(product);
				//
				situacaoEnvioLV = new SituacaoEnvioLV();
				situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVEnvioCompleto());
				saldoEstoqueProduto.setSituacaoEnvioLV(situacaoEnvioLV);
				eepService.atualizar(saldoEstoqueProduto);
			}
		}
		catch(Exception e)
		{
			logOperacaoLVService.logaAe(26, 1, "Erro ao autualizar estoque produto. Codigo de Barras: " + (saldoEstoqueProduto!=null?saldoEstoqueProduto.getEstoqueProduto().getCodigoBarras()+"":""));
		}			
	}
	
	private ProductInventoryUpdateData criarEstoqueProdutoWS(EstoqueProduto estoqueProduto, SaldoEstoqueProduto estoque)
	{
		ProductInventoryUpdateData piu = new ProductInventoryUpdateData();
		String idProduct = null;
		
		idProduct = estoqueProduto.getProduto().getCodigoProduto().toString()+"_"+estoqueProduto.getOrdemProduto().getCodigo().toString()+"_"+estoqueProduto.getCor().getCodigo().toString()+"_"+estoqueProduto.getTamanho().getCodigo();
		
		piu.setIdProduct(idProduct);
		piu.setNuLocalInventory(estoque.getSaldo().intValue());
		
		return piu;	
	}
	public void acionaEstoqueUpdateWebService(ProductInventoryUpdateData o) throws DsvProcessamentoLVException
	{
		try
		{
			String xml = xmlUtil.objectToXML(o, o.getClass());
			String res = new Catalog().getCatalogSoap().productInventoryUpdate(xml, "integracao", "woo5#7");
			ReturnData objRet = xmlUtil.XMLToObjectReturnData(new StringBuffer(res));
			if (objRet.getIdRet()==0)
			{
				throw new DsvProcessamentoLVException("Erro ao atualizar Estoque do Produto no Site");
			}
		}
		catch(Exception e)
		{
			throw new DsvProcessamentoLVException("Erro ao atualizar Estoque do Produto no Site", e);
		}
	}	
	
	
	/*public int gerarProdutoCor() throws DsvProcessamentoLVException
	{
		ProdutoCor produtoCor;
		ProdutoCor criterio;
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		System.out.println("gerarProdutoCor - Hora inicial: " + sdf.format(new Date()));
		
		EstoqueProdutoService epService = (EstoqueProdutoService) DsvConstante.getApplicationContext().getBean("EstoqueProdutoService");
		ProdutoCorService pcService = (ProdutoCorService) DsvConstante.getApplicationContext().getBean("ProdutoCorService");
		EstoqueProduto exemplo = new EstoqueProduto();
		exemplo.setIdSituacaoEnvioLV(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVAguardandoAprov());
		List<EstoqueProduto> listaCodigos = epService.queryByExample(exemplo);
		EstoqueProduto cb;
		int cont=0;
		
		System.out.println("Iniciando gerador de produto/cor");
		try
		{
			Iterator<EstoqueProduto> i = listaCodigos.iterator();
			while (i.hasNext())
			{
				cb = i.next();
				
				criterio = new ProdutoCor();
				criterio.setIdProduto(cb.getIdProduto());
				criterio.setIdCor(cb.getIdCor());
				if (produtoCorDao.queryByExample(criterio).isEmpty())
				{
					produtoCor = new ProdutoCor();	
					produtoCor.setIdCor(cb.getIdCor());
					produtoCor.setIdProduto(cb.getIdProduto());
					produtoCor.setUltimaAlteracao(null);
					produtoCor.setIdOrdem(cb.getIdOrdemProduto());
					produtoCor = pcService.saveProdutoCor(produtoCor);
					cont++;
				}
			}
			System.out.println("Prodto/Cor gerados: " + cont);
			System.out.println("gerarProdutoCor - Hora Final: " + sdf.format(new Date()));
			return cont;			
		}
		catch(Exception ex)
		{
			throw new DsvProcessamentoLVException("Nao foi possivel criar entidade ProdutoCor. Produto: " + ex.getMessage());
			
		}
	}*/	
	
	private Vector<String> recuperaListaArquivosFTP() throws Exception 
	{
		Vector<String> nomeArquivos = new Vector<String>();
		FTPClient ftp = new FTPClient();
		ftp.connect("200.155.17.122");
		ftp.login("user_ftp", "123qwe..");
		ftp.changeWorkingDirectory("/images/product/");
		nomeArquivos.addAll(Arrays.asList(ftp.listNames()));
		return nomeArquivos;
		//return new DsvUtilArquivoReferencia().buscaNomeProdutos();
	}
	
	private boolean existeImagemProdutoFTP(String idProduct)
	{
		String nomeProdutoValidacao;
		nomeProdutoValidacao = idProduct+"_1n.jpg";
		if (arquivosFTP.contains(nomeProdutoValidacao)==false)
			return false;
		nomeProdutoValidacao = idProduct+"_1b.jpg";
		if (arquivosFTP.contains(nomeProdutoValidacao)==false)
			return false;
		
		return true;
	}
	private boolean existeDepartamentoProduto(Produto produto) throws Exception
	{
		DepartamentoProduto dp = new DepartamentoProduto();
		DepartamentoProdutoService dpService = new DepartamentoProdutoService();
		dp.setProduto(produto);
		
		if (dpService.listarPorObjetoFiltro(dp).size()==0)
			return false;
		else
			return true;
		
	}
	
	private void testeWebService() throws Exception
	{
		
		String xml = "";
		
		Marca marca = new MarcaService().recuperarPorId(2276);
		acionaMarcaWebService(marca);
		
		xml = xml + "<?xml version=\"1.0\" encoding=\"UTF-16\" standalone=\"yes\"?>";
		xml = xml + "<Product xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">";
		xml = xml + "<IdProduct>61987_3900_998</IdProduct>";
		xml = xml + "<DsName>Sapato</DsName>";
		xml = xml + "<FgActive>1</FgActive>";
		xml = xml + "<IdProductType>1</IdProductType>";
		xml = xml + "<IdProductLine>1</IdProductLine>";
		xml = xml + "<IdProductBrand>2276</IdProductBrand>";
		xml = xml + "<IdProductSupplier>1</IdProductSupplier>";
		xml = xml + "<IdProductSector>1</IdProductSector>";
		xml = xml + "<VlWeight>2000</VlWeight>";
		xml = xml + "<IdProductCatalog>1</IdProductCatalog>";
		xml = xml + "<DsPartNumber></DsPartNumber>";
		xml = xml + "<NuLocalInventory>0</NuLocalInventory>";
		xml = xml + "<NuMinimumInventory>0</NuMinimumInventory>";
		xml = xml + "<NuExternalInventory>0</NuExternalInventory>";
		xml = xml + "<NuSupplierDeliveryTime>0</NuSupplierDeliveryTime>";
		xml = xml + "<NuExternalDeliveryTime>0</NuExternalDeliveryTime>";
		xml = xml + "<NuDeliveryTime>0</NuDeliveryTime>";
		xml = xml + "<VlListPrice>17655</VlListPrice>";
		xml = xml + "<VlSalePrice>17655</VlSalePrice>";
		xml = xml + "<dtSalePriceStart></dtSalePriceStart>";
		xml = xml + "<dtSalePriceHourStart></dtSalePriceHourStart>";
		xml = xml + "<dtSalePriceEnd></dtSalePriceEnd>";
		xml = xml + "<dtSalePriceHourEnd></dtSalePriceHourEnd>";
		xml = xml + "<DsDescription></DsDescription>";
		xml = xml + "</Product>";
		
		String res = new Catalog().getCatalogSoap().productAddUpdate(xml, "integracao", "woo5#7");
		//System.out.println(xml);
		ReturnData objRet = xmlUtil.XMLToObjectReturnData(new StringBuffer(res));
		System.out.println("Erro 1:" + objRet.getDsErr());
		
		xml="<?xml version=\"1.0\" encoding=\"UTF-16\" standalone=\"yes\"?>";
		xml = xml + "<Product xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">";
		xml = xml+"<IdProduct>62028_4264_872</IdProduct>";
		xml = xml+"<DsName>Tenis Bottero 181304 - nude/rose</DsName>";
		xml = xml+"<FgActive>1</FgActive>";
		xml = xml+"<IdProductType>1</IdProductType>";
		xml = xml+"<IdProductLine>1</IdProductLine>";
		xml = xml+"<IdProductBrand>112</IdProductBrand>";
		xml = xml+"<IdProductSupplier>1</IdProductSupplier>";
		xml = xml+"<IdProductSector>1</IdProductSector>";
		xml = xml+"<VlWeight>2000</VlWeight>";
		xml = xml+"<IdProductCatalog>1</IdProductCatalog>";
		xml = xml+"<DsPartNumber></DsPartNumber>";
		xml = xml+"<NuLocalInventory>0</NuLocalInventory>";
		xml = xml+"<NuMinimumInventory>0</NuMinimumInventory>";
		xml = xml+"<NuExternalInventory>0</NuExternalInventory>";
		xml = xml+"<NuSupplierDeliveryTime>0</NuSupplierDeliveryTime>";
		xml = xml+"<NuExternalDeliveryTime>0</NuExternalDeliveryTime>";
		xml = xml+"<NuDeliveryTime>0</NuDeliveryTime>";
		xml = xml+"<VlListPrice>6655</VlListPrice>";
		xml = xml+"<VlSalePrice>6655</VlSalePrice>";
		xml = xml+"<dtSalePriceStart></dtSalePriceStart>";
		xml = xml+"<dtSalePriceHourStart></dtSalePriceHourStart>";
		xml = xml+"<dtSalePriceEnd></dtSalePriceEnd>";
		xml = xml+"<dtSalePriceHourEnd></dtSalePriceHourEnd>";
		xml = xml+"<DsDescription>CONFECCIONADO EM COURO NOBUCK COM RECORTES EMPAETÃƒÅ S. FECHAMENTO COM ATACADORES PRESOS POR ILHOSES EM BANHO OURO. POSSUI PALMILHA EM EVA MACIO E SOLADO EM TR ANTIDERRAPANTE.&lt;br /&gt;&lt;br /&gt;CABEDAL: COURO.&lt;br /&gt;PALMILHA: EVA.&lt;br /&gt;SOLADO: BORRACHA.&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;</DsDescription>";
		xml = xml+"</Product>";		
		
		res = new Catalog().getCatalogSoap().productAddUpdate(xml, "integracao", "woo5#7");
		//System.out.println(xml);
		objRet = xmlUtil.XMLToObjectReturnData(new StringBuffer(res));
		System.out.println("Erro 2:" + objRet.getDsErr());		
		
	}
	public void verificaProdutosSite() throws Exception
	{
		List<Produto> listaProdutos;
		Produto produto = null;
		Marca marca = null;
		ProdutoService produtoService = new ProdutoService();
		EstoqueProduto estoqueProduto = null;
		ProductSKUAddData productSKU = null;
		List <EstoqueProduto> listaEstoqueProduto = null;
		HashMap<String, DsvProdutoAux> listaProductPai = new HashMap<String, DsvProdutoAux>();
		DsvEstoqueProdutoSKU estoqueProdutoSKU = new DsvEstoqueProdutoSKU();
		int estoqueTotal;
		EstoqueProdutoService estoqueProdutoService = new EstoqueProdutoService();
		EmpresaFisicaService empresaService = new EmpresaFisicaService();
		EmpresaFisica empresa;
		SaldoEstoqueProduto saldoEstoqueProduto;
		Set <DsvEstoqueProdutoSKU> listaEstoqueProdutoSKU;
		String idProduct = null;
		DsvProdutoAux prodAux = null;
		boolean validarFoto = false;
		int contProduto = 0;
		EstoqueProduto criterioEP;
		
		
		try
		{
			//recupera dados da empresa para ver Mes e Ano Abertos
			empresa = empresaService.recuperarPorId(DsvConstante.getInstanceParametrosSistema().getIdEmpresaLV());
			//recupera fotos
			Hashtable<String, ArrayList<Integer>> refCorArquivos = new Hashtable<String, ArrayList<Integer>>();
			//new DsvUtilImagens().buscaReferenciaFotosCores(DsvConstante.getInstanceParametrosSistema().getCaminhoAbsolutoFotos());
			Hashtable<Integer , ArrayList> refsArquivos = null; 
			//= new DsvUtilImagens().buscaReferenciaFotos(DsvConstante.getInstanceParametrosSistema().getCaminhoAbsolutoFotos());
			arquivosFTP = recuperaListaArquivosFTP();
			Vector produtosSite = recuperaProdutosSite("c:/desenv/tmp/tbProduct.rpt");
			//JtaTransactionManager txManager = ((JtaTransactionManager) ctx.getBean("transactionManager"));
			//TransactionStatus status = txManager.getTransaction(new DefaultTransactionDefinition()); 
			situacaoEnvioLV = new SituacaoEnvioLV();
			situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVProntoEnvio());
			estoqueProduto = new EstoqueProduto();
			estoqueProduto.setSituacaoEnvioLV(situacaoEnvioLV);
			//versao antiga que precisava da situacao do estoque produto. Agora nao precisa mais 11/10/2013 - Loja virtual nao tem mais estoque proprio.
			//listaProdutos = estoqueProdutoService.listarProdutoPorSituacaoEstoque(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVProntoEnvio());
			/////===>>>>>
			situacaoEnvioLV = new SituacaoEnvioLV();
			situacaoEnvioLV.setId(3);
			produto = new Produto();
			produto.setSituacaoEnvioLV(situacaoEnvioLV);
			listaProdutos = produtoService.listarPorObjetoFiltro(produto,"idProduto","ASC");
			Vector<String> listaProducts = new Vector<String>();
			
			//listaProdutos = produtoService.findProdutoByField("idProduto", "51406");
			
			Iterator<Produto> itProdo = listaProdutos.iterator();
			while (itProdo.hasNext())
			{

				produto = itProdo.next();
				estoqueTotal = 0;
				listaEstoqueProdutoSKU = new HashSet<DsvEstoqueProdutoSKU>();
				marca = produto.getMarca();
				criterioEP = new EstoqueProduto();
				criterioEP.setProduto(produto);
				listaEstoqueProduto = estoqueProdutoService.listarPorObjetoFiltro(criterioEP);
				Iterator<EstoqueProduto> itEstoqueProduto = listaEstoqueProduto.iterator();
				SituacaoEnvioLV sit = new SituacaoEnvioLV();
				sit.setId(2);
				//acionaMarcaWebService(marca);
				while (itEstoqueProduto.hasNext())
				{
					try
					{
						estoqueProduto = itEstoqueProduto.next();
						if (estoqueProduto.getCor()!=null && estoqueProduto.getCor().getId()!=null && estoqueProduto.getCor().getId()!=0)
						{
							idProduct = estoqueProduto.getProduto().getCodigoProduto().toString()+"_"+estoqueProduto.getOrdemProduto().getCodigo()+"_"+estoqueProduto.getCor().getCodigo().toString();
							saldoEstoqueProduto = null;
							//verifica se produto possui ao menos algumas imagens no site; Se nao possui gera erro.
							if (existeImagemProdutoFTP(idProduct)==false)
							{
								logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltProduto(), 1, "Produto nÃƒÂ£o possui imagem no site: " + idProduct + " " + produto.getNomeProduto());
								System.out.println("Produto nÃƒÂ£o possui imagem no site: " + idProduct + " " + produto.getNomeProduto());
							}							
							/*else if (existeDepartamentoProduto(produto)==false)
							{
								logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIncAltProduto(), 1, "Produto deve possuir ao menos um departamento vinculado: " + idProduct + " " + produto.getNomeProduto());
								System.out.println("Produto deve porssuir ao menos um departamento: " + idProduct + " " + produto.getNomeProduto());
							}
							else
							{
								listaProducts.add(idProduct);
							}*/
						}
					}
					catch(DsvProcessamentoLVException dsve)
					{
						dsve.printStackTrace();
						throw dsve;
					}
					catch(Exception dsvE)
					{
						//logOperacaoLVService.logaAe(DsvConstante.getInstanceParametrosSistema().getTipoOperacaoIndefinida(), 1, "Produto: " + produto.getCodigoProduto()+ "  " + dsvE.getMessage());
						dsvE.printStackTrace();
						throw new DsvProcessamentoLVException("Produto: " + produto.getCodigoProduto()+ "  " + dsvE.getMessage(), dsvE);
					}
				}
				//varre produtos Pai que foram criados e aciona webService
				
				for (String product : listaProducts) 
				{
					if (produtosSite.contains(product)==false)
					{
						produto = produtoService.recuperarPorId(new Integer(product.split("_")[0]));
						produto.setSituacaoEnvioLV(sit);
						produtoService.atualizar(produto);
						//System.out.println(product);
						
					}
				}
			}		
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	public void verificaProdutosSiteVer2() throws Exception
	{
		List<Produto> listaProdutos;
		Produto produto = null;
		Marca marca = null;
		ProdutoService produtoService = new ProdutoService();
		EstoqueProduto estoqueProduto = null;
		ProductSKUAddData productSKU = null;
		List <EstoqueProduto> listaEstoqueProduto = null;
		HashMap<String, DsvProdutoAux> listaProductPai = new HashMap<String, DsvProdutoAux>();
		DsvEstoqueProdutoSKU estoqueProdutoSKU = new DsvEstoqueProdutoSKU();
		int estoqueTotal;
		EstoqueProdutoService estoqueProdutoService = new EstoqueProdutoService();
		EmpresaFisicaService empresaService = new EmpresaFisicaService();
		EmpresaFisica empresa;
		SaldoEstoqueProduto saldoEstoqueProduto;
		Set <DsvEstoqueProdutoSKU> listaEstoqueProdutoSKU;
		String idProduct = null;
		DsvProdutoAux prodAux = null;
		boolean validarFoto = false;
		int contProduto = 0;
		EstoqueProduto criterioEP;
		int idProduto;
		
		
		try
		{
			//recupera dados da empresa para ver Mes e Ano Abertos
			empresa = empresaService.recuperarPorId(DsvConstante.getInstanceParametrosSistema().getIdEmpresaLV());
			//recupera fotos
			Hashtable<String, ArrayList<Integer>> refCorArquivos = new Hashtable<String, ArrayList<Integer>>();
			//new DsvUtilImagens().buscaReferenciaFotosCores(DsvConstante.getInstanceParametrosSistema().getCaminhoAbsolutoFotos());
			Hashtable<Integer , ArrayList> refsArquivos = null; 
			//= new DsvUtilImagens().buscaReferenciaFotos(DsvConstante.getInstanceParametrosSistema().getCaminhoAbsolutoFotos());
			arquivosFTP = recuperaListaArquivosFTP();
			ArrayList<String> produtosSite = recuperaProdutosSiteVer2("c:/desenv/tmp/tbProduct.rpt");
			//JtaTransactionManager txManager = ((JtaTransactionManager) ctx.getBean("transactionManager"));
			//TransactionStatus status = txManager.getTransaction(new DefaultTransactionDefinition()); 
			situacaoEnvioLV = new SituacaoEnvioLV();
			situacaoEnvioLV.setId(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVProntoEnvio());
			estoqueProduto = new EstoqueProduto();
			estoqueProduto.setSituacaoEnvioLV(situacaoEnvioLV);
			//versao antiga que precisava da situacao do estoque produto. Agora nao precisa mais 11/10/2013 - Loja virtual nao tem mais estoque proprio.
			//listaProdutos = estoqueProdutoService.listarProdutoPorSituacaoEstoque(DsvConstante.getInstanceParametrosSistema().getIdSituacaoEnvioLVProntoEnvio());
			/////===>>>>>
			situacaoEnvioLV = new SituacaoEnvioLV();
			situacaoEnvioLV.setId(2);
			produto = new Produto();
			produto.setSituacaoEnvioLV(situacaoEnvioLV);
			

			for (String string : produtosSite)
			{	
				if (string.length()>5)
				{
					string = string.replaceAll("[^0-9]", "");
					idProduto = new Integer(string);
				}
				else
				{
					idProduto = new Integer(string.trim()).intValue();
				}
				produto = produtoService.recuperarPorId(idProduto);
				produto.setSituacaoEnvioLV(situacaoEnvioLV);
				produto.setProdutoLV("S");
				produtoService.atualizar(produto);
			}
		
		}
		catch(Exception e)
		{
			throw e;
		}
	}	
	
	
	private Vector recuperaProdutosSite(String arquivo) throws Exception
	{
		FileReader reader = null;
		BufferedReader bufferedReader = null;
		String linha = null;
		Vector<String> arquivosSite = new Vector<String>();
		
		try
		{
			reader = new FileReader(arquivo);
		}
		catch(FileNotFoundException fnfe)
		{
			throw new Exception(fnfe);	
		}
		
		bufferedReader = new BufferedReader(reader);	
		HashMap<String, String> referencias = new HashMap<>();
		String valores[];
		int contLinhas = 0;  
		while (bufferedReader.ready())
		{
			linha = bufferedReader.readLine();
			contLinhas++;
			if (linha.trim().equals("")==false)
			{
				arquivosSite.add(linha.trim());
			}
		}
		bufferedReader.close();
		System.out.println("Linhas :" + contLinhas);
		return arquivosSite;
	}
	private ArrayList<String> recuperaProdutosSiteVer2(String arquivo) throws Exception
	{
		FileReader reader = null;
		BufferedReader bufferedReader = null;
		String linha = null;
		ArrayList<String> arquivosSite = new ArrayList<String>();
		
		try
		{
			reader = new FileReader(arquivo);
		}
		catch(FileNotFoundException fnfe)
		{
			throw new Exception(fnfe);	
		}
		
		bufferedReader = new BufferedReader(reader);	
		HashMap<String, String> referencias = new HashMap<>();
		String valores[];
		int contLinhas = 0;  
		while (bufferedReader.ready())
		{
			linha = bufferedReader.readLine();
			contLinhas++;
			if (linha.trim().equals("")==false)
			{
				if (arquivosSite.contains(linha.trim().split("_")[0])==false)
				{
					arquivosSite.add(linha.trim().split("_")[0]);
				}
			}
		}
		bufferedReader.close();
		System.out.println("Linhas :" + contLinhas);
		System.out.println("Produtos :" + arquivosSite.size());
		return arquivosSite;
	}	

}
