package br.com.desenv.nepalign.integracao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.integracao.dto.DataPackage;
import br.com.desenv.nepalign.integracao.dto.DuplicataAcordoDTO;
import br.com.desenv.nepalign.integracao.dto.DuplicataDTO;
import br.com.desenv.nepalign.integracao.dto.DuplicataParcelaDTO;
import br.com.desenv.nepalign.integracao.dto.LogDuplicataDTO;
import br.com.desenv.nepalign.model.Duplicata;
import br.com.desenv.nepalign.model.DuplicataAcordo;
import br.com.desenv.nepalign.model.DuplicataParcela;

import flex.messaging.io.ArrayCollection;
import flex.messaging.io.amf.client.AMFConnection;

public class DuplicataBundle implements DataBundle
{	
	public List<Integer> dataDuplicatasIntegradas;
	
	public DuplicataBundle()
	{
		dataDuplicatasIntegradas = new ArrayList<Integer>();
	}
	
	public void importar(AMFConnection con, EntityManager manager, EntityTransaction transaction, String idEmpresaFisica, Date dataInicial, Date dataFinal) throws Exception
	{
		final String databaseName = Util.getDatabaseName(idEmpresaFisica);
		
		do
		{
			System.out.println(">Excluindo duplicatas antigas... [" + IgnUtil.dateFormat.format(dataInicial) + "]");
			
			manager.createNativeQuery("DELETE FROM " + databaseName + ".duplicataParcela WHERE idDuplicata IN (SELECT idDuplicata FROM " + databaseName + ".duplicata WHERE dataCompra BETWEEN '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' AND '" + IgnUtil.dateFormatSql.format(dataInicial) + " 23:59:59');").executeUpdate();
			manager.createNativeQuery("DELETE FROM " + databaseName + ".duplicata WHERE dataCompra BETWEEN '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' AND '" + IgnUtil.dateFormatSql.format(dataInicial) + " 23:59:59';").executeUpdate();
			
			System.out.println(">>excluído com sucesso!");
			System.out.println(">Importando duplicatas... [" + new SimpleDateFormat("dd/MM/yyyy").format(dataInicial) + "]");
			
			Duplicata duplicataFiltro = new Duplicata();
			duplicataFiltro.setDataCompra(dataInicial);
			
			ArrayCollection listaDuplicata = (ArrayCollection) con.call(Util.SERVICE_GET_TERM_SALE, duplicataFiltro);
			
			System.out.println(">>" + listaDuplicata.size() + " Duplicatas encontradas. Importando...");
			
			int currentIndex = 0x00;
			
			if(!Util.DEBUG_MODE)
				System.out.print(">>Importando Duplicatas 		1/" + listaDuplicata.size());
			else
				System.out.println(">>Importando Duplicatas!");
			
			for(Object duplicataBuffer : listaDuplicata)
			{
				Duplicata duplicata = ((Duplicata) duplicataBuffer).clone();
				
				currentIndex++;
			
				if(!Util.DEBUG_MODE)
				{
					for (int j = 0; j < (listaDuplicata.size() + "").length() + 0x01; j++) 
		                System.out.print('\b');
					
		            for (int j = 0; j < (currentIndex + "").length(); j++)
		                System.out.print('\b');
		            
		            System.out.print(currentIndex + "/" + listaDuplicata.size());
				}
				
				runDuplicata(con, manager, transaction, idEmpresaFisica, databaseName, duplicata);
			}
			
			if(!Util.DEBUG_MODE)
				System.out.println();
			
			System.out.println(">>>Importadas " + listaDuplicata.size() + " duplicatas!");
			
			dataDuplicatasIntegradas.add(Integer.parseInt(new SimpleDateFormat("yyyyMMdd").format(dataInicial)));
			
			dataInicial = Util.addDay(dataInicial, 0x01); 
		}
		while(dataInicial.before(dataFinal) || dataInicial.equals(dataFinal));
	}
	
	private void runDuplicata(AMFConnection con, EntityManager manager, EntityTransaction transaction, String idEmpresaFisica, String databaseName, Duplicata duplicata) throws Exception
	{
		Util.clienteBundle.importar(con, manager, transaction, idEmpresaFisica, duplicata.getCliente(), false);				
        Util.vendedorBundle.importarUsuario(con, manager, transaction, idEmpresaFisica, duplicata.getUsuario());
		
		manager.merge(Util.<DuplicataDTO> copy(DuplicataDTO.class, duplicata));
		
		DuplicataParcela duplicataParcelaFiltro = new DuplicataParcela();
		duplicataParcelaFiltro.setDuplicata(duplicata);
		
		ArrayCollection listaDuplicataParcela = (ArrayCollection) con.call(Util.SERVICE_GET_TERM_SALE_ITEMS, duplicataParcelaFiltro);
		
		for(Object duplicataParcelaBuffer : listaDuplicataParcela)
		{
			DuplicataParcela duplicataParcela = ((DuplicataParcela) duplicataParcelaBuffer).clone();
			
			Util.vendedorBundle.importarUsuario(con, manager, transaction, idEmpresaFisica, duplicataParcela.getUsuario());
			
			manager.merge(Util.<DuplicataParcelaDTO> copy(DuplicataParcelaDTO.class, duplicataParcela));
			
			if(duplicataParcela.getAcordo() != null && duplicataParcela.getAcordo().equals("S"))
			{
				List<Duplicata> duplicatasAntigas = new ArrayList<Duplicata>();
				
				DuplicataAcordo duplicataAcordoFiltro = new DuplicataAcordo();
				duplicataAcordoFiltro.setDuplicataNova(duplicataParcela.getDuplicata());
				
				ArrayCollection listaDuplicataAcordo = (ArrayCollection) con.call(Util.SERVICE_GET_TERM_DEAL, duplicataAcordoFiltro);
				
				for(Object dupa : listaDuplicataAcordo) 
				{ 
					DuplicataAcordo duplicataAcordo = ((DuplicataAcordo)dupa).clone();
					
					if(!new IgnUtil().containId(duplicataAcordo.getDuplicataAntigaParcela().getDuplicata().getId().intValue(), duplicatasAntigas))
						duplicatasAntigas.add(duplicataAcordo.getDuplicataAntigaParcela().getDuplicata());
					
					Util.clienteBundle.importar(con, manager, transaction, idEmpresaFisica, duplicataAcordo.getCliente());
					
					manager.createNativeQuery("DELETE FROM " + databaseName + ".duplicataAcordo WHERE idDuplicataAcordo = " + duplicataAcordo.getId().intValue() + ";").executeUpdate();
					manager.merge(Util.<DuplicataAcordoDTO> copy(DuplicataAcordoDTO.class, duplicataAcordo));
				}
				
				for(Duplicata duplicataAntiga : duplicatasAntigas)
					runDuplicata(con, manager, transaction, idEmpresaFisica, databaseName, (Duplicata) con.call(Util.SERVICE_GET_TERM_SALE_ONLY, duplicataAntiga.getId().intValue()));
			}
		}
	}

	@SuppressWarnings("unchecked")
	public DataPackage exportar(final EntityManager manager, final DataPackage dataPackage, final Date dataInicial, final Date dataFinal) throws Exception
	{		
		final String duplicataQuery = " select dupli.* from duplicata dupli where (dataCompra between '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' and '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59'  " +
										" or idDuplicata in (select mov.idDuplicata from movimentoparceladuplicata mov where mov.dataMovimento between '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' and '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59')) " +
										" union " + 
										" select dupli.* from duplicataAcordo dupAcordo  " +
										" inner join duplicata dupli ON dupli.numeroDuplicata = dupAcordo.numeroDuplicataAntiga  " +
										" where dupAcordo.idduplicataNova in (select idDuplicata from duplicata where dataCompra between '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' and '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59')";
		
		final String duplicataParcelaQuery = " select parc.* from duplicataParcela parc " +
										" inner join  (" + duplicataQuery.replace("select dupli.*", "select dupli.idDuplicata") + ") as dups " +
										" on parc.idDuplicata = dups.idDuplicata ";
		
		final String duplicataAcordoQuery = " select acordo.* from duplicataAcordo acordo " +
											" join (select idDuplicata from duplicata where dataCompra between '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' and '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59') as duplicatas " +
											" ON acordo.idDuplicataNova = duplicatas.idDuplicata ";
		
		final String logDuplicataQuery = " select logDuplicata.* from logDuplicata " +
										" where logDuplicata.data between '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' and '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59'";
		
		dataPackage.<DuplicataDTO> put(DuplicataDTO.class, (List<DuplicataDTO>) manager.createNativeQuery(duplicataQuery, DuplicataDTO.class).getResultList());
		dataPackage.<DuplicataParcelaDTO> put(DuplicataParcelaDTO.class, (List<DuplicataParcelaDTO>) manager.createNativeQuery(duplicataParcelaQuery, DuplicataParcelaDTO.class).getResultList());
		dataPackage.<DuplicataAcordoDTO> put(DuplicataAcordoDTO.class, (List<DuplicataAcordoDTO>) manager.createNativeQuery(duplicataAcordoQuery, DuplicataAcordoDTO.class).getResultList());
		dataPackage.<LogDuplicataDTO> put(LogDuplicataDTO.class,  (List<LogDuplicataDTO>) manager.createNativeQuery(logDuplicataQuery, LogDuplicataDTO.class).getResultList());
		
		return dataPackage;
	}
}
