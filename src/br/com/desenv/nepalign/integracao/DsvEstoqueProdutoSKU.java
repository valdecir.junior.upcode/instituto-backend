package br.com.desenv.nepalign.integracao;


import br.com.desenv.nepalign.model.EstoqueProduto;

import com.desenv.nepal.catalog.xmlData.ProductSKUAddData;

public class DsvEstoqueProdutoSKU 
{

	private EstoqueProduto estoqueProduto;
	private ProductSKUAddData productSKU;
	
	
	public EstoqueProduto getEstoqueProduto() 
	{
		return estoqueProduto;
	}
	public void setEstoqueProduto(EstoqueProduto estoqueProduto) 
	{
		this.estoqueProduto = estoqueProduto;
	}
	public ProductSKUAddData getProductSKU() 
	{
		return productSKU;
	}
	public void setProductSKU(ProductSKUAddData productSKU) 
	{
		this.productSKU = productSKU;
	}
	
}

