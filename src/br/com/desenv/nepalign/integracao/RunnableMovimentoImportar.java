package br.com.desenv.nepalign.integracao;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.security.sasl.AuthenticationException;

import org.hibernate.proxy.pojo.javassist.JavassistLazyInitializer;

import br.com.desenv.frameworkignorante.DsvException;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.ParametrosSistema;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.ParametrosSistemaService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;
import flex.messaging.io.amf.client.AMFConnection;

public class RunnableMovimentoImportar implements Runnable 
{
	private Integer idEmpresaFisica;
	private Date dataInicial;
	private Date dataFinal;
	
	private StringBuilder log;
	
	public RunnableMovimentoImportar()
	{
		super();
	}
	
	public RunnableMovimentoImportar(final Integer _idEmpresaFisica, final Date _dataInicial, final Date _dataFinal)
	{
		super();
		
		idEmpresaFisica = _idEmpresaFisica;
		dataInicial = _dataInicial;
		dataFinal = _dataFinal;
		
		log = new StringBuilder();
	}
	
	public String getLog()
	{
		return log.toString();
	}

	private void atualizarParametro(EntityManager manager, EntityTransaction transaction, String nomeParametro, String valor) throws Exception
	{
		ParametrosSistema parametrosSistemaFiltro = new ParametrosSistema();
		parametrosSistemaFiltro.setDescricao(nomeParametro);
		
		ParametrosSistema parametroUltimaAtualizacao = null;
		
		ParametrosSistemaService parametrosSistemaService = new ParametrosSistemaService();
		
		List<ParametrosSistema> listaParametros = parametrosSistemaService.listarPorObjetoFiltro(manager, parametrosSistemaFiltro, null, null, null);
		
		if(listaParametros.size() == 0x01)
		{
			parametroUltimaAtualizacao = listaParametros.get(0x00);
			parametroUltimaAtualizacao.setValor(valor);
		}
		else if(listaParametros.size() == 0x00)
		{
			parametroUltimaAtualizacao = new ParametrosSistema();
			parametroUltimaAtualizacao.setDescricao(nomeParametro);
			parametroUltimaAtualizacao.setValor(valor);
		}
		
		parametrosSistemaService.atualizar(manager, transaction, parametroUltimaAtualizacao);	
	}

	@Override
	public void run() 
	{
		final String databaseName = Util.getDatabaseName(String.format("%02d", idEmpresaFisica)); 
		final String amfUrl = Util.getAmfUrl(String.format("%02d", idEmpresaFisica));	
		
		System.out.println(">Iniciando processo - " + new Date());
		
		System.out.println(">Criando conexão com o banco de dados [" + databaseName + "]");
		
		EntityManager manager = ConexaoUtil.getFactory(idEmpresaFisica).createEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		transaction.begin();
		
		try
		{
			System.out.println(">>Manager HOST > ".concat(((org.hibernate.engine.spi.SessionImplementor) manager.getDelegate()).connection().getMetaData().getURL()));	
		}
		catch(Exception ex)
		{
			
		}
		
		try
		{
			System.out.println(">>Conectado!");
			
			System.out.println(">Tentando conexão na URL > " + amfUrl);
			
			AMFConnection con = new AMFConnection();
			con.connect(amfUrl);
			
			System.out.println(">Aplicando Content-Type > application/x-amf");
			
			con.addHttpRequestHeader("Content-type", "application/x-amf");
			
			AMFConnection.registerAlias("JavassistLazyInitializer", JavassistLazyInitializer.class.getName());
			AMFConnection.registerAlias("DsvException", DsvException.class.getName());
			
			System.out.println(">>Sucesso"); 
			
			manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=0;").executeUpdate();
			 
			System.out.println(">Conectado com sucesso");
			System.out.println(">Tentando autenticar...");
			
			EntityManager managerIndependente = ConexaoUtil.getEntityManager();
			
			try
			{
				managerIndependente.getTransaction().begin();
				atualizarParametro(managerIndependente, managerIndependente.getTransaction(), "LOG_ULTIMA_ATUALIZACAO_MOVIMENTO_F" + String.format("%02d", idEmpresaFisica), "[" + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()) + "] TENTANDO CONECTAR A LOJA...");
				managerIndependente.getTransaction().commit();
			}
			catch(Exception exCleanParam)
			{
				exCleanParam.printStackTrace();
				managerIndependente.getTransaction().rollback();
			}
			
			if((boolean) con.call(Util.SERVICE_AUTH_USER, Util.generateCredentials()))
				System.out.println(">>Autenticado!");
			else
				throw new AuthenticationException("não foi possível autenticar o cliente.");
			
			log.append("[" + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()) + "] conexão COM A LOJA FEITA COM SUCESSO!");
			log.append(System.getProperty("line.separator"));
			
			try
			{
				managerIndependente.getTransaction().begin();
				atualizarParametro(managerIndependente, managerIndependente.getTransaction(), "LOG_ULTIMA_ATUALIZACAO_MOVIMENTO_F" + String.format("%02d", idEmpresaFisica), log.toString());
				managerIndependente.getTransaction().commit();
			}
			catch(Exception exCleanParam)
			{
				exCleanParam.printStackTrace();
				managerIndependente.getTransaction().rollback();
			}
			
			Util.vendedorBundle.importar(con, manager, transaction, String.format("%02d", idEmpresaFisica));
			
			System.out.println(">Aplicando Alterações feitas!");
			transaction.commit();
			System.out.println(">>Sucesso!");

			log.append("[" + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()) + "] VENDEDORES IMPORTADOS");
			log.append(System.getProperty("line.separator"));
			
			try
			{
				managerIndependente.getTransaction().begin();
				atualizarParametro(managerIndependente, managerIndependente.getTransaction(), "LOG_ULTIMA_ATUALIZACAO_MOVIMENTO_F" + String.format("%02d", idEmpresaFisica), log.toString());
				managerIndependente.getTransaction().commit();
			}
			catch(Exception exCleanParam)
			{
				exCleanParam.printStackTrace();
				managerIndependente.getTransaction().rollback();
			}
			
			transaction.begin();
			
			Util.pedidoVendaBundle.importar(con, managerIndependente, transaction, String.format("%02d", idEmpresaFisica), dataInicial, dataFinal);
			
			System.out.println(">Aplicando Alterações feitas!");
			transaction.commit();
			System.out.println(">>Sucesso!");

			log.append("[" + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()) + "] VENDAS IMPORTADAS");
			log.append(System.getProperty("line.separator"));
			
			try
			{
				managerIndependente.getTransaction().begin();
				atualizarParametro(managerIndependente, managerIndependente.getTransaction(), "LOG_ULTIMA_ATUALIZACAO_MOVIMENTO_F" + String.format("%02d", idEmpresaFisica), log.toString());
				managerIndependente.getTransaction().commit();
			}
			catch(Exception exCleanParam)
			{
				exCleanParam.printStackTrace();
				managerIndependente.getTransaction().rollback();
			}
			
			transaction.begin();
			Util.duplicataBundle.importar(con, manager, transaction, String.format("%02d", idEmpresaFisica), dataInicial, dataFinal);
			
			System.out.println(">Aplicando Alterações feitas!");
			transaction.commit();
			System.out.println(">>Sucesso!");
			
			log.append("[" + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()) + "] DUPLICATAS IMPORTADAS");
			log.append(System.getProperty("line.separator"));
			
			try
			{
				managerIndependente.getTransaction().begin();
				atualizarParametro(managerIndependente, managerIndependente.getTransaction(), "LOG_ULTIMA_ATUALIZACAO_MOVIMENTO_F" + String.format("%02d", idEmpresaFisica), log.toString());
				managerIndependente.getTransaction().commit();
			}
			catch(Exception exCleanParam)
			{
				exCleanParam.printStackTrace();
				managerIndependente.getTransaction().rollback();
			}
			
			managerIndependente.close();
			
			transaction.begin();
			new MovimentoLojaBundle().importar(con, manager, transaction, String.format("%02d", idEmpresaFisica), dataInicial, dataFinal);
			
			System.out.println(">Aplicando Alterações feitas!");
			transaction.commit();
			System.out.println(">>Sucesso!");

			log.append("[" + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()) + "] MOVIMENTO LOJA IMPORTADO");
			log.append(System.getProperty("line.separator"));
			
			transaction.begin();
			manager.createNativeQuery("SET FOREIGN_KEY_CHECKS=1;").executeUpdate();
			transaction.commit();
			
			manager.close();
			
			con.close();
			
			manager = ConexaoUtil.getEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			
			atualizarParametro(manager, transaction, "ULTIMA_ATUALIZACAO_MOVIMENTO_F" + String.format("%02d", idEmpresaFisica), new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
			
			log.append(System.getProperty("line.separator"));
			log.append("[" + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()) + "] FINALIZADO COM SUCESSO!");
			
			atualizarParametro(manager, transaction, "LOG_ULTIMA_ATUALIZACAO_MOVIMENTO_F" + String.format("%02d", idEmpresaFisica), log.toString());
			
			transaction.commit();
			manager.close();
			
			System.out.println(">Finalizado > " + new Date());	
		}
		catch(Exception ex)
		{
			if(transaction != null && transaction.isActive())
				transaction.rollback();
			
			log.append(System.getProperty("line.separator"));
			log.append(System.getProperty("line.separator"));
			log.append("[" + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()) + "] ERRO!! " + ex.getMessage());
			log.append(System.getProperty("line.separator"));
			
			EntityManager managerException = ConexaoUtil.getEntityManager();
			EntityTransaction transactionException = managerException.getTransaction();
			transactionException.begin();
			
			try
			{
				atualizarParametro(managerException, transactionException, "LOG_ULTIMA_ATUALIZACAO_MOVIMENTO_F" + String.format("%02d", idEmpresaFisica), log.toString());
				transactionException.commit();
			}
			catch(Exception _ex)
			{
				transactionException.rollback();
				
				_ex.printStackTrace();
			}
			finally
			{
				managerException.close();
			}
			
			ex.printStackTrace();
			
			try 
			{
				super.finalize();
			} 
			catch (Throwable e) 
			{
				e.printStackTrace();
			}
		}
		finally
		{
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			
			EmpresaFisica empresaFisica = null;
			
			try 
			{
				empresaFisica = new EmpresaFisicaService().recuperarPorId(idEmpresaFisica);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			ConsistenciaMovimento consistencia = new ConsistenciaMovimento();
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dataInicial);
			calendar.set(Calendar.HOUR_OF_DAY, calendar.getActualMaximum(Calendar.HOUR_OF_DAY));
			calendar.set(Calendar.MINUTE, calendar.getActualMaximum(Calendar.MINUTE));
			calendar.set(Calendar.SECOND, calendar.getActualMaximum(Calendar.SECOND));
			
			do
			{
				try 
				{
					consistencia.atualizarValoresConsistenciaMovimento(empresaFisica, calendar.getTime());
				} 
				catch (Exception e) 
				{
					e.printStackTrace(); 
				}
				
				calendar.add(Calendar.DAY_OF_YEAR, 0x01);
				calendar.set(Calendar.HOUR_OF_DAY, calendar.getActualMaximum(Calendar.HOUR_OF_DAY));
				calendar.set(Calendar.MINUTE, calendar.getActualMaximum(Calendar.MINUTE));
				calendar.set(Calendar.SECOND, calendar.getActualMaximum(Calendar.SECOND));
			}
			while(!(simpleDateFormat.format(calendar.getTime()).equals(simpleDateFormat.format(dataFinal))) && !(calendar.getTime().getTime() > dataFinal.getTime()));
			
			
			if(manager != null && manager.isOpen())
				manager.close();
		}
	}
}