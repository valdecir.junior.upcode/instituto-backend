package br.com.desenv.nepalign.integracao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import flex.messaging.io.amf.client.AMFConnection;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.integracao.dto.ChequeRecebidoDTO;
import br.com.desenv.nepalign.integracao.dto.DataPackage;
import br.com.desenv.nepalign.model.ChequeRecebido;

public class ChequeRecebidoBundle implements DataBundle
{
	private List<Integer> chequesIntegrados;
	
	public ChequeRecebidoBundle()
	{
		chequesIntegrados = new ArrayList<Integer>();
	}
	
	public void importar(AMFConnection con, EntityManager manager, EntityTransaction transaction, String idEmpresaFisica, ChequeRecebido chequeRecebido) throws Exception
	{
		if(chequeRecebido == null)
			return;
		if(chequesIntegrados.contains(chequeRecebido.getId().intValue()))
				return;
		
		ChequeRecebido chequeRecebido_ = (ChequeRecebido) con.call(Util.SERVICE_GET_CHEQUE, chequeRecebido.getId().intValue());
		
		if(chequeRecebido_ == null)
			throw new Exception("não foi possível importar o CHEQUE N>" + chequeRecebido.getNumeroCheque() + "/ C>" + chequeRecebido.getNumeroConta() + " ID > " + chequeRecebido.getId().intValue());
		
		Util.clienteBundle.importar(con, manager, transaction, idEmpresaFisica, chequeRecebido.getCliente());

		manager.merge(Util.<ChequeRecebidoDTO> copy(ChequeRecebidoDTO.class, chequeRecebido));
		
		chequesIntegrados.add(chequeRecebido.getId().intValue());
	}
	
	@SuppressWarnings("unchecked")
	public DataPackage exportar(final EntityManager manager, final DataPackage dataPackage, final Date dataInicial, final Date dataFinal) throws Exception
	{
		final String chequeRecebidoQuery = " select * from chequerecebido where dataEmissao between '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' and '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59'; ";
		
		dataPackage.<ChequeRecebidoDTO> put(ChequeRecebidoDTO.class, (List<ChequeRecebidoDTO>) manager.createNativeQuery(chequeRecebidoQuery, ChequeRecebidoDTO.class).getResultList());
		
		return dataPackage;
	}
}
