package br.com.desenv.nepalign.integracao;

public class ThreadMovimento extends Thread 
{
	private Runnable threadRunner;
	
	public ThreadMovimento()
	{
		super();
	}
	
	public ThreadMovimento(Runnable runner)
	{
		super(runner);
		
		threadRunner = runner;
	}
	
	public String getRunnerLog()
	{
		return null;
	}
}
