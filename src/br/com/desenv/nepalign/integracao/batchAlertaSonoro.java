package br.com.desenv.nepalign.integracao;

import java.util.Date;

import br.com.desenv.nepalign.service.AlertaPedidoVendaService;
import br.com.desenv.nepalign.util.DsvConstante;

public class batchAlertaSonoro  extends Thread{

	private IntegracaoPedido integracaoPedido;
	private AlertaPedidoVendaService alertaPedidoVendaService;
	
	public void run()
	{
		try
		{
			this.integracaoPedido = new IntegracaoPedido();
			this.alertaPedidoVendaService = new AlertaPedidoVendaService();
			System.out.println("Thread iniciada. " + new Date());
			do
			{
				this.integracaoPedido.integrarPedidos();
				Thread.sleep(DsvConstante.getInstanceParametrosSistema().getSleepTimeThreadVerificarAlertas());
				this.alertaPedidoVendaService.verificarAlteracoesAlertaPedidoVenda();
				Thread.sleep(DsvConstante.getInstanceParametrosSistema().getSleepTimeThreadIntegrarPedidos());
			}
			while(true);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public static void main(String[] args)
	{
		
		Thread checkThread = (new BatchIntegracaoVertico());
		checkThread.start();
	}
}


