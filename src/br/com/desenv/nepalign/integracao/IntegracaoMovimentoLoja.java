package br.com.desenv.nepalign.integracao;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.OperacaoMovimentacaoEstoque;
import br.com.desenv.nepalign.service.OperacaoMovimentacaoEstoqueService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.util.FtpUtil;
import br.com.desenv.nepalign.util.MailUtil;
import br.com.desenv.nepalign.util.UtilDatabase;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class IntegracaoMovimentoLoja 
{
	public static void main(String[] args) throws Exception
	{
		IntegracaoMovimentoLoja integracaoLoja = new IntegracaoMovimentoLoja();
		Integer sequencial = Integer.parseInt(args[0x1]);
		
		if(args[0x0].equals("ENVIAR_TG"))
		{
			integracaoLoja.enviarTabelasGerais(sequencial); 
		}
		else if(args[0x0].equals("ENVIAR_OPME"))
		{
			integracaoLoja.enviarOperacaoMovimentacaoEstoque(sequencial);
		}
		else
			throw new Exception("Operação inválida!");
	}
	
	/*
	 * Envia Dump das tabelas OperacaoMovimentacaoEstoque para o FTP 
	 */
	public void enviarOperacaoMovimentacaoEstoque(Integer sequencial) throws Exception
    { 
    	Exception processException = null;
    	EntityManager manager = null;
    	EntityTransaction transaction = null;
    	Date dataInicioProcesso = new Date();	

		String numeroEmpresaFisica = String.format ("%02d", DsvConstante.getParametrosSistema().getInt("EMPRESA_LOCAL"));
		
		boolean processOk = false;
    	
    	try
    	{	
			manager = ConexaoUtil.getEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			
    		String nomeArquivoDump = "dump_OPME_" + numeroEmpresaFisica + "_" + new SimpleDateFormat("yyyyMMdd").format(new Date()) + "_" + sequencial + ".NEPALDUMP";
    		String diretorioArquivoDump = DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump;
    		
			File arquivoDump = new File(diretorioArquivoDump);
			
			if(arquivoDump.exists())
				arquivoDump.delete();
			
			arquivoDump = new File(diretorioArquivoDump.replace("NEPALDUMP", "rar"));
			
			if(arquivoDump.exists())
				arquivoDump.delete();
    		
    		new UtilDatabase().generateBackupFile("nepalignloja" +  numeroEmpresaFisica, "operacaoMovimentacaoEstoque", diretorioArquivoDump);
    		IgnUtil.compactarArquivo(diretorioArquivoDump, diretorioArquivoDump.replace("NEPALDUMP", "rar"));
    		new FtpUtil().uploadFile(diretorioArquivoDump.replace("NEPALDUMP", "rar"), nomeArquivoDump.replace("NEPALDUMP", "rar"), DsvConstante.getParametrosSistema().get("INTEGRACAO_IP_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_USUARIO_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_FTP"));
    		
    		OperacaoMovimentacaoEstoqueService operacaoMovimentacaoEstoqueService = new OperacaoMovimentacaoEstoqueService();
    		OperacaoMovimentacaoEstoque operacaoMovimentacaoEstoqueFiltro = new OperacaoMovimentacaoEstoque();
    		operacaoMovimentacaoEstoqueFiltro.setFlagSincronizacao("0");
    		List<OperacaoMovimentacaoEstoque> listaOperacaoMovimentacaoEstoque = operacaoMovimentacaoEstoqueService.listarPorObjetoFiltro(operacaoMovimentacaoEstoqueFiltro, "idOperacao", "asc");
    		    
        	FileWriter arquivoSaida = new FileWriter(DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + numeroEmpresaFisica + "_backup_registro.sql");
        	BufferedWriter bufferSaida = new BufferedWriter(arquivoSaida);
        	
        	bufferSaida.write(" BACKUP DAS OPERACOES ENVIADAS PARA O Escritório COM O ARQUIVO " + nomeArquivoDump);
        
        	for (OperacaoMovimentacaoEstoque operacaoMovimentacaoEstoque : listaOperacaoMovimentacaoEstoque)
        	{
        	    bufferSaida.write("update nepalign.operacaomovimentacaoestoque set flagSincronizacao = '0' where idOperacao = " + operacaoMovimentacaoEstoque.getId() + ";");
        	    bufferSaida.newLine();
        	    operacaoMovimentacaoEstoque.setFlagSincronizacao("1");
        	    operacaoMovimentacaoEstoqueService.atualizar(operacaoMovimentacaoEstoque, false);
        	}
        	
        	bufferSaida.newLine();
        	bufferSaida.newLine();
        	bufferSaida.flush();
        	bufferSaida.close();
        	arquivoSaida.close();
        	transaction.commit();
        	
        	processOk = true;
    	}
    	catch(Exception ex)
    	{
		    if (transaction!=null)
		    	transaction.rollback();
		    
		    processOk = false;
		    processException = ex; 
    	}
    	finally
    	{
    		Date dataFimProcesso = new Date();
    		Date dataTerminoProcesso = new Date();
    		dataTerminoProcesso.setTime(dataFimProcesso.getTime() - dataInicioProcesso.getTime());
    		
    		if(processOk)
    		{
    			new MailUtil().sendMail(DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_ORIGEM_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_EMAIL_ORIGEM_NOTIFICACOES"), "Sistema Integrador Nepal", DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_DESTINO_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("SMPT_EMAIL_ORIGEM_NOTIFICACOES"), "integração Nepal. Envio OPME's da Loja " + numeroEmpresaFisica + " para FTP", 
        				"Olá, a integração de envio das OPME's da Loja " + numeroEmpresaFisica + " para o FTP foi concluída.<br><br>Iniciado em " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataInicioProcesso) + " e finalizado em " + 
        				new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataFimProcesso) + " o processo durou " + new SimpleDateFormat("mm:ss").format(dataTerminoProcesso) + " (minutos:segundos).");	
    		}
    		else
    		{
    			new MailUtil().sendMail(DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_ORIGEM_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_EMAIL_ORIGEM_NOTIFICACOES"), "Sistema Integrador Nepal", DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_DESTINO_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("SMPT_EMAIL_ORIGEM_NOTIFICACOES"), "integração Nepal. Envio OPME's da Loja " + numeroEmpresaFisica + " para FTP FALHOU!!!", 
        				"Olá, a integração de envio das OPME's da Loja " + numeroEmpresaFisica + " para o FTP não foi concluída.<br><br>Iniciado em " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataInicioProcesso) + " e finalizado em " + 
        				new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataFimProcesso) + " o processo durou " + new SimpleDateFormat("mm:ss").format(dataTerminoProcesso) + " (minutos:segundos) antes de ser abortado!!!!" + 
        				"<br><br>ERRO : " + processException.toString());
    			
    			System.exit(-1);
    		}
    	} 
    }

	public void enviarTabelasGerais(Integer sequencial) throws Exception
	{	
		Exception processException = null;
		boolean processOk = false;
		
    	Date dataInicioProcesso = new Date();	
    	String numeroEmpresaFisica = String.format ("%02d", DsvConstante.getParametrosSistema().getInt("EMPRESA_LOCAL"));
		 
		try
		{
			Logger.getGlobal().info("Initializing sync to F" + numeroEmpresaFisica);
			
			String nomeArquivoDump = "dump_TG_" + numeroEmpresaFisica + "_" + new SimpleDateFormat("yyyyMMdd").format(new Date()) + "_" + sequencial.toString() + ".NEPALDUMP";
			String diretorioArquivoDump = DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump;
			
			File arquivoDump = new File(diretorioArquivoDump);
			
			if(arquivoDump.exists())
				arquivoDump.delete();
			
			arquivoDump = new File(diretorioArquivoDump.replace("NEPALDUMP", "rar"));
			
			if(arquivoDump.exists())
				arquivoDump.delete();			
			 
			Logger.getGlobal().info("Initializing Dump [" + nomeArquivoDump + "] in " + diretorioArquivoDump);
			
			String tables = "avalista boletobancario caixadia cliente chequeRecebido comissaocobrador condicional conjuge duplicata duplicataacordo duplicataparcela emailcliente itemcondicional itemnotaauxnfe itemnotacofinsnfe itemnotafiscal itemnotaicmsnfe itemnotaiinfe itemnotapisnfe itempedidovenda " +
					"movimentocaixadia movimentoparceladuplicata notafiscal notafiscalcomplnfe notafiscallancamentocontareceber operacaorecebimentoduplicata pagamentopedidovenda parcelapagamentocartao pedidovenda valorfechamentocaixadia vendedor usuario ";
			
			Logger.getGlobal().info("Tables to Dump " + tables);

			new UtilDatabase().generateBackupFile("nepalignloja" + numeroEmpresaFisica, tables, diretorioArquivoDump);
			
			Logger.getGlobal().info("Dump created. Compressing.");
			
			IgnUtil.compactarArquivo(diretorioArquivoDump, diretorioArquivoDump.replace("NEPALDUMP", "rar"));
			
			Logger.getGlobal().info("Compressed. Uploading");
			 
			new FtpUtil().uploadFile(diretorioArquivoDump.replace("NEPALDUMP", "rar"), nomeArquivoDump.replace("NEPALDUMP", "rar"), DsvConstante.getParametrosSistema().get("INTEGRACAO_IP_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_USUARIO_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_FTP"));
			
			Logger.getGlobal().info("Process complete.");
			
			processOk = true;
		}
		catch(Exception ex)
		{
			processOk = false;
			processException = ex;
			
			ex.printStackTrace();
		}
		finally 
		{
    		Date dataFimProcesso = new Date();
    		Date dataTerminoProcesso = new Date();
    		dataTerminoProcesso.setTime(dataFimProcesso.getTime() - dataInicioProcesso.getTime());
    		
			if(processOk)
			{
        		new MailUtil().sendMail(DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_ORIGEM_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_EMAIL_ORIGEM_NOTIFICACOES"), "Sistema Integrador Nepal", DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_DESTINO_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("SMPT_EMAIL_ORIGEM_NOTIFICACOES"), "integração Nepal. Envio TG's da Loja " + numeroEmpresaFisica + " para FTP", 
        				"Olá, a integração de envio das TG's da Loja " + numeroEmpresaFisica + " para o FTP foi concluída.<br><br>Iniciado em " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataInicioProcesso) + " e finalizado em " + 
        				new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataFimProcesso) + " o processo durou " + new SimpleDateFormat("mm:ss").format(dataTerminoProcesso) + " (minutos:segundos).");	
			}
			else
			{
				new MailUtil().sendMail(DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_ORIGEM_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_EMAIL_ORIGEM_NOTIFICACOES"), "Sistema Integrador Nepal", DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_DESTINO_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("SMPT_EMAIL_ORIGEM_NOTIFICACOES"), "integração Nepal. Envio TG's da Loja " + numeroEmpresaFisica + " para FTP FALHOU!!!", 
        				"Olá, a integração de envio das TG's da Loja " + numeroEmpresaFisica + " para o FTP não foi concluída.<br><br>Iniciado em " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataInicioProcesso) + " e finalizado em " + 
        				new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataFimProcesso) + " o processo durou " + new SimpleDateFormat("mm:ss").format(dataTerminoProcesso) + " (minutos:segundos) antes de ser abortado!!!!" + 
        				"<br><br>ERRO : " + processException.toString());
				
				System.exit(-1);
			}	
		}
	}
}