package br.com.desenv.nepalign.integracao;

import flex.messaging.io.amf.client.AMFConnection;
import flex.messaging.io.amf.client.exceptions.ClientStatusException;
import flex.messaging.io.amf.client.exceptions.ServerStatusException;

public class AMFConnector extends AMFConnection 
{
	public void connect(final String url) throws ClientStatusException
	{
		super.connect(url);
		super.addHttpRequestHeader("Content-type", "application/x-amf");
		
		try 
		{
			if(!(boolean) call(Util.SERVICE_AUTH_USER, Util.generateCredentials()))
				throw new IllegalAccessException("Acesso negado!");
		}
		catch (ServerStatusException | IllegalAccessException e) 
		{
			throw new ClientStatusException(e.getMessage(), ClientStatusException.AMF_CONNECT_FAILED_CODE);
		}
	}
}
