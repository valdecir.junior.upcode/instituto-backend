package br.com.desenv.nepalign.integracao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.desenv.nepalign.model.Cidade;
import br.com.desenv.nepalign.model.Cliente;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.FormaEntrega;
import br.com.desenv.nepalign.model.Pais;
import br.com.desenv.nepalign.model.PedidoVenda;
import br.com.desenv.nepalign.model.SituacaoPedidoVenda;
import br.com.desenv.nepalign.model.StatusPedido;
import br.com.desenv.nepalign.model.TipoLogradouro;
import br.com.desenv.nepalign.model.TipoMovimentacaoEstoque;
import br.com.desenv.nepalign.model.Uf;
import br.com.desenv.nepalign.model.Vendedor;
import br.com.desenv.nepalign.service.CidadeService;
import br.com.desenv.nepalign.service.ClienteService;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.FormaEntregaService;
import br.com.desenv.nepalign.service.LogOperacaoNepalService;
import br.com.desenv.nepalign.service.PaisService;
import br.com.desenv.nepalign.service.PedidoVendaService;
import br.com.desenv.nepalign.service.SituacaoPedidoVendaService;
import br.com.desenv.nepalign.service.StatusPedidoService;
import br.com.desenv.nepalign.service.TipoLogradouroService;
import br.com.desenv.nepalign.service.TipoMovimentacaoEstoqueService;
import br.com.desenv.nepalign.service.UfService;
import br.com.desenv.nepalign.service.VendedorService;
import br.com.desenv.nepalign.util.DsvConstante;

import com.desenv.nepal.catalog.xmlData.ReturnData;
import com.desenv.nepal.integracao.util.DesenvXMLUtil;
import com.desenv.nepal.order.wsdl.Order;
import com.desenv.nepal.order.wsdl.OrderSoap;
import com.desenv.nepal.order.xmlData.OrderData;
import com.desenv.nepal.order.xmlData.OrderDataRet;
import com.desenv.nepal.order.xmlData.OrderStatusData;
import com.desenv.nepal.order.xmlData.OrderTrackingData;


public class IntegracaoPedido
{
	private TipoMovimentacaoEstoqueService tipoMovimentacaoEstoqueService = null;
	private SituacaoPedidoVendaService situacaoPedidoVenda = null;
	private IntegracaoItemPedidoVenda intPedidoVenda = null; 
	private FormaEntregaService formaEntregaService = null;
	private PedidoVendaService pedidoVendaService = null;
	private VendedorService vendedorService = null;
	private ClienteService clienteService = null;
	private CidadeService cidadeService = null;
	private UfService ufService = null;
	
	private OrderSoap orderSoap = null;
	private Order order = null;	
	
	private String webServiceUser = DsvConstante.getInstanceParametrosSistema().getUsuarioWebServiceOrder();
	private String webServicePassword = DsvConstante.getInstanceParametrosSistema().getSenhaWebServiceOrder();
	
	public static void main(String[] args) throws Exception
	{
		IntegracaoPedido integracao = new IntegracaoPedido();
		integracao.integrarPedidos();
	}
	
	public IntegracaoPedido() throws Exception
	{
		this.tipoMovimentacaoEstoqueService = new TipoMovimentacaoEstoqueService();
		this.situacaoPedidoVenda = new SituacaoPedidoVendaService();
		this.formaEntregaService = new FormaEntregaService();
		this.pedidoVendaService = new PedidoVendaService();
		this.clienteService = new ClienteService();
		this.cidadeService = new CidadeService();
		this.vendedorService = new VendedorService();
		this.intPedidoVenda = new IntegracaoItemPedidoVenda();
		this.order = new Order();
		this.ufService = new UfService();
		this.orderSoap = order.getOrderSoap();
	}
	
	public void integrarPedidos()
	{
		try
		{
			Logger.getGlobal().log(Level.INFO, "Pesquisando novos pedidos...");
			ArrayList<OrderDataRet> arrayOrders = this.getNewOrders();
			
			if(arrayOrders.size() < 1)
				Logger.getGlobal().log(Level.INFO, "Nenhum novo pedido foi encontrado.");
			
			for (int i = 0; i < arrayOrders.size(); i++)
			{
				OrderDataRet pedidoVertis = (OrderDataRet)arrayOrders.get(i);
				PedidoVenda pedidoVertisParaSalvar = this.montraObjectoOrdem(pedidoVertis);
				this.pedidoVendaService.salvar(pedidoVertisParaSalvar);
				this.atualizarPedido(pedidoVertis.getIdOrder());
				OrderData orderData = new OrderData();
				orderData.setIdOrder(pedidoVertis.getIdOrder());
				this.intPedidoVenda.integrarItemPedidoVendaPorPedidoVenda(orderData);
				Logger.getGlobal().log(Level.INFO, "Pedido [Nepal] número " + pedidoVertisParaSalvar.getId() + " salvo.");
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			System.out.println(ex.getMessage());
		}
	}
	
	public ArrayList<OrderDataRet> getNewOrders() throws Exception
	{
		try
		{
			ArrayList<OrderDataRet> buffer = new ArrayList<OrderDataRet>();
			ArrayList<Integer> orders = (ArrayList<Integer>)new DesenvXMLUtil().XMLToArray(orderSoap.getNewOrders(this.webServiceUser, this.webServicePassword).split("\n"), "IdOrder");
			for(int i = 0; i < orders.size(); i++)
			{
				OrderData oData = new OrderData();
				oData.setIdOrder(orders.get(i));
				
				String xmlDataClient = (String)new DesenvXMLUtil().objectToXML(oData, OrderData.class);
				String returnXMLData = orderSoap.getOrder(xmlDataClient, this.webServiceUser, this.webServicePassword);
				
				try
				{
					ReturnData returnData = new DesenvXMLUtil().XMLToObjectReturnData(new StringBuffer(returnXMLData));
					throw new Exception(returnData.getDsErr() + " - Pedido número " + oData.getIdOrder());
				}
				catch(Exception ex)
				{
					if(ex.getMessage().contains("- Pedido "))
					{
						new LogOperacaoNepalService().logaAe(3, DsvConstante.getInstanceParametrosSistema().getTipoOperacaoImportPedidoVenda(), ex.getMessage());
						throw ex;
					}
				}
				
				OrderDataRet dataRet = (OrderDataRet)new DesenvXMLUtil().XMLToObject(new StringBuffer(returnXMLData), OrderDataRet.class);
				buffer.add(dataRet);
				Logger.getGlobal().log(Level.INFO, "Pedido [Vertis] número " + dataRet.getIdOrder() + " encontrado...");
			}
			return buffer;
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
	
	public PedidoVenda montraObjectoOrdem(OrderDataRet novoOrder) throws Exception
	{
		try
		{
			Integer i;
			
			PedidoVenda pedidoVenda = new PedidoVenda();
			Cidade cidade = new Cidade();
			cidade.setNome(novoOrder.getDsCity());
			
			Uf uf = new Uf();
			uf.setSigla(novoOrder.getIdState());
			List<Uf> resultUf = ufService.listarPorObjetoFiltro(uf);
			
			if(resultUf.size() > 0)
			{
				uf = (Uf)resultUf.get(0);
				cidade.setUf(uf);
			}
			else
			{
				//throw new Exception("NÃ£o foi possÃ­vel encontrar a UF com sigla : " + uf.getSigla());
				pedidoVenda.setObservacao("UF não encontrada : " + uf.getSigla());
			}

			
			List<Cidade> listCidades = cidadeService.listarPorObjetoFiltro(cidade);
			if(listCidades.size() > 0)
			{
				cidade = (Cidade) cidadeService.listarPorObjetoFiltro(cidade).get(0);
				pedidoVenda.setCidadeEntrega(cidade);
			}
			else
			{
				pedidoVenda.setObservacao(pedidoVenda.getObservacao() + " - Cidade nÃ£o encontrada : " + novoOrder.getDsCity());
			}
				
			
			Cliente cliente = new Cliente();
			clienteService = new ClienteService();
			cliente.setCodigoExterno(novoOrder.getIdCustomer());
			cliente = (Cliente)new IntegracaoCliente().integrarClientesPorId(cliente.getCodigoExterno());
			
			//emrpesa padrao id 10
			
			EmpresaFisica empresaFisica = new EmpresaFisica();
			empresaFisica.setId(DsvConstante.getInstanceParametrosSistema().getPedidoVenda_integracao_empresaFisicaPadrao());
			EmpresaFisicaService empresaFisicaService = new EmpresaFisicaService();
			List<EmpresaFisica> resultEmpresaFisica = empresaFisicaService.listarPorObjetoFiltro(empresaFisica);
			if(resultEmpresaFisica.size() > 0)
			{
				empresaFisica = (EmpresaFisica) empresaFisicaService.listarPorObjetoFiltro(empresaFisica).get(0);
			}
			else
			{
				throw new Exception("NÃ£o foi possÃ­vel encontrar a empresa de nÃºmero : " + empresaFisica.getId());
			}
			

			
			//em aberto padrao id 2
			SituacaoPedidoVenda situacaoPedidoVenda = new SituacaoPedidoVenda();

			situacaoPedidoVenda.setId(DsvConstante.getInstanceParametrosSistema().getPedidoVenda_integracao_situacaoPedidoVendaPadrao());
			List<SituacaoPedidoVenda> resultSituacaoPedidoVenda = this.situacaoPedidoVenda.listarPorObjetoFiltro(situacaoPedidoVenda);
			if(resultSituacaoPedidoVenda.size() > 0)
			{
				situacaoPedidoVenda = resultSituacaoPedidoVenda.get(0);
			}
			else
			{
				throw new Exception("NÃ£o foi possÃ­vel encontrar a situaÃ§Ã£o do pedido de venda de nÃºmero : " + situacaoPedidoVenda.getId());
			}
					
			//padrao id 2
			TipoMovimentacaoEstoque tipoMovimentacaoEstoque = new TipoMovimentacaoEstoque();
			tipoMovimentacaoEstoque.setId(DsvConstante.getInstanceParametrosSistema().getPedidoVenda_integracao_tipoMovimentacaoEstoquePadrao());
			List<TipoMovimentacaoEstoque> resultTipoMovimentacaoEstoque = tipoMovimentacaoEstoqueService.listarPorObjetoFiltro(tipoMovimentacaoEstoque);
			if(resultTipoMovimentacaoEstoque.size() > 0)
			{
				tipoMovimentacaoEstoque = resultTipoMovimentacaoEstoque.get(0);
			}
			else
			{
				throw new Exception("NÃ£o foi possÃ­vel achar o tipo de movimentacao de estoque de nÃºmero : " + tipoMovimentacaoEstoque.getId());
			}
					
			
			FormaEntrega formaEntrega = new FormaEntrega();
			formaEntrega.setDescricao(novoOrder.getDsShippingMethod());
			List<FormaEntrega> resultFormaEntrega = formaEntregaService.listarPorObjetoFiltro(formaEntrega);
			if(resultFormaEntrega.size() > 0)
				formaEntrega = (FormaEntrega)resultFormaEntrega.get(0);
			else
			{
				throw new Exception("NÃ£o foi possÃ­vel encontrar a forma de entrega com descriÃ§Ã£o : " + novoOrder.getDsShippingMethod());
			}
			
			//MetodoPagamento metodoPagamento = null;
			//Segunda Versao
			/*MetodoPagamento metodoPagamento = new MetodoPagamento();
			MetodoPagamentoService metodoPagamentoService = new MetodoPagamentoService();
			metodoPagamento.setId(novoOrder.getIdPayMethod());
			List<MetodoPagamento> resultMetodoPagamento = metodoPagamentoService.listarPorObjetoFiltro(metodoPagamento);
			if(resultMetodoPagamento.size() > 0)
				metodoPagamento = (MetodoPagamento)resultMetodoPagamento.get(0);
			else
			{
				throw new Exception("NÃ£o foi possÃ­vel encontrar o mÃ©todo de pagamento com id : " + novoOrder.getIdPayMethod());
			}*/
			
			
			Pais pais = new Pais();
			PaisService paisService = new PaisService();
			pais.setId(1);
			List<Pais> resultPais = paisService.listarPorObjetoFiltro(pais);
			if(resultPais.size() > 0)
				pais = (Pais) paisService.listarPorObjetoFiltro(pais).get(0);
			else
			{
				throw new Exception("NÃ£o foi possível encontrar o pais com id : 1 (BRASIL)");
			}
			
			StatusPedido statusPedido = new StatusPedido();
			StatusPedidoService statusPedidoService = new StatusPedidoService();
			statusPedido.setId(novoOrder.getIdOrderStatus());	
			List<StatusPedido> resultStatusPedido = statusPedidoService.listarPorObjetoFiltro(statusPedido);
			if(resultStatusPedido.size() > 0)
				statusPedido = (StatusPedido)resultStatusPedido.get(0);
			else
			{
				throw new Exception("NÃ£o foi possível encontrar o status do pedido com id : " + novoOrder.getIdOrderStatus());
			}
			
			
			TipoLogradouro tipoLogradouro  = new TipoLogradouro();
			TipoLogradouroService tipoLogradouroService = new TipoLogradouroService();
			tipoLogradouro.setDescricao(novoOrder.getDsAddressType());
			List<TipoLogradouro> resultTipoLogradouro = tipoLogradouroService.listarPorObjetoFiltro(tipoLogradouro);
			if(resultTipoLogradouro.size() > 0)
				tipoLogradouro = (TipoLogradouro)resultTipoLogradouro.get(0);
			else
			{
				throw new Exception("NÃ£o foi possível encontrar o tipo de logradouro com descricÃ£o : " + novoOrder.getDsAddressType());
			}
			
			/*//Segunda Versao
			MeioPagamento meioPagamento = new MeioPagamento();
			MeioPagamentoService meioPagamentoService = new MeioPagamentoService();
			meioPagamento.setId(novoOrder.getIdPaymentMethodType());
			meioPagamento = (MeioPagamento) meioPagamentoService.listarPorObjetoFiltro(meioPagamento).get(0);*/
			
			Vendedor vendedor = new Vendedor();
			vendedor.setId(DsvConstante.getInstanceParametrosSistema().getPedidoVenda_integracao_vendedorPadrao());
			List<Vendedor> resultVendedor = this.vendedorService.listarPorObjetoFiltro(vendedor);
			if(resultVendedor.size() > 0)
			{
				vendedor = resultVendedor.get(0);
			}
			else
			{
				throw new Exception("NÃ£o foi possÃ­vel encontrar o vendedor com id : " + vendedor.getId());
			}
			
			if(novoOrder.getDsName() != null && novoOrder.getDsName().isEmpty())
			{
				pedidoVenda.setDestinatarioPedido(cliente.getNome());
			}
			else
			{
				pedidoVenda.setDestinatarioPedido(novoOrder.getDsName());
			}
			
			pedidoVenda.setCidadeEntrega(cidade);
			pedidoVenda.setCodigoExterno(novoOrder.getIdOrder());
			pedidoVenda.setCliente(cliente);
			pedidoVenda.setFormaEntrega(formaEntrega);
			pedidoVenda.setMetodoPagamento(null);		
			pedidoVenda.setPaisEntrega(pais);		
			pedidoVenda.setStatusPedido(statusPedido);
			pedidoVenda.setTipoLogradouroEnderecoEntrega(tipoLogradouro);
			pedidoVenda.setTipoMeioPagamento(null);
			pedidoVenda.setUfEntrega(uf);
			pedidoVenda.setVendedor(vendedor);
			
			
			pedidoVenda.setTipoMovimentacaoEstoque(tipoMovimentacaoEstoque);
			
			pedidoVenda.setUsuario(null);
			
			pedidoVenda.setSituacaoPedidoVenda(situacaoPedidoVenda);
			
			//ignora para segunda versÃƒÂ¯Ã‚Â¿Ã‚Â½o
			pedidoVenda.setPlanoPagamento(null);
			
			pedidoVenda.setBairroEnderecoEntrega(novoOrder.getDsDistrict());
			pedidoVenda.setCepEnderecoEntrega(novoOrder.getDsZip());
			
			pedidoVenda.setCodigoPedidoEnviadoCliente(novoOrder.getDsDispId());
			pedidoVenda.setComplementoEnderecoEntrega(novoOrder.getDsComplement());
			pedidoVenda.setDataEntregaPedido(novoOrder.getDtDeliveryDay().toGregorianCalendar().getTime());
			pedidoVenda.setDataExpiracaoPedido(novoOrder.getDtExpires().toGregorianCalendar().getTime());
			pedidoVenda.setDataVenda(novoOrder.getDtOrder().toGregorianCalendar().getTime());
			pedidoVenda.setDddTelefone(novoOrder.getDsPhoneDDD());		
			i = novoOrder.getVlOrderDiscount();	
			pedidoVenda.setDescontoTotal(i.doubleValue()/100);
			
			
			pedidoVenda.setEmpresaFisica(empresaFisica);
			pedidoVenda.setLogradouroEnderecoEntrega(novoOrder.getDsAddress());
			pedidoVenda.setNumCupomDesconto(novoOrder.getNuGiftPayment());
			pedidoVenda.setNumEnderecoEntrega(novoOrder.getDsNumber());
			
			//dispid
			pedidoVenda.setNumeroControle(novoOrder.getDsDispId());
			pedidoVenda.setNumParcelas(novoOrder.getNuParcelsNumber());
			pedidoVenda.setNumTelefone(novoOrder.getDsPhone());
			pedidoVenda.setReferenciaEnderecoEntrega(novoOrder.getDsReferenceAddress());
			i = novoOrder.getVlOrderDiscount();	
			pedidoVenda.setTotalDescPedido(i.doubleValue()/100);
			i= novoOrder.getVlGiftPayment();
			pedidoVenda.setValorCupomDesconto(i.doubleValue()/100);
			i = novoOrder.getVlGiftPackPrice();
			pedidoVenda.setValorEmbalagemPresente(i.doubleValue()/100);
			i = novoOrder.getVlInterest();
			pedidoVenda.setValorJuros(i.doubleValue()/100);
			i = novoOrder.getVlParcelsValue();
			pedidoVenda.setValorParcela(i.doubleValue()/100);
			i= novoOrder.getVlTotalShipCost();
			pedidoVenda.setValorTotalCustoFrete(i.doubleValue()/100);
			i=novoOrder.getVlShipCostDiscount();
			pedidoVenda.setValorTotalDescFrete(i.doubleValue()/100);
			i = novoOrder.getVlProductDiscount();
			pedidoVenda.setValorTotalDescProdutos(i.doubleValue()/100);
			i= novoOrder.getVlTotalOrder();
			pedidoVenda.setValorTotalPedido(i.doubleValue()/100);
			i = novoOrder.getVlTotalProduct();
			pedidoVenda.setValorTotalProdutosPedidos(i.doubleValue()/100);
			
			return pedidoVenda;
		}
		catch(Exception ex)
		{
			new LogOperacaoNepalService().logaAe(3, DsvConstante.getInstanceParametrosSistema().getTipoOperacaoImportPedidoVenda(), ex.getMessage());
			throw ex;
		}
		
	}
	
	public int alterarStatusPedido(int idOrder, int idStatus) throws Exception
	{
		try
		{
			OrderStatusData sData = new OrderStatusData();
			sData.setIdOrder(idOrder);
			sData.setIdOrderStatus(idStatus);
			String xmlToSend = (String)new DesenvXMLUtil().objectToXML(sData, OrderStatusData.class);
			String returnXML = this.orderSoap.setOrderStatus(xmlToSend, this.webServiceUser, this.webServicePassword);
			

			ReturnData returnData =  (ReturnData)new DesenvXMLUtil().XMLToObjectReturnData(new StringBuffer(returnXML));
			
			if(!returnData.getDsErr().equals(""))
			{
				throw new Exception(returnData.getDsErr() + " - Pedido número : " + idOrder);
			}
			
			return returnData.getIdRet();
		}
		catch(Exception ex)
		{
			new LogOperacaoNepalService().logaAe(3, DsvConstante.getInstanceParametrosSistema().getTipoOperacaoImportPedidoVenda(), ex.getMessage());
			throw ex;
		}
	}
	
	public void atualizarPedido(int idOrder) throws Exception
	{
		try
		{
			OrderData oData = new OrderData();
			oData.setIdOrder(idOrder);
			String xmlToSend = (String)new DesenvXMLUtil().objectToXML(oData, OrderData.class);
			String xmlReturn = this.orderSoap.setOrderImported(xmlToSend, this.webServiceUser, this.webServicePassword);
			ReturnData returnData = new DesenvXMLUtil().XMLToObjectReturnData(new StringBuffer(xmlReturn));
			if(!returnData.getDsErr().equals(""))
			{
				throw new Exception(returnData.getDsErr() + " - Pedido nÃºmero : " + returnData.getIdRet());
			}
		}
		catch(Exception ex)
		{
			new LogOperacaoNepalService().logaAe(3, DsvConstante.getInstanceParametrosSistema().getTipoOperacaoImportPedidoVenda(), ex.getMessage());
			throw ex;
		}
	}
	
	public void atualizarCodigoRastreamentoPedido(Integer idOrder, String trackingCode) throws Exception
	{
		try
		{
			OrderTrackingData orderTrackingData = new OrderTrackingData();
			orderTrackingData.setIdOrder(idOrder.intValue());
			orderTrackingData.setTrackingNumber(trackingCode);
			String xmlToSend = (String)new DesenvXMLUtil().objectToXML(orderTrackingData, OrderTrackingData.class);
			String xmlReturn = this.orderSoap.setOrderTracking(xmlToSend, this.webServiceUser, this.webServicePassword);
			ReturnData returnData = new DesenvXMLUtil().XMLToObjectReturnData(new StringBuffer(xmlReturn));
			if(!returnData.getDsErr().equals(""))
			{
				throw new Exception(returnData.getDsErr() + " - Pedido nÃºmero : " + returnData.getIdRet());
			}
		}
		catch(Exception ex)
		{
			new LogOperacaoNepalService().logaAe(3, DsvConstante.getInstanceParametrosSistema().getTipoOperacaoImportPedidoVenda(), ex.getMessage());
		}
	}

	public ArrayList<OrderDataRet> getOrder(int orderNumber) throws Exception
	{
		ArrayList<OrderDataRet> buffer = new ArrayList<OrderDataRet>();
		try
		{
			OrderData oData = new OrderData();
			oData.setIdOrder(orderNumber);
			
			String xmlDataClient = (String)new DesenvXMLUtil().objectToXML(oData, OrderData.class);
			String returnXMLData = orderSoap.getOrder(xmlDataClient, this.webServiceUser, this.webServicePassword);
			
			try
			{
				ReturnData returnData = new DesenvXMLUtil().XMLToObjectReturnData(new StringBuffer(returnXMLData));
				throw new Exception(returnData.getDsErr() + " - Pedido nÃºmero : " + orderNumber);
			}
			catch(Exception ex)
			{
				if(ex.getMessage().contains("- Pedido"))
				{
					new LogOperacaoNepalService().logaAe(3, DsvConstante.getInstanceParametrosSistema().getTipoOperacaoImportPedidoVenda(), ex.getMessage());
					throw ex;
				}
			}
			
			OrderDataRet dataRet = (OrderDataRet)new DesenvXMLUtil().XMLToObject(new StringBuffer(returnXMLData), OrderDataRet.class);
			buffer.add(dataRet);
			System.out.println("Pedido nÃºmero " + dataRet.getIdOrder() + " encontrado...");
			return buffer;

		}
		catch(Exception ex)
		{
			throw ex;
		}
	}	
}
