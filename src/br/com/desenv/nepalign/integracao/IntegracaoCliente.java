package br.com.desenv.nepalign.integracao;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import com.desenv.nepal.catalog.xmlData.ReturnData;
import com.desenv.nepal.integracao.util.DesenvXMLUtil;
import com.desenv.nepal.order.wsdl.Order;
import com.desenv.nepal.order.wsdl.OrderSoap;
import com.desenv.nepal.order.xmlData.ClientData;
import com.desenv.nepal.order.xmlData.ClientDataRet;

import br.com.desenv.nepalign.model.Cidade;
import br.com.desenv.nepalign.model.Cliente;
import br.com.desenv.nepalign.model.EmailCliente;
import br.com.desenv.nepalign.model.TipoLogradouro;
import br.com.desenv.nepalign.model.Uf;
import br.com.desenv.nepalign.service.CidadeService;
import br.com.desenv.nepalign.service.ClienteService;
import br.com.desenv.nepalign.service.EmailClienteService;
import br.com.desenv.nepalign.service.TipoLogradouroService;
import br.com.desenv.nepalign.service.UfService;
import br.com.desenv.nepalign.util.DsvConstante;

public class IntegracaoCliente
{	
	private OrderSoap orderSoap = null;
	private Order order = null;
	
	private CidadeService cidadeService = null;
	private ClienteService clienteService = null;
	private EmailClienteService emailClienteService = null;
	
	private String webServiceUser = DsvConstante.getInstanceParametrosSistema().getUsuarioWebServiceOrder();
	private String webServicePassword = DsvConstante.getInstanceParametrosSistema().getSenhaWebServiceOrder();
	
	public IntegracaoCliente() throws Exception
	{	
		this.order = new Order();
		this.orderSoap= order.getOrderSoap();
		this.cidadeService = new CidadeService();
		this.clienteService = new ClienteService();
		this.emailClienteService = new EmailClienteService();
	}
	
	public void integrarClientes() throws Exception
	{
		try
		{
			this.integrarNepal(this.getNewClients());
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
	
	public List<ClientDataRet> getNewClients() throws Exception
	{
		try
		{
			List<ClientDataRet> buffer = new ArrayList<ClientDataRet>();
			ArrayList<Integer> clients = new DesenvXMLUtil().XMLToArray(orderSoap.getNewClients(this.webServiceUser, this.webServicePassword).split("\n"), "IdCustomer");
			for(int i = 0; i < clients.size(); i++)
			{
				ClientData cData = new ClientData();
				cData.setIdCustomer(clients.get(i));
				String xmlDataClient = (String)new DesenvXMLUtil().objectToXML(cData, ClientData.class);
				String returnXMLData = this.orderSoap.getClient(xmlDataClient, this.webServiceUser, this.webServicePassword);
				
				ReturnData returnData = new DesenvXMLUtil().XMLToObjectReturnData(new StringBuffer(returnXMLData));
				
				if(returnData.getDsErr().equals(""))
				{
					ClientDataRet dataRet = (ClientDataRet)new DesenvXMLUtil().XMLToObject(new StringBuffer(returnXMLData), ClientDataRet.class);
					buffer.add(dataRet);
				}
				else
				{
					throw new Exception(returnData.getDsErr() + " - Cliente número : " + cData.getIdCustomer());
				}
			}
			return buffer;
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
	
	public Cliente integrarClientesPorId(int clienteId) throws JAXBException, Exception
	{
		try
		{
			ClientData cData = new ClientData();
			cData.setIdCustomer(clienteId);
			String xmlToSend = (String)new DesenvXMLUtil().objectToXML(cData, ClientData.class);
			String xmlReturn = this.orderSoap.getClient(xmlToSend, this.webServiceUser, this.webServicePassword);
			
			try
			{
				ReturnData returnData = new DesenvXMLUtil().XMLToObjectReturnData(new StringBuffer(xmlReturn));
				throw new Exception(returnData.getDsErr() + " - Cliente número : " + cData.getIdCustomer());
			}
			catch(Exception ex)
			{
				if(ex.getMessage().contains("- Cliente "))
				{
					throw ex;
				}
			}
			
			ClientDataRet clienteVertis = (ClientDataRet)new DesenvXMLUtil().XMLToObject(new StringBuffer(xmlReturn), ClientDataRet.class);
			List<ClientDataRet> paraIntegrar = new ArrayList<ClientDataRet>();
			paraIntegrar.add(clienteVertis);
			this.integrarNepal(paraIntegrar);
			
			Cliente cliente = this.montarObjetoCliente(clienteVertis);
			
			Cliente clientePesquisa = new Cliente();
			clientePesquisa.setCodigoExterno(cliente.getCodigoExterno());
			
			List<Cliente> resultClient = (List<Cliente>) new ClienteService().listarPorObjetoFiltro(clientePesquisa);
			if(resultClient.size() > 0)
			{
				Cliente resultCliente = (Cliente) resultClient.get(0);
				cliente.setId(resultCliente.getId());
				new ClienteService().atualizar(cliente);
				return new ClienteService().recuperarPorId(cliente.getId());
			}
			else
			{
				throw new Exception("Cliente recém importado não foi encontrado. Cliente número : " + clienteVertis.getIdCustomer());
			}
		}
		catch(JAXBException jaxbException)
		{
			throw jaxbException;
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
	
	
	public void integrarNepal(List<ClientDataRet> externalClient) throws Exception
	{
		try
		{
			for (int i = 0; i < externalClient.size(); i++)
			{
				ClientDataRet clienteVertis = (ClientDataRet)externalClient.get(i);
				Cliente bufferCliente = (Cliente)this.montarObjetoCliente(clienteVertis);
				Cliente clientePesquisa = new Cliente();
				clientePesquisa.setCodigoExterno(bufferCliente.getCodigoExterno());
				ArrayList listClientResult = (ArrayList)this.clienteService.listarPorObjetoFiltro(clientePesquisa);

				bufferCliente.setTipoLogradouro(null);

				if(listClientResult.size() == 0)
					this.clienteService.salvar(bufferCliente);
				else
				{
					bufferCliente = (Cliente)listClientResult.get(0);
					this.clienteService.atualizar(bufferCliente);
				}
				
				listClientResult = (ArrayList)this.clienteService.listarPorObjetoFiltro(bufferCliente);
				if(listClientResult.size() > 0)
				{
					bufferCliente = (Cliente) listClientResult.get(0);
					EmailCliente mailCliente = new EmailCliente();
					mailCliente.setEmail(clienteVertis.getDsEmail());
					mailCliente.setCliente(bufferCliente);
					if(((List<EmailCliente>)new EmailClienteService().listarPorObjetoFiltro(mailCliente)).size() == 0)
					{
						this.emailClienteService.salvar(mailCliente);
					}
					this.atualizarCliente(clienteVertis.getIdCustomer());
				}
				else
				{
					throw new Exception("Cliente recém importado não foi encontrado. Cliente número : " + clienteVertis.getIdCustomer());
				}
			}
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
	
	public void atualizarCliente(int idCliente) throws Exception
	{
		try
		{
			ClientData cData = new ClientData();
			cData.setIdCustomer(idCliente);
			String xml = (String)new DesenvXMLUtil().objectToXML(cData, ClientData.class);
			String returnXML = this.orderSoap.setClientImported(xml, this.webServiceUser, this.webServicePassword);
			ReturnData returnData = (ReturnData)new DesenvXMLUtil().XMLToObjectReturnData(new StringBuffer(returnXML));
			if(!returnData.getDsErr().equals(""))
			{
				throw new Exception(returnData.getDsErr() + " - Cliente número " + idCliente);
			}
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
	
	public Cliente montarObjetoCliente(ClientDataRet novoCliente) throws Exception
	{
		try
		{
			
			Cliente bufferCliente = new Cliente();
			bufferCliente.setBairro(novoCliente.getDsDistrict());
			bufferCliente.setCep(novoCliente.getDsZip());
			bufferCliente.setComplementoEndereco(novoCliente.getDsComplement());
			bufferCliente.setCpfCnpj(novoCliente.getDsCpf() + " " + novoCliente.getDsCNPJ()); //
			bufferCliente.setNome(novoCliente.getDsName());
			bufferCliente.setRgInscricaoEstadual(novoCliente.getDsRG());
			bufferCliente.setCodigoExterno(novoCliente.getIdCustomer());
			bufferCliente.setTelefoneCelular01(novoCliente.getDsCelularPhoneDDD() + " " +novoCliente.getDsCelularPhone());
			bufferCliente.setTelefoneResidencial(novoCliente.getDsPhoneDDD() + " " +novoCliente.getDsPhone());
			bufferCliente.setLogradouro(novoCliente.getDsAddress());
			bufferCliente.setNumeroEndereco(novoCliente.getDsNumber());
			bufferCliente.setCodigoExterno(novoCliente.getIdCustomer());
			
			TipoLogradouro tipoLogradouroFiltro = new TipoLogradouro();
			tipoLogradouroFiltro.setDescricao(novoCliente.getDsAddressType());
			List<TipoLogradouro> tipoLogradouroResult = (List<TipoLogradouro>)new TipoLogradouroService().listarPorObjetoFiltro(tipoLogradouroFiltro);
			if(tipoLogradouroResult.size() > 0)
			{
				bufferCliente.setTipoLogradouro(tipoLogradouroResult.get(0));
			}
			

			Uf ufCliente = new Uf();
			ufCliente.setSigla(novoCliente.getIdState());
			List<Uf> resultadoUf = new UfService().listarPorObjetoFiltro(ufCliente);
			if(resultadoUf.size() > 0)
			{
				ufCliente = (Uf)resultadoUf.get(0);
				Cidade cidadeClient = new Cidade();
				cidadeClient.setNome(novoCliente.getDsCity());
				cidadeClient.setUf(ufCliente);
				List<Cidade> listaResultadoCidade = this.cidadeService.listarPorObjetoFiltro(cidadeClient);
				if(listaResultadoCidade.size() > 0)
				{
					cidadeClient = (Cidade)this.cidadeService.listarPorObjetoFiltro(cidadeClient).get(0);
					bufferCliente.setCidadeEndereco(cidadeClient);
				}
				else
				{
					bufferCliente.setObservacao(bufferCliente.getObservacao() + " - Não foi possível encontrar cidade com nome : " + cidadeClient.getNome() + " e UF : " + cidadeClient.getUf().getSigla());
				}
			}
			else
			{
				bufferCliente.setObservacao(bufferCliente.getObservacao() + " Não foi possível encontrar UF com sigla : " + ufCliente.getSigla());
			}
			
			return bufferCliente;
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
}