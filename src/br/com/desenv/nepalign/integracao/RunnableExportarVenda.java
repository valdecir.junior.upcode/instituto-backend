package br.com.desenv.nepalign.integracao;

import java.util.Date;

import javax.security.sasl.AuthenticationException;

import org.apache.log4j.Logger;

import br.com.desenv.nepalign.integracao.dto.DataPackage;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class RunnableExportarVenda implements Runnable  
{
	private static final Logger logger = Logger.getLogger(RunnableExportarVenda.class);	
	private static final PedidoVendaBundle pedidoVendaBundle = new PedidoVendaBundle();
	
	private Integer idEmpresaFisica;
	private String AMF_CHANNEL = Util.getUrlAmfCentral();
	
	public RunnableExportarVenda()
	{
		try
		{
			idEmpresaFisica = DsvConstante.getParametrosSistema().getInt("EMPRESA_LOCAL");	
		}
		catch(Exception ex)
		{
			Logger.getRootLogger().error("Não foi possível inicializar o Runnable Exportar Venda", ex);
		}
	}
	
	public RunnableExportarVenda(final Integer idEmpresaFisica)
	{
		this.idEmpresaFisica = idEmpresaFisica;
	}
	
	public RunnableExportarVenda(final String amf)
	{
		this.AMF_CHANNEL = amf;
	}
	
	public RunnableExportarVenda(final Integer idEmpresaFisica, final String amf)
	{
		this.idEmpresaFisica = idEmpresaFisica;
		this.AMF_CHANNEL = amf;
	}
	
	@Override
	public void run() 
	{
		final DataPackage dataPackage = new DataPackage();
		
		try 
		{
			dataPackage.setIdEmpresaFisica(idEmpresaFisica);
			pedidoVendaBundle.exportar(ConexaoUtil.getEntityManager(), dataPackage, new Date(), new Date(), false);
			
			dataPackage.seal();
			
			logger.info("Connecting on " + AMF_CHANNEL);
			
			AMFConnector con = new AMFConnector();
			con.connect(AMF_CHANNEL);
			
			logger.info("Connected!");
			
			con.addHttpRequestHeader("Content-type", "application/x-amf");
			
			if((boolean) con.call(Util.SERVICE_AUTH_USER, Util.generateCredentials()))
				logger.info("Service authenticated!");
			else
				throw new AuthenticationException(" Failure! ");
			
			logger.info("Sending data AS " + idEmpresaFisica);
			con.call("snapshotVendaService.deployPackage", dataPackage);
			
			logger.info("Sent.");
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
}