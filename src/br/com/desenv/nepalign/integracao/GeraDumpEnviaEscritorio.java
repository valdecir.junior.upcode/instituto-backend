package br.com.desenv.nepalign.integracao;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.OperacaoMovimentacaoEstoque;
import br.com.desenv.nepalign.service.OperacaoMovimentacaoEstoqueService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.util.FtpUtil;
import br.com.desenv.nepalign.util.MailUtil;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class GeraDumpEnviaEscritorio
{
    public static void main(String[] args) throws Exception
    {
    	new GeraDumpEnviaEscritorio().criarDumpCompactarEnviarViaFTP(Integer.parseInt(args[0x0]));
    } 
     
    public GeraDumpEnviaEscritorio() throws Exception
    {
    	DsvConstante.getParametrosSistema();
    }
    
    public void criarDumpCompactarEnviarViaFTP(Integer sequencial) throws Exception
    { 
    	Exception processException = null;
    	EntityManager manager = null;
    	EntityTransaction transaction = null;
    	Date dataInicioProcesso = new Date();	

		String numeroEmpresaFisica = String.format ("%02d", DsvConstante.getParametrosSistema().getInt("EMPRESA_LOCAL"));
		
		boolean processOk = false;
    	
    	try
    	{	
			manager = ConexaoUtil.getEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			
    		String nomeArquivoDump = "dump_OPME_" + numeroEmpresaFisica + "_" + new SimpleDateFormat("yyyyMMdd").format(new Date()) + "_" + sequencial + ".NEPALDUMP";
    		String diretorioArquivoDump = DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump;
    		
    		generateBackupFile("nepalignloja" +  numeroEmpresaFisica, diretorioArquivoDump, "operacaoMovimentacaoEstoque");    	
    		IgnUtil.compactarArquivo(diretorioArquivoDump, diretorioArquivoDump.replace("NEPALDUMP", "rar"));
    		new FtpUtil().uploadFile(diretorioArquivoDump.replace("NEPALDUMP", "rar"), nomeArquivoDump.replace("NEPALDUMP", "rar"), DsvConstante.getParametrosSistema().get("INTEGRACAO_IP_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_USUARIO_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_FTP"));
    		
    		OperacaoMovimentacaoEstoqueService operacaoMovimentacaoEstoqueService = new OperacaoMovimentacaoEstoqueService();
    		OperacaoMovimentacaoEstoque operacaoMovimentacaoEstoqueFiltro = new OperacaoMovimentacaoEstoque();
    		operacaoMovimentacaoEstoqueFiltro.setFlagSincronizacao("0");
    		List<OperacaoMovimentacaoEstoque> listaOperacaoMovimentacaoEstoque = operacaoMovimentacaoEstoqueService.listarPorObjetoFiltro(operacaoMovimentacaoEstoqueFiltro, "idOperacao", "asc");
    		    
		    
        	FileWriter arquivoSaida = new FileWriter(DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + numeroEmpresaFisica + "_backup_registro.sql");
        	BufferedWriter bufferSaida = new BufferedWriter(arquivoSaida);
        
        	for (OperacaoMovimentacaoEstoque operacaoMovimentacaoEstoque : listaOperacaoMovimentacaoEstoque)
        	{
        	    bufferSaida.write("update nepalign.operacaomovimentacaoestoque set flagSincronizacao = '0' where idOperacao = " + operacaoMovimentacaoEstoque.getId() + ";");
        	    bufferSaida.newLine();
        	    operacaoMovimentacaoEstoque.setFlagSincronizacao("1");
        	    operacaoMovimentacaoEstoqueService.atualizar(operacaoMovimentacaoEstoque, false);
        	}
        	
        	bufferSaida.close();
        	arquivoSaida.close();
        	transaction.commit();
        	
        	processOk = true;
    	}
    	catch(Exception ex)
    	{
		    if (transaction!=null)
		    	transaction.rollback();
		    
		    processOk = false;
		    processException = ex; 
    	}
    	finally
    	{
    		Date dataFimProcesso = new Date();
    		Date dataTerminoProcesso = new Date();
    		dataTerminoProcesso.setTime(dataFimProcesso.getTime() - dataInicioProcesso.getTime());
    		
    		if(processOk)
    		{
    			new MailUtil().sendMail(DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_ORIGEM_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_EMAIL_ORIGEM_NOTIFICACOES"), "Sistema Integrador Nepal", DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_DESTINO_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("SMPT_EMAIL_ORIGEM_NOTIFICACOES"), "integração Nepal. Envio OPME's da Loja " + numeroEmpresaFisica + " para FTP", 
        				"Olá, a integração de envio das OPME's da Loja " + numeroEmpresaFisica + " para o FTP foi concluída.<br><br>Iniciado em " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataInicioProcesso) + " e finalizado em " + 
        				new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataFimProcesso) + " o processo durou " + new SimpleDateFormat("mm:ss").format(dataTerminoProcesso) + " (minutos:segundos).");	
    		}
    		else
    		{
    			new MailUtil().sendMail(DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_ORIGEM_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_EMAIL_ORIGEM_NOTIFICACOES"), "Sistema Integrador Nepal", DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_DESTINO_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("SMPT_EMAIL_ORIGEM_NOTIFICACOES"), "integração Nepal. Envio OPME's da Loja " + numeroEmpresaFisica + " para FTP FALHOU!!!", 
        				"Olá, a integração de envio das OPME's da Loja " + numeroEmpresaFisica + " para o FTP não foi concluída.<br><br>Iniciado em " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataInicioProcesso) + " e finalizado em " + 
        				new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataFimProcesso) + " o processo durou " + new SimpleDateFormat("mm:ss").format(dataTerminoProcesso) + " (minutos:segundos) antes de ser abortado!!!!" + 
        				"<br><br>ERRO : " + processException.toString());
    		}
    	} 
    }

    private void generateBackupFile(String database, String outputFile, String listaTabela) throws Exception
    {
		try
		{
		    File file = new File(outputFile);
	
		    if (file.exists())
		    	throw new Exception("O arquivo " + outputFile + " já existe.");
	
		    EntityManagerFactory managerFactory = ConexaoUtil.getFactory();
	
		    String connectionUsername = managerFactory.getProperties().get("javax.persistence.jdbc.user").toString();
		    String connectionPassword = managerFactory.getProperties().get("javax.persistence.jdbc.password").toString();
		    String connectionUrl = managerFactory.getProperties().get("javax.persistence.jdbc.url").toString();
	
		    String dumpCommand = "mysqldump --hex-blob --opt -h " + connectionUrl.split("//")[1].split("/")[0].split(":")[0] + " " + " -u " + connectionUsername + " -p" + connectionPassword + "  " + database + " --default-character-set=utf8 " + listaTabela;	    
		    
		    System.out.println(dumpCommand); 
		    
		    Logger.getGlobal().log(Level.INFO, "Iniciando backup do Banco de Dados " + database + " no diretório " + file.getPath());
	
		    try
		    {
				Process child = Runtime.getRuntime().exec(dumpCommand);
		
				PrintStream ps = new PrintStream(file);
				InputStream in = child.getInputStream();
		
				int ch;
		
				while ((ch = in.read()) != -1)
				{
				    ps.write(ch);
				}
		
				child.destroy();
				ps.close();
				in.close();
		
				child = null;
				ps = null;
				in = null;
		    } 
		    catch (Exception exc)
		    {
		    	throw exc;
		    }
	
		    Logger.getGlobal().log(Level.INFO, "Backup terminado.");
		} 
		catch (Exception ex)
		{
		    ex.printStackTrace();
		    throw ex;
		}
    }
}
