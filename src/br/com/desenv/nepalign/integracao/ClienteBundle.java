package br.com.desenv.nepalign.integracao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.integracao.dto.AvalistaDTO;
import br.com.desenv.nepalign.integracao.dto.ClienteDTO;
import br.com.desenv.nepalign.integracao.dto.ConjugeDTO;
import br.com.desenv.nepalign.integracao.dto.DataPackage;
import br.com.desenv.nepalign.integracao.dto.EmailClienteDTO;
import br.com.desenv.nepalign.integracao.dto.HistoricoCobrancaDTO;
import br.com.desenv.nepalign.integracao.dto.ImagemUsuarioDTO;
import br.com.desenv.nepalign.model.Cliente;
import br.com.desenv.nepalign.model.EmailCliente;
import br.com.desenv.nepalign.model.HistoricoCobranca;
import br.com.desenv.nepalign.model.ImagemUsuario;
import br.com.desenv.nepalign.service.ImagemUsuarioService;
import br.com.desenv.nepalign.util.DsvConstante;
import flex.messaging.io.ArrayCollection;
import flex.messaging.io.amf.client.AMFConnection;

public class ClienteBundle implements DataBundle
{
	private static final ImagemUsuarioService imagemUsuarioService = new ImagemUsuarioService();
	
	private List<Integer> clientesImportados;
	
	public ClienteBundle()
	{
		clientesImportados = new ArrayList<Integer>();
	}
	
	public void importar(AMFConnection con, EntityManager manager, EntityTransaction transaction, String idEmpresaFisica, Cliente cliente) throws Exception
	{
		importar(con, manager, transaction, idEmpresaFisica, cliente, true);
	}
	
	public void importar(AMFConnection con, EntityManager manager, EntityTransaction transaction, String idEmpresaFisica, Cliente cliente_, Boolean allocInCache) throws Exception
	{
		allocInCache = true;
		
		if(cliente_ == null || cliente_.getId().intValue() == 0x00)
			return;
		if(clientesImportados.contains(cliente_.getId().intValue()))
			return;
		
		final String databaseName = Util.getDatabaseName(idEmpresaFisica);
		
		Object clienteBuffer = con.call(Util.SERVICE_GET_CUSTOMER, cliente_.getId().intValue());
		
		if(clienteBuffer == null)
			throw new NullPointerException("Não foi possivel recuperar o Cliente " + cliente_.getId().intValue());

		Cliente cliente = ((Cliente) clienteBuffer).clone();
	
		if(cliente.getConjuge() != null && cliente.getConjuge().getId().intValue() != 0x00)
			manager.createNativeQuery("DELETE FROM " + databaseName + ".conjuge WHERE idConjuge = " + cliente.getConjuge().getId().intValue()).executeUpdate();
		if(cliente.getAvalista() != null && cliente.getAvalista().getId().intValue() != 0x00)
			manager.createNativeQuery("DELETE FROM " + databaseName + ".avalista WHERE idAvalista = " + cliente.getAvalista().getId().intValue()).executeUpdate();
		
		manager.createNativeQuery("DELETE FROM " + databaseName + ".cliente WHERE idCliente = " + cliente.getId().intValue()).executeUpdate();

		if(cliente.getAvalista() != null)
			manager.merge(Util.<AvalistaDTO> copy(AvalistaDTO.class, cliente.getAvalista().clone()));
		if(cliente.getConjuge() != null)
			manager.merge(Util.<ConjugeDTO> copy(ConjugeDTO.class, cliente.getConjuge().clone()));	
		
		manager.merge(Util.<ClienteDTO> copy(ClienteDTO.class, ((Cliente) clienteBuffer).clone()));
		
		manager.createNativeQuery("DELETE FROM " + databaseName + ".historicoCobranca WHERE idCliente = " + cliente.getId().intValue()).executeUpdate();
		
		HistoricoCobranca historicoCobrancaFiltro = new HistoricoCobranca();
		historicoCobrancaFiltro.setCliente(cliente);
		
		ArrayCollection listaCobranca = (ArrayCollection) con.call(Util.SERVICE_GET_CUSTOMER_INVITE, historicoCobrancaFiltro);
		
		for(Object obj : listaCobranca)
		{
			manager.merge(Util.<HistoricoCobrancaDTO> copy(HistoricoCobrancaDTO.class, ((HistoricoCobranca) obj).clone()));
		}
		
		EmailCliente emailClienteFiltro = new EmailCliente();
		emailClienteFiltro.setCliente(cliente);
		
		ArrayCollection listaEmails = (ArrayCollection) con.call(Util.SERVICE_GET_CUSTOMER_EMAIL_ADDRESSES, emailClienteFiltro);
		
		for(Object obj : listaEmails)
		{
			manager.merge(Util.<EmailClienteDTO> copy(EmailClienteDTO.class, ((EmailCliente) obj).clone()));
		}		
		
		clientesImportados.add(cliente_.getId().intValue());
	}
	
	@SuppressWarnings("unchecked")
	public DataPackage exportar(final EntityManager manager, final DataPackage dataPackage, final Date dataInicial, final Date dataFinal) throws Exception
	{
		final String clienteQuery = " select cli.* from cliente cli " + 
		" inner join ( " + 
		"  select dupli.idCliente from duplicata dupli where (dataCompra between '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' and '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59'   " +
		"  or idDuplicata in (select mov.idDuplicata from movimentoparceladuplicata mov where mov.dataMovimento between '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' and '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59'))  " +
		"  union  " + 
		"  select dupli.idCliente from duplicataAcordo dupAcordo   " +
		"  inner join duplicata dupli ON dupli.numeroDuplicata = dupAcordo.numeroDuplicataAntiga   " +
		"  where dupAcordo.idduplicataNova in (select idDuplicata from duplicata where dataCompra between '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' and '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59')" +
	    "  union select idCliente from movimentoCaixaDia where dataVencimento between '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' and '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59' and diversos = 'S' " +
	    "  union select idCliente from historicoCobranca where dataCobranca between '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' and '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59')" +
		" clientes ON cli.idCliente = clientes.idCliente "; 

		// RECUPERA OS AVALISTAS
		final String avalistaQuery = " select aval.* from avalista aval " + 
		" inner join (" + clienteQuery.replace("cli.*", "cli.idAvalista") + " where cli.idAvalista is not null) avalistas " +
		" ON aval.idAvalista = avalistas.idAvalista ";
		
		// RECUPERA OS CONJUGES DOS CLIENTES
		final String conjugeQuery = " select conj.* from conjuge conj " + 
		" inner join (" + clienteQuery.replace("cli.*", "cli.idConjuge") + " where cli.idConjuge is not null) conjuges " +
		" ON conj.idConjuge = conjuges.idConjuge ";

		// RECUPERA OS EMAILS DOS CLIENTES
		final String emailClienteQuery = " select emailc.* from emailCliente emailc " + 
		" inner join (" + clienteQuery.replace("cli.*", "cli.idCliente") + ") clientes " +
		" ON emailc.idCliente = clientes.idCliente ";
		
		// RECUPERA OS HISTORICOS DE TODOS OS CLIENTES EM UM PERÍODO DE UMA SEMANA.
		final String historicoCobrancaQuery = " select historicoc.* from historicoCobranca historicoc " + 
				" inner join (" + clienteQuery.replace("cli.*", "cli.idCliente") + ") clientes " +
				" ON historicoc.idCliente = clientes.idCliente " +
				" WHERE historicoc.dataCobranca BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY) AND NOW();";
		
		dataPackage.<AvalistaDTO> put(AvalistaDTO.class, (List<AvalistaDTO>) manager.createNativeQuery(avalistaQuery, AvalistaDTO.class).getResultList());
		dataPackage.<ConjugeDTO> put(ConjugeDTO.class, (List<ConjugeDTO>) manager.createNativeQuery(conjugeQuery, ConjugeDTO.class).getResultList());
		dataPackage.<ClienteDTO> put(ClienteDTO.class, (List<ClienteDTO>) manager.createNativeQuery(clienteQuery, ClienteDTO.class).getResultList());
		dataPackage.<EmailClienteDTO> put(EmailClienteDTO.class, (List<EmailClienteDTO>) manager.createNativeQuery(emailClienteQuery, EmailClienteDTO.class).getResultList());
		dataPackage.<HistoricoCobrancaDTO> put(HistoricoCobrancaDTO.class, (List<HistoricoCobrancaDTO>) manager.createNativeQuery(historicoCobrancaQuery, HistoricoCobrancaDTO.class).getResultList());

		if(DsvConstante.getParametrosSistema().get("EXPORTAR_FOTOS_MOVIMENTO") == null || DsvConstante.getParametrosSistema().get("EXPORTAR_FOTOS_MOVIMENTO").equalsIgnoreCase("S"))
		{
			for(final ClienteDTO cliente : dataPackage.<ClienteDTO> push(ClienteDTO.class))
			{
				List<ImagemUsuario> listaImagemUsuario = imagemUsuarioService.listarImagemUsuario(cliente);
				
				for(final ImagemUsuario imagemUsuario : listaImagemUsuario)
					dataPackage.put(ImagemUsuarioDTO.class, Util.<ImagemUsuarioDTO> copy(ImagemUsuarioDTO.class, imagemUsuario));
				
				listaImagemUsuario = null;
			}	
		}
		
		return dataPackage;
	}
}
