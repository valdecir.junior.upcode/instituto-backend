package br.com.desenv.nepalign.integracao;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import br.com.desenv.nepalign.model.Departamento;


public class DsvDepartamentoAux 
{

	private List<Departamento> listaDepartamentos;
	
	
	public DsvDepartamentoAux(List<Departamento> listaDepartamentos)
	{
		this.listaDepartamentos = listaDepartamentos;
	}
	public ArrayList<Departamento> varreOrdenandoDepartamentos()
	{
		HashMap<Integer, Departamento> lstOrdenada = new HashMap<Integer, Departamento>();
		List<Integer> listKey = new ArrayList<Integer>();
		Departamento departamento;
		Iterator<Departamento> itDepartamentos = listaDepartamentos.iterator();
		while(itDepartamentos.hasNext())
		{
			departamento = itDepartamentos.next();
			Integer chave = new Integer(buscaDinastia(departamento));
			lstOrdenada.put(chave, departamento);
			listKey.add(chave);
			
			//System.out.println(buscaDinastia(departamento) + " " + departamento.getDepartamentodescricao());
		}
		Collections.sort(listKey);
		Iterator<Integer> i = listKey.iterator();
		ArrayList<Departamento> departamentos = new ArrayList<Departamento>();
		while(i.hasNext())
		{
			departamentos.add(lstOrdenada.get(i.next()));
		}
		return departamentos;
	}
	private String buscaDinastia(Departamento departamento)
	{
		
		if (departamento.getDepartamentoPai()==null || departamento.getDepartamentoPai().getId()==null)
		{
			return departamento.getId().toString();
		}
		else
		{
			return buscaDinastia(departamento.getDepartamentoPai())+ajustaStr(departamento.getId().toString());
		}
	}
	
	private String ajustaStr(String codigo)
	{
		if (codigo.length()==1)
		{
			return "00"+codigo;
		}
		else 
		{
			return "0"+codigo;
		}
	}

}

