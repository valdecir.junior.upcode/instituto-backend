package br.com.desenv.nepalign.integracao.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="chequerecebido")
public class ChequeRecebidoDTO extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idChequeRecebido")
	private Integer id;
	
	@Column(name = "idEmpresaFisica")
	private Integer empresaFisica;
	
	@Column(name = "idCliente")
	private Integer cliente;
	
	@Column(name = "idBanco")
	private Integer banco;
	
	@Column(name="idStatusCheque")
	private Integer statusCheque;
	
	@Column(name="numeroConta")
	private String numeroConta;
	
	@Column(name="numeroCheque")
	private String numeroCheque;
	
	@Column(name="cpfCnpjPortador")
	private String cpfCnpjPortador;
	
	@Column(name="valor")
	private Double valor;
	
	@Column(name="dataEmissao")
	private Date dataEmissao;
	
	@Column(name="preDatado")
	private Integer preDatado;
	
	@Column(name="bomPara")
	private Date bomPara;
	
	@Column(name="observacao")
	private String observacao;
	
	@Column(name="emitente")
	private String emitente;
	
	public ChequeRecebidoDTO()
	{
		super();
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public Integer getEmpresaFisica() 
	{
		return empresaFisica;
	}

	public String getEmitente() 
	{
		return emitente;
	}

	public void setEmitente(String emitente) 
	{
		if(emitente == null)
			this.emitente = emitente;
		else
			this.emitente = emitente.toUpperCase();
	}

	public void setEmpresaFisica(Integer empresaFisica) 
	{
		this.empresaFisica = empresaFisica;
	}
	
	public Integer getCliente() 
	{
		return cliente;
	}

	public void setCliente(Integer cliente) 
	{
		this.cliente = cliente;
	}
	
	public Integer getBanco() 
	{
		return banco;
	}

	public void setBanco(Integer banco) 
	{
		this.banco = banco;
	}
	
	public Integer getStatusCheque()
	{
		return this.statusCheque;
	}
	
	public void setStatusCheque(Integer statusCheque)
	{
		this.statusCheque = statusCheque;
	}
	
	public String getNumeroConta() 
	{
		String retorno = null;
		
		if (numeroConta != null)
			retorno = numeroConta.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNumeroConta(String numeroConta) 
	{
		if (numeroConta != null)
			this.numeroConta = numeroConta.toUpperCase().trim();
		else
			this.numeroConta = null;
	}
		
	public String getNumeroCheque()
	{
		String retorno = null;
		
		if (numeroCheque != null)
			retorno = numeroCheque.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNumeroCheque(String numeroCheque) 
	{
		if (numeroCheque != null)
			this.numeroCheque = numeroCheque.toUpperCase().trim();
		else
			this.numeroCheque = null;
	}
		
	public String getCpfCnpjPortador() 
	{
		String retorno = null;
		
		if (cpfCnpjPortador != null)
			retorno = cpfCnpjPortador.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setCpfCnpjPortador(String cpfCnpjPortador) 
	{
		if (cpfCnpjPortador != null)
			this.cpfCnpjPortador = cpfCnpjPortador.toUpperCase().trim();
		else
			this.cpfCnpjPortador = null;
	}
		
	public Double getValor() 
	{
		return valor;
	}

	public void setValor(Double valor) 
	{
		if (valor != null && Double.isNaN(valor))
			this.valor = null;
		else
			this.valor = valor;	
	}
	
	public Integer getPreDatado() 
	{
		return preDatado;
	}
	
	public Date getDataEmissao()
	{
		return this.dataEmissao;
	}
	
	public void setDataEmissao(Date dataEmissao)
	{
		this.dataEmissao = dataEmissao;
	}

	public void setPreDatado(Integer preDatado) 
	{
		this.preDatado = preDatado;
	}
	
	public Date getBomPara() 
	{
		Date retorno = null;
		
		if (bomPara != null)
			retorno = bomPara;
		
		return retorno;
	}
	
	public void setBomPara(Date bomPara) 
	{
		if (bomPara != null)
			this.bomPara = bomPara;
		else 
			this.bomPara = null;
	}

	public String getObservacao() 
	{
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setObservacao(String observacao) 
	{
		if (observacao != null)
			this.observacao = observacao.toUpperCase().trim();
		else
			this.observacao = null;	
	}

	@Override
	public void validate() throws Exception 
	{
		if(getObservacao().equals(""))
			setObservacao(null);
		if(getEmitente().equals(""))
			setEmitente(null);
	}
}