package br.com.desenv.nepalign.integracao.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="tipoContaContaGer")
public class TipoContaContaGerDTO  extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idTipoContaContaGer")
	private Integer id;
	
	@Column(name = "idTipoConta")
	private Integer tipoConta;
	@Column(name="idContaGerencial")
	private Integer contaGerencial;

	public TipoContaContaGerDTO()
	{
		super();
	}
	
	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public Integer getTipoConta() 
	{
		return tipoConta;
	}

	public void setTipoConta(Integer tipoConta) 
	{
		this.tipoConta = tipoConta;
	}
	
	public Integer getContaGerencial() 
	{
		return contaGerencial;
	}

	public void setContaGerencial(Integer contaGerencial) 
	{
		this.contaGerencial = contaGerencial;
	}
	
	@Override
	public void validate() throws Exception { }
}