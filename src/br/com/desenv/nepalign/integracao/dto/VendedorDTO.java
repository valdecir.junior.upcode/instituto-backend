package br.com.desenv.nepalign.integracao.dto;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="vendedor")
public class VendedorDTO extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="idVendedor")
	private Integer id;
		
	@Column(name = "idEmpresaFisica") 
	private Integer empresaFisica;
	
	@Column(name="nome")
	private String nome;
	
	@Column(name = "foto")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] foto;
	
	@Column(name="codigoVendedorCache")
	private Integer codigoVendedorCache;
	
	@Column(name = "idUsuario")
	private Integer usuario;

	public VendedorDTO()
	{
		super();
	}
	
	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public Integer getEmpresaFisica() 
	{
		return empresaFisica;
	}

	public void setEmpresaFisica(Integer empresaFisica) 
	{
		this.empresaFisica = empresaFisica;
	}
	
	public String getNome() 
	{
		String retorno = null;
		
		if (nome != null)
			retorno = nome.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNome(String nome) 
	{
		if (nome != null)
			this.nome = nome.toUpperCase().trim();
		else
			this.nome = null;
	}
		
	public byte[] getFoto() 
	{
		return foto;
	}

	public void setFoto(byte[] foto) 
	{
		this.foto = foto;
	}
	
	public Integer getCodigoVendedorCache() 
	{
		return codigoVendedorCache;
	}

	public void setCodigoVendedorCache(Integer codigoVendedorCache) 
	{
		if (codigoVendedorCache != null && codigoVendedorCache == 0)
			this.codigoVendedorCache = null;
		else
			this.codigoVendedorCache = codigoVendedorCache;
	}
	
	public Integer getUsuario() 
	{
		return usuario;
	}

	public void setUsuario(Integer usuario) 
	{
		this.usuario = usuario;
	}
	
	@Override
	public void validate() throws Exception {}

	@Override
	public VendedorDTO clone() throws CloneNotSupportedException
	{
		return (VendedorDTO) super.clone();
	}
}
