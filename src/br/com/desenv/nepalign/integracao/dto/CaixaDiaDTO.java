package br.com.desenv.nepalign.integracao.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="caixadia")
public class CaixaDiaDTO extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;
	
	public static final String SITUACAOIMPORTACAO_PENDENTE = "P";
	public static final String SITUACAOIMPORTACAO_IMPORTADO = "I";

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idCaixaDia")
	private Integer id;
		
	@Column(name = "idEmpresaFisica")
	private Integer empresaFisica;
	
	@Column(name = "idUsuario")
	private Integer usuario;
	
	@Column(name="valorInicial")
	private Double valorInicial;
	
	@Column(name="situacao")
	private Integer situacao;
	
	@Column(name="dataAbertura")
	private Date dataAbertura;
	
	@Column(name="horaAbertura")
	private Date horaAbertura;
	
	@Column(name="dataFechamento")
	private Date dataFechamento;
	
	@Column(name="horaFechamento")
	private Date horaFechamento;
	
	@Column(name="observacao")
	private String observacao;

	@Column(name="ultimoEvento")
	private Date ultimoEvento;
	

	public CaixaDiaDTO()
	{
		super();
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public Integer getEmpresaFisica() 
	{
		return empresaFisica;
	}

	public void setEmpresaFisica(Integer empresaFisica) 
	{
		this.empresaFisica = empresaFisica;
	}
	
	public Integer getUsuario() 
	{
		return usuario;
	}

	public void setUsuario(Integer usuario) 
	{
		this.usuario = usuario;
	}
	
	public Double getValorInicial() 
	{
		return valorInicial;
	}

	public void setValorInicial(Double valorInicial) 
	{
		if (valorInicial != null && Double.isNaN(valorInicial))
			this.valorInicial = null;
		else
			this.valorInicial = valorInicial;
	}
	
	public Integer getSituacao() 
	{
		return situacao;
	}

	public void setSituacao(Integer situacao) 
	{
		this.situacao = situacao;
	}
	
	public Date getDataAbertura() 
	{
		return dataAbertura;
	}

	public void setDataAbertura(Date dataAbertura) 
	{
		this.dataAbertura = dataAbertura;
	}
	
	public Date getHoraAbertura() 
	{	 
		return horaAbertura;
	}

	public void setHoraAbertura(Date horaAbertura) 
	{	
		this.horaAbertura = horaAbertura;
	}
	
	public Date getDataFechamento() 
	{
		return dataFechamento;
	}

	public void setDataFechamento(Date dataFechamento) 
	{
		this.dataFechamento = dataFechamento;
	}
	
	public Date getHoraFechamento() 
	{
		return horaFechamento;
	}

	public void setHoraFechamento(Date horaFechamento) 
	{
		this.horaFechamento = horaFechamento;
	}
	
	public String getObservacao() 
	{
		return observacao;
	}

	public void setObservacao(String observacao) 
	{
		this.observacao = observacao;
	}

	public Date getUltimoEvento() 
	{
		return ultimoEvento;
	}

	public void setUltimoEvento(Date ultimoEvento) 
	{
		this.ultimoEvento = ultimoEvento;
	}

	@Override
	public void validate() throws Exception { }
	
	@Override
	public CaixaDiaDTO clone() throws CloneNotSupportedException 
	{
		return (CaixaDiaDTO) super.clone();
	}
}
