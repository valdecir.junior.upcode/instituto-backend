package br.com.desenv.nepalign.integracao.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="duplicataacordo")
public class DuplicataAcordoDTO extends GenericModelIGN implements Cloneable 
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idDuplicataAcordo")
	private Integer id;
		
	@Column(name = "idDuplicataNova")
	private Integer duplicataNova;
	
	@Column(name = "idDuplicataAntigaParcela")
	private Integer duplicataAntigaParcela;
	
	@Column(name = "idEmpresaFisica")
	private Integer empresaFisica;
	
	@Column(name = "idCliente")
	private Integer cliente;
	
	@Column(name="numeroNovaDuplicata")
	private Long numeroNovaDuplicata;
	
	@Column(name="numeroDuplicataAntiga")
	private Long numeroDuplicataAntiga;
	
	@Column(name="numeroParcelaDuplicataAntiga")
	private Integer numeroParcelaDuplicataAntiga;
	
	public DuplicataAcordoDTO()
	{
		super();
	}
	
	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public Integer getDuplicataNova() 
	{
		return duplicataNova;
	}

	public void setDuplicataNova(Integer duplicataNova) 
	{
		this.duplicataNova = duplicataNova;
	}
	
	public Integer getDuplicataAntigaParcela() 
	{
		return duplicataAntigaParcela;
	}

	public void setDuplicataAntigaParcela(Integer duplicataAntigaParcela) 
	{
		this.duplicataAntigaParcela = duplicataAntigaParcela;
	}
	
	public Integer getEmpresaFisica() 
	{
		return empresaFisica;
	}

	public void setEmpresaFisica(Integer empresaFisica) 
	{
		this.empresaFisica = empresaFisica;
	}
	
	public Integer getCliente() 
	{
		return cliente;
	}

	public void setCliente(Integer cliente) 
	{
		this.cliente = cliente;
	}
	
	public Long getNumeroNovaDuplicata() 
	{
		return numeroNovaDuplicata;
	}

	public void setNumeroNovaDuplicata(Long numeroNovaDuplicata) 
	{
		this.numeroNovaDuplicata = numeroNovaDuplicata;
	}
	
	public Long getNumeroDuplicataAntiga() 
	{
		return numeroDuplicataAntiga;
	}

	public void setNumeroDuplicataAntiga(Long numeroDuplicataAntiga) 
	{
		this.numeroDuplicataAntiga = numeroDuplicataAntiga;
	}	
	
	public Integer getNumeroParcelaDuplicataAntiga() 
	{
		return numeroParcelaDuplicataAntiga;
	}

	public void setNumeroParcelaDuplicataAntiga(Integer numeroParcelaDuplicataAntiga) 
	{
		this.numeroParcelaDuplicataAntiga = numeroParcelaDuplicataAntiga;
	}	
	
	@Override
	public void validate() throws Exception {}
	
	@Override
	public DuplicataAcordoDTO clone() throws CloneNotSupportedException 
	{
		return (DuplicataAcordoDTO) super.clone();
	}
}
