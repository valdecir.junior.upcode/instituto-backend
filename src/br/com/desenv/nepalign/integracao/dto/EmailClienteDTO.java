package br.com.desenv.nepalign.integracao.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="emailcliente")
public class EmailClienteDTO extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idEmailCliente")
	private Integer id;
	@Column(name = "idCliente")
	private Integer cliente;
	@Column(name="email")
	private String email;

	public EmailClienteDTO()
	{
		super();
	}

	public EmailClienteDTO(Integer cliente, String email) 
	{
		super();
		this.cliente = cliente;
		this.email = email;
		
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public Integer getCliente() 
	{
		return cliente;
	}

	public void setCliente(Integer cliente) 
	{
		this.cliente = cliente;
	}
	
	public String getEmail() 
	{
		String retorno = null;
		
		if (email != null)
			retorno = email.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setEmail(String email) 
	{
		if (email != null)
			this.email = email.toUpperCase().trim();
		
		else
			this.email = null;	
	}
		
	@Override
	public void validate() throws Exception
	{		
	}
	
	public EmailClienteDTO clone() throws CloneNotSupportedException
	{
		return (EmailClienteDTO) super.clone();
	}
}