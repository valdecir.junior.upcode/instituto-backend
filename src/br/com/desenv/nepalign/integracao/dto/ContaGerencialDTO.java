package br.com.desenv.nepalign.integracao.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="contaGerencial")
public class ContaGerencialDTO  extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idContaGerencial")
	private Integer id;
		
	@Column(name="codigoContaNivel1")
	private Integer codigoContaNivel1;
	
	@Column(name="codigoContaNivel2")
	private Integer codigoContaNivel2;
	
	@Column(name="codigoContaNivel3")
	private Integer codigoContaNivel3;
	
	@Column(name="codigoContaNivel4")
	private Integer codigoContaNivel4;
	
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="contaContabil")
	private String contaContabil;
	
	@Column(name="codigoReduzido")
	private Integer codigoReduzido;
	
	@Column(name="creditoDebito")
	private String creditoDebito;
	
	@Column(name="exibeDespesas")
	private String exibeDespesas;
	
	@Column(name="ativa")
	private Integer ativa = null;

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public Integer getCodigoContaNivel1() 
	{
		return codigoContaNivel1;
	}

	public void setCodigoContaNivel1(Integer codigoContaNivel1) 
	{
		if (codigoContaNivel1 != null && Double.isNaN(codigoContaNivel1))
			this.codigoContaNivel1 = null;
		else
			this.codigoContaNivel1 = codigoContaNivel1;		
	}	
	
	public Integer getCodigoContaNivel2() 
	{
		return codigoContaNivel2;
	}

	public void setCodigoContaNivel2(Integer codigoContaNivel2)
	{
		if (codigoContaNivel2 != null && Double.isNaN(codigoContaNivel2))
			this.codigoContaNivel2 = null;
		else
			this.codigoContaNivel2 = codigoContaNivel2;
	}
	
	public Integer getCodigoContaNivel3() 
	{
		return codigoContaNivel3;
	}

	public void setCodigoContaNivel3(Integer codigoContaNivel3) 
	{
		if (codigoContaNivel3 != null && Double.isNaN(codigoContaNivel3))
			this.codigoContaNivel3 = null;
		else
			this.codigoContaNivel3 = codigoContaNivel3;
	}	
	
	public Integer getCodigoContaNivel4() 
	{
		return codigoContaNivel4;
	}

	public void setCodigoContaNivel4(Integer codigoContaNivel4) 
	{
		if (codigoContaNivel4 != null && Double.isNaN(codigoContaNivel4))
			this.codigoContaNivel4 = null;
		else
			this.codigoContaNivel4 = codigoContaNivel4;
	}
	
	public String getDescricao() 
	{
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setDescricao(String descricao) 
	{
		if (descricao != null)
			this.descricao = descricao.toUpperCase().trim();
		else
			this.descricao = null;
	}
		
	public String getContaContabil() 
	{
		String retorno = null;
		
		if (contaContabil != null)
			retorno = contaContabil.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setContaContabil(String contaContabil) 
	{
		if (contaContabil != null)
			this.contaContabil = contaContabil.toUpperCase().trim();
		else
			this.contaContabil = null;
	}
		
	public Integer getCodigoReduzido() 
	{
		return codigoReduzido;
	}

	public void setCodigoReduzido(Integer codigoReduzido) 
	{
		this.codigoReduzido = codigoReduzido;
	}
	
	public String getCreditoDebito() 
	{
		return creditoDebito;
	}

	public void setCreditoDebito(String creditoDebito) 
	{
		this.creditoDebito = creditoDebito;
	}

	public String getExibeDespesas()  
	{
		return exibeDespesas;
	}

	public void setExibeDespesas(String exibeDespesas) 
	{
		if(exibeDespesas == null || exibeDespesas.equals(""))
			this.exibeDespesas = "S";
		else
			this.exibeDespesas = exibeDespesas;
	}
	
	public Integer getAtiva() 
	{
		return ativa;
	}

	public void setAtiva(Integer ativa) 
	{
		if(ativa == null)
			this.ativa = 1;
		else
			this.ativa = ativa;
	}

	@Override
	public void validate() throws Exception { }
}