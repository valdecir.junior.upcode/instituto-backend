package br.com.desenv.nepalign.integracao.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="logoperacao")
public class LogOperacaoDTO extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idLogOperacao")
	private Integer id;
	
	@Column(name="data")
	private Date data;
	
	@Column(name="campo")
	private String campo;
	
	@Column(name="agrupador")
	private String agrupador;
	
	@Column(name="valorOriginal")
	private String valorOriginal;
	
	@Column(name="valorAlterado")
	private String valorAlterado;
	
	@Column(name="idUsuario")
	private Integer usuario;
	
	@Column(name="nomeClasse")
	private String nomeClasse;
	
	@Column(name="idDocumentoOrigem")
	private String idDocumentoOrigem;
	
	@Column(name="idEmpresaFisica")
	private Integer empresaFisica;
	
	@Column(name="tipoLog")
	private Integer tipoLog;
	
	@Column(name="observacao")
	private String observacao;
	
	public LogOperacaoDTO()
	{
		super();
	}
	
	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}	
		
	public Date getData() 
	{
		return data;
	}

	public void setData(Date data) 
	{
		this.data = data;
	}
	
	public String getValorOriginal()
	{
		String retorno = null;
		
		if (valorOriginal != null)
			retorno = valorOriginal.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setValorOriginal(String valorOriginal)
	{
		if (valorOriginal != null)
			this.valorOriginal = valorOriginal.toUpperCase().trim();
		else
			this.valorOriginal = null;		
	}
		
	public String getValorAlterado() 
	{
		String retorno = null;
		
		if (valorAlterado != null)
			retorno = valorAlterado.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setValorAlterado(String valorAlterado) 
	{
		if (valorAlterado != null)
			this.valorAlterado = valorAlterado.toUpperCase().trim();
		else
			this.valorAlterado = null;
	}
		
	public String getNomeClasse() 
	{
		return nomeClasse;
	}

	public void setNomeClasse(String nomeClasse) 
	{
		this.nomeClasse = nomeClasse;
	}

	public String getIdDocumentoOrigem() 
	{
		return idDocumentoOrigem;
	}

	public void setIdDocumentoOrigem(String idDocumentoOrigem) 
	{
		this.idDocumentoOrigem = idDocumentoOrigem;
	}

	public String getCampo() 
	{
		return campo;
	}


	public void setCampo(String campo) 
	{
		this.campo = campo;
	}


	public String getAgrupador()
	{
		return agrupador;
	}


	public void setAgrupador(String agrupador) 
	{
		this.agrupador = agrupador;
	}


	public Integer getUsuario()
	{
		return usuario;
	}

	public void setUsuario(Integer usuario)
	{
		this.usuario = usuario;
	}

	public Integer getEmpresaFisica() 
	{
		return empresaFisica;
	}

	public void setEmpresaFisica(Integer empresaFisica) 
	{
		this.empresaFisica = empresaFisica;
	}

	public Integer getTipoLog()
	{
		return tipoLog;
	}

	public void setTipoLog(Integer tipoLog) 
	{
		this.tipoLog = tipoLog;
	}

	public String getObservacao() 
	{
		return observacao;
	}

	public void setObservacao(String observacao) 
	{
		this.observacao = observacao;
	}

	@Override
	public void validate() throws Exception { }
}