package br.com.desenv.nepalign.integracao.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="movimentocaixadia")
public class MovimentoCaixaDiaDTO extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idMovimentoCaixaDia")
	private Integer id;

	@Column(name = "idFormaPagamento")
	private Integer formaPagamento;
	
	@Column(name = "idFornecedor")
	private Integer fornecedor;

	@Column(name = "idCliente")
	private Integer cliente;
	
	@Column(name="idTipoMovimentoCaixaDia")
	private Integer tipoMovimentoCaixaDia;
	
	@Column(name="idOperadoraCartao")
	private Integer operadoraCartao;
	
	@Column(name="idContaGerencial")
	private Integer contaGerencial;
	
	@Column(name="idDuplicataParcela")
	private Integer duplicataParcela;
	
	@Column(name = "idCaixaDia")
	private Integer caixaDia;
	
	@Column(name = "idPagamentoPedidoVenda")
	private Integer pagamentoPedidoVenda;
	
	@Column(name = "idUsuario")
	private Integer usuario;
	
	@Column(name = "idTipoConta")
	private Integer tipoConta;
	
	@Column(name = "idMovimentoParcelaDuplicata")
	private Integer movimentoParcelaDuplicata;
	
	@Column(name="CreditoDebito")
	private String creditoDebito;
	
	@Column(name="numeroDocumento")
	private String numeroDocumento;
	
	@Column(name="dataVencimento")
	private Date dataVencimento;
	
	@Column(name="cpfCnpjPortador")
	private String cpfCnpjPortador;
	
	@Column(name="valor")
	private Double valor;
	
	@Column(name="situacao")
	private Integer situacao;
	
	@Column(name="observacao")
	private String observacao;
	
	@Column(name="diversos")
	private String diversos;
	
	@Column(name="mostrarBoletim")
	private String mostrarBoletim;
	
	public MovimentoCaixaDiaDTO()
	{
		super();
	}
	
	public Integer getDuplicataParcela() 
	{
		return duplicataParcela;
	}

	public void setDuplicataParcela(Integer duplicataParcela) 
	{
		this.duplicataParcela = duplicataParcela;
	}
	
	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public Integer getMovimentoParcelaDuplicata() 
	{
		return movimentoParcelaDuplicata;
	}

	public void setMovimentoParcelaDuplicata(Integer movimentoParcelaDuplicata) 
	{
		this.movimentoParcelaDuplicata = movimentoParcelaDuplicata;
	}

	public Integer getFormaPagamento() 
	{
		return formaPagamento;
	}

	public void setFormaPagamento(Integer formaPagamento) 
	{
		this.formaPagamento = formaPagamento;
	}
	
	public Integer getFornecedor() 
	{
		return fornecedor;
	}

	public void setFornecedor(Integer fornecedor) 
	{
		this.fornecedor = fornecedor;
	}
	
	public Integer getCliente() 
	{
		return cliente;
	}

	public void setCliente(Integer cliente) 
	{
		this.cliente = cliente;
	}
	
	public Integer getTipoMovimentoCaixaDia() 
	{
		return tipoMovimentoCaixaDia;
	}

	public void setTipoMovimentoCaixaDia(Integer tipoMovimentoCaixaDia) 
	{
		this.tipoMovimentoCaixaDia = tipoMovimentoCaixaDia;
	}
	
	public Integer getContaGerencial() 
	{
		return contaGerencial;
	}

	public void setContaGerencial(Integer contaGerencial) 
	{
		this.contaGerencial = contaGerencial;
	}

	public Integer getCaixaDia() 
	{
		return caixaDia;
	}

	public void setCaixaDia(Integer caixaDia) 
	{
		this.caixaDia = caixaDia;
	}
	
	public Integer getPagamentoPedidoVenda() 
	{
		return pagamentoPedidoVenda;
	}

	public void setPagamentoPedidoVenda(Integer pagamentoPedidoVenda) 
	{
		this.pagamentoPedidoVenda = pagamentoPedidoVenda;
	}
	
	public Integer getUsuario() 
	{
		return usuario;
	}

	public void setUsuario(Integer usuario) 
	{
		this.usuario = usuario;
	}
	
	public String getCreditoDebito() 
	{
		String retorno = null;
		
		if (creditoDebito != null)
			retorno = creditoDebito.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setCreditoDebito(String creditoDebito) 
	{
		if (creditoDebito != null)
			this.creditoDebito = creditoDebito.toUpperCase().trim();
		else
			this.creditoDebito = null;
	}
		
	public String getNumeroDocumento() 
	{
		String retorno = null;
		
		if (numeroDocumento != null)
			retorno = numeroDocumento.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNumeroDocumento(String numeroDocumento) 
	{
		if (numeroDocumento != null)
			this.numeroDocumento = numeroDocumento.toUpperCase().trim();
		else
			this.numeroDocumento = null;
	}
	
	public Date getDataVencimento() 
	{
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) 
	{
		this.dataVencimento = dataVencimento;
	}
	
	public String getCpfCnpjPortador() 
	{
		String retorno = null;
		
		if (cpfCnpjPortador != null)
			retorno = cpfCnpjPortador.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setCpfCnpjPortador(String cpfCnpjPortador) 
	{
		if (cpfCnpjPortador != null)
			this.cpfCnpjPortador = cpfCnpjPortador.toUpperCase().trim();
		else
			this.cpfCnpjPortador = null;
	}
		
	public Double getValor() 
	{
		return valor;
	}

	public void setValor(Double valor) 
	{
		if (valor != null && Double.isNaN(valor))
			this.valor = null;
		else
			this.valor = valor;	
	}
	
	public Integer getSituacao() 
	{
		return situacao;
	}

	public void setSituacao(Integer situacao) 
	{
		this.situacao = situacao;
	}
	
	public String getObservacao() 
	{
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setObservacao(String observacao) 
	{
		if (observacao != null)
			this.observacao = observacao.toUpperCase().trim();
		else
			this.observacao = null;	
	}
		
	public Integer getTipoConta() 
	{
		return tipoConta;
	}

	public void setTipoConta(Integer tipoConta) 
	{
		this.tipoConta = tipoConta;
	}

	public String getDiversos() 
	{
		return diversos;
	}

	public void setDiversos(String diversos) 
	{
		this.diversos = diversos;
	}

	public String getMostrarBoletim() 
	{
		return mostrarBoletim;
	}

	public void setMostrarBoletim(String mostrarBoletim) 
	{
		this.mostrarBoletim = mostrarBoletim;
	}

	public Integer getOperadoraCartao() 
	{
		return operadoraCartao;
	}

	public void setOperadoraCartao(Integer operadoraCartao) 
	{
		this.operadoraCartao = operadoraCartao;
	}

	@Override
	public void validate() throws Exception { }

	@Override
	public MovimentoCaixaDiaDTO clone() throws CloneNotSupportedException 
	{
		return (MovimentoCaixaDiaDTO) super.clone();
	}
}