package br.com.desenv.nepalign.integracao.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="pagamentopedidovenda")
public class PagamentoPedidoVendaDTO extends GenericModelIGN implements Cloneable 
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idPagamentoPedidoVenda")
	private Integer id;
		
	@Column(name = "idPedidoVenda")
	private Integer pedidoVenda;
	
	@Column(name = "idFormaPagamento")
	private Integer formaPagamento;
	
	@Column(name="dataPagamento")
	private Date dataPagamento;
	
	@Column(name="valor")
	private Double valor;
	
	@Column(name="numeroDocumento")
	private String numeroDocumento;
	
	@Column(name="situacaoLancamento")
	private Integer situacaoLancamento;
	
	@Column(name="idNotaFiscal")
	private String idNotaFiscal;
	
	@Column(name="itemSequencial")
	private String itemSequencial;

	@Column(name = "idUsuario")
	private Integer usuario;
	
	@Column(name = "idChequeRecebido")
	private Integer chequeRecebido;
	
	@Column(name = "idCaixaDia")
	private Integer caixaDia;
	
	@Column(name = "idPagamentoCartao")
	private Integer pagamentoCartao;
	
	@Column(name = "entrada")
	private String entrada;
	
	@Transient
	private Integer operadoraCartao;
	
	public PagamentoPedidoVendaDTO()
	{
		super();
	}
	
	public Integer getUsuario() 
	{
		return usuario;
	}

	public void setUsuario(Integer usuario) 
	{
		this.usuario = usuario;
	}

	public String getItemSequencial() 
	{
		return itemSequencial;
	}

	public void setItemSequencial(String itemSequencial) 
	{
		this.itemSequencial = itemSequencial;
	}

	public Integer getChequeRecebido() 
	{
		return chequeRecebido;
	}

	public String getEntrada()
	{
		return entrada;
	}

	public void setEntrada(String entrada) 
	{
		this.entrada = entrada;
	}

	public void setChequeRecebido(Integer chequeRecebido) 
	{
		this.chequeRecebido = chequeRecebido;
	}

	public Integer getCaixaDia() 
	{
		return caixaDia;
	}

	public void setCaixaDia(Integer caixaDia) 
	{
		this.caixaDia = caixaDia;
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public Integer getPedidoVenda() 
	{
		return pedidoVenda;
	}

	public Integer getPagamentoCartao() 
	{
		return pagamentoCartao;
	}

	public void setPagamentoCartao(Integer pagamentoCartao) 
	{
		this.pagamentoCartao = pagamentoCartao;
	}

	public void setPedidoVenda(Integer pedidoVenda) 
	{
		this.pedidoVenda = pedidoVenda;
	}
	
	public Integer getFormaPagamento() 
	{
		return formaPagamento;
	}

	public void setFormaPagamento(Integer formaPagamento) 
	{
		this.formaPagamento = formaPagamento;
	}
	
	public Date getDataPagamento() 
	{
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) 
	{
		this.dataPagamento = dataPagamento;
	}
	
	public Double getValor() 
	{
		return valor;
	}

	public void setValor(Double valor) 
	{
		if (valor != null && Double.isNaN(valor))
			this.valor = null;
		else
			this.valor = valor;	
	}
	
	public String getNumeroDocumento() 
	{
		String retorno = null;
		
		if (numeroDocumento != null)
			retorno = numeroDocumento.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNumeroDocumento(String numeroDocumento) 
	{
		if (numeroDocumento != null)
			this.numeroDocumento = numeroDocumento.toUpperCase().trim();
		else
			this.numeroDocumento = null;
	}

	public Integer getSituacaoLancamento() 
	{
		return situacaoLancamento;
	}

	public void setSituacaoLancamento(Integer situacaoLancamento) 
	{
		this.situacaoLancamento = situacaoLancamento;
	}
	
	public String getIdNotaFiscal() 
	{
		String retorno = null;
		
		if (idNotaFiscal != null)
			retorno = idNotaFiscal.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setIdNotaFiscal(String idNotaFiscal) 
	{
		if (idNotaFiscal != null)
			this.idNotaFiscal = idNotaFiscal.toUpperCase().trim();
		else
			this.idNotaFiscal = null;
	}
	
	public Integer getOperadoraCartao() {
		return operadoraCartao;
	}

	public void setOperadoraCartao(Integer operadoraCartao) {
		this.operadoraCartao = operadoraCartao;
	}

	@Override
	public void validate() throws Exception { }
	
	@Override
	public PagamentoPedidoVendaDTO clone() throws CloneNotSupportedException 
	{
		return (PagamentoPedidoVendaDTO) super.clone();
	}
}