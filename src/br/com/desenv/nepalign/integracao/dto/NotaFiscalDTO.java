package br.com.desenv.nepalign.integracao.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name = "notafiscal")
public class NotaFiscalDTO extends GenericModelIGN {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idNotaFiscal")
	private Integer id;
	@Column(name = "idEmpresa")
	private Integer empresa;
	@Column(name = "idMovimentacaoEstoque")
	private Integer movimentacaoEstoque;
	@Column(name = "idPedidoVenda")
	private Integer pedidoVenda;
	@Column(name = "idTipoNotaFiscal")
	private Integer tipoNotaFiscal;
	@Column(name = "idCFOP")
	private Integer cfop;
	@Column(name = "idTransportadora")
	private Integer transportadora;
	@Column(name = "idSituacaoNotaFiscal")
	private Integer situacaoNotaFiscal;
	@Column(name = "idCliente")
	private Integer cliente;
	@Column(name = "numeroFormulario")
	private Integer numeroFormulario;
	@Column(name = "dataEmissao")
	private Date dataEmissao;
	@Column(name = "numero")
	private Integer numero;
	@Column(name = "baseCalculoIss")
	private Double baseCalculoIss;
	@Column(name = "valorIss")
	private Double valorIss;
	@Column(name = "baseCalculoIcms")
	private Double baseCalculoIcms;
	@Column(name = "valorIcms")
	private Double valorIcms;
	@Column(name = "baseCalculoIcmsSubstituicao")
	private Double baseCalculoIcmsSubstituicao;

	@Column(name = "valorIcmsSubstituicao")
	private Double valorIcmsSubstituicao;

	@Column(name = "valorFrete")
	private Double valorFrete;

	@Column(name = "valorSeguro")
	private Double valorSeguro;

	@Column(name = "valorTotalIpi")
	private Double valorTotalIpi;

	@Column(name = "valorOutrasDespesas")
	private Double valorOutrasDespesas;

	@Column(name = "valorTotalServico")
	private Double valorTotalServico;

	@Column(name = "valorTotalProduto")
	private Double valorTotalProduto;

	@Column(name = "valorTotalNota")
	private Double valorTotalNota;

	@Column(name = "observacao")
	private String observacao;

	@Column(name = "cifFob")
	private String cifFob;

	@Column(name = "placaVeiculo")
	private String placaVeiculo;

	@Column(name = "horaSaidaEntrada")
	private Date horaSaidaEntrada;

	@Column(name = "dataSaidaEntrada")
	private Date dataSaidaEntrada;

	@Column(name = "valorTotalContaReceber")
	private Double valorTotalContaReceber;

	@Column(name = "situacao")
	private Integer situacao;

	@Column(name = "inscrSubsTrib")
	private String inscrSubsTrib;

	@Column(name = "quantidadeVolumes")
	private Integer quantidadeVolumes;

	@Column(name = "peso")
	private Double peso;

	@Column(name = "dataFinalizacao")
	private Date dataFinalizacao;

	@Column(name = "entradaSaida")
	private String entradaSaida;

	@Column(name = "idNotaFiscalVendaConsignacao")
	private String idNotaFiscalVendaConsignacao;

	@Column(name = "idNotaFiscalDevolucaoConsignacao")
	private String idNotaFiscalDevolucaoConsignacao;

	@Column(name = "idCFOP2")
	private Integer cfop2;
	@Column(name = "idCFOP3")
	private Integer cfop3;

	@Column(name = "valorTotalPis")
	private Double valorTotalPis;

	@Column(name = "valorTotalCofins")
	private Double valorTotalCofins;

	@Column(name = "valorTotalCsll")
	private Double valorTotalCsll;

	@Column(name = "valorTotalIrrf")
	private Double valorTotalIrrf;

	@Column(name = "idFormaPagamentoNfe")
	private Integer formaPagamentoNfe;

	@Column(name = "observacoesComplementares")
	private String observacoesComplementares;

	@Column(name = "statusNfe")
	private Integer statusNfe;

	@Column(name = "dataRetorno")
	private Date dataRetorno;

	@Column(name = "numProtocoloSefaz")
	private String numProtocoloSefaz;

	@Column(name = "chaveAcesso")
	private String chaveAcesso;

	@Column(name = "motivoStatus")
	private String motivoStatus;

	@Column(name = "codigoInutilizacao")
	private String codigoInutilizacao;

	@Column(name = "statusInutilizacao")
	private String statusInutilizacao;

	@Column(name = "statusImpressao")
	private Integer statusImpressao;

	@Column(name = "serieContingencia")
	private String serieContingencia;

	@Column(name = "serie")
	private String serie;

	@Column(name = "statusCancelamento")
	private Integer statusCancelamento;

	@Column(name = "statusContingencia")
	private Integer statusContingencia;

	@Column(name = "motivoCancelamento")
	private String motivoCancelamento;

	@Column(name = "retornoStatusCancelamento")
	private String retornoStatusCancelamento;

	@Column(name = "retornoStatusImpressao")
	private String retornoStatusImpressao;

	@Column(name = "motivoInutilizacao")
	private String motivoInutilizacao;

	@Column(name = "retornoStatusContingencia")
	private String retornoStatusContingencia;

	@Column(name = "valorRetencaoPis")
	private Double valorRetencaoPis;

	@Column(name = "valorRetencaoCofins")
	private Double valorRetencaoCofins;

	@Column(name = "valorRetencaoIrrf")
	private Double valorRetencaoIrrf;

	@Column(name = "valorRetencaoCsll")
	private Double valorRetencaoCsll;

	@Column(name = "valorDescontoTotal")
	private Double valorDescontoTotal;

	@Column(name = "valorIITotal")
	private Double valorIITotal;

	@Column(name = "nfeReferenciada")
	private String nfeReferenciada;

	@Column(name = "codigoUFNFeReferenciada")
	private Integer codigoUFNFeReferenciada;

	@Column(name = "anoMesEmissaoNFeRef")
	private String anoMesEmissaoNFeRef;

	@Column(name = "cnpjEmitente")
	private String cnpjEmitente;

	@Column(name = "modeloNfeRef")
	private String modeloNfeRef;

	@Column(name = "serieNFeRef")
	private String serieNFeRef;

	@Column(name = "numeroNFeRef")
	private String numeroNFeRef;

	@Column(name = "idUf")
	private int uf;

	@Column(name = "finalidade")
	private Integer finalidade;

	@Column(name = "modelo")
	private Integer modelo;

	@Column(name = "cpfReceptor")
	private String cpfReceptor;

	@Column(name = "tpAmb")
	private Integer tpAmb;

	@Column(name = "email")
	private String email;

	@Column(name = "enviouEmail")
	private Integer enviouEmail;

	public NotaFiscalDTO() {
		super();
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		if (id != null && id == 0) {
			this.id = null;
		} else {
			this.id = id;
		}
	}

	public Integer getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}

	public Integer getMovimentacaoEstoque() {
		return movimentacaoEstoque;
	}

	public void setMovimentacaoEstoque(Integer movimentacaoEstoque) {
		this.movimentacaoEstoque = movimentacaoEstoque;
	}

	public Integer getPedidoVenda() {
		return pedidoVenda;
	}

	public void setPedidoVenda(Integer pedidoVenda) {
		this.pedidoVenda = pedidoVenda;
	}

	public Integer getTipoNotaFiscal() {
		return tipoNotaFiscal;
	}

	public void setTipoNotaFiscal(Integer tipoNotaFiscal) {
		this.tipoNotaFiscal = tipoNotaFiscal;
	}

	public Integer getCfop() {
		return cfop;
	}

	public void setCfop(Integer cfop) {
		this.cfop = cfop;
	}

	public Integer getTransportadora() {
		return transportadora;
	}

	public void setTransportadora(Integer transportadora) {
		this.transportadora = transportadora;
	}

	public Integer getSituacaoNotaFiscal() {
		return situacaoNotaFiscal;
	}

	public void setSituacaoNotaFiscal(Integer situacaoNotaFiscal) {
		this.situacaoNotaFiscal = situacaoNotaFiscal;
	}

	public Integer getCliente() {
		return cliente;
	}

	public void setCliente(Integer cliente) {
		this.cliente = cliente;
	}

	public Integer getNumeroFormulario() {
		return numeroFormulario;
	}

	public void setNumeroFormulario(Integer numeroFormulario) {
		this.numeroFormulario = numeroFormulario;
	}

	public Date getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Double getBaseCalculoIss() {
		return baseCalculoIss;
	}

	public void setBaseCalculoIss(Double baseCalculoIss) {

		if (baseCalculoIss != null && Double.isNaN(baseCalculoIss)) {
			this.baseCalculoIss = null;
		} else {
			this.baseCalculoIss = baseCalculoIss;
		}

	}

	public Double getValorIss() {
		return valorIss;
	}

	public void setValorIss(Double valorIss) {

		if (valorIss != null && Double.isNaN(valorIss)) {
			this.valorIss = null;
		} else {
			this.valorIss = valorIss;
		}

	}

	public Double getBaseCalculoIcms() {
		return baseCalculoIcms;
	}

	public void setBaseCalculoIcms(Double baseCalculoIcms) {

		if (baseCalculoIcms != null && Double.isNaN(baseCalculoIcms)) {
			this.baseCalculoIcms = null;
		} else {
			this.baseCalculoIcms = baseCalculoIcms;
		}

	}

	public Double getValorIcms() {
		return valorIcms;
	}

	public void setValorIcms(Double valorIcms) {

		if (valorIcms != null && Double.isNaN(valorIcms)) {
			this.valorIcms = null;
		} else {
			this.valorIcms = valorIcms;
		}

	}

	public Double getBaseCalculoIcmsSubstituicao() {
		return baseCalculoIcmsSubstituicao;
	}

	public void setBaseCalculoIcmsSubstituicao(
			Double baseCalculoIcmsSubstituicao) {

		if (baseCalculoIcmsSubstituicao != null
				&& Double.isNaN(baseCalculoIcmsSubstituicao)) {
			this.baseCalculoIcmsSubstituicao = null;
		} else {
			this.baseCalculoIcmsSubstituicao = baseCalculoIcmsSubstituicao;
		}

	}

	public Double getValorIcmsSubstituicao() {
		return valorIcmsSubstituicao;
	}

	public void setValorIcmsSubstituicao(Double valorIcmsSubstituicao) {

		if (valorIcmsSubstituicao != null
				&& Double.isNaN(valorIcmsSubstituicao)) {
			this.valorIcmsSubstituicao = null;
		} else {
			this.valorIcmsSubstituicao = valorIcmsSubstituicao;
		}

	}

	public Double getValorFrete() {
		return valorFrete;
	}

	public void setValorFrete(Double valorFrete) {

		if (valorFrete != null && Double.isNaN(valorFrete)) {
			this.valorFrete = null;
		} else {
			this.valorFrete = valorFrete;
		}

	}

	public Double getValorSeguro() {
		return valorSeguro;
	}

	public void setValorSeguro(Double valorSeguro) {

		if (valorSeguro != null && Double.isNaN(valorSeguro)) {
			this.valorSeguro = null;
		} else {
			this.valorSeguro = valorSeguro;
		}

	}

	public Double getValorTotalIpi() {
		return valorTotalIpi;
	}

	public void setValorTotalIpi(Double valorTotalIpi) {

		if (valorTotalIpi != null && Double.isNaN(valorTotalIpi)) {
			this.valorTotalIpi = null;
		} else {
			this.valorTotalIpi = valorTotalIpi;
		}

	}

	public Double getValorOutrasDespesas() {
		return valorOutrasDespesas;
	}

	public void setValorOutrasDespesas(Double valorOutrasDespesas) {

		if (valorOutrasDespesas != null && Double.isNaN(valorOutrasDespesas)) {
			this.valorOutrasDespesas = null;
		} else {
			this.valorOutrasDespesas = valorOutrasDespesas;
		}

	}

	public Double getValorTotalServico() {
		return valorTotalServico;
	}

	public void setValorTotalServico(Double valorTotalServico) {

		if (valorTotalServico != null && Double.isNaN(valorTotalServico)) {
			this.valorTotalServico = null;
		} else {
			this.valorTotalServico = valorTotalServico;
		}

	}

	public Double getValorTotalProduto() {
		return valorTotalProduto;
	}

	public void setValorTotalProduto(Double valorTotalProduto) {

		if (valorTotalProduto != null && Double.isNaN(valorTotalProduto)) {
			this.valorTotalProduto = null;
		} else {
			this.valorTotalProduto = valorTotalProduto;
		}

	}

	public Double getValorTotalNota() {
		return valorTotalNota;
	}

	public void setValorTotalNota(Double valorTotalNota) {

		if (valorTotalNota != null && Double.isNaN(valorTotalNota)) {
			this.valorTotalNota = null;
		} else {
			this.valorTotalNota = valorTotalNota;
		}

	}

	public String getObservacao() {
		String retorno = null;

		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}

	public void setObservacao(String observacao) {
		if (observacao != null) {
			this.observacao = observacao.toUpperCase().trim();
		} else
			this.observacao = null;

	}

	public String getCifFob() {
		String retorno = null;

		if (cifFob != null)
			retorno = cifFob.toUpperCase().trim();
		return retorno;
	}

	public void setCifFob(String cifFob) {
		if (cifFob != null) {
			this.cifFob = cifFob.toUpperCase().trim();
		} else
			this.cifFob = null;

	}

	public String getPlacaVeiculo() {
		String retorno = null;

		if (placaVeiculo != null)
			retorno = placaVeiculo.toUpperCase().trim();
		return retorno;
	}

	public void setPlacaVeiculo(String placaVeiculo) {
		if (placaVeiculo != null) {
			this.placaVeiculo = placaVeiculo.toUpperCase().trim();
		} else
			this.placaVeiculo = null;

	}

	public Date getHoraSaidaEntrada() {
		return horaSaidaEntrada;
	}

	public void setHoraSaidaEntrada(Date horaSaidaEntrada) {
		this.horaSaidaEntrada = horaSaidaEntrada;
	}

	public Date getDataSaidaEntrada() {
		return dataSaidaEntrada;
	}

	public void setDataSaidaEntrada(Date dataSaidaEntrada) {
		this.dataSaidaEntrada = dataSaidaEntrada;
	}

	public Double getValorTotalContaReceber() {
		return valorTotalContaReceber;
	}

	public void setValorTotalContaReceber(Double valorTotalContaReceber) {

		if (valorTotalContaReceber != null
				&& Double.isNaN(valorTotalContaReceber)) {
			this.valorTotalContaReceber = null;
		} else {
			this.valorTotalContaReceber = valorTotalContaReceber;
		}

	}

	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public String getInscrSubsTrib() {
		String retorno = null;

		if (inscrSubsTrib != null)
			retorno = inscrSubsTrib.toUpperCase().trim();
		return retorno;
	}

	public void setInscrSubsTrib(String inscrSubsTrib) {
		if (inscrSubsTrib != null) {
			this.inscrSubsTrib = inscrSubsTrib.toUpperCase().trim();
		} else
			this.inscrSubsTrib = null;

	}

	public Integer getQuantidadeVolumes() {
		return quantidadeVolumes;
	}

	public void setQuantidadeVolumes(Integer quantidadeVolumes) {
		this.quantidadeVolumes = quantidadeVolumes;
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {

		if (peso != null && Double.isNaN(peso)) {
			this.peso = null;
		} else {
			this.peso = peso;
		}

	}

	public Date getDataFinalizacao() {
		return dataFinalizacao;
	}

	public void setDataFinalizacao(Date dataFinalizacao) {
		this.dataFinalizacao = dataFinalizacao;
	}

	public String getEntradaSaida() {
		String retorno = null;

		if (entradaSaida != null)
			retorno = entradaSaida.toUpperCase().trim();
		return retorno;
	}

	public void setEntradaSaida(String entradaSaida) {
		if (entradaSaida != null) {
			this.entradaSaida = entradaSaida.toUpperCase().trim();
		} else
			this.entradaSaida = null;

	}

	public String getIdNotaFiscalVendaConsignacao() {
		String retorno = null;

		if (idNotaFiscalVendaConsignacao != null)
			retorno = idNotaFiscalVendaConsignacao.toUpperCase().trim();
		return retorno;
	}

	public void setIdNotaFiscalVendaConsignacao(
			String idNotaFiscalVendaConsignacao) {
		if (idNotaFiscalVendaConsignacao != null) {
			this.idNotaFiscalVendaConsignacao = idNotaFiscalVendaConsignacao
					.toUpperCase().trim();
		} else
			this.idNotaFiscalVendaConsignacao = null;

	}

	public String getIdNotaFiscalDevolucaoConsignacao() {
		String retorno = null;

		if (idNotaFiscalDevolucaoConsignacao != null)
			retorno = idNotaFiscalDevolucaoConsignacao.toUpperCase().trim();
		return retorno;
	}

	public void setIdNotaFiscalDevolucaoConsignacao(
			String idNotaFiscalDevolucaoConsignacao) {
		if (idNotaFiscalDevolucaoConsignacao != null) {
			this.idNotaFiscalDevolucaoConsignacao = idNotaFiscalDevolucaoConsignacao
					.toUpperCase().trim();
		} else
			this.idNotaFiscalDevolucaoConsignacao = null;
	}

	public Integer getCfop2() {
		return cfop2;
	}

	public void setCfop2(Integer cfop2) {
		this.cfop2 = cfop2;
	}

	public Integer getCfop3() {
		return cfop3;
	}

	public void setCfop3(Integer cfop3) {
		this.cfop3 = cfop3;
	}

	public Double getValorTotalPis() {
		return valorTotalPis;
	}

	public void setValorTotalPis(Double valorTotalPis) {

		if (valorTotalPis != null && Double.isNaN(valorTotalPis)) {
			this.valorTotalPis = null;
		} else {
			this.valorTotalPis = valorTotalPis;
		}

	}

	public Double getValorTotalCofins() {
		return valorTotalCofins;
	}

	public void setValorTotalCofins(Double valorTotalCofins) {

		if (valorTotalCofins != null && Double.isNaN(valorTotalCofins)) {
			this.valorTotalCofins = null;
		} else {
			this.valorTotalCofins = valorTotalCofins;
		}

	}

	public Double getValorTotalCsll() {
		return valorTotalCsll;
	}

	public void setValorTotalCsll(Double valorTotalCsll) {

		if (valorTotalCsll != null && Double.isNaN(valorTotalCsll)) {
			this.valorTotalCsll = null;
		} else {
			this.valorTotalCsll = valorTotalCsll;
		}

	}

	public Double getValorTotalIrrf() {
		return valorTotalIrrf;
	}

	public void setValorTotalIrrf(Double valorTotalIrrf) {

		if (valorTotalIrrf != null && Double.isNaN(valorTotalIrrf)) {
			this.valorTotalIrrf = null;
		} else {
			this.valorTotalIrrf = valorTotalIrrf;
		}

	}

	public Integer getFormaPagamentoNfe() {
		return formaPagamentoNfe;
	}

	public void setFormaPagamentoNfe(Integer formaPagamentoNfe) {
		this.formaPagamentoNfe = formaPagamentoNfe;
	}

	public String getObservacoesComplementares() {
		String retorno = null;

		if (observacoesComplementares != null)
			retorno = observacoesComplementares.toUpperCase().trim();
		return retorno;
	}

	public void setObservacoesComplementares(String observacoesComplementares) {
		if (observacoesComplementares != null) {
			this.observacoesComplementares = observacoesComplementares
					.toUpperCase().trim();
		} else
			this.observacoesComplementares = null;

	}

	public Integer getStatusNfe() {
		return statusNfe;
	}

	public void setStatusNfe(Integer statusNfe) {
		this.statusNfe = statusNfe;
	}

	public Date getDataRetorno() {
		return dataRetorno;
	}

	public void setDataRetorno(Date dataRetorno) {
		this.dataRetorno = dataRetorno;
	}

	public String getNumProtocoloSefaz() {
		String retorno = null;

		if (numProtocoloSefaz != null)
			retorno = numProtocoloSefaz.toUpperCase().trim();
		return retorno;
	}

	public void setNumProtocoloSefaz(String numProtocoloSefaz) {
		if (numProtocoloSefaz != null) {
			this.numProtocoloSefaz = numProtocoloSefaz.toUpperCase().trim();
		} else
			this.numProtocoloSefaz = null;

	}

	public String getChaveAcesso() {
		String retorno = null;

		if (chaveAcesso != null)
			retorno = chaveAcesso.toUpperCase().trim();
		return retorno;
	}

	public void setChaveAcesso(String chaveAcesso) {
		if (chaveAcesso != null) {
			this.chaveAcesso = chaveAcesso.toUpperCase().trim();
		} else
			this.chaveAcesso = null;

	}

	public String getMotivoStatus() {
		String retorno = null;

		if (motivoStatus != null)
			retorno = motivoStatus.toUpperCase().trim();
		return retorno;
	}

	public void setMotivoStatus(String motivoStatus) {
		if (motivoStatus != null) {
			this.motivoStatus = motivoStatus.toUpperCase().trim();
		} else
			this.motivoStatus = null;

	}

	public String getCodigoInutilizacao() {
		String retorno = null;

		if (codigoInutilizacao != null)
			retorno = codigoInutilizacao.toUpperCase().trim();
		return retorno;
	}

	public void setCodigoInutilizacao(String codigoInutilizacao) {
		if (codigoInutilizacao != null) {
			this.codigoInutilizacao = codigoInutilizacao.toUpperCase().trim();
		} else
			this.codigoInutilizacao = null;

	}

	public String getStatusInutilizacao() {
		String retorno = null;

		if (statusInutilizacao != null)
			retorno = statusInutilizacao.toUpperCase().trim();
		return retorno;
	}

	public void setStatusInutilizacao(String statusInutilizacao) {
		if (statusInutilizacao != null) {
			this.statusInutilizacao = statusInutilizacao.toUpperCase().trim();
		} else
			this.statusInutilizacao = null;

	}

	public Integer getStatusImpressao() {
		return statusImpressao;
	}

	public void setStatusImpressao(Integer statusImpressao) {
		this.statusImpressao = statusImpressao;
	}

	public String getSerieContingencia() {
		String retorno = null;

		if (serieContingencia != null)
			retorno = serieContingencia.toUpperCase().trim();
		return retorno;
	}

	public void setSerieContingencia(String serieContingencia) {
		if (serieContingencia != null) {
			this.serieContingencia = serieContingencia.toUpperCase().trim();
		} else
			this.serieContingencia = null;

	}

	public String getSerie() {
		String retorno = null;

		if (serie != null)
			retorno = serie.toUpperCase().trim();
		return retorno;
	}

	public void setSerie(String serie) {
		if (serie != null) {
			this.serie = serie.toUpperCase().trim();
		} else
			this.serie = null;

	}

	public Integer getStatusCancelamento() {
		return statusCancelamento;
	}

	public void setStatusCancelamento(Integer statusCancelamento) {
		this.statusCancelamento = statusCancelamento;
	}

	public Integer getStatusContingencia() {
		return statusContingencia;
	}

	public void setStatusContingencia(Integer statusContingencia) {
		this.statusContingencia = statusContingencia;
	}

	public String getMotivoCancelamento() {
		String retorno = null;

		if (motivoCancelamento != null)
			retorno = motivoCancelamento.toUpperCase().trim();
		return retorno;
	}

	public void setMotivoCancelamento(String motivoCancelamento) {
		if (motivoCancelamento != null) {
			this.motivoCancelamento = motivoCancelamento.toUpperCase().trim();
		} else
			this.motivoCancelamento = null;

	}

	public String getRetornoStatusCancelamento() {
		String retorno = null;

		if (retornoStatusCancelamento != null)
			retorno = retornoStatusCancelamento.toUpperCase().trim();
		return retorno;
	}

	public void setRetornoStatusCancelamento(String retornoStatusCancelamento) {
		if (retornoStatusCancelamento != null) {
			this.retornoStatusCancelamento = retornoStatusCancelamento
					.toUpperCase().trim();
		} else
			this.retornoStatusCancelamento = null;

	}

	public String getRetornoStatusImpressao() {
		String retorno = null;

		if (retornoStatusImpressao != null)
			retorno = retornoStatusImpressao.toUpperCase().trim();
		return retorno;
	}

	public void setRetornoStatusImpressao(String retornoStatusImpressao) {
		if (retornoStatusImpressao != null) {
			this.retornoStatusImpressao = retornoStatusImpressao.toUpperCase()
					.trim();
		} else
			this.retornoStatusImpressao = null;

	}

	public String getMotivoInutilizacao() {
		String retorno = null;

		if (motivoInutilizacao != null)
			retorno = motivoInutilizacao.toUpperCase().trim();
		return retorno;
	}

	public void setMotivoInutilizacao(String motivoInutilizacao) {
		if (motivoInutilizacao != null) {
			this.motivoInutilizacao = motivoInutilizacao.toUpperCase().trim();
		} else
			this.motivoInutilizacao = null;

	}

	public String getRetornoStatusContingencia() {
		String retorno = null;

		if (retornoStatusContingencia != null)
			retorno = retornoStatusContingencia.toUpperCase().trim();
		return retorno;
	}

	public void setRetornoStatusContingencia(String retornoStatusContingencia) {
		if (retornoStatusContingencia != null) {
			this.retornoStatusContingencia = retornoStatusContingencia
					.toUpperCase().trim();
		} else
			this.retornoStatusContingencia = null;

	}

	public Double getValorRetencaoPis() {
		return valorRetencaoPis;
	}

	public void setValorRetencaoPis(Double valorRetencaoPis) {

		if (valorRetencaoPis != null && Double.isNaN(valorRetencaoPis)) {
			this.valorRetencaoPis = null;
		} else {
			this.valorRetencaoPis = valorRetencaoPis;
		}

	}

	public Double getValorRetencaoCofins() {
		return valorRetencaoCofins;
	}

	public void setValorRetencaoCofins(Double valorRetencaoCofins) {

		if (valorRetencaoCofins != null && Double.isNaN(valorRetencaoCofins)) {
			this.valorRetencaoCofins = null;
		} else {
			this.valorRetencaoCofins = valorRetencaoCofins;
		}

	}

	public Double getValorRetencaoIrrf() {
		return valorRetencaoIrrf;
	}

	public void setValorRetencaoIrrf(Double valorRetencaoIrrf) {

		if (valorRetencaoIrrf != null && Double.isNaN(valorRetencaoIrrf)) {
			this.valorRetencaoIrrf = null;
		} else {
			this.valorRetencaoIrrf = valorRetencaoIrrf;
		}

	}

	public Double getValorRetencaoCsll() {
		return valorRetencaoCsll;
	}

	public void setValorRetencaoCsll(Double valorRetencaoCsll) {

		if (valorRetencaoCsll != null && Double.isNaN(valorRetencaoCsll)) {
			this.valorRetencaoCsll = null;
		} else {
			this.valorRetencaoCsll = valorRetencaoCsll;
		}

	}

	public Double getValorDescontoTotal() {
		return valorDescontoTotal;
	}

	public void setValorDescontoTotal(Double valorDescontoTotal) {

		if (valorDescontoTotal != null && Double.isNaN(valorDescontoTotal)) {
			this.valorDescontoTotal = null;
		} else {
			this.valorDescontoTotal = valorDescontoTotal;
		}

	}

	public Double getValorIITotal() {
		return valorIITotal;
	}

	public void setValorIITotal(Double valorIITotal) {

		if (valorIITotal != null && Double.isNaN(valorIITotal)) {
			this.valorIITotal = null;
		} else {
			this.valorIITotal = valorIITotal;
		}

	}

	public String getNfeReferenciada() {
		String retorno = null;

		if (nfeReferenciada != null)
			retorno = nfeReferenciada.toUpperCase().trim();
		return retorno;
	}

	public void setNfeReferenciada(String nfeReferenciada) {
		if (nfeReferenciada != null) {
			this.nfeReferenciada = nfeReferenciada.toUpperCase().trim();
		} else
			this.nfeReferenciada = null;

	}

	public Integer getCodigoUFNFeReferenciada() {
		return codigoUFNFeReferenciada;
	}

	public void setCodigoUFNFeReferenciada(Integer codigoUFNFeReferenciada) {
		this.codigoUFNFeReferenciada = codigoUFNFeReferenciada;
	}

	public String getAnoMesEmissaoNFeRef() {
		String retorno = null;

		if (anoMesEmissaoNFeRef != null)
			retorno = anoMesEmissaoNFeRef.toUpperCase().trim();
		return retorno;
	}

	public void setAnoMesEmissaoNFeRef(String anoMesEmissaoNFeRef) {
		if (anoMesEmissaoNFeRef != null) {
			this.anoMesEmissaoNFeRef = anoMesEmissaoNFeRef.toUpperCase().trim();
		} else
			this.anoMesEmissaoNFeRef = null;

	}

	public String getCnpjEmitente() {
		String retorno = null;

		if (cnpjEmitente != null)
			retorno = cnpjEmitente.toUpperCase().trim();
		return retorno;
	}

	public void setCnpjEmitente(String cnpjEmitente) {
		if (cnpjEmitente != null) {
			this.cnpjEmitente = cnpjEmitente.toUpperCase().trim();
		} else
			this.cnpjEmitente = null;

	}

	public String getModeloNfeRef() {
		String retorno = null;

		if (modeloNfeRef != null)
			retorno = modeloNfeRef.toUpperCase().trim();
		return retorno;
	}

	public void setModeloNfeRef(String modeloNfeRef) {
		if (modeloNfeRef != null) {
			this.modeloNfeRef = modeloNfeRef.toUpperCase().trim();
		} else
			this.modeloNfeRef = null;

	}

	public String getSerieNFeRef() {
		String retorno = null;

		if (serieNFeRef != null)
			retorno = serieNFeRef.toUpperCase().trim();
		return retorno;
	}

	public void setSerieNFeRef(String serieNFeRef) {
		if (serieNFeRef != null) {
			this.serieNFeRef = serieNFeRef.toUpperCase().trim();
		} else
			this.serieNFeRef = null;

	}

	public String getNumeroNFeRef() {
		String retorno = null;

		if (numeroNFeRef != null)
			retorno = numeroNFeRef.toUpperCase().trim();
		return retorno;
	}

	public void setNumeroNFeRef(String numeroNFeRef) {
		if (numeroNFeRef != null) {
			this.numeroNFeRef = numeroNFeRef.toUpperCase().trim();
		} else
			this.numeroNFeRef = null;

	}

	public Integer getUf() {
		return uf;
	}

	public void setUf(Integer uf) {
		this.uf = uf;
	}

	public Integer getFinalidade() {
		return finalidade;
	}

	public void setFinalidade(Integer finalidade) {
		this.finalidade = finalidade;
	}

	public Integer getModelo() {
		return modelo;
	}

	public void setModelo(Integer modelo) {
		this.modelo = modelo;
	}

	public String getCpfReceptor() {
		String retorno = null;

		if (cpfReceptor != null)
			retorno = cpfReceptor.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}

	public void setCpfReceptor(String cpfReceptor) {
		if (cpfReceptor != null) {
			this.cpfReceptor = cpfReceptor.toUpperCase().trim()
					.replaceAll("[^0-9]", "");
		} else
			this.cpfReceptor = null;

	}

	public Integer getTpAmb() {
		return tpAmb;
	}

	public void setTpAmb(Integer tpAmb) {
		this.tpAmb = tpAmb;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getEnviouEmail() {
		return enviouEmail;
	}

	public void setEnviouEmail(Integer enviouEmail) {
		this.enviouEmail = enviouEmail;
	}

	@Override
	public void validate() throws Exception {
	}
}