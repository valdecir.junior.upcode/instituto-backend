package br.com.desenv.nepalign.integracao.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="historicocobranca")
public class HistoricoCobrancaDTO extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idhistoricoCobranca")
	private Integer id;
	@Column(name="dataCobranca")
	private Date dataCobranca;
	@Column(name="observacao")
	private String observacao;
	@Column(name = "idTipoContato")
	private Integer tipoContato;
	@Column(name = "idCliente")
	private Integer cliente;
	@Column(name="alerta")
	private Integer alerta;

	public HistoricoCobrancaDTO()
	{
		super();
	}

	public HistoricoCobrancaDTO
	(
		Date dataCobranca,
		String observacao,
		Integer tipoContato,
		Integer cliente,
		Integer alerta
	) 
	{
		super();
		this.dataCobranca = dataCobranca;
		this.observacao = observacao;
		this.tipoContato = tipoContato;
		this.cliente = cliente;
		this.alerta = alerta;
		
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}	
	
	public Date getDataCobranca() 
	{
		return dataCobranca;
	}

	public void setDataCobranca(Date dataCobranca) 
	{
		this.dataCobranca = dataCobranca;
	}
	
	public String getObservacao() 
	{
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setObservacao(String observacao) 
	{
		if (observacao != null)
			this.observacao = observacao.toUpperCase().trim();
		else
			this.observacao = null;
	}
		
	public Integer getTipoContato() 
	{
		return tipoContato;
	}

	public void setTipoContato(Integer tipoContato) 
	{
		this.tipoContato = tipoContato;
	}
	
	public Integer getCliente() 
	{
		return cliente;
	}

	public void setCliente(Integer cliente) 
	{
		this.cliente = cliente;
	}
	
	public Integer getAlerta() 
	{
		return alerta;
	}

	public void setAlerta(Integer alerta) 
	{
		this.alerta = alerta;
	}

	@Override
	public void validate() throws Exception
	{
	}
	
	public HistoricoCobrancaDTO clone() throws CloneNotSupportedException 
	{
		return (HistoricoCobrancaDTO) super.clone();
	}
}