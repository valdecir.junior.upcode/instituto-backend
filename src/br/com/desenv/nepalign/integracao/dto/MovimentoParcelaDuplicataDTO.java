package br.com.desenv.nepalign.integracao.dto;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;
import br.com.desenv.nepalign.model.DuplicataParcelaCobranca;
import br.com.desenv.nepalign.model.SeprocadoReativados;

@Entity
@Table(name="MovimentoParcelaDuplicata")
public class MovimentoParcelaDuplicataDTO extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idMovimentoParcelaDuplicata")
	private Integer id;
		
	@Column(name = "idDuplicata")
	private Integer duplicata;

	@Column(name = "idDuplicataParcela")
	private Integer duplicataParcela;
	
	@Column(name = "idEmpresaFisica")
	private Integer empresaFisica;
	
	@Column(name = "idCliente")
	private Integer cliente;
	
	@Column(name = "idUsuario")
	private Integer usuario;
	
	@Column(name = "idFormaPagamento")
	private Integer formaPagamento;
	
	@Column(name="numeroDuplicata")
	private Long numeroDuplicata;
	
	@Column(name="numeroParcela")
	private Integer numeroParcela;
	
	@Column(name="dataMovimento")
	private Date dataMovimento;
	
	@Column(name="tipoPagamento")
	private String tipoPagamento;
	
	@Column(name="valorCompraPago")
	private Double valorCompraPago;
	
	@Column(name="valorEntradaJuros")
	private Double valorEntradaJuros;
	
	@Column(name="valorRecebido")
	private Double valorRecebido;
	
	@Column(name="dataVencimento")
	private Date dataVencimento;
	
	@Column(name="valorParcela")
	private Double valorParcela;
	
	@Column(name="historico")
	private String historico;

	@Column(name = "idChequeRecebido")
	private Integer chequeRecebido;

	@Column(name = "idPagamentoCartao")
	private Integer pagamentoCartao;
	
	@Column(name="valorDesconto")
	private Double valorDesconto;
	
	@Column(name="horaMovimento")
	private Timestamp horaMovimento;
	
	@Transient
	private Integer seprocadoReativados;
	
	@Transient
	private Integer duplicataParcelaCobranca;
	
	public MovimentoParcelaDuplicataDTO()
	{
		super();
	}
	
	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{	
			this.id = id;	
	}	
	
	public Integer getFormaPagamento() 
	{
		return formaPagamento;
	}

	public void setFormaPagamento(Integer formaPagamento) 
	{
		this.formaPagamento = formaPagamento;
	}

	public Integer getDuplicata() 
	{
		return duplicata;
	}

	public void setDuplicata(Integer duplicata) 
	{
		this.duplicata = duplicata;
	}
	
	public Integer getDuplicataParcela() 
	{
		return duplicataParcela;
	}

	public void setDuplicataParcela(Integer duplicataParcela) 
	{
		this.duplicataParcela = duplicataParcela;
	}
	
	public Double getValorDesconto() 
	{
		return valorDesconto;
	}

	public void setValorDesconto(Double valorDesconto) 
	{
		if(valorDesconto.isNaN())
			this.valorDesconto = null;	
		else
			this.valorDesconto = valorDesconto;
	}

	public Integer getEmpresaFisica() 
	{
		return empresaFisica;
	}

	public void setEmpresaFisica(Integer empresaFisica) 
	{
		this.empresaFisica = empresaFisica;
	}
	
	public Integer getCliente() 
	{
		return cliente;
	}

	public void setCliente(Integer cliente) 
	{
		this.cliente = cliente;
	}
	
	public Integer getUsuario() 
	{
		return usuario;
	}

	public void setUsuario(Integer usuario) 
	{
		this.usuario = usuario;
	}
	
	public Long getNumeroDuplicata() 
	{
		return numeroDuplicata;
	}

	public void setNumeroDuplicata(Long numeroDuplicata) 
	{
			this.numeroDuplicata = numeroDuplicata;	
	}	
	
	public Integer getNumeroParcela() 
	{
		return numeroParcela;
	}

	public void setNumeroParcela(Integer numeroParcela) 
	{	
			this.numeroParcela = numeroParcela;	
	}	
	
	public Date getDataMovimento() 
	{
		return dataMovimento;
	}

	public void setDataMovimento(Date dataMovimento) 
	{
		this.dataMovimento = dataMovimento;
	}
	
	public String getTipoPagamento() 
	{
		String retorno = null;
		
		if (tipoPagamento != null)
			retorno = tipoPagamento.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setTipoPagamento(String tipoPagamento) 
	{
		if (tipoPagamento != null)
			this.tipoPagamento = tipoPagamento.toUpperCase().trim();
		else
			this.tipoPagamento = null;
	}
		
	public Double getValorCompraPago() 
	{
		return valorCompraPago;
	}

	public void setValorCompraPago(Double valorCompraPago) 
	{
		if (valorCompraPago != null && Double.isNaN(valorCompraPago))
			this.valorCompraPago = null;
		else
			this.valorCompraPago = valorCompraPago;
	}
	
	public Double getValorEntradaJuros() 
	{
		return valorEntradaJuros;
	}

	public void setValorEntradaJuros(Double valorEntradaJuros) 
	{	
		if(valorEntradaJuros == null)
			this.valorEntradaJuros = null;
		else if (valorEntradaJuros != null && valorEntradaJuros == 0.0)
			this.valorEntradaJuros = null;
		else if(valorEntradaJuros.isNaN())
			this.valorEntradaJuros = null;
		else
			this.valorEntradaJuros = valorEntradaJuros;
	}
	
	public Double getValorRecebido() 
	{
		return valorRecebido;
	}

	public void setValorRecebido(Double valorRecebido) 
	{		
		this.valorRecebido = valorRecebido;
	}	
	
	public Date getDataVencimento() 
	{
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) 
	{
		this.dataVencimento = dataVencimento;
	}
	
	public Double getValorParcela() 
	{
		return valorParcela;
	}

	public void setValorParcela(Double valorParcela) 
	{
		if (valorParcela != null && valorParcela == 0.0)
			this.valorParcela = null;
		else
			this.valorParcela = valorParcela;
	}	
	
	public String getHistorico() 
	{
		String retorno = null;
		
		if (historico != null)
			retorno = historico.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setHistorico(String historico) 
	{
		if (historico != null)
			this.historico = historico.toUpperCase().trim();
		else
			this.historico = null;
	}
		
	public Integer getChequeRecebido() 
	{
		return chequeRecebido;
	}

	public void setChequeRecebido(Integer chequeRecebido) 
	{
		this.chequeRecebido = chequeRecebido;
	}

	public Integer getPagamentoCartao() 
	{
		return pagamentoCartao;
	}

	public void setPagamentoCartao(Integer pagamentoCartao) 
	{
		this.pagamentoCartao = pagamentoCartao;
	}

	public Timestamp getHoraMovimento() 
	{
		return horaMovimento;
	}

	public void setHoraMovimento(Timestamp horaMovimento) 
	{
		this.horaMovimento = horaMovimento;
	}

	public Integer getSeprocadoReativados() 
	{
		return seprocadoReativados;
	}

	public void setSeprocadoReativados(Integer seprocadoReativados) 
	{
		this.seprocadoReativados = seprocadoReativados;
	}

	public Integer getDuplicataParcelaCobranca() 
	{
		return duplicataParcelaCobranca;
	}

	public void setDuplicataParcelaCobranca(Integer duplicataParcelaCobranca) 
	{
		this.duplicataParcelaCobranca = duplicataParcelaCobranca;
	}

	@Override
	public void validate() throws Exception { }

	@Override
	public MovimentoParcelaDuplicataDTO clone() throws CloneNotSupportedException 
	{
		return (MovimentoParcelaDuplicataDTO) super.clone();
	}
}