package br.com.desenv.nepalign.integracao.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="imagemUsuario")
public class ImagemUsuarioDTO  extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;
	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idImagemUsuario")
	private Integer id;
	@Column(name = "idTabelaSistema")
	private Integer tabelaSistema;
	@Column(name="descricao")
	private String descricao; 
	@Column(name="campoChave")
	private String campoChave;
	@Column(name="infoCampoChave")
	private String infoCampoChave;
	@Transient
	private byte[] imagem;

	public ImagemUsuarioDTO()
	{
		super();
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public Integer getTabelaSistema() 
	{
		return tabelaSistema;
	}

	public void setTabelaSistema(Integer tabelaSistema) 
	{
		this.tabelaSistema = tabelaSistema;
	}
	
	public String getDescricao() 
	{
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setDescricao(String descricao) 
	{
		if (descricao != null)
			this.descricao = descricao.toUpperCase().trim();
		else
			this.descricao = null;
	}
		
	public String getCampoChave() 
	{
		String retorno = null;
		
		if (campoChave != null)
			retorno = campoChave.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setCampoChave(String campoChave) 
	{
		if (campoChave != null)
			this.campoChave = campoChave.toUpperCase().trim();
		else
			this.campoChave = null;
	}
		
	public String getInfoCampoChave() 
	{
		String retorno = null;
		
		if (infoCampoChave != null)
			retorno = infoCampoChave.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setInfoCampoChave(String infoCampoChave) 
	{
		if (infoCampoChave != null)
			this.infoCampoChave = infoCampoChave.toUpperCase().trim();
		else
			this.infoCampoChave = null;
	}
		
	public byte[] getImagem() 
	{
		return imagem;
	}

	public void setImagem(byte[] imagem) 
	{
		this.imagem = imagem;
	}
	
	@Override
	public void validate() throws Exception { }
}