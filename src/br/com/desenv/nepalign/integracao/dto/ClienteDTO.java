package br.com.desenv.nepalign.integracao.dto;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="cliente")
public class ClienteDTO extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idCliente")
	private Integer id;
		
	@Column(name = "foto")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] foto;
	
	@Column(name="nome")
	private String nome;
	
	@Column(name="logradouro")
	private String logradouro;
	
	@Column(name="numeroEndereco")
	private String numeroEndereco;
	
	@Column(name="complementoEndereco")
	private String complementoEndereco;
	
	@Column(name="itinerarioEndereco")
	private String itinerario;
	
	@Column(name = "idCidadeEndereco")
	private Integer cidadeEndereco;
	
	@Column(name="cep")
	private String cep;
	
	@Column(name="cidadeCache")
	private String cidadeCache;
	
	@Column(name="estadoCache")
	private String estadoCache;
	
	@Column(name="residenciaPropria")
	private String residenciaPropria;
	
	@Column(name="valorAluguelPrestacao")
	private Double valorAluguelPrestacao;
	
	@Column(name="cpfCnpj")
	private String cpfCnpj;
	
	@Column(name="bairro")
	private String bairro;
	
	@Column(name="nomeMae")
	private String nomeMae;
	
	@Column(name="nomePai")
	private String nomePai;
	
	@Column(name="dataHoraCadastro")
	private Date dataHoraCadastro;
	
	@Column(name="dataNascimento")
	private Date dataNascimento;
	
	@Column(name = "idOperadoraTelefoneResidencial")
	private Integer operadoraTelefoneResidencial;
	
	@Column(name="telefoneResidencial")
	private String telefoneResidencial;
	
	@Column(name = "idOperadoraCelular01")
	private Integer operadoraCelular01;
	
	@Column(name="telefoneCelular01")
	private String telefoneCelular01;
	
	@Column(name = "idOperadoraCelular02")
	private Integer operadoraCelular02;
	
	@Column(name="telefoneCelular02")
	private String telefoneCelular02;
	
	@Column(name="mediaAtraso")
	private Integer mediaAtraso;
	
	@Column(name="dataEntradaSCPC")
	private Date dataEntradaScpc;
	
	@Column(name="dataBaixaSCPC")
	private Date dataBaixaScpc;
	
	@Column(name="salario")
	private Double salario;
	
	@Column(name="outraRenda")
	private Double outraRenda;
	
	@Column(name="estadoCivil")
	private String estadoCivil;
	
	@Column(name="rgInscricaoEstadual")
	private String rgInscricaoEstadual;
	
	@Column(name="tempoResidencia")
	private Date tempoResidencia;
	
	@Column(name="empresaTrabalha")
	private String empresaTrabalha;
	
	@Column(name="profissao")
	private String profissao;
	
	@Column(name="tempoTrabalho")
	private Date tempoTrabalho;
	
	@Column(name="logradouroComercial")
	private String logradouroComercial;
	
	@Column(name="numeroComercial")
	private String numeroComercial;
	
	@Column(name = "idCidadeComercial")
	private Integer cidadeComercial;
	
	@Column(name="cidadeComercialCache")
	private String cidadeComercialCache;
	
	@Column(name="estadoComercialCache")
	private String estadoComercialCache;
	
	@Column(name="cepComercial")
	private String cepComercial;
	
	@Column(name = "idOperadoraComercial")
	private Integer operadoraComercial;
	
	@Column(name="telefoneComercial")
	private String telefoneComercial;
	
	@Column(name="informacaoComercial")
	private String informacaoComercial;
	
	@Column(name="numeroCalcado")
	private String numeroCalcado;
	
	@Column(name="time")
	private String time;
	
	@Column(name="observacao")
	private String observacao;
	
	@Column(name="historico")
	private String historico;
	
	@Column(name="codigoExterno")
	private Integer codigoExterno;

	@Column(name="idTipoLogradouro")
	private Integer tipoLogradouro;
	
	@Column(name="idGrupoCliente")
	private Integer grupoCliente;

	@Column(name = "idAtividade")
	private Integer atividade;
	
	@Column(name = "idConjuge")
	private Integer conjuge;
	
	@Column(name = "idAvalista")
	private Integer avalista;
	
	@Column(name = "fotoRg")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] fotoRg;
	
	@Column(name = "fotoRgVerso")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] fotoRgVerso;
	
	@Column(name = "fotoCpf")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] fotoCpf;
	
	@Column(name = "fotoInconsistencia")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] fotoInconsistencia;
	
	@Column(name = "contato")
	private String contato;
	
	@Column(name = "emailPessoal")
	private String emailPessoal;
	
	@Column(name = "emailComercial")
	private String emailComercial;
	
	@Column(name = "emailOutro")
	private String emailOutro;

	public ClienteDTO()
	{
		super();
	}
	
	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public byte[] getFoto() 
	{
		return foto;
	}

	public void setFoto(byte[] foto) 
	{
		this.foto = foto;
	}
	
	public String getNome() 
	{
		String retorno = null;
		
		if (nome != null)
			retorno = nome.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNome(String nome) 
	{
		if (nome != null)
			this.nome = nome.toUpperCase().trim();
		else
			this.nome = null;	
	}
		
	public String getLogradouro() 
	{
		String retorno = null;
		
		if (logradouro != null)
			retorno = logradouro.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setLogradouro(String logradouro) 
	{
		if (logradouro != null)
			this.logradouro = logradouro.toUpperCase().trim();
		else
			this.logradouro = null;
	}
	
	public String getNumeroEndereco() 
	{
		String retorno = null;
		
		if (numeroEndereco != null)
			retorno = numeroEndereco.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNumeroEndereco(String numeroEndereco) 
	{
		if (numeroEndereco != null)
			this.numeroEndereco = numeroEndereco.toUpperCase().trim();
		else
			this.numeroEndereco = null;	
	}
		
	public String getComplementoEndereco() 
	{
		String retorno = null;
		
		if (complementoEndereco != null)
			retorno = complementoEndereco.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setComplementoEndereco(String complementoEndereco) 
	{
		if (complementoEndereco != null)
			this.complementoEndereco = complementoEndereco.toUpperCase().trim();
		else
			this.complementoEndereco = null;	
	}
		
	public String getItinerario() 
	{
		String retorno = null;
		
		if (itinerario != null)
			retorno = itinerario.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setItinerario(String itinerario) 
	{
		if (itinerario != null)
			this.itinerario = itinerario.toUpperCase().trim();
		else
			this.itinerario = null;	
	}
		
	public Integer getCidadeEndereco() 
	{
		return cidadeEndereco;
	}

	public void setCidadeEndereco(Integer cidadeEndereco) 
	{
		this.cidadeEndereco = cidadeEndereco;
	}
	
	public String getCep() 
	{
		String retorno = null;
		
		if (cep != null)
			retorno = cep.toUpperCase().trim().replaceAll("[^0-9]", "");
		
		return retorno;
	}
	
	public void setCep(String cep) 
	{
		if (cep != null)
			this.cep = cep.toUpperCase().trim().replaceAll("[^0-9]", "");
		else
			this.cep = null;
	}
		
	public String getCidadeCache() 
	{
		String retorno = null;
		
		if (cidadeCache != null)
			retorno = cidadeCache.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setCidadeCache(String cidadeCache) 
	{
		if (cidadeCache != null)
			this.cidadeCache = cidadeCache.toUpperCase().trim();
		else
			this.cidadeCache = null;
	}
		
	public String getEstadoCache() 
	{
		String retorno = null;
		
		if (estadoCache != null)
			retorno = estadoCache.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setEstadoCache(String estadoCache) 
	{
		if (estadoCache != null)
			this.estadoCache = estadoCache.toUpperCase().trim();
		else
			this.estadoCache = null;
	}
		
	public String getResidenciaPropria() 
	{
		String retorno = null;
		
		if (residenciaPropria != null)
			retorno = residenciaPropria.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setResidenciaPropria(String residenciaPropria) 
	{
		if (residenciaPropria != null)
			this.residenciaPropria = residenciaPropria.toUpperCase().trim();
		else
			this.residenciaPropria = null;	
	}
		
	public Double getValorAluguelPrestacao() 
	{
		return valorAluguelPrestacao;
	}

	public void setValorAluguelPrestacao(Double valorAluguelPrestacao) 
	{
		if (valorAluguelPrestacao != null && Double.isNaN(valorAluguelPrestacao))
			this.valorAluguelPrestacao = null;
		else
			this.valorAluguelPrestacao = valorAluguelPrestacao;
	}
	
	public String getCpfCnpj() 
	{
		String retorno = null;
		
		if (cpfCnpj != null)
			retorno = cpfCnpj.toUpperCase().trim().replaceAll("[^0-9]", "");
		
		return retorno;
	}
	
	public void setCpfCnpj(String cpfCnpj) 
	{
		if (cpfCnpj != null)
			this.cpfCnpj = cpfCnpj.toUpperCase().trim().replaceAll("[^0-9]", "");
		else
			this.cpfCnpj = null;	
	}
		
	public String getBairro() 
	{
		String retorno = null;
		
		if (bairro != null)
			retorno = bairro.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setBairro(String bairro) 
	{
		if (bairro != null)
			this.bairro = bairro.toUpperCase().trim();
		else
			this.bairro = null;	
	}
		
	public String getNomeMae() 
	{
		String retorno = null;
		
		if (nomeMae != null)
			retorno = nomeMae.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNomeMae(String nomeMae) 
	{
		if (nomeMae != null)
			this.nomeMae = nomeMae.toUpperCase().trim();
		else
			this.nomeMae = null;	
	}
		
	public String getNomePai() 
	{
		String retorno = null;
		
		if (nomePai != null)
			retorno = nomePai.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNomePai(String nomePai) 
	{
		if (nomePai != null)
			this.nomePai = nomePai.toUpperCase().trim();
		else
			this.nomePai = null;	
	}
		
	public Date getDataHoraCadastro() 
	{
		return dataHoraCadastro;
	}

	public void setDataHoraCadastro(Date dataHoraCadastro) 
	{
		this.dataHoraCadastro = dataHoraCadastro;
	}
	
	public Date getDataNascimento() 
	{
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) 
	{
		this.dataNascimento = dataNascimento;
	}
	
	public Integer getOperadoraTelefoneResidencial() 
	{
		return operadoraTelefoneResidencial;
	}

	public void setOperadoraTelefoneResidencial(Integer operadoraTelefoneResidencial) 
	{
		this.operadoraTelefoneResidencial = operadoraTelefoneResidencial;
	}
	
	public String getTelefoneResidencial() 
	{
		String retorno = null;
		
		if (telefoneResidencial != null)
			retorno = telefoneResidencial.toUpperCase().trim().replaceAll("[^0-9]", "");
		
		return retorno;
	}
	
	public void setTelefoneResidencial(String telefoneResidencial) 
	{
		if (telefoneResidencial != null)
			this.telefoneResidencial = telefoneResidencial.toUpperCase().trim().replaceAll("[^0-9]", "");
		else
			this.telefoneResidencial = null;	
	}
		
	public Integer getOperadoraCelular01() 
	{
		return operadoraCelular01;
	}

	public void setOperadoraCelular01(Integer operadoraCelular01) 
	{
		this.operadoraCelular01 = operadoraCelular01;
	}
	
	public String getTelefoneCelular01() 
	{
		String retorno = null;
		
		if (telefoneCelular01 != null)
			retorno = telefoneCelular01.toUpperCase().trim().replaceAll("[^0-9]", "");
		
		return retorno;
	}
	
	public void setTelefoneCelular01(String telefoneCelular01) 
	{
		if (telefoneCelular01 != null)
			this.telefoneCelular01 = telefoneCelular01.toUpperCase().trim().replaceAll("[^0-9]", "");
		else
			this.telefoneCelular01 = null;	
	}
		
	public Integer getOperadoraCelular02() 
	{
		return operadoraCelular02;
	}

	public void setOperadoraCelular02(Integer operadoraCelular02) 
	{
		this.operadoraCelular02 = operadoraCelular02;
	}
	
	public String getTelefoneCelular02() 
	{
		String retorno = null;
		
		if (telefoneCelular02 != null)
			retorno = telefoneCelular02.toUpperCase().trim().replaceAll("[^0-9]", "");
		
		return retorno;
	}
	
	public void setTelefoneCelular02(String telefoneCelular02) 
	{
		if (telefoneCelular02 != null)
			this.telefoneCelular02 = telefoneCelular02.toUpperCase().trim().replaceAll("[^0-9]", "");
		else
			this.telefoneCelular02 = null;	
	}
		
	public Integer getConjuge() 
	{
		return conjuge;
	}

	public void setConjuge(Integer conjuge) 
	{
		this.conjuge = conjuge;
	}

	public Integer getMediaAtraso() 
	{
		return mediaAtraso;
	}

	public void setMediaAtraso(Integer mediaAtraso) 
	{
		if (mediaAtraso != null && mediaAtraso == 0)
			this.mediaAtraso = null;
		else
			this.mediaAtraso = mediaAtraso;
	}
	
	public Date getDataEntradaScpc() 
	{
		return dataEntradaScpc;
	}

	public void setDataEntradaScpc(Date dataEntradaScpc) 
	{
		this.dataEntradaScpc = dataEntradaScpc;
	}
	
	public Date getDataBaixaScpc() 
	{
		return dataBaixaScpc;
	}

	public void setDataBaixaScpc(Date dataBaixaScpc) 
	{
		this.dataBaixaScpc = dataBaixaScpc;
	}
	
	public Double getSalario() 
	{
		return salario;
	}

	public void setSalario(Double salario) 
	{
		if (salario != null && Double.isNaN(salario))
			this.salario = null;
		else
			this.salario = salario;
	}
	
	public Double getOutraRenda() 
	{
		return outraRenda;
	}

	public void setOutraRenda(Double outraRenda) 
	{
		if (outraRenda != null && Double.isNaN(outraRenda))
			this.outraRenda = null;
		else
			this.outraRenda = outraRenda;
	}
	
	public String getEstadoCivil() 
	{
		String retorno = null;
		
		if (estadoCivil != null)
			retorno = estadoCivil.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setEstadoCivil(String estadoCivil) 
	{
		if (estadoCivil != null)
			this.estadoCivil = estadoCivil.toUpperCase().trim();
		else
			this.estadoCivil = null;
	}
		
	public String getRgInscricaoEstadual() 
	{
		String retorno = null;
		
		if (rgInscricaoEstadual != null)
			retorno = rgInscricaoEstadual.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setRgInscricaoEstadual(String rgInscricaoEstadual) 
	{
		if (rgInscricaoEstadual != null)
			this.rgInscricaoEstadual = rgInscricaoEstadual.toUpperCase().trim();
		else
			this.rgInscricaoEstadual = null;	
	}
		
	public Date getTempoResidencia() 
	{
		return tempoResidencia;
	}

	public void setTempoResidencia(Date tempoResidencia) 
	{
		this.tempoResidencia = tempoResidencia;
	}
	
	public String getEmpresaTrabalha() 
	{
		String retorno = null;
		
		if (empresaTrabalha != null)
			retorno = empresaTrabalha.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setEmpresaTrabalha(String empresaTrabalha) 
	{
		if (empresaTrabalha != null)
			this.empresaTrabalha = empresaTrabalha.toUpperCase().trim();
		else
			this.empresaTrabalha = null;	
	}
		
	public String getProfissao() 
	{
		String retorno = null;
		
		if (profissao != null)
			retorno = profissao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setProfissao(String profissao) 
	{
		if (profissao != null)
			this.profissao = profissao.toUpperCase().trim();
		else
			this.profissao = null;	
	}
		
	public Date getTempoTrabalho() 
	{
		return tempoTrabalho;
	}

	public void setTempoTrabalho(Date tempoTrabalho) 
	{
		this.tempoTrabalho = tempoTrabalho;
	}
	
	public String getLogradouroComercial() 
	{
		String retorno = null;
		
		if (logradouroComercial != null)
			retorno = logradouroComercial.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setLogradouroComercial(String logradouroComercial) 
	{
		if (logradouroComercial != null)
			this.logradouroComercial = logradouroComercial.toUpperCase().trim();
		else
			this.logradouroComercial = null;	
	}
		
	public String getNumeroComercial() 
	{
		String retorno = null;
		
		if (numeroComercial != null)
			retorno = numeroComercial.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNumeroComercial(String numeroComercial) 
	{
		if (numeroComercial != null)
			this.numeroComercial = numeroComercial.toUpperCase().trim();
		else
			this.numeroComercial = null;
	}
		
	public Integer getCidadeComercial() 
	{
		return cidadeComercial;
	}

	public void setCidadeComercial(Integer cidadeComercial) 
	{
		this.cidadeComercial = cidadeComercial;
	}
	
	public String getCidadeComercialCache() 
	{
		String retorno = null;
		
		if (cidadeComercialCache != null)
			retorno = cidadeComercialCache.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setCidadeComercialCache(String cidadeComercialCache) 
	{
		if (cidadeComercialCache != null)
			this.cidadeComercialCache = cidadeComercialCache.toUpperCase().trim();
		else
			this.cidadeComercialCache = null;
	}
		
	public String getEstadoComercialCache() 
	{
		String retorno = null;
		
		if (estadoComercialCache != null)
			retorno = estadoComercialCache.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setEstadoComercialCache(String estadoComercialCache) 
	{
		if (estadoComercialCache != null)
			this.estadoComercialCache = estadoComercialCache.toUpperCase().trim();
		else
			this.estadoComercialCache = null;
	}
		
	public String getCepComercial() 
	{
		String retorno = null;
		
		if (cepComercial != null)
			retorno = cepComercial.toUpperCase().trim().replaceAll("[^0-9]", "");
		
		return retorno;
	}
	
	public void setCepComercial(String cepComercial) 
	{
		if (cepComercial != null)
			this.cepComercial = cepComercial.toUpperCase().trim().replaceAll("[^0-9]", "");
		else
			this.cepComercial = null;
	}
		
	public Integer getOperadoraComercial() 
	{
		return operadoraComercial;
	}

	public void setOperadoraComercial(Integer operadoraComercial) 
	{
		this.operadoraComercial = operadoraComercial;
	}
	
	public String getTelefoneComercial() 
	{
		String retorno = null;
		
		if (telefoneComercial != null)
			retorno = telefoneComercial.toUpperCase().trim().replaceAll("[^0-9]", "");
		
		return retorno;
	}
	
	public void setTelefoneComercial(String telefoneComercial) 
	{
		if (telefoneComercial != null)
			this.telefoneComercial = telefoneComercial.toUpperCase().trim().replaceAll("[^0-9]", "");
		else
			this.telefoneComercial = null;	
	}
		
	public String getInformacaoComercial() 
	{
		String retorno = null;
		
		if (informacaoComercial != null)
			retorno = informacaoComercial.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setInformacaoComercial(String informacaoComercial) 
	{
		if (informacaoComercial != null)
			this.informacaoComercial = informacaoComercial.toUpperCase().trim();
		else
			this.informacaoComercial = null;
	}
		
	public String getNumeroCalcado() 
	{
		String retorno = null;
		
		if (numeroCalcado != null)
			retorno = numeroCalcado.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNumeroCalcado(String numeroCalcado) 
	{
		if (numeroCalcado != null)
			this.numeroCalcado = numeroCalcado.toUpperCase().trim();
		else
			this.numeroCalcado = null;
	}
		
	public String getTime() 
	{
		String retorno = null;
		
		if (time != null)
			retorno = time.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setTime(String time) 
	{
		if (time != null)
			this.time = time.toUpperCase().trim();
		else
			this.time = null;
	}
		
	public String getObservacao() 
	{
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setObservacao(String observacao) 
	{
		if (observacao != null)
			this.observacao = observacao.toUpperCase().trim();
		else
			this.observacao = null;
	}
		
	public String getHistorico() 
	{
		String retorno = null;
		
		if (historico != null)
			retorno = historico.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setHistorico(String historico) 
	{
		if (historico != null)
			this.historico = historico.toUpperCase().trim();
		else
			this.historico = null;	
	}
	
	public Integer getCodigoExterno()
	{
		return this.codigoExterno;
	}
	
	public void setCodigoExterno(Integer value)
	{
		this.codigoExterno = value;
	}
		
	public byte[] getFotoRg() 
	{
		return fotoRg;
	}

	public void setFotoRg(byte[] fotoRg) 
	{
		this.fotoRg = fotoRg;
	}
	
	public byte[] getFotoCpf() 
	{
		return fotoCpf;
	}

	public void setFotoCpf(byte[] fotoCpf) 
	{
		this.fotoCpf = fotoCpf;
	}	
	
	public byte[] getFotoInconsistencia()
	{
		return fotoInconsistencia;
	}
	
	public void setFotoInconsistencia(byte[] fotoInconsistencia)
	{
		this.fotoInconsistencia = fotoInconsistencia;
	}
	
	public Integer getTipoLogradouro() 
	{
		return tipoLogradouro;
	}

	public void setTipoLogradouro(Integer tipoLogradouro) 
	{
		this.tipoLogradouro = tipoLogradouro;
	}
	
	public Integer getGrupoCliente() 
	{
		return grupoCliente;
	}

	public void setGrupoCliente(Integer grupoCliente)
	{
		this.grupoCliente = grupoCliente;
	}

	public Integer getAtividade() 
	{
		return atividade;
	}


	public void setAtividade(Integer atividade) 
	{
		this.atividade = atividade;
	}

	public Integer getAvalista() 
	{
		return avalista;
	}

	public void setAvalista(Integer avalista) 
	{
		this.avalista = avalista;
	}
	
	public String getContato()
	{
		return contato;
	}
	
	public void setContato(String contato)
	{
		this.contato = contato;
	}


	public byte[] getFotoRgVerso() 
	{
		return fotoRgVerso;
	}

	public void setFotoRgVerso(byte[] fotoRgVerso) 
	{
		this.fotoRgVerso = fotoRgVerso;
	}

	public String getEmailPessoal() 
	{
		return emailPessoal;
	}

	public void setEmailPessoal(String emailPessoal) 
	{
		this.emailPessoal = emailPessoal;
	}

	public String getEmailComercial() 
	{
		return emailComercial;
	}

	public void setEmailComercial(String emailComercial) 
	{
		this.emailComercial = emailComercial;
	}

	public String getEmailOutro() 
	{
		return emailOutro;
	}

	public void setEmailOutro(String emailOutro) 
	{
		this.emailOutro = emailOutro;
	}
	
	@Override
	public void validate() throws Exception { }
	
	@Override
	public ClienteDTO clone() throws CloneNotSupportedException 
	{
		return (ClienteDTO) super.clone();
	}
}