package br.com.desenv.nepalign.integracao.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="tipoConta")
public class TipoContaDTO  extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idTipoConta")
	private Integer id;
	@Column(name="descricao")
	private String descricao;
	@Column(name="incideCpmf")
	private Integer incideCpmf;
	@Column(name="credito")
	private Integer credito;

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public String getDescricao() 
	{
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) 
	{
		if (descricao != null)
			this.descricao = descricao.toUpperCase().trim();
		else
			this.descricao = null;
	}
		
	public Integer getIncideCpmf() 
	{
		return incideCpmf;
	}

	public void setIncideCpmf(Integer incideCpmf) 
	{
		this.incideCpmf = incideCpmf;
	}
	
	public Integer getCredito() 
	{
		return credito;
	}

	public void setCredito(Integer credito) 
	{
		this.credito = credito;
	}	
	
	@Override
	public void validate() throws Exception {	 }
}