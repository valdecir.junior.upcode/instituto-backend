package br.com.desenv.nepalign.integracao.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="itempedidovenda")
public class ItemPedidoVendaDTO extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idItemPedidoVenda")
	private Integer id;
		
	@Column(name="idPedidoVenda")
	private Integer pedidoVenda;

	@Column(name="itemSequencial")
	private Integer itemSequencial;
	
	@Column(name="entradaSaida")
	private String entradaSaida;
	
	@Column(name="quantidade")
	private Double quantidade;
	
	@Column(name="precoTabela")
	private Double precoTabela;
	
	@Column(name="precoVenda")
	private Double precoVenda;
	
	@Column(name="custoProduto")
	private Double custoProduto;
	
	@Column(name = "idEstoqueProduto")
	private Integer estoqueProduto;
	
	@Column(name = "idProduto")
	private Integer produto;

	@Column(name = "idStatusItemPedido")
	private Integer statusItemPedido;
	
	@JoinColumn(name = "codigoRastreamento")
	private String codigoRastreamento;

	public ItemPedidoVendaDTO()
	{
		super();
	}
	
	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		if (id != null && id == 0)
			this.id = null;
		else
			this.id = id;
	}
	
	public Integer getPedidoVenda() 
	{
		return pedidoVenda;
	}

	public void setPedidoVenda(Integer pedidoVenda) 
	{
		this.pedidoVenda = pedidoVenda;
	}
	
	public Integer getItemSequencial() 
	{
		return itemSequencial;
	}

	public void setItemSequencial(Integer itemSequencial) 
	{
		if (itemSequencial != null && itemSequencial == 0)
			this.itemSequencial = null;
		else
			this.itemSequencial = itemSequencial;
	}
	
	public String getEntradaSaida() 
	{
		String retorno = null;
		
		if (entradaSaida != null)
			retorno = entradaSaida.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setEntradaSaida(String entradaSaida) 
	{
		if (entradaSaida != null)
			this.entradaSaida = entradaSaida.toUpperCase().trim();
		else
			this.entradaSaida = null;
	}
		
	public Double getQuantidade() 
	{
		return quantidade;
	}

	public void setQuantidade(Double quantidade) 
	{
		if (quantidade != null && quantidade == 0.0)
			this.quantidade = null;
		else
			this.quantidade = quantidade;
	}
	
	public Double getPrecoTabela() 
	{
		return precoTabela;
	}

	public void setPrecoTabela(Double precoTabela) 
	{
		this.precoTabela = precoTabela;
	}
	
	public Double getPrecoVenda() 
	{
		return precoVenda;
	}

	public void setPrecoVenda(Double precoVenda) 
	{
		this.precoVenda = precoVenda;
	}
	
	public Double getCustoProduto() 
	{
		return custoProduto;
	}

	public void setCustoProduto(Double custoProduto) 
	{
		if (custoProduto != null && custoProduto == 0.0)
			this.custoProduto = null;
		else
			this.custoProduto = custoProduto;	
	}
	
	public Integer getEstoqueProduto() 
	{
		return estoqueProduto;
	}

	public void setEstoqueProduto(Integer estoqueProduto) 
	{
		this.estoqueProduto = estoqueProduto;
	}
	
	public Integer getProduto() 
	{
		return produto;
	}

	public void setProduto(Integer produto) 
	{
		this.produto = produto;
	}
	
	public void setCodigoRastreamento(String codigoRastreamento) 
	{
		if (codigoRastreamento != null)
			this.codigoRastreamento = codigoRastreamento.toUpperCase().trim();
		else
			this.codigoRastreamento = null;
	}
	
	public String getCodigoRastreamento()
	{
		String retorno = null;
		
		if(this.codigoRastreamento != null)
			retorno = this.codigoRastreamento.toUpperCase().trim();
		
		return retorno;
	}

	public Integer getStatusItemPedido() 
	{
		return statusItemPedido;
	}

	public void setStatusItemPedido(Integer statusItemPedido) 
	{
		this.statusItemPedido = statusItemPedido;
	}
	
	@Override
	public void validate() throws Exception { }
	
	public ItemPedidoVendaDTO clone() throws CloneNotSupportedException
	{
		return (ItemPedidoVendaDTO) super.clone();
	}
}
