package br.com.desenv.nepalign.integracao.dto;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;
import br.com.desenv.nepalign.model.Acao;

@Entity
@Table(name="usuario")
public class UsuarioDTO extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idUsuario")
	private Integer id;
		
	@Column(name="nome")
	private String nome;
	
	@Column(name="senha")
	private String senha;
	
	@Column(name="login")
	private String login;
	
	@Column(name="usuarioGlobal")
	private String usuarioGlobal;
	
	@Column(name="usuarioMaster")
	private String usuarioMaster;
	
	@Column(name="usuarioCaixa")
	private String usuarioCaixa;
	
	@Column(name = "idEmpresaPadrao")
	private Integer empresaPadrao;
	
	@Column(name = "idOperadora")
	private Integer operadora;
	
	@Column(name="telefone")
	private String telefone;
	
	@Column(name="usuarioDesenv")
	private String usuarioDesenv;
	
	@Column(name = "foto")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] foto;
	
	@Transient
	private List<Acao> acoesLiberadas = new ArrayList<Acao>();

	public UsuarioDTO()
	{
		super();
	}
	
	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		if (id != null && id == 0)
			this.id = null;
		else
			this.id = id;
	}	
	
	public String getNome() 
	{
		String retorno = null;
		
		if (nome != null)
			retorno = nome.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNome(String nome) 
	{
		if (nome != null)
			this.nome = nome.toUpperCase().trim();
		else
			this.nome = null;
	}
		
	public String getSenha() 
	{
		return this.senha;
	}
	
	public void setSenha(String senha) 
	{
		this.senha = senha;
	}
		
	public String getLogin() 
	{
		String retorno = null;
		
		if (login != null)
			retorno = login.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setLogin(String login) 
	{
		if (login != null)
			this.login = login.toUpperCase().trim();
		else
			this.login = null;
	}
		
	public String getUsuarioGlobal() 
	{
		String retorno = null;
		
		if (usuarioGlobal != null)
			retorno = usuarioGlobal.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setUsuarioGlobal(String usuarioGlobal) 
	{
		if (usuarioGlobal != null)
			this.usuarioGlobal = usuarioGlobal.toUpperCase().trim();
		else
			this.usuarioGlobal = null;	
	}
		
	public String getUsuarioMaster() 
	{
		String retorno = null;
		
		if (usuarioMaster != null)
			retorno = usuarioMaster.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setUsuarioMaster(String usuarioMaster) 
	{
		if (usuarioMaster != null)
			this.usuarioMaster = usuarioMaster.toUpperCase().trim();
		else
			this.usuarioMaster = null;		
	}
		
	public String getUsuarioCaixa() 
	{
		String retorno = null;
		
		if (usuarioCaixa != null)
			retorno = usuarioCaixa.toUpperCase().trim();
		
		return retorno;
	}

	public void setUsuarioCaixa(String usuarioCaixa) 
	{
		if (usuarioCaixa != null)
			this.usuarioCaixa = usuarioCaixa.toUpperCase().trim();
		else
			this.usuarioCaixa = null;
	}

	public byte[] getFoto() 
	{
		return foto;
	}

	public void setFoto(byte[] foto) 
	{
		this.foto = foto;
	}
	
	public Integer getOperadora() 
	{
		return operadora;
	}

	public void setOperadora(Integer operadora) 
	{
		this.operadora = operadora;
	}

	public String getTelefone() 
	{
		return telefone;
	}
 
	public void setTelefone(String telefone) 
	{
		this.telefone = telefone;
	}
	
	public String getUsuarioDesenv() 
	{
		return usuarioDesenv;
	}

	public void setUsuarioDesenv(String usuarioDesenv) 
	{
		this.usuarioDesenv = usuarioDesenv;
	}

	public Integer getEmpresaPadrao() 
	{
		return empresaPadrao;
	}

	public void setEmpresaPadrao(Integer empresaPadrao) 
	{
		this.empresaPadrao = empresaPadrao;
	}
	
	public List<Acao> getAcoesLiberadas() 
	{
		return acoesLiberadas;
	}

	public void setAcoesLiberadas(List<Acao> acoesLiberadas) 
	{
		this.acoesLiberadas = acoesLiberadas;
	}

	@Override
	public void validate() throws Exception { }

	@Override
	public UsuarioDTO clone() throws CloneNotSupportedException 
	{
		return (UsuarioDTO) super.clone();
	}
}
