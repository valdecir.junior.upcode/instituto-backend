package br.com.desenv.nepalign.integracao.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="pedidovenda")
public class PedidoVendaDTO extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idPedidoVenda")
	private Integer id;
		
	@Column(name="idTipoMovimentacaoEstoque")
	private Integer tipoMovimentacaoEstoque;
	
	@Column(name="idEmpresaFisica")
	private Integer empresaFisica;

	@Column(name = "idVendedor")
	private Integer vendedor;

	@Column(name = "idCliente")
	private Integer cliente;
	
	@Column(name = "idUsuario")
	private Integer usuario;
	
	@Column(name = "idSituacaoPedidoVenda")
	private Integer situacaoPedidoVenda;
	
	@Column(name = "IdPlanoPagamento")
	private Integer planoPagamento;
	
	@Column(name = "idMetodoPagamento")
	private Integer metodoPagamento;
	
	@Column(name = "idCidadeEntrega")
	private Integer cidadeEntrega;
	
	@Column(name = "idUfEntrega")
	private Integer ufEntrega;

	@Column(name = "idPaisEntrega")
	private Integer paisEntrega;
	
	@Column(name = "idFormaEntrega")
	private Integer formaEntrega;
	
	@Column(name = "idStatusPedido")
	private Integer statusPedido;
	
	@Column(name = "idMeioPagamento")
	private Integer tipoMeioPagamento;
	
	@Column(name = "idTipoLogradouroEnderecoEntrega")
	private Integer tipoLogradouroEnderecoEntrega;
	
	@Column(name="dataVenda")
	private Date dataVenda;
	
	@Column(name="descontoTotal")
	private Double descontoTotal;
	
	@Column(name="numeroControle")
	private String numeroControle;
	
	@Column(name="valorTotalPedido")
	private Double valorTotalPedido;
	
	@Column(name="codigoPedidoEnviadoCliente")
	private String codigoPedidoEnviadoCliente;
	
	@Column(name="valorTotalProdutosPedidos")
	private Double valorTotalProdutosPedidos;
	
	@Column(name="valorTotalCustoFrete")
	private Double valorTotalCustoFrete;
	
	@Column(name="valorTotalDescProdutos")
	private Double valorTotalDescProdutos;
	
	@Column(name="valorTotalDescFrete")
	private Double valorTotalDescFrete;
	
	@Column(name="numParcelas")
	private Integer numParcelas;
	
	@Column(name="valorParcela")
	private Double valorParcela;
	
	@Column(name="totalDescPedido")
	private Double totalDescPedido;
	
	@Column(name="dataExpiracaoPedido")
	private Date dataExpiracaoPedido;
	
	@Column(name="logradouroEnderecoEntrega")
	private String logradouroEnderecoEntrega;
	
	@Column(name="bairroEnderecoEntrega")
	private String bairroEnderecoEntrega;
	
	@Column(name="numEnderecoEntrega")
	private String numEnderecoEntrega;
	
	@Column(name="complementoEnderecoEntrega")
	private String complementoEnderecoEntrega;
	
	@Column(name="cepEnderecoEntrega")
	private String cepEnderecoEntrega;
	
	@Column(name="numTelefone")
	private String numTelefone;
	
	@Column(name="dddTelefone")
	private String dddTelefone;
	
	@Column(name="dataEntregaPedido")
	private Date dataEntregaPedido;
	
	@Column(name="valorEmbalagemPresente")
	private Double valorEmbalagemPresente;
	
	@Column(name="valorJuros")
	private Double valorJuros;
	
	@Column(name="valorCupomDesconto")
	private Double valorCupomDesconto;
	
	@Column(name="numCupomDesconto")
	private Integer numCupomDesconto;
	
	@Column(name="referenciaEnderecoEntrega")
	private String referenciaEnderecoEntrega;
	
	@Column(name="observacao")
	private String observacao;

	@Column(name = "idUsuarioAutorizador")
	private Integer usuarioAutorizador;
	
	@Column(name = "codigoExterno")
	private Integer codigoExterno;
	
	@Column(name = "destinatarioPedido")
	private String destinatarioPedido;

	@Column(name = "codigoPlano")
	private String codigoPlano;

	@Column(name = "idNotaFiscal")
	private Integer notaFiscal;
	
	@Column(name = "email")
	private String email;
	
	@Column(name="cpfNotaFiscal")
	private String cpfNotaFiscal;

	@Column(name="enviouEmail")
	private String enviouEmail;
	
	public PedidoVendaDTO()
	{
		super();
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		if (id != null && id == 0)
			this.id = null;
		else
			this.id = id;
	}
	
	public Integer getTipoMovimentacaoEstoque() 
	{
		return tipoMovimentacaoEstoque;
	}

	public void setTipoMovimentacaoEstoque(Integer tipoMovimentacaoEstoque) 
	{
		this.tipoMovimentacaoEstoque = tipoMovimentacaoEstoque;
	}
	
	public Integer getEmpresaFisica() 
	{
		return empresaFisica;
	}

	public void setEmpresaFisica(Integer empresaFisica) 
	{
		this.empresaFisica = empresaFisica;
	}
	
	public Integer getVendedor() 
	{
		return vendedor;
	}

	public void setVendedor(Integer vendedor) 
	{
		this.vendedor = vendedor;
	}
	
	public Integer getCliente() 
	{
		return cliente;
	}

	public void setCliente(Integer cliente) 
	{
		this.cliente = cliente;
	}
	
	public Integer getUsuario() 
	{
		return usuario;
	}

	public void setUsuario(Integer usuario) 
	{
		this.usuario = usuario;
	}
	
	public Integer getSituacaoPedidoVenda() 
	{
		return situacaoPedidoVenda;
	}

	public void setSituacaoPedidoVenda(Integer situacaoPedidoVenda) 
	{
		this.situacaoPedidoVenda = situacaoPedidoVenda;
	}
	
	public Integer getPlanoPagamento() 
	{
		return planoPagamento;
	}

	public void setPlanoPagamento(Integer planoPagamento) 
	{
		this.planoPagamento = planoPagamento;
	}
	
	public Integer getMetodoPagamento() 
	{
		return metodoPagamento;
	}

	public void setMetodoPagamento(Integer metodoPagamento) 
	{
		this.metodoPagamento = metodoPagamento;
	}
	
	public Integer getCidadeEntrega() 
	{
		return cidadeEntrega;
	}

	public void setCidadeEntrega(Integer cidadeEntrega) 
	{
		this.cidadeEntrega = cidadeEntrega;
	}

	public Integer getUfEntrega() 
	{
		return ufEntrega;
	}

	public void setUfEntrega(Integer ufEntrega) 
	{
		this.ufEntrega = ufEntrega;
	}
	
	public Integer getPaisEntrega() 
	{
		return paisEntrega;
	}

	public void setPaisEntrega(Integer paisEntrega) 
	{
		this.paisEntrega = paisEntrega;
	}
	
	public Integer getFormaEntrega() 
	{
		return formaEntrega;
	}

	public void setFormaEntrega(Integer formaEntrega) 
	{
		this.formaEntrega = formaEntrega;
	}
	
	public Integer getStatusPedido() 
	{
		return statusPedido;
	}

	public void setStatusPedido(Integer statusPedido) 
	{
		this.statusPedido = statusPedido;
	}

	public Integer getTipoMeioPagamento() 
	{
		return tipoMeioPagamento;
	}

	public void setTipoMeioPagamento(Integer tipoMeioPagamento) 
	{
		this.tipoMeioPagamento = tipoMeioPagamento;
	}
	
	public Integer getTipoLogradouroEnderecoEntrega() 
	{
		return tipoLogradouroEnderecoEntrega;
	}

	public void setTipoLogradouroEnderecoEntrega(Integer tipoLogradouroEnderecoEntrega) 
	{
		this.tipoLogradouroEnderecoEntrega = tipoLogradouroEnderecoEntrega;
	}
	
	public Date getDataVenda() 
	{
		return dataVenda;
	}

	public void setDataVenda(Date dataVenda) 
	{
		this.dataVenda = dataVenda;
	}
	
	public Double getDescontoTotal() 
	{
		return descontoTotal;
	}

	public void setDescontoTotal(Double descontoTotal) 
	{
		if (descontoTotal != null && descontoTotal == 0.0)
			this.descontoTotal = null;
		else
			this.descontoTotal = descontoTotal;
	}
	
	public String getNumeroControle() 
	{
		String retorno = null;
		
		if (numeroControle != null)
			retorno = numeroControle.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNumeroControle(String numeroControle) 
	{
		if (numeroControle != null)
			this.numeroControle = numeroControle.toUpperCase().trim();
		else
			this.numeroControle = null;
	}
		
	public Double getValorTotalPedido() 
	{
		return valorTotalPedido;
	}

	public void setValorTotalPedido(Double valorTotalPedido) 
	{
		if (valorTotalPedido != null && Double.isNaN(valorTotalPedido))
			this.valorTotalPedido = null;
		else
			this.valorTotalPedido = valorTotalPedido;
	}	
	
	public String getCodigoPedidoEnviadoCliente() 
	{
		String retorno = null;
		
		if (codigoPedidoEnviadoCliente != null)
			retorno = codigoPedidoEnviadoCliente.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setCodigoPedidoEnviadoCliente(String codigoPedidoEnviadoCliente) 
	{
		if (codigoPedidoEnviadoCliente != null)
			this.codigoPedidoEnviadoCliente = codigoPedidoEnviadoCliente.toUpperCase().trim();
		else
			this.codigoPedidoEnviadoCliente = null;
	}
		
	public Double getValorTotalProdutosPedidos() 
	{
		return valorTotalProdutosPedidos;
	}

	public void setValorTotalProdutosPedidos(Double valorTotalProdutosPedidos) 
	{
		if (valorTotalProdutosPedidos != null && valorTotalProdutosPedidos == 0.0)
			this.valorTotalProdutosPedidos = null;
		else
			this.valorTotalProdutosPedidos = valorTotalProdutosPedidos;
	}
	
	public Double getValorTotalCustoFrete() 
	{
		return valorTotalCustoFrete;
	}

	public void setValorTotalCustoFrete(Double valorTotalCustoFrete) 
	{
		if (valorTotalCustoFrete != null && valorTotalCustoFrete == 0.0)
			this.valorTotalCustoFrete = null;
		else
			this.valorTotalCustoFrete = valorTotalCustoFrete;
	}
	
	public Double getValorTotalDescProdutos() 
	{
		return valorTotalDescProdutos;
	}

	public void setValorTotalDescProdutos(Double valorTotalDescProdutos) 
	{
		if (valorTotalDescProdutos != null && valorTotalDescProdutos == 0.0)
			this.valorTotalDescProdutos = null;
		else
			this.valorTotalDescProdutos = valorTotalDescProdutos;
	}
	
	public Double getValorTotalDescFrete() 
	{
		return valorTotalDescFrete;
	}

	public void setValorTotalDescFrete(Double valorTotalDescFrete) 
	{
		if (valorTotalDescFrete != null && valorTotalDescFrete == 0.0)
			this.valorTotalDescFrete = null;
		else
			this.valorTotalDescFrete = valorTotalDescFrete;
	}
	
	public Integer getNumParcelas() 
	{
		return numParcelas;
	}

	public void setNumParcelas(Integer numParcelas) 
	{
		if (numParcelas != null && numParcelas == 0)
			this.numParcelas = null;
		else
			this.numParcelas = numParcelas;
	}
	
	public Double getValorParcela() 
	{
		return valorParcela;
	}

	public void setValorParcela(Double valorParcela) 
	{
		if (valorParcela != null && valorParcela == 0.0)
			this.valorParcela = null;
		else
			this.valorParcela = valorParcela;
	}
	
	public Double getTotalDescPedido() 
	{
		return totalDescPedido;
	}

	public void setTotalDescPedido(Double totalDescPedido) 
	{
		if (totalDescPedido != null && totalDescPedido == 0.0)
			this.totalDescPedido = null;
		else
			this.totalDescPedido = totalDescPedido;
	}
	
	public Date getDataExpiracaoPedido() 
	{
		return dataExpiracaoPedido;
	}

	public void setDataExpiracaoPedido(Date dataExpiracaoPedido) 
	{
		this.dataExpiracaoPedido = dataExpiracaoPedido;
	}
	
	public String getLogradouroEnderecoEntrega() 
	{
		String retorno = null;
		
		if (logradouroEnderecoEntrega != null)
			retorno = logradouroEnderecoEntrega.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setLogradouroEnderecoEntrega(String logradouroEnderecoEntrega) 
	{
		if (logradouroEnderecoEntrega != null)
			this.logradouroEnderecoEntrega = logradouroEnderecoEntrega.toUpperCase().trim();
		else
			this.logradouroEnderecoEntrega = null;
	}
		
	public String getBairroEnderecoEntrega() 
	{
		String retorno = null;
		
		if (bairroEnderecoEntrega != null)
			retorno = bairroEnderecoEntrega.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setBairroEnderecoEntrega(String bairroEnderecoEntrega) 
	{
		if (bairroEnderecoEntrega != null)
			this.bairroEnderecoEntrega = bairroEnderecoEntrega.toUpperCase().trim();
		else
			this.bairroEnderecoEntrega = null;
	}
		
	public String getNumEnderecoEntrega() 
	{
		String retorno = null;
		
		if (numEnderecoEntrega != null)
			retorno = numEnderecoEntrega.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNumEnderecoEntrega(String numEnderecoEntrega) 
	{
		if (numEnderecoEntrega != null)
			this.numEnderecoEntrega = numEnderecoEntrega.toUpperCase().trim();
		else
			this.numEnderecoEntrega = null;
	}
		
	public String getComplementoEnderecoEntrega() 
	{
		String retorno = null;
		
		if (complementoEnderecoEntrega != null)
			retorno = complementoEnderecoEntrega.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setComplementoEnderecoEntrega(String complementoEnderecoEntrega) 
	{
		if (complementoEnderecoEntrega != null)
			this.complementoEnderecoEntrega = complementoEnderecoEntrega.toUpperCase().trim();
		else
			this.complementoEnderecoEntrega = null;
	}
		
	public String getCepEnderecoEntrega() 
	{
		String retorno = null;
		
		if (cepEnderecoEntrega != null)
			retorno = cepEnderecoEntrega.toUpperCase().trim().replaceAll("[^0-9]", "");
		
		return retorno;
	}

	public void setCepEnderecoEntrega(String cepEnderecoEntrega) 
	{
		if (cepEnderecoEntrega != null)
			this.cepEnderecoEntrega = cepEnderecoEntrega.toUpperCase().trim().replaceAll("[^0-9]", "");
		else
			this.cepEnderecoEntrega = null;
	}
		
	public String getNumTelefone() 
	{
		String retorno = null;
		
		if (numTelefone != null)
			retorno = numTelefone.toUpperCase().trim().replaceAll("[^0-9]", "");
		
		return retorno;
	}
	
	public void setNumTelefone(String numTelefone) 
	{
		if (numTelefone != null)
			this.numTelefone = numTelefone.toUpperCase().trim().replaceAll("[^0-9]", "");
		else
			this.numTelefone = null;
	}
		
	public String getDddTelefone() 
	{
		String retorno = null;
		
		if (dddTelefone != null)
			retorno = dddTelefone.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setDddTelefone(String dddTelefone) 
	{
		if (dddTelefone != null)
			this.dddTelefone = dddTelefone.toUpperCase().trim();
		else
			this.dddTelefone = null;
	}
		
	public Date getDataEntregaPedido() 
	{
		return dataEntregaPedido;
	}

	public void setDataEntregaPedido(Date dataEntregaPedido) 
	{
		this.dataEntregaPedido = dataEntregaPedido;
	}
	
	public Double getValorEmbalagemPresente() 
	{
		return valorEmbalagemPresente;
	}

	public void setValorEmbalagemPresente(Double valorEmbalagemPresente) 
	{
		if (valorEmbalagemPresente != null && valorEmbalagemPresente == 0.0)
			this.valorEmbalagemPresente = null;
		else
			this.valorEmbalagemPresente = valorEmbalagemPresente;
	}
	
	public Double getValorJuros() 
	{
		return valorJuros;
	}

	public void setValorJuros(Double valorJuros) 
	{
		if (valorJuros != null && valorJuros == 0.0)
			this.valorJuros = null;
		else
			this.valorJuros = valorJuros;
	}
	
	public Double getValorCupomDesconto() 
	{
		return valorCupomDesconto;
	}

	public void setValorCupomDesconto(Double valorCupomDesconto) 
	{
		if (valorCupomDesconto != null && valorCupomDesconto == 0.0)
			this.valorCupomDesconto = null;
		else
			this.valorCupomDesconto = valorCupomDesconto;
	}
	
	public Integer getNumCupomDesconto() 
	{
		return numCupomDesconto;
	}

	public void setNumCupomDesconto(Integer numCupomDesconto) 
	{
		if (numCupomDesconto != null && numCupomDesconto == 0)
			this.numCupomDesconto = null;
		else
			this.numCupomDesconto = numCupomDesconto;
	}
	
	public String getReferenciaEnderecoEntrega() 
	{	
		String retorno = null;
		
		if (referenciaEnderecoEntrega != null)
			retorno = referenciaEnderecoEntrega.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setReferenciaEnderecoEntrega(String referenciaEnderecoEntrega) 
	{
		if (referenciaEnderecoEntrega != null)
			this.referenciaEnderecoEntrega = referenciaEnderecoEntrega.toUpperCase().trim();
		else
			this.referenciaEnderecoEntrega = null;
	}
		
	public String getObservacao() 
	{
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setObservacao(String observacao) 
	{
		if (observacao != null)
			this.observacao = observacao.toUpperCase().trim();
		else
			this.observacao = null;
	}
		
	public Integer getUsuarioAutorizador() 
	{
		return usuarioAutorizador;
	}

	public void setUsuarioAutorizador(Integer usuarioAutorizador) 
	{
		this.usuarioAutorizador = usuarioAutorizador;
	}
	
	public Integer getCodigoExterno()
	{
		return this.codigoExterno;
	}
	
	public void setCodigoExterno(Integer codigoExterno)
	{
		if (codigoExterno != null && codigoExterno == 0)
			this.codigoExterno = null;
		else
			this.codigoExterno = codigoExterno;
	}
	
	public String getDestinatarioPedido() 
	{
		return destinatarioPedido;
	}

	public void setDestinatarioPedido(String destinatarioPedido) 
	{
		this.destinatarioPedido = destinatarioPedido;
	}

	public String getCodigoPlano() 
	{
		return codigoPlano;
	}

	public void setCodigoPlano(String codigoPlano) 
	{
		this.codigoPlano = codigoPlano; 
	}

	public Integer getNotaFiscal() {
		return notaFiscal;
	}

	public void setNotaFiscal(Integer notaFiscal) {
		this.notaFiscal = notaFiscal;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCpfNotaFiscal() {
		return cpfNotaFiscal;
	}

	public void setCpfNotaFiscal(String cpfNotaFiscal) {
		this.cpfNotaFiscal = cpfNotaFiscal;
	}

	public String getEnviouEmail() {
		return enviouEmail;
	}

	public void setEnviouEmail(String enviouEmail) {
		this.enviouEmail = enviouEmail;
	}

	@Override
	public void validate() throws Exception {}
	
	@Override
	public PedidoVendaDTO clone() throws CloneNotSupportedException 
	{
		return (PedidoVendaDTO) super.clone();
	}
}
