package br.com.desenv.nepalign.integracao.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="tipoMovimentoCaixaDia")
public class TipoMovimentoCaixaDiaDTO  extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idTipoMovimentoCaixaDia")
	private Integer id;
	
	@Column(name="idEmpresaFisica")
	private Integer empresaFisica;
	
	@Column(name="idTipoConta")
	private Integer tipoConta;

	@Column(name="descricao")
	private String descricao;
	
	@Column(name="origem")
	private Integer origem;
	
	@Column(name="geraDespesa")
	private String geraDespesa;

	public TipoMovimentoCaixaDiaDTO()
	{
		super();
	}
	
	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}	
	
	public String getDescricao()
	{
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		
		return retorno;
	}
	
	public Integer getEmpresaFisica() 
	{
		return empresaFisica;
	}
	
	public void setEmpresaFisica(Integer empresaFisica) 
	{
		this.empresaFisica = empresaFisica;
	}
	
	public Integer getTipoConta() 
	{
		return tipoConta;
	}
	
	public void setTipoConta(Integer tipoConta) 
	{
		this.tipoConta = tipoConta;
	}
	
	public void setDescricao(String descricao)
	{
		if (descricao != null)
			this.descricao = descricao.toUpperCase().trim();
		else
			this.descricao = null;
	}

	public Integer getOrigem() 
	{
		return origem;
	}

	public void setOrigem(Integer origem)
	{
		this.origem = origem;
	}	

	public String getGeraDespesa() 
	{
		return geraDespesa;
	}

	public void setGeraDespesa(String geraDespesa) 
	{
		this.geraDespesa = geraDespesa;
	}

	@Override
	public void validate() throws Exception { }
}