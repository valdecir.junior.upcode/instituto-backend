package br.com.desenv.nepalign.integracao.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="itemnotafiscal")
public class ItemNotaFiscalDTO extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idItemNotaFiscal")
	private Integer id;
	@Column(name = "idNotaFiscal")
	private Integer notaFiscal;
	@Column(name = "idProduto")
	private Integer produto;
	@Column(name="quantidade")
	private Double quantidade;
	@Column(name = "idUnidadeMedida")
	private Integer unidadeMedida;
	@Column(name="valorUnitario")
	private Double valorUnitario;
	
	@Column(name="valorTotal")
	private Double valorTotal;
	
	@Column(name="aliquotaICMS")
	private Double aliquotaICMS;
	
	@Column(name="aliquotaIPI")
	private Double aliquotaIPI;
	
	@Column(name="valorIPI")
	private Double valorIPI;
	
	@Column(name="lote")
	private String lote;
	
	@Column(name="validade")
	private Date validade;
	

	@Column(name = "idItemMovimentacaoEstoque")
	private Integer itemMovimentacaoEstoque;
	@Column(name="percPis")
	private Double percPis;
	
	@Column(name="percCofins")
	private Double percCofins;
	
	@Column(name="percCsll")
	private Double percCsll;
	
	@Column(name="percIrrf")
	private Double percIrrf;
	
	@Column(name="percReducaoBaseCalc")
	private Double percReducaoBaseCalc;
	
	@Column(name="margemLucro")
	private Double margemLucro;
	

	@Column(name = "idCodigoSituacaoTributaria")
	private Integer codigoSituacaoTributaria;

	@Column(name = "idOrigemMercadoria")
	private Integer origemMercadoria;
	@Column(name="valorFrete")
	private Double valorFrete;
	
	@Column(name="valorSeguro")
	private Double valorSeguro;
	
	@Column(name="dataFabricacao")
	private Date dataFabricacao;
	
	@Column(name="valorBaseCalcICMS")
	private Double valorBaseCalcICMS;
	
	@Column(name="valorBaseCalcICMSST")
	private Double valorBaseCalcICMSST;
	
	@Column(name="valorICMS")
	private Double valorICMS;
	
	@Column(name="valorICMSST")
	private Double valorICMSST;
	
	@Column(name="PMC")
	private Double pMC;
	
	@Column(name="percReducaoBaseCalcST")
	private Double percReducaoBaseCalcST;
	
	@Column(name="codigoCFOP")
	private String codigoCFOP;
	
	@Column(name="sequenciaItem")
	private Integer sequenciaItem;
	
	@Column(name="cstPis")
	private Integer cstPis;
	
	@Column(name="cstCofins")
	private Integer cstCofins;
	
	@Column(name="valorDesconto")
	private Double valorDesconto;
	
	@Column(name = "idCfop")
	private Integer cfop;
	
	@Column(name = "idCodigoSituacaoTributariaICMS")
	private Integer codigoSituacaoTributariaICMS;
	
	@Column(name = "idCodigoSituacaoTributariaCOFINS")
	private Integer codigoSituacaoTributariaCOFINS;

	@Column(name = "idCodigoSituacaoTributariaPIS")
	private Integer codigoSituacaoTributariaPIS;
	
	@Column(name="aliquotaCOFINS")
	private Double aliquotaCOFINS;
	
	@Column(name="fabricacao")
	private Date fabricacao;
	
	@Column(name="aliquotaPIS")
	private Double aliquotaPIS;
	

	public ItemNotaFiscalDTO()
	{
		super();
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Integer getNotaFiscal() {
		return notaFiscal;
	}

	public void setNotaFiscal(Integer notaFiscal) {
		this.notaFiscal = notaFiscal;
	}
	
	public Integer getProduto() {
		return produto;
	}

	public void setProduto(Integer produto) {
		this.produto = produto;
	}
	
	public Double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Double quantidade) {
	
		if (quantidade != null && Double.isNaN(quantidade))
		{
			this.quantidade = null;
		}
		else
		{
			this.quantidade = quantidade;
		}
		
	}	
	public Integer getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(Integer unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	
	public Double getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(Double valorUnitario) {
	
		if (valorUnitario != null && Double.isNaN(valorUnitario))
		{
			this.valorUnitario = null;
		}
		else
		{
			this.valorUnitario = valorUnitario;
		}
		
	}	
	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
	
		if (valorTotal != null && Double.isNaN(valorTotal))
		{
			this.valorTotal = null;
		}
		else
		{
			this.valorTotal = valorTotal;
		}
		
	}	
	public Double getAliquotaICMS() {
		return aliquotaICMS;
	}

	public void setAliquotaICMS(Double aliquotaICMS) {
	
		if (aliquotaICMS != null && Double.isNaN(aliquotaICMS))
		{
			this.aliquotaICMS = null;
		}
		else
		{
			this.aliquotaICMS = aliquotaICMS;
		}
		
	}	
	public Double getAliquotaIPI() {
		return aliquotaIPI;
	}

	public void setAliquotaIPI(Double aliquotaIPI) {
	
		if (aliquotaIPI != null && Double.isNaN(aliquotaIPI))
		{
			this.aliquotaIPI = null;
		}
		else
		{
			this.aliquotaIPI = aliquotaIPI;
		}
		
	}	
	
	public Double getValorIPI() {
		return valorIPI;
	}


	public void setValorIPI(Double valorIPI) {
		if (valorIPI != null && Double.isNaN(valorIPI))
		{
			this.valorIPI = null;
		}
		else
		{
			this.valorIPI = valorIPI;
		}
	}


	public String getLote() {
		String retorno = null;
		
		if (lote != null)
			retorno = lote.toUpperCase().trim();
		return retorno;
	}
	
	public void setLote(String lote) {
		if (lote != null)
		{
			this.lote = lote.toUpperCase().trim();
		}
		else
			this.lote = null;
			
		
	}
		
	public Date getValidade() {
		return validade;
	}

	public void setValidade(Date validade) {
		this.validade = validade;
	}
	public Integer getItemMovimentacaoEstoque() {
		return itemMovimentacaoEstoque;
	}

	public void setItemMovimentacaoEstoque(Integer itemMovimentacaoEstoque) {
		this.itemMovimentacaoEstoque = itemMovimentacaoEstoque;
	}
	
	public Double getPercPis() {
		return percPis;
	}

	public void setPercPis(Double percPis) {
	
		if (percPis != null && Double.isNaN(percPis))
		{
			this.percPis = null;
		}
		else
		{
			this.percPis = percPis;
		}
		
	}	
	public Double getPercCofins() {
		return percCofins;
	}

	public void setPercCofins(Double percCofins) {
	
		if (percCofins != null && Double.isNaN(percCofins))
		{
			this.percCofins = null;
		}
		else
		{
			this.percCofins = percCofins;
		}
		
	}	
	public Double getPercCsll() {
		return percCsll;
	}

	public void setPercCsll(Double percCsll) {
	
		if (percCsll != null && Double.isNaN(percCsll))
		{
			this.percCsll = null;
		}
		else
		{
			this.percCsll = percCsll;
		}
		
	}	
	public Double getPercIrrf() {
		return percIrrf;
	}

	public void setPercIrrf(Double percIrrf) {
	
		if (percIrrf != null && Double.isNaN(percIrrf))
		{
			this.percIrrf = null;
		}
		else
		{
			this.percIrrf = percIrrf;
		}
		
	}	
	public Double getPercReducaoBaseCalc() {
		return percReducaoBaseCalc;
	}

	public void setPercReducaoBaseCalc(Double percReducaoBaseCalc) {
	
		if (percReducaoBaseCalc != null && Double.isNaN(percReducaoBaseCalc))
		{
			this.percReducaoBaseCalc = null;
		}
		else
		{
			this.percReducaoBaseCalc = percReducaoBaseCalc;
		}
		
	}	
	public Double getMargemLucro() {
		return margemLucro;
	}

	public void setMargemLucro(Double margemLucro) {
	
		if (margemLucro != null && Double.isNaN(margemLucro))
		{
			this.margemLucro = null;
		}
		else
		{
			this.margemLucro = margemLucro;
		}
		
	}	
	public Integer getCodigoSituacaoTributaria() {
		return codigoSituacaoTributaria;
	}

	public void setCodigoSituacaoTributaria(Integer codigoSituacaoTributaria) {
		this.codigoSituacaoTributaria = codigoSituacaoTributaria;
	}
	
	public Integer getOrigemMercadoria() {
		return origemMercadoria;
	}

	public void setOrigemMercadoria(Integer origemMercadoria) {
		this.origemMercadoria = origemMercadoria;
	}
	
	public Double getValorFrete() {
		return valorFrete;
	}

	public void setValorFrete(Double valorFrete) {
	
		if (valorFrete != null && Double.isNaN(valorFrete))
		{
			this.valorFrete = null;
		}
		else
		{
			this.valorFrete = valorFrete;
		}
		
	}	
	public Double getValorSeguro() {
		return valorSeguro;
	}

	public void setValorSeguro(Double valorSeguro) {
	
		if (valorSeguro != null && Double.isNaN(valorSeguro))
		{
			this.valorSeguro = null;
		}
		else
		{
			this.valorSeguro = valorSeguro;
		}
		
	}	
	public Date getDataFabricacao() {
		return dataFabricacao;
	}

	public void setDataFabricacao(Date dataFabricacao) {
		this.dataFabricacao = dataFabricacao;
	}
	public Double getValorBaseCalcICMS() {
		return valorBaseCalcICMS;
	}

	public void setValorBaseCalcICMS(Double valorBaseCalcICMS) {
	
		if (valorBaseCalcICMS != null && Double.isNaN(valorBaseCalcICMS))
		{
			this.valorBaseCalcICMS = null;
		}
		else
		{
			this.valorBaseCalcICMS = valorBaseCalcICMS;
		}
		
	}	
	public Double getValorBaseCalcICMSST() {
		return valorBaseCalcICMSST;
	}

	public void setValorBaseCalcICMSST(Double valorBaseCalcICMSST) {
	
		if (valorBaseCalcICMSST != null && Double.isNaN(valorBaseCalcICMSST))
		{
			this.valorBaseCalcICMSST = null;
		}
		else
		{
			this.valorBaseCalcICMSST = valorBaseCalcICMSST;
		}
		
	}	
	public Double getValorICMS() {
		return valorICMS;
	}

	public void setValorICMS(Double valorICMS) {
	
		if (valorICMS != null && Double.isNaN(valorICMS))
		{
			this.valorICMS = null;
		}
		else
		{
			this.valorICMS = valorICMS;
		}
		
	}	
	public Double getValorICMSST() {
		return valorICMSST;
	}

	public void setValorICMSST(Double valorICMSST) {
	
		if (valorICMSST != null && Double.isNaN(valorICMSST))
		{
			this.valorICMSST = null;
		}
		else
		{
			this.valorICMSST = valorICMSST;
		}
		
	}	
	public Double getPMC() {
		return pMC;
	}

	public void setPMC(Double pMC) {
	
		if (pMC != null && Double.isNaN(pMC))
		{
			this.pMC = null;
		}
		else
		{
			this.pMC = pMC;
		}
		
	}	
	public Double getPercReducaoBaseCalcST() {
		return percReducaoBaseCalcST;
	}

	public void setPercReducaoBaseCalcST(Double percReducaoBaseCalcST) {
	
		if (percReducaoBaseCalcST != null && Double.isNaN(percReducaoBaseCalcST))
		{
			this.percReducaoBaseCalcST = null;
		}
		else
		{
			this.percReducaoBaseCalcST = percReducaoBaseCalcST;
		}
		
	}	
	public String getCodigoCFOP() {
		String retorno = null;
		
		if (codigoCFOP != null)
			retorno = codigoCFOP.toUpperCase().trim();
		return retorno;
	}
	
	public void setCodigoCFOP(String codigoCFOP) {
		if (codigoCFOP != null)
		{
			this.codigoCFOP = codigoCFOP.toUpperCase().trim();
		}
		else
			this.codigoCFOP = null;
			
		
	}
		
	public Integer getSequenciaItem() {
		return sequenciaItem;
	}

	public void setSequenciaItem(Integer sequenciaItem) {
		this.sequenciaItem = sequenciaItem;
	}	
	public Integer getCstPis() {
		return cstPis;
	}

	public void setCstPis(Integer cstPis) {
		this.cstPis = cstPis;
	}	
	public Integer getCstCofins() {
		return cstCofins;
	}

	public void setCstCofins(Integer cstCofins) {
		this.cstCofins = cstCofins;
	}	
	public Double getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(Double valorDesconto) {
	
		if (valorDesconto != null && Double.isNaN(valorDesconto))
		{
			this.valorDesconto = null;
		}
		else
		{
			this.valorDesconto = valorDesconto;
		}
		
	}	
	public Integer getCfop() {
		return cfop;
	}

	public void setCfop(Integer cfop) {
		this.cfop = cfop;
	}
	
	public Integer getCodigoSituacaoTributariaICMS() {
		return codigoSituacaoTributariaICMS;
	}

	public void setCodigoSituacaoTributariaICMS(Integer codigoSituacaoTributariaICMS) {
		this.codigoSituacaoTributariaICMS = codigoSituacaoTributariaICMS;
	}
	
	public Integer getCodigoSituacaoTributariaCOFINS() {
		return codigoSituacaoTributariaCOFINS;
	}

	public void setCodigoSituacaoTributariaCOFINS(Integer codigoSituacaoTributariaCOFINS) {
		this.codigoSituacaoTributariaCOFINS = codigoSituacaoTributariaCOFINS;
	}
	
	public Integer getCodigoSituacaoTributariaPIS() {
		return codigoSituacaoTributariaPIS;
	}

	public void setCodigoSituacaoTributariaPIS(Integer CodigoSituacaoTributariaPIS) {
		this.codigoSituacaoTributariaPIS = CodigoSituacaoTributariaPIS;
	}
	
	public Double getAliquotaCOFINS() {
		return aliquotaCOFINS;
	}

	public void setAliquotaCOFINS(Double aliquotaCOFINS) {
	
		if (aliquotaCOFINS != null && Double.isNaN(aliquotaCOFINS))
		{
			this.aliquotaCOFINS = null;
		}
		else
		{
			this.aliquotaCOFINS = aliquotaCOFINS;
		}
		
	}	
	public Date getFabricacao() {
		return fabricacao;
	}

	public void setFabricacao(Date fabricacao) {
		this.fabricacao = fabricacao;
	}
	public Double getAliquotaPIS() {
		return aliquotaPIS;
	}

	public void setAliquotaPIS(Double aliquotaPIS) {
	
		if (aliquotaPIS != null && Double.isNaN(aliquotaPIS))
		{
			this.aliquotaPIS = null;
		}
		else
		{
			this.aliquotaPIS = aliquotaPIS;
		}
		
	}	
	public Double getpMC() {
		return pMC;
	}


	public void setpMC(Double pMC) {
		if(pMC != null && Double.isNaN(pMC))
		{
			this.pMC = null;	
		}
		else
		{
			this.pMC = pMC;
		}
		
	}


	@Override
	public void validate() throws Exception
	{	
	}
}