package br.com.desenv.nepalign.integracao.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DataPackage 
{	
	private Integer idEmpresaFisica;
	private Date dataInicial;
	private Date dataFinal;
	
	private List<UsuarioDTO> listaUsuario;
	private List<VendedorDTO> listaVendedor;
	private List<PedidoVendaDTO> listaPedidoVenda;
	private List<ItemPedidoVendaDTO> listaItemPedidoVenda;
	private List<DuplicataDTO> listaDuplicata;
	private List<DuplicataParcelaDTO> listaDuplicataParcela;
	private List<DuplicataAcordoDTO> listaDuplicataAcordo;
	private List<ClienteDTO> listaCliente;
	private List<ConjugeDTO> listaConjuge;
	private List<AvalistaDTO> listaAvalista;
	private List<ImagemUsuarioDTO> listaImagemUsuario;
	private List<HistoricoCobrancaDTO> listaHistoricoCobranca;
	private List<EmailClienteDTO> listaEmailCliente;
	private List<CaixaDiaDTO> listaCaixaDia;
	private List<ValorFechamentoCaixaDiaDTO> listaValorFechamentoCaixaDia;
	private List<PagamentoPedidoVendaDTO> listaPagamentoPedidoVenda;
	private List<ChequeRecebidoDTO> listaChequeRecebido;
	private List<PagamentoCartaoDTO> listaPagamentoCartao;
	private List<MovimentoCaixaDiaDTO> listaMovimentoCaixaDia;
	private List<MovimentoParcelaDuplicataDTO> listaMovimentoParcelaDuplicata;
	
	private List<NotaFiscalDTO> listaNotaFiscal;
	private List<ItemNotaFiscalDTO> listaItemNotaFiscal;
	
	private List<LogOperacaoDTO> listaLogOperacao;
	private List<LogDuplicataDTO> listaLogDuplicata;
	
	private List<TipoContaDTO> listaTipoConta;
	private List<ContaGerencialDTO> listaContaGerencial;
	private List<TipoContaContaGerDTO> listaTipoContaContaGer;
	private List<TipoMovimentoCaixaDiaDTO> listaTipoMovimentoCaixaDia;
	
	private boolean packageSealed = false;
	
	public DataPackage()
	{
		listaUsuario = new ArrayList<UsuarioDTO>();
		listaVendedor = new ArrayList<VendedorDTO>();
		listaPedidoVenda = new ArrayList<PedidoVendaDTO>();
		listaItemPedidoVenda = new ArrayList<ItemPedidoVendaDTO>();
		listaDuplicata = new ArrayList<DuplicataDTO>();
		listaDuplicataParcela = new ArrayList<DuplicataParcelaDTO>();
		listaDuplicataAcordo = new ArrayList<DuplicataAcordoDTO>();
		listaCliente = new ArrayList<ClienteDTO>();
		listaConjuge = new ArrayList<ConjugeDTO>();
		listaAvalista = new ArrayList<AvalistaDTO>();
		listaImagemUsuario = new ArrayList<ImagemUsuarioDTO>();
		listaHistoricoCobranca = new ArrayList<HistoricoCobrancaDTO>();
		listaEmailCliente = new ArrayList<EmailClienteDTO>();
		listaCaixaDia = new ArrayList<CaixaDiaDTO>();
		listaValorFechamentoCaixaDia = new ArrayList<ValorFechamentoCaixaDiaDTO>();
		listaPagamentoPedidoVenda = new ArrayList<PagamentoPedidoVendaDTO>();
		listaChequeRecebido = new ArrayList<ChequeRecebidoDTO>();
		listaPagamentoCartao = new ArrayList<PagamentoCartaoDTO>();
		listaMovimentoCaixaDia = new ArrayList<MovimentoCaixaDiaDTO>();
		listaMovimentoParcelaDuplicata = new ArrayList<MovimentoParcelaDuplicataDTO>();
		
		listaLogOperacao = new ArrayList<LogOperacaoDTO>();
		listaLogDuplicata = new ArrayList<LogDuplicataDTO>();
		
		listaNotaFiscal = new ArrayList<NotaFiscalDTO>();
		listaItemNotaFiscal = new ArrayList<ItemNotaFiscalDTO>();
		
		listaTipoConta = new ArrayList<TipoContaDTO>();
		listaContaGerencial = new ArrayList<ContaGerencialDTO>();
		listaTipoContaContaGer = new ArrayList<TipoContaContaGerDTO>();
		listaTipoMovimentoCaixaDia = new ArrayList<TipoMovimentoCaixaDiaDTO>();
	}

	public final Integer getIdEmpresaFisica() 
	{
		return idEmpresaFisica;
	}

	public void setIdEmpresaFisica(final Integer idEmpresaFisica) 
	{
		this.idEmpresaFisica = idEmpresaFisica;
	}

	public final Date getDataInicial() 
	{
		return dataInicial;
	}

	public void setDataInicial(final Date dataInicial) 
	{
		this.dataInicial = dataInicial;
	}

	public final Date getDataFinal() 
	{
		return dataFinal;
	}

	public void setDataFinal(final Date dataFinal) 
	{
		this.dataFinal = dataFinal;
	}
	
	public final boolean isPackageSealed() 
	{
		return packageSealed;
	}
	
	public final boolean getPackageSealed() 
	{
		return packageSealed;
	}

	public void setPackageSealed(final boolean packageSealed) 
	{
		this.packageSealed = packageSealed;
	}

	public final List<UsuarioDTO> getListaUsuario() 
	{
		return listaUsuario;
	}

	public void setListaUsuario(final List<UsuarioDTO> listaUsuario) 
	{
		if(this.listaUsuario.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaUsuario = listaUsuario;
	}

	public final List<VendedorDTO> getListaVendedor() 
	{
		return listaVendedor;
	}

	public void setListaVendedor(final List<VendedorDTO> listaVendedor) 
	{
		if(this.listaVendedor.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaVendedor = listaVendedor;
	}

	public final List<PedidoVendaDTO> getListaPedidoVenda() 
	{
		return listaPedidoVenda;
	}

	public void setListaPedidoVenda(final List<PedidoVendaDTO> listaPedidoVenda) throws IllegalStateException 
	{
		if(this.listaPedidoVenda.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation."); 
		
		this.listaPedidoVenda = listaPedidoVenda;
	}

	public final List<ItemPedidoVendaDTO> getListaItemPedidoVenda() 
	{
		return listaItemPedidoVenda;
	}

	public void setListaItemPedidoVenda(final List<ItemPedidoVendaDTO> listaItemPedidoVenda) throws IllegalStateException 
	{
		if(this.listaItemPedidoVenda.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaItemPedidoVenda = listaItemPedidoVenda;
	}

	public final List<DuplicataDTO> getListaDuplicata() 
	{
		return listaDuplicata;
	}

	public void setListaDuplicata(final List<DuplicataDTO> listaDuplicata) 
	{
		if(this.listaDuplicata.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaDuplicata = listaDuplicata;
	}

	public final List<DuplicataParcelaDTO> getListaDuplicataParcela() 
	{
		return listaDuplicataParcela;
	}

	public void setListaDuplicataParcela(final List<DuplicataParcelaDTO> listaDuplicataParcela) 
	{
		if(this.listaDuplicataParcela.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaDuplicataParcela = listaDuplicataParcela;
	}

	public final List<DuplicataAcordoDTO> getListaDuplicataAcordo() 
	{
		return listaDuplicataAcordo;
	}

	public void setListaDuplicataAcordo(final List<DuplicataAcordoDTO> listaDuplicataAcordo) 
	{
		if(this.listaDuplicataAcordo.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaDuplicataAcordo = listaDuplicataAcordo;
	}

	public final List<ClienteDTO> getListaCliente() 
	{
		return listaCliente;
	}

	public void setListaCliente(final List<ClienteDTO> listaCliente) 
	{
		if(this.listaCliente.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaCliente = listaCliente;
	}

	public final List<ConjugeDTO> getListaConjuge() 
	{
		return listaConjuge;
	}

	public void setListaConjuge(final List<ConjugeDTO> listaConjuge) 
	{
		if(this.listaConjuge.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaConjuge = listaConjuge;
	}

	public final List<AvalistaDTO> getListaAvalista() 
	{
		return listaAvalista;
	}

	public void setListaAvalista(final List<AvalistaDTO> listaAvalista) 
	{
		if(this.listaAvalista.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaAvalista = listaAvalista;
	}

	public final List<ImagemUsuarioDTO> getListaImagemUsuario() 
	{
		return listaImagemUsuario;
	}

	public void setListaImagemUsuario(final List<ImagemUsuarioDTO> listaImagemUsuario) 
	{
		if(this.listaImagemUsuario.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaImagemUsuario = listaImagemUsuario;
	}

	public final List<HistoricoCobrancaDTO> getListaHistoricoCobranca() 
	{
		return listaHistoricoCobranca;
	}

	public void setListaHistoricoCobranca(final List<HistoricoCobrancaDTO> listaHistoricoCobranca) 
	{
		if(this.listaHistoricoCobranca.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaHistoricoCobranca = listaHistoricoCobranca;
	}

	public final List<EmailClienteDTO> getListaEmailCliente() 
	{
		return listaEmailCliente;
	}

	public void setListaEmailCliente(final List<EmailClienteDTO> listaEmailCliente) 
	{
		if(this.listaEmailCliente.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaEmailCliente = listaEmailCliente;
	}

	public final List<CaixaDiaDTO> getListaCaixaDia() 
	{
		return listaCaixaDia;
	}

	public void setListaCaixaDia(final List<CaixaDiaDTO> listaCaixaDia) 
	{
		if(this.listaCaixaDia.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaCaixaDia = listaCaixaDia;
	}

	public final List<ValorFechamentoCaixaDiaDTO> getListaValorFechamentoCaixaDia() 
	{
		return listaValorFechamentoCaixaDia;
	}

	public void setListaValorFechamentoCaixaDia(final List<ValorFechamentoCaixaDiaDTO> listaValorFechamentoCaixaDia) 
	{
		if(this.listaValorFechamentoCaixaDia.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaValorFechamentoCaixaDia = listaValorFechamentoCaixaDia;
	}

	public final List<PagamentoPedidoVendaDTO> getListaPagamentoPedidoVenda() 
	{
		return listaPagamentoPedidoVenda;
	}

	public void setListaPagamentoPedidoVenda(final List<PagamentoPedidoVendaDTO> listaPagamentoPedidoVenda) 
	{
		if(this.listaPagamentoPedidoVenda.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaPagamentoPedidoVenda = listaPagamentoPedidoVenda;
	}

	public final List<ChequeRecebidoDTO> getListaChequeRecebido() 
	{
		return listaChequeRecebido;
	}

	public void setListaChequeRecebido(final List<ChequeRecebidoDTO> listaChequeRecebido) 
	{
		if(this.listaChequeRecebido.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaChequeRecebido = listaChequeRecebido;
	}
	
	public final List<PagamentoCartaoDTO> getListaPagamentoCartao() 
	{
		return listaPagamentoCartao;
	}

	public void setListaPagamentoCartao(final List<PagamentoCartaoDTO> listaPagamentoCartao) 
	{
		if(this.listaPagamentoCartao.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaPagamentoCartao = listaPagamentoCartao;
	}

	public final List<MovimentoCaixaDiaDTO> getListaMovimentoCaixaDia() 
	{
		return listaMovimentoCaixaDia;
	}

	public void setListaMovimentoCaixaDia(final List<MovimentoCaixaDiaDTO> listaMovimentoCaixaDia) 
	{
		if(this.listaMovimentoCaixaDia.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaMovimentoCaixaDia = listaMovimentoCaixaDia;
	}

	public final List<MovimentoParcelaDuplicataDTO> getListaMovimentoParcelaDuplicata() 
	{
		return listaMovimentoParcelaDuplicata;
	}

	public void setListaMovimentoParcelaDuplicata(final List<MovimentoParcelaDuplicataDTO> listaMovimentoParcelaDuplicata) 
	{
		if(this.listaMovimentoParcelaDuplicata.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaMovimentoParcelaDuplicata = listaMovimentoParcelaDuplicata;
	}
	
	public final List<LogOperacaoDTO> getListaLogOperacao() 
	{
		return listaLogOperacao;
	}

	public void setListaLogOperacao(final List<LogOperacaoDTO> listaLogOperacao) 
	{
		if(this.listaLogOperacao.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaLogOperacao = listaLogOperacao;
	}
	
	public final List<LogDuplicataDTO> getListaLogDuplicata() 
	{
		return listaLogDuplicata;
	}

	public void setListaLogDuplicata(final List<LogDuplicataDTO> listaLogDuplicata) 
	{
		if(this.listaLogDuplicata.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaLogDuplicata = listaLogDuplicata;
	}
	
	public final List<ContaGerencialDTO> getListaContaGerencial() 
	{
		return listaContaGerencial;
	}

	public void setListaContaGerencial(final List<ContaGerencialDTO> listaContaGerencial) 
	{
		if(this.listaContaGerencial.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaContaGerencial = listaContaGerencial;
	}

	public final List<TipoContaDTO> getListaTipoConta() 
	{
		return listaTipoConta;
	}

	public void setListaTipoConta(final List<TipoContaDTO> listaTipoConta) 
	{
		if(this.listaTipoConta.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaTipoConta = listaTipoConta;
	}

	public final List<TipoContaContaGerDTO> getListaTipoContaContaGer() 
	{
		return listaTipoContaContaGer;
	}

	public void setListaTipoContaContaGer(final List<TipoContaContaGerDTO> listaTipoContaContaGer) 
	{
		if(this.listaTipoContaContaGer.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaTipoContaContaGer = listaTipoContaContaGer;
	}

	public final List<TipoMovimentoCaixaDiaDTO> getListaTipoMovimentoCaixaDia() 
	{
		return listaTipoMovimentoCaixaDia;
	}

	public void setListaTipoMovimentoCaixaDia(final List<TipoMovimentoCaixaDiaDTO> listaTipoMovimentoCaixaDia) 
	{
		if(this.listaTipoMovimentoCaixaDia.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaTipoMovimentoCaixaDia = listaTipoMovimentoCaixaDia;
	}
	
	public final List<NotaFiscalDTO> getListaNotaFiscal()
	{
		return listaNotaFiscal;
	}
	
	public void setListaNotaFiscal(final List<NotaFiscalDTO> listaNotaFiscal)
	{
		if(this.listaNotaFiscal.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaNotaFiscal = listaNotaFiscal;
	}
	
	public final List<ItemNotaFiscalDTO> getListaItemNotaFiscal()
	{
		return listaItemNotaFiscal;
	}
	
	public void setListaItemNotaFiscal(final List<ItemNotaFiscalDTO> listaItemNotaFiscal)
	{
		if(this.listaItemNotaFiscal.size() > 0x00) throw new IllegalStateException("Package data will be overwritted. Dispose Package before run this operation.");
		
		this.listaItemNotaFiscal = listaItemNotaFiscal;
	}

	public <T> void put(Class<T> typeDef, Object obj) throws IllegalStateException
	{
		if(packageSealed) throw new IllegalStateException("Package is sealed");
		
		if(typeDef == UsuarioDTO.class) listaUsuario.add((UsuarioDTO) obj);
		else if(typeDef == VendedorDTO.class) listaVendedor.add((VendedorDTO) obj);
		else if(typeDef == PedidoVendaDTO.class) listaPedidoVenda.add((PedidoVendaDTO) obj);
		else if(typeDef == ItemPedidoVendaDTO.class) listaItemPedidoVenda.add((ItemPedidoVendaDTO) obj);
		else if(typeDef == DuplicataDTO.class) listaDuplicata.add((DuplicataDTO) obj);
		else if(typeDef == DuplicataParcelaDTO.class) listaDuplicataParcela.add((DuplicataParcelaDTO) obj);
		else if(typeDef == DuplicataAcordoDTO.class) listaDuplicataAcordo.add((DuplicataAcordoDTO) obj);
		else if(typeDef == ClienteDTO.class) listaCliente.add((ClienteDTO) obj);
		else if(typeDef == ConjugeDTO.class) listaConjuge.add((ConjugeDTO) obj);
		else if(typeDef == AvalistaDTO.class) listaAvalista.add((AvalistaDTO) obj);
		else if(typeDef == ImagemUsuarioDTO.class) listaImagemUsuario.add((ImagemUsuarioDTO) obj);
		else if(typeDef == HistoricoCobrancaDTO.class) listaHistoricoCobranca.add((HistoricoCobrancaDTO) obj);
		else if(typeDef == EmailClienteDTO.class) listaEmailCliente.add((EmailClienteDTO) obj);
		else if(typeDef == CaixaDiaDTO.class) listaCaixaDia.add((CaixaDiaDTO) obj);
		else if(typeDef == ValorFechamentoCaixaDiaDTO.class) listaValorFechamentoCaixaDia.add((ValorFechamentoCaixaDiaDTO) obj);
		else if(typeDef == PagamentoPedidoVendaDTO.class) listaPagamentoPedidoVenda.add((PagamentoPedidoVendaDTO) obj);
		else if(typeDef == ChequeRecebidoDTO.class) listaChequeRecebido.add((ChequeRecebidoDTO) obj);
		else if(typeDef == PagamentoCartaoDTO.class) listaPagamentoCartao.add((PagamentoCartaoDTO) obj);
		else if(typeDef == MovimentoCaixaDiaDTO.class) listaMovimentoCaixaDia.add((MovimentoCaixaDiaDTO) obj);
		else if(typeDef == MovimentoParcelaDuplicataDTO.class) listaMovimentoParcelaDuplicata.add((MovimentoParcelaDuplicataDTO) obj);
		else if(typeDef == LogOperacaoDTO.class) listaLogOperacao.add((LogOperacaoDTO) obj);
		else if(typeDef == NotaFiscalDTO.class) listaNotaFiscal.add((NotaFiscalDTO) obj);
		else if(typeDef == ItemNotaFiscalDTO.class) listaItemNotaFiscal.add((ItemNotaFiscalDTO) obj); 
		else if(typeDef == TipoContaDTO.class) listaTipoConta.add((TipoContaDTO) obj);
		else if(typeDef == ContaGerencialDTO.class) listaContaGerencial.add((ContaGerencialDTO) obj);
		else if(typeDef == TipoContaContaGerDTO.class) listaTipoContaContaGer.add((TipoContaContaGerDTO) obj);
		else if(typeDef == TipoMovimentoCaixaDiaDTO.class) listaTipoMovimentoCaixaDia.add((TipoMovimentoCaixaDiaDTO) obj);
	} 
	
	@SuppressWarnings("unchecked")
	public <T> void put(Class<T> typeDef, List<T> list) throws IllegalStateException
	{
		if(packageSealed) throw new IllegalStateException("Package is sealed");
		
		if(typeDef == UsuarioDTO.class) listaUsuario.addAll((List<UsuarioDTO>) list);
		else if(typeDef == VendedorDTO.class) listaVendedor.addAll((List<VendedorDTO>) list);
		else if(typeDef == PedidoVendaDTO.class) listaPedidoVenda.addAll((List<PedidoVendaDTO>) list);
		else if(typeDef == ItemPedidoVendaDTO.class) listaItemPedidoVenda.addAll((List<ItemPedidoVendaDTO>) list);
		else if(typeDef == DuplicataDTO.class) listaDuplicata.addAll((List<DuplicataDTO>) list);
		else if(typeDef == DuplicataParcelaDTO.class) listaDuplicataParcela.addAll((List<DuplicataParcelaDTO>) list);
		else if(typeDef == DuplicataAcordoDTO.class) listaDuplicataAcordo.addAll((List<DuplicataAcordoDTO>) list);
		else if(typeDef == ClienteDTO.class) listaCliente.addAll((List<ClienteDTO>) list);
		else if(typeDef == ConjugeDTO.class) listaConjuge.addAll((List<ConjugeDTO>) list);
		else if(typeDef == AvalistaDTO.class) listaAvalista.addAll((List<AvalistaDTO>) list);
		else if(typeDef == ImagemUsuarioDTO.class) listaImagemUsuario.addAll((List<ImagemUsuarioDTO>) list);
		else if(typeDef == HistoricoCobrancaDTO.class) listaHistoricoCobranca.addAll((List<HistoricoCobrancaDTO>) list);
		else if(typeDef == EmailClienteDTO.class) listaEmailCliente.addAll((List<EmailClienteDTO>) list);
		else if(typeDef == CaixaDiaDTO.class) listaCaixaDia.addAll((List<CaixaDiaDTO>) list);
		else if(typeDef == ValorFechamentoCaixaDiaDTO.class) listaValorFechamentoCaixaDia.addAll((List<ValorFechamentoCaixaDiaDTO>) list);
		else if(typeDef == PagamentoPedidoVendaDTO.class) listaPagamentoPedidoVenda.addAll((List<PagamentoPedidoVendaDTO>) list);
		else if(typeDef == ChequeRecebidoDTO.class) listaChequeRecebido.addAll((List<ChequeRecebidoDTO>) list);
		else if(typeDef == PagamentoCartaoDTO.class) listaPagamentoCartao.addAll((List<PagamentoCartaoDTO>) list);
		else if(typeDef == MovimentoCaixaDiaDTO.class) listaMovimentoCaixaDia.addAll((List<MovimentoCaixaDiaDTO>) list);
		else if(typeDef == MovimentoParcelaDuplicataDTO.class) listaMovimentoParcelaDuplicata.addAll((List<MovimentoParcelaDuplicataDTO>) list);
		else if(typeDef == LogOperacaoDTO.class) listaLogOperacao.addAll((List<LogOperacaoDTO>) list);
		else if(typeDef == LogDuplicataDTO.class) listaLogDuplicata.addAll((List<LogDuplicataDTO>) list);
		else if(typeDef == NotaFiscalDTO.class) listaNotaFiscal.addAll((List<NotaFiscalDTO>) list);
		else if(typeDef == ItemNotaFiscalDTO.class) listaItemNotaFiscal.addAll((List<ItemNotaFiscalDTO>) list);
		else if(typeDef == TipoContaDTO.class) listaTipoConta.addAll((List<TipoContaDTO>) list);
		else if(typeDef == ContaGerencialDTO.class) listaContaGerencial.addAll((List<ContaGerencialDTO>) list);
		else if(typeDef == TipoContaContaGerDTO.class) listaTipoContaContaGer.addAll((List<TipoContaContaGerDTO>) list);
		else if(typeDef == TipoMovimentoCaixaDiaDTO.class) listaTipoMovimentoCaixaDia.addAll((List<TipoMovimentoCaixaDiaDTO>) list);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> List<T> push(Class typeDef)
	{
		if(typeDef == UsuarioDTO.class) return (List<T>) listaUsuario;
		else if(typeDef == VendedorDTO.class) return (List<T>) listaVendedor;
		else if(typeDef == PedidoVendaDTO.class) return (List<T>) listaPedidoVenda;
		else if(typeDef == ItemPedidoVendaDTO.class) return (List<T>) listaItemPedidoVenda;
		else if(typeDef == DuplicataDTO.class) return (List<T>) listaDuplicata;
		else if(typeDef == DuplicataParcelaDTO.class) return (List<T>) listaDuplicataParcela;
		else if(typeDef == DuplicataAcordoDTO.class) return (List<T>) listaDuplicataAcordo;
		else if(typeDef == ClienteDTO.class) return (List<T>) listaCliente;
		else if(typeDef == ConjugeDTO.class) return (List<T>) listaConjuge;
		else if(typeDef == AvalistaDTO.class) return (List<T>) listaAvalista;
		else if(typeDef == ImagemUsuarioDTO.class) return (List<T>) listaImagemUsuario;
		else if(typeDef == HistoricoCobrancaDTO.class) return (List<T>) listaHistoricoCobranca;
		else if(typeDef == EmailClienteDTO.class) return (List<T>) listaEmailCliente;
		else if(typeDef == CaixaDiaDTO.class) return (List<T>) listaCaixaDia;
		else if(typeDef == ValorFechamentoCaixaDiaDTO.class) return (List<T>) listaValorFechamentoCaixaDia;
		else if(typeDef == PagamentoPedidoVendaDTO.class) return (List<T>) listaPagamentoPedidoVenda;
		else if(typeDef == ChequeRecebidoDTO.class) return (List<T>) listaChequeRecebido;
		else if(typeDef == PagamentoCartaoDTO.class) return (List<T>) listaPagamentoCartao;
		else if(typeDef == MovimentoCaixaDiaDTO.class) return (List<T>) listaMovimentoCaixaDia;
		else if(typeDef == MovimentoParcelaDuplicataDTO.class) return (List<T>) listaMovimentoParcelaDuplicata;
		else if(typeDef == LogOperacaoDTO.class) return (List<T>) listaLogOperacao;
		else if(typeDef == LogDuplicataDTO.class) return (List<T>) listaLogDuplicata;
		else if(typeDef == NotaFiscalDTO.class) return (List<T>) listaNotaFiscal;
		else if(typeDef == ItemNotaFiscalDTO.class)return (List<T>) listaItemNotaFiscal; 
		else if(typeDef == TipoContaDTO.class) return (List<T>) listaTipoConta;
		else if(typeDef == ContaGerencialDTO.class) return (List<T>) listaContaGerencial;
		else if(typeDef == TipoContaContaGerDTO.class) return (List<T>) listaTipoContaContaGer;
		else if(typeDef == TipoMovimentoCaixaDiaDTO.class) return (List<T>) listaTipoMovimentoCaixaDia;
		else return null;
	}
	
	/**
	 * clear all package data
	 */
	public void dispose()
	{
		synchronized(this)
		{
			listaUsuario.clear();
			listaVendedor.clear();
			listaPedidoVenda.clear();
			listaItemPedidoVenda.clear();
			listaDuplicata.clear();
			listaDuplicataParcela.clear();
			listaDuplicataAcordo.clear();
			listaCliente.clear();
			listaConjuge.clear();
			listaAvalista.clear();
			listaImagemUsuario.clear();
			listaHistoricoCobranca.clear();
			listaEmailCliente.clear();
			listaCaixaDia.clear();
			listaValorFechamentoCaixaDia.clear();
			listaPagamentoPedidoVenda.clear();
			listaChequeRecebido.clear();
			listaPagamentoCartao.clear();
			listaMovimentoCaixaDia.clear();
			listaMovimentoParcelaDuplicata.clear();
			listaLogOperacao.clear();
			listaLogDuplicata.clear();
			listaNotaFiscal.clear();
			listaItemNotaFiscal.clear();
			listaContaGerencial.clear();
			listaTipoConta.clear();
			listaTipoContaContaGer.clear();
			listaTipoMovimentoCaixaDia.clear();
			
			listaUsuario = null;
			listaVendedor = null;
			listaPedidoVenda = null;
			listaItemPedidoVenda = null;
			listaDuplicata = null;
			listaDuplicataParcela = null;
			listaDuplicataAcordo = null;
			listaCliente = null;
			listaConjuge = null;
			listaAvalista = null;
			listaImagemUsuario = null;
			listaHistoricoCobranca = null;
			listaEmailCliente = null;
			listaCaixaDia = null;
			listaValorFechamentoCaixaDia = null;
			listaPagamentoPedidoVenda = null;
			listaChequeRecebido = null;
			listaPagamentoCartao = null;
			listaMovimentoCaixaDia = null;
			listaMovimentoParcelaDuplicata = null;
			listaLogOperacao = null;
			listaLogDuplicata = null;
			listaContaGerencial = null;
			listaTipoConta = null;
			listaTipoContaContaGer = null;
			listaTipoMovimentoCaixaDia = null;
			
			packageSealed = false;
		}
		
		System.gc();
	}
	
	/**
	 * Print every Data list size
	 */
	public void dump()
	{
		synchronized(this)
		{
			System.out.println("---DATA PACKAGE---"); 
			System.out.println();
			System.out.println();
			System.out.println(" 	 PACKAGE " + (packageSealed ? "IS SEALED" : "IS NOT SEALED"));
			System.out.println(".................idEmpresaFisica " + idEmpresaFisica);
			System.out.println(".....................dataInicial " + dataInicial.toString());
			System.out.println(".......................dataFinal " + dataFinal.toString());
			System.out.println("....................listaUsuario size " + listaUsuario.size());
			System.out.println("...................listaVendedor size " + listaVendedor.size());
			System.out.println("................listaPedidoVenda size " + listaPedidoVenda.size());
			System.out.println("............listaItemPedidoVenda size " + listaItemPedidoVenda.size());
			System.out.println("..................listaDuplicata size " + listaDuplicata.size());
			System.out.println("...........listaDuplicataParcela size " + listaDuplicataParcela.size());
			System.out.println("............listaDuplicataAcordo size " + listaDuplicataAcordo.size());
			System.out.println("....................listaCliente size " + listaCliente.size());
			System.out.println("....................listaConjuge size " + listaConjuge.size());
			System.out.println("...................listaAvalista size " + listaAvalista.size());
			System.out.println("..............listaImagemUsuario size " + listaImagemUsuario.size());
			System.out.println("..........listaHistoricoCobranca size " + listaHistoricoCobranca.size());
			System.out.println("...............listaEmailCliente size " + listaEmailCliente.size());
			System.out.println("...................listaCaixaDia size " + listaCaixaDia.size()); 
			System.out.println("....listaValorFechamentoCaixaDia size " + listaValorFechamentoCaixaDia.size());
			System.out.println(".......listaPagamentoPedidoVenda size " + listaPagamentoPedidoVenda.size());
			System.out.println(".............listaChequeRecebido size " + listaChequeRecebido.size());
			System.out.println("............listaPagamentoCartao size " + listaPagamentoCartao.size());
			System.out.println("..........listaMovimentoCaixaDia size " + listaMovimentoCaixaDia.size());
			System.out.println("..listaMovimentoParcelaDuplicata size " + listaMovimentoParcelaDuplicata.size());
			System.out.println("................listaLogOperacao size " + listaLogOperacao.size());
			System.out.println("...............listaLogDuplicata size " + listaLogDuplicata.size());
			System.out.println(".................listaNotaFiscal size " + listaNotaFiscal.size());
			System.out.println(".............listaItemNotaFiscal size " + listaItemNotaFiscal.size());
			System.out.println(".............listaContaGerencial size " + listaContaGerencial.size());
			System.out.println("..................listaTipoConta size " + listaTipoConta.size());
			System.out.println("..........listaTipoContaContaGer size " + listaTipoContaContaGer.size());
			System.out.println("......listaTipoMovimentoCaixaDia size " + listaTipoMovimentoCaixaDia.size());

			System.out.println();
			System.out.println();
			System.out.println("---DATA PACKAGE---");	
		}		
	}
	
	
	/**
	 * Validate current DataPackage
	 * @throws IllegalAccessException when package is not sealed or idempresafisica is null or have no data definied
	 */
	public void validate() throws IllegalAccessException
	{
		if(!packageSealed)
			throw new IllegalAccessException("Package is not sealed!");
		if(idEmpresaFisica == null || idEmpresaFisica == 0x00)
			throw new IllegalStateException("Package idEmpresaFisica cannot be null");
		if(dataInicial == null || dataFinal == null)
			throw new IllegalStateException("Package Timestamp cannot be null");
	}
	
	/*
	 * seals the package.
	 * 
	 * Cannot add data anymore.
	 */
	public void seal()
	{
		synchronized(this)
		{
			packageSealed = true;	
		}
	}
}