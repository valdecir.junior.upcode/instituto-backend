package br.com.desenv.nepalign.integracao.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="duplicata")
public class DuplicataDTO extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idDuplicata")
	private Integer id;
		
	@Column(name = "idEmpresaFisica")
	private Integer empresaFisica;
	
	@Column(name = "idCliente")
	private Integer cliente;
	
	@Column(name = "idUsuario")
	private Integer usuario;
	
	@Column(name = "idPedidoVenda")
	private Integer pedidoVenda;
	
	@Column(name = "idVendedor")
	private Integer vendedor;
	
	@Column(name="numeroDuplicata")
	private Long numeroDuplicata;
	
	@Column(name="dataHoraInclusao")
	private Date dataHoraInclusao;
	
	@Column(name="numeroFatura")
	private Long numeroFatura;
	
	@Column(name="dataCompra")
	private Date dataCompra;
	
	@Column(name="numeroNotaFiscal")
	private String numeroNotaFiscal;
	
	@Column(name="valorTotalCompra")
	private Double valorTotalCompra;
	
	@Column(name="dataVencimentoPrimeiraPrestacao")
	private Date dataVencimentoPrimeiraPrestacao;
	
	@Column(name="valorEntrada")
	private Double valorEntrada;
	
	@Column(name="numeroTotalParcela")
	private String numeroTotalParcela;
	
	@Column(name="semPedidoVenda")
	private String semPedidoVenda;
	
	@Column(name="observacao")
	private String observacao;
	
	public DuplicataDTO()
	{
		super();
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		if (id != null && id == 0)
			this.id = null;
		else
			this.id = id;
	}	
	
	public Integer getPedidoVenda() 
	{
		return pedidoVenda;
	}

	public void setPedidoVenda(Integer pedidoVenda) 
	{
		this.pedidoVenda = pedidoVenda;
	}

	public Integer getEmpresaFisica() 
	{
		return empresaFisica;
	}

	public void setEmpresaFisica(Integer empresaFisica) 
	{
		this.empresaFisica = empresaFisica;
	}
	
	public Integer getCliente() 
	{
		return cliente;
	}

	public void setCliente(Integer cliente) 
	{
		this.cliente = cliente;
	}
	
	public Integer getUsuario() 
	{
		return usuario;
	}

	public void setUsuario(Integer usuario) 
	{
		this.usuario = usuario;
	}
	
	public Integer getVendedor() 
	{
		return vendedor;
	}

	public void setVendedor(Integer vendedor) 
	{
		this.vendedor = vendedor;
	}
	
	public Long getNumeroDuplicata() 
	{
		return numeroDuplicata;
	}

	public void setNumeroDuplicata(Long numeroDuplicata) 
	{
		if (numeroDuplicata != null && numeroDuplicata == 0)
			this.numeroDuplicata = null;
		else
			this.numeroDuplicata = numeroDuplicata;
	}	
	
	public Date getDataHoraInclusao() 
	{
		return dataHoraInclusao;
	}

	public void setDataHoraInclusao(Date dataHoraInclusao) 
	{
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	public Long getNumeroFatura() 
	{
		return numeroFatura;
	}

	public void setNumeroFatura(Long numeroFatura) 
	{
		if (numeroFatura != null && numeroFatura == 0)
			this.numeroFatura = null;
		else
			this.numeroFatura = numeroFatura;
	}
	
	public Date getDataCompra() 
	{
		return dataCompra;
	}

	public void setDataCompra(Date dataCompra) 
	{
		this.dataCompra = dataCompra;
	}
	
	public String getNumeroNotaFiscal() 
	{
		String retorno = null;
		
		if (numeroNotaFiscal != null)
			retorno = numeroNotaFiscal.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNumeroNotaFiscal(String numeroNotaFiscal) 
	{
		if (numeroNotaFiscal != null)
			this.numeroNotaFiscal = numeroNotaFiscal.toUpperCase().trim();
		else
			this.numeroNotaFiscal = null;
	}
		
	public Double getValorTotalCompra() 
	{
		return valorTotalCompra;
	}

	public void setValorTotalCompra(Double valorTotalCompra) 
	{
		if (valorTotalCompra != null && valorTotalCompra == 0.0)
			this.valorTotalCompra = null;
		else
			this.valorTotalCompra = valorTotalCompra;
	}
	
	public Date getDataVencimentoPrimeiraPrestacao() 
	{
		return dataVencimentoPrimeiraPrestacao;
	}

	public void setDataVencimentoPrimeiraPrestacao(Date dataVencimentoPrimeiraPrestacao) 
	{
		this.dataVencimentoPrimeiraPrestacao = dataVencimentoPrimeiraPrestacao;
	}
	
	public Double getValorEntrada() 
	{
		return valorEntrada;
	}

	public void setValorEntrada(Double valorEntrada) 
	{
		if (valorEntrada != null && valorEntrada == 0.0)
			this.valorEntrada = null;
		else
			this.valorEntrada = valorEntrada;
	}
	
	public String getNumeroTotalParcela() 
	{
		String retorno = null;
		
		if (numeroTotalParcela != null)
			retorno = numeroTotalParcela.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNumeroTotalParcela(String numeroTotalParcela) 
	{
		if (numeroTotalParcela != null)
			this.numeroTotalParcela = numeroTotalParcela.toUpperCase().trim(); 
		else
			this.numeroTotalParcela = null;
	}
		
	public String getSemPedidoVenda() 
	{
		return semPedidoVenda;
	}

	public void setSemPedidoVenda(String semPedidoVenda) 
	{
		this.semPedidoVenda = semPedidoVenda;
	}

	public String getObservacao() 
	{
		return observacao;
	}

	public void setObservacao(String observacao) 
	{
		this.observacao = observacao;
	}

	@Override
	public void validate() throws Exception { }

	@Override
	public DuplicataDTO clone() throws CloneNotSupportedException 
	{
		return (DuplicataDTO) super.clone();
	}
}
