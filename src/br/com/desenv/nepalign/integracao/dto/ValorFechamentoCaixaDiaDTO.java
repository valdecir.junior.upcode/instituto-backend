package br.com.desenv.nepalign.integracao.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="valorFechamentoCaixaDia")
public class ValorFechamentoCaixaDiaDTO extends GenericModelIGN implements Cloneable 
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idValorFechamentoCaixaDia")
	private Integer id;
		
	@Column(name = "idCaixaDia")
	private Integer caixadia;
	
	@Column(name = "idFormaPagamento")
	private Integer formaPagamento;
	
	@Column(name="dataFechamento")
	private Date dataFechamento;
	
	@Column(name="valorCalculado")
	private Double valorCalculado;
	
	@Column(name="valorInformado")
	private Double valorInformado;
	
	public ValorFechamentoCaixaDiaDTO()
	{
		super();
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public Integer getCaixadia() 
	{
		return caixadia;
	}

	public void setCaixadia(Integer caixadia) 
	{
		this.caixadia = caixadia;
	}
	
	public Integer getFormaPagamento() 
	{
		return formaPagamento;
	}

	public void setFormaPagamento(Integer formaPagamento) 
	{
		this.formaPagamento = formaPagamento;
	}
	
	public Date getDataFechamento() 
	{	
		return dataFechamento;
	}

	public void setDataFechamento(Date dataFechamento) 
	{
		this.dataFechamento = dataFechamento;
	}
	
	public Double getValorCalculado() 
	{
		return valorCalculado;
	}

	public void setValorCalculado(Double valorCalculado) 
	{
		if (valorCalculado != null && Double.isNaN(valorCalculado))
			this.valorCalculado = null;
		else
			this.valorCalculado = valorCalculado;	
	}
	
	public Double getValorInformado() 
	{
		return valorInformado;
	}

	public void setValorInformado(Double valorInformado) 
	{
		if (valorInformado != null && Double.isNaN(valorInformado))
			this.valorInformado = null;
		else
			this.valorInformado = valorInformado;
	}	
	
	@Override
	public void validate() throws Exception { }
	
	public ValorFechamentoCaixaDiaDTO clone() throws CloneNotSupportedException
	{
		return (ValorFechamentoCaixaDiaDTO) super.clone();
	}
}
