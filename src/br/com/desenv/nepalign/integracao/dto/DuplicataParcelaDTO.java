package br.com.desenv.nepalign.integracao.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="DuplicataParcela")
public class DuplicataParcelaDTO extends GenericModelIGN implements Cloneable 
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idDuplicataParcela")
	private Integer id;
		 
	@Column(name = "idDuplicata")
	private Integer duplicata;
	
	@Column(name = "idEmpresaFisica")
	private Integer empresaFisica;

	@Column(name = "idCliente")
	private Integer cliente;

	@Column(name = "idUsuario")
	private Integer usuario;
	
	@Column(name="numeroDuplicata")
	private Long numeroDuplicata;
	
	@Column(name="numeroParcela")
	private Integer numeroParcela;
	
	@Column(name="dataVencimento")
	private Date dataVencimento;
	
	@Column(name="valorParcela")
	private Double valorParcela;
	
	@Column(name="situacao")
	private String situacao;
	
	@Column(name="valorPago")
	private Double valorPago;
	
	@Column(name="dataBaixa")
	private Date dataBaixa;
	
	@Column(name="tipoPagamento")
	private String tipoPagamento;
	
	@Column(name="dataVencimento02Acordado")
	private Date dataVencimentoAcordado;
	
	@Column(name = "idFormaPagamento")
	private Integer formaPagamento;

	@Column(name = "idChequeRecebido")
	private Integer chequeRecebido;

	@Column(name = "idCobrador")
	private Integer cobrador;
	
	@Column(name="acordo")
	private String acordo;
	
	@Column(name="valorDesconto")
	private Double valorDesconto;
	
	@Column(name="jurosInformado")
	private Double jurosInformado;
	
	@Column(name="jurosOriginal")
	private Double jurosOriginal;
	
	@Column (name="dataCobranca")
	private Date dataCobranca;
	
	@Column(name="jurosPago")
	private Double jurosPago;
	
	@Column(name="valorAPagarInformado")
	private Double valorAPagarInformado;
	
	@Column(name="valorAPagarOriginal")
	private Double valorAPagarOriginal;
	
	@Transient
	private String seprocadaReativada;
	
	@Transient
	private String cobranca;
	
	public DuplicataParcelaDTO()
	{
		super();
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		if (id != null && id == 0)
			this.id = null;
		else
			this.id = id;
	}	
	
	public Double getJurosInformado() 
	{
		if(jurosInformado == null)
			return Double.NaN;
		
		return jurosInformado;	
	}

	public void setJurosInformado(Double jurosInformado)
	{
		if(jurosInformado==null || jurosInformado.isNaN())
			this.jurosInformado = null;	
		else
			this.jurosInformado = jurosInformado;	
	}

	public Double getValorDesconto() 
	{
		return valorDesconto;
	}

	public void setValorDesconto(Double valorDesconto) 
	{
		if(valorDesconto==null || valorDesconto.isNaN())
			this.valorDesconto = null;
		else
			this.valorDesconto = valorDesconto;	
	}

	public String getAcordo() 
	{
		return acordo;
	}

	public void setAcordo(String acordo) 
	{
		this.acordo = acordo;
	}
	
	public Integer getDuplicata() 
	{
		return duplicata;
	}

	public void setDuplicata(Integer duplicata) 
	{
		this.duplicata = duplicata;
	}
	
	public Integer getEmpresaFisica() 
	{
		return empresaFisica;
	}

	public void setEmpresaFisica(Integer empresaFisica) 
	{
		this.empresaFisica = empresaFisica;
	}
	
	public Integer getCliente() 
	{
		return cliente;
	}
	
	public Integer getFormaPagamento() 
	{
		return formaPagamento;
	}

	public void setFormaPagamento(Integer formaPagamento) 
	{
		this.formaPagamento = formaPagamento;
	}

	public void setCliente(Integer cliente) 
	{
		this.cliente = cliente;
	}
	
	public Integer getUsuario() 
	{
		return usuario;
	}

	public void setUsuario(Integer usuario) 
	{
		this.usuario = usuario;
	}
	
	public Long getNumeroDuplicata() 
	{
		return numeroDuplicata;
	}

	public void setNumeroDuplicata(Long numeroDuplicata) 
	{
		this.numeroDuplicata = numeroDuplicata;
	}	
	
	public Integer getNumeroParcela() 
	{
		return numeroParcela;
	}

	public void setNumeroParcela(Integer numeroParcela) 
	{
		this.numeroParcela = numeroParcela;
	}	

	public Date getDataVencimento() 
	{
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) 
	{
		this.dataVencimento = dataVencimento;
	}
	
	public Double getValorParcela() 
	{
		if(valorParcela == null || valorParcela.isNaN())
			return null;
		else
			return valorParcela;
	}

	public void setValorParcela(Double valorParcela) 
	{
		this.valorParcela = valorParcela;
		
	}	
	public String getSituacao() 
	{
		String retorno = null;
		
		if (situacao != null)
			retorno = situacao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setSituacao(String situacao) 
	{
		if (situacao != null)
			this.situacao = situacao.toUpperCase().trim();
		else
			this.situacao = null;
	}
		
	public Double getValorPago() 
	{
		if(valorPago == null || valorPago.isNaN())
			return null;
		else
			return valorPago;
	}

	public void setValorPago(Double valorPago) 
	{
		this.valorPago = valorPago;	
	}	
	
	public Date getDataBaixa() 
	{
		return dataBaixa;
	}

	public void setDataBaixa(Date dataBaixa) 
	{
		this.dataBaixa = dataBaixa;
	}
	
	public String getTipoPagamento() 
	{
		String retorno = null;
		
		if (tipoPagamento != null)
			retorno = tipoPagamento.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setTipoPagamento(String tipoPagamento) 
	{
		if (tipoPagamento != null)
			this.tipoPagamento = tipoPagamento.toUpperCase().trim();
		else
			this.tipoPagamento = null;
	}
		
	public Date getDataVencimentoAcordado() 
	{
		return dataVencimentoAcordado;
	}

	public void setDataVencimentoAcordado(Date dataVencimentoAcordado) 
	{
		this.dataVencimentoAcordado = dataVencimentoAcordado;
	}
	
	public Integer getChequeRecebido() 
	{
		return chequeRecebido;
	}

	public void setChequeRecebido(Integer chequeRecebido) 
	{
		this.chequeRecebido = chequeRecebido;
	}

	public Integer getCobrador() 
	{
		return cobrador;
	}

	public void setCobrador(Integer cobrador) 
	{
		this.cobrador = cobrador;
	}

	public Double getJurosOriginal() 
	{
		if(jurosOriginal==null)
			return null;
		if(jurosOriginal.isNaN())
			return null;
		else
			return jurosOriginal;
	}

	public void setJurosOriginal(Double jurosOriginal)
	{
		if(jurosOriginal==null || jurosOriginal.isNaN())
			this.jurosOriginal = null;
		else
			this.jurosOriginal = jurosOriginal;
	}
	
	public Date getDataCobranca() 
	{
		return dataCobranca;
	}

	public Double getJurosPago() 
	{
		return jurosPago;
	}

	public void setJurosPago(Double jurosPago) 
	{
		this.jurosPago = jurosPago;
	}

	public void setDataCobranca(Date dataCobranca) 
	{
		this.dataCobranca = dataCobranca;
	}

	public Double getValorAPagarInformado() 
	{
		return valorAPagarInformado;
	}

	public void setValorAPagarInformado(Double valorAPagarInformado) 
	{
		this.valorAPagarInformado = valorAPagarInformado;
	}

	public Double getValorAPagarOriginal() 
	{
		return valorAPagarOriginal;
	}

	public void setValorAPagarOriginal(Double valorAPagarOriginal) 
	{
		this.valorAPagarOriginal = valorAPagarOriginal;
	}
	
	public String getSeprocadaReativada() 
	{
		return seprocadaReativada;
	}

	public void setSeprocadaReativada(String seprocadaReativada) 
	{
		this.seprocadaReativada = seprocadaReativada;
	}

	public String getCobranca() 
	{
		return cobranca;
	}

	public void setCobranca(String cobranca) 
	{
		this.cobranca = cobranca;
	}

	@Override
	public DuplicataParcelaDTO clone() throws CloneNotSupportedException 
	{
        return (DuplicataParcelaDTO) super.clone();
    }

	@Override
	public void validate() throws Exception { }
}