package br.com.desenv.nepalign.integracao.dto;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="conjuge")
public class ConjugeDTO extends GenericModelIGN implements Cloneable 
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idConjuge")
	private Integer id;
		
	@Column(name = "foto")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] foto;
	
	@Column(name="nome")
	private String nome;
	
	@Column(name="cpfCnpj")
	private String cpfCnpj;
	
	@Column(name="rg")
	private String rg;
	
	@Column(name="dataNascimento")
	private Date dataNascimento;
	
	@Column(name = "idOperadoraCelular01")
	private Integer operadoraCelular01;
	
	@Column(name="telefoneCelular01")
	private String telefoneCelular01;
	
	@Column(name = "idOperadoraCelular02")
	private Integer operadoraCelular02;
	
	@Column(name="telefoneCelular02")
	private String telefoneCelular02;
	
	@Column(name="salario")
	private Double salario;
	
	@Column(name="outraRenda")
	private Double outraRenda;
	
	@Column(name="empresaTrabalha")
	private String empresaTrabalha;
	
	@Column(name="profissao")
	private String profissao;
	
	@Column(name="tempoTrabalho")
	private Date tempoTrabalho;
	
	@Column(name="logradouroComercial")
	private String logradouroComercial;
	
	@Column(name="numeroComercial")
	private String numeroComercial;
	
	@Column(name = "idCidadeComercial")
	private Integer cidadeComercial;
	
	@Column(name="cidadeComercialCache")
	private String cidadeComercialCache;
	
	@Column(name="estadoComercialCache")
	private String estadoComercialCache;
	
	@Column(name="cepComercial")
	private String cepComercial;
	
	@Column(name = "idOperadoraComercial")
	private Integer operadoraComercial;
	
	@Column(name="telefoneComercial")
	private String telefoneComercial;
	
	@Column(name="informacaoComercial")
	private String informacaoComercial;
	
	@Column(name="numeroCalcado")
	private String numeroCalcado;
	
	@Column(name="time")
	private String time;

	public ConjugeDTO()
	{
		super();
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		if (id != null && id == 0)
			this.id = null;
		else
			this.id = id;
	}
	
	public byte[] getFoto() 
	{
		return foto;
	}

	public void setFoto(byte[] foto) 
	{
		this.foto = foto;
	}
	
	public String getNome() 
	{
		String retorno = null;
		
		if (nome != null)
			retorno = nome.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNome(String nome) 
	{
		if (nome != null)
			this.nome = nome.toUpperCase().trim();
		else
			this.nome = null;
	}
		
	public String getCpfCnpj() 
	{
		String retorno = null;
		
		if (cpfCnpj != null)
			retorno = cpfCnpj.toUpperCase().trim().replaceAll("[^0-9]", "");
		
		return retorno;
	}

	public void setCpfCnpj(String cpfCnpj) 
	{
		if (cpfCnpj != null)
			this.cpfCnpj = cpfCnpj.toUpperCase().trim().replaceAll("[^0-9]", "");
		else
			this.cpfCnpj = null;
	}
		
	public String getRg() 
	{
		String retorno = null;
		
		if (rg != null)
			retorno = rg.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setRg(String rg) 
	{
		if (rg != null)
			this.rg = rg.toUpperCase().trim();
		else
			this.rg = null;
	}
		
	public Date getDataNascimento() 
	{
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) 
	{
		this.dataNascimento = dataNascimento;
	}
	
	public Integer getOperadoraCelular01() 
	{
		return operadoraCelular01;
	}

	public void setOperadoraCelular01(Integer operadoraCelular01) 
	{
		this.operadoraCelular01 = operadoraCelular01;
	}
	
	public String getTelefoneCelular01() 
	{
		String retorno = null;
		
		if (telefoneCelular01 != null)
			retorno = telefoneCelular01.toUpperCase().trim().replaceAll("[^0-9]", "");
		
		return retorno;
	}
	
	public void setTelefoneCelular01(String telefoneCelular01) 
	{
		if (telefoneCelular01 != null)
			this.telefoneCelular01 = telefoneCelular01.toUpperCase().trim().replaceAll("[^0-9]", "");
		else
			this.telefoneCelular01 = null;
	}
		
	public Integer getOperadoraCelular02() 
	{
		return operadoraCelular02;
	}

	public void setOperadoraCelular02(Integer operadoraCelular02) 
	{
		this.operadoraCelular02 = operadoraCelular02;
	}
	
	public String getTelefoneCelular02() 
	{
		String retorno = null;
		
		if (telefoneCelular02 != null)
			retorno = telefoneCelular02.toUpperCase().trim().replaceAll("[^0-9]", "");
		
		return retorno;
	}
	
	public void setTelefoneCelular02(String telefoneCelular02) 
	{
		if (telefoneCelular02 != null)
			this.telefoneCelular02 = telefoneCelular02.toUpperCase().trim().replaceAll("[^0-9]", "");
		else
			this.telefoneCelular02 = null;
	}
		
	public Double getSalario() 
	{
		return salario;
	}

	public void setSalario(Double salario) 
	{
		if (salario != null && Double.isNaN(salario))
			this.salario = null;
		else
			this.salario = salario;
	}
	
	public Double getOutraRenda() 
	{
		return outraRenda;
	}

	public void setOutraRenda(Double outraRenda) 
	{
		if (outraRenda != null && Double.isNaN(outraRenda))
			this.outraRenda = null;
		else
			this.outraRenda = outraRenda;
	}
	
	public String getEmpresaTrabalha() 
	{
		String retorno = null;
		
		if (empresaTrabalha != null)
			retorno = empresaTrabalha.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setEmpresaTrabalha(String empresaTrabalha) 
	{
		if (empresaTrabalha != null)
			this.empresaTrabalha = empresaTrabalha.toUpperCase().trim();
		else
			this.empresaTrabalha = null;
	}
		
	public String getProfissao() 
	{
		String retorno = null;
		
		if (profissao != null)
			retorno = profissao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setProfissao(String profissao) {
		if (profissao != null)
			this.profissao = profissao.toUpperCase().trim();
		else
			this.profissao = null;
	}
		
	public Date getTempoTrabalho() 
	{
		return tempoTrabalho;
	}

	public void setTempoTrabalho(Date tempoTrabalho) 
	{
		this.tempoTrabalho = tempoTrabalho;
	}
	
	public String getLogradouroComercial() 
	{
		String retorno = null;
		
		if (logradouroComercial != null)
			retorno = logradouroComercial.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setLogradouroComercial(String logradouroComercial) 
	{
		if (logradouroComercial != null)
			this.logradouroComercial = logradouroComercial.toUpperCase().trim();
		else
			this.logradouroComercial = null;
	}
		
	public String getNumeroComercial() 
	{
		String retorno = null;
		
		if (numeroComercial != null)
			retorno = numeroComercial.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNumeroComercial(String numeroComercial)
	{
		if (numeroComercial != null)
			this.numeroComercial = numeroComercial.toUpperCase().trim();
		else
			this.numeroComercial = null;
	}
		
	public Integer getCidadeComercial() 
	{
		return cidadeComercial;
	}

	public void setCidadeComercial(Integer cidadeComercial) 
	{
		this.cidadeComercial = cidadeComercial;
	}
	
	public String getCidadeComercialCache() 
	{
		String retorno = null;
		
		if (cidadeComercialCache != null)
			retorno = cidadeComercialCache.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setCidadeComercialCache(String cidadeComercialCache) 
	{
		if (cidadeComercialCache != null)
			this.cidadeComercialCache = cidadeComercialCache.toUpperCase().trim();
		else
			this.cidadeComercialCache = null;
	}
		
	public String getEstadoComercialCache() 
	{
		String retorno = null;
		
		if (estadoComercialCache != null)
			retorno = estadoComercialCache.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setEstadoComercialCache(String estadoComercialCache) 
	{
		if (estadoComercialCache != null)
			this.estadoComercialCache = estadoComercialCache.toUpperCase().trim();
		else
			this.estadoComercialCache = null;
	}
		
	public String getCepComercial() 
	{
		String retorno = null;
		
		if (cepComercial != null)
			retorno = cepComercial.toUpperCase().trim().replaceAll("[^0-9]", "");
		
		return retorno;
	}
	
	public void setCepComercial(String cepComercial) 
	{
		if (cepComercial != null)
			this.cepComercial = cepComercial.toUpperCase().trim().replaceAll("[^0-9]", "");
		else
			this.cepComercial = null;
	}
		
	public Integer getOperadoraComercial() 
	{
		return operadoraComercial;
	}

	public void setOperadoraComercial(Integer operadoraComercial) 
	{
		this.operadoraComercial = operadoraComercial;
	}
	
	public String getTelefoneComercial() 
	{
		String retorno = null;
		
		if (telefoneComercial != null)
			retorno = telefoneComercial.toUpperCase().trim().replaceAll("[^0-9]", "");
		
		return retorno;
	}
	
	public void setTelefoneComercial(String telefoneComercial) 
	{
		if (telefoneComercial != null)
			this.telefoneComercial = telefoneComercial.toUpperCase().trim().replaceAll("[^0-9]", "");
		else
			this.telefoneComercial = null;
	}
		
	public String getInformacaoComercial() 
	{
		String retorno = null;
		
		if (informacaoComercial != null)
			retorno = informacaoComercial.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setInformacaoComercial(String informacaoComercial) 
	{
		if (informacaoComercial != null)
			this.informacaoComercial = informacaoComercial.toUpperCase().trim();
		else
			this.informacaoComercial = null;
	}
		
	public String getNumeroCalcado() 
	{
		String retorno = null;
		
		if (numeroCalcado != null)
			retorno = numeroCalcado.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setNumeroCalcado(String numeroCalcado) 
	{
		if (numeroCalcado != null)
			this.numeroCalcado = numeroCalcado.toUpperCase().trim();
		else
			this.numeroCalcado = null;
	}
		
	public String getTime() 
	{
		String retorno = null;
		
		if (time != null)
			retorno = time.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setTime(String time) 
	{
		if (time != null)
			this.time = time.toUpperCase().trim();
		else
			this.time = null;
	}
	
	/**
	 * Tentativa de resolver memory leaks
	 */
	public void setFotoRg(byte[] unused) { unused = null; }
	public byte[] getFotoRg() { return null; }
	public void setFotoCpf(byte[] unused) { unused = null; }
	public byte[] getFotoCpf() { return null; }
	/**
	 * 
	 */
	

	@Override
	public void validate() throws Exception { }
	
	@Override
	public ConjugeDTO clone() throws CloneNotSupportedException 
	{
		return (ConjugeDTO) super.clone();
	}
}
