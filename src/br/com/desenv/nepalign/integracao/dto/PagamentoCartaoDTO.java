package br.com.desenv.nepalign.integracao.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="pagamentocartao")
public class PagamentoCartaoDTO extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idPagamentoCartao")
	private Integer id;
	@Column(name = "idEmpresaFisica")
	private Integer empresaFisica;
	@Column(name="dataEmissao")
	private Date dataEmissao;
	@Column(name="emitente")
	private String emitente;
	@Column(name = "idFormaPagamento")
	private Integer formaPagamento;
	@Column(name="valorOriginal")
	private Double valorOriginal;
	@Column(name="valorTaxado")
	private Double valorTaxado;
	@Column(name="numeroParcelas")
	private Integer numeroParcelas;
	@Column(name="valorParcela")
	private Double valorParcela;
	@Column(name = "idTaxaPagamento")
	private Integer taxaPagamento;
	@Column(name = "idCaixaDia")
	private Integer caixaDia;
	@Column(name="alterado")
	private Boolean alterado;
	@Column(name = "idOperadoraCartao")
	private Integer operadoraCartao;

	public PagamentoCartaoDTO()
	{
		super();
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public Integer getEmpresaFisica() 
	{
		return empresaFisica;
	}

	public void setEmpresaFisica(Integer empresaFisica) 
	{
		this.empresaFisica = empresaFisica;
	}

	public Date getDataEmissao() 
	{
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao) 
	{
		this.dataEmissao = dataEmissao;
	}
	
	public String getEmitente() 
	{
		String retorno = null;

		if (emitente != null)
			retorno = emitente.toUpperCase().trim();
		
		return retorno;
	}

	public Boolean getAlterado() 
	{
		return alterado;
	}

	public void setAlterado(Boolean alterado) 
	{
		this.alterado = alterado;
	}

	public void setEmitente(String emitente) 
	{
		if (emitente != null)
			this.emitente = emitente.toUpperCase().trim();
		else
			this.emitente = null;
	}

	public Integer getFormaPagamento() 
	{
		return formaPagamento;
	}

	public void setFormaPagamento(Integer formaPagamento) 
	{
		this.formaPagamento = formaPagamento;
	}

	public Double getValorOriginal() 
	{
		return valorOriginal;
	}

	public void setValorOriginal(Double valorOriginal) 
	{
		if (valorOriginal != null && Double.isNaN(valorOriginal))
			this.valorOriginal = null;
		else
			this.valorOriginal = valorOriginal;
	}	
	
	public Integer getNumeroParcelas() 
	{
		return numeroParcelas;
	}

	public void setNumeroParcelas(Integer numeroParcelas) 
	{
		this.numeroParcelas = numeroParcelas;
	}
	
	public Double getValorParcela() 
	{
		return valorParcela;
	}

	public void setValorParcela(Double valorParcela) 
	{
		if (valorParcela != null && Double.isNaN(valorParcela))
			this.valorParcela = null;
		else
			this.valorParcela = valorParcela;
	}	
	
	public Integer getTaxaPagamento() 
	{
		return taxaPagamento;
	}

	public void setTaxaPagamento(Integer taxaPagamento) 
	{
		this.taxaPagamento = taxaPagamento;
	}

	public Integer getCaixaDia() 
	{
		return caixaDia;
	}

	public void setCaixaDia(Integer caixaDia) 
	{
		this.caixaDia = caixaDia;
	}

	public Double getValorTaxado() 
	{
		return valorTaxado;
	}

	public void setValorTaxado(Double valorTaxado) 
	{
		this.valorTaxado = valorTaxado;
	}

	public Integer getOperadoraCartao() 
	{
		return operadoraCartao;
	}

	public void setOperadoraCartao(Integer operadoraCartao) 
	{
		this.operadoraCartao = operadoraCartao;
	}

	@Override
	public void validate() throws Exception { }
}