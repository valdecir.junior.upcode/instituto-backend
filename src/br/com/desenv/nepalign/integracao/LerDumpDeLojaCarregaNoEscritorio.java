package br.com.desenv.nepalign.integracao;

import java.io.File;
import java.io.FilenameFilter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.ItemMovimentacaoEstoque;
import br.com.desenv.nepalign.model.MovimentacaoEstoque;
import br.com.desenv.nepalign.model.TmpItemMovimentacaoEstoque;
import br.com.desenv.nepalign.model.TmpMovimentacaoEstoque;
import br.com.desenv.nepalign.persistence.ItemMovimentacaoEstoquePersistence;
import br.com.desenv.nepalign.persistence.MovimentacaoEstoquePersistence;
import br.com.desenv.nepalign.service.ItemMovimentacaoEstoqueService;
import br.com.desenv.nepalign.service.MovimentacaoEstoqueService;
import br.com.desenv.nepalign.service.TmpItemMovimentacaoEstoqueService;
import br.com.desenv.nepalign.service.TmpMovimentacaoEstoqueService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.util.FtpUtil;
import br.com.desenv.nepalign.util.MailUtil;
import br.com.desenv.nepalign.util.UtilDatabase;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class LerDumpDeLojaCarregaNoEscritorio
{
    public static void main(String[] args) throws Exception
    { 
    	new LerDumpDeLojaCarregaNoEscritorio().iniciarIntegracao(Integer.parseInt(args[0x1]), Integer.parseInt(args[0x2]), args[0x3]);  
    } 
      
    public LerDumpDeLojaCarregaNoEscritorio() throws Exception 
    {
    	ConexaoUtil.getConexaoPadrao();
    	DsvConstante.getParametrosSistema();
    	DsvConstante.getParametrosSistema().get("loja"); 
    }
    
    public void iniciarIntegracao(Integer idEmpresaFisica, Integer sequencial, String data) throws Exception
    { 
		Exception processException = null;
		boolean processOk = false;
		
    	Date dataInicioProcesso = new Date();	
    	String numeroEmpresaFisica = String.format ("%02d", idEmpresaFisica);
    	
    	try
    	{
    		Date dataIntegracao = null;
    		if(data.equals("0"))
    			dataIntegracao = new Date();
    		else
    			dataIntegracao = new SimpleDateFormat("dd/MM/yyyy").parse(data);

    		lerArquivoServidorFtpECarregaEscritorio(idEmpresaFisica, sequencial, dataIntegracao);
        	
        	processOk = true;
    	}
    	catch(Exception ex)
    	{
			processOk = false; 
			processException = ex;
    	}
    	finally
    	{
    		if(processException != null)
    			processException.printStackTrace();
    		
    		Date dataFimProcesso = new Date();
    		Date dataTerminoProcesso = new Date();
    		dataTerminoProcesso.setTime(dataFimProcesso.getTime() - dataInicioProcesso.getTime());
    		
			if(processOk)
			{
				new MailUtil().sendMail(DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_ORIGEM_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_EMAIL_ORIGEM_NOTIFICACOES"), "Sistema Integrador Nepal", DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_DESTINO_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("SMPT_EMAIL_ORIGEM_NOTIFICACOES"), "integração Nepal. Baixa das OPME's da Loja " + numeroEmpresaFisica + " no Escritório", 
        				"Olá, a integração de baixa das OPME's da Loja " + numeroEmpresaFisica + " no Escritório foi concluída.<br><br>Iniciado em " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataInicioProcesso) + " e finalizado em " + 
        				new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataFimProcesso) + " o processo durou " + new SimpleDateFormat("mm:ss").format(dataTerminoProcesso) + " (minutos:segundos).");	
			}
			else
			{ 
				new MailUtil().sendMail(DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_ORIGEM_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_EMAIL_ORIGEM_NOTIFICACOES"), "Sistema Integrador Nepal", DsvConstante.getParametrosSistema().get("INTEGRACAO_EMAIL_DESTINO_NOTIFICACOES"), DsvConstante.getParametrosSistema().get("SMPT_EMAIL_ORIGEM_NOTIFICACOES"), "integração Nepal. Baixa das OPME's da Loja " + numeroEmpresaFisica + " no Escritório FALHOU!!!", 
        				"Olá, a integração de baixa das OPME's da Loja " + numeroEmpresaFisica + " no Escritório não foi concluída.<br><br>Iniciado em " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataInicioProcesso) + " e finalizado em " + 
        				new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(dataFimProcesso) + " o processo durou " + new SimpleDateFormat("mm:ss").format(dataTerminoProcesso) + " (minutos:segundos) antes de ser abortado!!!!" + 
        				"<br><br>ERRO : " + processException.toString());
			}	
    	}
    }
  
    private Connection obterConexao(String url, String usuario, String senha) throws Exception
    {
		Class.forName("com.mysql.jdbc.Driver");
		return DriverManager.getConnection(url, usuario, senha); 
    }
    
    private void lerArquivoServidorFtpECarregaEscritorio(Integer idEmpresaFisica, Integer sequencial, Date data) throws Exception
    {
    	try
    	{
        	String numeroEmpresaFisicaFormatado = String.format("%02d", idEmpresaFisica);
        	String nomeArquivoDump = "dump_OPME_" + numeroEmpresaFisicaFormatado + "_" + new SimpleDateFormat("yyyyMMdd").format(data) + "_" + sequencial.toString() + ".NEPALDUMP";
        	
        	Logger.getGlobal().info("Initializing Download of file " + nomeArquivoDump + " for F" + numeroEmpresaFisicaFormatado);

        	new FtpUtil().downloadFile(nomeArquivoDump.replace("NEPALDUMP", "rar"), DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump.replace("NEPALDUMP", "rar"), DsvConstante.getParametrosSistema().get("INTEGRACAO_IP_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_USUARIO_FTP"), DsvConstante.getParametrosSistema().get("INTEGRACAO_SENHA_FTP"));
        	
        	Logger.getGlobal().info("Downloaded! Extracting"); 
        	
        	IgnUtil.descompactarArquivo(DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump.replace("NEPALDUMP", "rar"), DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump.split("\\.")[0]);
        	
        	Logger.getGlobal().info("Done! Restoring " + (DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump.split("\\.")[0] + "/" + nomeArquivoDump) + " in Database " + ("nepalignloja" + numeroEmpresaFisicaFormatado));

        	new UtilDatabase().restoreBackupFile(DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + nomeArquivoDump.split("\\.")[0] + "/" + nomeArquivoDump, "nepalignloja" + numeroEmpresaFisicaFormatado);
    	
        	Logger.getGlobal().info("Restore complete!");
        	
        	atualizarNomeArquivosJaUsados();	
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    		throw ex;
    	}
    }

    @Deprecated
    private void sincronizarLojaComEscritorio() throws Exception
    {
    	Logger.getGlobal().info("Initializing Sync OP's");
    	
		EntityManager manager = null;
		EntityTransaction transaction = null;
		IgnUtil ignUtil = new IgnUtil();
		
	    EntityManagerFactory managerFactory = ConexaoUtil.getFactory();

	    String connectionUsername = managerFactory.getProperties().get("javax.persistence.jdbc.user").toString();
	    String connectionPassword = managerFactory.getProperties().get("javax.persistence.jdbc.password").toString();

		MovimentacaoEstoqueService movimentacaoEstoqueService = new MovimentacaoEstoqueService();
		ItemMovimentacaoEstoqueService itemMovimentacaoEstoqueService = new ItemMovimentacaoEstoqueService();
		
		try
		{
			manager = ConexaoUtil.getEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();	
		
			for (int i = 8; i < 9; i++)
			{	        	    
				String numeroEmpresaFisica = String.format ("%02d", i);
				 
				Logger.getGlobal().info("Initializing Sync to F" + numeroEmpresaFisica);

				String url = DsvConstante.getParametrosSistema().get("INTEGRACAO_URL_DATABASE_LOJA") + "nepalignloja" + numeroEmpresaFisica; 
	        	    
				Connection con = obterConexao(url, connectionUsername, connectionPassword);
	        	     
				Logger.getGlobal().info("Connection established to URL " + url);
	        	    
				String sql = "select operacao, objeto, idoperacao, nomeClasse from nepalignloja"  + numeroEmpresaFisica + ".operacaomovimentacaoestoque where flagsincronizacao = 0 order by operacao desc, dataOperacao asc;";
				
				Logger.getGlobal().info("Checking pending OP's with Query " + sql);
				
				Statement st = con.createStatement();
				Statement stAtualizacao = con.createStatement();
	        	   
				ResultSet rs = st.executeQuery(sql);
				
				boolean hasOp = rs.next();
				
				if(!hasOp)
					Logger.getGlobal().info("No Pending OP's");
				else 
				{
					Logger.getGlobal().info("Found Pending OP's!");
					rs.beforeFirst();	
				}
				 
				while (rs.next())
				{
					String operacao = rs.getString(1);
					byte[] objeto = rs.getBytes(2);
	        		int idOperacaoMovimentacaoEstoque = rs.getInt(3);
	        		String classe = rs.getString(4);
	        		
	        		Logger.getGlobal().info("Handling OP " + idOperacaoMovimentacaoEstoque + " [" + operacao + "] to class " + classe);
	        		
	        		boolean handled = false;
	        		
	        		if(classe.equalsIgnoreCase(MovimentacaoEstoquePersistence.class.getName()))
	        		{
	            		MovimentacaoEstoque movimentacaoEstoque = (MovimentacaoEstoque) ignUtil.recuperarObjectoPorByteArray(objeto);
	            		
	            		if(movimentacaoEstoque.getId().equals(501081229))
	            			System.out.println("Oi"); 
	            		
	            		movimentacaoEstoque.setUsuario(null);
	            		movimentacaoEstoque.setForncedor(null);
	            		movimentacaoEstoque.setCliente(null);
	            		
	            		TmpMovimentacaoEstoque tmpMovimentacaoEstoque = new TmpMovimentacaoEstoqueService().getTmpMovimentacaoEstoquePorMovimentacaoEstoque(movimentacaoEstoque);
	                    
	            		try
	            		{
		            		if (operacao != null && operacao.equalsIgnoreCase("SALVAR"))
		            			new TmpMovimentacaoEstoqueService().salvar(tmpMovimentacaoEstoque);
		            		    //movimentacaoEstoqueService.salvar(movimentacaoEstoque, false);
		            		else if (operacao != null && operacao.equalsIgnoreCase("ALTERAR"))
		            			new TmpMovimentacaoEstoqueService().atualizar(tmpMovimentacaoEstoque);
		            			//movimentacaoEstoqueService.atualizar(movimentacaoEstoque, false);
		            		else if (operacao != null && operacao.equalsIgnoreCase("EXCLUIR"))
		            			new TmpMovimentacaoEstoqueService().excluir(tmpMovimentacaoEstoque);
		            		    //movimentacaoEstoqueService.excluir(movimentacaoEstoque);
		            		
		            		handled = true;
	            		}
	            		catch(Exception ex)
	            		{
	            			Logger.getGlobal().severe("Fail ME " + (movimentacaoEstoque == null ? "null" : movimentacaoEstoque.getId().toString()));
	            		}
	        		}
	        		else if(classe.equalsIgnoreCase(ItemMovimentacaoEstoquePersistence.class.getName()))
	        		{
	            		ItemMovimentacaoEstoque itemMovimentacaoEstoque = (ItemMovimentacaoEstoque) ignUtil.recuperarObjectoPorByteArray(objeto); 
	            		TmpItemMovimentacaoEstoque tmpItemMovimentacaoEstoque = new TmpItemMovimentacaoEstoqueService().getTmpItemMovimentacaoEstoquePorItemMovimentacaoEstoque(itemMovimentacaoEstoque);
	            		
	            		try  
	            		{
		        			if(operacao != null && operacao.equalsIgnoreCase("SALVAR"))
		        				new TmpItemMovimentacaoEstoqueService().salvar(tmpItemMovimentacaoEstoque);
		        				//itemMovimentacaoEstoqueService.salvar(itemMovimentacaoEstoque, false);
		            		else if (operacao != null && operacao.equalsIgnoreCase("ALTERAR"))
		            			new TmpItemMovimentacaoEstoqueService().atualizar(tmpItemMovimentacaoEstoque);
		            			//itemMovimentacaoEstoqueService.atualizar(itemMovimentacaoEstoque, false);
		            		else if (operacao != null && operacao.equalsIgnoreCase("EXCLUIR"))
		            			new TmpItemMovimentacaoEstoqueService().excluir(tmpItemMovimentacaoEstoque);
		            			//itemMovimentacaoEstoqueService.excluir(itemMovimentacaoEstoque, false);
		        			
		        			handled = true;
	            		}
	            		catch(Exception ex)
	            		{
	            			Logger.getGlobal().severe("Fail on IME to ME " + (itemMovimentacaoEstoque.getMovimentacaoEstoque() == null ? "null" : itemMovimentacaoEstoque.getMovimentacaoEstoque().getId().toString()));
	            		}
	        		}
	        		else
	        			Logger.getGlobal().warning("Invalid OP Class : " + classe);

	        		if(handled)
	        		{
		        		stAtualizacao.executeUpdate("update nepalignloja"  + numeroEmpresaFisica + ".operacaomovimentacaoestoque set flagsincronizacao = 1 where idoperacao = " + idOperacaoMovimentacaoEstoque + ";");
		        		
		        		Logger.getGlobal().info("OP Updated to Handled.");	
	        		}
	        		else
	        			Logger.getGlobal().info("Fail to Handle");
				}

				if(!hasOp)
					Logger.getGlobal().info("No more OP's to handle");
				
				rs.close();
				st.close();
				stAtualizacao.close();
				con.close();
			} 
			
			transaction.commit();
			
			Logger.getGlobal().info("Finish");
		}
		catch(Exception ex)
		{
		    if (transaction != null)
		    	transaction.rollback();
		    ex.printStackTrace();
		    throw ex;
		} 
    }

    private void atualizarNomeArquivosJaUsados() throws Exception
    {
    	Logger.getGlobal().info("Updating name of files already handled");
    	
    	File directory = new File(DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS"));
	
    	File[] myFiles = directory.listFiles(new FilenameFilter() 
    	{
    		public boolean accept(File directory, String fileName) 
    		{
    			return fileName.endsWith(".rar");
    		}
    	});
	
    	for (File file: myFiles)
    	{
    		Logger.getGlobal().info(file.getName() + " updated to " + file.getName().replace(".rar", ".ok"));
    		
    		File novoArquivo = new File(DsvConstante.getParametrosSistema().get("INTEGRACAO_DIRETORIO_ARQUIVOS") + file.getName().replace(".rar", ".ok"));
    		file.renameTo(novoArquivo);
    		
    		file = null;
    		novoArquivo = null;
    	}
    	
    	Logger.getGlobal().info("Finish filename updates");
    }
}