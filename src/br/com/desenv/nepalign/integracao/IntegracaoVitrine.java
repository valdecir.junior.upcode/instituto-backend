package br.com.desenv.nepalign.integracao;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import br.com.desenv.nepalign.model.VitrineLv;
import br.com.desenv.nepalign.model.VitrineLvDepartamento;
import br.com.desenv.nepalign.model.VitrineLvProduto;

import com.desenv.nepal.catalog.wsdl.Catalog;
import com.desenv.nepal.catalog.wsdl.CatalogSoap;
import com.desenv.nepal.catalog.xmlData.ListGroupDeptRemove;
import com.desenv.nepal.catalog.xmlData.ListGroupProductRemove;
import com.desenv.nepal.catalog.xmlData.ListGroupRemove;
import com.desenv.nepal.catalog.xmlData.ListGroupAddUpdate;
import com.desenv.nepal.catalog.xmlData.ListGroupDeptAdd;
import com.desenv.nepal.catalog.xmlData.ListGroupProductAdd;
import com.desenv.nepal.catalog.xmlData.ReturnData;
import com.desenv.nepal.integracao.util.DesenvXMLUtil;

public class IntegracaoVitrine 
{
	private Catalog catalog;
	private CatalogSoap catalogSoap;
	private DesenvXMLUtil desenvXMLUtil;
	
	private String user;
	private String password;
	
	public IntegracaoVitrine(String user, String password) throws Exception
	{
		try
		{
			this.user = user;
			this.password = password;
			
			this.catalog = new Catalog();
			this.catalogSoap = this.catalog.getCatalogSoap();
			this.desenvXMLUtil = new DesenvXMLUtil();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}
	
	public static void main(String[] args) throws Exception
	{
		VitrineLv vitrineLv = new VitrineLv();
		vitrineLv.setId(1);
		vitrineLv.setAtivo("1");
		vitrineLv.setDataInicio(new Date());
		
		Calendar dataFinalCalendar = Calendar.getInstance();
		dataFinalCalendar.setTime(new Date());
		dataFinalCalendar.set(Calendar.YEAR, 2015);
		
		vitrineLv.setDataFinal(dataFinalCalendar.getTime());
		vitrineLv.setDescricao("");
		vitrineLv.setHtmlCustomizado("");
		vitrineLv.setImagem("");
		vitrineLv.setMarca(null);
		vitrineLv.setNome("Vitrine Teste");
		vitrineLv.setNumeroColunas(4);
		vitrineLv.setOrderExibicao(1);
		vitrineLv.setPaginacao("0");
		vitrineLv.setRandomizar("0");
		vitrineLv.setTipoExibicao("1");
		
		new IntegracaoVitrine("integracao","woo5#7").integrarVitrine(vitrineLv);
	}
	
	public void integrarVitrine(VitrineLv vitrineLv) throws Exception
	{
		try
		{
			if(vitrineLv == null)
				throw new Exception("Vitrine Loja Virtual não pode ser nulo");
			if(vitrineLv.getId() == null)
				throw new Exception("Vitrine Loja Virtual é inválida");
			if(vitrineLv.getNome().length() < 1)
				throw new Exception("Nome da Vitrine é inválido");
			if(vitrineLv.getTipoExibicao().length() < 1)
				throw new Exception("Tipo de Exibição da Vitrine é inválido");
			if(vitrineLv.getTipoExibicao() == "3" && vitrineLv.getMarca() == null)
				throw new Exception("Vitrine definida como HotSite deve conter uma Marca");
			
			SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd");
			SimpleDateFormat hourFormatter = new SimpleDateFormat("hh:mm:ss");
			
			ListGroupAddUpdate listGroupAdd = new ListGroupAddUpdate();
			listGroupAdd.setDsCustomHTML(vitrineLv.getHtmlCustomizado());
			listGroupAdd.setDsDescription(vitrineLv.getDescricao());
			listGroupAdd.setDsImage(vitrineLv.getImagem());
			listGroupAdd.setDsName(vitrineLv.getNome());
			listGroupAdd.setDtEnd(dateFormatter.format(vitrineLv.getDataFinal()));
			listGroupAdd.setDtStart(dateFormatter.format(vitrineLv.getDataInicio()));
			listGroupAdd.setFgActive(Integer.parseInt(vitrineLv.getAtivo()));
			listGroupAdd.setFgAllowPaging(Integer.parseInt(vitrineLv.getPaginacao()));
			listGroupAdd.setFgRandomize(Integer.parseInt(vitrineLv.getRandomizar()));
			listGroupAdd.setHourEnd(hourFormatter.format(vitrineLv.getDataFinal()));
			listGroupAdd.setHourStart(hourFormatter.format(vitrineLv.getDataInicio()));
			listGroupAdd.setIdCatalog(1);
			listGroupAdd.setIdListGroup(vitrineLv.getId().toString());
			listGroupAdd.setIdListGroupType(Integer.parseInt(vitrineLv.getTipoExibicao()));
			//listGroupAdd.setIdProductBrandErp(vitrineLv.getMarca() == null ? null : vitrineLv.getMarca().getId());
			listGroupAdd.setIdProductBrandErp(1);
			listGroupAdd.setNuColumns(vitrineLv.getNumeroColunas());
			listGroupAdd.setNuOrder(vitrineLv.getOrderExibicao());
			
			String xmlToSend = this.desenvXMLUtil.objectToXML(listGroupAdd, ListGroupAddUpdate.class);
			String receivedXML = this.catalogSoap.listGroupAddUpdate(xmlToSend, this.user, this.password);
			
			ReturnData returnData = this.desenvXMLUtil.XMLToObjectReturnData(receivedXML);
			
			if(returnData.getIdRet() == 0)
				throw new Exception(returnData.getDsErr());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}
	
	public void apagarVitrine(VitrineLv vitrineLv) throws Exception
	{
		try
		{
			if(vitrineLv == null)
				throw new Exception("Vitrine Loja Virtual não pode ser nulo");
			if(vitrineLv.getId() == null || vitrineLv.getId() == 0)
				throw new Exception("Vitrine Loja Virtual inválida");
			
			ListGroupRemove listGroupRemove = new ListGroupRemove();
			listGroupRemove.setIdListGroup(vitrineLv.getId().toString());
			
			String xmlToSend = this.desenvXMLUtil.objectToXML(listGroupRemove, ListGroupRemove.class);
			String receivedXML = this.catalogSoap.listGroupRemove(xmlToSend, this.user, this.password);
			
			ReturnData returnData = this.desenvXMLUtil.XMLToObjectReturnData(receivedXML);
			
			if(returnData.getIdRet() == 0)
				throw new Exception(returnData.getDsErr());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}
	
	public void integrarVitrineProduto(VitrineLvProduto vitrineLvProduto) throws Exception
	{
		try
		{
			if(vitrineLvProduto == null)
				throw new Exception("Vitrine Loja Virtual Produto não pode ser nulo");
			if(vitrineLvProduto.getId() == null)
				throw new Exception("Vitrine Loja Virtual Produto é inválida");
			if(vitrineLvProduto.getEstoqueProduto() == null)
				throw new Exception("Informe um Produto válido");
			
			ListGroupProductAdd listGroupProductAdd = new ListGroupProductAdd();
			
			listGroupProductAdd.setFgHighLight(Integer.parseInt(vitrineLvProduto.getProdutoDestaque()));
			listGroupProductAdd.setIdListGroup(vitrineLvProduto.getVitrineLv().getId().toString());
			listGroupProductAdd.setIdProduct(vitrineLvProduto.getEstoqueProduto().getProduto().getCodigoProduto() + "_" + 
			vitrineLvProduto.getEstoqueProduto().getOrdemProduto().getCodigo().toString() + "_" +
			vitrineLvProduto.getEstoqueProduto().getCor().getCodigo());
			listGroupProductAdd.setNuDispOrder(vitrineLvProduto.getOrdemExibicao());
			
			String xmlToSend = this.desenvXMLUtil.objectToXML(listGroupProductAdd, ListGroupProductAdd.class);
			String receivedXML = this.catalogSoap.listGroupProductAdd(xmlToSend, this.user, this.password);
			
			ReturnData returnData = this.desenvXMLUtil.XMLToObjectReturnData(receivedXML);
			
			if(returnData.getIdRet() == 0)
				throw new Exception(returnData.getDsErr());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}
	
	public void apagarVitrineProduto(VitrineLvProduto vitrineLvProduto) throws Exception
	{
		try
		{
			if(vitrineLvProduto == null)
				throw new Exception("Vitrine Loja Virtual Produto não pode ser nulo");
			if(vitrineLvProduto.getEstoqueProduto() == null)
				throw new Exception("Produto não pode ser nulo");
			if(vitrineLvProduto.getVitrineLv() == null)
				throw new Exception("Vitrine Loja Virtual não pode ser nulo");
			
			ListGroupProductRemove listGroupProductRemove = new ListGroupProductRemove();
			listGroupProductRemove.setIdListGroup(vitrineLvProduto.getVitrineLv().getId().toString());
			listGroupProductRemove.setIdProduct(vitrineLvProduto.getEstoqueProduto().getProduto().getCodigoProduto() + "_" + 
					vitrineLvProduto.getEstoqueProduto().getOrdemProduto().getCodigo().toString() + "_" +
					vitrineLvProduto.getEstoqueProduto().getCor().getCodigo());
			
			String xmlToSend = this.desenvXMLUtil.objectToXML(listGroupProductRemove, ListGroupProductRemove.class);
			String receivedXML = this.catalogSoap.listGroupProductRemove(xmlToSend, this.user, this.password);
			
			ReturnData returnData = this.desenvXMLUtil.XMLToObjectReturnData(receivedXML);
			
			if(returnData.getIdRet() == 0)
				throw new Exception(returnData.getDsErr());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}
	
	public void integrarVitrineDepartamento(VitrineLvDepartamento vitrineLvDepartamento) throws Exception
	{
		try
		{
			if(vitrineLvDepartamento == null)
				throw new Exception("Vitrine Loja Virtual Produto não pode ser nulo");
			if(vitrineLvDepartamento.getId() == null)
				throw new Exception("Vitrine Loja Virtual Produto é inválida");
			if(vitrineLvDepartamento.getDepartamento() == null)
				throw new Exception("Informe um Departamento válido");
			
			ListGroupDeptAdd listGroupDeptAdd = new ListGroupDeptAdd();
			listGroupDeptAdd.setIdListGroup(vitrineLvDepartamento.getVitrineLv().getId().toString());
			listGroupDeptAdd.setIdDept(vitrineLvDepartamento.getDepartamento().getId());
			
			String xmlToSend = this.desenvXMLUtil.objectToXML(listGroupDeptAdd, ListGroupProductAdd.class);
			String receivedXML = this.catalogSoap.listGroupDeptAdd(xmlToSend, this.user, this.password);
			
			ReturnData returnData = this.desenvXMLUtil.XMLToObjectReturnData(receivedXML);
			
			if(returnData.getIdRet() == 0)
				throw new Exception(returnData.getDsErr());
		}
		catch(Exception ex)
		{
			 ex.printStackTrace();
			 throw ex;
		}
	}
	
	public void apagarVitrineDepartamento(VitrineLvDepartamento vitrineLvDepartamento) throws Exception
	{
		try
		{
			if(vitrineLvDepartamento == null)
				throw new Exception("Vitrine Loja Virtual Produto não pode ser nulo");
			if(vitrineLvDepartamento.getId() == null)
				throw new Exception("Vitrine Loja Virtual Produto é inválida");
			if(vitrineLvDepartamento.getDepartamento() == null)
				throw new Exception("Informe um Departamento válido");
			
			ListGroupDeptRemove listGroupDeptRemove = new ListGroupDeptRemove();
			listGroupDeptRemove.setIdDept(vitrineLvDepartamento.getDepartamento().getId());
			listGroupDeptRemove.setIdListGroup(vitrineLvDepartamento.getVitrineLv().getId().toString());
			
			String xmlToSend = this.desenvXMLUtil.objectToXML(listGroupDeptRemove, ListGroupDeptRemove.class);
			String receivedXML = this.catalogSoap.listGroupDeptRemove(xmlToSend, this.user, this.password);
			
			ReturnData returnData = this.desenvXMLUtil.XMLToObjectReturnData(receivedXML);
			
			if(returnData.getIdRet() == 0)
				throw new Exception(returnData.getDsErr());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}
}
