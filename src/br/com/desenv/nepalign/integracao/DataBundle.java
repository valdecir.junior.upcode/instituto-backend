package br.com.desenv.nepalign.integracao;

import java.util.Date;

import javax.persistence.EntityManager;

import br.com.desenv.nepalign.integracao.dto.DataPackage;

public interface DataBundle 
{
	public DataPackage exportar(final EntityManager manager, final DataPackage dataPackage, final Date dataInicial, final Date dataFinal) throws Exception;
}