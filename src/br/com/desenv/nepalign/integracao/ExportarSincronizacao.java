package br.com.desenv.nepalign.integracao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.transaction.Transaction;

import br.com.desenv.nepalign.model.OperacaoMovimentacaoEstoque;
import br.com.desenv.nepalign.service.OperacaoMovimentacaoEstoqueService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ExportarSincronizacao
{
    public static void main(String[] args) throws Exception
    {

	String usuarioBackup = "root";
	String senhaUsuarioBackup = "vitoria7";
	String nomeBd = "nepalign";
	String listaTabelas = "operacaoMovimentacaoEstoque";
	String caminhoMysql = "C:\\Program Files\\MySQL\\MySQL Server 5.6\\bin";
	String caminhoArquivo = "c:/Desenv/Exportacao";
	String ipServidorMysql = "192.168.1.104";
	Process runtimeProcess;
	EntityManager manager=null;
	EntityTransaction transaction = null;
	
	
	
	
	SimpleDateFormat sdfArquivo = new SimpleDateFormat("yyyyMMdd_hhmmss");
	Integer idEmpresaFisica = DsvConstante.getParametrosSistema().getInt("EMPRESA_LOCAL");
	String prefixoArquivo = (idEmpresaFisica>9?""+idEmpresaFisica:""+idEmpresaFisica)+"0"+sdfArquivo.format(new Date()); 
	OperacaoMovimentacaoEstoqueService operacaoMovimentacaoEstoqueService = new OperacaoMovimentacaoEstoqueService();
	OperacaoMovimentacaoEstoque operacaoMovimentacaoEstoqueFiltro = new OperacaoMovimentacaoEstoque();
	operacaoMovimentacaoEstoqueFiltro.setFlagSincronizacao("0");
	List<OperacaoMovimentacaoEstoque> listaOperacaoMovimentacaoEstoque = operacaoMovimentacaoEstoqueService.listarPorObjetoFiltro(operacaoMovimentacaoEstoqueFiltro, "idOperacao", "asc");
	 //==> Versao
	try
	{
		manager = ConexaoUtil.getEntityManager();
		transaction = manager.getTransaction();
		transaction.begin();	    
	    
        	FileWriter arquivoSaida = new FileWriter("c://Desenv/Exportacao/"+prefixoArquivo+"_backup_registro.sql");
        	BufferedWriter bufferSaida = new BufferedWriter(arquivoSaida);
        
        	String comandoMysql = "\""+caminhoMysql+"\\"+"mysqldump\" -h" + ipServidorMysql + " -u " + usuarioBackup + " -p"+senhaUsuarioBackup+ " " + nomeBd + " " + listaTabelas; // + " >" + caminhoArquivo+"\\"+prefixoArquivo + "_sinc" ;
        	runtimeProcess = Runtime.getRuntime().exec(comandoMysql); 
             
        	 StreamGobbler sgInput = new StreamGobbler(  
        		 runtimeProcess.getInputStream(), "input", new File( caminhoArquivo+"\\"+prefixoArquivo + "_sinc") );  
        	        StreamGobbler sgError = new StreamGobbler(  
        	        	runtimeProcess.getErrorStream(), "error" );  
        	  
        	        // cria uma thread para cada stream gobbler e as inicia  
        	        new Thread( sgInput ).start();  
        	        new Thread( sgError ).start();  	
        	
                int processCompleted = runtimeProcess.waitFor();
        
                if(processCompleted == 0)
                {
                    System.out.println("Dump done!");
                }
                else
                {
                    System.out.println("Error doing dump!");
                }	
                
        	for (OperacaoMovimentacaoEstoque operacaoMovimentacaoEstoque : listaOperacaoMovimentacaoEstoque)
        	{
        	    bufferSaida.write("update nepalign.operacaomovimentacaoestoque set flagSincronizacao = '0' where idOperacao = " + operacaoMovimentacaoEstoque.getId() + ";");
        	    bufferSaida.newLine();
        	    operacaoMovimentacaoEstoque.setFlagSincronizacao("1");
        	    operacaoMovimentacaoEstoqueService.atualizar(operacaoMovimentacaoEstoque, false);
        	    
        	}
        	
        	bufferSaida.close();
        	arquivoSaida.close();
        	transaction.commit();
	}
	catch(Exception ex)
	{
	    if (transaction!=null)
		transaction.rollback();
	}
    }

}    
    /** 
    * Thread para consumir os streams de processos. 
    * Baseada na implementação apresentada em: 
    * http://www.javaworld.com/javaworld/jw-12-2000/jw-1229-traps.html?page=1 
    * 
    * @author David Buzatto 
    */  
    class StreamGobbler implements Runnable {  
      
        private InputStream is;  
        private String type;  
        private FileWriter fw;  
      
        public StreamGobbler( InputStream is, String type ) {  
            this.is = is;  
            this.type = type;  
        }  
      
        public StreamGobbler( InputStream is, String type, File file )  
                throws IOException {  
            this.is = is;  
            this.type = type;  
            this.fw = new FileWriter( file );  
        }  
      
        @Override  
        public void run() {  
            try {  
                InputStreamReader isr = new InputStreamReader( is );  
                BufferedReader br = new BufferedReader( isr );  
                String line = null;  
                while ( ( line = br.readLine() ) != null ) {  
                    if ( fw != null ) {  
                        fw.write( line + "\n" );  
                    } else {  
                        System.out.println( type + ">" + line );  
                    }  
                }  
                if ( fw != null ) {  
                    fw.close();  
                }  
            } catch ( IOException ioe ) {  
                ioe.printStackTrace();  
            }  
        }      
    }