package br.com.desenv.nepalign.integracao;

import br.com.desenv.nepalign.model.Cor;
import br.com.desenv.nepalign.model.Produto;


public class DsvProdutoAux 
{
	private Produto produto;
	private int quantidadeTotal;
	//private EstoqueCodigoBarras estoqueCodigoBarras;
	private Cor cor;
	public double getValorProduto() {
		return valorProduto;
	}
	public void setValorProduto(double valorProduto) {
		this.valorProduto = valorProduto;
	}
	private double valorProduto;
	
	public Cor getCor() 
	{
		return cor;
	}
	public void setCor(Cor cor) 
	{
		this.cor = cor;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public int getQuantidadeTotal() {
		return quantidadeTotal;
	}
	public void setQuantidadeTotal(int quantidadeTotal) {
		this.quantidadeTotal = quantidadeTotal;
	}
	//public EstoqueCodigoBarras getEstoqueCodigoBarras() {
		//return estoqueCodigoBarras;
	//}
	//public void setEstoqueCodigoBarras(EstoqueCodigoBarras estoqueCodigoBarras) {
		//this.estoqueCodigoBarras = estoqueCodigoBarras;
	//}
	
	
}
