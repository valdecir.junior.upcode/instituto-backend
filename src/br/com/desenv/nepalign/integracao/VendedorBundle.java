package br.com.desenv.nepalign.integracao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.nepalign.integracao.dto.DataPackage;
import br.com.desenv.nepalign.integracao.dto.UsuarioDTO;
import br.com.desenv.nepalign.integracao.dto.VendedorDTO;
import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.nepalign.model.Vendedor;

import flex.messaging.io.ArrayCollection;
import flex.messaging.io.amf.client.AMFConnection;

public class VendedorBundle implements DataBundle
{
	private List<Integer> usuariosIntegrados;
	
	public VendedorBundle()
	{
		usuariosIntegrados = new ArrayList<Integer>();
	}
	
	public void importarUsuario(AMFConnection con, EntityManager manager, EntityTransaction transaction, String idEmpresaFisica, Usuario usuario_) throws Exception
	{
		if(usuario_ == null)
			return;
		if(usuariosIntegrados.contains(usuario_.getId().intValue()))
			return;

		//manager.createNativeQuery("DELETE FROM " + Util.getDatabaseName(idEmpresaFisica) + ".usuario WHERE idUsuario = " + usuario_.getId().intValue() + ";").executeUpdate();
		manager.merge(Util.<UsuarioDTO> copy(UsuarioDTO.class, usuario_.clone()));
	}
	
	public void importar(AMFConnection con, EntityManager manager, EntityTransaction transaction, String idEmpresaFisica) throws Exception
	{
		System.out.println(">Iniciando Integracao Vendedores...");
		
		String databaseName = Util.getDatabaseName(idEmpresaFisica);
		
		System.out.println(">>Importando usuarios..");
		
		ArrayCollection listaUsuariosBuffer = (ArrayCollection) con.call(Util.SERVICE_GET_USERS);
		
		for(Object usr : listaUsuariosBuffer)
		{
			manager.createNativeQuery("DELETE FROM " + databaseName + ".usuario WHERE idUsuario = " + ((Usuario) usr).getId().intValue()).executeUpdate();
			manager.merge(Util.<UsuarioDTO>copy(UsuarioDTO.class, ((Usuario) usr).clone()));
			
			usuariosIntegrados.add(((Usuario) usr).getId().intValue());
		}
		
		System.out.println(">>>Usuarios importados com sucesso!");
		
		ArrayCollection listaVendedores = (ArrayCollection) con.call(Util.SERVICE_GET_SALLERS);
		
		System.out.println(">>Vendedores filtrados com sucesso! " + listaVendedores.size() + " Vendedores a serem integradas!");
		
		if(listaVendedores.size() > 0x00)
			manager.createNativeQuery("DELETE FROM " + databaseName + ".vendedor;").executeUpdate();
		
		System.out.println(">>Importando...");
		 
		for(Object obj : listaVendedores)
		{
			try
			{
				Vendedor vendedor = ((Vendedor) obj).clone();
				
				if(vendedor.getId() == null) // ACORDO
					vendedor.setId(0x00);

				manager.merge(Util.<VendedorDTO>copy(VendedorDTO.class, ((Vendedor) obj).clone()));
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}	
		
		System.out.println(">>>Integracao Vendedores Concluida!");
	}
	
	@SuppressWarnings("unchecked")
	public DataPackage exportar(final EntityManager manager, final DataPackage dataPackage, final Date dataInicial, final Date dataFinal) throws Exception
	{
		final String usuarioQuery = "select * from usuario ";
		final String vendedorQuery = "select * from vendedor ";
		
		dataPackage.<UsuarioDTO> put(UsuarioDTO.class, (List<UsuarioDTO>) manager.createNativeQuery(usuarioQuery, UsuarioDTO.class).getResultList());
		dataPackage.<VendedorDTO> put(VendedorDTO.class, (List<VendedorDTO>) manager.createNativeQuery(vendedorQuery, VendedorDTO.class).getResultList());
		
		return dataPackage;
	}
}