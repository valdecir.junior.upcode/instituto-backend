package br.com.desenv.nepalign.integracao;

import java.util.List;

import br.com.desenv.nepalign.model.Departamento;
import br.com.desenv.nepalign.model.DepartamentoProduto;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.model.SituacaoEnvioLV;
import br.com.desenv.nepalign.service.DepartamentoProdutoService;
import br.com.desenv.nepalign.service.DepartamentoService;
import br.com.desenv.nepalign.service.ProdutoService;

public class DsvUtilGeraDepartamentoProduto 
{

	public void init() throws Exception
	{
		try
		{
			ProdutoService pService = new ProdutoService();
			Produto p = new Produto();
			DepartamentoService dService = new DepartamentoService();
			Departamento d = new Departamento();
			DepartamentoProduto dp;
			DepartamentoProdutoService dpService = new DepartamentoProdutoService();

			SituacaoEnvioLV sitLV = new SituacaoEnvioLV();
			sitLV.setId(2);
			
			Produto pCriterio = new Produto();
			pCriterio.setProdutoLV("S");
			pCriterio.setSituacaoEnvioLV(sitLV);
			List<Produto> listaProdutos = pService.listarPorObjetoFiltro(pCriterio, "idProduto", "ASC");
			
			List<Departamento> listaDepartamento;
			List<DepartamentoProduto> listaDepartamentoProduto;
			
			for (Produto produto : listaProdutos) 
			{
				d.setIdSubDepCache(produto.getGrupo().getCodigo());
				listaDepartamento = dService.listarPorObjetoFiltro(d);
				if (listaDepartamento.size()>0)
				{
					for (Departamento departamento : listaDepartamento) 
					{
						//TENIS FICA NO FEMININO E NO MASCULINO
						dp = new DepartamentoProduto();
						dp.setDepartamento(departamento);
						dp.setProduto(produto);
						listaDepartamentoProduto = dpService.listarPorObjetoFiltro(dp);
						if (listaDepartamentoProduto.size()==0)
						{
							dp.setSituacaoEnvioLV(sitLV);
							dpService.salvar(dp);
							departamento.setSituacaoEnvioLV(sitLV);
							dService.atualizar(departamento);
						}						
						else
						{

							dp = listaDepartamentoProduto.get(0);
							if (dp.getSituacaoEnvioLV().getId().intValue()!=3)
							{
								dp.setSituacaoEnvioLV(sitLV);
								dpService.atualizar(dp);
							}
						}
					}
				}
				else
				{
					System.out.println("Produto sem departamento equivalente: " + produto.getCodigoProduto() + " " + produto.getNomeProduto());
				}	
			}			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) throws Exception
	{
		DsvUtilGeraDepartamentoProduto d = new DsvUtilGeraDepartamentoProduto();
		d.init();
	}
}
