package br.com.desenv.nepalign.integracao;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.model.SituacaoEnvioLV;
import br.com.desenv.nepalign.service.ProdutoService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class DsvUtilArquivoReferencia 
{

	public static void main(String args[]) throws Exception
	{
		DsvUtilArquivoReferencia dsv = new DsvUtilArquivoReferencia();
		dsv.initRecuperaProdutosParaSubir();
		//dsv.validaPreco();
		//dsv.buscaNomeProdutos();
	}
	
	public void initValidaListaReferencias() throws Exception
	{
		
		String arquivo = "C:/desenv/Produtos20131231.csv";
		FileReader reader = null;
		BufferedReader bufferedReader = null;
		String linha = null;
		
		try
		{
			reader = new FileReader(arquivo);
		}
		catch(FileNotFoundException fnfe)
		{
			throw new Exception(fnfe);	
		}
		
		bufferedReader = new BufferedReader(reader);	
		HashMap<String, String> referencias = new HashMap<>();
		String valores[];
		int contLinhas = 0;
		try
		{ //41 
			while (bufferedReader.ready())
			{
				linha = bufferedReader.readLine();
				contLinhas++;
				if (linha.trim().equals("")==false)
				{
					valores = linha.split("_");
					if (valores.length>1)
					{
						referencias.put(valores[0], linha);
					}
				}
			}
			bufferedReader.close();
			System.out.println("Linhas :" + contLinhas);
			
			Iterator<String> i = referencias.keySet().iterator();
			while (i.hasNext()) 
			{
				String ref = (String) i.next();
				System.out.println(ref);
			}
			System.out.println("Referencias: " + referencias.size());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	
	public void initRecuperaProdutosParaSubir() throws Exception
	{
		
		String arquivo = "C:/desenv/manamaue/Produtos20131231.csv";
		FileReader reader = null;
		BufferedReader bufferedReader = null;
		String linha = null;
		Produto produto = new Produto();
		ProdutoService pService = new ProdutoService();
		
		
		try
		{
			reader = new FileReader(arquivo);
		}
		catch(FileNotFoundException fnfe)
		{
			throw new Exception(fnfe);	
		}
		
		bufferedReader = new BufferedReader(reader);	
		HashMap<String, String> referencias = new HashMap<>();
		String referencia;
		SituacaoEnvioLV sit = new SituacaoEnvioLV();
		sit.setId(2);
		int contLinhas = 0;
		try
		{ //41 
			while (bufferedReader.ready())
			{
				linha = bufferedReader.readLine();
				contLinhas++;
				if (linha.trim().equals("")==false)
				{
					try
					{
						referencia = linha;
						produto = pService.recuperarPorId(new Integer(referencia));
						produto.setSituacaoEnvioLV(sit);
						produto.setProdutoLV("S");
						pService.atualizar(produto);
					}
					catch(Exception ex)
					{
						System.out.println("Erro na linha " + contLinhas + ", referencia " + linha);
					}
				}
			}
			bufferedReader.close();
			System.out.println("Referencias: " + contLinhas);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}	
	public void validaPreco() throws Exception
	{
		
		String arquivo = "C:/desenv/tmp/precosProduto.rpt";
		FileReader reader = null;
		BufferedReader bufferedReader = null;
		String linha = null;
		
		try
		{
			reader = new FileReader(arquivo);
		}
		catch(FileNotFoundException fnfe)
		{
			throw new Exception(fnfe);	
		}
		
		bufferedReader = new BufferedReader(reader);	
		HashMap<String, String> referencias = new HashMap<>();
		HashMap<String, String> precoDiferente = new HashMap<>();
		
		String valores[];
		String valores2[];
		String preco;
		String outroPreco;
		int contLinhas = 0;
		try
		{ //41
			EntityManager manager = ConexaoUtil.getEntityManager();
			EntityTransaction transaction = manager.getTransaction();	
			transaction.begin();
			while (bufferedReader.ready())
			{
				linha = bufferedReader.readLine();
				contLinhas++;
				if (linha.trim().equals("")==false)
				{
					
					valores2 = linha.split("       ");
					valores = valores2[0].trim().split("_");
					if (valores.length>1)
					{
						preco = valores2[1].trim();
						if (referencias.containsKey(valores[0]))
						{
							outroPreco = referencias.get(valores[0]);
							if (outroPreco.equals(preco)==false)
							{
								System.out.println(valores[0] + " " + preco + " " + outroPreco);
								if (precoDiferente.containsKey(valores[0])==false)
									precoDiferente.put(valores[0], preco);
							}
						}
						referencias.put(valores[0], preco);
					}
				}
			}
			bufferedReader.close();
			System.out.println("Linhas :" + contLinhas);
			ProdutoService pService = new ProdutoService();
			SituacaoEnvioLV sit = new SituacaoEnvioLV();
			sit.setId(2);
			Produto p;
			contLinhas=0;
			Iterator<String> i = precoDiferente.keySet().iterator();
			while (i.hasNext()) 
			{
				String ref = (String) i.next();
				p = pService.recuperarPorId(manager, new Integer(ref));
				p.setSituacaoEnvioLV(sit);
				pService.atualizar(manager, transaction, p);
				System.out.println(ref);
			}
			transaction.commit();
			manager.close();
			System.out.println("Referencias: " + precoDiferente.size());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}	
	}
	public Vector<String> buscaNomeProdutos() throws Exception
	{
		
		String arquivo = "C:/desenv/tmp/arquivos.txt";
		FileReader reader = null;
		BufferedReader bufferedReader = null;
		String linha = null;
		Vector<String> nomeArquivos = new Vector<String>();
		
		try
		{
			reader = new FileReader(arquivo);
		}
		catch(FileNotFoundException fnfe)
		{
			throw new Exception(fnfe);	
		}
		
		bufferedReader = new BufferedReader(reader);	
		HashMap<String, String> referencias = new HashMap<>();
		HashMap<String, String> precoDiferente = new HashMap<>();
		
		String valores[];
		String valores2[];
		int contLinhas = 0;
		String strArquivo;
		try
		{ //41

			while (bufferedReader.ready())
			{
				linha = bufferedReader.readLine();
				contLinhas++;
				if (linha.trim().equals("")==false)
				{
					
					valores2 = linha.split("       ");
					valores = valores2[1].trim().split(" ");
					if (valores.length>1)
					{
						strArquivo = valores[1].trim();
						nomeArquivos.add(strArquivo);
					}
				}
			}
			bufferedReader.close();

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return nomeArquivos;
	}	
}
