package br.com.desenv.nepalign.integracao;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import br.com.desenv.frameworkignorante.GenericModelIGN;
import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.wsdl.SessionInfo;

public class Util 
{
	//static final boolean DEBUG_MODE = !System.getProperty("java.vm.info", "").contains("sharing");
	static final boolean DEBUG_MODE = false;
	static final String URL_AMF_CENTRAL = "http://200.150.73.67:33388/nepalign/messagebroker/amf/";
	
	static final String SERVICE_AUTH_USER = "usuarioService.colocarDadosFlexSession";
	static final String SERVICE_GET_SALES = "pedidoVendaService.listarPorObjetoFiltro";
	static final String SERVICE_GET_SALE_ITEMS = "itemPedidoVendaService.listarPorObjetoFiltro";
	static final String SERVICE_GET_USERS = "usuarioService.listar";
	static final String SERVICE_GET_SALLERS = "vendedorService.listar";
	static final String SERVICE_GET_PB = "caixaDiaService.listarPorObjetoFiltro";
	static final String SERVICE_GET_PB_ACTIVITY = "movimentoCaixaDiaService.listarPorObjetoFiltro";
	static final String SERVICE_GET_PB_SALE_PAYMENT = "pagamentoPedidoVendaService.listarPorObjetoFiltro";
	static final String SERVICE_GET_PB_TERM_SALE_ACTIVITY = "movimentoParcelaDuplicataService.listarPorObjetoFiltro";
	static final String SERVICE_GET_TERM_SALE_ONLY = "duplicataService.recuperarPorId";
	static final String SERVICE_GET_TERM_SALE = "duplicataService.listarPorObjetoFiltro";
	static final String SERVICE_GET_TERM_SALE_ITEMS = "duplicataParcelaService.listarPorObjetoFiltro";
	static final String SERVICE_GET_CUSTOMER = "clienteService.recuperarPorId";
	static final String SERVICE_GET_CUSTOMER_INVITE = "historicoCobrancaService.listarPorObjetoFiltro";
	static final String SERVICE_GET_CUSTOMER_EMAIL_ADDRESSES = "emailClienteService.listarPorObjetoFiltro";
	static final String SERVICE_GET_PB_VALUES = "valorFechamentoCaixaDiaService.listarPorObjetoFiltro";
	static final String SERVICE_GET_CHEQUE = "chequeRecebidoService.recuperarPorId";
	static final String SERVICE_GET_TERM_DEAL = "duplicataAcordoService.listarPorObjetoFiltro";
	
	public static final ClienteBundle clienteBundle = new ClienteBundle();
	public static final VendedorBundle vendedorBundle = new VendedorBundle();
	public static final DuplicataBundle duplicataBundle = new DuplicataBundle();
	public static final PedidoVendaBundle pedidoVendaBundle = new PedidoVendaBundle();
	public static final MovimentoLojaBundle movimentoLojaBundle = new MovimentoLojaBundle();
	public static final ChequeRecebidoBundle chequeRecebidoBundle = new ChequeRecebidoBundle();
	
	public static Usuario getSysUser()
	{
		Usuario sysUser = new Usuario();
		sysUser.setId(0x01);
		sysUser.setUsuarioDesenv("desenv@master");
		
		return sysUser;
	}
	
	public static final String getUrlAmfCentral()
	{
		try 
		{
			return DsvConstante.getParametrosSistema().get("URL_AMF_CHANNEL_CENTRAL");
		}
		catch (Exception e) 
		{
			System.err.println("Não foi possível recuperar a URL do AMF Central!");
			return null;
		}
	}
	
	public static SessionInfo generateCredentials()
	{
		SessionInfo userCredentials = new SessionInfo();
		userCredentials.setTimezoneOffset(Calendar.getInstance().getTimeZone().getOffset(new Date().getTime()));
		userCredentials.setUsuario(getSysUser());
		
		return userCredentials;
	}
	
	public static String getDatabaseName(String idEmpresaFisica)
	{
		return "nepalignloja".concat(idEmpresaFisica);
	}
	
	public static String getAmfUrl(String idEmpresaFisica)
	{
		switch(Integer.parseInt(idEmpresaFisica))
		{
			case 0x01:
				return "http://f01-londrina.no-ip.org:33388/nepalign/messagebroker/amf/";
			case 0x02:
				return "http://f02-paranavai.no-ip.org:33388/nepalign/messagebroker/amf/";
			case 0x03:
				return "http://f03-apucarana.no-ip.org:33388/nepalign/messagebroker/amf/";
			case 0x04:
				return "http://f04-umuarama.no-ip.org:33388/nepalign/messagebroker/amf/";
			case 0x05:
				return "http://f05-novaesperanca.no-ip.org:33388/nepalign/messagebroker/amf/";
			case 0x06:
				return "http://f06-loanda.no-ip.org:33388/nepalign/messagebroker/amf/";
			case 0x07:
				return "http://f07.upcode.duia.us:8080/nepalign/messagebroker/amf/"; // IPv6 PROTOCOL 
			case 0x08:
				return "http://f08-paranavai.no-ip.org:33388/nepalign/messagebroker/amf/";
			case 0x09:
				return "http://f09-arapongas.no-ip.org:33388/nepalign/messagebroker/amf/";
			case 0x0A:
				return "http://f10-marialva.no-ip.org:33388/nepalign/messagebroker/amf/";
			case 0x0D:
				return "http://f13-paranavai.no-ip.org:33388/nepalign/messagebroker/amf/";
			default:
				return null;
		}
	}
	
	public static String getDatabaseUrl(Integer idEmpresaFisica)
	{
		switch(idEmpresaFisica)
		{
			case 0x01:
				return "jdbc:mysql://f01-londrina.no-ip.org:33306/nepalignloja01";
			case 0x02:
				return "jdbc:mysql://f02-paranavai.no-ip.org:33306/nepalignloja02";
			case 0x03:
				return "jdbc:mysql://f03-apucarana.no-ip.org:3306/nepalignloja03";
			case 0x04:
				return "jdbc:mysql://f04-umuarama.no-ip.org:3306/nepalignloja04";
			case 0x05:
				return "jdbc:mysql://f05-novaesperanca.no-ip.org:33306/nepalignloja05";
			case 0x06:
				return "jdbc:mysql://f06-loanda.no-ip.org:33306/nepalignloja06"; 
			case 0x07:
				return "jdbc:mysql://serallecornelio.ddns.me:3306/nepalignloja07";
			case 0x08:
				return "jdbc:mysql://f08-paranavai.no-ip.org:33306/nepalignloja08";
			case 0x09:
				return "jdbc:mysql://f09-arapongas.no-ip.org:33306/nepalignloja09";
			case 0x0A:
				return "jdbc:mysql://f10-marialva.no-ip.org:33306/nepalignloja10";
			case 0x0D: 
				return "jdbc:mysql://f13-paranavai.no-ip.org:33306/nepalignloja13";
			default:
				return null;
		}
	}
	
	public static Date addDay(Date date, int days)
	{
		Calendar buffer = Calendar.getInstance();
		buffer.setTime(date);
		buffer.add(Calendar.DAY_OF_YEAR, days);
		
		return buffer.getTime();
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T copy(Class<T> typeDef, Object obj) throws Exception
	{	
		@SuppressWarnings("rawtypes") Constructor constructorToUse = null;
		
		if(typeDef == List.class)
			constructorToUse = ArrayList.class.getConstructor();
		
		if(constructorToUse == null)
		{
		    for (@SuppressWarnings("rawtypes") Constructor constructor : typeDef.getConstructors()) 
		    {
		    	if (constructor.getParameterTypes().length == 0x00) 
		    	{	
		    		constructorToUse = constructor;
		    		constructorToUse.setAccessible(true);
		    	}
		    }	
		}
	    
	    if (constructorToUse != null) 
	    {
	    	try 
	    	{
	    		Object objCopy = constructorToUse.newInstance();
	        
	    		for (Method method : obj.getClass().getMethods()) 
	    		{
	    			if(!method.getName().startsWith("get") || method.getName().equals("getClass"))
	    				continue;
	    			
	    			if(method.getReturnType().getSuperclass() == GenericModelIGN.class)
	    			{
	    				try
	    				{
	    					objCopy.getClass().getMethod(method.getName().replace("get", "set"), method.getReturnType()).getParameterTypes();	
	    				}
	    				catch(Exception ex)
	    				{ 
	    					Class<?>[] parameters = objCopy.getClass().getMethod(method.getName().replace("get", "set"), Integer.class).getParameterTypes();
	    					
	    					if(parameters.length > 0x00 && parameters[0x00] == Integer.class && ((GenericModelIGN) method.invoke(obj)) != null)
	    					{
		    					objCopy.getClass().getMethod(method.getName().replace("get", "set"), Integer.class).invoke(objCopy, ((GenericModelIGN) method.invoke(obj)).getId());	
		    					continue;
	    					}	
	    					else
	    						continue;
	    				}
	    			}
	    			
	    			try 
	    			{
	    				objCopy.getClass().getMethod(method.getName().replace("get", "set"), method.getReturnType()).invoke(objCopy, method.invoke(obj));
	    			} 
	    			catch (NoSuchMethodException ex) 
	    			{
	    				throw ex;
	    			}
	    		}
	    		return (T) objCopy;
	    	} 
	    	catch (Exception ex) 
	    	{
	    		throw ex;
	    	}
	    }
	    else
	    	throw new Exception("Class " + typeDef.getName() + " hasn't any constructor");
	}
	
	public static boolean isMovSyncRunning(String idEmpresaFisica) throws Exception
	{
		Set<Thread> threadSet = Thread.getAllStackTraces().keySet();

		Object[] threadArray = threadSet.toArray();
		
		for(Object _threadRunning : threadArray)
		{
			Thread threadRunning = (Thread) _threadRunning;
			
			if(threadRunning.getName().startsWith("THREAD-SYNC-MOV-".concat(idEmpresaFisica)))
				return true;
		}
		
		return false;
	}
	
	public static final boolean isBackupRunning() throws Exception
	{
		Set<Thread> threadSet = Thread.getAllStackTraces().keySet();

		Object[] threadArray = threadSet.toArray();
		
		for(Object _threadRunning : threadArray)
		{
			Thread threadRunning = (Thread) _threadRunning;
			
			if(threadRunning.getName().startsWith("BackupRunner"))
				return true;
		}
		
		return false;
	}
	
	public static final String getMovSyncRunningLog(String idEmpresaFisica) throws Exception
	{
		Set<Thread> threadSet = Thread.getAllStackTraces().keySet();

		Object[] threadArray = threadSet.toArray();
		
		for(Object _threadRunning : threadArray)
		{
			Thread threadRunning = (Thread) _threadRunning;
			
			if(threadRunning.getName().startsWith("THREAD-SYNC-MOV-".concat(idEmpresaFisica)))
			{
				return ((ThreadMovimento) threadRunning).getRunnerLog();
			}
		}
		
		return null;
	}
	
	public static boolean interruptMovSyncRunning(String idEmpresaFisica) throws Exception
	{
		Set<Thread> threadSet = Thread.getAllStackTraces().keySet();

		Object[] threadArray = threadSet.toArray();
		
		for(Object _threadRunning : threadArray)
		{
			Thread threadRunning = (Thread) _threadRunning;
			
			if(threadRunning.getName().startsWith("THREAD-SYNC-MOV-".concat(idEmpresaFisica)))
			{
				try
				{
					threadRunning.checkAccess();
					threadRunning.interrupt();
				}
				catch(SecurityException ex)
				{
					throw new Exception("Operação não pode ser cancelada! Tente novamente mais tarde!");
				}
				break;
			}
		}
		
		return false;
	}
}