package br.com.desenv.nepalign.integracao;

import java.io.EOFException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.integracao.dto.DataPackage;
import br.com.desenv.nepalign.model.ParametrosSistema;
import br.com.desenv.nepalign.service.ParametrosSistemaService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;
import flex.messaging.MessageException;
import flex.messaging.io.amf.client.exceptions.ClientStatusException;
import flex.messaging.io.amf.client.exceptions.ServerStatusException;

public class RunnableMovimentoExportar implements Runnable 
{
	private static final IgnUtil ignUtil = new IgnUtil();
	private static final Logger logger = Logger.getLogger(RunnableMovimentoExportar.class);	
	private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(0x01);
	
	private enum ExportStatus
	{
		RUNNING((short) 0x01),
		FINISHED((short) 0x02),
		ERROR((short) 0x03);
		
		private short code;
		
		ExportStatus(short code)
		{
			this.code = code;
		}
	}
	
	private enum ExportSteps
	{
		OPENING_CONNECTIONS((short) 0x00),
		RUNNING_PEDIDOVENDA_BUNDLE((short) 0x01),
		RUNNING_CLIENTE_BUNDLE((short) 0x02),
		RUNNING_DUPLICATAS_BUNDLE((short) 0x03),
		RUNNING_MOVIMENTOLOJA_BUNDLE((short) 0x04),
		RUNNING_DEPLOY((short) 0x05);
		
		private short code;
		
		ExportSteps(short code)
		{
			this.code = code;
		}
	}
	
	private final short MAX_ATTEMPTS	   = 0x05;
	private final short FAIL_ATTEMPT_DELAY = 0x1E;

	private static short FAILED_ATTEMPTS   = 0x00;
	
	private AMFConnector con;
	private EntityManager manager = ConexaoUtil.getEntityManager();
	
	private DataPackage dataPackage = new DataPackage();
	
	private Date dataInicial;
	private Date dataFinal;
	private Integer idEmpresaFisica;
	
	private final int[] situacoes = new int[ExportSteps.values().length];
	
	public RunnableMovimentoExportar()
	{
		super();
	}
	
	public RunnableMovimentoExportar(final Integer _idEmpresaFisica, final Date _dataInicial, final Date _dataFinal)
	{
		super();
		
		dataInicial = _dataInicial;
		dataFinal = _dataFinal;
		
		idEmpresaFisica = _idEmpresaFisica;
	}

	private void atualizarParametro(EntityManager manager, EntityTransaction transaction, String nomeParametro, String valor) throws Exception
	{
		Boolean transacaoIndependente = false;
		
		try
		{			
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			} 
			
			ParametrosSistema parametrosSistemaFiltro = new ParametrosSistema();
			parametrosSistemaFiltro.setDescricao(nomeParametro);
			
			ParametrosSistema parametroUltimaAtualizacao = null;
			
			ParametrosSistemaService parametrosSistemaService = new ParametrosSistemaService();
			
			List<ParametrosSistema> listaParametros = parametrosSistemaService.listarPorObjetoFiltro(manager, parametrosSistemaFiltro, null, null, null);
			
			if(listaParametros.size() == 0x01)
			{
				parametroUltimaAtualizacao = listaParametros.get(0x00);
				parametroUltimaAtualizacao.setValor(valor);
			}
			else if(listaParametros.size() == 0x00)
			{
				parametroUltimaAtualizacao = new ParametrosSistema();
				parametroUltimaAtualizacao.setDescricao(nomeParametro);
				parametroUltimaAtualizacao.setValor(valor);
			}
			
			parametrosSistemaService.atualizar(manager, transaction, parametroUltimaAtualizacao);	
			
			if(transacaoIndependente)
				transaction.commit();
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();
			
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	
	public void atualizarSituacao(ExportSteps step, ExportStatus status) throws Exception
	{
		Log.info("[STEP] " + step.name() + " updated to [STATUS] " + status.name());
		
		situacoes[step.code] = status.code;
		
		String situacaoData = "";
		
		for(int i = 0x00; i < situacoes.length; i++)
			situacaoData += (situacoes[i] + ",");
		
		situacaoData = ignUtil.trimEnd(situacaoData, ",");
		
		atualizarParametro(null, null, "LOG_ULTIMA_ATUALIZACAO_MOVIMENTO_F" + String.format("%02d", idEmpresaFisica), situacaoData);
	}

	private void scheduleNextAttempt()
	{
		logger.info("Scheduling attempt to send data again in " + FAIL_ATTEMPT_DELAY + " seconds");
		scheduler.schedule((Runnable) this, FAIL_ATTEMPT_DELAY, TimeUnit.SECONDS);
		
		FAILED_ATTEMPTS++;
	}
	
	@Override
	public void run() 
	{
		if(FAILED_ATTEMPTS > MAX_ATTEMPTS)
		{
			logger.warn("Maximium attempts to send data achieved. Stopping proccess.");
			FAILED_ATTEMPTS = 0x0000;
			return;
		}
		else if(FAILED_ATTEMPTS > 0x00)
			logger.info("Trying to send data again. Attempts [" + FAILED_ATTEMPTS + "]");
		else
			logger.info("Initializing process");
		
		if(!manager.isOpen())
			manager = ConexaoUtil.getEntityManager();
		
		con = new AMFConnector();
		
		try
		{
			dataPackage.setIdEmpresaFisica(idEmpresaFisica);
			dataPackage.setDataInicial(dataInicial);
			dataPackage.setDataFinal(dataFinal);
			
			manager.getTransaction().begin();
			
			try { con.connect(Util.getUrlAmfCentral()); }
			catch(ClientStatusException cse)
			{
				scheduleNextAttempt();
				logger.warn("Não foi possível conectar com o Escritório!", cse);
				return;				
			}

			atualizarSituacao(ExportSteps.OPENING_CONNECTIONS, ExportStatus.FINISHED);
			
			atualizarSituacao(ExportSteps.RUNNING_PEDIDOVENDA_BUNDLE, ExportStatus.RUNNING);
			
			final List<Date> datasExportacao = Util.movimentoLojaBundle.listaDatasExportacao(manager, dataInicial, dataFinal);
			
			Util.vendedorBundle.exportar(manager, dataPackage, dataInicial, dataFinal);
			Util.pedidoVendaBundle.exportar(manager, dataPackage, datasExportacao);
			atualizarSituacao(ExportSteps.RUNNING_PEDIDOVENDA_BUNDLE, ExportStatus.FINISHED);
			
			atualizarSituacao(ExportSteps.RUNNING_CLIENTE_BUNDLE, ExportStatus.RUNNING);
			Util.clienteBundle.exportar(manager, dataPackage, dataInicial, dataFinal);
			atualizarSituacao(ExportSteps.RUNNING_CLIENTE_BUNDLE, ExportStatus.FINISHED);
			
			atualizarSituacao(ExportSteps.RUNNING_DUPLICATAS_BUNDLE, ExportStatus.RUNNING);
			Util.duplicataBundle.exportar(manager, dataPackage, dataInicial, dataFinal);
			atualizarSituacao(ExportSteps.RUNNING_DUPLICATAS_BUNDLE, ExportStatus.FINISHED);
			
			atualizarSituacao(ExportSteps.RUNNING_MOVIMENTOLOJA_BUNDLE, ExportStatus.RUNNING);
			Util.movimentoLojaBundle.exportar(manager, dataPackage, dataInicial, dataFinal);
			Util.chequeRecebidoBundle.exportar(manager, dataPackage, dataInicial, dataFinal);
			atualizarSituacao(ExportSteps.RUNNING_MOVIMENTOLOJA_BUNDLE, ExportStatus.FINISHED);
			
			dataPackage.seal();
			
			atualizarSituacao(ExportSteps.RUNNING_DEPLOY, ExportStatus.RUNNING);

			try
			{
				con.call("movimentoService.deployDataPackage", dataPackage);
				
				atualizarSituacao(ExportSteps.RUNNING_DEPLOY, ExportStatus.FINISHED);
				atualizarParametro(manager, manager.getTransaction(), "ULTIMA_ATUALIZACAO_MOVIMENTO_F" + String.format("%02d", idEmpresaFisica), new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
				
				manager.getTransaction().commit();
				
				logger.info("Send data OK!");
			}
			catch(ClientStatusException cse)
			{
				if(cse.getCode() == ClientStatusException.AMF_CONNECT_FAILED_CODE)
				{
					scheduleNextAttempt();
					logger.warn("Não foi possível conectar com o Escritório!", cse);
					return;
				}
				else if(cse.getCode() == ClientStatusException.AMF_CALL_FAILED_CODE)
				{
					if(cse.getCause() instanceof EOFException)
					{
						//Geralmente quando os models estão desatualizados o Cliente não consegue fazer a leitura da estrutura de dados
						//o que gera um EOFException. Nesse caso certifique-se que todos os class do package br.com.desenv.nepalign.model. estejam atualizados
						logger.warn("Não foi possível realizar a chamada. Verifique se os Models do Servidor são compatíveis com os Models do Cliente", cse);
						atualizarSituacao(ExportSteps.RUNNING_DEPLOY, ExportStatus.ERROR);
						manager.getTransaction().rollback();	
					}
					else if(cse.getCause() instanceof MessageException)
					{
						//Provavelmente falha no banco de dados do escritório. Agendar nova tentativa!
						if(cse.getCause().getMessage().indexOf("org.hibernate.exception.GenericJDBCException") >= 0)
						{
							scheduleNextAttempt();
							logger.warn("Não foi possível conectar com o Escritório. Provavelmente o banco de dados não conseguiu iniciar uma transação. Agendando nova tentativa!", cse);
							return;
						}
						else
						{
							logger.warn("Não foi realizar o deploy! Causa do erro não foi identificada", cse);
							atualizarSituacao(ExportSteps.RUNNING_DEPLOY, ExportStatus.ERROR);
							manager.getTransaction().rollback();	
						}
					}
					else
					{
						logger.warn("Não foi realizar o deploy! Causa do erro não foi identificada", cse);
						atualizarSituacao(ExportSteps.RUNNING_DEPLOY, ExportStatus.ERROR);
						manager.getTransaction().rollback();
					}
				}
			}
			catch(ServerStatusException sse)
			{ 
				throw sse;
			}
		}
		catch(Exception ex)
		{
			try 
			{
				atualizarSituacao(ExportSteps.RUNNING_DEPLOY, ExportStatus.ERROR);
				
				logger.error("ERROR", ex);
				manager.getTransaction().rollback();	
			}
			catch (Exception e) 
			{
				logger.error(ex);
			}
		}
		finally
		{
			con.close();
			manager.close();
			
			dataPackage.dispose();
			dataPackage = null;
		}
	}
}