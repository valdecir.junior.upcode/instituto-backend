package br.com.desenv.nepalign.integracao;

import java.util.Date;

import javax.persistence.EntityManager;

import br.com.desenv.nepalign.integracao.dto.ContaGerencialDTO;
import br.com.desenv.nepalign.integracao.dto.DataPackage;
import br.com.desenv.nepalign.integracao.dto.TipoContaContaGerDTO;
import br.com.desenv.nepalign.integracao.dto.TipoContaDTO;
import br.com.desenv.nepalign.integracao.dto.TipoMovimentoCaixaDiaDTO;

public class ContasBundle implements DataBundle 
{
	@SuppressWarnings("unchecked")
	@Override
	public DataPackage exportar(final EntityManager manager, final DataPackage dataPackage, final Date dataInicial, final Date dataFinal) throws Exception 
	{
		dataPackage.put(TipoContaDTO.class, manager.createNativeQuery("SELECT `tipoConta`.* FROM `tipoConta`;", TipoContaDTO.class).getResultList());
		dataPackage.put(ContaGerencialDTO.class, manager.createNativeQuery("SELECT `contaGerencial`.* FROM `contaGerencial`;", ContaGerencialDTO.class).getResultList());
		dataPackage.put(TipoContaContaGerDTO.class, manager.createNativeQuery("SELECT `tipoContaContaGer`.* FROM `tipoContaContaGer`;", TipoContaContaGerDTO.class).getResultList());
		
		dataPackage.put(TipoMovimentoCaixaDiaDTO.class, 
				manager.createNativeQuery("SELECT `tipoMovimentoCaixaDia`.* FROM `tipoMovimentoCaixaDia` WHERE (`tipoMovimentoCaixaDia`.`idEmpresaFisica` IS NULL OR `tipoMovimentoCaixaDia`.`idEmpresaFisica` = " + dataPackage.getIdEmpresaFisica() + ");", 
						TipoMovimentoCaixaDiaDTO.class).getResultList());
		
		return dataPackage;
	}
}