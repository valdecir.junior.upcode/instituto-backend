package br.com.desenv.nepalign.integracao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.integracao.dto.DataPackage;
import br.com.desenv.nepalign.integracao.dto.ItemPedidoVendaDTO;
import br.com.desenv.nepalign.integracao.dto.LogOperacaoDTO;
import br.com.desenv.nepalign.integracao.dto.PedidoVendaDTO;
import br.com.desenv.nepalign.model.ItemPedidoVenda;
import br.com.desenv.nepalign.model.MovimentacaoEstoque;
import br.com.desenv.nepalign.model.PedidoVenda;
import br.com.desenv.nepalign.service.MovimentacaoEstoqueService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

import flex.messaging.io.ArrayCollection;
import flex.messaging.io.amf.client.AMFConnection;

public class PedidoVendaBundle implements DataBundle
{
	public List<Integer> dataPedidosIntegrados;

	public PedidoVendaBundle()
	{
		dataPedidosIntegrados = new ArrayList<Integer>();
	} 
	
	public void importar(AMFConnection con, EntityManager manager, EntityTransaction transaction, String idEmpresaFisica, Date dataInicial, Date dataFinal) throws Exception
	{
		String databaseName = Util.getDatabaseName(idEmpresaFisica);
		
		EntityManager managerCentral = ConexaoUtil.getEntityManager();
		managerCentral.getTransaction().begin();
		
		MovimentacaoEstoqueService movimentacaoEstoqueService = new MovimentacaoEstoqueService();
		
		try
		{
			do
			{
				System.out.println(">Filtrando Vendas disponíveis... [" + new SimpleDateFormat("dd/MM/yyyy").format(dataInicial) + "]");
				
				PedidoVenda pedidoVendaFiltro = new PedidoVenda();
				pedidoVendaFiltro.setDataVenda(dataInicial); 
			
				ArrayCollection listaPedidoVenda = (ArrayCollection) con.call(Util.SERVICE_GET_SALES, pedidoVendaFiltro, null, null);
				
				System.out.println(">>Vendas filtradas com sucesso! " + listaPedidoVenda.size() + " Vendas a serem integradas!");
				
				if(listaPedidoVenda.size() > 0x00)
				{
					String queryDeleteItemPedidoVenda = "DELETE FROM " + databaseName + ".itemPedidoVenda WHERE idPedidoVenda IN (";
					queryDeleteItemPedidoVenda = queryDeleteItemPedidoVenda.concat("SELECT idPedidoVenda FROM " + databaseName + ".pedidoVenda WHERE dataVenda BETWEEN '" + new SimpleDateFormat("yyyy-MM-dd").format(dataInicial) + " 00:00:00' and '" + new SimpleDateFormat("yyyy-MM-dd").format(dataInicial) + " 23:59:59'); ");
					
					manager.createNativeQuery(queryDeleteItemPedidoVenda).executeUpdate();
					manager.createNativeQuery("DELETE FROM " + databaseName + ".pedidoVenda WHERE dataVenda BETWEEN '" + new SimpleDateFormat("yyyy-MM-dd").format(dataInicial) + " 00:00:00' and '" + new SimpleDateFormat("yyyy-MM-dd").format(dataInicial) + " 23:59:59'; ").executeUpdate();
				}
				
				System.out.println(">Importando...");
				
				int currentIndex = 0x00;
				
				if(!Util.DEBUG_MODE)
					System.out.print(">>Importando Vendas 		1/" + listaPedidoVenda.size());
				
				for (Object venda : listaPedidoVenda)
				{
					currentIndex++;
					
					if(!Util.DEBUG_MODE)
					{
			            for (int j = 0; j < (listaPedidoVenda.size() + "").length() + 0x01; j++) 
			                System.out.print('\b');
			            
			            for (int j = 0; j < (currentIndex + "").length(); j++)
			                System.out.print('\b');
			            
			            System.out.print(currentIndex + "/" + listaPedidoVenda.size());	
					}
					
					PedidoVenda pedidoVenda = ((PedidoVenda) venda).clone();
					
					Util.clienteBundle.importar(con, manager, transaction, idEmpresaFisica, pedidoVenda.getCliente(), false);
				
					try 
					{					
						manager.merge(Util.<PedidoVendaDTO> copy(PedidoVendaDTO.class, pedidoVenda.clone()));	
					}
					catch(Exception ex)
					{
						throw ex;  
					} 
					
					ItemPedidoVenda itemPedidoVendaFiltro = new ItemPedidoVenda();
					itemPedidoVendaFiltro.setPedidoVenda((PedidoVenda) venda);
				
					ArrayCollection listaItemPedidoVenda = (ArrayCollection) con.call(Util.SERVICE_GET_SALE_ITEMS, itemPedidoVendaFiltro);
					List<ItemPedidoVenda> listaItemVenda = new ArrayList<ItemPedidoVenda>();
					
					for(Object itemVenda : listaItemPedidoVenda)
					{
						listaItemVenda.add(((ItemPedidoVenda) itemVenda).clone());
						manager.merge(Util.<ItemPedidoVendaDTO> copy(ItemPedidoVendaDTO.class, ((ItemPedidoVenda) itemVenda).clone()));	
					} 
					
					if(pedidoVenda.getSituacaoPedidoVenda().getId() != 0x05) // cancelado
					{
						if(movimentacaoEstoqueService.recuperarPorPedidoVenda(managerCentral, pedidoVenda).size() == 0x00)
							movimentacaoEstoqueService.gerarMovimentacaoEstoquePorPedidoVenda(pedidoVenda, listaItemVenda, managerCentral, managerCentral.getTransaction());	
					}
					else  
					{
						List<MovimentacaoEstoque> listaMovimentacaoEstoques = movimentacaoEstoqueService.recuperarPorPedidoVenda(managerCentral, pedidoVenda);
						
						for(MovimentacaoEstoque movimentacaoEstoque : listaMovimentacaoEstoques)
							movimentacaoEstoqueService.excluirMovimentacaoEstoque(movimentacaoEstoque, managerCentral, managerCentral.getTransaction());
					}
				}
				
				if(!Util.DEBUG_MODE)
					System.out.println();
				
				System.out.println(">>" + currentIndex + " Vendas importadas!");
				
				dataPedidosIntegrados.add(Integer.parseInt(new SimpleDateFormat("yyyyMMdd").format(dataInicial)));
				
				dataInicial = Util.addDay(dataInicial, 0x01);
			}
			while(dataInicial.before(dataFinal) || dataInicial.equals(dataFinal));	
			
			managerCentral.getTransaction().commit();
			managerCentral.close();
		}
		catch(Exception ex)
		{
			if(managerCentral.getTransaction().isActive())
				managerCentral.getTransaction().rollback();
			
			ex.printStackTrace();
			throw ex;
		}
	}
	
	public DataPackage exportar(final EntityManager manager, final DataPackage dataPackage, List<Date> data) throws Exception
	{
		return exportar(manager, dataPackage, data, true);
	}

	@SuppressWarnings("unchecked")
	public DataPackage exportar(final EntityManager manager, final DataPackage dataPackage, List<Date> data, final boolean cancelados) throws Exception
	{
		String pedidoVendaQuery = "SELECT * FROM pedidoVenda ";
		String itemPedidoVendaQuery = " select ipv.* from itempedidovenda ipv " +
		" inner join pedidovenda PV on PV.idPedidoVenda = ipv.idPedidoVenda ";
		String logOperacaoPedidoVendaQuery = "SELECT * FROM logOperacao ";
		
		for(int dataIndex = 0x00; dataIndex < data.size(); dataIndex++)
		{
			if(dataIndex == 0x00)
			{
				pedidoVendaQuery += " WHERE ";	
				itemPedidoVendaQuery += " WHERE ";
				logOperacaoPedidoVendaQuery += " WHERE ";
			}
			else 
			{
				pedidoVendaQuery += " OR ";
				itemPedidoVendaQuery += " OR ";
				logOperacaoPedidoVendaQuery += " OR ";
			}
			
			pedidoVendaQuery += " dataVenda BETWEEN '" + IgnUtil.dateFormatSql.format(data.get(dataIndex)) + " 00:00:00' and '" + IgnUtil.dateFormatSql.format(data.get(dataIndex)) + " 23:59:59' ";
			itemPedidoVendaQuery += " dataVenda BETWEEN '" + IgnUtil.dateFormatSql.format(data.get(dataIndex)) + " 00:00:00' and '" + IgnUtil.dateFormatSql.format(data.get(dataIndex)) + " 23:59:59' ";
			logOperacaoPedidoVendaQuery += " data BETWEEN '" + IgnUtil.dateFormatSql.format(data.get(dataIndex)) + " 00:00:00' and '" + IgnUtil.dateFormatSql.format(data.get(dataIndex)) + " 23:59:59' ";
		}
		
		if(!cancelados)
		{
			pedidoVendaQuery += " and idSituacaoPedidoVenda <> 5;";
			itemPedidoVendaQuery += " and idSituacaoPedidoVenda <> 5;";
		}
		else 
		{
			pedidoVendaQuery += ";";
			itemPedidoVendaQuery += ";";
		}
		
		logOperacaoPedidoVendaQuery += " AND nomeClasse = 'PedidoVenda';";

		dataPackage.<LogOperacaoDTO> put(LogOperacaoDTO.class, (List<LogOperacaoDTO>) manager.createNativeQuery(logOperacaoPedidoVendaQuery, LogOperacaoDTO.class).getResultList());
		dataPackage.<PedidoVendaDTO> put(PedidoVendaDTO.class, (List<PedidoVendaDTO>) manager.createNativeQuery(pedidoVendaQuery, PedidoVendaDTO.class).getResultList());
		dataPackage.<ItemPedidoVendaDTO> put(ItemPedidoVendaDTO.class, (List<ItemPedidoVendaDTO>) manager.createNativeQuery(itemPedidoVendaQuery, ItemPedidoVendaDTO.class).getResultList());
		
		return dataPackage;
	}
	
	public DataPackage exportar(final EntityManager manager, final DataPackage dataPackage, final Date dataInicial, final Date dataFinal) throws Exception
	{
		return exportar(manager, dataPackage, dataInicial, dataFinal, true);
	}

	@SuppressWarnings("unchecked")
	public DataPackage exportar(final EntityManager manager, final DataPackage dataPackage, final Date dataInicial, final Date dataFinal, final boolean cancelados) throws Exception
	{
		final String pedidoVendaQuery = "SELECT * FROM pedidoVenda WHERE dataVenda BETWEEN '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' and '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59' "
		.concat(cancelados ? ";" : " and idSituacaoPedidoVenda <> 5;");
		final String itemPedidoVendaQuery = " select ipv.* from itempedidovenda ipv " +
		" inner join pedidovenda PV on PV.idPedidoVenda = ipv.idPedidoVenda " +
		" where dataVenda BETWEEN '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' and '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59' "
		.concat(cancelados ? ";" : " and pv.idSituacaoPedidoVenda <> 5;");
		final String logOperacaoPedidoVendaQuery = "SELECT * FROM logOperacao WHERE data BETWEEN '" + IgnUtil.dateFormatSql.format(dataInicial) + " 00:00:00' and '" + IgnUtil.dateFormatSql.format(dataFinal) + " 23:59:59' "
		.concat(" and nomeClasse = 'PedidoVenda';");
		
		dataPackage.<LogOperacaoDTO> put(LogOperacaoDTO.class, (List<LogOperacaoDTO>) manager.createNativeQuery(logOperacaoPedidoVendaQuery, LogOperacaoDTO.class).getResultList());
		dataPackage.<PedidoVendaDTO> put(PedidoVendaDTO.class, (List<PedidoVendaDTO>) manager.createNativeQuery(pedidoVendaQuery, PedidoVendaDTO.class).getResultList());
		dataPackage.<ItemPedidoVendaDTO> put(ItemPedidoVendaDTO.class, (List<ItemPedidoVendaDTO>) manager.createNativeQuery(itemPedidoVendaQuery, ItemPedidoVendaDTO.class).getResultList());
		
		return dataPackage;
	}
}