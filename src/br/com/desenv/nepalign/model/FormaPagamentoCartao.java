package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="formapagamentocartao")
public class FormaPagamentoCartao extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;
	
	public static final Integer DEBITO = 0x01;
	public static final Integer CREDITO = 0x02;
	public static final Integer PARCELADO = 0x03;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idFormaPagamentoCartao")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idOperadoraCartao")
	private OperadoraCartao operadoraCartao;
	
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="quantidadeParcelas")
	private Integer quantidadeParcelas;

	public FormaPagamentoCartao()
	{
		super();
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public OperadoraCartao getOperadoraCartao() 
	{
		return operadoraCartao;
	}

	public void setOperadoraCartao(OperadoraCartao operadoraCartao) 
	{
		this.operadoraCartao = operadoraCartao;
	}
	
	public String getDescricao() 
	{
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) 
	{
		if (descricao != null)
			this.descricao = descricao.toUpperCase().trim();
		else
			this.descricao = null;		
	}
	
	public Integer getQuantidadeParcelas()
	{
		return this.quantidadeParcelas;
	}
	
	public void setQuantidadeParcelas(Integer quantidadeParcelas) throws Exception
	{
		if(Double.isNaN(quantidadeParcelas))
			this.quantidadeParcelas = null;
		else 
			this.quantidadeParcelas = quantidadeParcelas;
	}

	@Override
	public void validate() throws Exception
	{

	}
}