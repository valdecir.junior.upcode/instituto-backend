package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="departamentoproduto")


public class DepartamentoProduto extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idDepartamentoProduto")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idDepartamento")
	private Departamento departamento;
	@ManyToOne
	@JoinColumn(name = "idProduto")
	private Produto produto;
	
	@ManyToOne
	@JoinColumn(name = "idSituacaoEnvioLV")
	private SituacaoEnvioLV situacaoEnvioLV;

		public DepartamentoProduto()
	{
		super();
	}
	

	public DepartamentoProduto
	(
	Departamento departamento,
	Produto produto
	) 
	{
		super();
		this.departamento = departamento;
		this.produto = produto;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	public SituacaoEnvioLV getSituacaoEnvioLV() {
		return situacaoEnvioLV;
	}


	public void setSituacaoEnvioLV(SituacaoEnvioLV situacaoEnvioLV) {
		this.situacaoEnvioLV = situacaoEnvioLV;
	}
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
