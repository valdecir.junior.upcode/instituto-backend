package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="cfop")


public class Cfop extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idCFOP")
	private Integer id;
		
	@Column(name="codigo")
	private Integer codigo;
	
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="naturezaOperacao")
	private String naturezaOperacao;
	
	@Column(name="possuiTributacaoICMS")
	private Integer possuiTributacaoICMS;
	
	@Column(name="possuiTributacaoIPI")
	private Integer possuiTributacaoIPI;
	

	public Cfop()
	{
		super();
	}
	

	public Cfop
	(
		Integer codigo,
		String descricao,
		String naturezaOperacao,
		Integer possuiTributacaoICMS,
		Integer possuiTributacaoIPI
	) 
	{
		super();
		this.codigo = codigo;
		this.descricao = descricao;
		this.naturezaOperacao = naturezaOperacao;
		this.possuiTributacaoICMS = possuiTributacaoICMS;
		this.possuiTributacaoIPI = possuiTributacaoIPI;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public String getNaturezaOperacao() {
		String retorno = null;
		
		if (naturezaOperacao != null)
			retorno = naturezaOperacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setNaturezaOperacao(String naturezaOperacao) {
		if (naturezaOperacao != null)
		{
			this.naturezaOperacao = naturezaOperacao.toUpperCase().trim();
		}
		else
			this.naturezaOperacao = null;
			
		
	}
		
	public Integer getPossuiTributacaoICMS() {
		return possuiTributacaoICMS;
	}

	public void setPossuiTributacaoICMS(Integer possuiTributacaoICMS) {
		this.possuiTributacaoICMS = possuiTributacaoICMS;
	}	
	public Integer getPossuiTributacaoIPI() {
		return possuiTributacaoIPI;
	}

	public void setPossuiTributacaoIPI(Integer possuiTributacaoIPI) {
		this.possuiTributacaoIPI = possuiTributacaoIPI;
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
