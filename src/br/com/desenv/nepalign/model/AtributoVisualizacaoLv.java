package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="atributovisualizacaolv")


public class AtributoVisualizacaoLv extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idAtributoVisualizacaoLv")
	private Integer id;
		
	@Column(name="nome")
	private String nome;
	
	@Column(name="ordemExibicao")
	private Integer ordemExibicao;
	
	@Column(name="imagemListaProduto")
	private String imagemListaProduto;
	
	@Column(name="imagemDetalheProduto")
	private String imagemDetalheProduto;
	
	@Column(name="url")
	private String url;
	
	@ManyToOne
	@JoinColumn(name = "idSituacaoEnvioLv")
	private SituacaoEnvioLV situacaoEnvioLv;

	public AtributoVisualizacaoLv()
	{
		super();
	}
	

	public AtributoVisualizacaoLv
	(
		String nome,
		Integer ordemExibicao,
		String imagemListaProduto,
		String imagemDetalheProduto,
		String url,
	SituacaoEnvioLV situacaoEnvioLv
	) 
	{
		super();
		this.nome = nome;
		this.ordemExibicao = ordemExibicao;
		this.imagemListaProduto = imagemListaProduto;
		this.imagemDetalheProduto = imagemDetalheProduto;
		this.url = url;
		this.situacaoEnvioLv = situacaoEnvioLv;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getNome() {
		String retorno = null;
		
		if (nome != null)
			retorno = nome.toUpperCase().trim();
		return retorno;
	}
	
	public void setNome(String nome) {
		if (nome != null)
		{
			this.nome = nome.toUpperCase().trim();
		}
		else
			this.nome = null;
			
		
	}
		
	public Integer getOrdemExibicao() {
		return ordemExibicao;
	}

	public void setOrdemExibicao(Integer ordemExibicao) {
		this.ordemExibicao = ordemExibicao;
	}	
	public String getImagemListaProduto() {
		String retorno = null;
		
		if (imagemListaProduto != null)
			retorno = imagemListaProduto.toUpperCase().trim();
		return retorno;
	}
	
	public void setImagemListaProduto(String imagemListaProduto) {
		if (imagemListaProduto != null)
		{
			this.imagemListaProduto = imagemListaProduto.toUpperCase().trim();
		}
		else
			this.imagemListaProduto = null;
			
		
	}
		
	public String getImagemDetalheProduto() {
		String retorno = null;
		
		if (imagemDetalheProduto != null)
			retorno = imagemDetalheProduto.toUpperCase().trim();
		return retorno;
	}
	
	public void setImagemDetalheProduto(String imagemDetalheProduto) {
		if (imagemDetalheProduto != null)
		{
			this.imagemDetalheProduto = imagemDetalheProduto.toUpperCase().trim();
		}
		else
			this.imagemDetalheProduto = null;
			
		
	}
		
	public String getUrl() {
		String retorno = null;
		
		if (url != null)
			retorno = url.toUpperCase().trim();
		return retorno;
	}
	
	public void setUrl(String url) {
		if (url != null)
		{
			this.url = url.toUpperCase().trim();
		}
		else
			this.url = null;
			
		
	}
		
	public SituacaoEnvioLV getSituacaoEnvioLv() {
		return situacaoEnvioLv;
	}

	public void setSituacaoEnvioLv(SituacaoEnvioLV situacaoEnvioLv) {
		this.situacaoEnvioLv = situacaoEnvioLv;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
