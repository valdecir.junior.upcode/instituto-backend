package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name = "itemcontagembalanco")
public class ItemContagemBalanco extends GenericModelIGN {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idItemContagemBalanco")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "idDispositivoBalanco")
	private DispositivoBalanco dispositivoBalanco;
	
	@ManyToOne
	@JoinColumn(name = "idEstoqueProduto")
	private EstoqueProduto estoqueProduto;
	
	@Column(name = "quantidade")
	private Integer quantidade;
	
	@Column(name = "itemContagem")
	private Integer itemContagem;

	public ItemContagemBalanco() {
		super();
	}

	public ItemContagemBalanco(Integer quantidade, DispositivoBalanco dispositivoBalanco, EstoqueProduto estoqueProduto, Integer itemContagem) {
		super();
		this.quantidade = quantidade;
		this.dispositivoBalanco = dispositivoBalanco;
		this.estoqueProduto = estoqueProduto;
		this.itemContagem = itemContagem;
	}

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public EstoqueProduto getEstoqueProduto() {
		return estoqueProduto;
	}

	public void setEstoqueProduto(EstoqueProduto estoqueProduto) {
		this.estoqueProduto = estoqueProduto;
	}

	public DispositivoBalanco getDispositivoBalanco() {
		return dispositivoBalanco;
	}

	public void setDispositivoBalanco(DispositivoBalanco dispositivoBalanco) {
		this.dispositivoBalanco = dispositivoBalanco;
	}

	public Integer getItemContagem() {
		return itemContagem;
	}

	public void setItemContagem(Integer itemContagem) {
		this.itemContagem = itemContagem;
	}

	@Override
	public void validate() throws Exception {
	}

}
