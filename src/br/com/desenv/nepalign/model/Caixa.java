package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="caixa")
public class Caixa extends GenericModelIGN 
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idCaixa")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="idEmpresaCaixa")
	private EmpresaCaixa empresaCaixa;
	
	@Column(name="nomeCaixa")
	private String nomeCaixa;
	
	@Column(name="mesAberto")
	private Integer mesAberto;
	
	@Column(name="anoAberto") 
	private Integer anoAberto;

	public Caixa()
	{
		super();
	}

	@Override
	public Integer getId() 
	{
		return this.id;
	}

	@Override
	public void setId(Integer id) 
	{
		this.id = id;
	}

	public EmpresaCaixa getEmpresaCaixa() 
	{
		return empresaCaixa;
	}

	public void setEmpresaCaixa(EmpresaCaixa empresaCaixa) 
	{
		this.empresaCaixa = empresaCaixa;
	}

	public String getNomeCaixa() 
	{
		return nomeCaixa == null ? null : nomeCaixa.toUpperCase();
	}

	public void setNomeCaixa(String nomeCaixa) 
	{
		this.nomeCaixa = (nomeCaixa == null) ? null : nomeCaixa.toUpperCase();
	}
	
	public void setMesAberto(Integer mesAberto) 
	{
		this.mesAberto = (mesAberto == null || Double.isNaN(mesAberto)) ? null : mesAberto;
	}
	
	public Integer getMesAberto() 
	{
		return mesAberto;
	}

	public void setAnoAberto(Integer anoAberto) 
	{
		this.anoAberto = (anoAberto == null || Double.isNaN(anoAberto)) ? null : anoAberto;
	}
	

	public Integer getAnoAberto() 
	{
		return anoAberto;
	}
	
	@Override
	public void validate() throws Exception  { }
}