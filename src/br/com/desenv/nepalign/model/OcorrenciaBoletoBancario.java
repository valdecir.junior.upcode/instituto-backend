package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="OcorrenciaBoletoBancario")


public class OcorrenciaBoletoBancario extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idOcorrenciaBoletoBancario")
	private Integer id;
		
	@Column(name="codigoOcorrencia")
	private String codigoOcorrencia;
	
	@Column(name="descricao")
	private String descricao;
	

	public OcorrenciaBoletoBancario()
	{
		super();
	}
	

	public OcorrenciaBoletoBancario
	(
		String codigoOcorrencia,
		String descricao
	) 
	{
		super();
		this.codigoOcorrencia = codigoOcorrencia;
		this.descricao = descricao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getCodigoOcorrencia() {
		String retorno = null;
		
		if (codigoOcorrencia != null)
			retorno = codigoOcorrencia.toUpperCase().trim();
		return retorno;
	}
	
	public void setCodigoOcorrencia(String codigoOcorrencia) {
		if (codigoOcorrencia != null)
		{
			this.codigoOcorrencia = codigoOcorrencia.toUpperCase().trim();
		}
		else
			this.codigoOcorrencia = null;
			
		
	}
		
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
