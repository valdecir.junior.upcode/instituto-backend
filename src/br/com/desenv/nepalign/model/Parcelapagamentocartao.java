package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="parcelapagamentocartao")


public class Parcelapagamentocartao extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idParcelaPagamentoCartao")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idPagamentoCartao")
	private PagamentoCartao pagamentoCartao;
	@Column(name="valor")
	private Double valor;
	
	@Column(name="DataVencimento")
	private Date dataVencimento;
	
	@Column(name="numero")
	private Integer numero;
	

	public Parcelapagamentocartao()
	{
		super();
	}
	

	public Parcelapagamentocartao
	(
	PagamentoCartao pagamentoCartao,
		Double valor,
		Date dataVencimento,
		Integer numero
	) 
	{
		super();
		this.pagamentoCartao = pagamentoCartao;
		this.valor = valor;
		this.dataVencimento = dataVencimento;
		this.numero = numero;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public PagamentoCartao getPagamentoCartao() {
		return pagamentoCartao;
	}

	public void setPagamentoCartao(PagamentoCartao pagamentoCartao) {
		this.pagamentoCartao = pagamentoCartao;
	}
	
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
	
		if (valor != null && Double.isNaN(valor))
		{
			this.valor = null;
		}
		else
		{
			this.valor = valor;
		}
		
	}	
	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}
	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
