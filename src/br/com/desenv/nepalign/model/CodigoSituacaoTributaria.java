package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="codigosituacaotributaria")


public class CodigoSituacaoTributaria extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idCodigoSituacaoTributaria")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idImpostosTaxasTarifas")
	private ImpostosTaxasTarifas impostosTaxasTarifas;
	@Column(name="codigo")
	private String codigo;
	
	@Column(name="descricao")
	private String descricao;
	

	public CodigoSituacaoTributaria()
	{
		super();
	}
	

	public CodigoSituacaoTributaria
	(
	ImpostosTaxasTarifas impostosTaxasTarifas,
		String codigo,
		String descricao
	) 
	{
		super();
		this.impostosTaxasTarifas = impostosTaxasTarifas;
		this.codigo = codigo;
		this.descricao = descricao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public ImpostosTaxasTarifas getImpostosTaxasTarifas() {
		return impostosTaxasTarifas;
	}

	public void setImpostosTaxasTarifas(ImpostosTaxasTarifas impostosTaxasTarifas) {
		this.impostosTaxasTarifas = impostosTaxasTarifas;
	}
	
	public String getCodigo() {
		String retorno = null;
		
		if (codigo != null)
			retorno = codigo.toUpperCase().trim();
		return retorno;
	}
	
	public void setCodigo(String codigo) {
		if (codigo != null)
		{
			this.codigo = codigo.toUpperCase().trim();
		}
		else
			this.codigo = null;
			
		
	}
		
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
