package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="fechamento")


public class Fechamento extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idFechamento")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idFormaFechamento")
	private FormaFechamento formaFechamento;
	@Column(name="movimento")
	private String movimento;
	
	@Column(name="descricao")
	private String descricao;
	

	public Fechamento()
	{
		super();
	}
	

	public Fechamento
	(
	EmpresaFisica empresaFisica,
	FormaFechamento formaFechamento,
		String movimento,
		String descricao
	) 
	{
		super();
		this.empresaFisica = empresaFisica;
		this.formaFechamento = formaFechamento;
		this.movimento = movimento;
		this.descricao = descricao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public FormaFechamento getFormaFechamento() {
		return formaFechamento;
	}

	public void setFormaFechamento(FormaFechamento formaFechamento) {
		this.formaFechamento = formaFechamento;
	}
	
	public String getMovimento() {
		String retorno = null;
		
		if (movimento != null)
			retorno = movimento.toUpperCase().trim();
		return retorno;
	}
	
	public void setMovimento(String movimento) {
		if (movimento != null)
		{
			this.movimento = movimento.toUpperCase().trim();
		}
		else
			this.movimento = null;
			
		
	}
		
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
