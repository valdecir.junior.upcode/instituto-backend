package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="emailcliente")
public class EmailCliente extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idEmailCliente")
	private Integer id;
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@Column(name="email")
	private String email;
	
	public EmailCliente()
	{
		super();
	}

	public EmailCliente(Cliente cliente, String email) 
	{
		super();
		this.cliente = cliente;
		this.email = email;
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}	
	
	public Cliente getCliente() 
	{
		return cliente;
	}

	public void setCliente(Cliente cliente) 
	{
		this.cliente = cliente;
	}
	
	public String getEmail() 
	{
		String retorno = null;
		
		if (email != null)
			retorno = email.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setEmail(String email) 
	{
		if (email != null)
			this.email = email.toUpperCase().trim();
		else
			this.email = null;
	}
		
	@Override
	public void validate() throws Exception
	{
	}
	
	public EmailCliente clone() throws CloneNotSupportedException
	{
		return (EmailCliente) super.clone();
	}
}