package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="tipomovimentocaixadia")


public class TipoMovimentoCaixaDia extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idTipoMovimentoCaixaDia")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	
	@ManyToOne
	@JoinColumn(name = "idTipoConta")
	private TipoConta tipoConta;
	
	@Column(name="origem")
	private Integer origem;
	
	@Column(name="geraDespesa")
	private String geraDespesa;
	

	public TipoMovimentoCaixaDia()
	{
		super();
	}
	

	public TipoMovimentoCaixaDia
	(
		String descricao,
	TipoConta tipoConta,
		Integer origem
	) 
	{
		super();
		this.descricao = descricao;
		this.tipoConta = tipoConta;
		this.origem = origem;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public TipoConta getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}
	
	public Integer getOrigem() {
		return origem;
	}

	public void setOrigem(Integer origem) {
		this.origem = origem;
	}	

	public String getGeraDespesa() {
		return geraDespesa;
	}


	public void setGeraDespesa(String geraDespesa) {
		this.geraDespesa = geraDespesa;
	}


	@Override
	public void validate() throws Exception
	{	
	}
}