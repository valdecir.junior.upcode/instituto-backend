package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="taxapagamento")


public class TaxaPagamento extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idTaxaPagamento")
	private Integer id;
		
	@Column(name="percentual")
	private Double percentual;
	
	@Column(name="parcelaAte")
	private Integer parcelaAte;
	
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idFormaPagamento")
	private FormaPagamento formaPagamento;

	@ManyToOne
	@JoinColumn(name = "idOperadoraCartao")
	private OperadoraCartao operadoraCartao;
	
	@ManyToOne
	@JoinColumn(name = "idEmpresaFinanceiro")
	private EmpresaFinanceiro empresaFinanceiro;
	public TaxaPagamento()
	{
		super();
	}
	

	public TaxaPagamento
	(
		Double percentual,
		Integer parcelaAte,
	EmpresaFisica empresaFisica,
	FormaPagamento formaPagamento,
	OperadoraCartao operadoraCartao,
	EmpresaFinanceiro empresaFinanceiro
	) 
	{
		super();
		this.percentual = percentual;
		this.parcelaAte = parcelaAte;
		this.empresaFisica = empresaFisica;
		this.formaPagamento = formaPagamento;
		this.operadoraCartao = operadoraCartao;
		this.empresaFinanceiro = empresaFinanceiro;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public EmpresaFinanceiro getEmpresaFinanceiro() {
		return empresaFinanceiro;
	}


	public void setEmpresaFinanceiro(EmpresaFinanceiro empresaFinanceiro) {
		this.empresaFinanceiro = empresaFinanceiro;
	}


	public Double getPercentual() {
		return percentual;
	}

	public void setPercentual(Double percentual) {
	
		if (percentual != null && Double.isNaN(percentual))
		{
			this.percentual = null;
		}
		else
		{
			this.percentual = percentual;
		}
		
	}	
	public Integer getParcelaAte() {
		return parcelaAte;
	}

	public void setParcelaAte(Integer parcelaAte) {
		this.parcelaAte = parcelaAte;
	}	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
	public OperadoraCartao getOperadoraCartao() {
		return operadoraCartao;
	}


	public void setOperadoraCartao(OperadoraCartao operadoraCartao) {
		this.operadoraCartao = operadoraCartao;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
