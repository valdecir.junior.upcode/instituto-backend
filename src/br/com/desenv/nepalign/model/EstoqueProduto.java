package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="estoqueproduto")


public class EstoqueProduto extends GenericModelIGN implements Cloneable
{

	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idEstoqueProduto")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idTamanho")
	private Tamanho tamanho;
	@ManyToOne
	@JoinColumn(name = "idCor")
	private Cor cor;
	@ManyToOne
	@JoinColumn(name = "idProduto")
	private Produto produto;
	@ManyToOne
	@JoinColumn(name = "idOrdemProduto")
	private OrdemProduto ordemProduto;
	@ManyToOne
	@JoinColumn(name = "idSituacaoEnvioLV")
	private SituacaoEnvioLV situacaoEnvioLV;
	@Column(name="observacao")
	private String observacao;
	
	@Column(name="codigoBarras")
	private String codigoBarras;
	

	public String getCodigoBarras() {
		return codigoBarras;
	}


	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}


	public EstoqueProduto()
	{
		super();
	}
	

	public EstoqueProduto
	(
	Tamanho tamanho,
	Cor cor,
	Produto produto,
	OrdemProduto ordemProduto,
	SituacaoEnvioLV situacaoEnvioLV,
		String observacao
	) 
	{
		super();
		this.tamanho = tamanho;
		this.cor = cor;
		this.produto = produto;
		this.ordemProduto = ordemProduto;
		this.situacaoEnvioLV = situacaoEnvioLV;
		this.observacao = observacao;
		
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public Tamanho getTamanho() {
		return tamanho;
	}

	public void setTamanho(Tamanho tamanho) {
		this.tamanho = tamanho;
	}
	
	public Cor getCor() {
		return cor;
	}

	public void setCor(Cor cor) {
		this.cor = cor;
	}
	
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	public OrdemProduto getOrdemProduto() {
		return ordemProduto;
	}

	public void setOrdemProduto(OrdemProduto ordemProduto) {
		this.ordemProduto = ordemProduto;
	}
	
	public SituacaoEnvioLV getSituacaoEnvioLV() {
		return situacaoEnvioLV;
	}

	public void setSituacaoEnvioLV(SituacaoEnvioLV situacaoEnvioLV) {
		this.situacaoEnvioLV = situacaoEnvioLV;
	}
	
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
	}
	
	public EstoqueProduto clone() throws CloneNotSupportedException
	{
		return (EstoqueProduto) super.clone();
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}
	
	@Override
	public boolean equals(Object object) 
	{
		if(object == null)
			return false;
		if(!object.getClass().equals(this.getClass()))
			return false;
		
		if(((EstoqueProduto) object).getCodigoBarras().equals(getCodigoBarras()))
			return true;
		
		return super.equals(object);
	}

}
