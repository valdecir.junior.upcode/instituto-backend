package br.com.desenv.nepalign.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="lancamentocontapagar")


public class LancamentoContaPagar extends GenericModelIGN implements Cloneable
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idLancamentoContaPagar")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEmpresa")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idFornecedor")
	private Fornecedor fornecedor;
	@ManyToOne
	@JoinColumn(name = "idCentroFinanceiro")
	private CentroFinanceiro centroFinanceiro;
	@ManyToOne
	@JoinColumn(name = "idContaGerencial")
	private ContaGerencial contaGerencial;
	@ManyToOne
	@JoinColumn(name = "idTipoConta")
	private TipoConta tipoConta;
	@ManyToOne
	@JoinColumn(name = "idContaCorrente")
	private ContaCorrente contaCorrente;
	@Column(name="dataLancamento")
	private Date dataLancamento;
	
	@Column(name="dataVencimento")
	private Date dataVencimento;
	
	@Column(name="dataQuitacao")
	private Date dataQuitacao;
	
	@Column(name="valorOriginal")
	private Double valorOriginal;
	
	@Column(name="valorDesconto")
	private Double valorDesconto;
	
	@Column(name="valorMulta")
	private Double valorMulta;
	
	@Column(name="percentualMulta")
	private Double percentualMulta;
	
	@Column(name="valorPago")
	private Double valorPago;
	
	@Column(name="documentoPagamento")
	private String documentoPagamento;
	
	@Column(name="valorJuroDiario")
	private Double valorJuroDiario;
	
	@Column(name="percentualJuroDiario")
	private Double percentualJuroDiario;
	
	@Column(name="favorecido")
	private String favorecido;
	
	@Column(name="numeroDocumento")
	private String numeroDocumento;
	
	@Column(name="observacao")
	private String observacao;
	
	@Column(name="dataLimiteDesconto")
	private Date dataLimiteDesconto;
	
	@Column(name="situacaoEmissaoCheque")
	private Integer situacaoEmissaoCheque;
	
	@ManyToOne
	@JoinColumn(name = "idFormaPagamento")
	private FormaPagamento formaPagamento;
	@ManyToOne
	@JoinColumn(name = "idLancamentoContaPagarFixo")
	private LancamentoContaPagarFixo lancamentoContaPagarFixo;
	@ManyToOne
	@JoinColumn(name = "idSituacaoLancamento")
	private SituacaoLancamento situacaoLancamento;
	@Column(name="codigoBarra")
	private String codigoBarra;
	
	@Column(name="codigoBarraConvertido")
	private String codigoBarraConvertido;
	
	@Column(name="nossoNumero")
	private String nossoNumero;
	
	@Column(name="prazoPagamento")
	private String prazoPagamento;
	
	@Column(name="idLancamentoAgrupador")
	private Integer lancamentoAgrupador;
	
	@Column(name="planoContaAntigo")
	private String planoContaAntigo;
	
	@Column(name="codigoSeguranca")
	private String codigoSeguranca;
	
	@ManyToOne
	@JoinColumn(name = "idPagamentoDocumentoEntradaSaida")
	private PagamentoDocumentoEntradaSaida pagamentoDocumentoEntradaSaida;
	
	@Column(name="empresasRateio")
	private String empresasRateio;

	public LancamentoContaPagar()
	{
		super();
	}
	

	public LancamentoContaPagar
	(
	EmpresaFisica empresaFisica,
	Fornecedor fornecedor,
	CentroFinanceiro centroFinanceiro,
	ContaGerencial contaGerencial,
	TipoConta tipoConta,
	ContaCorrente contaCorrente,
		Date dataLancamento,
		Date dataVencimento,
		Date dataQuitacao,
		Double valorOriginal,
		Double valorDesconto,
		Double valorMulta,
		Double percentualMulta,
		Double valorPago,
		String documentoPagamento,
		Double valorJuroDiario,
		Double percentualJuroDiario,
		String favorecido,
		String numeroDocumento,
		String observacao,
		Date dataLimiteDesconto,
		Integer situacaoEmissaoCheque,
	FormaPagamento formaPagamento,
	LancamentoContaPagarFixo lancamentoContaPagarFixo,
	SituacaoLancamento situacaoLancamento,
		String codigoBarra,
		String codigoBarraConvertido,
		String nossoNumero,
		String prazoPagamento,
		Integer lancamentoAgrupador
	) 
	{
		super();
		this.empresaFisica = empresaFisica;
		this.fornecedor = fornecedor;
		this.centroFinanceiro = centroFinanceiro;
		this.contaGerencial = contaGerencial;
		this.tipoConta = tipoConta;
		this.contaCorrente = contaCorrente;
		this.dataLancamento = dataLancamento;
		this.dataVencimento = dataVencimento;
		this.dataQuitacao = dataQuitacao;
		this.valorOriginal = valorOriginal;
		this.valorDesconto = valorDesconto;
		this.valorMulta = valorMulta;
		this.percentualMulta = percentualMulta;
		this.valorPago = valorPago;
		this.documentoPagamento = documentoPagamento;
		this.valorJuroDiario = valorJuroDiario;
		this.percentualJuroDiario = percentualJuroDiario;
		this.favorecido = favorecido;
		this.numeroDocumento = numeroDocumento;
		this.observacao = observacao;
		this.dataLimiteDesconto = dataLimiteDesconto;
		this.situacaoEmissaoCheque = situacaoEmissaoCheque;
		this.formaPagamento = formaPagamento;
		this.lancamentoContaPagarFixo = lancamentoContaPagarFixo;
		this.situacaoLancamento = situacaoLancamento;
		this.codigoBarra = codigoBarra;
		this.codigoBarraConvertido = codigoBarraConvertido;
		this.nossoNumero = nossoNumero;
		this.prazoPagamento = prazoPagamento;
		this.lancamentoAgrupador = lancamentoAgrupador;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	public CentroFinanceiro getCentroFinanceiro() {
		return centroFinanceiro;
	}

	public void setCentroFinanceiro(CentroFinanceiro centroFinanceiro) {
		this.centroFinanceiro = centroFinanceiro;
	}
	
	public ContaGerencial getContaGerencial() {
		return contaGerencial;
	}

	public void setContaGerencial(ContaGerencial contaGerencial) {
		this.contaGerencial = contaGerencial;
	}
	
	public TipoConta getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}
	
	public ContaCorrente getContaCorrente() {
		return contaCorrente;
	}

	public void setContaCorrente(ContaCorrente contaCorrente) {
		this.contaCorrente = contaCorrente;
	}
	
	public Date getDataLancamento() {
		return dataLancamento;
	}

	public void setDataLancamento(Date dataLancamento) {
		this.dataLancamento = dataLancamento;
	}
	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}
	public Date getDataQuitacao() {
		return dataQuitacao;
	}

	public void setDataQuitacao(Date dataQuitacao) {
		this.dataQuitacao = dataQuitacao;
	}
	public Double getValorOriginal() {
		return valorOriginal;
	}

	public void setValorOriginal(Double valorOriginal) {
	
		if (valorOriginal != null && Double.isNaN(valorOriginal))
		{
			this.valorOriginal = null;
		}
		else
		{
			this.valorOriginal = valorOriginal;
		}
		
	}	
	public Double getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(Double valorDesconto) {
	
		if (valorDesconto != null && Double.isNaN(valorDesconto))
		{
			this.valorDesconto = null;
		}
		else
		{
			this.valorDesconto = valorDesconto;
		}
		
	}	
	public Double getValorMulta() {
		return valorMulta;
	}

	public void setValorMulta(Double valorMulta) {
	
		if (valorMulta != null && Double.isNaN(valorMulta))
		{
			this.valorMulta = null;
		}
		else
		{
			this.valorMulta = valorMulta;
		}
		
	}	
	public Double getPercentualMulta() {
		return percentualMulta;
	}

	public void setPercentualMulta(Double percentualMulta) {
	
		if (percentualMulta != null && Double.isNaN(percentualMulta))
		{
			this.percentualMulta = null;
		}
		else
		{
			this.percentualMulta = percentualMulta;
		}
		
	}	
	public Double getValorPago() {
		return valorPago;
	}

	public void setValorPago(Double valorPago) {
	
		if (valorPago != null && Double.isNaN(valorPago))
		{
			this.valorPago = null;
		}
		else
		{
			this.valorPago = valorPago;
		}
		
	}	
	public String getDocumentoPagamento() {
		String retorno = null;
		
		if (documentoPagamento != null)
			retorno = documentoPagamento.toUpperCase().trim();
		return retorno;
	}
	
	public void setDocumentoPagamento(String documentoPagamento) {
		if (documentoPagamento != null)
		{
			this.documentoPagamento = documentoPagamento.toUpperCase().trim();
		}
		else
			this.documentoPagamento = null;
			
		
	}
		
	public Double getValorJuroDiario() {
		return valorJuroDiario;
	}

	public void setValorJuroDiario(Double valorJuroDiario) {
	
		if (valorJuroDiario != null && Double.isNaN(valorJuroDiario))
		{
			this.valorJuroDiario = null;
		}
		else
		{
			this.valorJuroDiario = valorJuroDiario;
		}
		
	}	
	public Double getPercentualJuroDiario() {
		return percentualJuroDiario;
	}

	public void setPercentualJuroDiario(Double percentualJuroDiario) {
	
		if (percentualJuroDiario != null && Double.isNaN(percentualJuroDiario))
		{
			this.percentualJuroDiario = null;
		}
		else
		{
			this.percentualJuroDiario = percentualJuroDiario;
		}
		
	}	
	public String getFavorecido() {
		String retorno = null;
		
		if (favorecido != null)
			retorno = favorecido.toUpperCase().trim();
		return retorno;
	}
	
	public void setFavorecido(String favorecido) {
		if (favorecido != null)
		{
			this.favorecido = favorecido.toUpperCase().trim();
		}
		else
			this.favorecido = null;
			
		
	}
		
	public String getNumeroDocumento() {
		String retorno = null;
		
		if (numeroDocumento != null)
			retorno = numeroDocumento.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroDocumento(String numeroDocumento) {
		if (numeroDocumento != null)
		{
			this.numeroDocumento = numeroDocumento.toUpperCase().trim();
		}
		else
			this.numeroDocumento = null;
			
		
	}
		
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	public Date getDataLimiteDesconto() {
		return dataLimiteDesconto;
	}

	public void setDataLimiteDesconto(Date dataLimiteDesconto) {
		this.dataLimiteDesconto = dataLimiteDesconto;
	}
	public Integer getSituacaoEmissaoCheque() {
		return situacaoEmissaoCheque;
	}

	public void setSituacaoEmissaoCheque(Integer situacaoEmissaoCheque) {
		this.situacaoEmissaoCheque = situacaoEmissaoCheque;
	}	
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
	public LancamentoContaPagarFixo getLancamentoContaPagarFixo() {
		return lancamentoContaPagarFixo;
	}

	public void setLancamentoContaPagarFixo(LancamentoContaPagarFixo lancamentoContaPagarFixo) {
		this.lancamentoContaPagarFixo = lancamentoContaPagarFixo;
	}
	
	public SituacaoLancamento getSituacaoLancamento() {
		return situacaoLancamento;
	}

	public void setSituacaoLancamento(SituacaoLancamento situacaoLancamento) {
		this.situacaoLancamento = situacaoLancamento;
	}
	
	public String getCodigoBarra() {
		String retorno = null;
		
		if (codigoBarra != null)
			retorno = codigoBarra.toUpperCase().trim();
		return retorno;
	}
	
	public void setCodigoBarra(String codigoBarra) {
		if (codigoBarra != null)
		{
			this.codigoBarra = codigoBarra.toUpperCase().trim();
		}
		else
			this.codigoBarra = null;
			
		
	}
		
	public String getCodigoBarraConvertido() {
		String retorno = null;
		
		if (codigoBarraConvertido != null)
			retorno = codigoBarraConvertido.toUpperCase().trim();
		return retorno;
	}
	
	public void setCodigoBarraConvertido(String codigoBarraConvertido) {
		if (codigoBarraConvertido != null)
		{
			this.codigoBarraConvertido = codigoBarraConvertido.toUpperCase().trim();
		}
		else
			this.codigoBarraConvertido = null;
			
		
	}
		
	public String getNossoNumero() {
		String retorno = null;
		
		if (nossoNumero != null)
			retorno = nossoNumero.toUpperCase().trim();
		return retorno;
	}
	
	public void setNossoNumero(String nossoNumero) {
		if (nossoNumero != null)
		{
			this.nossoNumero = nossoNumero.toUpperCase().trim();
		}
		else
			this.nossoNumero = null;
			
		
	}
		
	public String getPrazoPagamento() {
		String retorno = null;
		
		if (prazoPagamento != null)
			retorno = prazoPagamento.toUpperCase().trim();
		return retorno;
	}
	
	public void setPrazoPagamento(String prazoPagamento) {
		if (prazoPagamento != null)
		{
			this.prazoPagamento = prazoPagamento.toUpperCase().trim();
		}
		else
			this.prazoPagamento = null;
			
		
	}
		
	public Integer getLancamentoAgrupador() {
		return lancamentoAgrupador;
	}

	public void setLancamentoAgrupador(Integer lancamentoAgrupador) {
		this.lancamentoAgrupador = lancamentoAgrupador;
	}	
	public String getPlanoContaAntigo() {
		return planoContaAntigo;
	}


	public void setPlanoContaAntigo(String planoContaAntigo) {
		this.planoContaAntigo = planoContaAntigo;
	}


	/**
	 * Todo código de segurança segue um padrão [PREFIXO + CODIGO]
	 * RN: Rateio com N. de replicações.
	 * RP: Rateio com Parcelas.
	 * @return codigoSeguranca
	 */
	public String getCodigoSeguranca() {
		return codigoSeguranca;
	}

	public void setCodigoSeguranca(String codigoSeguranca) {
		this.codigoSeguranca = codigoSeguranca;
	}


	public PagamentoDocumentoEntradaSaida getPagamentoDocumentoEntradaSaida() {
		return pagamentoDocumentoEntradaSaida;
	}


	public void setPagamentoDocumentoEntradaSaida(
			PagamentoDocumentoEntradaSaida pagamentoDocumentoEntradaSaida) {
		this.pagamentoDocumentoEntradaSaida = pagamentoDocumentoEntradaSaida;
	}


	public String getEmpresasRateio() {
		return empresasRateio;
	}


	public void setEmpresasRateio(String empresasRateio) {
		this.empresasRateio = empresasRateio;
	}
	
	public Double getValorSaldo()
	{
		if(this.getSituacaoLancamento().getId() != 2)
		{
			return getValorOriginal() - getValorPago(); 
		}
		return 0.0;
	}


	@Override
	public void validate() throws Exception
	{
		if(numeroDocumento != null)
			numeroDocumento = numeroDocumento.replace('"', ' ');
		if(observacao != null)
			observacao = observacao.replace('"', ' ');
	}
	
	@Override
	public br.com.desenv.nepalign.model.LancamentoContaPagar clone() throws CloneNotSupportedException 
	{
		return (br.com.desenv.nepalign.model.LancamentoContaPagar) super.clone();
	}
}