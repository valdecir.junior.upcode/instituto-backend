package br.com.desenv.nepalign.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="fornecedor")


public class Fornecedor extends GenericModelIGN
{
	public static final int FORNECEDOR_NAOINFORMADO = 0x0000;
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idFornecedor")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idTipoPessoa")
	private TipoPessoa tipoPessoa;
	@Column(name="dataCadastro")
	private Date dataCadastro;
	
	@Column(name="nomeFantasia")
	private String nomeFantasia;
	
	@Column(name="razaoSocial")
	private String razaoSocial;
	
	@Column(name="cnpjCpf")
	private String cnpjCpf;
	
	@Column(name="inscricaoEstadualRg")
	private String inscricaoEstadualRg;
	
	@ManyToOne
	@JoinColumn(name = "idAtividade")
	private Atividade atividade;
	@ManyToOne
	@JoinColumn(name = "idStatusEntidade")
	private StatusEntidade statusEntidade;
	@ManyToOne
	@JoinColumn(name = "idGrupoFornecedor")
	private GrupoFornecedor grupoFornecedor;
	@Column(name="nomeEtiqueta")
	private String nomeEtiqueta;
	
	@Column(name="enderecoFornecedor")
	private String enderecoFornecedor;
	
	@Column(name="complementoEndereco")
	private String complementoEndereco;
	
	@Column(name="bairro")
	private String bairro;
	
	@Column(name="cep")
	private String cep;
	
	@ManyToOne
	@JoinColumn(name = "idCidade")
	private Cidade cidade;
	@Column(name="contato")
	private String contato;
	
	@ManyToOne
	@JoinColumn(name = "idOperadoraTelefone")
	private Operadora operadoraTelefone;
	@Column(name="telefone")
	private String telefone;
	
	@Column(name="fax")
	private String fax;
	
	@Column(name="observacao")
	private String observacao;
	
	@Column(name="comentario")
	private String comentario;
	
	@Column(name="email")
	private String email;
	
	@Column(name="inscricaoMunicipal")
	private String inscricaoMunicipal;
	
	@ManyToOne
	@JoinColumn(name = "idClassificacaoFornecedor")
	private ClassificacaoFornecedor classificacaoFornecedor;
	@ManyToOne
	@JoinColumn(name = "idCategoriaFornecedor")
	private CategoriaFornecedor categoriaFornecedor;
	@ManyToOne
	@JoinColumn(name = "idNaturezaFornecedor")
	private NaturezaFornecedor naturezaFornecedor;
	@Column(name="numeroEndereco")
	private String numeroEndereco;
	
	@ManyToOne
	@JoinColumn(name = "idOperadoraFax")
	private Operadora operadoraFax;
	@Column(name="tipoFornecedor")
	private String tipoFornecedor;
	
	public Fornecedor()
	{
		super();
	}
	

	public Fornecedor
	(
	TipoPessoa tipoPessoa,
		Date dataCadastro,
		String nomeFantasia,
		String razaoSocial,
		String cnpjCpf,
		String inscricaoEstadualRg,
	Atividade atividade,
	StatusEntidade statusEntidade,
		String nomeEtiqueta,
		String enderecoFornecedor,
		String complementoEndereco,
		String bairro,
		String cep,
	Cidade cidade,
		String contato,
	Operadora operadoraTelefone,
		String telefone,
		String fax,
		String observacao,
		String comentario,
		String email,
		String inscricaoMunicipal,
	ClassificacaoFornecedor classificacaoFornecedor,
	CategoriaFornecedor categoriaFornecedor,
	NaturezaFornecedor naturezaFornecedor,
		String numeroEndereco,
	Operadora operadoraFax,
	GrupoFornecedor grupoFornecedor)
	
	{
		super();
		this.tipoPessoa = tipoPessoa;
		this.dataCadastro = dataCadastro;
		this.nomeFantasia = nomeFantasia;
		this.razaoSocial = razaoSocial;
		this.cnpjCpf = cnpjCpf;
		this.inscricaoEstadualRg = inscricaoEstadualRg;
		this.atividade = atividade;
		this.statusEntidade = statusEntidade;
		this.nomeEtiqueta = nomeEtiqueta;
		this.enderecoFornecedor = enderecoFornecedor;
		this.complementoEndereco = complementoEndereco;
		this.bairro = bairro;
		this.cep = cep;
		this.cidade = cidade;
		this.contato = contato;
		this.operadoraTelefone = operadoraTelefone;
		this.telefone = telefone;
		this.fax = fax;
		this.observacao = observacao;
		this.comentario = comentario;
		this.email = email;
		this.inscricaoMunicipal = inscricaoMunicipal;
		this.classificacaoFornecedor = classificacaoFornecedor;
		this.categoriaFornecedor = categoriaFornecedor;
		this.naturezaFornecedor = naturezaFornecedor;
		this.numeroEndereco = numeroEndereco;
		this.operadoraFax = operadoraFax;
		this.grupoFornecedor = grupoFornecedor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	

	public TipoPessoa getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(TipoPessoa tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}
	
	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public String getNomeFantasia() {
		String retorno = null;
		
		if (nomeFantasia != null)
			retorno = nomeFantasia.toUpperCase().trim();
		return retorno;
	}
	
	public void setNomeFantasia(String nomeFantasia) {
		if (nomeFantasia != null)
		{
			this.nomeFantasia = nomeFantasia.toUpperCase().trim();
		}
		else
			this.nomeFantasia = null;
			
		
	}
		
	public String getRazaoSocial() {
		String retorno = null;
		
		if (razaoSocial != null)
			retorno = razaoSocial.toUpperCase().trim();
		return retorno;
	}

	public void setRazaoSocial(String razaoSocial) {
		if (razaoSocial != null)
		{
			this.razaoSocial = razaoSocial.toUpperCase().trim();
		}
		else
			this.razaoSocial = null;
			
		
	}
		
	public String getCnpjCpf() {
		String retorno = null;
		
		if (cnpjCpf != null)
			retorno = cnpjCpf.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setCnpjCpf(String cnpjCpf) {
		if (cnpjCpf != null)
		{
			this.cnpjCpf = cnpjCpf.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.cnpjCpf = null;
			
		
	}
		
	public String getInscricaoEstadualRg() {
		String retorno = null;
		
		if (inscricaoEstadualRg != null)
			retorno = inscricaoEstadualRg.toUpperCase().trim();
		return retorno;
	}
	
	public void setInscricaoEstadualRg(String inscricaoEstadualRg) {
		if (inscricaoEstadualRg != null)
		{
			this.inscricaoEstadualRg = inscricaoEstadualRg.toUpperCase().trim();
		}
		else
			this.inscricaoEstadualRg = null;
			
		
	}
		
	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}
	
	public StatusEntidade getStatusEntidade() {
		return statusEntidade;
	}

	public void setStatusEntidade(StatusEntidade statusEntidade) {
		this.statusEntidade = statusEntidade;
	}
	
	public String getNomeEtiqueta() {
		String retorno = null;
		
		if (nomeEtiqueta != null)
			retorno = nomeEtiqueta.toUpperCase().trim();
		return retorno;
	}
	
	public void setNomeEtiqueta(String nomeEtiqueta) {
		if (nomeEtiqueta != null)
		{
			this.nomeEtiqueta = nomeEtiqueta.toUpperCase().trim();
		}
		else
			this.nomeEtiqueta = null;
			
		
	}
		
	public String getEnderecoFornecedor() {
		String retorno = null;
		
		if (enderecoFornecedor != null)
			retorno = enderecoFornecedor.toUpperCase().trim();
		return retorno;
	}
	
	public void setEnderecoFornecedor(String enderecoFornecedor) {
		if (enderecoFornecedor != null)
		{
			this.enderecoFornecedor = enderecoFornecedor.toUpperCase().trim();
		}
		else
			this.enderecoFornecedor = null;
			
		
	}
		
	public String getComplementoEndereco() {
		String retorno = null;
		
		if (complementoEndereco != null)
			retorno = complementoEndereco.toUpperCase().trim();
		return retorno;
	}
	
	public void setComplementoEndereco(String complementoEndereco) {
		if (complementoEndereco != null)
		{
			this.complementoEndereco = complementoEndereco.toUpperCase().trim();
		}
		else
			this.complementoEndereco = null;
			
		
	}
		
	public String getBairro() {
		String retorno = null;
		
		if (bairro != null)
			retorno = bairro.toUpperCase().trim();
		return retorno;
	}
	
	public void setBairro(String bairro) {
		if (bairro != null)
		{
			this.bairro = bairro.toUpperCase().trim();
		}
		else
			this.bairro = null;
			
		
	}
		
	public String getCep() {
		String retorno = null;
		
		if (cep != null)
			retorno = cep.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setCep(String cep) {
		if (cep != null)
		{
			this.cep = cep.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.cep = null;
			
		
	}
		
	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	
	public String getContato() {
		String retorno = null;
		
		if (contato != null)
			retorno = contato.toUpperCase().trim();
		return retorno;
	}
	
	public void setContato(String contato) {
		if (contato != null)
		{
			this.contato = contato.toUpperCase().trim();
		}
		else
			this.contato = null;
			
		
	}
		
	public Operadora getOperadoraTelefone() {
		return operadoraTelefone;
	}

	public void setOperadoraTelefone(Operadora operadoraTelefone) {
		this.operadoraTelefone = operadoraTelefone;
	}
	
	public String getTelefone() {
		String retorno = null;
		
		if (telefone != null)
			retorno = telefone.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setTelefone(String telefone) {
		if (telefone != null)
		{
			this.telefone = telefone.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.telefone = null;
			
		
	}
		
	public String getFax() {
		String retorno = null;
		
		if (fax != null)
			retorno = fax.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setFax(String fax) {
		if (fax != null)
		{
			this.fax = fax.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.fax = null;
			
		
	}
		
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	public String getComentario() {
		String retorno = null;
		
		if (comentario != null)
			retorno = comentario.toUpperCase().trim();
		return retorno;
	}
	
	public void setComentario(String comentario) {
		if (comentario != null)
		{
			this.comentario = comentario.toUpperCase().trim();
		}
		else
			this.comentario = null;
			
		
	}
		
	public String getEmail() {
		String retorno = null;
		
		if (email != null)
			retorno = email.toUpperCase().trim();
		return retorno;
	}
	
	public void setEmail(String email) {
		if (email != null)
		{
			this.email = email.toUpperCase().trim();
		}
		else
			this.email = null;
			
		
	}
		
	public String getInscricaoMunicipal() {
		String retorno = null;
		
		if (inscricaoMunicipal != null)
			retorno = inscricaoMunicipal.toUpperCase().trim();
		return retorno;
	}
	
	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		if (inscricaoMunicipal != null)
		{
			this.inscricaoMunicipal = inscricaoMunicipal.toUpperCase().trim();
		}
		else
			this.inscricaoMunicipal = null;
			
		
	}
		
	public ClassificacaoFornecedor getClassificacaoFornecedor() {
		return classificacaoFornecedor;
	}

	public void setClassificacaoFornecedor(ClassificacaoFornecedor classificacaoFornecedor) {
		this.classificacaoFornecedor = classificacaoFornecedor;
	}
	
	public CategoriaFornecedor getCategoriaFornecedor() {
		return categoriaFornecedor;
	}

	public void setCategoriaFornecedor(CategoriaFornecedor categoriaFornecedor) {
		this.categoriaFornecedor = categoriaFornecedor;
	}
	
	public NaturezaFornecedor getNaturezaFornecedor() {
		return naturezaFornecedor;
	}

	public void setNaturezaFornecedor(NaturezaFornecedor naturezaFornecedor) {
		this.naturezaFornecedor = naturezaFornecedor;
	}
	
	public String getNumeroEndereco() {
		String retorno = null;
		
		if (numeroEndereco != null)
			retorno = numeroEndereco.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroEndereco(String numeroEndereco) {
		if (numeroEndereco != null)
		{
			this.numeroEndereco = numeroEndereco.toUpperCase().trim();
		}
		else
			this.numeroEndereco = null;
			
		
	}
	
	
		
	public GrupoFornecedor getGrupoFornecedor()
	{
		return grupoFornecedor;
	}


	public void setGrupoFornecedor(GrupoFornecedor grupoFornecedor)
	{
		this.grupoFornecedor = grupoFornecedor;
	}


	public Operadora getOperadoraFax() {
		return operadoraFax;
	}

	public void setOperadoraFax(Operadora operadoraFax) {
		this.operadoraFax = operadoraFax;
	}
	
	public String getTipoFornecedor() {
      String retorno = null;
		
		if (tipoFornecedor != null)
			retorno = tipoFornecedor.toUpperCase().trim();
		return retorno;
	}


	public void setTipoFornecedor(String tipoFornecedor) {
		if (tipoFornecedor != null)
		{
			this.tipoFornecedor = tipoFornecedor.toUpperCase().trim();
		}
		else
			this.tipoFornecedor = null;
	}


	
	
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
