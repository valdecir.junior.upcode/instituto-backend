package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="endereco", schema="basecep")


public class Endereco extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="endereco_codigo")
	private Integer id;
		
	@Column(name="bairro_codigo")
	private Integer codigoBairro;
	
	@Column(name="endereco_cep")
	private String enderecoCep;
	
	@Column(name="endereco_logradouro")
	private String logradouroEndereco;
	
	@Column(name="endereco_complemento")
	private String complementoEndereco;
	

	public Endereco()
	{
		super();
	}
	

	public Endereco
	(
		Integer codigoBairro,
		String enderecoCep,
		String logradouroEndereco,
		String complementoEndereco
	) 
	{
		super();
		this.codigoBairro = codigoBairro;
		this.enderecoCep = enderecoCep;
		this.logradouroEndereco = logradouroEndereco;
		this.complementoEndereco = complementoEndereco;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public Integer getCodigoBairro() {
		return codigoBairro;
	}

	public void setCodigoBairro(Integer codigoBairro) {
		if (codigoBairro != null && codigoBairro == 0)
		{
			this.codigoBairro = null;
		}
		else
		{
			this.codigoBairro = codigoBairro;
		}
	}	
	public String getEnderecoCep() {
		String retorno = null;
		
		if (enderecoCep != null)
			retorno = enderecoCep.toUpperCase().trim();
		return retorno;
	}
	
	public void setEnderecoCep(String enderecoCep) {
		if (enderecoCep != null)
		{
			this.enderecoCep = enderecoCep.toUpperCase().trim();
		}
		else
			this.enderecoCep = null;
			
		
	}
		
	public String getLogradouroEndereco() {
		String retorno = null;
		
		if (logradouroEndereco != null)
			retorno = logradouroEndereco.toUpperCase().trim();
		return retorno;
	}
	
	public void setLogradouroEndereco(String logradouroEndereco) {
		if (logradouroEndereco != null)
		{
			this.logradouroEndereco = logradouroEndereco.toUpperCase().trim();
		}
		else
			this.logradouroEndereco = null;
			
		
	}
		
	public String getComplementoEndereco() {
		String retorno = null;
		
		if (complementoEndereco != null)
			retorno = complementoEndereco.toUpperCase().trim();
		return retorno;
	}
	
	public void setComplementoEndereco(String complementoEndereco) {
		if (complementoEndereco != null)
		{
			this.complementoEndereco = complementoEndereco.toUpperCase().trim();
		}
		else
			this.complementoEndereco = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
