package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="tipodocumentomovimentoestoque")


public class TipoDocumentoMovimentoEstoque extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idTipoDocumentoMovimentoEstoque")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="ativo")
	private String ativo;
	
	@Column(name="atualizaPreco")
	private String atualizaPreco;
	
	@Column(name="entradaSaida")
	private String entradaSaida;
	

	public TipoDocumentoMovimentoEstoque()
	{
		super();
	}
	

	public TipoDocumentoMovimentoEstoque
	(
		String descricao,
		String ativo,
		String atualizaPreco,
		String entradaSaida
	) 
	{
		super();
		this.descricao = descricao;
		this.ativo = ativo;
		this.atualizaPreco = atualizaPreco;
		this.entradaSaida = entradaSaida;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public String getAtivo() {
		String retorno = null;
		
		if (ativo != null)
			retorno = ativo.toUpperCase().trim();
		return retorno;
	}
	
	public void setAtivo(String ativo) {
		if (ativo != null)
		{
			this.ativo = ativo.toUpperCase().trim();
		}
		else
			this.ativo = null;
			
		
	}
		
	public String getAtualizaPreco() {
		String retorno = null;
		
		if (atualizaPreco != null)
			retorno = atualizaPreco.toUpperCase().trim();
		return retorno;
	}
	
	public void setAtualizaPreco(String atualizaPreco) {
		if (atualizaPreco != null)
		{
			this.atualizaPreco = atualizaPreco.toUpperCase().trim();
		}
		else
			this.atualizaPreco = null;
			
		
	}
		
	public String getEntradaSaida() {
		String retorno = null;
		
		if (entradaSaida != null)
			retorno = entradaSaida.toUpperCase().trim();
		return retorno;
	}
	
	public void setEntradaSaida(String entradaSaida) {
		if (entradaSaida != null)
		{
			this.entradaSaida = entradaSaida.toUpperCase().trim();
		}
		else
			this.entradaSaida = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
