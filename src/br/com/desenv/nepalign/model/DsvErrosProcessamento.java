package br.com.desenv.nepalign.model;

import java.io.Serializable;

public class DsvErrosProcessamento implements Serializable 
{
	private static final long serialVersionUID = 1L;
	private String descricaoErro;
	public String getDescricaoErro() 
	{
		return descricaoErro;
	}
	public void setDescricaoErro(String descricaoErro) {
		this.descricaoErro = descricaoErro;
	}
	public Object getObjeto() {
		return objeto;
	}
	public void setObjeto(Object objeto) {
		this.objeto = objeto;
	}
	private Object objeto;
}