package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="baixalancamentocontapagar")


public class BaixaLancamentoContaPagar extends GenericModelIGN implements Cloneable
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idBaixaLancamentoContaPagar")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idLancamentoContaPagar")
	private LancamentoContaPagar lancamentoContaPagar;
	@ManyToOne
	@JoinColumn(name = "idFormaPagamento")
	private FormaPagamento formaPagamento;
	@ManyToOne
	@JoinColumn(name = "idContaCorrente")
	private ContaCorrente contaCorrente;
	@Column(name="documento")
	private String documento;
	
	@Column(name="dataPagamento")
	private Date dataPagamento;
	
	@Column(name="valor")
	private Double valor;
	
	@Column(name="considerarBaixaIntegral")
	private Integer considerarBaixaIntegral;
	
	@Column(name="dataLancamento")
	private Date dataLancamento;
	
	@Column(name="observacao")
	private String observacao;
	
	@Column(name="valorDesconto")
	private Double valorDesconto;
	
	@Column(name="valorMulta")
	private Double valorMulta;
	
	@Column(name="valorJuros")
	private Double valorJuros;
	
	@Column(name="valorOriginal")
	private Double valorOriginal;
	
	@Column(name="codigoSeguranca")
	private String codigoSeguranca;
	
	@Column(name="numeroAgrupador")
	private String numeroAgrupador;
	

	public BaixaLancamentoContaPagar()
	{
		super();
	}
	

	public BaixaLancamentoContaPagar
	(
	LancamentoContaPagar lancamentoContaPagar,
	FormaPagamento formaPagamento,
	ContaCorrente contaCorrente,
		String documento,
		Date dataPagamento,
		Double valor,
		Integer considerarBaixaIntegral,
		Date dataLancamento,
		String observacao,
		Double valorDesconto,
		Double valorMulta,
		Double valorJuros,
		Double valorOriginal
	) 
	{
		super();
		this.lancamentoContaPagar = lancamentoContaPagar;
		this.formaPagamento = formaPagamento;
		this.contaCorrente = contaCorrente;
		this.documento = documento;
		this.dataPagamento = dataPagamento;
		this.valor = valor;
		this.considerarBaixaIntegral = considerarBaixaIntegral;
		this.dataLancamento = dataLancamento;
		this.observacao = observacao;
		this.valorDesconto = valorDesconto;
		this.valorMulta = valorMulta;
		this.valorJuros = valorJuros;
		this.valorOriginal = valorOriginal;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public LancamentoContaPagar getLancamentoContaPagar() {
		return lancamentoContaPagar;
	}

	public void setLancamentoContaPagar(LancamentoContaPagar lancamentoContaPagar) {
		this.lancamentoContaPagar = lancamentoContaPagar;
	}
	
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
	public ContaCorrente getContaCorrente() {
		return contaCorrente;
	}

	public void setContaCorrente(ContaCorrente contaCorrente) {
		this.contaCorrente = contaCorrente;
	}
	
	public String getDocumento() {
		String retorno = null;
		
		if (documento != null)
			retorno = documento.toUpperCase().trim();
		return retorno;
	}
	
	public void setDocumento(String documento) {
		if (documento != null)
		{
			this.documento = documento.toUpperCase().trim();
		}
		else
			this.documento = null;
			
		
	}
		
	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
	
		if (valor != null && Double.isNaN(valor))
		{
			this.valor = null;
		}
		else
		{
			this.valor = valor;
		}
		
	}	
	public Integer getConsiderarBaixaIntegral() {
		return considerarBaixaIntegral;
	}

	public void setConsiderarBaixaIntegral(Integer considerarBaixaIntegral) {
		this.considerarBaixaIntegral = considerarBaixaIntegral;
	}	
	public Date getDataLancamento() {
		return dataLancamento;
	}

	public void setDataLancamento(Date dataLancamento) {
		this.dataLancamento = dataLancamento;
	}
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	public Double getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(Double valorDesconto) {
	
		if (valorDesconto != null && Double.isNaN(valorDesconto))
		{
			this.valorDesconto = null;
		}
		else
		{
			this.valorDesconto = valorDesconto;
		}
		
	}	
	public Double getValorMulta() {
		return valorMulta;
	}

	public void setValorMulta(Double valorMulta) {
	
		if (valorMulta != null && Double.isNaN(valorMulta))
		{
			this.valorMulta = null;
		}
		else
		{
			this.valorMulta = valorMulta;
		}
		
	}	
	public Double getValorJuros() {
		return valorJuros;
	}

	public void setValorJuros(Double valorJuros) {
	
		if (valorJuros != null && Double.isNaN(valorJuros))
		{
			this.valorJuros = null;
		}
		else
		{
			this.valorJuros = valorJuros;
		}
		
	}	
	public Double getValorOriginal() {
		return valorOriginal;
	}

	public void setValorOriginal(Double valorOriginal) {
	
		if (valorOriginal != null && Double.isNaN(valorOriginal))
		{
			this.valorOriginal = null;
		}
		else
		{
			this.valorOriginal = valorOriginal;
		}
		
	}	
	
	public String getCodigoSeguranca() {
		return codigoSeguranca;
	}


	public void setCodigoSeguranca(String codigoSeguranca) {
		this.codigoSeguranca = codigoSeguranca;
	}


	public String getNumeroAgrupador() {
		return numeroAgrupador;
	}


	public void setNumeroAgrupador(String numeroAgrupador) {
		this.numeroAgrupador = numeroAgrupador;
	}


	public Object clone() throws CloneNotSupportedException 
	{
		return super.clone();
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
