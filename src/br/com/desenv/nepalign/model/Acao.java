package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;


@Entity
@Table(name="acao")


public class Acao extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idAcao")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	
	@ManyToOne
	@JoinColumn(name = "idFuncionalidade")
	private Funcionalidade funcionalidade;
	@Column(name="nomeMetodo")
	private String nomeMetodo;
	
	@Column(name="logarAcao")
	private String logarAcao;
	
	@Column(name="situacao")
	private String situacao;
	

	public Acao()
	{
		super();
	}
	

	public Acao
	(
		String descricao,
	Funcionalidade funcionalidade,
		String nomeMetodo,
		String logarAcao,
		String situacao
	) 
	{
		super();
		this.descricao = descricao;
		this.funcionalidade = funcionalidade;
		this.nomeMetodo = nomeMetodo;
		this.logarAcao = logarAcao;
		this.situacao = situacao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public Funcionalidade getFuncionalidade() {
		return funcionalidade;
	}

	public void setFuncionalidade(Funcionalidade funcionalidade) {
		this.funcionalidade = funcionalidade;
	}
	
	public String getNomeMetodo() {
		
		return this.nomeMetodo;
	}
	
	public void setNomeMetodo(String nomeMetodo) {
	
		
		this.nomeMetodo = nomeMetodo;
			
		
	}
		
	public String getLogarAcao() {
		String retorno = null;
		
		if (logarAcao != null)
			retorno = logarAcao.toUpperCase().trim();
		return retorno;
	}
	
	public void setLogarAcao(String logarAcao) {
		if (logarAcao != null)
		{
			this.logarAcao = logarAcao.toUpperCase().trim();
		}
		else
			this.logarAcao = null;
			
		
	}
		
	public String getSituacao() {
		String retorno = null;
		
		if (situacao != null)
			retorno = situacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setSituacao(String situacao) {
		if (situacao != null)
		{
			this.situacao = situacao.toUpperCase().trim();
		}
		else
			this.situacao = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}
	@Override
	public int compareTo(GenericModelIGN object)  
	{  
		Acao acao = (Acao) object;
		
		if (acao!=null && acao.getId()!=null && this.getId()!=null)
		{
			if (acao.getDescricao()!=null && this.getDescricao()!=null && acao.getFuncionalidade()!=null && acao.getFuncionalidade().getId()!=null && this.getFuncionalidade()!=null && this.getFuncionalidade().getId()!=null )
			{
				if (acao.getNomeMetodo().equalsIgnoreCase(this.getNomeMetodo()) && acao.getFuncionalidade().getNomeServico().equalsIgnoreCase(this.getFuncionalidade().getNomeServico()))
					return 0;
				else 
					return -1;
			}
			else
			{
				if (object.getId() > this.getId()) return 1;
			}
		}
		return -1;  
	}	
	@Override
	public boolean equals(Object object) {
		Acao acao = (Acao) object;
		
		if (acao!=null && acao.getNomeMetodo()!=null && this.getNomeMetodo()!=null)
		{
			if (acao.getNomeMetodo()!=null && this.getNomeMetodo()!=null && acao.getFuncionalidade()!=null && acao.getFuncionalidade().getNomeServico()!=null && this.getFuncionalidade()!=null && this.getFuncionalidade().getNomeServico()!=null )
			{
				if (acao.getNomeMetodo().equalsIgnoreCase(this.getNomeMetodo()) && acao.getFuncionalidade().getNomeServico().equalsIgnoreCase(this.getFuncionalidade().getNomeServico()))
					return true;
				else 
					return false;
			}
			else
			{
				return false;
			}
		}
		return false;
	}

}
