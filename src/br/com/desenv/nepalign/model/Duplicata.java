package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="Duplicata")


public class Duplicata extends GenericModelIGN implements Cloneable
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idDuplicata")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@ManyToOne
	@JoinColumn(name = "idPedidoVenda")
	private PedidoVenda pedidoVenda;
	@ManyToOne
	@JoinColumn(name = "idVendedor")
	private Vendedor vendedor;
	@Column(name="numeroDuplicata")
	private Long numeroDuplicata;

	@Column(name="dataHoraInclusao")
	private Date dataHoraInclusao;

	@Column(name="numeroFatura")
	private Long numeroFatura;

	@Column(name="dataCompra")
	private Date dataCompra;

	@Column(name="numeroNotaFiscal")
	private String numeroNotaFiscal;

	@Column(name="valorTotalCompra")
	private Double valorTotalCompra;

	@Column(name="dataVencimentoPrimeiraPrestacao")
	private Date dataVencimentoPrimeiraPrestacao;

	@Column(name="valorEntrada")
	private Double valorEntrada;

	@Column(name="numeroTotalParcela")
	private String numeroTotalParcela;
	@Column(name="semPedidoVenda")
	private String semPedidoVenda;
	@Column(name="observacao")
	private String observacao;
	@Column(name="jorrovi")
	private Integer jorrovi;

	public Duplicata()
	{
		super();
	}


	public Duplicata
	(
			EmpresaFisica empresaFisica,
			Cliente cliente,
			Usuario usuario,
			Vendedor vendedor,
			Long numeroDuplicata,
			Date dataHoraInclusao,
			Long numeroFatura,
			Date dataCompra,
			String numeroNotaFiscal,
			Double valorTotalCompra,
			Date dataVencimentoPrimeiraPrestacao,
			Double valorEntrada,
			String numeroTotalParcela,
			PedidoVenda pedidoVenda,
			String semPedidoVenda,
			String observacao,
			Integer jorrovi
			) 
	{
		super();
		this.empresaFisica = empresaFisica;
		this.cliente = cliente;
		this.usuario = usuario;
		this.vendedor = vendedor;
		this.numeroDuplicata = numeroDuplicata;
		this.dataHoraInclusao = dataHoraInclusao;
		this.numeroFatura = numeroFatura;
		this.dataCompra = dataCompra;
		this.numeroNotaFiscal = numeroNotaFiscal;
		this.valorTotalCompra = valorTotalCompra;
		this.dataVencimentoPrimeiraPrestacao = dataVencimentoPrimeiraPrestacao;
		this.valorEntrada = valorEntrada;
		this.numeroTotalParcela = numeroTotalParcela;
		this.pedidoVenda = pedidoVenda;
		this.semPedidoVenda = semPedidoVenda;
		this.observacao = observacao;
		this.jorrovi = jorrovi;
	}
	public Integer getJorrovi() {
		return jorrovi;
	}



	public void setJorrovi(Integer jorrovi) {
		this.jorrovi = jorrovi;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	

	public PedidoVenda getPedidoVenda() {
		return pedidoVenda;
	}


	public void setPedidoVenda(PedidoVenda pedidoVenda) {
		this.pedidoVenda = pedidoVenda;
	}


	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Vendedor getVendedor() {
		return vendedor;
	}

	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}

	public Long getNumeroDuplicata() {
		return numeroDuplicata;
	}

	public void setNumeroDuplicata(Long numeroDuplicata) {
		if (numeroDuplicata != null && numeroDuplicata == 0)
		{
			this.numeroDuplicata = null;
		}
		else
		{
			this.numeroDuplicata = numeroDuplicata;
		}
	}	
	public Date getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	public void setDataHoraInclusao(Date dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	public Long getNumeroFatura() {
		return numeroFatura;
	}

	public void setNumeroFatura(Long numeroFatura) {
		if (numeroFatura != null && numeroFatura == 0)
		{
			this.numeroFatura = null;
		}
		else
		{
			this.numeroFatura = numeroFatura;
		}
	}	
	public Date getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(Date dataCompra) {
		this.dataCompra = dataCompra;
	}
	public String getNumeroNotaFiscal() {
		String retorno = null;

		if (numeroNotaFiscal != null)
			retorno = numeroNotaFiscal.toUpperCase().trim();
		return retorno;
	}

	public void setNumeroNotaFiscal(String numeroNotaFiscal) {
		if (numeroNotaFiscal != null)
		{
			this.numeroNotaFiscal = numeroNotaFiscal.toUpperCase().trim();
		}
		else
			this.numeroNotaFiscal = null;


	}

	public Double getValorTotalCompra() {
		return valorTotalCompra;
	}

	public void setValorTotalCompra(Double valorTotalCompra) {
		if (valorTotalCompra != null && valorTotalCompra == 0.0)
		{
			this.valorTotalCompra = null;
		}
		else
		{
			this.valorTotalCompra = valorTotalCompra;
		}
	}	
	public Date getDataVencimentoPrimeiraPrestacao() {
		return dataVencimentoPrimeiraPrestacao;
	}

	public void setDataVencimentoPrimeiraPrestacao(Date dataVencimentoPrimeiraPrestacao) {
		this.dataVencimentoPrimeiraPrestacao = dataVencimentoPrimeiraPrestacao;
	}
	public Double getValorEntrada() {
		return valorEntrada;
	}

	public void setValorEntrada(Double valorEntrada) {
		if (valorEntrada != null && valorEntrada == 0.0)
		{
			this.valorEntrada = null;
		}
		else
		{
			this.valorEntrada = valorEntrada;
		}
	}	
	public String getNumeroTotalParcela() {
		String retorno = null;

		if (numeroTotalParcela != null)
			retorno = numeroTotalParcela.toUpperCase().trim();
		return retorno;
	}

	public void setNumeroTotalParcela(String numeroTotalParcela) {
		if (numeroTotalParcela != null)
		{
			this.numeroTotalParcela = numeroTotalParcela.toUpperCase().trim(); 
		}
		else
			this.numeroTotalParcela = null;


	}

	public String getSemPedidoVenda() {
		return semPedidoVenda;
	}


	public void setSemPedidoVenda(String semPedidoVenda) {
		this.semPedidoVenda = semPedidoVenda;
	}


	public String getObservacao() {
		return observacao;
	}


	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}




	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		 */
	}

	@Override
	public Duplicata clone() throws CloneNotSupportedException 
	{
		return (Duplicata) super.clone();
	}
}