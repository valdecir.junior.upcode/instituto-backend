package br.com.desenv.nepalign.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name = "documentoentradasaida")
public class DocumentoEntradaSaida extends GenericModelIGN
{
	public static final String CADASTRADO = "1";
	public static final String BAIXADO = "2";
	public static final String REVISADO = "3";
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idDocumentoEntradaSaida")
	private Integer id;
	@Column(name = "tipo")
	private String tipo;
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idFornecedor")
	private Fornecedor fornecedor;
	@ManyToOne
	@JoinColumn(name = "idPedidoCompraVenda")
	private PedidoCompraVenda pedidoCompraVenda;
	@Column(name = "numeroDocumentoEntradaSaida")
	private String numeroDocumentoEntradaSaida;
	@Column(name = "dataEmissao")
	private Date dataEmissao;
	@Column(name = "dataEntradaSaida")
	private Date dataEntrada;
	@Column(name = "numeroNotaFiscal")
	private String numeroNotaFiscal;
	@Column(name = "serie")
	private Integer serie;
	@Column(name = "valorTotal")
	private Double valorTotal;
	@Column(name = "valorTotalNotaFiscal")
	private Double valorTotalNotaFiscal;	
	@Column(name = "quantidadeTotal")
	private Integer quantidadeTotal;
	@Column(name = "situacao")
	private String situacao;
	@Column(name = "observacao")
	private String observacao;
	@Column(name = "dataRevisao")
	private Date dataRevisao;

	public DocumentoEntradaSaida()
	{
		super();
	}

	public DocumentoEntradaSaida(String tipo, EmpresaFisica empresaFisica, Fornecedor fornecedor, PedidoCompraVenda pedidoCompraVenda, String numeroDocumentoEntradaSaida, Date dataEmissao, Date dataEntrada, String numeroNotaFiscal, Integer serie, Double valorTotal, Integer quantidadeTotal,
			String situacao, String observacao)
	{
		super();
		this.tipo = tipo;
		this.empresaFisica = empresaFisica;
		this.fornecedor = fornecedor;
		this.pedidoCompraVenda = pedidoCompraVenda;
		this.numeroDocumentoEntradaSaida = numeroDocumentoEntradaSaida;
		this.dataEmissao = dataEmissao;
		this.dataEntrada = dataEntrada;
		this.numeroNotaFiscal = numeroNotaFiscal;
		this.serie = serie;
		this.valorTotal = valorTotal;
		this.quantidadeTotal = quantidadeTotal;
		this.situacao = situacao;
		this.observacao = observacao;

	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getTipo()
	{
		String retorno = null;

		if (tipo != null)
			retorno = tipo.toUpperCase().trim();
		
		return retorno;
	}

	public void setTipo(String tipo)
	{
		if (tipo != null)
			this.tipo = tipo.toUpperCase().trim();
		else
			this.tipo = null;
	}

	public EmpresaFisica getEmpresaFisica()
	{
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica)
	{
		this.empresaFisica = empresaFisica;
	}

	public Fornecedor getFornecedor()
	{
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor)
	{
		this.fornecedor = fornecedor;
	}

	public PedidoCompraVenda getPedidoCompraVenda()
	{
		return pedidoCompraVenda;
	}

	public void setPedidoCompraVenda(PedidoCompraVenda pedidoCompraVenda)
	{
		this.pedidoCompraVenda = pedidoCompraVenda;
	}

	public String getNumeroDocumentoEntradaSaida()
	{
		String retorno = null;

		if (numeroDocumentoEntradaSaida != null)
			retorno = numeroDocumentoEntradaSaida.toUpperCase().trim();
		
		return retorno;
	}

	public void setNumeroDocumentoEntradaSaida(String numeroDocumentoEntradaSaida)
	{
		if (numeroDocumentoEntradaSaida != null)
			this.numeroDocumentoEntradaSaida = numeroDocumentoEntradaSaida.toUpperCase().trim();
		else
			this.numeroDocumentoEntradaSaida = null;
	}

	public Date getDataEmissao()
	{
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao)
	{
		this.dataEmissao = dataEmissao;
	}

	public Date getDataEntrada()
	{
		return dataEntrada;
	}

	public void setDataEntrada(Date dataEntrada)
	{
		this.dataEntrada = dataEntrada;
	}

	public String getNumeroNotaFiscal()
	{
		String retorno = null;

		if (numeroNotaFiscal != null)
			retorno = numeroNotaFiscal.toUpperCase().trim();
		
		return retorno;
	}

	public void setNumeroNotaFiscal(String numeroNotaFiscal)
	{
		if (numeroNotaFiscal != null)
			this.numeroNotaFiscal = numeroNotaFiscal.toUpperCase().trim();
		else
			this.numeroNotaFiscal = null;
	}

	public Integer getSerie()
	{
		return serie;
	}

	public void setSerie(Integer serie)
	{
		this.serie = serie;
	}

	public Double getValorTotal()
	{
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal)
	{
		if (valorTotal != null && Double.isNaN(valorTotal))
			this.valorTotal = null;
		else
			this.valorTotal = valorTotal;
	}

	public Integer getQuantidadeTotal()
	{
		return quantidadeTotal;
	}

	public void setQuantidadeTotal(Integer quantidadeTotal)
	{
		this.quantidadeTotal = quantidadeTotal;
	}

	public String getSituacao()
	{
		String retorno = null;

		if (situacao != null)
			retorno = situacao.toUpperCase().trim();
		
		return retorno;
	}

	public void setSituacao(String situacao)
	{
		if (situacao != null)
			this.situacao = situacao.toUpperCase().trim();
		else
			this.situacao = null;
	}

	public String getObservacao()
	{
		String retorno = null;

		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		
		return retorno;
	}

	public void setObservacao(String observacao)
	{
		if (observacao != null)
			this.observacao = observacao.toUpperCase().trim();
		else
			this.observacao = null;
	}

	public Double getValorTotalNotaFiscal()
	{
		return valorTotalNotaFiscal;
	}

	public void setValorTotalNotaFiscal(Double valorTotalNotaFiscal)
	{
		if (valorTotalNotaFiscal != null && Double.isNaN(valorTotalNotaFiscal))
			this.valorTotalNotaFiscal = null;
		else
			this.valorTotalNotaFiscal = valorTotalNotaFiscal;	
	}

	public Date getDataRevisao() 
	{
		return dataRevisao;
	}

	public void setDataRevisao(Date dataRevisao)
	{
		this.dataRevisao = dataRevisao;
	}

	@Override
	public void validate() throws Exception { }
}