package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="bairro", schema="basecep")


public class Bairro extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="bairro_codigo")
	private Integer id;
		
	@Column(name="cidade_codigo")
	private Integer cidadeCodigo;
	
	@Column(name="bairro_descricao")
	private String descricaoBairro;
	

	public Bairro()
	{
		super();
	}
	

	public Bairro
	(
		Integer cidadeCodigo,
		String descricaoBairro
	) 
	{
		super();
		this.cidadeCodigo = cidadeCodigo;
		this.descricaoBairro = descricaoBairro;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public Integer getCidadeCodigo() {
		return cidadeCodigo;
	}

	public void setCidadeCodigo(Integer cidadeCodigo) {
		if (cidadeCodigo != null && cidadeCodigo == 0)
		{
			this.cidadeCodigo = null;
		}
		else
		{
			this.cidadeCodigo = cidadeCodigo;
		}
	}	
	public String getDescricaoBairro() {
		String retorno = null;
		
		if (descricaoBairro != null)
			retorno = descricaoBairro.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricaoBairro(String descricaoBairro) {
		if (descricaoBairro != null)
		{
			this.descricaoBairro = descricaoBairro.toUpperCase().trim();
		}
		else
			this.descricaoBairro = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
