package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name = "sequenciabasebalanco")
public class SequenciaBaseBalanco extends GenericModelIGN {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idSequenciaBaseBalanco")
	private Integer id;
	
	@Column(name = "sequenciaBase")
	private Integer sequenciaBase;
	
	@Column(name = "descricao")
	private String descricao;

	public SequenciaBaseBalanco() {
		super();
	}

	public SequenciaBaseBalanco(Integer sequenciaBase, String descricao) {
		super();
		this.sequenciaBase = sequenciaBase;
		this.descricao = descricao;
	}

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSequenciaBase() {
		return sequenciaBase;
	}

	public void setSequenciaBase(Integer sequenciaBase) {
		this.sequenciaBase = sequenciaBase;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public void validate() throws Exception {
	}

}
