package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="bandeiracartao")
public class BandeiraCartao extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idBandeiracartao")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	

	public BandeiraCartao()
	{
		super();
	}
	
	public BandeiraCartao
	(
		String descricao
	) 
	{
		super();
		this.descricao = descricao;
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public String getDescricao() 
	{
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setDescricao(String descricao) 
	{
		if (descricao != null)
			this.descricao = descricao.toUpperCase().trim();
		else
			this.descricao = null;
	}
		
	@Override
	public void validate() throws Exception { }
}