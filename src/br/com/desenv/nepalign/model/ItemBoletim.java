package br.com.desenv.nepalign.model;

public class ItemBoletim {
private String descricao;
private Double credito;
private Double debito;
public String getDescricao() {
	return descricao;
}
public void setDescricao(String descricao) {
	this.descricao = descricao;
}
public Double getCredito() {
	return credito;
}
public void setCredito(Double credito) {
	this.credito = credito;
}
public Double getDebito() {
	return debito;
}
public void setDebito(Double debito) {
	this.debito = debito;
}




}
