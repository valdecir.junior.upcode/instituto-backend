package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="produtopromocao")


public class ProdutoPromocao extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idProdutoPromocao")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idProduto")
	private Produto produto;
	@Column(name="precoPromocional")
	private Double precoPromocional;
	
	@Column(name="dataInicial")
	private Date dataInicial;
	
	@Column(name="dataFinal")
	private Date dataFinal;
	

	public ProdutoPromocao()
	{
		super();
	}
	

	public ProdutoPromocao
	(
	EmpresaFisica empresaFisica,
	Produto produto,
		Double precoPromocional,
		Date dataInicial,
		Date dataFinal
	) 
	{
		super();
		this.empresaFisica = empresaFisica;
		this.produto = produto;
		this.precoPromocional = precoPromocional;
		this.dataInicial = dataInicial;
		this.dataFinal = dataFinal;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	public Double getPrecoPromocional() {
		return precoPromocional;
	}

	public void setPrecoPromocional(Double precoPromocional) {
	
		if (precoPromocional != null && Double.isNaN(precoPromocional))
		{
			this.precoPromocional = null;
		}
		else
		{
			this.precoPromocional = precoPromocional;
		}
		
	}	
	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}
	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
