package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name = "parametrosboletoconta")
public class ParametrosBoletoConta extends GenericModelIGN {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idParametrosBoletoConta")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "idContaCorrente")
	private ContaCorrente contaCorrente;

	@Column(name = "ano")
	private Integer ano;

	@Column(name = "codigoCedente")
	private Integer codigoCedente;

	@Column(name = "posto")
	private Integer posto;

	@Column(name = "sequencial")
	private Integer sequencial;

	@Column(name = "instrucaoPadrao")
	private Integer instrucaoPadrao;

	@Column(name = "carteiraCobranca")
	private Integer carteiraCobranca;

	@Column(name = "ativoInativo")
	private Integer ativoInativo;

	@Column(name = "geracaoNossoNumero")
	private Integer geracaoNossoNumero;

	public ParametrosBoletoConta() {
		super();
	}

	public ParametrosBoletoConta(ContaCorrente contaCorrente, Integer ano,
			Integer codigoCedente, Integer posto, Integer sequencial,
			Integer instrucaoPadrao, Integer carteiraCobranca,
			Integer ativoInativo, Integer geracaoNossoNumero) {
		super();
		this.contaCorrente = contaCorrente;
		this.ano = ano;
		this.codigoCedente = codigoCedente;
		this.posto = posto;
		this.sequencial = sequencial;
		this.instrucaoPadrao = instrucaoPadrao;
		this.carteiraCobranca = carteiraCobranca;
		this.ativoInativo = ativoInativo;
		this.geracaoNossoNumero = geracaoNossoNumero;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ContaCorrente getContaCorrente() {
		return contaCorrente;
	}

	public void setContaCorrente(ContaCorrente contaCorrente) {
		this.contaCorrente = contaCorrente;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getCodigoCedente() {
		return codigoCedente;
	}

	public void setCodigoCedente(Integer codigoCedente) {
		this.codigoCedente = codigoCedente;
	}

	public Integer getPosto() {
		return posto;
	}

	public void setPosto(Integer posto) {
		this.posto = posto;
	}

	public Integer getSequencial() {
		return sequencial;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}

	public Integer getInstrucaoPadrao() {
		return instrucaoPadrao;
	}

	public void setInstrucaoPadrao(Integer instrucaoPadrao) {
		this.instrucaoPadrao = instrucaoPadrao;
	}

	public Integer getCarteiraCobranca() {
		return carteiraCobranca;
	}

	public void setCarteiraCobranca(Integer carteiraCobranca) {
		this.carteiraCobranca = carteiraCobranca;
	}

	public Integer getAtivoInativo() {
		return ativoInativo;
	}

	public void setAtivoInativo(Integer ativoInativo) {
		this.ativoInativo = ativoInativo;
	}

	public Integer getGeracaoNossoNumero() {
		return geracaoNossoNumero;
	}

	public void setGeracaoNossoNumero(Integer geracaoNossoNumero) {
		this.geracaoNossoNumero = geracaoNossoNumero;
	}

	@Override
	public void validate() throws Exception {
		// Coloque aqui suas validações ignorantes
		/*
		 * if (this.descricao == null || this.descricao.trim().equals("")) {
		 * throw new Exception("Descrição é obrigatório."); }
		 */

	}

}
