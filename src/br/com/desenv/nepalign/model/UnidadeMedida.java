package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="unidademedida")
public class UnidadeMedida extends GenericModelIGN
{
	public static final int UNIDADEMEDIDA_PAR = 0x0002;
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idUnidadeMedida")
	private Integer id;
		
	@Column(name="sigla")
	private String sigla;
	
	@Column(name="descricao")
	private String descricao;
	

	public UnidadeMedida()
	{
		super();
	}
	

	public UnidadeMedida
	(
		String sigla,
		String descricao
	) 
	{
		super();
		this.sigla = sigla;
		this.descricao = descricao;
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}	
	
	public String getSigla() 
	{
		String retorno = null;
		
		if (sigla != null)
			retorno = sigla.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setSigla(String sigla) 
	{
		if (sigla != null)
			this.sigla = sigla.toUpperCase().trim();
		else
			this.sigla = null;
	}
		
	public String getDescricao() 
	{
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setDescricao(String descricao)
	{
		if (descricao != null)
			this.descricao = descricao.toUpperCase().trim();
		else
			this.descricao = null;	
	}
		
	@Override
	public void validate() throws Exception { }

	@Override
	public boolean equals(Object object) 
	{
		if(object == null)
			return false;
		if(!object.getClass().equals(this.getClass()))
			return false;
		
		if(((UnidadeMedida) object).getDescricao().equals(getDescricao()))
			return true;
		
		return super.equals(object);
	}
}