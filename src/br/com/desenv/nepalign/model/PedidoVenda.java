package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import java.text.Normalizer;

import javax.persistence.Lob;

import org.hibernate.annotations.Cascade;
import org.hibernate.envers.CrossTypeRevisionChangesReader;

import flex.messaging.messages.ErrorMessage;
import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;
import java.util.List;

@Entity
@Table(name="pedidovenda")


public class PedidoVenda extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idPedidoVenda")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "idTipoMovimentacaoEstoque")
	private TipoMovimentacaoEstoque tipoMovimentacaoEstoque;
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;

	@ManyToOne
	@JoinColumn(name = "idVendedor")
	private Vendedor vendedor;
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@ManyToOne
	@JoinColumn(name = "idSituacaoPedidoVenda")
	private SituacaoPedidoVenda situacaoPedidoVenda;
	@ManyToOne
	@JoinColumn(name = "IdPlanoPagamento")
	private PlanoPagamento planoPagamento;
	@ManyToOne
	@JoinColumn(name = "idMetodoPagamento")
	private MetodoPagamento metodoPagamento;
	@ManyToOne
	@JoinColumn(name = "idCidadeEntrega")
	private Cidade cidadeEntrega;
	@ManyToOne
	@JoinColumn(name = "idUfEntrega")
	private Uf ufEntrega;
	@ManyToOne
	@JoinColumn(name = "idPaisEntrega")
	private Pais paisEntrega;
	@ManyToOne
	@JoinColumn(name = "idFormaEntrega")
	private FormaEntrega formaEntrega;
	@ManyToOne
	@JoinColumn(name = "idStatusPedido")
	private StatusPedido statusPedido;
	@ManyToOne
	@JoinColumn(name = "idMeioPagamento")
	private MeioPagamento tipoMeioPagamento;
	@ManyToOne
	@JoinColumn(name = "idTipoLogradouroEnderecoEntrega")
	private TipoLogradouro tipoLogradouroEnderecoEntrega;
	@Column(name="dataVenda")
	private Date dataVenda;

	@Column(name="descontoTotal")
	private Double descontoTotal;

	@Column(name="numeroControle")
	private String numeroControle;

	@Column(name="valorTotalPedido")
	private Double valorTotalPedido;

	@Column(name="codigoPedidoEnviadoCliente")
	private String codigoPedidoEnviadoCliente;

	@Column(name="valorTotalProdutosPedidos")
	private Double valorTotalProdutosPedidos;

	@Column(name="valorTotalCustoFrete")
	private Double valorTotalCustoFrete;

	@Column(name="valorTotalDescProdutos")
	private Double valorTotalDescProdutos;

	@Column(name="valorTotalDescFrete")
	private Double valorTotalDescFrete;

	@Column(name="numParcelas")
	private Integer numParcelas;

	@Column(name="valorParcela")
	private Double valorParcela;

	@Column(name="totalDescPedido")
	private Double totalDescPedido;

	@Column(name="dataExpiracaoPedido")
	private Date dataExpiracaoPedido;

	@Column(name="logradouroEnderecoEntrega")
	private String logradouroEnderecoEntrega;

	@Column(name="bairroEnderecoEntrega")
	private String bairroEnderecoEntrega;

	@Column(name="numEnderecoEntrega")
	private String numEnderecoEntrega;

	@Column(name="complementoEnderecoEntrega")
	private String complementoEnderecoEntrega;

	@Column(name="cepEnderecoEntrega")
	private String cepEnderecoEntrega;

	@Column(name="numTelefone")
	private String numTelefone;

	@Column(name="dddTelefone")
	private String dddTelefone;

	@Column(name="dataEntregaPedido")
	private Date dataEntregaPedido;

	@Column(name="valorEmbalagemPresente")
	private Double valorEmbalagemPresente;

	@Column(name="valorJuros")
	private Double valorJuros;

	@Column(name="valorCupomDesconto")
	private Double valorCupomDesconto;

	@Column(name="numCupomDesconto")
	private Integer numCupomDesconto;

	@Column(name="referenciaEnderecoEntrega")
	private String referenciaEnderecoEntrega;

	@Column(name="observacao")
	private String observacao;

	@ManyToOne
	@JoinColumn(name = "idUsuarioAutorizador")
	private Usuario usuarioAutorizador;

	@Column(name = "codigoExterno")
	private Integer codigoExterno;

	@Column(name = "destinatarioPedido")
	private String destinatarioPedido;

	@Column(name = "codigoPlano")
	private String codigoPlano;

	@Column (name="situacaoFila")
	private Integer situacaoFila;

	@Column (name="valorTotalVendaAjustado")
	private Double valorTotalVendaAjustado;

	@Transient
	private List<ItemPedidoVenda> listaItensPedidoVenda;

	@Transient
	private List<PagamentoPedidoVenda> listaPagamentosPedidoVenda;
	public List<ItemPedidoVenda> getListaItensPedidoVenda() {
		return listaItensPedidoVenda;
	}

	public void setListaItensPedidoVenda(List<ItemPedidoVenda> listaItensPedidoVenda) {
		this.listaItensPedidoVenda = listaItensPedidoVenda;
	}

	public List<PagamentoPedidoVenda> getListaPagamentosPedidoVenda() {
		return listaPagamentosPedidoVenda;
	}

	public void setListaPagamentosPedidoVenda(
			List<PagamentoPedidoVenda> listaPagamentosPedidoVenda) {
		this.listaPagamentosPedidoVenda = listaPagamentosPedidoVenda;
	}

	public Integer getSituacaoFila() {
		return situacaoFila;
	}

	public void setSituacaoFila(Integer situacaoFila) {
		this.situacaoFila = situacaoFila;
	}


	public PedidoVenda()
	{
		super();
	}


	public PedidoVenda
	(
			TipoMovimentacaoEstoque tipoMovimentacaoEstoque,
			EmpresaFisica empresaFisica,
			Vendedor vendedor,
			Cliente cliente,
			Usuario usuario,
			SituacaoPedidoVenda situacaoPedidoVenda,
			PlanoPagamento planoPagamento,
			MetodoPagamento metodoPagamento,
			Cidade cidadeEntrega,
			Uf ufEntrega,
			Pais paisEntrega,
			FormaEntrega formaEntrega,
			StatusPedido statusPedido,
			MeioPagamento tipoMeioPagamento,
			TipoLogradouro tipoLogradouroEnderecoEntrega,
			Date dataVenda,
			Double descontoTotal,
			String numeroControle,
			Double valorTotalPedido,
			String codigoPedidoEnviadoCliente,
			Double valorTotalProdutosPedidos,
			Double valorTotalCustoFrete,
			Double valorTotalDescProdutos,
			Double valorTotalDescFrete,
			Integer numParcelas,
			Double valorParcela,
			Double totalDescPedido,
			Date dataExpiracaoPedido,
			String logradouroEnderecoEntrega,
			String bairroEnderecoEntrega,
			String numEnderecoEntrega,
			String complementoEnderecoEntrega,
			String cepEnderecoEntrega,
			String numTelefone,
			String dddTelefone,
			Date dataEntregaPedido,
			Double valorEmbalagemPresente,
			Double valorJuros,
			Double valorCupomDesconto,
			Integer numCupomDesconto,
			String referenciaEnderecoEntrega,
			String observacao,
			Usuario usuarioAutorizador,
			String codigoPlano,
			Double valorTotalVendaAjustado
			) 
	{
		super();
		this.tipoMovimentacaoEstoque = tipoMovimentacaoEstoque;
		this.empresaFisica = empresaFisica;
		this.vendedor = vendedor;
		this.cliente = cliente;
		this.usuario = usuario;
		this.situacaoPedidoVenda = situacaoPedidoVenda;
		this.planoPagamento = planoPagamento;
		this.metodoPagamento = metodoPagamento;
		this.cidadeEntrega = cidadeEntrega;
		this.ufEntrega = ufEntrega;
		this.paisEntrega = paisEntrega;
		this.formaEntrega = formaEntrega;
		this.statusPedido = statusPedido;
		this.tipoMeioPagamento = tipoMeioPagamento;
		this.tipoLogradouroEnderecoEntrega = tipoLogradouroEnderecoEntrega;
		this.dataVenda = dataVenda;
		this.descontoTotal = descontoTotal;
		this.numeroControle = numeroControle;
		this.valorTotalPedido = valorTotalPedido;
		this.codigoPedidoEnviadoCliente = codigoPedidoEnviadoCliente;
		this.valorTotalProdutosPedidos = valorTotalProdutosPedidos;
		this.valorTotalCustoFrete = valorTotalCustoFrete;
		this.valorTotalDescProdutos = valorTotalDescProdutos;
		this.valorTotalDescFrete = valorTotalDescFrete;
		this.numParcelas = numParcelas;
		this.valorParcela = valorParcela;
		this.totalDescPedido = totalDescPedido;
		this.dataExpiracaoPedido = dataExpiracaoPedido;
		this.logradouroEnderecoEntrega = logradouroEnderecoEntrega;
		this.bairroEnderecoEntrega = bairroEnderecoEntrega;
		this.numEnderecoEntrega = numEnderecoEntrega;
		this.complementoEnderecoEntrega = complementoEnderecoEntrega;
		this.cepEnderecoEntrega = cepEnderecoEntrega;
		this.numTelefone = numTelefone;
		this.dddTelefone = dddTelefone;
		this.dataEntregaPedido = dataEntregaPedido;
		this.valorEmbalagemPresente = valorEmbalagemPresente;
		this.valorJuros = valorJuros;
		this.valorCupomDesconto = valorCupomDesconto;
		this.numCupomDesconto = numCupomDesconto;
		this.referenciaEnderecoEntrega = referenciaEnderecoEntrega;
		this.observacao = observacao;
		this.usuarioAutorizador = usuarioAutorizador;
		this.codigoPlano = codigoPlano;
		this.valorTotalVendaAjustado = valorTotalVendaAjustado;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public TipoMovimentacaoEstoque getTipoMovimentacaoEstoque() {
		return tipoMovimentacaoEstoque;
	}

	public void setTipoMovimentacaoEstoque(TipoMovimentacaoEstoque tipoMovimentacaoEstoque) {
		this.tipoMovimentacaoEstoque = tipoMovimentacaoEstoque;
	}

	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}

	public Vendedor getVendedor() {
		return vendedor;
	}

	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public SituacaoPedidoVenda getSituacaoPedidoVenda() {
		return situacaoPedidoVenda;
	}

	public void setSituacaoPedidoVenda(SituacaoPedidoVenda situacaoPedidoVenda) {
		this.situacaoPedidoVenda = situacaoPedidoVenda;
	}

	public PlanoPagamento getPlanoPagamento() {
		return planoPagamento;
	}

	public void setPlanoPagamento(PlanoPagamento planoPagamento) {
		this.planoPagamento = planoPagamento;
	}

	public MetodoPagamento getMetodoPagamento() {
		return metodoPagamento;
	}

	public void setMetodoPagamento(MetodoPagamento metodoPagamento) {
		this.metodoPagamento = metodoPagamento;
	}

	public Cidade getCidadeEntrega() {
		return cidadeEntrega;
	}

	public void setCidadeEntrega(Cidade cidadeEntrega) {
		this.cidadeEntrega = cidadeEntrega;
	}

	public Uf getUfEntrega() {
		return ufEntrega;
	}

	public void setUfEntrega(Uf ufEntrega) {
		this.ufEntrega = ufEntrega;
	}

	public Pais getPaisEntrega() {
		return paisEntrega;
	}

	public void setPaisEntrega(Pais paisEntrega) {
		this.paisEntrega = paisEntrega;
	}

	public FormaEntrega getFormaEntrega() {
		return formaEntrega;
	}

	public void setFormaEntrega(FormaEntrega formaEntrega) {
		this.formaEntrega = formaEntrega;
	}

	public StatusPedido getStatusPedido() {
		return statusPedido;
	}

	public void setStatusPedido(StatusPedido statusPedido) {
		this.statusPedido = statusPedido;
	}

	public MeioPagamento getTipoMeioPagamento() {
		return tipoMeioPagamento;
	}

	public void setTipoMeioPagamento(MeioPagamento tipoMeioPagamento) {
		this.tipoMeioPagamento = tipoMeioPagamento;
	}

	public TipoLogradouro getTipoLogradouroEnderecoEntrega() {
		return tipoLogradouroEnderecoEntrega;
	}

	public void setTipoLogradouroEnderecoEntrega(TipoLogradouro tipoLogradouroEnderecoEntrega) {
		this.tipoLogradouroEnderecoEntrega = tipoLogradouroEnderecoEntrega;
	}

	public Date getDataVenda() {
		return dataVenda;
	}

	public void setDataVenda(Date dataVenda) {
		this.dataVenda = dataVenda;
	}
	public Double getDescontoTotal() {
		return descontoTotal;
	}

	public void setDescontoTotal(Double descontoTotal) {
		if (descontoTotal != null && descontoTotal == 0.0)
		{
			this.descontoTotal = null;
		}
		else
		{
			this.descontoTotal = descontoTotal;
		}
	}	
	public String getNumeroControle() {
		String retorno = null;

		if (numeroControle != null)
			retorno = numeroControle.toUpperCase().trim();
		return retorno;
	}

	public void setNumeroControle(String numeroControle) {
		if (numeroControle != null)
		{
			this.numeroControle = numeroControle.toUpperCase().trim();
		}
		else
			this.numeroControle = null;


	}

	public Double getValorTotalPedido() {
		return valorTotalPedido;
	}

	public void setValorTotalPedido(Double valorTotalPedido) {
		if (valorTotalPedido != null && Double.isNaN(valorTotalPedido))
		{
			this.valorTotalPedido = null;
		}
		else
		{
			this.valorTotalPedido = valorTotalPedido;
		}
	}	
	public String getCodigoPedidoEnviadoCliente() {
		String retorno = null;

		if (codigoPedidoEnviadoCliente != null)
			retorno = codigoPedidoEnviadoCliente.toUpperCase().trim();
		return retorno;
	}

	public void setCodigoPedidoEnviadoCliente(String codigoPedidoEnviadoCliente) {
		if (codigoPedidoEnviadoCliente != null)
		{
			this.codigoPedidoEnviadoCliente = codigoPedidoEnviadoCliente.toUpperCase().trim();
		}
		else
			this.codigoPedidoEnviadoCliente = null;


	}

	public Double getValorTotalProdutosPedidos() {
		return valorTotalProdutosPedidos;
	}

	public void setValorTotalProdutosPedidos(Double valorTotalProdutosPedidos) {
		if (valorTotalProdutosPedidos != null && valorTotalProdutosPedidos == 0.0)
		{
			this.valorTotalProdutosPedidos = null;
		}
		else
		{
			this.valorTotalProdutosPedidos = valorTotalProdutosPedidos;
		}
	}	
	public Double getValorTotalCustoFrete() {
		return valorTotalCustoFrete;
	}

	public void setValorTotalCustoFrete(Double valorTotalCustoFrete) {
		if (valorTotalCustoFrete != null && valorTotalCustoFrete == 0.0)
		{
			this.valorTotalCustoFrete = null;
		}
		else
		{
			this.valorTotalCustoFrete = valorTotalCustoFrete;
		}
	}	
	public Double getValorTotalDescProdutos() {
		return valorTotalDescProdutos;
	}

	public void setValorTotalDescProdutos(Double valorTotalDescProdutos) {
		if (valorTotalDescProdutos != null && valorTotalDescProdutos == 0.0)
		{
			this.valorTotalDescProdutos = null;
		}
		else
		{
			this.valorTotalDescProdutos = valorTotalDescProdutos;
		}
	}	
	public Double getValorTotalDescFrete() {
		return valorTotalDescFrete;
	}

	public void setValorTotalDescFrete(Double valorTotalDescFrete) {
		if (valorTotalDescFrete != null && valorTotalDescFrete == 0.0)
		{
			this.valorTotalDescFrete = null;
		}
		else
		{
			this.valorTotalDescFrete = valorTotalDescFrete;
		}
	}	
	public Integer getNumParcelas() {
		return numParcelas;
	}

	public void setNumParcelas(Integer numParcelas) {
		if (numParcelas != null && numParcelas == 0)
		{
			this.numParcelas = null;
		}
		else
		{
			this.numParcelas = numParcelas;
		}
	}	
	public Double getValorParcela() {
		return valorParcela;
	}

	public void setValorParcela(Double valorParcela) {
		if (valorParcela != null && valorParcela == 0.0)
		{
			this.valorParcela = null;
		}
		else
		{
			this.valorParcela = valorParcela;
		}
	}	
	public Double getTotalDescPedido() {
		return totalDescPedido;
	}

	public void setTotalDescPedido(Double totalDescPedido) {
		if (totalDescPedido != null && totalDescPedido == 0.0)
		{
			this.totalDescPedido = null;
		}
		else
		{
			this.totalDescPedido = totalDescPedido;
		}
	}	
	public Date getDataExpiracaoPedido() {
		return dataExpiracaoPedido;
	}

	public void setDataExpiracaoPedido(Date dataExpiracaoPedido) {
		this.dataExpiracaoPedido = dataExpiracaoPedido;
	}
	public String getLogradouroEnderecoEntrega() {
		String retorno = null;

		if (logradouroEnderecoEntrega != null)
			retorno = logradouroEnderecoEntrega.toUpperCase().trim();
		return retorno;
	}

	public void setLogradouroEnderecoEntrega(String logradouroEnderecoEntrega) {
		if (logradouroEnderecoEntrega != null)
		{
			this.logradouroEnderecoEntrega = logradouroEnderecoEntrega.toUpperCase().trim();
		}
		else
			this.logradouroEnderecoEntrega = null;


	}

	public String getBairroEnderecoEntrega() {
		String retorno = null;

		if (bairroEnderecoEntrega != null)
			retorno = bairroEnderecoEntrega.toUpperCase().trim();
		return retorno;
	}

	public void setBairroEnderecoEntrega(String bairroEnderecoEntrega) {
		if (bairroEnderecoEntrega != null)
		{
			this.bairroEnderecoEntrega = bairroEnderecoEntrega.toUpperCase().trim();
		}
		else
			this.bairroEnderecoEntrega = null;


	}

	public String getNumEnderecoEntrega() {
		String retorno = null;

		if (numEnderecoEntrega != null)
			retorno = numEnderecoEntrega.toUpperCase().trim();
		return retorno;
	}

	public void setNumEnderecoEntrega(String numEnderecoEntrega) {
		if (numEnderecoEntrega != null)
		{
			this.numEnderecoEntrega = numEnderecoEntrega.toUpperCase().trim();
		}
		else
			this.numEnderecoEntrega = null;


	}

	public String getComplementoEnderecoEntrega() {
		String retorno = null;

		if (complementoEnderecoEntrega != null)
			retorno = complementoEnderecoEntrega.toUpperCase().trim();
		return retorno;
	}

	public void setComplementoEnderecoEntrega(String complementoEnderecoEntrega) {
		if (complementoEnderecoEntrega != null)
		{
			this.complementoEnderecoEntrega = complementoEnderecoEntrega.toUpperCase().trim();
		}
		else
			this.complementoEnderecoEntrega = null;


	}

	public String getCepEnderecoEntrega() {
		String retorno = null;

		if (cepEnderecoEntrega != null)
			retorno = cepEnderecoEntrega.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}

	public void setCepEnderecoEntrega(String cepEnderecoEntrega) {
		if (cepEnderecoEntrega != null)
		{
			this.cepEnderecoEntrega = cepEnderecoEntrega.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.cepEnderecoEntrega = null;


	}

	public String getNumTelefone() {
		String retorno = null;

		if (numTelefone != null)
			retorno = numTelefone.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}

	public void setNumTelefone(String numTelefone) {
		if (numTelefone != null)
		{
			this.numTelefone = numTelefone.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.numTelefone = null;


	}

	public String getDddTelefone() {
		String retorno = null;

		if (dddTelefone != null)
			retorno = dddTelefone.toUpperCase().trim();
		return retorno;
	}

	public void setDddTelefone(String dddTelefone) {
		if (dddTelefone != null)
		{
			this.dddTelefone = dddTelefone.toUpperCase().trim();
		}
		else
			this.dddTelefone = null;


	}

	public Date getDataEntregaPedido() {
		return dataEntregaPedido;
	}

	public void setDataEntregaPedido(Date dataEntregaPedido) {
		this.dataEntregaPedido = dataEntregaPedido;
	}
	public Double getValorEmbalagemPresente() {
		return valorEmbalagemPresente;
	}

	public void setValorEmbalagemPresente(Double valorEmbalagemPresente) {
		if (valorEmbalagemPresente != null && valorEmbalagemPresente == 0.0)
		{
			this.valorEmbalagemPresente = null;
		}
		else
		{
			this.valorEmbalagemPresente = valorEmbalagemPresente;
		}
	}	
	public Double getValorJuros() {
		return valorJuros;
	}

	public void setValorJuros(Double valorJuros) {
		if (valorJuros != null && valorJuros == 0.0)
		{
			this.valorJuros = null;
		}
		else
		{
			this.valorJuros = valorJuros;
		}
	}	
	public Double getValorCupomDesconto() {
		return valorCupomDesconto;
	}

	public void setValorCupomDesconto(Double valorCupomDesconto) {
		if (valorCupomDesconto != null && valorCupomDesconto == 0.0)
		{
			this.valorCupomDesconto = null;
		}
		else
		{
			this.valorCupomDesconto = valorCupomDesconto;
		}
	}	
	public Integer getNumCupomDesconto() {
		return numCupomDesconto;
	}

	public void setNumCupomDesconto(Integer numCupomDesconto) {
		if (numCupomDesconto != null && numCupomDesconto == 0)
		{
			this.numCupomDesconto = null;
		}
		else
		{
			this.numCupomDesconto = numCupomDesconto;
		}
	}	
	public String getReferenciaEnderecoEntrega() {
		String retorno = null;

		if (referenciaEnderecoEntrega != null)
			retorno = referenciaEnderecoEntrega.toUpperCase().trim();
		return retorno;
	}

	public void setReferenciaEnderecoEntrega(String referenciaEnderecoEntrega) {
		if (referenciaEnderecoEntrega != null)
		{
			this.referenciaEnderecoEntrega = referenciaEnderecoEntrega.toUpperCase().trim();
		}
		else
			this.referenciaEnderecoEntrega = null;


	}

	public String getObservacao() {
		String retorno = null;

		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}

	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;


	}

	public Usuario getUsuarioAutorizador() {
		return usuarioAutorizador;
	}

	public void setUsuarioAutorizador(Usuario usuarioAutorizador) {
		this.usuarioAutorizador = usuarioAutorizador;
	}

	public Integer getCodigoExterno()
	{
		return this.codigoExterno;
	}

	public void setCodigoExterno(Integer codigoExterno)
	{
		if (codigoExterno != null && codigoExterno == 0)
		{
			this.codigoExterno = null;
		}
		else
		{
			this.codigoExterno = codigoExterno;
		}
	}

	public String getDestinatarioPedido() {
		return destinatarioPedido;
	}


	public void setDestinatarioPedido(String destinatarioPedido) {
		this.destinatarioPedido = destinatarioPedido;
	}


	public String getCodigoPlano() {
		return codigoPlano; 
	}    


	public void setCodigoPlano(String codigoPlano) {
		this.codigoPlano = codigoPlano;
	}

	public Double getValorTotalVendaAjustado() {
		return valorTotalVendaAjustado;
	}

	public void setValorTotalVendaAjustado(Double valorTotalVendaAjustado) {
		if (valorTotalVendaAjustado != null && valorTotalVendaAjustado == 0.0)
			this.valorTotalVendaAjustado = null;
		else
			this.valorTotalVendaAjustado = valorTotalVendaAjustado;
	}


	public PedidoVenda clone() throws CloneNotSupportedException
	{
		return (PedidoVenda) super.clone();
	}

	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		 */

	}

}
