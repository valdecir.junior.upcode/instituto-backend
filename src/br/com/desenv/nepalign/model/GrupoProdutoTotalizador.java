package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="grupoprodutototalizador")
public class GrupoProdutoTotalizador extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idGrupoProdutoTotalizador")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="idGrupoTotalizador")
	private GrupoTotalizador grupoTotalizador;
	
	@ManyToOne
	@JoinColumn(name="idGrupoProduto")
	private GrupoProduto grupoProduto;

	public GrupoProdutoTotalizador()
	{
		super();
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		if (id != null && id == 0)
			this.id = null;
		else
			this.id = id;
	}

	public GrupoTotalizador getGrupoTotalizador() 
	{
		return grupoTotalizador;
	}

	public void setGrupoTotalizador(GrupoTotalizador grupoTotalizador) 
	{
		this.grupoTotalizador = grupoTotalizador;
	}

	public GrupoProduto getGrupoProduto() 
	{
		return grupoProduto;
	}

	public void setGrupoProduto(GrupoProduto grupoProduto) 
	{
		this.grupoProduto = grupoProduto;
	}

	@Override
	public void validate() throws Exception
	{
	}
	
	@Override
	public boolean equals(Object object) 
	{
		if(object == null)
			return false;
		if(!object.getClass().equals(this.getClass()))
			return false;
		
		return super.equals(object);
	}
}