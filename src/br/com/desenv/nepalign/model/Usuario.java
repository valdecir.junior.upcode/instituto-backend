package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import javax.persistence.Lob;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="usuario")
@XmlAccessorType(XmlAccessType.FIELD)
public class Usuario extends GenericModelIGN implements Cloneable 
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idUsuario")
	private Integer id;
		
	@Column(name="nome")
	private String nome;
	
	@Column(name="senha")
	private String senha;
	
	@Column(name="login")
	private String login;
	
	@Column(name="usuarioGlobal")
	private String usuarioGlobal;
	
	@Column(name="usuarioMaster")
	private String usuarioMaster;
	
	@Column(name="usuarioCaixa")
	private String usuarioCaixa;
	
	@ManyToOne
	@JoinColumn(name = "idEmpresaPadrao")
	private Empresa empresaPadrao;
	
	@ManyToOne
	@JoinColumn(name = "idOperadora")
	private Operadora operadora;
	
	@Column(name="telefone")
	private String telefone;
	
	@Column(name="usuarioDesenv")
	private String usuarioDesenv;
	
	@Transient
	private boolean uae;
	
	@ManyToOne
	@JoinColumn(name = "idPerfilUsuario")
	private PerfilUsuario perfilUsuario;
	
	@Column(name="ativo")
	private String ativo;
	
	public String getUsuarioDesenv() 
	{
		return usuarioDesenv;
	}

	public void setUsuarioDesenv(String usuarioDesenv) 
	{
		this.usuarioDesenv = usuarioDesenv;
	}
	
	@Transient
	private List<Acao> acoesLiberadas = new ArrayList<Acao>();
	
	public List<Acao> getAcoesLiberadas() 
	{
		return acoesLiberadas;
	}

	public void setAcoesLiberadas(List<Acao> acoesLiberadas) 
	{
		this.acoesLiberadas = acoesLiberadas;
	}


	public Empresa getEmpresaPadrao() {
		return empresaPadrao;
	}


	public void setEmpresaPadrao(Empresa empresaPadrao) {
		this.empresaPadrao = empresaPadrao;
	}
	@Column(name = "foto")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] foto;

	public Usuario()
	{
		super();
	}
	

	public Usuario
	(
		String nome,
		String senha,
		String login,
		String usuarioGlobal,
		String usuarioMaster,
		Empresa empresaPadrao,
		byte[] foto,
		PerfilUsuario perfilUsuario,
		String ativo
	) 
	{
		super();
		this.nome = nome;
		this.senha = senha;
		this.login = login;
		this.usuarioGlobal = usuarioGlobal;
		this.usuarioMaster = usuarioMaster;
		this.foto = foto;
		this.empresaPadrao = empresaPadrao;
		this.perfilUsuario = perfilUsuario;
		this.ativo = ativo;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public String getNome() {
		String retorno = null;
		
		if (nome != null)
			retorno = nome.toUpperCase().trim();
		return retorno;
	}
	
	public void setNome(String nome) {
		if (nome != null)
		{
			this.nome = nome.toUpperCase().trim();
		}
		else
			this.nome = null;
			
		
	}
		
	public String getSenha() {
	
		return this.senha;
	}
	
	public void setSenha(String senha) {
		
			this.senha = senha;
			
		
	}
		
	public String getLogin() {
		String retorno = null;
		
		if (login != null)
			retorno = login.toUpperCase().trim();
		return retorno;
	}
	
	public void setLogin(String login) {
		if (login != null)
		{
			this.login = login.toUpperCase().trim();
		}
		else
			this.login = null;
			
		
	}
		
	public String getUsuarioGlobal() {
		String retorno = null;
		
		if (usuarioGlobal != null)
			retorno = usuarioGlobal.toUpperCase().trim();
		return retorno;
	}
	
	public void setUsuarioGlobal(String usuarioGlobal) {
		if (usuarioGlobal != null)
		{
			this.usuarioGlobal = usuarioGlobal.toUpperCase().trim();
		}
		else
			this.usuarioGlobal = null;
			
		
	}
		
	public String getUsuarioMaster() {
		String retorno = null;
		
		if (usuarioMaster != null)
			retorno = usuarioMaster.toUpperCase().trim();
		return retorno;
	}
	
	public void setUsuarioMaster(String usuarioMaster) {
		if (usuarioMaster != null)
		{
			this.usuarioMaster = usuarioMaster.toUpperCase().trim();
		}
		else
			this.usuarioMaster = null;		
	}
	
		
	public String getUsuarioCaixa() {
		String retorno = null;
		
		if (usuarioCaixa != null)
			retorno = usuarioCaixa.toUpperCase().trim();
		
		return retorno;
	}

	public void setUsuarioCaixa(String usuarioCaixa) {
		if (usuarioCaixa != null)
		{
			this.usuarioCaixa = usuarioCaixa.toUpperCase().trim();
		}else
			this.usuarioCaixa = null;
	}


	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}	
	public Operadora getOperadora() {
		return operadora;
	}


	public void setOperadora(Operadora operadora) {
		this.operadora = operadora;
	}


	public String getTelefone() {
		return telefone;
	}


	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}
	@Override
	public boolean equals(Object object) 
	{
		Usuario usuario = (Usuario) object;
		if(usuario == null)
			return false;
		else if (usuario.getId().intValue()==this.getId().intValue())
			return true;
		else
			return false;
	}

	@Override
	public int compareTo(GenericModelIGN object)  
	{  
		
		if (object!=null && object.getId()!=null && this.getId()!=null)
		{
			if (object.getId() == this.getId()) return 0;
			if (object.getId() > this.getId()) return 1;
		}
		return -1;  
	}

	public boolean isUae() {
		return uae;
	}

	public void setUae(boolean uae) {
		this.uae = uae;
	}	

	public PerfilUsuario getPerfilUsuario() {
		return perfilUsuario;
	}

	public void setPerfilUsuario(PerfilUsuario perfilUsuario) {
		this.perfilUsuario = perfilUsuario;
	}

	public String getAtivo() {
		return ativo;
	}

	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}

	@Override
	public Usuario clone() throws CloneNotSupportedException 
	{
		return (Usuario) super.clone();
	}
}