package br.com.desenv.nepalign.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="saldoestoqueproduto")
public class SaldoEstoqueProdutoReferenciaLoja  extends GenericModelIGN implements Cloneable, Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@Deprecated
	@Column(name="idSaldoEstoqueProduto")
	private int id;//unused
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idEstoqueProduto")
	private EstoqueProduto estoqueProduto;
	@Transient
	private Double quantidadeUltimaEntrada;
	@Column(name="saldo")
	private Double saldo;
	@Column(name="custo")
	private Double valorCusto;
	@Column(name="venda")
	private Double valorVenda;
	
	public EmpresaFisica getEmpresaFisica() 
	{
		return empresaFisica;
	}
	
	public void setEmpresaFisica(EmpresaFisica empresaFisica) 
	{
		this.empresaFisica = empresaFisica;
	}
	
	public EstoqueProduto getEstoqueProduto() 
	{
		return estoqueProduto;
	}
	
	public void setEstoqueProduto(EstoqueProduto estoqueProduto) 
	{
		this.estoqueProduto = estoqueProduto;
	}
	
	public Double getQuantidadeUltimaEntrada() {
		return quantidadeUltimaEntrada;
	}

	public void setQuantidadeUltimaEntrada(Double quantidadeUltimaEntrada) {
		this.quantidadeUltimaEntrada = quantidadeUltimaEntrada;
	}

	public Double getSaldo() 
	{
		return saldo;
	}
	
	public void setSaldo(Double saldo) 
	{
		this.saldo = saldo;
	}
	
	public Double getValorCusto() 
	{
		return valorCusto;
	}
	
	public void setValorCusto(Double valorCusto) 
	{
		this.valorCusto = valorCusto;
	}
	
	public Double getValorVenda() 
	{
		return valorVenda;
	}
	
	public void setValorVenda(Double valorVenda) 
	{
		this.valorVenda = valorVenda;
	}

	@Deprecated
	@Override
	public Integer getId()  { return null; }

	@Deprecated
	@Override
	public void setId(Integer id) { }

	@Override
	public void validate() throws Exception { }
}