package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="saldocontacorrente")


public class SaldoContaCorrente extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idSaldoContaCorrente")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idContaCorrente")
	private ContaCorrente contaCorrente;
	@Column(name="anoMes")
	private String anoMes;
	
	@Column(name="saldo")
	private Double saldo;
	
	@Transient
	private String anoMesAtual;
	
	@Transient
	private Double saldoAtual;
	

	public SaldoContaCorrente()
	{
		super();
	}
	

	public SaldoContaCorrente
	(
	ContaCorrente contaCorrente,
		String anoMes,
		Double saldo
	) 
	{
		super();
		this.contaCorrente = contaCorrente;
		this.anoMes = anoMes;
		this.saldo = saldo;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public ContaCorrente getContaCorrente() {
		return contaCorrente;
	}

	public void setContaCorrente(ContaCorrente contaCorrente) {
		this.contaCorrente = contaCorrente;
	}
	
	public String getAnoMes() {
		String retorno = null;
		
		if (anoMes != null)
			retorno = anoMes.toUpperCase().trim();
		return retorno;
	}
	
	public void setAnoMes(String anoMes) {
		if (anoMes != null)
		{
			this.anoMes = anoMes.toUpperCase().trim();
		}
		else
			this.anoMes = null;
			
		
	}
		
	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
	
		if (saldo != null && Double.isNaN(saldo))
		{
			this.saldo = null;
		}
		else
		{
			this.saldo = saldo;
		}
		
	}	
	public String getAnoMesAtual() {
		return anoMesAtual;
	}


	public void setAnoMesAtual(String anoMesAtual) {
		this.anoMesAtual = anoMesAtual;
	}


	public Double getSaldoAtual() {
		return saldoAtual;
	}


	public void setSaldoAtual(Double saldoAtual) {
		this.saldoAtual = saldoAtual;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
