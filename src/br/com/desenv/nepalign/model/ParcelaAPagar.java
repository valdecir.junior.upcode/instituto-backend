package br.com.desenv.nepalign.model;

import java.util.Date;

public class ParcelaAPagar {
private String cliente;
private Integer diasAtrazo;
private Date dataBaixa;
private Date dataVencimento;
private String situacao;
private Integer numeroParcela;
private Double valorParcela;
private Double valorPago;
private Double juros;
private Double desconto;
private Double valorTotal;


public Double getJuros() {
	return juros;
}
public void setJuros(Double juros) {
	this.juros = juros;
}
public Double getDesconto() {
	return desconto;
}
public void setDesconto(Double desconto) {
	this.desconto = desconto;
}
public Double getValorTotal() {
	return valorTotal;
}
public void setValorTotal(Double valorTotal) {
	this.valorTotal = valorTotal;
}
public String getCliente() {
	return cliente;
}
public void setCliente(String cliente) {
	this.cliente = cliente;
}

public Integer getDiasAtrazo() {
	return diasAtrazo;
}
public void setDiasAtrazo(Integer diasAtrazo) {
	this.diasAtrazo = diasAtrazo;
}
public Date getDataBaixa() {
	return dataBaixa;
}
public void setDataBaixa(Date dataBaixa) {
	this.dataBaixa = dataBaixa;
}
public Date getDataVencimento() {
	return dataVencimento;
}
public void setDataVencimento(Date dataVencimento) {
	this.dataVencimento = dataVencimento;
}
public String getSituacao() {
	return situacao;
}
public void setSituacao(String situacao) {
	this.situacao = situacao;
}
public Integer getNumeroParcela() {
	return numeroParcela;
}
public void setNumeroParcela(Integer numeroParcela) {
	this.numeroParcela = numeroParcela;
}
public Double getValorParcela() {
	return valorParcela;
}
public void setValorParcela(Double valorParcela) {
	this.valorParcela = valorParcela;
}
public Double getValorPago() {
	return valorPago;
}
public void setValorPago(Double valorPago) {
	this.valorPago = valorPago;
}

}
