package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="parametrosfaturamento")


public class ParamentrosFaturamento extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idParametrosFaturamento")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idTransportadora")
	private Transportadora transportadora;
	@Column(name="aliquotaICMSPadrao")
	private Double aliquotaICMSPadrao;
	
	@Column(name="aliquotaIPIPadrao")
	private Double aliquotaIPIPadrao;
	
	@Column(name="observacaoPadraoNotaSaida")
	private String observacaoPadraoNotaSaida;
	
	@Column(name="observacaoPadraoNotaEntrada")
	private String observacaoPadraoNotaEntrada;
	
	@Column(name="descricaoEspeciePadrao")
	private String descricaoEspeciePadrao;
	
	@Column(name="observacaoIsentoICMS")
	private String observacaoIsentoICMS;
	
	@Column(name="observacaoDiferidoICMS")
	private String observacaoDiferidoICMS;
	
	@Column(name="observacaoReducaoBaseICMS")
	private String observacaoReducaoBaseICMS;
	
	@Column(name="substitutoTributario")
	private Integer substitutoTributario;
	
	@Column(name="observacaoSubTrib")
	private String observacaoSubTrib;
	
	@Column(name="inscricaoSubstitutoTributario")
	private String inscricaoSubstitutoTributario;
	
	@Column(name="percReducaoICMSEmpresa")
	private Double percReducaoICMSEmpresa;
	
	@Column(name="percReducaoIPIEmpresa")
	private Double percReducaoIPIEmpresa;
	
	@Column(name="percReducaoISSEmpresa")
	private Double percReducaoISSEmpresa;
	
	@Column(name="numeroUltimaNotaImpressa")
	private Integer numeroUltimaNotaImpressa;
	
	@Column(name="observacaoProdutoSemICMS")
	private String observacaoProdutoSemICMS;
	

	public ParamentrosFaturamento()
	{
		super();
	}
	

	public ParamentrosFaturamento
	(
	EmpresaFisica empresaFisica,
	Transportadora transportadora,
		Double aliquotaICMSPadrao,
		Double aliquotaIPIPadrao,
		String observacaoPadraoNotaSaida,
		String observacaoPadraoNotaEntrada,
		String descricaoEspeciePadrao,
		String observacaoIsentoICMS,
		String observacaoDiferidoICMS,
		String observacaoReducaoBaseICMS,
		Integer substitutoTributario,
		String observacaoSubTrib,
		String inscricaoSubstitutoTributario,
		Double percReducaoICMSEmpresa,
		Double percReducaoIPIEmpresa,
		Double percReducaoISSEmpresa,
		Integer numeroUltimaNotaImpressa,
		String observacaoProdutoSemICMS
	) 
	{
		super();
		this.empresaFisica = empresaFisica;
		this.transportadora = transportadora;
		this.aliquotaICMSPadrao = aliquotaICMSPadrao;
		this.aliquotaIPIPadrao = aliquotaIPIPadrao;
		this.observacaoPadraoNotaSaida = observacaoPadraoNotaSaida;
		this.observacaoPadraoNotaEntrada = observacaoPadraoNotaEntrada;
		this.descricaoEspeciePadrao = descricaoEspeciePadrao;
		this.observacaoIsentoICMS = observacaoIsentoICMS;
		this.observacaoDiferidoICMS = observacaoDiferidoICMS;
		this.observacaoReducaoBaseICMS = observacaoReducaoBaseICMS;
		this.substitutoTributario = substitutoTributario;
		this.observacaoSubTrib = observacaoSubTrib;
		this.inscricaoSubstitutoTributario = inscricaoSubstitutoTributario;
		this.percReducaoICMSEmpresa = percReducaoICMSEmpresa;
		this.percReducaoIPIEmpresa = percReducaoIPIEmpresa;
		this.percReducaoISSEmpresa = percReducaoISSEmpresa;
		this.numeroUltimaNotaImpressa = numeroUltimaNotaImpressa;
		this.observacaoProdutoSemICMS = observacaoProdutoSemICMS;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public Transportadora getTransportadora() {
		return transportadora;
	}

	public void setTransportadora(Transportadora transportadora) {
		this.transportadora = transportadora;
	}
	
	public Double getAliquotaICMSPadrao() {
		return aliquotaICMSPadrao;
	}

	public void setAliquotaICMSPadrao(Double aliquotaICMSPadrao) {
	
		if (aliquotaICMSPadrao != null && Double.isNaN(aliquotaICMSPadrao))
		{
			this.aliquotaICMSPadrao = null;
		}
		else
		{
			this.aliquotaICMSPadrao = aliquotaICMSPadrao;
		}
		
	}	
	public Double getAliquotaIPIPadrao() {
		return aliquotaIPIPadrao;
	}

	public void setAliquotaIPIPadrao(Double aliquotaIPIPadrao) {
	
		if (aliquotaIPIPadrao != null && Double.isNaN(aliquotaIPIPadrao))
		{
			this.aliquotaIPIPadrao = null;
		}
		else
		{
			this.aliquotaIPIPadrao = aliquotaIPIPadrao;
		}
		
	}	
	public String getObservacaoPadraoNotaSaida() {
		String retorno = null;
		
		if (observacaoPadraoNotaSaida != null)
			retorno = observacaoPadraoNotaSaida.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacaoPadraoNotaSaida(String observacaoPadraoNotaSaida) {
		if (observacaoPadraoNotaSaida != null)
		{
			this.observacaoPadraoNotaSaida = observacaoPadraoNotaSaida.toUpperCase().trim();
		}
		else
			this.observacaoPadraoNotaSaida = null;
			
		
	}
		
	public String getObservacaoPadraoNotaEntrada() {
		String retorno = null;
		
		if (observacaoPadraoNotaEntrada != null)
			retorno = observacaoPadraoNotaEntrada.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacaoPadraoNotaEntrada(String observacaoPadraoNotaEntrada) {
		if (observacaoPadraoNotaEntrada != null)
		{
			this.observacaoPadraoNotaEntrada = observacaoPadraoNotaEntrada.toUpperCase().trim();
		}
		else
			this.observacaoPadraoNotaEntrada = null;
			
		
	}
		
	public String getDescricaoEspeciePadrao() {
		String retorno = null;
		
		if (descricaoEspeciePadrao != null)
			retorno = descricaoEspeciePadrao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricaoEspeciePadrao(String descricaoEspeciePadrao) {
		if (descricaoEspeciePadrao != null)
		{
			this.descricaoEspeciePadrao = descricaoEspeciePadrao.toUpperCase().trim();
		}
		else
			this.descricaoEspeciePadrao = null;
			
		
	}
		
	public String getObservacaoIsentoICMS() {
		String retorno = null;
		
		if (observacaoIsentoICMS != null)
			retorno = observacaoIsentoICMS.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacaoIsentoICMS(String observacaoIsentoICMS) {
		if (observacaoIsentoICMS != null)
		{
			this.observacaoIsentoICMS = observacaoIsentoICMS.toUpperCase().trim();
		}
		else
			this.observacaoIsentoICMS = null;
			
		
	}
		
	public String getObservacaoDiferidoICMS() {
		String retorno = null;
		
		if (observacaoDiferidoICMS != null)
			retorno = observacaoDiferidoICMS.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacaoDiferidoICMS(String observacaoDiferidoICMS) {
		if (observacaoDiferidoICMS != null)
		{
			this.observacaoDiferidoICMS = observacaoDiferidoICMS.toUpperCase().trim();
		}
		else
			this.observacaoDiferidoICMS = null;
			
		
	}
		
	public String getObservacaoReducaoBaseICMS() {
		String retorno = null;
		
		if (observacaoReducaoBaseICMS != null)
			retorno = observacaoReducaoBaseICMS.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacaoReducaoBaseICMS(String observacaoReducaoBaseICMS) {
		if (observacaoReducaoBaseICMS != null)
		{
			this.observacaoReducaoBaseICMS = observacaoReducaoBaseICMS.toUpperCase().trim();
		}
		else
			this.observacaoReducaoBaseICMS = null;
			
		
	}
		
	public Integer getSubstitutoTributario() {
		return substitutoTributario;
	}

	public void setSubstitutoTributario(Integer substitutoTributario) {
		this.substitutoTributario = substitutoTributario;
	}	
	public String getObservacaoSubTrib() {
		String retorno = null;
		
		if (observacaoSubTrib != null)
			retorno = observacaoSubTrib.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacaoSubTrib(String observacaoSubTrib) {
		if (observacaoSubTrib != null)
		{
			this.observacaoSubTrib = observacaoSubTrib.toUpperCase().trim();
		}
		else
			this.observacaoSubTrib = null;
			
		
	}
		
	public String getInscricaoSubstitutoTributario() {
		String retorno = null;
		
		if (inscricaoSubstitutoTributario != null)
			retorno = inscricaoSubstitutoTributario.toUpperCase().trim();
		return retorno;
	}
	
	public void setInscricaoSubstitutoTributario(String inscricaoSubstitutoTributario) {
		if (inscricaoSubstitutoTributario != null)
		{
			this.inscricaoSubstitutoTributario = inscricaoSubstitutoTributario.toUpperCase().trim();
		}
		else
			this.inscricaoSubstitutoTributario = null;
			
		
	}
		
	public Double getPercReducaoICMSEmpresa() {
		return percReducaoICMSEmpresa;
	}

	public void setPercReducaoICMSEmpresa(Double percReducaoICMSEmpresa) {
	
		if (percReducaoICMSEmpresa != null && Double.isNaN(percReducaoICMSEmpresa))
		{
			this.percReducaoICMSEmpresa = null;
		}
		else
		{
			this.percReducaoICMSEmpresa = percReducaoICMSEmpresa;
		}
		
	}	
	public Double getPercReducaoIPIEmpresa() {
		return percReducaoIPIEmpresa;
	}

	public void setPercReducaoIPIEmpresa(Double percReducaoIPIEmpresa) {
	
		if (percReducaoIPIEmpresa != null && Double.isNaN(percReducaoIPIEmpresa))
		{
			this.percReducaoIPIEmpresa = null;
		}
		else
		{
			this.percReducaoIPIEmpresa = percReducaoIPIEmpresa;
		}
		
	}	
	public Double getPercReducaoISSEmpresa() {
		return percReducaoISSEmpresa;
	}

	public void setPercReducaoISSEmpresa(Double percReducaoISSEmpresa) {
	
		if (percReducaoISSEmpresa != null && Double.isNaN(percReducaoISSEmpresa))
		{
			this.percReducaoISSEmpresa = null;
		}
		else
		{
			this.percReducaoISSEmpresa = percReducaoISSEmpresa;
		}
		
	}	
	public Integer getNumeroUltimaNotaImpressa() {
		return numeroUltimaNotaImpressa;
	}

	public void setNumeroUltimaNotaImpressa(Integer numeroUltimaNotaImpressa) {
		this.numeroUltimaNotaImpressa = numeroUltimaNotaImpressa;
	}	
	public String getObservacaoProdutoSemICMS() {
		String retorno = null;
		
		if (observacaoProdutoSemICMS != null)
			retorno = observacaoProdutoSemICMS.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacaoProdutoSemICMS(String observacaoProdutoSemICMS) {
		if (observacaoProdutoSemICMS != null)
		{
			this.observacaoProdutoSemICMS = observacaoProdutoSemICMS.toUpperCase().trim();
		}
		else
			this.observacaoProdutoSemICMS = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
