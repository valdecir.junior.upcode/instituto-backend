package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="tipomovimentacaoestoque")
public class TipoMovimentacaoEstoque extends GenericModelIGN
{
	public static final int COMPRA = 0x01;
	public static final int TRANSFERENCIA_FILIAIS_ENTRADA = 0x03;
	public static final int TRANSFERENCIA_FILIAIS_SAIDA = 0x04;
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idTipoMovimentacaoEstoque")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="emprestimo")
	private String emprestimo;
	
	@Column(name="exigePedido")
	private String exigePedido;
	
	@Column(name="exigeCliente")
	private String exigeCliente;
	
	@ManyToOne
	@JoinColumn(name = "idEstoque")
	private Estoque estoque;
	
	@ManyToOne
	@JoinColumn(name = "idTipoNotaFiscal")
	private TipoNotaFiscal tipoNotaFiscal;
	
	@Column(name="observacao")
	private String observacao;
	
	@Column(name="exigePagamento")
	private String exigePagamento;
	
	@Column(name="exibirNumPedidoVenda")
	private String exibirNumPedidoVenda;
	
	@Column(name="entradaSaida")
	private String entradaSaida;
	

	public TipoMovimentacaoEstoque()
	{
		super();
	}
	
	public TipoMovimentacaoEstoque
	(
		String descricao,
		String emprestimo,
		String exigePedido,
		String exigeCliente,
		Estoque estoque,
		TipoNotaFiscal tipoNotaFiscal,
		String observacao,
		String exigePagamento,
		String exibirNumPedidoVenda,
		String entradaSaida
	) 
	{
		super();
		this.descricao = descricao;
		this.emprestimo = emprestimo;
		this.exigePedido = exigePedido;
		this.exigeCliente = exigeCliente;
		this.estoque = estoque;
		this.tipoNotaFiscal = tipoNotaFiscal;
		this.observacao = observacao;
		this.exigePagamento = exigePagamento;
		this.exibirNumPedidoVenda = exibirNumPedidoVenda;
		this.entradaSaida = entradaSaida;
		
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		if (id != null && id == 0)
			this.id = null;
		else
			this.id = id;
	}	
	public String getDescricao() 
	{
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setDescricao(String descricao) 
	{
		if (descricao != null)
			this.descricao = descricao.toUpperCase().trim();
		else
			this.descricao = null;
	}
		
	public String getEmprestimo() 
	{
		String retorno = null;
		
		if (emprestimo != null)
			retorno = emprestimo.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setEmprestimo(String emprestimo) 
	{
		if (emprestimo != null)
			this.emprestimo = emprestimo.toUpperCase().trim();
		else
			this.emprestimo = null;
	}
		
	public String getExigePedido() 
	{
		String retorno = null;
		
		if (exigePedido != null)
			retorno = exigePedido.toUpperCase().trim();
		
		return retorno;
	}

	public void setExigePedido(String exigePedido) 
	{
		if (exigePedido != null)
			this.exigePedido = exigePedido.toUpperCase().trim();
		else
			this.exigePedido = null;	
	}
		
	public String getExigeCliente() 
	{
		String retorno = null;
		
		if (exigeCliente != null)
			retorno = exigeCliente.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setExigeCliente(String exigeCliente) 
	{
		if (exigeCliente != null)
			this.exigeCliente = exigeCliente.toUpperCase().trim();
		else
			this.exigeCliente = null;	
	}
		
	public Estoque getEstoque() 
	{
		return estoque;
	}

	public void setEstoque(Estoque estoque) 
	{
		this.estoque = estoque;
	}
	
	public TipoNotaFiscal getTipoNotaFiscal() 
	{
		return tipoNotaFiscal;
	}

	public void setTipoNotaFiscal(TipoNotaFiscal tipoNotaFiscal) 
	{
		this.tipoNotaFiscal = tipoNotaFiscal;
	}
	
	public String getObservacao() 
	{
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setObservacao(String observacao) 
	{
		if (observacao != null)
			this.observacao = observacao.toUpperCase().trim();
		else
			this.observacao = null;
	}
		
	public String getExigePagamento() 
	{
		String retorno = null;
		
		if (exigePagamento != null)
			retorno = exigePagamento.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setExigePagamento(String exigePagamento) 
	{
		if (exigePagamento != null)
			this.exigePagamento = exigePagamento.toUpperCase().trim();
		else
			this.exigePagamento = null;
	}
		
	public String getExibirNumPedidoVenda() 
	{
		String retorno = null;
		
		if (exibirNumPedidoVenda != null)
			retorno = exibirNumPedidoVenda.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setExibirNumPedidoVenda(String exibirNumPedidoVenda) 
	{
		if (exibirNumPedidoVenda != null)
			this.exibirNumPedidoVenda = exibirNumPedidoVenda.toUpperCase().trim();
		else
			this.exibirNumPedidoVenda = null;	
	}
		
	public String getEntradaSaida() 
	{	
		String retorno = null;
		
		if (entradaSaida != null)
			retorno = entradaSaida.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setEntradaSaida(String entradaSaida) 
	{
		if (entradaSaida != null)
			this.entradaSaida = entradaSaida.toUpperCase().trim();
		else
			this.entradaSaida = null;
	}
		
	@Override
	public void validate() throws Exception { }
}