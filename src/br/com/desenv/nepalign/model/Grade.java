package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name = "grade")
public class Grade extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idGrade")
	private Integer id;

	@Column(name = "descricao")
	private String descricao;

	@ManyToOne
	@JoinColumn(name = "idTamanho1")
	private Tamanho tamanho1;
	@ManyToOne
	@JoinColumn(name = "idTamanho2")
	private Tamanho tamanho2;
	@ManyToOne
	@JoinColumn(name = "idTamanho3")
	private Tamanho tamanho3;
	@ManyToOne
	@JoinColumn(name = "idTamanho4")
	private Tamanho tamanho4;
	@ManyToOne
	@JoinColumn(name = "idTamanho5")
	private Tamanho tamanho5;
	@ManyToOne
	@JoinColumn(name = "idTamanho6")
	private Tamanho tamanho6;
	@ManyToOne
	@JoinColumn(name = "idTamanho7")
	private Tamanho tamanho7;
	@ManyToOne
	@JoinColumn(name = "idTamanho8")
	private Tamanho tamanho8;
	@ManyToOne
	@JoinColumn(name = "idTamanho9")
	private Tamanho tamanho9;
	@ManyToOne
	@JoinColumn(name = "idTamanho10")
	private Tamanho tamanho10;
	@ManyToOne
	@JoinColumn(name = "idTamanho11")
	private Tamanho tamanho11;
	@ManyToOne
	@JoinColumn(name = "idTamanho12")
	private Tamanho tamanho12;
	@ManyToOne
	@JoinColumn(name = "idTamanho13")
	private Tamanho tamanho13;
	@ManyToOne
	@JoinColumn(name = "idTamanho14")
	private Tamanho tamanho14;
	@ManyToOne
	@JoinColumn(name = "idTamanho15")
	private Tamanho tamanho15;
	@ManyToOne
	@JoinColumn(name = "idTamanho16")
	private Tamanho tamanho16;
	@ManyToOne
	@JoinColumn(name = "idTamanho17")
	private Tamanho tamanho17;

	public Grade()
	{
		super();
	}

	public Grade(String descricao, Tamanho tamanho1, Tamanho tamanho2, Tamanho tamanho3, Tamanho tamanho4, Tamanho tamanho5, Tamanho tamanho6, Tamanho tamanho7, Tamanho tamanho8, Tamanho tamanho9, Tamanho tamanho10, Tamanho tamanho11, Tamanho tamanho12, Tamanho tamanho13, Tamanho tamanho14,
			Tamanho tamanho15, Tamanho tamanho16, Tamanho tamanho17)
	{
		super();
		this.descricao = descricao;
		this.tamanho1 = tamanho1;
		this.tamanho2 = tamanho2;
		this.tamanho3 = tamanho3;
		this.tamanho4 = tamanho4;
		this.tamanho5 = tamanho5;
		this.tamanho6 = tamanho6;
		this.tamanho7 = tamanho7;
		this.tamanho8 = tamanho8;
		this.tamanho9 = tamanho9;
		this.tamanho10 = tamanho10;
		this.tamanho11 = tamanho11;
		this.tamanho12 = tamanho12;
		this.tamanho13 = tamanho13;
		this.tamanho14 = tamanho14;
		this.tamanho15 = tamanho15;
		this.tamanho16 = tamanho16;
		this.tamanho17 = tamanho17;

	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getDescricao()
	{
		String retorno = null;

		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}

	public void setDescricao(String descricao)
	{
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;

	}

	public Tamanho getTamanho1()
	{
		return tamanho1;
	}

	public void setTamanho1(Tamanho tamanho1)
	{
		this.tamanho1 = tamanho1;
	}

	public Tamanho getTamanho2()
	{
		return tamanho2;
	}

	public void setTamanho2(Tamanho tamanho2)
	{
		this.tamanho2 = tamanho2;
	}

	public Tamanho getTamanho3()
	{
		return tamanho3;
	}

	public void setTamanho3(Tamanho tamanho3)
	{
		this.tamanho3 = tamanho3;
	}

	public Tamanho getTamanho4()
	{
		return tamanho4;
	}

	public void setTamanho4(Tamanho tamanho4)
	{
		this.tamanho4 = tamanho4;
	}

	public Tamanho getTamanho5()
	{
		return tamanho5;
	}

	public void setTamanho5(Tamanho tamanho5)
	{
		this.tamanho5 = tamanho5;
	}

	public Tamanho getTamanho6()
	{
		return tamanho6;
	}

	public void setTamanho6(Tamanho tamanho6)
	{
		this.tamanho6 = tamanho6;
	}

	public Tamanho getTamanho7()
	{
		return tamanho7;
	}

	public void setTamanho7(Tamanho tamanho7)
	{
		this.tamanho7 = tamanho7;
	}

	public Tamanho getTamanho8()
	{
		return tamanho8;
	}

	public void setTamanho8(Tamanho tamanho8)
	{
		this.tamanho8 = tamanho8;
	}

	public Tamanho getTamanho9()
	{
		return tamanho9;
	}

	public void setTamanho9(Tamanho tamanho9)
	{
		this.tamanho9 = tamanho9;
	}

	public Tamanho getTamanho10()
	{
		return tamanho10;
	}

	public void setTamanho10(Tamanho tamanho10)
	{
		this.tamanho10 = tamanho10;
	}

	public Tamanho getTamanho11()
	{
		return tamanho11;
	}

	public void setTamanho11(Tamanho tamanho11)
	{
		this.tamanho11 = tamanho11;
	}

	public Tamanho getTamanho12()
	{
		return tamanho12;
	}

	public void setTamanho12(Tamanho tamanho12)
	{
		this.tamanho12 = tamanho12;
	}

	public Tamanho getTamanho13()
	{
		return tamanho13;
	}

	public void setTamanho13(Tamanho tamanho13)
	{
		this.tamanho13 = tamanho13;
	}

	public Tamanho getTamanho14()
	{
		return tamanho14;
	}

	public void setTamanho14(Tamanho tamanho14)
	{
		this.tamanho14 = tamanho14;
	}

	public Tamanho getTamanho15()
	{
		return tamanho15;
	}

	public void setTamanho15(Tamanho tamanho15)
	{
		this.tamanho15 = tamanho15;
	}

	public Tamanho getTamanho16()
	{
		return tamanho16;
	}

	public void setTamanho16(Tamanho tamanho16)
	{
		this.tamanho16 = tamanho16;
	}

	public Tamanho getTamanho17()
	{
		return tamanho17;
	}

	public void setTamanho17(Tamanho tamanho17)
	{
		this.tamanho17 = tamanho17;
	}

	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		 * if (this.descricao == null || this.descricao.trim().equals("")) {
		 * throw new Exception("Descrição é obrigatório."); }
		 */

	}

}
