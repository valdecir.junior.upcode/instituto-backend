package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="senhagerentes")


public class SenhaGerentes extends GenericModelIGN 
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idSenhaGerentes")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@Column(name="senha")
	private String senha;

	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name="idModuloSenhaGerentes")
	private ModuloSenhaGerentes modulo;

	public SenhaGerentes()
	{
		super();
	}


	public SenhaGerentes
	(
			Usuario usuario,
			String senha,
			EmpresaFisica empresaFisica,
			ModuloSenhaGerentes modulo
			) 
	{
		super();
		this.usuario = usuario;
		this.senha = senha;
		this.empresaFisica = empresaFisica;
		this.modulo = modulo;

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}




	public ModuloSenhaGerentes getModulo() {
		return modulo;
	}


	public void setModulo(ModuloSenhaGerentes modulo) {
		this.modulo = modulo;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		 */

	}

}
