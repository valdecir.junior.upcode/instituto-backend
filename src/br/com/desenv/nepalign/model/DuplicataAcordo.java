package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="duplicataacordo")


public class DuplicataAcordo extends GenericModelIGN implements Cloneable
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idDuplicataAcordo")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idDuplicataNova")
	private Duplicata duplicataNova;
	@ManyToOne
	@JoinColumn(name = "idDuplicataAntigaParcela")
	private DuplicataParcela duplicataAntigaParcela;
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@Column(name="numeroNovaDuplicata")
	private Long numeroNovaDuplicata;
	
	@Column(name="numeroDuplicataAntiga")
	private Long numeroDuplicataAntiga;
	
	@Column(name="numeroParcelaDuplicataAntiga")
	private Integer numeroParcelaDuplicataAntiga;
	

	public DuplicataAcordo()
	{
		super();
	}
	

	public DuplicataAcordo
	(
	Duplicata duplicataNova,
	DuplicataParcela duplicataAntigaParcela,
	EmpresaFisica empresaFisica,
	Cliente cliente,
		Long numeroNovaDuplicata,
		Long numeroDuplicataAntiga,
		Integer numeroParcelaDuplicataAntiga
	) 
	{
		super();
		this.duplicataNova = duplicataNova;
		this.duplicataAntigaParcela = duplicataAntigaParcela;
		this.empresaFisica = empresaFisica;
		this.cliente = cliente;
		this.numeroNovaDuplicata = numeroNovaDuplicata;
		this.numeroDuplicataAntiga = numeroDuplicataAntiga;
		this.numeroParcelaDuplicataAntiga = numeroParcelaDuplicataAntiga;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Duplicata getDuplicataNova() {
		return duplicataNova;
	}

	public void setDuplicataNova(Duplicata duplicataNova) {
		this.duplicataNova = duplicataNova;
	}
	
	public DuplicataParcela getDuplicataAntigaParcela() {
		return duplicataAntigaParcela;
	}

	public void setDuplicataAntigaParcela(DuplicataParcela duplicataAntigaParcela) {
		this.duplicataAntigaParcela = duplicataAntigaParcela;
	}
	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Long getNumeroNovaDuplicata() {
		return numeroNovaDuplicata;
	}

	public void setNumeroNovaDuplicata(Long numeroNovaDuplicata) {
		this.numeroNovaDuplicata = numeroNovaDuplicata;
	}	
	public Long getNumeroDuplicataAntiga() {
		return numeroDuplicataAntiga;
	}

	public void setNumeroDuplicataAntiga(Long numeroDuplicataAntiga) {
		this.numeroDuplicataAntiga = numeroDuplicataAntiga;
	}	
	public Integer getNumeroParcelaDuplicataAntiga() {
		return numeroParcelaDuplicataAntiga;
	}

	public void setNumeroParcelaDuplicataAntiga(Integer numeroParcelaDuplicataAntiga) {
		this.numeroParcelaDuplicataAntiga = numeroParcelaDuplicataAntiga;
	}	
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/	
	}
	
	@Override
	public DuplicataAcordo clone() throws CloneNotSupportedException 
	{
		return (DuplicataAcordo) super.clone();
	}
}