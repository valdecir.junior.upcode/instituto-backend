package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
public class OperadoraSincronizacao  
{

    private static final long serialVersionUID = 1L;
    

    @Id
    @Column(name = "idOperadora")
    private Integer id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "codigo")
    private Integer codigo;

    public OperadoraSincronizacao()
    {
	super();
    }

    public OperadoraSincronizacao(String nome, Integer codigo)
    {
	super();
	this.nome = nome;
	this.codigo = codigo;

    }

    public Integer getId()
    {
	return id;
    }

    public void setId(Integer id)
    {
	if (id != null && id == 0)
	{
	    this.id = null;
	} else
	{
	    this.id = id;
	}
    }

    public String getNome()
    {
	String retorno = null;

	if (nome != null)
	    retorno = nome.toUpperCase().trim();
	return retorno;
    }

    public void setNome(String nome)
    {
	if (nome != null)
	{
	    this.nome = nome.toUpperCase().trim();
	} else
	    this.nome = null;

    }

    public Integer getCodigo()
    {
	return codigo;
    }

    public void setCodigo(Integer codigo)
    {
	if (codigo != null && codigo == 0)
	{
	    this.codigo = null;
	} else
	{
	    this.codigo = codigo;
	}
    }

   /* @Override
    public void validate() throws Exception
    {
	// Coloque aqui suas validações ignorantes
	
	 if (this.descricao == null || this.descricao.trim().equals("")) {
	 throw new Exception("Descrição é obrigatório."); }
	

    }*/
}
