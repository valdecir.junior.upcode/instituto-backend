package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="itemnotaauxnfe")


public class ItemNotaAuxNfe extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idItemNotaAuxNFE")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idItemNotaFiscal")
	private ItemNotaFiscal itemNotaFiscal;
	@Column(name="flgItemImportado")
	private Integer flgItemImportado;
	
	@Column(name="flgItemCombustivel")
	private Integer flgItemCombustivel;
	
	@Column(name="flgGrupoCombIncCIDE")
	private Integer flgGrupoCombIncCIDE;
	
	@Column(name="flgItemMedicamento")
	private Integer flgItemMedicamento;
	
	@Column(name="flgItemVeiculo")
	private Integer flgItemVeiculo;
	
	@Column(name="flgICMSIncide")
	private Integer flgICMSIncide;
	
	@Column(name="flgIPIIncide")
	private Integer flgIPIIncide;
	
	@Column(name="flgIIincide")
	private Integer flgIIincide;
	
	@Column(name="flgPISIncide")
	private Integer flgPISIncide;
	
	@Column(name="flgPISSTIncide")
	private Integer flgPISSTIncide;
	
	@Column(name="flgCOFINSIncide")
	private Integer flgCOFINSIncide;
	
	@Column(name="flgCOFINSSTIncide")
	private Integer flgCOFINSSTIncide;
	
	@Column(name="flgISSQNIncide")
	private Integer flgISSQNIncide;
	
	@Column(name="observacao")
	private String observacao;
	

	public ItemNotaAuxNfe()
	{
		super();
	}
	

	public ItemNotaAuxNfe
	(
	ItemNotaFiscal itemNotaFiscal,
		Integer flgItemImportado,
		Integer flgItemCombustivel,
		Integer flgGrupoCombIncCIDE,
		Integer flgItemMedicamento,
		Integer flgItemVeiculo,
		Integer flgICMSIncide,
		Integer flgIPIIncide,
		Integer flgIIincide,
		Integer flgPISIncide,
		Integer flgPISSTIncide,
		Integer flgCOFINSIncide,
		Integer flgCOFINSSTIncide,
		Integer flgISSQNIncide,
		String observacao
	) 
	{
		super();
		this.itemNotaFiscal = itemNotaFiscal;
		this.flgItemImportado = flgItemImportado;
		this.flgItemCombustivel = flgItemCombustivel;
		this.flgGrupoCombIncCIDE = flgGrupoCombIncCIDE;
		this.flgItemMedicamento = flgItemMedicamento;
		this.flgItemVeiculo = flgItemVeiculo;
		this.flgICMSIncide = flgICMSIncide;
		this.flgIPIIncide = flgIPIIncide;
		this.flgIIincide = flgIIincide;
		this.flgPISIncide = flgPISIncide;
		this.flgPISSTIncide = flgPISSTIncide;
		this.flgCOFINSIncide = flgCOFINSIncide;
		this.flgCOFINSSTIncide = flgCOFINSSTIncide;
		this.flgISSQNIncide = flgISSQNIncide;
		this.observacao = observacao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public ItemNotaFiscal getItemNotaFiscal() {
		return itemNotaFiscal;
	}

	public void setItemNotaFiscal(ItemNotaFiscal itemNotaFiscal) {
		this.itemNotaFiscal = itemNotaFiscal;
	}
	
	public Integer getFlgItemImportado() {
		return flgItemImportado;
	}

	public void setFlgItemImportado(Integer flgItemImportado) {
		this.flgItemImportado = flgItemImportado;
	}	
	public Integer getFlgItemCombustivel() {
		return flgItemCombustivel;
	}

	public void setFlgItemCombustivel(Integer flgItemCombustivel) {
		this.flgItemCombustivel = flgItemCombustivel;
	}	
	public Integer getFlgGrupoCombIncCIDE() {
		return flgGrupoCombIncCIDE;
	}

	public void setFlgGrupoCombIncCIDE(Integer flgGrupoCombIncCIDE) {
		this.flgGrupoCombIncCIDE = flgGrupoCombIncCIDE;
	}	
	public Integer getFlgItemMedicamento() {
		return flgItemMedicamento;
	}

	public void setFlgItemMedicamento(Integer flgItemMedicamento) {
		this.flgItemMedicamento = flgItemMedicamento;
	}	
	public Integer getFlgItemVeiculo() {
		return flgItemVeiculo;
	}

	public void setFlgItemVeiculo(Integer flgItemVeiculo) {
		this.flgItemVeiculo = flgItemVeiculo;
	}	
	public Integer getFlgICMSIncide() {
		return flgICMSIncide;
	}

	public void setFlgICMSIncide(Integer flgICMSIncide) {
		this.flgICMSIncide = flgICMSIncide;
	}	
	public Integer getFlgIPIIncide() {
		return flgIPIIncide;
	}

	public void setFlgIPIIncide(Integer flgIPIIncide) {
		this.flgIPIIncide = flgIPIIncide;
	}	
	public Integer getFlgIIincide() {
		return flgIIincide;
	}

	public void setFlgIIincide(Integer flgIIincide) {
		this.flgIIincide = flgIIincide;
	}	
	public Integer getFlgPISIncide() {
		return flgPISIncide;
	}

	public void setFlgPISIncide(Integer flgPISIncide) {
		this.flgPISIncide = flgPISIncide;
	}	
	public Integer getFlgPISSTIncide() {
		return flgPISSTIncide;
	}

	public void setFlgPISSTIncide(Integer flgPISSTIncide) {
		this.flgPISSTIncide = flgPISSTIncide;
	}	
	public Integer getFlgCOFINSIncide() {
		return flgCOFINSIncide;
	}

	public void setFlgCOFINSIncide(Integer flgCOFINSIncide) {
		this.flgCOFINSIncide = flgCOFINSIncide;
	}	
	public Integer getFlgCOFINSSTIncide() {
		return flgCOFINSSTIncide;
	}

	public void setFlgCOFINSSTIncide(Integer flgCOFINSSTIncide) {
		this.flgCOFINSSTIncide = flgCOFINSSTIncide;
	}	
	public Integer getFlgISSQNIncide() {
		return flgISSQNIncide;
	}

	public void setFlgISSQNIncide(Integer flgISSQNIncide) {
		this.flgISSQNIncide = flgISSQNIncide;
	}	
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
