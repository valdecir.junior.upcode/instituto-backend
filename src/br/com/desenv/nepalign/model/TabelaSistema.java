package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="tabelasistema")
public class TabelaSistema extends GenericModelIGN
{
	public static final int TABELA_SISTEMA_CLIENTE = 0x03;
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idTabelaSistema")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="nomeTabela")
	private String nomeTabela;
	
	@Column(name="campoPrincipal")
	private String campoPrincipal;
	
	@Column(name="descricaoCampo")
	private String descricaoCampo;
	

	public TabelaSistema()
	{
		super();
	}
	

	public TabelaSistema
	(
		String descricao,
		String nomeTabela,
		String campoPrincipal,
		String descricaoCampo
	) 
	{
		super();
		this.descricao = descricao;
		this.nomeTabela = nomeTabela;
		this.campoPrincipal = campoPrincipal;
		this.descricaoCampo = descricaoCampo;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public String getNomeTabela() {
		String retorno = null;
		
		if (nomeTabela != null)
			retorno = nomeTabela.toUpperCase().trim();
		return retorno;
	}
	
	public void setNomeTabela(String nomeTabela) {
		if (nomeTabela != null)
		{
			this.nomeTabela = nomeTabela.toUpperCase().trim();
		}
		else
			this.nomeTabela = null;
			
		
	}
		
	public String getCampoPrincipal() {
		String retorno = null;
		
		if (campoPrincipal != null)
			retorno = campoPrincipal.toUpperCase().trim();
		return retorno;
	}
	
	public void setCampoPrincipal(String campoPrincipal) {
		if (campoPrincipal != null)
		{
			this.campoPrincipal = campoPrincipal.toUpperCase().trim();
		}
		else
			this.campoPrincipal = null;
			
		
	}
		
	public String getDescricaoCampo() {
		String retorno = null;
		
		if (descricaoCampo != null)
			retorno = descricaoCampo.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricaoCampo(String descricaoCampo) {
		if (descricaoCampo != null)
		{
			this.descricaoCampo = descricaoCampo.toUpperCase().trim();
		}
		else
			this.descricaoCampo = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
