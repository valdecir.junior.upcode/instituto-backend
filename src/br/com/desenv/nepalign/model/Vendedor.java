package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="vendedor")


public class Vendedor extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idVendedor")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@Column(name="nome")
	private String nome;
	
	@Column(name = "foto")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] foto;
	@Column(name="codigoVendedorCache")
	private Integer codigoVendedorCache;
	
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;

	public Vendedor()
	{
		super();
	}

	public Vendedor
	(
	EmpresaFisica empresaFisica,
		String nome,
		byte[] foto,
		Integer codigoVendedorCache,
	Usuario usuario
	) 
	{
		super();
		this.empresaFisica = empresaFisica;
		this.nome = nome;
		this.foto = foto;
		this.codigoVendedorCache = codigoVendedorCache;
		this.usuario = usuario;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) 
	{
		 if (id != null && id == 0)
		  {
		   this.id = null;
		  }
		  else
		  {
		   this.id = id;
		  }
	}
	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public String getNome() {
		String retorno = null;
		
		if (nome != null)
			retorno = nome.toUpperCase().trim();
		return retorno;
	}
	
	public void setNome(String nome) {
		if (nome != null)
		{
			this.nome = nome.toUpperCase().trim();
		}
		else
			this.nome = null;
			
		
	}
		
	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}	
	public Integer getCodigoVendedorCache() {
		return codigoVendedorCache;
	}

	public void setCodigoVendedorCache(Integer codigoVendedorCache) {
		if (codigoVendedorCache != null && codigoVendedorCache == 0)
		{
			this.codigoVendedorCache = null;
		}
		else
		{
			this.codigoVendedorCache = codigoVendedorCache;
		}
	}	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/		
	}

	@Override
	public Vendedor clone() throws CloneNotSupportedException
	{
		return (Vendedor) super.clone();
	}
}