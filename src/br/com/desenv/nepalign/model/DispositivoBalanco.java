package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name = "dispositivobalanco")
public class DispositivoBalanco extends GenericModelIGN {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idDispositivoBalanco")
	private Integer id;
	
	@OneToOne
	@JoinColumn(name = "idSequenciaBaseBalanco")
	private SequenciaBaseBalanco sequenciaBaseBalanco;
	
	@Column(name = "descricao")
	private String descricao;
	
	@Column(name = "operador1")
	private String operador1;
	
	@Column(name = "operador2")
	private String operador2;

	public DispositivoBalanco() {
		super();
	}

	public DispositivoBalanco(SequenciaBaseBalanco sequenciaBaseBalanco, String descricao, String operador1, String operador2) {
		super();
		this.sequenciaBaseBalanco = sequenciaBaseBalanco;
		this.descricao = descricao;
		this.operador1 = operador1;
		this.operador2 = operador2;
	}

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	public SequenciaBaseBalanco getSequenciaBaseBalanco() {
		return sequenciaBaseBalanco;
	}
	
	public void setSequenciaBaseBalanco(SequenciaBaseBalanco sequenciaBaseBalanco) {
		this.sequenciaBaseBalanco = sequenciaBaseBalanco;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getOperador1() {
		return operador1;
	}

	public void setOperador1(String operador1) {
		this.operador1 = operador1;
	}

	public String getOperador2() {
		return operador2;
	}

	public void setOperador2(String operador2) {
		this.operador2 = operador2;
	}
	
	@Override
	public void validate() throws Exception {
	}

}
