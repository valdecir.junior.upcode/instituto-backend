package br.com.desenv.nepalign.model;

import java.util.Date;

public class ItemCrediarioResumoVendaRecebimento 
{
	private Date data;
	private Double vendas;
	private Double entrada;
	private Double recebimentoLiquido;
	private Double recebimentoBruto;
	private Double juros;
	private Double desconto;
	private Integer tv;
	private Integer tr;
	private Integer idEmpresafisica;
	private String descricaoEmpresaFisica;
	
	private String temAcordo;
	private Double acorodoVendas;
	private Double acorodoEntrada;
	private Double acorodoRecebimentoLiquido;
	private Double acorodoRecebimentoBruto;
	private Double acorodoJuros;
	private Integer acorodoTv;
	private Integer acorodoTr;
	
	private String temVale;
	private Double valeVendas;
	private Double valeEntrada;
	private Double valeRecebimentoLiquido;
	private Double valeRecebimentoBruto;
	private Double valeJuros;
	private Double valedesconto;
	private Integer valeTv;
	private Integer valeTr;
	
	public String getTemAcordo() {
		return temAcordo;
	}
	public void setTemAcordo(String temAcordo) {
		this.temAcordo = temAcordo;
	}
	public Double getAcorodoVendas() {
		return acorodoVendas;
	}
	public void setAcorodoVendas(Double acorodoVendas) {
		this.acorodoVendas = acorodoVendas;
	}
	public Double getAcorodoEntrada() {
		return acorodoEntrada;
	}
	public void setAcorodoEntrada(Double acorodoEntrada) {
		this.acorodoEntrada = acorodoEntrada;
	}
	public Double getAcorodoRecebimentoLiquido() {
		return acorodoRecebimentoLiquido;
	}
	public void setAcorodoRecebimentoLiquido(Double acorodoRecebimentoLiquido) {
		this.acorodoRecebimentoLiquido = acorodoRecebimentoLiquido;
	}
	public Double getAcorodoRecebimentoBruto() {
		return acorodoRecebimentoBruto;
	}
	public void setAcorodoRecebimentoBruto(Double acorodoRecebimentoBruto) {
		this.acorodoRecebimentoBruto = acorodoRecebimentoBruto;
	}
	public Double getAcorodoJuros() {
		return acorodoJuros;
	}
	public void setAcorodoJuros(Double acorodoJuros) {
		this.acorodoJuros = acorodoJuros;
	}
	public Integer getAcorodoTv() {
		return acorodoTv;
	}
	public void setAcorodoTv(Integer acorodoTv) {
		this.acorodoTv = acorodoTv;
	}
	public Integer getAcorodoTr() {
		return acorodoTr;
	}
	public void setAcorodoTr(Integer acorodoTr) {
		this.acorodoTr = acorodoTr;
	}
	public Integer getIdEmpresafisica() {
		return idEmpresafisica;
	}
	public void setIdEmpresafisica(Integer idEmpresafisica) {
		this.idEmpresafisica = idEmpresafisica;
	}
	public String getDescricaoEmpresaFisica() {
		return descricaoEmpresaFisica;
	}
	public void setDescricaoEmpresaFisica(String descricaoEmpresaFisica) {
		this.descricaoEmpresaFisica = descricaoEmpresaFisica;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	
	
	public Double getVendas() {
		return vendas;
	}
	public void setVendas(Double vendas) {
		this.vendas = vendas;
	}
	
	public Double getEntrada() {
		return entrada;
	}
	public void setEntrada(Double entrada) {
		this.entrada = entrada;
	}
	public Double getRecebimentoLiquido() {
		return recebimentoLiquido;
	}
	public void setRecebimentoLiquido(Double recebimentoLiquido) {
		this.recebimentoLiquido = recebimentoLiquido;
	}
	public Double getRecebimentoBruto() {
		return recebimentoBruto;
	}
	public void setRecebimentoBruto(Double recebimentoBruto) {
		this.recebimentoBruto = recebimentoBruto;
	}
	public Double getJuros() {
		return juros;
	}
	public void setJuros(Double juros) {
		this.juros = juros;
	}
	public Integer getTv() {
		return tv;
	}
	public void setTv(Integer tv) {
		this.tv = tv;
	}
	public Integer getTr() {
		return tr;
	}
	public void setTr(Integer tr) {
		this.tr = tr;
	}
	public Double getDesconto() {
		return desconto;
	}
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	public String getTemVale() {
		return temVale;
	}
	public void setTemVale(String temVale) {
		this.temVale = temVale;
	}
	public Double getValeVendas() {
		return valeVendas;
	}
	public void setValeVendas(Double valeVendas) {
		this.valeVendas = valeVendas;
	}
	public Double getValeEntrada() {
		return valeEntrada;
	}
	public void setValeEntrada(Double valeEntrada) {
		this.valeEntrada = valeEntrada;
	}
	public Double getValeRecebimentoLiquido() {
		return valeRecebimentoLiquido;
	}
	public void setValeRecebimentoLiquido(Double valeRecebimentoLiquido) {
		this.valeRecebimentoLiquido = valeRecebimentoLiquido;
	}
	public Double getValeRecebimentoBruto() {
		return valeRecebimentoBruto;
	}
	public void setValeRecebimentoBruto(Double valeRecebimentoBruto) {
		this.valeRecebimentoBruto = valeRecebimentoBruto;
	}
	public Double getValeJuros() {
		return valeJuros;
	}
	public void setValeJuros(Double valeJuros) {
		this.valeJuros = valeJuros;
	}
	public Integer getValeTv() {
		return valeTv;
	}
	public void setValeTv(Integer valeTv) {
		this.valeTv = valeTv;
	}
	public Integer getValeTr() {
		return valeTr;
	}
	public void setValeTr(Integer valeTr) {
		this.valeTr = valeTr;
	}
	public Double getValeDesconto() {
		return valedesconto;
	}
	public void setValeDesconto(Double valedesconto) {
		this.valedesconto = valedesconto;
	}
	public boolean validarItem()
	{
		if(	   (getAcorodoEntrada()==null||getAcorodoEntrada().doubleValue()==0.0) 
			&& (getAcorodoJuros()==null || getAcorodoJuros().doubleValue()==0.0)
			&& (getAcorodoRecebimentoBruto()== null || getAcorodoRecebimentoBruto().doubleValue() ==0.0)
			&& (getAcorodoRecebimentoLiquido() == null || getAcorodoRecebimentoLiquido().doubleValue()==0.0)
			&& (getAcorodoTr() == null || getAcorodoTr().doubleValue()==0.0)
			&& (getAcorodoTv() == null || getAcorodoTv().doubleValue()==0.0)
			&& (getAcorodoVendas() == null || getAcorodoVendas().doubleValue()==0.0)
			&& (getDesconto() == null|| getDesconto().doubleValue()==0.0)
			&& getDescricaoEmpresaFisica() == null
			&& (getEntrada()==null || getEntrada().doubleValue()==0.0)
			&& getIdEmpresafisica() == null
			&& (getJuros() == null || getJuros().doubleValue()==0.0)
			&& (getRecebimentoBruto() == null || getRecebimentoBruto()==0.0 )
			&& (getRecebimentoLiquido() == null || getRecebimentoLiquido().doubleValue()==0.0 )
			&& (getTr() ==null || getTr().intValue() == 0)
			&& (getTv() == null || getTv().intValue() ==0)
			&& (getVendas() == null || getVendas().doubleValue() ==0.0)
			&&(getValeEntrada()==null||getValeEntrada().doubleValue()==0.0) 
			&& (getValeRecebimentoBruto()== null || getValeRecebimentoBruto().doubleValue() ==0.0)
			&& (getValeRecebimentoLiquido() == null || getValeRecebimentoLiquido().doubleValue()==0.0)
			&& (getValeJuros()==null || getValeJuros().doubleValue()==0.0)
			&& (getValeTr() == null || getValeTr().doubleValue()==0.0)
			&& (getValeTv() == null || getValeTv().doubleValue()==0.0)
			&& (getValeVendas() == null || getValeVendas().doubleValue()==0.0)
			&& (getValeDesconto() == null|| getValeDesconto().doubleValue()==0.0)
			)
		{
			return false;
		}
		else
		{
		return true;
		}
	}
	
}
