package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="referenciabancaria")


public class ReferenciaBancaria extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idReferenciaBancaria")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@Column(name="tipoConta")
	private String tipoConta;
	
	@Column(name="numeroBanco")
	private Integer numeroBanco;
	
	@Column(name="nomeBanco")
	private String nomeBanco;
	
	@Column(name="agencia")
	private String agencia;
	
	@Column(name="digitoVerificadorAgencia")
	private String digitoVerificadorAgencia;
	
	@Column(name="conta")
	private String conta;
	
	@Column(name="digitoVerificadorConta")
	private String digitoVerificadorConta;
	

	public ReferenciaBancaria()
	{
		super();
	}
	

	public ReferenciaBancaria
	(
	Cliente cliente,
		String tipoConta,
		Integer numeroBanco,
		String nomeBanco,
		String agencia,
		String digitoVerificadorAgencia,
		String conta,
		String digitoVerificadorConta
	) 
	{
		super();
		this.cliente = cliente;
		this.tipoConta = tipoConta;
		this.numeroBanco = numeroBanco;
		this.nomeBanco = nomeBanco;
		this.agencia = agencia;
		this.digitoVerificadorAgencia = digitoVerificadorAgencia;
		this.conta = conta;
		this.digitoVerificadorConta = digitoVerificadorConta;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public String getTipoConta() {
		String retorno = null;
		
		if (tipoConta != null)
			retorno = tipoConta.toUpperCase().trim();
		return retorno;
	}
	
	public void setTipoConta(String tipoConta) {
		if (tipoConta != null)
		{
			this.tipoConta = tipoConta.toUpperCase().trim();
		}
		else
			this.tipoConta = null;
			
		
	}
		
	public Integer getNumeroBanco() {
		return numeroBanco;
	}

	public void setNumeroBanco(Integer numeroBanco) {
		if (numeroBanco != null && numeroBanco == 0)
		{
			this.numeroBanco = null;
		}
		else
		{
			this.numeroBanco = numeroBanco;
		}
	}	
	public String getNomeBanco() {
		String retorno = null;
		
		if (nomeBanco != null)
			retorno = nomeBanco.toUpperCase().trim();
		return retorno;
	}
	
	public void setNomeBanco(String nomeBanco) {
		if (nomeBanco != null)
		{
			this.nomeBanco = nomeBanco.toUpperCase().trim();
		}
		else
			this.nomeBanco = null;
			
		
	}
		
	public String getAgencia() {
		String retorno = null;
		
		if (agencia != null)
			retorno = agencia.toUpperCase().trim();
		return retorno;
	}
	
	public void setAgencia(String agencia) {
		if (agencia != null)
		{
			this.agencia = agencia.toUpperCase().trim();
		}
		else
			this.agencia = null;
			
		
	}
		
	public String getDigitoVerificadorAgencia() {
		String retorno = null;
		
		if (digitoVerificadorAgencia != null)
			retorno = digitoVerificadorAgencia.toUpperCase().trim();
		return retorno;
	}
	
	public void setDigitoVerificadorAgencia(String digitoVerificadorAgencia) {
		if (digitoVerificadorAgencia != null)
		{
			this.digitoVerificadorAgencia = digitoVerificadorAgencia.toUpperCase().trim();
		}
		else
			this.digitoVerificadorAgencia = null;
			
		
	}
		
	public String getConta() {
		String retorno = null;
		
		if (conta != null)
			retorno = conta.toUpperCase().trim();
		return retorno;
	}
	
	public void setConta(String conta) {
		if (conta != null)
		{
			this.conta = conta.toUpperCase().trim();
		}
		else
			this.conta = null;
			
		
	}
		
	public String getDigitoVerificadorConta() {
		String retorno = null;
		
		if (digitoVerificadorConta != null)
			retorno = digitoVerificadorConta.toUpperCase().trim();
		return retorno;
	}
	
	public void setDigitoVerificadorConta(String digitoVerificadorConta) {
		if (digitoVerificadorConta != null)
		{
			this.digitoVerificadorConta = digitoVerificadorConta.toUpperCase().trim();
		}
		else
			this.digitoVerificadorConta = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
