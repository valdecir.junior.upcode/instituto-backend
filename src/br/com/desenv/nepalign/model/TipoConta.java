package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="tipoconta")


public class TipoConta extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idTipoConta")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="incideCpmf")
	private Integer incideCpmf;
	
	@Column(name="credito")
	private Integer credito;
	

	public TipoConta()
	{
		super();
	}
	

	public TipoConta
	(
		String descricao,
		Integer incideCpmf,
		Integer credito
	) 
	{
		super();
		this.descricao = descricao;
		this.incideCpmf = incideCpmf;
		this.credito = credito;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public Integer getIncideCpmf() {
		return incideCpmf;
	}

	public void setIncideCpmf(Integer incideCpmf) {
		this.incideCpmf = incideCpmf;
	}	
	public Integer getCredito() {
		return credito;
	}

	public void setCredito(Integer credito) {
		this.credito = credito;
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
