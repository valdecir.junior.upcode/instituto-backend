package br.com.desenv.nepalign.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="logCodigoBarras")
public class LogCodigoBarras extends GenericModelIGN
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idLogCodigoBarras")
	private Integer id;
	@Column(name="metodo")
	private String metodo;
	
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	
	@Column(name="data")
	private Date data;
	@Column(name="resultadoMetodo")
	private String resultadoMetodo;
	
	public LogCodigoBarras()
	{
		super();
	}
	

	public LogCodigoBarras
	(
	Usuario idUsuario,
		Double valorInicial,
		String metodo,
		Date data
	) 
	{
		super();
		this.usuario = idUsuario;
		this.data = data;
		this.metodo = metodo;
	}
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}


	public String getResultadoMetodo() {
		return resultadoMetodo;
	}


	public void setResultadoMetodo(String resultadoMetodo) {
		this.resultadoMetodo = resultadoMetodo;
	}


	@Override
	public void validate() throws Exception {
		// TODO Auto-generated method stub
		
	}

	
	
}
