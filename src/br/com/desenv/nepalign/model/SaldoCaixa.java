package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="saldoCaixa")
public class SaldoCaixa extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idSaldoCaixa")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="idEmpresaCaixa")
	private EmpresaCaixa empresaCaixa;
	
	@ManyToOne
	@JoinColumn(name="idCaixa")
	private Caixa caixa;
	
	@Column(name="mes")
	private Integer mes;
	
	@Column(name="ano")
	private Integer ano;
	
	@Column(name="saldo")
	private Double saldo;
	
	public SaldoCaixa() { super(); }

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}	

	public EmpresaCaixa getEmpresaCaixa() 
	{
		return empresaCaixa;
	}

	public void setEmpresaCaixa(EmpresaCaixa empresaCaixa) 
	{
		this.empresaCaixa = empresaCaixa;
	}

	public Caixa getCaixa() 
	{
		return caixa;
	}

	public void setCaixa(Caixa caixa) 
	{
		this.caixa = caixa;
	}

	public Integer getMes() 
	{
		return mes;
	}

	public void setMes(Integer mes) 
	{
		this.mes = (mes == null || Double.isNaN(mes)) ? null : mes;
	}

	public Integer getAno() 
	{
		return ano;
	}

	public void setAno(Integer ano) 
	{
		this.ano = (ano == null || Double.isNaN(ano)) ? null : ano;
	}

	public Double getSaldo() 
	{
		return saldo;
	}

	public void setSaldo(Double saldo) 
	{
		this.saldo = (saldo == null || Double.isNaN(saldo)) ? null : saldo;
	}

	@Override
	public void validate() throws Exception { }
}