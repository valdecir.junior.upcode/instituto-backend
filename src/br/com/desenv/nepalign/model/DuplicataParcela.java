package br.com.desenv.nepalign.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="DuplicataParcela")
public class DuplicataParcela extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idDuplicataParcela")
	private Integer id;
		 
	@ManyToOne
	@JoinColumn(name = "idDuplicata")
	private Duplicata duplicata;
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@Column(name="numeroDuplicata")
	private Long numeroDuplicata;
	
	@Column(name="numeroParcela")
	private Integer numeroParcela;
	
	@Column(name="dataVencimento")
	private Date dataVencimento;
	
	@Column(name="valorParcela")
	private Double valorParcela;
	
	@Column(name="situacao")
	private String situacao;
	
	@Column(name="valorPago")
	private Double valorPago;
	
	@Column(name="dataBaixa")
	private Date dataBaixa;
	
	@Column(name="tipoPagamento")
	private String tipoPagamento;
	
	@Column(name="dataVencimento02Acordado")
	private Date dataVencimentoAcordado;
	@ManyToOne
	@JoinColumn(name = "idFormaPagamento")
	private FormaPagamento formaPagamento;

	@ManyToOne
	@JoinColumn(name = "idChequeRecebido")
	private ChequeRecebido chequeRecebido;

	@ManyToOne
	@JoinColumn(name = "idCobrador")
	private Cobrador cobrador;
	
	@Column(name="acordo")
	private String acordo;
	
	@Column(name="valorDesconto")
	private Double valorDesconto;
	
	@Column(name="jurosInformado")
	private Double jurosInformado;
	
	@Column(name="jurosOriginal")
	private Double jurosOriginal;
	
	@Column (name="dataCobranca")
	private Date dataCobranca;
	
	@Column(name="jurosPago")
	private Double jurosPago;
	
	@Column(name="valorAPagarInformado")
	private Double valorAPagarInformado;
	
	@Column(name="valorAPagarOriginal")
	private Double valorAPagarOriginal;
	
	@Column(name="seprocadaReativada")
	private String seprocadaReativada;
	@Column(name="cobranca")
	private String cobranca;
	public DuplicataParcela()
	{
		super();
	}
	

	public DuplicataParcela
	(
	Duplicata duplicata,
	EmpresaFisica empresaFisica,
	Cliente cliente,
	Usuario usuario,
		Long numeroDuplicata,
		Integer numeroParcela,
		Date dataVencimento,
		Double valorParcela,
		String situacao,
		Double valorPago,
		Date dataBaixa,
		String tipoPagamento,
		Date dataVencimentoAcordado,
		FormaPagamento formaPagamento,
		ChequeRecebido chequeRecebido,
		Cobrador cobrador,
		String acordo,
		Double valorDesconto,
		Double jurosInformado,
		Double jurosOriginal,
		Date dataCobranca,
		Double jurosPago,
		Double valorAPagarInformado,
		Double valorAPagarOriginal,
		String seprocadaReativada,
		String cobranca
	) 
	{
		super();
		this.duplicata = duplicata;
		this.empresaFisica = empresaFisica;
		this.cliente = cliente;
		this.usuario = usuario;
		this.numeroDuplicata = numeroDuplicata;
		this.numeroParcela = numeroParcela;
		this.dataVencimento = dataVencimento;
		this.valorParcela = valorParcela;
		this.situacao = situacao;
		this.valorPago = valorPago;
		this.dataBaixa = dataBaixa;
		this.tipoPagamento = tipoPagamento;
		this.dataVencimentoAcordado = dataVencimentoAcordado;
		this.formaPagamento = formaPagamento;
		this.chequeRecebido =chequeRecebido;
		this.cobrador = cobrador;
		this.acordo = acordo;
		this.valorDesconto =valorDesconto;
		this.jurosInformado = jurosInformado;
		this.jurosOriginal = jurosOriginal;
		this.dataCobranca = dataCobranca;
		this.jurosPago = jurosPago;
		this.valorAPagarOriginal = valorAPagarOriginal;
		this.valorAPagarInformado = valorAPagarInformado;
		this.seprocadaReativada = seprocadaReativada;
		this.cobranca = cobranca;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	
	public Double getJurosInformado() 
	{
		if(jurosInformado == null)
			return Double.NaN;
		
		return jurosInformado;	
	}


	public void setJurosInformado(Double jurosInformado)
	{
		if(jurosInformado.isNaN())
		{
			this.jurosInformado = null;	
		}
		else
		{
			this.jurosInformado = jurosInformado;	
		}
	}


	public Double getValorDesconto() {
	
		
		return valorDesconto;
	}


	public void setValorDesconto(Double valorDesconto) {
		if(valorDesconto.isNaN())
		{
			this.valorDesconto = null;
		}
		
		else
		{
			this.valorDesconto = valorDesconto;	
		}
		
	}


	public String getAcordo() {
		return acordo;
	}


	public void setAcordo(String acordo) {
		this.acordo = acordo;
	}


	public Duplicata getDuplicata() {
		return duplicata;
	}

	public void setDuplicata(Duplicata duplicata) {
		this.duplicata = duplicata;
	}
	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}


	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Long getNumeroDuplicata() {
		return numeroDuplicata;
	}

	public void setNumeroDuplicata(Long numeroDuplicata) {
		this.numeroDuplicata = numeroDuplicata;
	}	
	
	public Integer getNumeroParcela() {
		return numeroParcela;
	}

	public void setNumeroParcela(Integer numeroParcela) 
	{
		this.numeroParcela = numeroParcela;
	}	

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}
	public Double getValorParcela() {
		
		if(valorParcela == null || valorParcela.isNaN())
		{
			return null;
		}
		else
		{
		return valorParcela;
		}
	}

	public void setValorParcela(Double valorParcela) 
	{
		this.valorParcela = valorParcela;
		
	}	
	public String getSituacao() {
		String retorno = null;
		
		if (situacao != null)
			retorno = situacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setSituacao(String situacao) {
		if (situacao != null)
		{
			this.situacao = situacao.toUpperCase().trim();
		}
		else
			this.situacao = null;
			
		
	}
		
	public Double getValorPago() {
		if(valorPago == null || valorPago.isNaN())
		{
			return null;
		}
		else
		{
			return valorPago;
		}
		
	}

	public void setValorPago(Double valorPago) 
	{
		this.valorPago = valorPago;	
	}	
	
	public Date getDataBaixa() {
		return dataBaixa;
	}

	public void setDataBaixa(Date dataBaixa) {
		this.dataBaixa = dataBaixa;
	}
	public String getTipoPagamento() {
		String retorno = null;
		
		if (tipoPagamento != null)
			retorno = tipoPagamento.toUpperCase().trim();
		return retorno;
	}
	
	public void setTipoPagamento(String tipoPagamento) {
		if (tipoPagamento != null)
		{
			this.tipoPagamento = tipoPagamento.toUpperCase().trim();
		}
		else
			this.tipoPagamento = null;
			
		
	}
		
	public Date getDataVencimentoAcordado() {
		return dataVencimentoAcordado;
	}

	public void setDataVencimentoAcordado(Date dataVencimentoAcordado) {
		this.dataVencimentoAcordado = dataVencimentoAcordado;
	}
	
	public ChequeRecebido getChequeRecebido() {
		return chequeRecebido;
	}


	public void setChequeRecebido(ChequeRecebido chequeRecebido) {
		this.chequeRecebido = chequeRecebido;
	}

	public Cobrador getCobrador() {
		return cobrador;
	}


	public void setCobrador(Cobrador cobrador) {
		this.cobrador = cobrador;
	}


	public Double getJurosOriginal() {
		if(jurosOriginal==null)
		{
			return null;
		}
		if(jurosOriginal.isNaN()){
			return null;
		}
		else
		{
		return jurosOriginal;
		}
	}


	public void setJurosOriginal(Double jurosOriginal) {
		if(jurosOriginal.isNaN()){
			this.jurosOriginal = null;
		}
		else
		{
		this.jurosOriginal = jurosOriginal;
		}
	}
	
	public Date getDataCobranca() {
		return dataCobranca;
	}


	public Double getJurosPago() {
		return jurosPago;
	}


	public void setJurosPago(Double jurosPago) {
		this.jurosPago = jurosPago;
	}


	public void setDataCobranca(Date dataCobranca) {
		this.dataCobranca = dataCobranca;
	}


	public Double getValorAPagarInformado() {
		return valorAPagarInformado;
	}


	public void setValorAPagarInformado(Double valorAPagarInformado) {
		this.valorAPagarInformado = valorAPagarInformado;
	}


	public Double getValorAPagarOriginal() {
		return valorAPagarOriginal;
	}


	public void setValorAPagarOriginal(Double valorAPagarOriginal) {
		this.valorAPagarOriginal = valorAPagarOriginal;
	}
	
	public String getSeprocadaReativada() {
		return seprocadaReativada;
	}


	public void setSeprocadaReativada(String seprocadaReativada) {
		this.seprocadaReativada = seprocadaReativada;
	}


	public String getCobranca() {
		return cobranca;
	}


	public void setCobranca(String cobranca) {
		this.cobranca = cobranca;
	}

	public BigDecimal getValorParcelaSemJuros(){
		Double valor = 0.0;
		if(this.getDataBaixa()!=null && this.getSituacao().equals("2"))	
		{

			Double jurosPago = this.getJurosPago()==null?0.0:this.getJurosPago();
			Double valorPago = this.getValorPago()==null?0.0:this.getValorPago();
			if(this.getValorPago()<jurosPago)
			{
				valor = (jurosPago - valorPago)+this.getValorParcela();
			}
			else 
			{
				valor = this.getValorParcela() - (valorPago - jurosPago );
			}
		}
		else
		{
			valor = this.getValorParcela();
		}
		return new BigDecimal(valor).setScale(2,BigDecimal.ROUND_HALF_EVEN);
	}
	@Override
	public DuplicataParcela clone() throws CloneNotSupportedException 
	{
        return (DuplicataParcela) super.clone();
    }

	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}
}