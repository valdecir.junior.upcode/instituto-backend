package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="conjuge")


public class Conjuge extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idConjuge")
	private Integer id;
		
	@Column(name = "foto")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] foto;
	@Column(name="nome")
	private String nome;
	
	@Column(name="cpfCnpj")
	private String cpfCnpj;
	
	@Column(name="rg")
	private String rg;
	
	@Column(name="dataNascimento")
	private Date dataNascimento;
	
	@ManyToOne
	@JoinColumn(name = "idOperadoraCelular01")
	private Operadora operadoraCelular01;
	@Column(name="telefoneCelular01")
	private String telefoneCelular01;
	
	@ManyToOne
	@JoinColumn(name = "idOperadoraCelular02")
	private Operadora operadoraCelular02;
	@Column(name="telefoneCelular02")
	private String telefoneCelular02;
	
	@Column(name="salario")
	private Double salario;
	
	@Column(name="outraRenda")
	private Double outraRenda;
	
	@Column(name="empresaTrabalha")
	private String empresaTrabalha;
	
	@Column(name="profissao")
	private String profissao;
	
	@Column(name="tempoTrabalho")
	private Date tempoTrabalho;
	
	@Column(name="logradouroComercial")
	private String logradouroComercial;
	
	@Column(name="numeroComercial")
	private String numeroComercial;
	
	@ManyToOne
	@JoinColumn(name = "idCidadeComercial")
	private Cidade cidadeComercial;
	@Column(name="cidadeComercialCache")
	private String cidadeComercialCache;
	
	@Column(name="estadoComercialCache")
	private String estadoComercialCache;
	
	@Column(name="cepComercial")
	private String cepComercial;
	
	@ManyToOne
	@JoinColumn(name = "idOperadoraComercial")
	private Operadora operadoraComercial;
	@Column(name="telefoneComercial")
	private String telefoneComercial;
	
	@Column(name="informacaoComercial")
	private String informacaoComercial;
	
	@Column(name="numeroCalcado")
	private String numeroCalcado;
	
	@Column(name="time")
	private String time;
	
	@Column(name = "fotoRg")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] fotoRg;
	@Column(name = "fotoCpf")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] fotoCpf;

	public Conjuge()
	{
		super();
	}
	

	public Conjuge
	(
		byte[] foto,
		String nome,
		String cpfCnpj,
		String rg,
		Date dataNascimento,
	Operadora operadoraCelular01,
		String telefoneCelular01,
	Operadora operadoraCelular02,
		String telefoneCelular02,
		Double salario,
		Double outraRenda,
		String empresaTrabalha,
		String profissao,
		Date tempoTrabalho,
		String logradouroComercial,
		String numeroComercial,
	Cidade cidadeComercial,
		String cidadeComercialCache,
		String estadoComercialCache,
		String cepComercial,
	Operadora operadoraComercial,
		String telefoneComercial,
		String informacaoComercial,
		String numeroCalcado,
		String time,
		byte[] fotoRg,
		byte[] fotoCpf
	) 
	{
		super();
		this.foto = foto;
		this.nome = nome;
		this.cpfCnpj = cpfCnpj;
		this.rg = rg;
		this.dataNascimento = dataNascimento;
		this.operadoraCelular01 = operadoraCelular01;
		this.telefoneCelular01 = telefoneCelular01;
		this.operadoraCelular02 = operadoraCelular02;
		this.telefoneCelular02 = telefoneCelular02;
		this.salario = salario;
		this.outraRenda = outraRenda;
		this.empresaTrabalha = empresaTrabalha;
		this.profissao = profissao;
		this.tempoTrabalho = tempoTrabalho;
		this.logradouroComercial = logradouroComercial;
		this.numeroComercial = numeroComercial;
		this.cidadeComercial = cidadeComercial;
		this.cidadeComercialCache = cidadeComercialCache;
		this.estadoComercialCache = estadoComercialCache;
		this.cepComercial = cepComercial;
		this.operadoraComercial = operadoraComercial;
		this.telefoneComercial = telefoneComercial;
		this.informacaoComercial = informacaoComercial;
		this.numeroCalcado = numeroCalcado;
		this.time = time;
		this.fotoRg = fotoRg;
		this.fotoCpf = fotoCpf;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}	
	public String getNome() {
		String retorno = null;
		
		if (nome != null)
			retorno = nome.toUpperCase().trim();
		return retorno;
	}
	
	public void setNome(String nome) {
		if (nome != null)
		{
			this.nome = nome.toUpperCase().trim();
		}
		else
			this.nome = null;
			
		
	}
		
	public String getCpfCnpj() {
		String retorno = null;
		
		if (cpfCnpj != null)
			retorno = cpfCnpj.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setCpfCnpj(String cpfCnpj) {
		if (cpfCnpj != null)
		{
			this.cpfCnpj = cpfCnpj.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.cpfCnpj = null;
			
		
	}
		
	public String getRg() {
		String retorno = null;
		
		if (rg != null)
			retorno = rg.toUpperCase().trim();
		return retorno;
	}
	
	public void setRg(String rg) {
		if (rg != null)
		{
			this.rg = rg.toUpperCase().trim();
		}
		else
			this.rg = null;
			
		
	}
		
	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public Operadora getOperadoraCelular01() {
		return operadoraCelular01;
	}

	public void setOperadoraCelular01(Operadora operadoraCelular01) {
		this.operadoraCelular01 = operadoraCelular01;
	}
	
	public String getTelefoneCelular01() {
		String retorno = null;
		
		if (telefoneCelular01 != null)
			retorno = telefoneCelular01.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setTelefoneCelular01(String telefoneCelular01) {
		if (telefoneCelular01 != null)
		{
			this.telefoneCelular01 = telefoneCelular01.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.telefoneCelular01 = null;
			
		
	}
		
	public Operadora getOperadoraCelular02() {
		return operadoraCelular02;
	}

	public void setOperadoraCelular02(Operadora operadoraCelular02) {
		this.operadoraCelular02 = operadoraCelular02;
	}
	
	public String getTelefoneCelular02() {
		String retorno = null;
		
		if (telefoneCelular02 != null)
			retorno = telefoneCelular02.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setTelefoneCelular02(String telefoneCelular02) {
		if (telefoneCelular02 != null)
		{
			this.telefoneCelular02 = telefoneCelular02.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.telefoneCelular02 = null;
			
		
	}
		
	public Double getSalario() {
		return salario;
	}

	public void setSalario(Double salario) {
		
		if (salario != null && Double.isNaN(salario))
		{
			this.salario = null;
		}
		else
		{
			this.salario = salario;
		}
		
	
	}	
	public Double getOutraRenda() {
		return outraRenda;
	}

	public void setOutraRenda(Double outraRenda) {
		if (outraRenda != null && Double.isNaN(outraRenda))
		{
			this.outraRenda = null;
		}
		else
		{
			this.outraRenda = outraRenda;
		}
	}	
	public String getEmpresaTrabalha() {
		String retorno = null;
		
		if (empresaTrabalha != null)
			retorno = empresaTrabalha.toUpperCase().trim();
		return retorno;
	}
	
	public void setEmpresaTrabalha(String empresaTrabalha) {
		if (empresaTrabalha != null)
		{
			this.empresaTrabalha = empresaTrabalha.toUpperCase().trim();
		}
		else
			this.empresaTrabalha = null;
			
		
	}
		
	public String getProfissao() {
		String retorno = null;
		
		if (profissao != null)
			retorno = profissao.toUpperCase().trim();
		return retorno;
	}
	
	public void setProfissao(String profissao) {
		if (profissao != null)
		{
			this.profissao = profissao.toUpperCase().trim();
		}
		else
			this.profissao = null;
			
		
	}
		
	public Date getTempoTrabalho() {
		return tempoTrabalho;
	}

	public void setTempoTrabalho(Date tempoTrabalho) {
		this.tempoTrabalho = tempoTrabalho;
	}
	public String getLogradouroComercial() {
		String retorno = null;
		
		if (logradouroComercial != null)
			retorno = logradouroComercial.toUpperCase().trim();
		return retorno;
	}
	
	public void setLogradouroComercial(String logradouroComercial) {
		if (logradouroComercial != null)
		{
			this.logradouroComercial = logradouroComercial.toUpperCase().trim();
		}
		else
			this.logradouroComercial = null;
			
		
	}
		
	public String getNumeroComercial() {
		String retorno = null;
		
		if (numeroComercial != null)
			retorno = numeroComercial.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroComercial(String numeroComercial) {
		if (numeroComercial != null)
		{
			this.numeroComercial = numeroComercial.toUpperCase().trim();
		}
		else
			this.numeroComercial = null;
			
		
	}
		
	public Cidade getCidadeComercial() {
		return cidadeComercial;
	}

	public void setCidadeComercial(Cidade cidadeComercial) {
		this.cidadeComercial = cidadeComercial;
	}
	
	public String getCidadeComercialCache() {
		String retorno = null;
		
		if (cidadeComercialCache != null)
			retorno = cidadeComercialCache.toUpperCase().trim();
		return retorno;
	}
	
	public void setCidadeComercialCache(String cidadeComercialCache) {
		if (cidadeComercialCache != null)
		{
			this.cidadeComercialCache = cidadeComercialCache.toUpperCase().trim();
		}
		else
			this.cidadeComercialCache = null;
			
		
	}
		
	public String getEstadoComercialCache() {
		String retorno = null;
		
		if (estadoComercialCache != null)
			retorno = estadoComercialCache.toUpperCase().trim();
		return retorno;
	}
	
	public void setEstadoComercialCache(String estadoComercialCache) {
		if (estadoComercialCache != null)
		{
			this.estadoComercialCache = estadoComercialCache.toUpperCase().trim();
		}
		else
			this.estadoComercialCache = null;
			
		
	}
		
	public String getCepComercial() {
		String retorno = null;
		
		if (cepComercial != null)
			retorno = cepComercial.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setCepComercial(String cepComercial) {
		if (cepComercial != null)
		{
			this.cepComercial = cepComercial.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.cepComercial = null;
			
		
	}
		
	public Operadora getOperadoraComercial() {
		return operadoraComercial;
	}

	public void setOperadoraComercial(Operadora operadoraComercial) {
		this.operadoraComercial = operadoraComercial;
	}
	
	public String getTelefoneComercial() {
		String retorno = null;
		
		if (telefoneComercial != null)
			retorno = telefoneComercial.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setTelefoneComercial(String telefoneComercial) {
		if (telefoneComercial != null)
		{
			this.telefoneComercial = telefoneComercial.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.telefoneComercial = null;
			
		
	}
		
	public String getInformacaoComercial() {
		String retorno = null;
		
		if (informacaoComercial != null)
			retorno = informacaoComercial.toUpperCase().trim();
		return retorno;
	}
	
	public void setInformacaoComercial(String informacaoComercial) {
		if (informacaoComercial != null)
		{
			this.informacaoComercial = informacaoComercial.toUpperCase().trim();
		}
		else
			this.informacaoComercial = null;
			
		
	}
		
	public String getNumeroCalcado() {
		String retorno = null;
		
		if (numeroCalcado != null)
			retorno = numeroCalcado.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroCalcado(String numeroCalcado) {
		if (numeroCalcado != null)
		{
			this.numeroCalcado = numeroCalcado.toUpperCase().trim();
		}
		else
			this.numeroCalcado = null;
			
		
	}
		
	public String getTime() {
		String retorno = null;
		
		if (time != null)
			retorno = time.toUpperCase().trim();
		return retorno;
	}
	
	public void setTime(String time) {
		if (time != null)
		{
			this.time = time.toUpperCase().trim();
		}
		else
			this.time = null;
			
		
	}
		
	public byte[] getFotoRg() {
		return fotoRg;
	}

	public void setFotoRg(byte[] fotoRg) {
		this.fotoRg = fotoRg;
	}	
	public byte[] getFotoCpf() {
		return fotoCpf;
	}

	public void setFotoCpf(byte[] fotoCpf) {
		this.fotoCpf = fotoCpf;
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
	}
	
	@Override
	public Conjuge clone() throws CloneNotSupportedException 
	{
		return (Conjuge) super.clone();
	}
}