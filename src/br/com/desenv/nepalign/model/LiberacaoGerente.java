package br.com.desenv.nepalign.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="liberacaoGerente")


public class LiberacaoGerente extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idLiberacaoGerente")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@OneToOne
	@JoinColumn(name = "idDuplicata")
	private Duplicata duplicata;
	@Column(name="nomeUsuario")
	private String nomeUsuario;
	
	@OneToMany(fetch=FetchType.EAGER)
	Set<DuplicataParcela> parcelasLiberadas;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataHora;
	
	
	@PrePersist
	void prePersist(){
		if(dataHora == null)
			dataHora = new Date();
	}
	public LiberacaoGerente()
	{
		super();
	}
	

	public LiberacaoGerente
	(
	Usuario usuario,
	Duplicata duplicata,
		String nomeUsuario
	) 
	{
		super();
		this.usuario = usuario;
		this.duplicata = duplicata;
		this.nomeUsuario = nomeUsuario;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Duplicata getDuplicata() {
		return duplicata;
	}

	public void setDuplicata(Duplicata duplicata) {
		this.duplicata = duplicata;
	}
	
	public String getNomeUsuario() {
		String retorno = null;
		
		if (nomeUsuario != null)
			retorno = nomeUsuario.toUpperCase().trim();
		return retorno;
	}
	
	public void setNomeUsuario(String nomeUsuario) {
		if (nomeUsuario != null)
		{
			this.nomeUsuario = nomeUsuario.toUpperCase().trim();
		}
		else
			this.nomeUsuario = null;
			
		
	}
		
	public Set<DuplicataParcela> getParcelasLiberadas() {
		if(parcelasLiberadas == null)
			parcelasLiberadas = new HashSet<>();
			
		return parcelasLiberadas;
	}


	public void setParcelasLiberadas(Set<DuplicataParcela> parcelasLiberadas) {
		this.parcelasLiberadas = parcelasLiberadas;
	}
	
	public void adicionarParcelasLiberadas(DuplicataParcela...parcelas){
		if(parcelasLiberadas == null)
			parcelasLiberadas = new HashSet<>();
			
			for (DuplicataParcela duplicataParcela : parcelas) {
				parcelasLiberadas.add(duplicataParcela);
			}
	}

	public Date getDataHora() {
		return dataHora;
	}
	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
