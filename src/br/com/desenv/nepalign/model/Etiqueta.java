package br.com.desenv.nepalign.model;

public class Etiqueta implements Cloneable
{
	private String codigoBarras;
	private String data;
	private String referencia;
	private String idReferencia;
	private String ordem;
	private String idOrdem;
	private String cor;
	private String idCor;
	private Double venda;
	private String produto;
	private String numero;
	private String quantidade;
	private String tamanho;
	private String idTamanho;
	private EmpresaFisica empresa;
	
	public Etiqueta(){}
	
	public EmpresaFisica getEmpresa() 
	{
		return empresa;
	}
	
	public void setEmpresa(EmpresaFisica empresa) 
	{
		this.empresa = empresa;
	}
	
	public String getIdReferencia() 
	{
		return idReferencia;
	}
	
	public void setIdReferencia(String idReferencia) 
	{
		this.idReferencia = idReferencia;
	}
	
	public String getIdOrdem() 
	{
		return idOrdem;
	}
	
	public void setIdOrdem(String idOrdem) 
	{
		this.idOrdem = idOrdem;
	}
	
	public String getIdTamanho() 
	{
		return idTamanho;
	}
	
	public void setIdTamanho(String idTamanho) 
	{
		this.idTamanho = idTamanho;
	}
	
	public String getTamanho() 
	{
		return tamanho;
	}
	
	public void setTamanho(String tamanho) 
	{
		this.tamanho = tamanho;
	}
	
	public String getNumero() 
	{
		return numero;
	}
	
	public void setNumero(String numero) 
	{
		this.numero = numero;
	}
	
	public String getQuantidade() 
	{
		return quantidade;
	}
	
	public void setQuantidade(String quantidade) 
	{
		this.quantidade = quantidade;
	}
	
	public String getCodigoBarras() 
	{
		return codigoBarras;
	}
	
	public void setCodigoBarras(String codigoBarras) 
	{
		this.codigoBarras = codigoBarras;
	}
	
	public String getData() 
	{
		return data;
	}
	
	public void setData(String data) 
	{
		this.data = data;
	}
	
	public String getReferencia() 
	{
		return referencia;
	}
	
	public void setReferencia(String referencia) 
	{
		this.referencia = referencia;
	}
	
	public String getOrdem() 
	{
		return ordem;
	}
	
	public void setOrdem(String ordem) 
	{
		this.ordem = ordem;
	}
	
	public String getCor() 
	{
		return cor;
	}
	
	public void setCor(String cor) 
	{
		this.cor = cor;
	}
	
	public String getIdCor()
	{
		return this.idCor;
	}
	
	public void setIdCor(String idCor)
	{
		this.idCor = idCor;
	}
	
	public Double getVenda() 
	{
		return venda;
	}
	
	public void setVenda(Double venda) 
	{
		this.venda = venda;
	}
	
	public String getProduto() 
	{
		return produto;
	}
	
	public void setProduto(String produto) 
	{
		this.produto = produto;
	}
	
	@Override
	public Etiqueta clone() throws CloneNotSupportedException
	{
		return (Etiqueta) super.clone();
	}
}