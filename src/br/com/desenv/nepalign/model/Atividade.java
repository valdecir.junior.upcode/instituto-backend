package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="atividade")


public class Atividade extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idAtividade")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="cnae")
	private String cnae;
	

	public Atividade()
	{
		super();
	}
	

	public Atividade
	(
		String descricao,
		String cnae
	) 
	{
		super();
		this.descricao = descricao;
		this.cnae = cnae;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public String getCnae() {
		String retorno = null;
		
		if (cnae != null)
			retorno = cnae.toUpperCase().trim();
		return retorno;
	}
	
	public void setCnae(String cnae) {
		if (cnae != null)
		{
			this.cnae = cnae.toUpperCase().trim();
		}
		else
			this.cnae = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
