package br.com.desenv.nepalign.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="marca")


public class Marca extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idMarca")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;

	@Column(name="ultimaAlteracao")
	private Date ultimaAlteracao;
	
	@Column(name="codigo")
	private Integer codigo;
	
	@Column(name="franquia")
	private Integer franquia;

	public Marca()
	{
		super();
	}
	

	public Marca
	(
		String descricao
	) 
	{
		super();
		this.descricao = descricao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;		
	}
	
	/*public void setId(Integer id) {
	
		this.id = id;
	}*/	

	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
	
	public Integer getCodigo() {
		return codigo;
	}


	public void setCodigo(Integer codigo) {
		this.codigo = codigo;		
	}


	public Date getUltimaAlteracao() {
		return ultimaAlteracao;
	}


	public void setUltimaAlteracao(Date ultimaAlteracao) {
		this.ultimaAlteracao = ultimaAlteracao;
	}
	
	public Integer getFranquia(){
		return franquia;
	}
	
	public void setFranquia(Integer franquia){
		this.franquia = franquia;
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}
	
	@Override
	public boolean equals(Object object) 
	{
		if(object == null)
			return false;
		if(!object.getClass().equals(this.getClass()))
			return false;
		
		if(((Marca) object).getDescricao().equals(getDescricao()))
			return true;
		
		return super.equals(object);
	}
}
