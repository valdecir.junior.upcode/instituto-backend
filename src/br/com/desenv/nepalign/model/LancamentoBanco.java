package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="lancamentobanco")
public class LancamentoBanco extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idLancamentoBanco")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name="idEmpresaBancos")
	private EmpresaBancos empresaBancos;
	
	@ManyToOne
	@JoinColumn(name="idBancos")
	private Bancos banco;
	
	@ManyToOne
	@JoinColumn(name="idContaContabil")
	private ContaContabil contaContabil;
	
	@ManyToOne
	@JoinColumn(name="idTipoMovimentoBancos")
	private TipoMovimentoBancos tipoMovimentoBancos;
	
	@Column(name="dataLancamento")
	private Date dataLancamento;
	
	@Column(name="dataCompensacao")
	private Date dataCompensacao;
	
	@Column(name="dataRecebimento")
	private Date dataRecebimento;
	
	@Column(name="valor")
	private Double valor;

	@Column(name="observacao")
	private String observacao;

	public LancamentoBanco() { super(); }

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}	

	public EmpresaBancos getEmpresaBancos() 
	{
		return empresaBancos;
	}

	public void setEmpresaBancos(EmpresaBancos empresaBancos) 
	{
		this.empresaBancos = empresaBancos;
	}

	public Bancos getBanco() 
	{
		return banco;
	}

	public void setBanco(Bancos banco) 
	{
		this.banco = banco;
	}

	public ContaContabil getContaContabil() 
	{
		return contaContabil;
	}

	public void setContaContabil(ContaContabil contaContabil) 
	{
		this.contaContabil = contaContabil;
	}

	public TipoMovimentoBancos getTipoMovimentoBancos() 
	{
		return tipoMovimentoBancos;
	}

	public void setTipoMovimentoBancos(TipoMovimentoBancos tipoMovimentoBancos) 
	{
		this.tipoMovimentoBancos = tipoMovimentoBancos;
	}

	public Date getDataLancamento() 
	{
		return dataLancamento;
	}

	public void setDataLancamento(Date dataLancamento) 
	{
		this.dataLancamento = dataLancamento;
	}

	public Date getDataCompensacao() 
	{
		return dataCompensacao;
	}

	public void setDataCompensacao(Date dataCompensacao) 
	{
		this.dataCompensacao = dataCompensacao;
	}

	public Date getDataRecebimento() 
	{
		return dataRecebimento;
	}

	public void setDataRecebimento(Date dataRecebimento) 
	{
		this.dataRecebimento = dataRecebimento;
	}

	public Double getValor() 
	{
		return valor;
	}

	public void setValor(Double valor) 
	{
		this.valor = (valor == null || Double.isNaN(valor)) ? null : valor;
	}

	public String getObservacao() 
	{
		return observacao == null ? null : observacao.toUpperCase();
	}

	public void setObservacao(String observacao) 
	{
		this.observacao = observacao == null ? null : observacao.toUpperCase();
	}

	@Override
	public void validate() throws Exception { }
}