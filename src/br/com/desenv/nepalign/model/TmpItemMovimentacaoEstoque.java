package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="tmpitemmovimentacaoestoque")


public class TmpItemMovimentacaoEstoque extends GenericModelIGN 
{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="idTmpItemMovimentacaoEstoque")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEstoqueProduto")
	private EstoqueProduto estoqueProduto;
	@ManyToOne
	@JoinColumn(name = "idTmpMovimentacaoEstoque")
	private TmpMovimentacaoEstoque tmpMovimentacaoEstoque;
	@ManyToOne
	@JoinColumn(name = "idUnidadeMedida")
	private UnidadeMedida unidadeMedida;
	@ManyToOne
	@JoinColumn(name = "idProduto")
	private Produto produto;
	@Column(name="idDocumentoOrigem")
	private String documentoOrigem;
	
	@Column(name="sequencialItem")
	private Integer sequencialItem;
	
	@Column(name="quantidade")
	private Double quantidade;
	
	@Column(name="valor")
	private Double valor;
	
	@Column(name="custo")
	private Double custo;
	
	@Column(name="numeroLote")
	private String numeroLote;
	
	@Column(name="dataValidade")
	private Date dataValidade;
	
	@Column(name="dataFabricacao")
	private Date dataFabricacao;
	
	@Column(name="venda")
	private Double venda;
	

	public TmpItemMovimentacaoEstoque()
	{
		super();
	}
	

	public TmpItemMovimentacaoEstoque
	(
	EstoqueProduto estoqueProduto,
	TmpMovimentacaoEstoque tmpMovimentacaoEstoque,
	UnidadeMedida unidadeMedida,
	Produto produto,
		String documentoOrigem,
		Integer sequencialItem,
		Double quantidade,
		Double valor,
		Double custo,
		String numeroLote,
		Date dataValidade,
		Date dataFabricacao,
		Double venda
	) 
	{
		super();
		this.estoqueProduto = estoqueProduto;
		this.tmpMovimentacaoEstoque = tmpMovimentacaoEstoque;
		this.unidadeMedida = unidadeMedida;
		this.produto = produto;
		this.documentoOrigem = documentoOrigem;
		this.sequencialItem = sequencialItem;
		this.quantidade = quantidade;
		this.valor = valor;
		this.custo = custo;
		this.numeroLote = numeroLote;
		this.dataValidade = dataValidade;
		this.dataFabricacao = dataFabricacao;
		this.venda = venda;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public EstoqueProduto getEstoqueProduto() {
		return estoqueProduto;
	}

	public void setEstoqueProduto(EstoqueProduto estoqueProduto) {
		this.estoqueProduto = estoqueProduto;
	}
	
	public TmpMovimentacaoEstoque getTmpMovimentacaoEstoque() {
		return tmpMovimentacaoEstoque;
	}

	public void setTmpMovimentacaoEstoque(TmpMovimentacaoEstoque tmpMovimentacaoEstoque) {
		this.tmpMovimentacaoEstoque = tmpMovimentacaoEstoque;
	}
	
	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	public String getDocumentoOrigem() {
		String retorno = null;
		
		if (documentoOrigem != null)
			retorno = documentoOrigem.toUpperCase().trim();
		return retorno;
	}
	
	public void setDocumentoOrigem(String documentoOrigem) {
		if (documentoOrigem != null)
		{
			this.documentoOrigem = documentoOrigem.toUpperCase().trim();
		}
		else
			this.documentoOrigem = null;
			
		
	}
		
	public Integer getSequencialItem() {
		return sequencialItem;
	}

	public void setSequencialItem(Integer sequencialItem) {
		this.sequencialItem = sequencialItem;
	}	
	public Double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Double quantidade) {
	
		if (quantidade != null && Double.isNaN(quantidade))
		{
			this.quantidade = null;
		}
		else
		{
			this.quantidade = quantidade;
		}
		
	}	
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
	
		if (valor != null && Double.isNaN(valor))
		{
			this.valor = null;
		}
		else
		{
			this.valor = valor;
		}
		
	}	
	public Double getCusto() {
		return custo;
	}

	public void setCusto(Double custo) {
	
		if (custo != null && Double.isNaN(custo))
		{
			this.custo = null;
		}
		else
		{
			this.custo = custo;
		}
		
	}	
	public String getNumeroLote() {
		String retorno = null;
		
		if (numeroLote != null)
			retorno = numeroLote.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroLote(String numeroLote) {
		if (numeroLote != null)
		{
			this.numeroLote = numeroLote.toUpperCase().trim();
		}
		else
			this.numeroLote = null;
			
		
	}
		
	public Date getDataValidade() {
		return dataValidade;
	}

	public void setDataValidade(Date dataValidade) {
		this.dataValidade = dataValidade;
	}
	public Date getDataFabricacao() {
		return dataFabricacao;
	}

	public void setDataFabricacao(Date dataFabricacao) {
		this.dataFabricacao = dataFabricacao;
	}
	public Double getVenda() {
		return venda;
	}

	public void setVenda(Double venda) {
	
		if (venda != null && Double.isNaN(venda))
		{
			this.venda = null;
		}
		else
		{
			this.venda = venda;
		}
		
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
