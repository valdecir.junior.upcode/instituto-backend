package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="logimportacaocache")


public class LogImportacaoCache extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idLogImportacaoCache")
	private Integer id;
		
	@Column(name="dataHora")
	private Date dataHora;
	
	@ManyToOne
	@JoinColumn(name = "idTipoOperacaoLV")
	private TipoOperacaoLV tipoOperacaoLV;
	@Column(name="descricaoErro")
	private String descricaoErro;
	

	public LogImportacaoCache()
	{
		super();
	}
	

	public LogImportacaoCache
	(
		Date dataHora,
	TipoOperacaoLV tipoOperacaoLV,
		String descricaoErro
	) 
	{
		super();
		this.dataHora = dataHora;
		this.tipoOperacaoLV = tipoOperacaoLV;
		this.descricaoErro = descricaoErro;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}
	public TipoOperacaoLV getTipoOperacaoLV() {
		return tipoOperacaoLV;
	}

	public void setTipoOperacaoLV(TipoOperacaoLV tipoOperacaoLV) {
		this.tipoOperacaoLV = tipoOperacaoLV;
	}
	
	public String getDescricaoErro() {
		String retorno = null;
		
		if (descricaoErro != null)
			retorno = descricaoErro.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricaoErro(String descricaoErro) {
		if (descricaoErro != null)
		{
			this.descricaoErro = descricaoErro.toUpperCase().trim();
		}
		else
			this.descricaoErro = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
