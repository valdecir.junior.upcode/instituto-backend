package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="itemdocumentoentradasaida")


public class ItemDocumentoEntradaSaida extends GenericModelIGN
{
	public static final String ENTREGUE = "1";
	public static final String NAO_ENTREGUE = "2";
	public static final String BAIXA_AUTORIZADA = "3";
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idItemDocumentoEntradaSaida")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idDocumentoEntradaSaida", nullable=false)
	private DocumentoEntradaSaida documentoEntradaSaida;
	
	
	@ManyToOne
	@JoinColumn(name = "idProduto")
	private Produto produto;
	@ManyToOne
	@JoinColumn(name = "idCor")
	private Cor cor;
	@ManyToOne
	@JoinColumn(name = "idOrdemProduto")
	private OrdemProduto ordemProduto;
	@Column(name="sequencial")
	private Integer sequencial;
	
	@Column(name="descricaoProduto")
	private String descricaoProduto;
	
	@ManyToOne
	@JoinColumn(name = "idGrade")
	private Grade grade;
	@Column(name="quantidade1")
	private Integer quantidade1;
	
	@Column(name="quantidade2")
	private Integer quantidade2;
	
	@Column(name="quantidade3")
	private Integer quantidade3;
	
	@Column(name="quantidade4")
	private Integer quantidade4;
	
	@Column(name="quantidade5")
	private Integer quantidade5;
	
	@Column(name="quantidade6")
	private Integer quantidade6;
	
	@Column(name="quantidade7")
	private Integer quantidade7;
	
	@Column(name="quantidade8")
	private Integer quantidade8;
	
	@Column(name="quantidade9")
	private Integer quantidade9;
	
	@Column(name="quantidade10")
	private Integer quantidade10;
	
	@Column(name="quantidade11")
	private Integer quantidade11;
	
	@Column(name="quantidade12")
	private Integer quantidade12;
	
	@Column(name="quantidade13")
	private Integer quantidade13;
	
	@Column(name="quantidade14")
	private Integer quantidade14;
	
	@Column(name="quantidade15")
	private Integer quantidade15;
	
	@Column(name="quantidade16")
	private Integer quantidade16;
	
	@Column(name="quantidade17")
	private Integer quantidade17;
	
	@Column(name="quantidadeTotal")
	private Integer quantidadeTotal;
	
	@Column(name="valorUnitarioCompra")
	private Double valorUnitarioCompra;
	
	@Column(name="valorUnitarioVenda")
	private Double valorUnitarioVenda;
	
	@Column(name="valorTotalCompra")
	private Double valorTotalCompra;
	
	@Column(name="valorTotalVenda")
	private Double valorTotalVenda;
	
	@Column(name="situacao")
	private String situacao;
	
	@Column(name="observacao")
	private String observacao;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idItemPedidoCompraVenda")
	private ItemPedidoCompraVenda itemPedidoCompraVenda;
	

	public ItemDocumentoEntradaSaida()
	{
		super();
	}
	

	public ItemDocumentoEntradaSaida
	(
	DocumentoEntradaSaida documentoEntradaSaida,
	Produto produto,
	Cor cor,
	OrdemProduto ordemProduto,
		Integer sequencial,
		String descricaoProduto,
	Grade grade,
		Integer quantidade1,
		Integer quantidade2,
		Integer quantidade3,
		Integer quantidade4,
		Integer quantidade5,
		Integer quantidade6,
		Integer quantidade7,
		Integer quantidade8,
		Integer quantidade9,
		Integer quantidade10,
		Integer quantidade11,
		Integer quantidade12,
		Integer quantidade13,
		Integer quantidade14,
		Integer quantidade15,
		Integer quantidade16,
		Integer quantidade17,
		Integer quantidadeTotal,
		Double valorUnitarioCompra,
		Double valorUnitarioVenda,
		Double valorTotalCompra,
		Double valorTotalVenda,
		String situacao,
		String observacao
	) 
	{
		super();
		this.documentoEntradaSaida = documentoEntradaSaida;
		this.produto = produto;
		this.cor = cor;
		this.ordemProduto = ordemProduto;
		this.sequencial = sequencial;
		this.descricaoProduto = descricaoProduto;
		this.grade = grade;
		this.quantidade1 = quantidade1;
		this.quantidade2 = quantidade2;
		this.quantidade3 = quantidade3;
		this.quantidade4 = quantidade4;
		this.quantidade5 = quantidade5;
		this.quantidade6 = quantidade6;
		this.quantidade7 = quantidade7;
		this.quantidade8 = quantidade8;
		this.quantidade9 = quantidade9;
		this.quantidade10 = quantidade10;
		this.quantidade11 = quantidade11;
		this.quantidade12 = quantidade12;
		this.quantidade13 = quantidade13;
		this.quantidade14 = quantidade14;
		this.quantidade15 = quantidade15;
		this.quantidade16 = quantidade16;
		this.quantidade17 = quantidade17;
		this.quantidadeTotal = quantidadeTotal;
		this.valorUnitarioCompra = valorUnitarioCompra;
		this.valorUnitarioVenda = valorUnitarioVenda;
		this.valorTotalCompra = valorTotalCompra;
		this.valorTotalVenda = valorTotalVenda;
		this.situacao = situacao;
		this.observacao = observacao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
		
	}	
	public DocumentoEntradaSaida getDocumentoEntradaSaida() {
		return documentoEntradaSaida;
	}

	public void setDocumentoEntradaSaida(DocumentoEntradaSaida documentoEntradaSaida) {
		this.documentoEntradaSaida = documentoEntradaSaida;
	}
	
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	public Cor getCor() {
		return cor;
	}

	public void setCor(Cor cor) {
		this.cor = cor;
	}
	
	public OrdemProduto getOrdemProduto() {
		return ordemProduto;
	}

	public void setOrdemProduto(OrdemProduto ordemProduto) {
		this.ordemProduto = ordemProduto;
	}
	
	public Integer getSequencial() {
		return sequencial;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
		
	}	
	public String getDescricaoProduto() {
		String retorno = null;
		
		if (descricaoProduto != null)
			retorno = descricaoProduto.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricaoProduto(String descricaoProduto) {
		if (descricaoProduto != null)
		{
			this.descricaoProduto = descricaoProduto.toUpperCase().trim();
		}
		else
			this.descricaoProduto = null;
			
		
	}
		
	public Grade getGrade() {
		return grade;
	}

	public void setGrade(Grade grade) {
		this.grade = grade;
	}
	
	public Integer getQuantidade1() {
		return quantidade1;
	}

	public void setQuantidade1(Integer quantidade1) {
		this.quantidade1 = quantidade1;
		
	}	
	public Integer getQuantidade2() {
		return quantidade2;
	}

	public void setQuantidade2(Integer quantidade2) {
		this.quantidade2 = quantidade2;
		
	}	
	public Integer getQuantidade3() {
		return quantidade3;
	}

	public void setQuantidade3(Integer quantidade3) {
		this.quantidade3 = quantidade3;
		
	}	
	public Integer getQuantidade4() {
		return quantidade4;
	}

	public void setQuantidade4(Integer quantidade4) {
		this.quantidade4 = quantidade4;
		
	}	
	public Integer getQuantidade5() {
		return quantidade5;
	}

	public void setQuantidade5(Integer quantidade5) {
		this.quantidade5 = quantidade5;
		
	}	
	public Integer getQuantidade6() {
		return quantidade6;
	}

	public void setQuantidade6(Integer quantidade6) {
		this.quantidade6 = quantidade6;
		
	}	
	public Integer getQuantidade7() {
		return quantidade7;
	}

	public void setQuantidade7(Integer quantidade7) {
		this.quantidade7 = quantidade7;
		
	}	
	public Integer getQuantidade8() {
		return quantidade8;
	}

	public void setQuantidade8(Integer quantidade8) {
		this.quantidade8 = quantidade8;
		
	}	
	public Integer getQuantidade9() {
		return quantidade9;
	}

	public void setQuantidade9(Integer quantidade9) {
		this.quantidade9 = quantidade9;
		
	}	
	public Integer getQuantidade10() {
		return quantidade10;
	}

	public void setQuantidade10(Integer quantidade10) {
		this.quantidade10 = quantidade10;
		
	}	
	public Integer getQuantidade11() {
		return quantidade11;
	}

	public void setQuantidade11(Integer quantidade11) {
		this.quantidade11 = quantidade11;
		
	}	
	public Integer getQuantidade12() {
		return quantidade12;
	}

	public void setQuantidade12(Integer quantidade12) {
		this.quantidade12 = quantidade12;
		
	}	
	public Integer getQuantidade13() {
		return quantidade13;
	}

	public void setQuantidade13(Integer quantidade13) {
		this.quantidade13 = quantidade13;
		
	}	
	public Integer getQuantidade14() {
		return quantidade14;
	}

	public void setQuantidade14(Integer quantidade14) {
		this.quantidade14 = quantidade14;
		
	}	
	public Integer getQuantidade15() {
		return quantidade15;
	}

	public void setQuantidade15(Integer quantidade15) {
		this.quantidade15 = quantidade15;
		
	}	
	public Integer getQuantidade16() {
		return quantidade16;
	}

	public void setQuantidade16(Integer quantidade16) {
		this.quantidade16 = quantidade16;
		
	}	
	public Integer getQuantidade17() {
		return quantidade17;
	}

	public void setQuantidade17(Integer quantidade17) {
		this.quantidade17 = quantidade17;
		
	}	
	public Integer getQuantidadeTotal() {
		return quantidadeTotal;
	}

	public void setQuantidadeTotal(Integer quantidadeTotal) {
		this.quantidadeTotal = quantidadeTotal;
		
	}	
	public Double getValorUnitarioCompra() {
		return valorUnitarioCompra;
	}

	public void setValorUnitarioCompra(Double valorUnitarioCompra) {
		
		if (valorUnitarioCompra != null && Double.isNaN(valorUnitarioCompra))
		{
			this.valorUnitarioCompra = null;
		}
		else
		{
			this.valorUnitarioCompra = valorUnitarioCompra;
		}	
	}	
	public Double getValorUnitarioVenda() {
		return valorUnitarioVenda;
	}

	public void setValorUnitarioVenda(Double valorUnitarioVenda) {
		
		if (valorUnitarioVenda != null && Double.isNaN(valorUnitarioVenda))
		{
			this.valorUnitarioVenda = null;
		}
		else
		{
			this.valorUnitarioVenda = valorUnitarioVenda;
		}	
	}	
	public Double getValorTotalCompra() {
		return valorTotalCompra;
	}

	public void setValorTotalCompra(Double valorTotalCompra) {
		if (valorTotalCompra != null && Double.isNaN(valorTotalCompra))
		{
			this.valorTotalCompra = null;
		}
		else
		{
			this.valorTotalCompra = valorTotalCompra;
		}	
		
	}	
	public Double getValorTotalVenda() {
		return valorTotalVenda;
	}

	public void setValorTotalVenda(Double valorTotalVenda) {
		if (valorTotalVenda != null && Double.isNaN(valorTotalVenda))
		{
			this.valorTotalVenda = null;
		}
		else
		{
			this.valorTotalVenda = valorTotalVenda;
		}	
		
		
		
	}	
	public String getSituacao() {
		String retorno = null;
		
		if (situacao != null)
			retorno = situacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setSituacao(String situacao) {
		if (situacao != null)
		{
			this.situacao = situacao.toUpperCase().trim();
		}
		else
			this.situacao = null;
			
		
	}
		
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}


	public ItemPedidoCompraVenda getItemPedidoCompraVenda() {
		return itemPedidoCompraVenda;
	}


	public void setItemPedidoCompraVenda(ItemPedidoCompraVenda itemPedidoCompraVenda) {
		this.itemPedidoCompraVenda = itemPedidoCompraVenda;
	}

}
