package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="impostostaxastarifasestado")


public class ImpostosTaxasTarifasEstado extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idImpostosTaxasTarifasEstado")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idUf")
	private Uf uf;
	@ManyToOne
	@JoinColumn(name = "idImpostosTaxasTarifas")
	private ImpostosTaxasTarifas impostostaxastarifas;
	@Column(name="valorPercentual")
	private Double valorPercentual;
	
	@Column(name="valorPercentualInter")
	private Double valorPercentualInter;
	

	public ImpostosTaxasTarifasEstado()
	{
		super();
	}
	

	public ImpostosTaxasTarifasEstado
	(
	Uf uf,
	ImpostosTaxasTarifas impostostaxastarifas,
		Double valorPercentual,
		Double valorPercentualInter
	) 
	{
		super();
		this.uf = uf;
		this.impostostaxastarifas = impostostaxastarifas;
		this.valorPercentual = valorPercentual;
		this.valorPercentualInter = valorPercentualInter;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}
	
	public ImpostosTaxasTarifas getImpostostaxastarifas() {
		return impostostaxastarifas;
	}

	public void setImpostostaxastarifas(ImpostosTaxasTarifas impostostaxastarifas) {
		this.impostostaxastarifas = impostostaxastarifas;
	}
	
	public Double getValorPercentual() {
		return valorPercentual;
	}

	public void setValorPercentual(Double valorPercentual) {
	
		if (valorPercentual != null && Double.isNaN(valorPercentual))
		{
			this.valorPercentual = null;
		}
		else
		{
			this.valorPercentual = valorPercentual;
		}
		
	}	
	public Double getValorPercentualInter() {
		return valorPercentualInter;
	}

	public void setValorPercentualInter(Double valorPercentualInter) {
	
		if (valorPercentualInter != null && Double.isNaN(valorPercentualInter))
		{
			this.valorPercentualInter = null;
		}
		else
		{
			this.valorPercentualInter = valorPercentualInter;
		}
		
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
