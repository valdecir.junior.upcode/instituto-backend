package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="agenda")


public class Agenda extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idAgenda")
	private Integer id;
		
	@Column(name="nome")
	private String nome;
	
	@ManyToOne
	@JoinColumn(name = "idOperadoraTelefoneComercial")
	private Operadora operadoraTelefoneComercial;
	@Column(name="telefoneComecial")
	private String telefoneComecial;
	
	@ManyToOne
	@JoinColumn(name = "idOperadoraTelefoneResidencial")
	private Operadora operadoraTelefoneResidencial;
	@Column(name="telefoneResidencial")
	private String telefoneResidencial;
	
	@ManyToOne
	@JoinColumn(name = "idOperadoraCelular1")
	private Operadora operadoraCelular1;
	@Column(name="celular1")
	private String celular1;
	
	@ManyToOne
	@JoinColumn(name = "idOperadora2Celular2")
	private Operadora operadoraCelular2;
	@Column(name="celular2")
	private String celular2;
	
	@Column(name="emailComercial")
	private String emailComercial;
	
	@Column(name="emailPessoal")
	private String emailPessoal;
	
	@Column(name="cidade")
	private String cidade;
	
	@Column(name = "foto")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] foto;

	public Agenda()
	{
		super();
	}
	

	public Agenda
	(
		String nome,
	Operadora operadoraTelefoneComercial,
		String telefoneComecial,
	Operadora operadoraTelefoneResidencial,
		String telefoneResidencial,
	Operadora operadoraCelular1,
		String celular1,
	Operadora operadoraCelular2,
		String celular2,
		String emailComercial,
		String emailPessoal,
		String cidade,
		byte[] foto
	) 
	{
		super();
		this.nome = nome;
		this.operadoraTelefoneComercial = operadoraTelefoneComercial;
		this.telefoneComecial = telefoneComecial;
		this.operadoraTelefoneResidencial = operadoraTelefoneResidencial;
		this.telefoneResidencial = telefoneResidencial;
		this.operadoraCelular1 = operadoraCelular1;
		this.celular1 = celular1;
		this.operadoraCelular2 = operadoraCelular2;
		this.celular2 = celular2;
		this.emailComercial = emailComercial;
		this.emailPessoal = emailPessoal;
		this.cidade = cidade;
		this.foto = foto;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public String getNome() {
		String retorno = null;
		
		if (nome != null)
			retorno = nome.toUpperCase().trim();
		return retorno;
	}
	
	public void setNome(String nome) {
		if (nome != null)
		{
			this.nome = nome.toUpperCase().trim();
		}
		else
			this.nome = null;
			
		
	}
		
	public Operadora getOperadoraTelefoneComercial() {
		return operadoraTelefoneComercial;
	}

	public void setOperadoraTelefoneComercial(Operadora operadoraTelefoneComercial) {
		this.operadoraTelefoneComercial = operadoraTelefoneComercial;
	}
	
	public String getTelefoneComecial() {
		String retorno = null;
		
		if (telefoneComecial != null)
			retorno = telefoneComecial.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setTelefoneComecial(String telefoneComecial) {
		if (telefoneComecial != null)
		{
			this.telefoneComecial = telefoneComecial.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.telefoneComecial = null;
			
		
	}
		
	public Operadora getOperadoraTelefoneResidencial() {
		return operadoraTelefoneResidencial;
	}

	public void setOperadoraTelefoneResidencial(Operadora operadoraTelefoneResidencial) {
		this.operadoraTelefoneResidencial = operadoraTelefoneResidencial;
	}
	
	public String getTelefoneResidencial() {
		String retorno = null;
		
		if (telefoneResidencial != null)
			retorno = telefoneResidencial.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setTelefoneResidencial(String telefoneResidencial) {
		if (telefoneResidencial != null)
		{
			this.telefoneResidencial = telefoneResidencial.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.telefoneResidencial = null;
			
		
	}
		
	public Operadora getOperadoraCelular1() {
		return operadoraCelular1;
	}

	public void setOperadoraCelular1(Operadora operadoraCelular1) {
		this.operadoraCelular1 = operadoraCelular1;
	}
	
	public String getCelular1() {
		String retorno = null;
		
		if (celular1 != null)
			retorno = celular1.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setCelular1(String celular1) {
		if (celular1 != null)
		{
			this.celular1 = celular1.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.celular1 = null;
			
		
	}
		
	public Operadora getOperadoraCelular2() {
		return operadoraCelular2;
	}

	public void setOperadoraCelular2(Operadora operadoraCelular2) {
		this.operadoraCelular2 = operadoraCelular2;
	}
	
	public String getCelular2() {
		String retorno = null;
		
		if (celular2 != null)
			retorno = celular2.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setCelular2(String celular2) {
		if (celular2 != null)
		{
			this.celular2 = celular2.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.celular2 = null;
			
		
	}
		
	public String getEmailComercial() {
		String retorno = null;
		
		if (emailComercial != null)
			retorno = emailComercial.toUpperCase().trim();
		return retorno;
	}
	
	public void setEmailComercial(String emailComercial) {
		if (emailComercial != null)
		{
			this.emailComercial = emailComercial.toUpperCase().trim();
		}
		else
			this.emailComercial = null;
			
		
	}
		
	public String getEmailPessoal() {
		String retorno = null;
		
		if (emailPessoal != null)
			retorno = emailPessoal.toUpperCase().trim();
		return retorno;
	}
	
	public void setEmailPessoal(String emailPessoal) {
		if (emailPessoal != null)
		{
			this.emailPessoal = emailPessoal.toUpperCase().trim();
		}
		else
			this.emailPessoal = null;
			
		
	}
		
	public String getCidade() {
		String retorno = null;
		
		if (cidade != null)
			retorno = cidade.toUpperCase().trim();
		return retorno;
	}
	
	public void setCidade(String cidade) {
		if (cidade != null)
		{
			this.cidade = cidade.toUpperCase().trim();
		}
		else
			this.cidade = null;
			
		
	}
		
	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
