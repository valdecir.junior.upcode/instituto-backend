package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="tabeladatacarteiracobranca")


public class TabelaCarteiraCobranca extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idTabeladataCarteiraCobranca")
	private Integer id;
		
	@Column(name="data")
	private Date data;
	
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@Column(name="percentual")
	private Double percentual;
	@ManyToOne
	@JoinColumn(name = "idEmpresaFinanceiro")
	private EmpresaFinanceiro empresaFinanceiro;
	
	@Column(name = "tipo")
	private String tipo;

	public TabelaCarteiraCobranca()
	{
		super();
	}
	

	public TabelaCarteiraCobranca
	(
		Date data,
	EmpresaFisica empresaFisica,
		Double percentual,
		String tipo,
		EmpresaFinanceiro empresaFinanceiro
	) 
	{
		super();
		this.data = data;
		this.empresaFisica = empresaFisica;
		this.percentual = percentual;
		this.tipo = tipo;
		this.empresaFinanceiro = empresaFinanceiro;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public Double getPercentual() {
		return percentual;
	}

	public void setPercentual(Double percentual) {
	
		if (percentual != null && Double.isNaN(percentual))
		{
			this.percentual = null;
		}
		else
		{
			this.percentual = percentual;
		}
		
	}	
	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public EmpresaFinanceiro getEmpresaFinanceiro() {
		return empresaFinanceiro;
	}


	public void setEmpresaFinanceiro(EmpresaFinanceiro empresaFinanceiro) {
		this.empresaFinanceiro = empresaFinanceiro;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
