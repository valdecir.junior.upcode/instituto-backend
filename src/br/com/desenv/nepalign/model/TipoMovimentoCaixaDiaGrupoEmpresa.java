package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="tipomovimentocaixadia")


public class TipoMovimentoCaixaDiaGrupoEmpresa extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idTipoMovimentoCaixaDiaGrupoEmpresa")
	private Integer id;

	@ManyToOne
	@JoinColumn(name="idTipoMovimentoCaixaDia")
	private TipoMovimentoCaixaDia tipoMovimentoCaixaDia;
	
	@ManyToOne
	@JoinColumn(name = "idGrupoEmpresa")
	private GrupoEmpresa grupoEmpresa;
	
	
	public TipoMovimentoCaixaDiaGrupoEmpresa()
	{
		super();
	}
	

	public TipoMovimentoCaixaDiaGrupoEmpresa
	(
		TipoMovimentoCaixaDia tipoMovimentoCaixaDia,
		GrupoEmpresa grupoEmpresa
		) 
	{
		super();
		this.tipoMovimentoCaixaDia =tipoMovimentoCaixaDia;
		this.grupoEmpresa = grupoEmpresa;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	

	public TipoMovimentoCaixaDia getTipoMovimentoCaixaDia() {
		return tipoMovimentoCaixaDia;
	}


	public void setTipoMovimentoCaixaDia(TipoMovimentoCaixaDia tipoMovimentoCaixaDia) {
		this.tipoMovimentoCaixaDia = tipoMovimentoCaixaDia;
	}


	public GrupoEmpresa getGrupoEmpresa() {
		return grupoEmpresa;
	}


	public void setGrupoEmpresa(GrupoEmpresa grupoEmpresa) {
		this.grupoEmpresa = grupoEmpresa;
	}


	@Override
	public void validate() throws Exception
	{	
	}
}