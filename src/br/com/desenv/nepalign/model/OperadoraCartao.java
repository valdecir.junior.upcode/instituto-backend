package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="operadoracartao")
public class OperadoraCartao extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;

	public static final int CIELO = 0x01;
	public static final int REDECARD = 0x02;
	public static final int HIPERCARD = 0x03;
	public static final int COOPERCRED= 0x04;
	public static final int ASSEMPAR = 0x05;
	public static final int SENFF = 0x06;
	public static final int BIN = 0x07;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idOperadoraCartao")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;

	public OperadoraCartao()
	{
		super();
	}
	

	public OperadoraCartao(String descricao) 
	{
		super();
		this.descricao = descricao;
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public String getDescricao() 
	{
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setDescricao(String descricao) 
	{
		if (descricao != null)
			this.descricao = descricao.toUpperCase().trim();
		else
			this.descricao = null;
	}
		
	@Override
	public void validate() throws Exception { }
}