package br.com.desenv.nepalign.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name = "PRODUTO")
public class Produto extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "IDPRODUTO")
	private Integer id;

	@Column(name = "CODIGOPRODUTO")
	private String codigoProduto;

	@Column(name = "CODIGOEAN")
	private String codigoEan;

	@Column(name = "NOMEPRODUTO")
	private String nomeProduto;

	@Column(name = "CODIGONCM")
	private String codigoNcm;

	@Column(name = "EXPTIPI")
	private String exptipi;

	@ManyToOne
	@JoinColumn(name = "IDUNIDADEMEDIDA")
	private UnidadeMedida unidadeMedida;
	@ManyToOne
	@JoinColumn(name = "IDTIPOITEM")
	private TipoItem tipoItem;
	@ManyToOne
	@JoinColumn(name = "IDREGIMETRIBUTARIO")
	private RegimeTributario regimeTributario;
	@ManyToOne
	@JoinColumn(name = "IDSITUACAOTRIBUTARIA")
	private SituacaoTributaria situacaoTributaria;
	@ManyToOne
	@JoinColumn(name = "IDORIGEMMERCADORIA")
	private OrigemMercadoria origemMercadoria;
	@ManyToOne
	@JoinColumn(name = "IDMVA")
	private Mva mva;
	@Column(name = "ALIQUOTAICMS")
	private Double aliquotaIcms;

	@ManyToOne
	@JoinColumn(name = "IDLISTASERVICO")
	private ListaServico listaServico;
	@ManyToOne
	@JoinColumn(name = "IDGRUPOPRODUTO")
	private GrupoProduto grupo;
	@Column(name = "SUBSTITUTOTRIBUTARIO")
	private String substitutoTributario;

	@Column(name = "APLICACAO")
	private String aplicacao;

	@Column(name = "ATIVOINATIVO")
	private String ativoInativo;

	@ManyToOne
	@JoinColumn(name = "IDMARCA")
	private Marca marca;
	@ManyToOne
	@JoinColumn(name = "IDFORNECEDOR")
	private Fornecedor fornecedor;
	
	@ManyToOne
	@JoinColumn(name = "idTipoLista")
	private TipoLista tipoLista;
	
	@Column(name = "PESOPRODUTO")
	private Double pesoProduto;

	@Column(name = "CODIGOCATALOGO")
	private String codigoCatalogo;

	@Column(name = "CODIGOFABRICANTE")
	private String codigoFabricante;

	@Column(name = "NOME1PRODUTO")
	private String nome1Produto;

	@Column(name = "NOME2PRODUTO")
	private String nome2Produto;

	@Column(name = "NOME3PRODUTO")
	private String nome3Produto;

	@Column(name = "NOME4PRODUTO")
	private String nome4Produto;

	@Column(name = "NOME5PRODUTO")
	private String nome5Produto;

	@Column(name = "VALORESDESCRITIVO1")
	private String valoresDescritivo1;

	@Column(name = "VALORESDESCRITIVO2")
	private String valoresDescritivo2;

	@Column(name = "VALORESDESCRITIVO3")
	private String valoresDescritivo3;

	@Column(name = "GARANTIA")
	private Integer garantia;

	@Column(name = "ALTURA")
	private Integer altura;

	@Column(name = "LARGURA")
	private Integer largura;

	@Column(name = "PROFUNDIDADE")
	private Integer profundidade;

	@ManyToOne
	@JoinColumn(name = "IDGENERO")
	private Genero genero;
	@ManyToOne
	@JoinColumn(name = "IDOCASIAOUSO")
	private OcasiaoUso ocasiaoUso;
	@ManyToOne
	@JoinColumn(name = "IDMATERIAL")
	private Material material;
	@Column(name = "LOCALFOTO")
	private String localfoto;

	@Column(name = "FOTOEXEMPLO")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] exemploFoto;
	@Column(name = "ULTIMAATERACAO")
	private Date ultimaAlteracao;

	@Column(name = "PRODUTOLV")
	private String produtoLV;

	@Column(name = "ULTALTERACAOPRECO")
	private Date ultAlteracaoPreco;

	@ManyToOne
	@JoinColumn(name = "IDSITUACAOENVIOLV")
	private SituacaoEnvioLV situacaoEnvioLV;
	
	@Column(name = "PRECOPROMOCAO")
	private Double precoPromocao;
	

	public Produto()
	{
		super();
	}

	public Produto(String codigoProduto, String codigoEan, String nomeProduto, String codigoNcm, String exptipi, UnidadeMedida unidadeMedida, TipoItem tipoItem, RegimeTributario regimeTributario, SituacaoTributaria situacaoTributaria, OrigemMercadoria origemMercadoria, Mva mva, Double aliquotaIcms,
			ListaServico listaServico, GrupoProduto grupo, String substitutoTributario, String aplicacao, String ativoInativo, Marca marca, Fornecedor fornecedor, Double pesoProduto, String codigoCatalogo, String codigoFabricante, String nome1Produto, String nome2Produto, String nome3Produto,
			String nome4Produto, String nome5Produto, String valoresDescritivo1, String valoresDescritivo2, String valoresDescritivo3, Integer garantia, Integer altura, Integer largura, Integer profundidade, Genero genero, OcasiaoUso ocasiaoUso, Material material, String localfoto,
			byte[] exemploFoto, Date ultimaAlteracao, String produtoLV, Date ultAlteracaoPreco, SituacaoEnvioLV situacaoEnvioLV, Double precoPromocao)
	{
		super();
		this.codigoProduto = codigoProduto;
		this.codigoEan = codigoEan;
		this.nomeProduto = nomeProduto;
		this.codigoNcm = codigoNcm;
		this.exptipi = exptipi;
		this.unidadeMedida = unidadeMedida;
		this.tipoItem = tipoItem;
		this.regimeTributario = regimeTributario;
		this.situacaoTributaria = situacaoTributaria;
		this.origemMercadoria = origemMercadoria;
		this.mva = mva;
		this.aliquotaIcms = aliquotaIcms;
		this.listaServico = listaServico;
		this.grupo = grupo;
		this.substitutoTributario = substitutoTributario;
		this.aplicacao = aplicacao;
		this.ativoInativo = ativoInativo;
		this.marca = marca;
		this.fornecedor = fornecedor;
		this.pesoProduto = pesoProduto;
		this.codigoCatalogo = codigoCatalogo;
		this.codigoFabricante = codigoFabricante;
		this.nome1Produto = nome1Produto;
		this.nome2Produto = nome2Produto;
		this.nome3Produto = nome3Produto;
		this.nome4Produto = nome4Produto;
		this.nome5Produto = nome5Produto;
		this.valoresDescritivo1 = valoresDescritivo1;
		this.valoresDescritivo2 = valoresDescritivo2;
		this.valoresDescritivo3 = valoresDescritivo3;
		this.garantia = garantia;
		this.altura = altura;
		this.largura = largura;
		this.profundidade = profundidade;
		this.genero = genero;
		this.ocasiaoUso = ocasiaoUso;
		this.material = material;
		this.localfoto = localfoto;
		this.exemploFoto = exemploFoto;
		this.ultimaAlteracao = ultimaAlteracao;
		this.produtoLV = produtoLV;
		this.ultAlteracaoPreco = ultAlteracaoPreco;
		this.situacaoEnvioLV = situacaoEnvioLV;
		this.precoPromocao = precoPromocao;

	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getCodigoProduto()
	{
		String retorno = null;

		if (codigoProduto != null)
			retorno = codigoProduto.toUpperCase().trim();
		return retorno;
	}

	public void setCodigoProduto(String codigoProduto)
	{
		if (codigoProduto != null)
		{
			this.codigoProduto = codigoProduto.toUpperCase().trim();
		}
		else
			this.codigoProduto = null;

	}

	public String getCodigoEan()
	{
		String retorno = null;

		if (codigoEan != null)
			retorno = codigoEan.toUpperCase().trim();
		return retorno;
	}

	public void setCodigoEan(String codigoEan)
	{
		if (codigoEan != null)
		{
			this.codigoEan = codigoEan.toUpperCase().trim();
		}
		else
			this.codigoEan = null;

	}

	public String getNomeProduto()
	{
		String retorno = null;

		if (nomeProduto != null)
			retorno = nomeProduto.toUpperCase().trim();
		return retorno;
	}

	public void setNomeProduto(String nomeProduto)
	{
		if (nomeProduto != null)
		{
			this.nomeProduto = nomeProduto.toUpperCase().trim();
		}
		else
			this.nomeProduto = null;

	}

	public String getCodigoNcm()
	{
		String retorno = null;

		if (codigoNcm != null)
			retorno = codigoNcm.toUpperCase().trim();
		return retorno;
	}

	public void setCodigoNcm(String codigoNcm)
	{
		if (codigoNcm != null)
		{
			this.codigoNcm = codigoNcm.toUpperCase().trim();
		}
		else
			this.codigoNcm = null;

	}

	public String getExptipi()
	{
		String retorno = null;

		if (exptipi != null)
			retorno = exptipi.toUpperCase().trim();
		return retorno;
	}

	public void setExptipi(String exptipi)
	{
		if (exptipi != null)
		{
			this.exptipi = exptipi.toUpperCase().trim();
		}
		else
			this.exptipi = null;

	}

	public UnidadeMedida getUnidadeMedida()
	{
		return unidadeMedida;
	}

	public void setUnidadeMedida(UnidadeMedida unidadeMedida)
	{
		this.unidadeMedida = unidadeMedida;
	}

	public TipoItem getTipoItem()
	{
		return tipoItem;
	}

	public void setTipoItem(TipoItem tipoItem)
	{
		this.tipoItem = tipoItem;
	}

	public RegimeTributario getRegimeTributario()
	{
		return regimeTributario;
	}

	public void setRegimeTributario(RegimeTributario regimeTributario)
	{
		this.regimeTributario = regimeTributario;
	}

	public SituacaoTributaria getSituacaoTributaria()
	{
		return situacaoTributaria;
	}

	public void setSituacaoTributaria(SituacaoTributaria situacaoTributaria)
	{
		this.situacaoTributaria = situacaoTributaria;
	}

	public OrigemMercadoria getOrigemMercadoria()
	{
		return origemMercadoria;
	}

	public void setOrigemMercadoria(OrigemMercadoria origemMercadoria)
	{
		this.origemMercadoria = origemMercadoria;
	}

	public Mva getMva()
	{
		return mva;
	}

	public void setMva(Mva mva)
	{
		this.mva = mva;
	}

	public Double getAliquotaIcms()
	{
		return aliquotaIcms;
	}

	public void setAliquotaIcms(Double aliquotaIcms)
	{
		if (aliquotaIcms != null && Double.isNaN(aliquotaIcms))
		{
			this.aliquotaIcms = null;
		}
		else
		{
			this.aliquotaIcms = aliquotaIcms;
		}
	}

	public ListaServico getListaServico()
	{
		return listaServico;
	}

	public void setListaServico(ListaServico listaServico)
	{
		this.listaServico = listaServico;
	}

	public GrupoProduto getGrupo()
	{
		return grupo;
	}

	public void setGrupo(GrupoProduto grupo)
	{
		this.grupo = grupo;
	}

	public String getSubstitutoTributario()
	{
		String retorno = null;

		if (substitutoTributario != null)
			retorno = substitutoTributario.toUpperCase().trim();
		return retorno;
	}

	public void setSubstitutoTributario(String substitutoTributario)
	{
		if (substitutoTributario != null)
		{
			this.substitutoTributario = substitutoTributario.toUpperCase().trim();
		}
		else
			this.substitutoTributario = null;

	}

	public String getAplicacao()
	{
		String retorno = null;

		if (aplicacao != null)
			retorno = aplicacao.toUpperCase().trim();
		return retorno;
	}

	public void setAplicacao(String aplicacao)
	{
		if (aplicacao != null)
		{
			this.aplicacao = aplicacao.toUpperCase().trim();
		}
		else
			this.aplicacao = null;

	}

	public String getAtivoInativo()
	{
		String retorno = null;

		if (ativoInativo != null)
			retorno = ativoInativo.toUpperCase().trim();
		return retorno;
	}

	public void setAtivoInativo(String ativoInativo)
	{
		if (ativoInativo != null)
		{
			this.ativoInativo = ativoInativo.toUpperCase().trim();
		}
		else
			this.ativoInativo = null;

	}

	public Marca getMarca()
	{
		return marca;
	}

	public void setMarca(Marca marca)
	{
		this.marca = marca;
	}

	public Fornecedor getFornecedor()
	{
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor)
	{
		this.fornecedor = fornecedor;
	}
	
	

	public TipoLista getTipoLista() {
		return tipoLista;
	}

	public void setTipoLista(TipoLista tipoLista) {
		this.tipoLista = tipoLista;
	}

	public Double getPesoProduto()
	{
		return pesoProduto;
	}

	public void setPesoProduto(Double pesoProduto)
	{
		if (pesoProduto != null && Double.isNaN(pesoProduto))
		{
			this.pesoProduto = null;
		}
		else
		{
			this.pesoProduto = pesoProduto;
		}
	}

	public String getCodigoCatalogo()
	{
		String retorno = null;

		if (codigoCatalogo != null)
			retorno = codigoCatalogo.toUpperCase().trim();
		return retorno;
	}

	public void setCodigoCatalogo(String codigoCatalogo)
	{
		if (codigoCatalogo != null)
		{
			this.codigoCatalogo = codigoCatalogo.toUpperCase().trim();
		}
		else
			this.codigoCatalogo = null;

	}

	public String getCodigoFabricante()
	{
		String retorno = null;

		if (codigoFabricante != null)
			retorno = codigoFabricante.toUpperCase().trim();
		return retorno;
	}

	public void setCodigoFabricante(String codigoFabricante)
	{
		if (codigoFabricante != null)
		{
			this.codigoFabricante = codigoFabricante.toUpperCase().trim();
		}
		else
			this.codigoFabricante = null;

	}

	public String getNome1Produto()
	{
		String retorno = null;

		if (nome1Produto != null)
			retorno = nome1Produto.toUpperCase().trim();
		return retorno;
	}

	public void setNome1Produto(String nome1Produto)
	{
		if (nome1Produto != null)
		{
			this.nome1Produto = nome1Produto.toUpperCase().trim();
		}
		else
			this.nome1Produto = null;

	}

	public String getNome2Produto()
	{
		String retorno = null;

		if (nome2Produto != null)
			retorno = nome2Produto.toUpperCase().trim();
		return retorno;
	}

	public void setNome2Produto(String nome2Produto)
	{
		if (nome2Produto != null)
		{
			this.nome2Produto = nome2Produto.toUpperCase().trim();
		}
		else
			this.nome2Produto = null;

	}

	public String getNome3Produto()
	{
		String retorno = null;

		if (nome3Produto != null)
			retorno = nome3Produto.toUpperCase().trim();
		return retorno;
	}

	public void setNome3Produto(String nome3Produto)
	{
		if (nome3Produto != null)
		{
			this.nome3Produto = nome3Produto.toUpperCase().trim();
		}
		else
			this.nome3Produto = null;

	}

	public String getNome4Produto()
	{
		String retorno = null;

		if (nome4Produto != null)
			retorno = nome4Produto.toUpperCase().trim();
		return retorno;
	}

	public void setNome4Produto(String nome4Produto)
	{
		if (nome4Produto != null)
		{
			this.nome4Produto = nome4Produto.toUpperCase().trim();
		}
		else
			this.nome4Produto = null;

	}

	public String getNome5Produto()
	{
		String retorno = null;

		if (nome5Produto != null)
			retorno = nome5Produto.toUpperCase().trim();
		return retorno;
	}

	public void setNome5Produto(String nome5Produto)
	{
		if (nome5Produto != null)
		{
			this.nome5Produto = nome5Produto.toUpperCase().trim();
		}
		else
			this.nome5Produto = null;

	}

	public String getValoresDescritivo1()
	{
		String retorno = null;

		if (valoresDescritivo1 != null)
			retorno = valoresDescritivo1.toUpperCase().trim();
		return retorno;
	}

	public void setValoresDescritivo1(String valoresDescritivo1)
	{
		if (valoresDescritivo1 != null)
		{
			this.valoresDescritivo1 = valoresDescritivo1.toUpperCase().trim();
		}
		else
			this.valoresDescritivo1 = null;

	}

	public String getValoresDescritivo2()
	{
		String retorno = null;

		if (valoresDescritivo2 != null)
			retorno = valoresDescritivo2.toUpperCase().trim();
		return retorno;
	}

	public void setValoresDescritivo2(String valoresDescritivo2)
	{
		if (valoresDescritivo2 != null)
		{
			this.valoresDescritivo2 = valoresDescritivo2.toUpperCase().trim();
		}
		else
			this.valoresDescritivo2 = null;

	}

	public String getValoresDescritivo3()
	{
		String retorno = null;

		if (valoresDescritivo3 != null)
			retorno = valoresDescritivo3.toUpperCase().trim();
		return retorno;
	}

	public void setValoresDescritivo3(String valoresDescritivo3)
	{
		if (valoresDescritivo3 != null)
		{
			this.valoresDescritivo3 = valoresDescritivo3.toUpperCase().trim();
		}
		else
			this.valoresDescritivo3 = null;

	}

	public Integer getGarantia()
	{
		return garantia;
	}

	public void setGarantia(Integer garantia)
	{
		this.garantia = garantia;
	}

	public Integer getAltura()
	{
		return altura;
	}

	public void setAltura(Integer altura)
	{
		this.altura = altura;
	}

	public Integer getLargura()
	{
		return largura;
	}

	public void setLargura(Integer largura)
	{
		this.largura = largura;
	}

	public Integer getProfundidade()
	{
		return profundidade;
	}

	public void setProfundidade(Integer profundidade)
	{
		this.profundidade = profundidade;
	}

	public Genero getGenero()
	{
		return genero;
	}

	public void setGenero(Genero genero)
	{
		this.genero = genero;
	}

	public OcasiaoUso getOcasiaoUso()
	{
		return ocasiaoUso;
	}

	public void setOcasiaoUso(OcasiaoUso ocasiaoUso)
	{
		this.ocasiaoUso = ocasiaoUso;
	}

	public Material getMaterial()
	{
		return material;
	}

	public void setMaterial(Material material)
	{
		this.material = material;
	}

	public String getLocalfoto()
	{
		String retorno = null;

		if (localfoto != null)
			retorno = localfoto.toUpperCase().trim();
		return retorno;
	}

	public void setLocalfoto(String localfoto)
	{
		if (localfoto != null)
		{
			this.localfoto = localfoto.toUpperCase().trim();
		}
		else
			this.localfoto = null;

	}

	public byte[] getExemploFoto()
	{
		return exemploFoto;
	}

	public void setExemploFoto(byte[] exemploFoto)
	{
		this.exemploFoto = exemploFoto;
	}

	public Date getUltimaAlteracao()
	{
		return ultimaAlteracao;
	}

	public void setUltimaAlteracao(Date ultimaAlteracao)
	{
		this.ultimaAlteracao = ultimaAlteracao;
	}

	public String getProdutoLV()
	{
		String retorno = null;

		if (produtoLV != null)
			retorno = produtoLV.toUpperCase().trim();
		
		return retorno;
	}

	public void setProdutoLV(String produtoLV)
	{
		if (produtoLV != null)
			this.produtoLV = produtoLV.toUpperCase().trim();
		else
			this.produtoLV = null;
	}

	public Date getUltAlteracaoPreco()
	{
		return ultAlteracaoPreco;
	}

	public void setUltAlteracaoPreco(Date ultAlteracaoPreco)
	{
		this.ultAlteracaoPreco = ultAlteracaoPreco;
	}

	public SituacaoEnvioLV getSituacaoEnvioLV()
	{
		return situacaoEnvioLV;
	}

	public void setSituacaoEnvioLV(SituacaoEnvioLV situacaoEnvioLV)
	{
		this.situacaoEnvioLV = situacaoEnvioLV;
	}

	public Double getPrecoPromocao()
	{
		return precoPromocao;
	}

	public void setPrecoPromocao(Double precoPromocao)
	{
		if (precoPromocao != null && Double.isNaN(precoPromocao))
			this.precoPromocao = null;
		else
			this.precoPromocao = precoPromocao;
	}

	@Override
	public void validate() throws Exception
	{
		if (codigoNcm != null && codigoNcm.length() > 15) throw new Exception("Problema na referência ".concat(String.valueOf(id)).concat("! Código NCM não pode conter mais que 15 caracteres!"));
	}
}