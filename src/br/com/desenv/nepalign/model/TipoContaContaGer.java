package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="tipocontacontager")


public class TipoContaContaGer extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idTipoContaContaGer")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idTipoConta")
	private TipoConta tipoConta;
	@ManyToOne
	@JoinColumn(name = "idContaGerencial")
	private ContaGerencial contaGerencial;

	public TipoContaContaGer()
	{
		super();
	}
	

	public TipoContaContaGer
	(
	TipoConta tipoConta,
	ContaGerencial contaGerencial
	) 
	{
		super();
		this.tipoConta = tipoConta;
		this.contaGerencial = contaGerencial;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public TipoConta getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}
	
	public ContaGerencial getContaGerencial() {
		return contaGerencial;
	}

	public void setContaGerencial(ContaGerencial contaGerencial) {
		this.contaGerencial = contaGerencial;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
