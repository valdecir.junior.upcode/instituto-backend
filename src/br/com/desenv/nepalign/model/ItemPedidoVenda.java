package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="itempedidovenda")


public class ItemPedidoVenda extends GenericModelIGN implements Cloneable
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idItemPedidoVenda")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idPedidoVenda")
	private PedidoVenda pedidoVenda;

	@Column(name="itemSequencial")
	private Integer itemSequencial;
	
	@Column(name="entradaSaida")
	private String entradaSaida;
	
	@Column(name="quantidade")
	private Double quantidade;
	
	@Column(name="precoTabela")
	private Double precoTabela;
	
	@Column(name="precoVenda")
	private Double precoVenda;
	
	@Column(name="custoProduto")
	private Double custoProduto;
	
	@ManyToOne
	@JoinColumn(name = "idEstoqueProduto")
	private EstoqueProduto estoqueProduto;
	@ManyToOne
	@JoinColumn(name = "idProduto")
	private Produto produto;

	@ManyToOne
	@JoinColumn(name = "idStatusItemPedido")
	private StatusItemPedido statusItemPedido;
	
	@JoinColumn(name = "codigoRastreamento")
	private String codigoRastreamento;
	
	@Column(name="precoVendaAjustado")
	private Double precoVendaAjustado;
	
	@Column(name = "situacaoFila")
	private Integer situacaoFila;
	
	public Double getPrecoVendaAjustado() {
		return precoVendaAjustado;
	}

	
	public void setPrecoVendaAjustado(Double precoVendaAjustado) {
		this.precoVendaAjustado = precoVendaAjustado;
	}


	public StatusItemPedido getStatusItemPedido() {
		return statusItemPedido;
	}

	public void setStatusItemPedido(StatusItemPedido statusItemPedido) {
		this.statusItemPedido = statusItemPedido;
	}

	public Integer getSituacaoFila() {
		return situacaoFila;
	}
	public void setSituacaoFila(Integer situacaoFila) {
		this.situacaoFila = situacaoFila;
	}
	public ItemPedidoVenda()
	{
		super();
	}
	

	public ItemPedidoVenda
	(
	PedidoVenda pedidoVenda,
		Integer itemSequencial,
		String entradaSaida,
		Double quantidade,
		Double precoTabela,
		Double precoVenda,
		Double custoProduto,
	EstoqueProduto estoqueProduto,
	Produto produto
	) 
	{
		super();
		this.pedidoVenda = pedidoVenda;
		this.itemSequencial = itemSequencial;
		this.entradaSaida = entradaSaida;
		this.quantidade = quantidade;
		this.precoTabela = precoTabela;
		this.precoVenda = precoVenda;
		this.custoProduto = custoProduto;
		this.estoqueProduto = estoqueProduto;
		this.produto = produto;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public PedidoVenda getPedidoVenda() {
		return pedidoVenda;
	}

	public void setPedidoVenda(PedidoVenda pedidoVenda) {
		this.pedidoVenda = pedidoVenda;
	}
	
	public Integer getItemSequencial() {
		return itemSequencial;
	}

	public void setItemSequencial(Integer itemSequencial) {
		if (itemSequencial != null && itemSequencial == 0)
		{
			this.itemSequencial = null;
		}
		else
		{
			this.itemSequencial = itemSequencial;
		}
	}	
	public String getEntradaSaida() {
		String retorno = null;
		
		if (entradaSaida != null)
			retorno = entradaSaida.toUpperCase().trim();
		return retorno;
	}
	
	public void setEntradaSaida(String entradaSaida) {
		if (entradaSaida != null)
		{
			this.entradaSaida = entradaSaida.toUpperCase().trim();
		}
		else
			this.entradaSaida = null;
			
		
	}
		
	public Double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Double quantidade) {
		if (quantidade != null && quantidade == 0.0)
		{
			this.quantidade = null;
		}
		else
		{
			this.quantidade = quantidade;
		}
	}	
	public Double getPrecoTabela() {
		return precoTabela;
	}

	public void setPrecoTabela(Double precoTabela) 
	{
		this.precoTabela = precoTabela;
	}	
	public Double getPrecoVenda() {
		return precoVenda;
	}

	public void setPrecoVenda(Double precoVenda) {
			this.precoVenda = precoVenda;
	}	
	public Double getCustoProduto() {
		return custoProduto;
	}

	public void setCustoProduto(Double custoProduto) {
		if (custoProduto != null && custoProduto == 0.0)
		{
			this.custoProduto = null;
		}
		else
		{
			this.custoProduto = custoProduto;
		}
		
	}	
	public EstoqueProduto getEstoqueProduto() {
		return estoqueProduto;
	}

	public void setEstoqueProduto(EstoqueProduto estoqueProduto) {
		this.estoqueProduto = estoqueProduto;
	}
	
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	public void setCodigoRastreamento(String codigoRastreamento) 
	{
		if (codigoRastreamento != null)
			this.codigoRastreamento = codigoRastreamento.toUpperCase().trim();
		else
			this.codigoRastreamento = null;
	}
	
	public String getCodigoRastreamento()
	{
		String retorno = null;
		
		if(this.codigoRastreamento != null)
			retorno = this.codigoRastreamento.toUpperCase().trim();
		
		return retorno;
	}
	
	@Override
	public void validate() throws Exception
	{
	}
	
	public ItemPedidoVenda clone() throws CloneNotSupportedException
	{
		return (ItemPedidoVenda) super.clone();
	}
}