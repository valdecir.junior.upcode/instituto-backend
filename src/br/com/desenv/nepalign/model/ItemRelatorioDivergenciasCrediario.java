package br.com.desenv.nepalign.model;

import java.util.Date;

public class ItemRelatorioDivergenciasCrediario
{
	public Double valorRecebido;
	public Double juros;
	public Double valorDesconto;
	public String cliente;
	public Integer numeroparcela;
	public Long  numeroDuplicata;
	public String situacao;
	public Date dataBaixa;
	public Double valorParcela;
	public Date datavencimento;
	public Double valorAPagarOriginal;
	public Integer idempresafisica;
	public Double diferenca;
	public Double valorJurosOriginal;
	public String acordo;
	public String getAcordo() {
		return acordo;
	}
	public void setAcordo(String acordo) {
		this.acordo = acordo;
	}
	public Double getValorRecebido() {
		return valorRecebido;
	}
	public void setValorRecebido(Double valorRecebido) {
		this.valorRecebido = valorRecebido;
	}
	public Double getJuros() {
		return juros;
	}
	public void setJuros(Double juros) {
		this.juros = juros;
	}
	public Double getValorDesconto() {
		return valorDesconto;
	}
	public void setValorDesconto(Double valorDesconto) {
		this.valorDesconto = valorDesconto;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public Integer getNumeroparcela() {
		return numeroparcela;
	}
	public void setNumeroparcela(Integer numeroparcela) {
		this.numeroparcela = numeroparcela;
	}
	public Long getNumeroDuplicata() {
		return numeroDuplicata;
	}
	public void setNumeroDuplicata(Long numeroDuplicata) {
		this.numeroDuplicata = numeroDuplicata;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public Date getDataBaixa() {
		return dataBaixa;
	}
	public void setDataBaixa(Date dataBaixa) {
		this.dataBaixa = dataBaixa;
	}
	public Double getValorParcela() {
		return valorParcela;
	}
	public void setValorParcela(Double valorParcela) {
		this.valorParcela = valorParcela;
	}
	public Date getDatavencimento() {
		return datavencimento;
	}
	public void setDatavencimento(Date datavencimento) {
		this.datavencimento = datavencimento;
	}
	public Double getValorAPagarOriginal() {
		return valorAPagarOriginal;
	}
	public void setValorAPagarOriginal(Double valorAPagarOriginal) {
		this.valorAPagarOriginal = valorAPagarOriginal;
	}
	public Integer getIdempresafisica() {
		return idempresafisica;
	}
	public void setIdempresafisica(Integer idempresafisica) {
		this.idempresafisica = idempresafisica;
	}
	public Double getDiferenca() {
		return diferenca;
	}
	public void setDiferenca(Double diferenca) {
		this.diferenca = diferenca;
	}
	public Double getValorJurosOriginal() {
		return valorJurosOriginal;
	}
	public void setValorJurosOriginal(Double valorJurosOriginal) {
		this.valorJurosOriginal = valorJurosOriginal;
	}
	
	
}
