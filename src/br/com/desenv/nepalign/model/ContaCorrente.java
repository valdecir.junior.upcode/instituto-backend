package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="contacorrente")


public class ContaCorrente extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idContaCorrente")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEmpresa")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idBanco")
	private Banco banco;
	@Column(name="dataAberturaConta")
	private Date dataAberturaConta;
	
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="numeroAgencia")
	private String numeroAgencia;
	
	@Column(name="numeroConta")
	private String numeroConta;
	
	@Column(name="telefone")
	private String telefone;
	
	@Column(name="fax")
	private String fax;
	
	@Column(name="limite")
	private Double limite;
	
	@Column(name="vencimentoLimite")
	private Date vencimentoLimite;
	
	@Column(name="anoMesAberto")
	private String anoMesAberto;
	
	@Column(name="saldoInicial")
	private Double saldoInicial;
	
	@Column(name="gerente")
	private String gerente;
	
	@Column(name="instrucaoBoleto")
	private String instrucaoBoleto;
	
	@Column(name="carteiraCobranca")
	private String carteiraCobranca;
	
	@Column(name="sequenciaNossoNumero")
	private Integer sequenciaNossoNumero;
	
	@Column(name="idEmpresaFisica")
	private Integer empresaFisica2;
	
	@Column(name="dvContaCorrente")
	private Integer dvContaCorrente;
	
	@Column(name="postoAgencia")
	private Integer postoAgencia;
	

	public ContaCorrente()
	{
		super();
	}
	

	public ContaCorrente
	(
	EmpresaFisica empresaFisica,
	Banco banco,
		Date dataAberturaConta,
		String descricao,
		String numeroAgencia,
		String numeroConta,
		String telefone,
		String fax,
		Double limite,
		Date vencimentoLimite,
		String anoMesAberto,
		Double saldoInicial,
		String gerente,
		String instrucaoBoleto,
		String carteiraCobranca,
		Integer sequenciaNossoNumero,
		Integer empresaFisica2
	) 
	{
		super();
		this.empresaFisica = empresaFisica;
		this.banco = banco;
		this.dataAberturaConta = dataAberturaConta;
		this.descricao = descricao;
		this.numeroAgencia = numeroAgencia;
		this.numeroConta = numeroConta;
		this.telefone = telefone;
		this.fax = fax;
		this.limite = limite;
		this.vencimentoLimite = vencimentoLimite;
		this.anoMesAberto = anoMesAberto;
		this.saldoInicial = saldoInicial;
		this.gerente = gerente;
		this.instrucaoBoleto = instrucaoBoleto;
		this.carteiraCobranca = carteiraCobranca;
		this.sequenciaNossoNumero = sequenciaNossoNumero;
		this.empresaFisica2 = empresaFisica2;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	
	public Date getDataAberturaConta() {
		return dataAberturaConta;
	}

	public void setDataAberturaConta(Date dataAberturaConta) {
		this.dataAberturaConta = dataAberturaConta;
	}
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public String getNumeroAgencia() {
		String retorno = null;
		
		if (numeroAgencia != null)
			retorno = numeroAgencia.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroAgencia(String numeroAgencia) {
		if (numeroAgencia != null)
		{
			this.numeroAgencia = numeroAgencia.toUpperCase().trim();
		}
		else
			this.numeroAgencia = null;
			
		
	}
		
	public String getNumeroConta() {
		String retorno = null;
		
		if (numeroConta != null)
			retorno = numeroConta.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroConta(String numeroConta) {
		if (numeroConta != null)
		{
			this.numeroConta = numeroConta.toUpperCase().trim();
		}
		else
			this.numeroConta = null;
			
		
	}
		
	public String getTelefone() {
		String retorno = null;
		
		if (telefone != null)
			retorno = telefone.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setTelefone(String telefone) {
		if (telefone != null)
		{
			this.telefone = telefone.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.telefone = null;
			
		
	}
		
	public String getFax() {
		String retorno = null;
		
		if (fax != null)
			retorno = fax.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setFax(String fax) {
		if (fax != null)
		{
			this.fax = fax.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.fax = null;
			
		
	}
		
	public Double getLimite() {
		return limite;
	}

	public void setLimite(Double limite) {
	
		if (limite != null && Double.isNaN(limite))
		{
			this.limite = null;
		}
		else
		{
			this.limite = limite;
		}
		
	}	
	public Date getVencimentoLimite() {
		return vencimentoLimite;
	}

	public void setVencimentoLimite(Date vencimentoLimite) {
		this.vencimentoLimite = vencimentoLimite;
	}
	public String getAnoMesAberto() {
		String retorno = null;
		
		if (anoMesAberto != null)
			retorno = anoMesAberto.toUpperCase().trim();
		return retorno;
	}
	
	public void setAnoMesAberto(String anoMesAberto) {
		if (anoMesAberto != null)
		{
			this.anoMesAberto = anoMesAberto.toUpperCase().trim();
		}
		else
			this.anoMesAberto = null;
			
		
	}
		
	public Double getSaldoInicial() {
		return saldoInicial;
	}

	public void setSaldoInicial(Double saldoInicial) {
	
		if (saldoInicial != null && Double.isNaN(saldoInicial))
		{
			this.saldoInicial = null;
		}
		else
		{
			this.saldoInicial = saldoInicial;
		}
		
	}	
	public String getGerente() {
		String retorno = null;
		
		if (gerente != null)
			retorno = gerente.toUpperCase().trim();
		return retorno;
	}
	
	public void setGerente(String gerente) {
		if (gerente != null)
		{
			this.gerente = gerente.toUpperCase().trim();
		}
		else
			this.gerente = null;
			
		
	}
		
	public String getInstrucaoBoleto() {
		String retorno = null;
		
		if (instrucaoBoleto != null)
			retorno = instrucaoBoleto.toUpperCase().trim();
		return retorno;
	}
	
	public void setInstrucaoBoleto(String instrucaoBoleto) {
		if (instrucaoBoleto != null)
		{
			this.instrucaoBoleto = instrucaoBoleto.toUpperCase().trim();
		}
		else
			this.instrucaoBoleto = null;
			
		
	}
		
	public String getCarteiraCobranca() {
		String retorno = null;
		
		if (carteiraCobranca != null)
			retorno = carteiraCobranca.toUpperCase().trim();
		return retorno;
	}
	
	public void setCarteiraCobranca(String carteiraCobranca) {
		if (carteiraCobranca != null)
		{
			this.carteiraCobranca = carteiraCobranca.toUpperCase().trim();
		}
		else
			this.carteiraCobranca = null;
			
		
	}
		
	public Integer getSequenciaNossoNumero() {
		return sequenciaNossoNumero;
	}

	public void setSequenciaNossoNumero(Integer sequenciaNossoNumero) {
		this.sequenciaNossoNumero = sequenciaNossoNumero;
	}	
	public Integer getEmpresaFisica2() {
		return empresaFisica2;
	}

	public void setEmpresaFisica2(Integer empresaFisica2) {
		this.empresaFisica2 = empresaFisica2;
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}


	public Integer getDvContaCorrente() {
		return dvContaCorrente;
	}


	public void setDvContaCorrente(Integer dvContaCorrente) {
		this.dvContaCorrente = dvContaCorrente;
	}


	public Integer getPostoAgencia() {
		return postoAgencia;
	}


	public void setPostoAgencia(Integer postoAgencia) {
		this.postoAgencia = postoAgencia;
	}

}
