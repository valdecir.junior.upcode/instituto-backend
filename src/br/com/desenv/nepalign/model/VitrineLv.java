package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="vitrinelv")


public class VitrineLv extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idVitrineLv")
	private Integer id;
		
	@Column(name="nome")
	private String nome;
	
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="imagem")
	private String imagem;
	
	@Column(name="dataInicio")
	private Date dataInicio;
	
	@Column(name="dataFinal")
	private Date dataFinal;
	
	@Column(name="ativo")
	private String ativo;
	
	@Column(name="numeroColunas")
	private Integer numeroColunas;
	
	@Column(name="paginacao")
	private String paginacao;
	
	@Column(name="randomizar")
	private String randomizar;
	
	@Column(name="htmlCustomizado")
	private String htmlCustomizado;
	
	@Column(name="ordemExibicao")
	private Integer orderExibicao;
	
	@Column(name="tipoExibicao")
	private String tipoExibicao;
	
	@ManyToOne
	@JoinColumn(name = "idMarca")
	private Marca marca;
	
	@ManyToOne
	@JoinColumn(name = "idSituacaoEnvioLV")
	private SituacaoEnvioLV situacaoEnvioLV;

	public VitrineLv()
	{
		super();
	}

	public VitrineLv
	(
		String nome,
		String descricao,
		String imagem,
		Date dataInicio,
		Date dataFinal,
		String ativo,
		Integer numeroColunas,
		String paginacao,
		String randomizar,
		String htmlCustomizado,
		Integer orderExibicao,
		String tipoExibicao,
	Marca marca,
	SituacaoEnvioLV situacaoEnvioLV
	) 
	{
		super();
		this.nome = nome;
		this.descricao = descricao;
		this.imagem = imagem;
		this.dataInicio = dataInicio;
		this.dataFinal = dataFinal;
		this.ativo = ativo;
		this.numeroColunas = numeroColunas;
		this.paginacao = paginacao;
		this.randomizar = randomizar;
		this.htmlCustomizado = htmlCustomizado;
		this.orderExibicao = orderExibicao;
		this.tipoExibicao = tipoExibicao;
		this.marca = marca;
		this.situacaoEnvioLV = situacaoEnvioLV;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getNome() {
		String retorno = null;
		
		if (nome != null)
			retorno = nome.toUpperCase().trim();
		return retorno;
	}
	
	public void setNome(String nome) {
		if (nome != null)
		{
			this.nome = nome.toUpperCase().trim();
		}
		else
			this.nome = null;
			
		
	}
		
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public String getImagem() {
		String retorno = null;
		
		if (imagem != null)
			retorno = imagem.toUpperCase().trim();
		return retorno;
	}
	
	public void setImagem(String imagem) {
		if (imagem != null)
		{
			this.imagem = imagem.toUpperCase().trim();
		}
		else
			this.imagem = null;
			
		
	}
		
	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}
	public String getAtivo() {
		String retorno = null;
		
		if (ativo != null)
			retorno = ativo.toUpperCase().trim();
		return retorno;
	}
	
	public void setAtivo(String ativo) {
		if (ativo != null)
		{
			this.ativo = ativo.toUpperCase().trim();
		}
		else
			this.ativo = null;
			
		
	}
		
	public Integer getNumeroColunas() {
		return numeroColunas;
	}

	public void setNumeroColunas(Integer numeroColunas) {
		this.numeroColunas = numeroColunas;
	}	
	public String getPaginacao() {
		String retorno = null;
		
		if (paginacao != null)
			retorno = paginacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setPaginacao(String paginacao) {
		if (paginacao != null)
		{
			this.paginacao = paginacao.toUpperCase().trim();
		}
		else
			this.paginacao = null;
			
		
	}
		
	public String getRandomizar() {
		String retorno = null;
		
		if (randomizar != null)
			retorno = randomizar.toUpperCase().trim();
		return retorno;
	}
	
	public void setRandomizar(String randomizar) {
		if (randomizar != null)
		{
			this.randomizar = randomizar.toUpperCase().trim();
		}
		else
			this.randomizar = null;
			
		
	}
		
	public String getHtmlCustomizado() {
		String retorno = null;
		
		if (htmlCustomizado != null)
			retorno = htmlCustomizado.toUpperCase().trim();
		return retorno;
	}
	
	public void setHtmlCustomizado(String htmlCustomizado) {
		if (htmlCustomizado != null)
		{
			this.htmlCustomizado = htmlCustomizado.toUpperCase().trim();
		}
		else
			this.htmlCustomizado = null;
			
		
	}
		
	public Integer getOrderExibicao() {
		return orderExibicao;
	}

	public void setOrderExibicao(Integer orderExibicao) {
		this.orderExibicao = orderExibicao;
	}	
	public String getTipoExibicao() {
		String retorno = null;
		
		if (tipoExibicao != null)
			retorno = tipoExibicao.toUpperCase().trim();
		return retorno;
	}
	
	public void setTipoExibicao(String tipoExibicao) {
		if (tipoExibicao != null)
		{
			this.tipoExibicao = tipoExibicao.toUpperCase().trim();
		}
		else
			this.tipoExibicao = null;
			
		
	}
		
	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}
	
	public SituacaoEnvioLV getSituacaoEnvioLV() {
		return situacaoEnvioLV;
	}


	public void setSituacaoEnvioLV(SituacaoEnvioLV situacaoEnvioLV) {
		this.situacaoEnvioLV = situacaoEnvioLV;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
