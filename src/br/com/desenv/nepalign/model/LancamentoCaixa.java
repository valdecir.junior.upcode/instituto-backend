package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="lancamentoCaixa")
public class LancamentoCaixa extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idLancamentoCaixa")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name="idEmpresaCaixa")
	private EmpresaCaixa empresaCaixa;
	
	@ManyToOne
	@JoinColumn(name="idCaixa")
	private Caixa caixa;
	
	@ManyToOne
	@JoinColumn(name="idTipoMovimentoCaixa")
	private TipoMovimentoCaixa tipoMovimentoCaixa;
	
	@ManyToOne
	@JoinColumn(name="idContaContabil")
	private ContaContabil contaContabil;
	
	@Column(name="dataLancamento")
	private Date dataLancamento;
	
	@Column(name="valor")
	private Double valor;

	@Column(name="observacao")
	private String observacao;

	public LancamentoCaixa() { super(); }

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}	

	public EmpresaCaixa getEmpresaCaixa() 
	{
		return empresaCaixa;
	}

	public void setEmpresaCaixa(EmpresaCaixa empresaCaixa) 
	{
		this.empresaCaixa = empresaCaixa;
	}

	public Caixa getCaixa() 
	{
		return caixa;
	}

	public void setCaixa(Caixa caixa) 
	{
		this.caixa = caixa;
	}

	public TipoMovimentoCaixa getTipoMovimentoCaixa()
	{
		return tipoMovimentoCaixa;
	}

	public void setTipoMovimentoCaixa(TipoMovimentoCaixa tipoMovimentoCaixa) 
	{
		this.tipoMovimentoCaixa = tipoMovimentoCaixa;
	}

	public ContaContabil getContaContabil()
	{
		return contaContabil;
	}

	public void setContaContabil(ContaContabil contaContabil) 
	{
		this.contaContabil = contaContabil;
	}

	public Date getDataLancamento() 
	{
		return dataLancamento;
	}

	public void setDataLancamento(Date dataLancamento) 
	{
		this.dataLancamento = dataLancamento;
	}

	public Double getValor() 
	{
		return valor;
	}

	public void setValor(Double valor) 
	{
		this.valor = (valor == null || Double.isNaN(valor)) ? null : valor;
	}

	public String getObservacao() 
	{
		return observacao == null ? null : observacao.toUpperCase();
	}

	public void setObservacao(String observacao) 
	{
		this.observacao = observacao == null ? null : observacao.toUpperCase();
	}

	@Override
	public void validate() throws Exception { }
}