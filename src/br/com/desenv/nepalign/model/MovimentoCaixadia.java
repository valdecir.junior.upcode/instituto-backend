package br.com.desenv.nepalign.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="movimentocaixadia")
public class MovimentoCaixadia extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idMovimentoCaixaDia")
	private Integer id;
		
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idLancamentoContaReceber")
	private LancamentoContaReceber lancamentoContaReceber;
	@ManyToOne
	@JoinColumn(name = "idFormaPagamento")
	private FormaPagamento formaPagamento;
	@ManyToOne
	@JoinColumn(name = "idFornecedor")
	private Fornecedor fornecedor;
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@ManyToOne
	@JoinColumn(name="idTipoMovimentoCaixaDia")
	private TipoMovimentoCaixaDia tipoMovimentoCaixaDia;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="idDuplicataParcela")
	private DuplicataParcela duplicataParcela;
	


	@ManyToOne
	@JoinColumn(name = "idCaixaDia")
	private CaixaDia caixaDia;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idPagamentoPedidoVenda")
	private PagamentoPedidoVenda pagamentoPedidoVenda;
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@ManyToOne
	@JoinColumn(name = "idoperacacaorecebimentoduplicata")
	private OperacaoRecebimentoDuplicata operacao;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idTipoConta")
	private TipoConta tipoConta;
	@ManyToOne
	@JoinColumn(name = "idMovimentoParcelaDuplicata")
	private MovimentoParcelaDuplicata movimentoParcelaDuplicata;
	
	@Column(name="CreditoDebito")
	private String creditoDebito;
	
	@Column(name="numeroDocumento")
	private String numeroDocumento;
	
	@Column(name="dataVencimento")
	private Date dataVencimento;
	
	@Column(name="cpfCnpjPortador")
	private String cpfCnpjPortador;
	
	@Column(name="valor")
	private Double valor;
	
	@Column(name="situacao")
	private Integer situacao;
	
	@Column(name="observacao")
	private String observacao;
	@Column(name="diversos")
	private String diversos;
	@Column(name="mostrarBoletim")
	private String mostrarBoletim;
	
	@ManyToOne
	@JoinColumn(name = "idContaGerencial")
	private ContaGerencial contaGerencial;
	
	@ManyToOne
	@JoinColumn(name = "idOperadoraCartao")
	private OperadoraCartao operadoraCartao;
	@ManyToOne
	@JoinColumn(name = "idPagamentoCartao")
	private PagamentoCartao pagamentoCartao;
	
	public MovimentoCaixadia()
	{
		super();
	}
	

	public MovimentoCaixadia
	(
	LancamentoContaReceber lancamentoContaReceber,
	FormaPagamento formaPagamento,
	Fornecedor fornecedor,
	Cliente cliente,
		TipoMovimentoCaixaDia tipoMovimentoCaixaDia,
	CaixaDia caixaDia,
	PagamentoPedidoVenda pagamentoPedidoVenda,
	Usuario usuario,
		String creditoDebito,
		String numeroDocumento,
		Date dataVencimento,
		String cpfCnpjPortador,
		Double valor,
		Integer situacao,
		String observacao,
		DuplicataParcela duplicataParcela,
		MovimentoParcelaDuplicata movimentoParcelaDuplicata,
		OperacaoRecebimentoDuplicata operacao,
		 String diversos,
		 String mostrarBoletim,
		 ContaGerencial contaGerencial,
		 OperadoraCartao operadoraCartao,
		 PagamentoCartao pagamentoCartao
	) 
	{
		super();
		this.lancamentoContaReceber = lancamentoContaReceber;
		this.formaPagamento = formaPagamento;
		this.fornecedor = fornecedor;
		this.cliente = cliente;
		this.tipoMovimentoCaixaDia = tipoMovimentoCaixaDia;
		this.caixaDia = caixaDia;
		this.pagamentoPedidoVenda = pagamentoPedidoVenda;
		this.usuario = usuario;
		this.creditoDebito = creditoDebito;
		this.numeroDocumento = numeroDocumento;
		this.dataVencimento = dataVencimento;
		this.cpfCnpjPortador = cpfCnpjPortador;
		this.valor = valor;
		this.situacao = situacao;
		this.observacao = observacao;
		this.duplicataParcela =duplicataParcela;
		this.movimentoParcelaDuplicata = movimentoParcelaDuplicata;
		this.operacao = operacao;
		this.diversos =   diversos;
		this.mostrarBoletim = mostrarBoletim;
		this.contaGerencial = contaGerencial;
		this.operadoraCartao = operadoraCartao;
		this.pagamentoCartao = pagamentoCartao;
	}
	public DuplicataParcela getDuplicataParcela() {
		return duplicataParcela;
	}


	public void setDuplicataParcela(DuplicataParcela duplicataParcela) {
		this.duplicataParcela = duplicataParcela;
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public LancamentoContaReceber getLancamentoContaReceber() {
		return lancamentoContaReceber;
	}

	public void setLancamentoContaReceber(LancamentoContaReceber lancamentoContaReceber) {
		this.lancamentoContaReceber = lancamentoContaReceber;
	}
	
	public MovimentoParcelaDuplicata getMovimentoParcelaDuplicata() {
		return movimentoParcelaDuplicata;
	}


	public void setMovimentoParcelaDuplicata(
			MovimentoParcelaDuplicata movimentoParcelaDuplicata) {
		this.movimentoParcelaDuplicata = movimentoParcelaDuplicata;
	}


	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public TipoMovimentoCaixaDia getTipoMovimentoCaixaDia() {
		return tipoMovimentoCaixaDia;
	}

	public void setTipoMovimentoCaixaDia(TipoMovimentoCaixaDia tipoMovimentoCaixaDia) {
		this.tipoMovimentoCaixaDia = tipoMovimentoCaixaDia;
	}	
	public CaixaDia getCaixaDia() {
		return caixaDia;
	}

	public void setCaixaDia(CaixaDia caixaDia) {
		this.caixaDia = caixaDia;
	}
	
	public PagamentoPedidoVenda getPagamentoPedidoVenda() {
		return pagamentoPedidoVenda;
	}

	public void setPagamentoPedidoVenda(PagamentoPedidoVenda pagamentoPedidoVenda) {
		this.pagamentoPedidoVenda = pagamentoPedidoVenda;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String getCreditoDebito() {
		String retorno = null;
		
		if (creditoDebito != null)
			retorno = creditoDebito.toUpperCase().trim();
		return retorno;
	}
	
	public void setCreditoDebito(String creditoDebito) {
		if (creditoDebito != null)
		{
			this.creditoDebito = creditoDebito.toUpperCase().trim();
		}
		else
			this.creditoDebito = null;
			
		
	}
		
	public String getNumeroDocumento() {
		String retorno = null;
		
		if (numeroDocumento != null)
			retorno = numeroDocumento.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroDocumento(String numeroDocumento) {
		if (numeroDocumento != null)
		{
			this.numeroDocumento = numeroDocumento.toUpperCase().trim();
		}
		else
			this.numeroDocumento = null;
			
		
	}
		
	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}
	public String getCpfCnpjPortador() {
		String retorno = null;
		
		if (cpfCnpjPortador != null)
			retorno = cpfCnpjPortador.toUpperCase().trim();
		return retorno;
	}
	
	public void setCpfCnpjPortador(String cpfCnpjPortador) {
		if (cpfCnpjPortador != null)
		{
			this.cpfCnpjPortador = cpfCnpjPortador.toUpperCase().trim();
		}
		else
			this.cpfCnpjPortador = null;
			
		
	}
		
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
	
		if (valor != null && Double.isNaN(valor))
		{
			this.valor = null;
		}
		else
		{
			this.valor = valor;
		}
		
	}	
	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}	
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	public TipoConta getTipoConta() {
		return tipoConta;
	}


	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}


	public OperacaoRecebimentoDuplicata getOperacao() {
		return operacao;
	}


	public void setOperacao(OperacaoRecebimentoDuplicata operacao) {
		this.operacao = operacao;
	}


	public String getDiversos() {
		return diversos;
	}


	public void setDiversos(String diversos) {
		this.diversos = diversos;
	}


	public String getMostrarBoletim() {
		return mostrarBoletim;
	}


	public void setMostrarBoletim(String mostrarBoletim) {
		this.mostrarBoletim = mostrarBoletim;
	}


	public ContaGerencial getContaGerencial() {
		return contaGerencial;
	}


	public void setContaGerencial(ContaGerencial contaGerencial) {
		this.contaGerencial = contaGerencial;
	}


	public OperadoraCartao getOperadoraCartao() {
		return operadoraCartao;
	}


	public void setOperadoraCartao(OperadoraCartao operadoraCartao) {
		this.operadoraCartao = operadoraCartao;
	}


	public PagamentoCartao getPagamentoCartao() {
		return pagamentoCartao;
	}


	public void setPagamentoCartao(PagamentoCartao pagamentoCartao) {
		this.pagamentoCartao = pagamentoCartao;
	}

	@Override
	public void validate() throws Exception { }

	@Override
	public MovimentoCaixadia clone() throws CloneNotSupportedException 
	{
		return (MovimentoCaixadia) super.clone();
	}
}