package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="alertaloja")


public class AlertaLoja extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idAlertaLoja")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idAlertaPedidoVenda")
	private AlertaPedidoVenda alertaPedidoVenda;
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@Column(name="respostaAlerta")
	private String respostaAlerta;
	

	public AlertaLoja()
	{
		super();
	}
	

	public AlertaLoja
	(
	AlertaPedidoVenda idAlertaPedidoVenda,
	EmpresaFisica idEmpresaFisica,
		String respostaAlerta
	) 
	{
		super();
		this.alertaPedidoVenda = idAlertaPedidoVenda;
		this.empresaFisica = idEmpresaFisica;
		this.respostaAlerta = respostaAlerta;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public AlertaPedidoVenda getAlertaPedidoVenda() {
		return alertaPedidoVenda;
	}

	public void setAlertaPedidoVenda(AlertaPedidoVenda alertaPedidoVenda) {
		this.alertaPedidoVenda = alertaPedidoVenda;
	}
	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public String getRespostaAlerta() {
		String retorno = null;
		
		if (respostaAlerta != null)
			retorno = respostaAlerta.toUpperCase().trim();
		return retorno;
	}
	
	public void setRespostaAlerta(String respostaAlerta) {
		if (respostaAlerta != null)
		{
			this.respostaAlerta = respostaAlerta.toUpperCase().trim();
		}
		else
			this.respostaAlerta = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
