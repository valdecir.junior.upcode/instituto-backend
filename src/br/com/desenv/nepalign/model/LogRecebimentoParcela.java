package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="logrecebimentoparcela")


public class LogRecebimentoParcela extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idLogRecebimentoParcela")
	private Integer id;
		
	@Column(name="motivo")
	private String motivo;
	
	@Column(name="data")
	private Date data;
	
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@Column(name="valorSelecionado")
	private Double valorSelecionado;
	
	@Column(name="valorInformado")
	private Double valorInformado;
	
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;

	public LogRecebimentoParcela()
	{
		super();
	}
	

	public LogRecebimentoParcela
	(
		String motivo,
		Date data,
	Usuario usuario,
		Double valorSelecionado,
		Double valorInformado,
	Cliente cliente,
	EmpresaFisica empresaFisica
	) 
	{
		super();
		this.motivo = motivo;
		this.data = data;
		this.usuario = usuario;
		this.valorSelecionado = valorSelecionado;
		this.valorInformado = valorInformado;
		this.cliente = cliente;
		this.empresaFisica = empresaFisica;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getMotivo() {
		String retorno = null;
		
		if (motivo != null)
			retorno = motivo.toUpperCase().trim();
		return retorno;
	}
	
	public void setMotivo(String motivo) {
		if (motivo != null)
		{
			this.motivo = motivo.toUpperCase().trim();
		}
		else
			this.motivo = null;
			
		
	}
		
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Double getValorSelecionado() {
		return valorSelecionado;
	}

	public void setValorSelecionado(Double valorSelecionado) {
	
		if (valorSelecionado != null && Double.isNaN(valorSelecionado))
		{
			this.valorSelecionado = null;
		}
		else
		{
			this.valorSelecionado = valorSelecionado;
		}
		
	}	
	public Double getValorInformado() {
		return valorInformado;
	}

	public void setValorInformado(Double valorInformado) {
	
		if (valorInformado != null && Double.isNaN(valorInformado))
		{
			this.valorInformado = null;
		}
		else
		{
			this.valorInformado = valorInformado;
		}
		
	}	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
