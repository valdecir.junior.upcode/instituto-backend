package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="logoperacaonepal")


public class LogOperacaoNepal extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idLogOperacaoNepal")
	private Integer id;
		
	@Column(name="descricao")
	private String desricao;
	
	@Column(name="dataHora")
	private Date dataHora;
	
	@Column(name="tipoLog")
	private String tipoLog;
	
	@Column(name="idTipoOperacaoLV")
	private Integer tipoOperacaoLV;
	
	@Column(name="strExeption")
	private String strException;
	

	public LogOperacaoNepal()
	{
		super();
	}
	

	public LogOperacaoNepal
	(
		String desricao,
		Date dataHora,
		String tipoLog,
		Integer tipoOperacaoLV,
		String strException
	) 
	{
		super();
		this.desricao = desricao;
		this.dataHora = dataHora;
		this.tipoLog = tipoLog;
		this.tipoOperacaoLV = tipoOperacaoLV;
		this.strException = strException;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public String getDesricao() {
		String retorno = null;
		
		if (desricao != null)
			retorno = desricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDesricao(String desricao) {
		if (desricao != null)
		{
			this.desricao = desricao.toUpperCase().trim();
		}
		else
			this.desricao = null;
			
		
	}
		
	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}
	public String getTipoLog() {
		String retorno = null;
		
		if (tipoLog != null)
			retorno = tipoLog.toUpperCase().trim();
		return retorno;
	}
	
	public void setTipoLog(String tipoLog) {
		if (tipoLog != null)
		{
			this.tipoLog = tipoLog.toUpperCase().trim();
		}
		else
			this.tipoLog = null;
			
		
	}
		
	public Integer getTipoOperacaoLV() {
		return tipoOperacaoLV;
	}

	public void setTipoOperacaoLV(Integer tipoOperacaoLV) {
		if (tipoOperacaoLV != null && tipoOperacaoLV == 0)
		{
			this.tipoOperacaoLV = null;
		}
		else
		{
			this.tipoOperacaoLV = tipoOperacaoLV;
		}
	}	
	public String getStrException() {
		String retorno = null;
		
		if (strException != null)
			retorno = strException.toUpperCase().trim();
		return retorno;
	}
	
	public void setStrException(String strException) {
		if (strException != null)
		{
			this.strException = strException.toUpperCase().trim();
		}
		else
			this.strException = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
