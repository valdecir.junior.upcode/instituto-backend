package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="usuarioacao")


public class Usuarioacao extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idUsuarioAcao")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idAcao")
	private Acao acao;
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@ManyToOne
	@JoinColumn(name = "idEmpresa")
	private Empresa empresa;

	public Usuarioacao()
	{
		super();
	}
	

	public Usuarioacao
	(
	Acao acao,
	Usuario usuario,
	Empresa empresa
	) 
	{
		super();
		this.acao = acao;
		this.usuario = usuario;
		this.empresa = empresa;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public Acao getAcao() {
		return acao;
	}

	public void setAcao(Acao acao) {
		this.acao = acao;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
