package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="tmprelatoriocrosstab")


public class RelatorioCrossTab extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idTmpCrossTab")
	private Integer id;
		
	@Column(name="grupo1")
	private String grupo1;
	
	@Column(name="grupo2")
	private String grupo2;
	
	@Column(name="grupo3")
	private String grupo3;
	
	@Column(name="valor")
	private Double valor;
	
	@Column(name="indiceCampoColuna")
	private Integer indiceCampoColuna;
	
	@Column(name="campoColuna")
	private String campoColuna;
	
	@Column(name="total1000")
	private Double total1000;
	
	@Column(name="total1001")
	private Double total1001;
	
	@Column(name="total1002")
	private Double total1002;
	
	@Column(name="total1003")
	private Double total1003;
	
	@Column(name="total1004")
	private Double total1004;
	
	@Column(name="total1005")
	private Double total1005;
	
	@Column(name="idRelatorio")
	private String idRelatorio;
	

	public RelatorioCrossTab()
	{
		super();
	}
	

	public RelatorioCrossTab
	(
		String grupo1,
		String grupo2,
		String grupo3,
		Double valor,
		Integer indiceCampoColuna,
		String campoColuna,
		Double total1000,
		Double total1001,
		Double total1002,
		Double total1003,
		Double total1004,
		Double total1005,
		String idRelatorio
	) 
	{
		super();
		this.grupo1 = grupo1;
		this.grupo2 = grupo2;
		this.grupo3 = grupo3;
		this.valor = valor;
		this.indiceCampoColuna = indiceCampoColuna;
		this.campoColuna = campoColuna;
		this.total1000 = total1000;
		this.total1001 = total1001;
		this.total1002 = total1002;
		this.total1003 = total1003;
		this.total1004 = total1004;
		this.total1005 = total1005;
		this.idRelatorio = idRelatorio;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getGrupo1() {
		String retorno = null;
		
		if (grupo1 != null)
			retorno = grupo1.toUpperCase().trim();
		return retorno;
	}
	
	public void setGrupo1(String grupo1) {
		if (grupo1 != null)
		{
			this.grupo1 = grupo1.toUpperCase().trim();
		}
		else
			this.grupo1 = null;
			
		
	}
		
	public String getGrupo2() {
		String retorno = null;
		
		if (grupo2 != null)
			retorno = grupo2.toUpperCase().trim();
		return retorno;
	}
	
	public void setGrupo2(String grupo2) {
		if (grupo2 != null)
		{
			this.grupo2 = grupo2.toUpperCase().trim();
		}
		else
			this.grupo2 = null;
			
		
	}
		
	public String getGrupo3() {
		String retorno = null;
		
		if (grupo3 != null)
			retorno = grupo3.toUpperCase().trim();
		return retorno;
	}
	
	public void setGrupo3(String grupo3) {
		if (grupo3 != null)
		{
			this.grupo3 = grupo3.toUpperCase().trim();
		}
		else
			this.grupo3 = null;
			
		
	}
		
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
	
		if (valor != null && Double.isNaN(valor))
		{
			this.valor = null;
		}
		else
		{
			this.valor = valor;
		}
		
	}	
	public Integer getIndiceCampoColuna() {
		return indiceCampoColuna;
	}

	public void setIndiceCampoColuna(Integer indiceCampoColuna) {
		this.indiceCampoColuna = indiceCampoColuna;
	}	
	public String getCampoColuna() {
		String retorno = null;
		
		if (campoColuna != null)
			retorno = campoColuna.toUpperCase().trim();
		return retorno;
	}
	
	public void setCampoColuna(String campoColuna) {
		if (campoColuna != null)
		{
			this.campoColuna = campoColuna.toUpperCase().trim();
		}
		else
			this.campoColuna = null;
			
		
	}
		
	public Double getTotal1000() {
		return total1000;
	}

	public void setTotal1000(Double total1000) {
	
		if (total1000 != null && Double.isNaN(total1000))
		{
			this.total1000 = null;
		}
		else
		{
			this.total1000 = total1000;
		}
		
	}	
	public Double getTotal1001() {
		return total1001;
	}

	public void setTotal1001(Double total1001) {
	
		if (total1001 != null && Double.isNaN(total1001))
		{
			this.total1001 = null;
		}
		else
		{
			this.total1001 = total1001;
		}
		
	}	
	public Double getTotal1002() {
		return total1002;
	}

	public void setTotal1002(Double total1002) {
	
		if (total1002 != null && Double.isNaN(total1002))
		{
			this.total1002 = null;
		}
		else
		{
			this.total1002 = total1002;
		}
		
	}	
	public Double getTotal1003() {
		return total1003;
	}

	public void setTotal1003(Double total1003) {
	
		if (total1003 != null && Double.isNaN(total1003))
		{
			this.total1003 = null;
		}
		else
		{
			this.total1003 = total1003;
		}
		
	}	
	public Double getTotal1004() {
		return total1004;
	}

	public void setTotal1004(Double total1004) {
	
		if (total1004 != null && Double.isNaN(total1004))
		{
			this.total1004 = null;
		}
		else
		{
			this.total1004 = total1004;
		}
		
	}	
	public Double getTotal1005() {
		return total1005;
	}

	public void setTotal1005(Double total1005) {
	
		if (total1005 != null && Double.isNaN(total1005))
		{
			this.total1005 = null;
		}
		else
		{
			this.total1005 = total1005;
		}
		
	}	
	public String getIdRelatorio() {
		String retorno = null;
		
		if (idRelatorio != null)
			retorno = idRelatorio.toUpperCase().trim();
		return retorno;
	}
	
	public void setIdRelatorio(String idRelatorio) {
		if (idRelatorio != null)
		{
			this.idRelatorio = idRelatorio.toUpperCase().trim();
		}
		else
			this.idRelatorio = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
