package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="imagemusuario")


public class ImagemUsuario extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idImagemUsuario")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idTabelaSistema")
	private TabelaSistema tabelaSistema;
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="campoChave")
	private String campoChave;
	
	@Column(name="infoCampoChave")
	private String infoCampoChave;
	
	@Column(name = "imagem")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	private byte[] imagem;

	public ImagemUsuario()
	{
		super();
	}
	

	public ImagemUsuario
	(
	TabelaSistema tabelaSistema,
		String descricao,
		String campoChave,
		String infoCampoChave,
		byte[] imagem
	) 
	{
		super();
		this.tabelaSistema = tabelaSistema;
		this.descricao = descricao;
		this.campoChave = campoChave;
		this.infoCampoChave = infoCampoChave;
		this.imagem = imagem;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public TabelaSistema getTabelaSistema() {
		return tabelaSistema;
	}

	public void setTabelaSistema(TabelaSistema tabelaSistema) {
		this.tabelaSistema = tabelaSistema;
	}
	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public String getCampoChave() {
		String retorno = null;
		
		if (campoChave != null)
			retorno = campoChave.toUpperCase().trim();
		return retorno;
	}
	
	public void setCampoChave(String campoChave) {
		if (campoChave != null)
		{
			this.campoChave = campoChave.toUpperCase().trim();
		}
		else
			this.campoChave = null;
			
		
	}
		
	public String getInfoCampoChave() {
		String retorno = null;
		
		if (infoCampoChave != null)
			retorno = infoCampoChave.toUpperCase().trim();
		return retorno;
	}
	
	public void setInfoCampoChave(String infoCampoChave) {
		if (infoCampoChave != null)
		{
			this.infoCampoChave = infoCampoChave.toUpperCase().trim();
		}
		else
			this.infoCampoChave = null;
			
		
	}
		
	public byte[] getImagem() {
		return imagem;
	}

	public void setImagem(byte[] imagem) {
		this.imagem = imagem;
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
