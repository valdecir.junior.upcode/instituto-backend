package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="caracteristicasfiscaiscliente")


public class Caracteristicasfiscaiscliente extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idCaracteristicasFiscaisCliente")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idCategoriaFiscal")
	private CategoriaFiscal categoriaFiscal;
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@Column(name="situacao")
	private Integer situacao;
	
	@Column(name="reducaoBaseCalculo")
	private Double reducaoBaseCalculo;
	
	@Column(name="observacaoNotaFiscal")
	private String observacaoNotaFiscal;
	

	public Caracteristicasfiscaiscliente()
	{
		super();
	}
	

	public Caracteristicasfiscaiscliente
	(
	CategoriaFiscal categoriaFiscal,
	Cliente cliente,
		Integer situacao,
		Double reducaoBaseCalculo,
		String observacaoNotaFiscal
	) 
	{
		super();
		this.categoriaFiscal = categoriaFiscal;
		this.cliente = cliente;
		this.situacao = situacao;
		this.reducaoBaseCalculo = reducaoBaseCalculo;
		this.observacaoNotaFiscal = observacaoNotaFiscal;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public CategoriaFiscal getCategoriaFiscal() {
		return categoriaFiscal;
	}

	public void setCategoriaFiscal(CategoriaFiscal categoriaFiscal) {
		this.categoriaFiscal = categoriaFiscal;
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}	
	public Double getReducaoBaseCalculo() {
		return reducaoBaseCalculo;
	}

	public void setReducaoBaseCalculo(Double reducaoBaseCalculo) {
	
		if (reducaoBaseCalculo != null && Double.isNaN(reducaoBaseCalculo))
		{
			this.reducaoBaseCalculo = null;
		}
		else
		{
			this.reducaoBaseCalculo = reducaoBaseCalculo;
		}
		
	}	
	public String getObservacaoNotaFiscal() {
		String retorno = null;
		
		if (observacaoNotaFiscal != null)
			retorno = observacaoNotaFiscal.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacaoNotaFiscal(String observacaoNotaFiscal) {
		if (observacaoNotaFiscal != null)
		{
			this.observacaoNotaFiscal = observacaoNotaFiscal.toUpperCase().trim();
		}
		else
			this.observacaoNotaFiscal = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
