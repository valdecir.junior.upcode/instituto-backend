package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="planopagamento")


public class PlanoPagamento extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idPlanoPagamento")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idTipoPagamento")
	private TipoPagamento tipoPagamento;
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="tipoPercentual")
	private String tipoPercentual;
	
	@Column(name="percentual")
	private Double percentual;
	
	@Column(name="diasCarencia")
	private Integer diasCarencia;
	
	@Column(name="quantParcelas")
	private Integer quantParcelas;
	
	@ManyToOne
	@JoinColumn(name = "idFormaPagamento")
	private FormaPagamento formaPagamento;
	@Column(name="diasPagamento")
	private String diasPagamento;
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	
	@Column(name="crediario")
	String crediario;
	@Column(name="grupoPlano")
	private String grupoPlano;
	@Column(name="codigoPlano")
	private String codigoPlano;
	
	public PlanoPagamento()
	{
		super();
	}
	

	public PlanoPagamento
	(
	TipoPagamento tipoPagamento,
		String descricao,
		String tipoPercentual,
		Double percentual,
		Integer diasCarencia,
		Integer quantParcelas,
	FormaPagamento formaPagamento,
		String diasPagamento,
		EmpresaFisica empresaFisica,
		String crediario,
		String grupoPlano,
		String codigoPlano
	) 
	{
		super();
		this.tipoPagamento = tipoPagamento;
		this.descricao = descricao;
		this.tipoPercentual = tipoPercentual;
		this.percentual = percentual;
		this.diasCarencia = diasCarencia;
		this.quantParcelas = quantParcelas;
		this.formaPagamento = formaPagamento;
		this.diasPagamento = diasPagamento;
		this.empresaFisica = empresaFisica;
		this.crediario = crediario;
		this.grupoPlano = grupoPlano;
		this.codigoPlano = codigoPlano;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public TipoPagamento getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(TipoPagamento tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}
	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public String getCrediario() {
		return crediario;
	}


	public void setCrediario(String crediario) {
		this.crediario = crediario;
	}


	public String getTipoPercentual() {
		String retorno = null;
		
		if (tipoPercentual != null)
			retorno = tipoPercentual.toUpperCase().trim();
		return retorno;
	}
	
	public void setTipoPercentual(String tipoPercentual) {
		if (tipoPercentual != null)
		{
			this.tipoPercentual = tipoPercentual.toUpperCase().trim();
		}
		else
			this.tipoPercentual = null;
			
		
	}
		
	public Double getPercentual() {
		return percentual;
	}

	public void setPercentual(Double percentual) {
	
		if (percentual != null && Double.isNaN(percentual))
		{
			this.percentual = null;
		}
		else
		{
			this.percentual = percentual;
		}
		
	}	
	public Integer getDiasCarencia() {
		return diasCarencia;
	}

	public void setDiasCarencia(Integer diasCarencia) {
		this.diasCarencia = diasCarencia;
	}	
	public Integer getQuantParcelas() {
		return quantParcelas;
	}

	public void setQuantParcelas(Integer quantParcelas) {
		this.quantParcelas = quantParcelas;
	}	
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
	public String getDiasPagamento() {
		String retorno = null;
		
		if (diasPagamento != null)
			retorno = diasPagamento.toUpperCase().trim();
		return retorno;
	}
	
	public void setDiasPagamento(String diasPagamento) {
		if (diasPagamento != null)
		{
			this.diasPagamento = diasPagamento.toUpperCase().trim();
		}
		else
			this.diasPagamento = null;
			
		
	}
		
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}


	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}


	public String getGrupoPlano() {
		return grupoPlano;
	}


	public void setGrupoPlano(String grupoPlano) {
		this.grupoPlano = grupoPlano;
	}


	public String getCodigoPlano() {
		return codigoPlano;
	}


	public void setCodigoPlano(String codigoPlano) {
		this.codigoPlano = codigoPlano;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
