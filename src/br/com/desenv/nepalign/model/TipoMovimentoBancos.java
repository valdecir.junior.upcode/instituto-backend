package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="tipoMovimentoBancos")
public class TipoMovimentoBancos extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idTipoMovimentoBancos")
	private Integer id;
	
	@Column(name="codigo")
	private String codigo;
	
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="creditoDebito")
	private Integer creditoDebito;
	
	public TipoMovimentoBancos() { super(); }

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}	
	
	public String getCodigo() 
	{
		return codigo != null ? codigo.toUpperCase().trim() : null;
	}
	
	public void setCodigo(String codigo) 
	{
		this.codigo = codigo != null ? codigo.toUpperCase().trim() : null;
	}

	public String getDescricao() 
	{
		return descricao != null ? descricao.toUpperCase().trim() : null;
	}
	
	public void setDescricao(String descricao) 
	{
		this.descricao = descricao != null ? descricao.toUpperCase().trim() : null;
	}
		
	public Integer getCreditoDebito() 
	{
		return creditoDebito;
	}

	public void setCreditoDebito(Integer creditoDebito) 
	{
		this.creditoDebito = (creditoDebito == null || Double.isNaN(creditoDebito)) ? null : creditoDebito;
	}

	@Override
	public void validate() throws Exception { }
}