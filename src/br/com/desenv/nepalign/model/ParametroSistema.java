package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="parametrosistema")


public class ParametroSistema extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idParametroSistema")
	private Integer id;
		
	@Column(name="chave")
	private String chave;
	
	@Column(name="valor")
	private String valor;
	
	@Column(name="descricao")
	private String descricao;
	
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;

	public ParametroSistema()
	{
		super();
	}
	

	public ParametroSistema
	(
		String chave,
		String valor,
		String descricao,
	EmpresaFisica empresaFisica
	) 
	{
		super();
		this.chave = chave;
		this.valor = valor;
		this.descricao = descricao;
		this.empresaFisica = empresaFisica;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getChave() {
		String retorno = null;
		
		if (chave != null)
			retorno = chave.toUpperCase().trim();
		return retorno;
	}
	
	public void setChave(String chave) {
		if (chave != null)
		{
			this.chave = chave.toUpperCase().trim();
		}
		else
			this.chave = null;
			
		
	}
		
	public String getValor() 
	{
		String retorno = null;
		
		if (valor != null)
			retorno = valor.trim();
		
		return retorno;
	}
	
	public void setValor(String valor) {
		if (valor != null)
		{
			this.valor = valor.trim();
		}
		else
			this.valor = null;
			
		
	}
		
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
