package br.com.desenv.nepalign.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="preLancamentocontapagarrateio")
public class PreLancamentoContaPagarRateio extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idPreLancamentoContaPagarRateio")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "idPreLancamentoContaPagar")
	private PreLancamentoContaPagar preLancamentoContaPagar;
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@Column(name="porcentagemRateio")
	private BigDecimal porcentagemRateio;
	
	@Override
	public Integer getId() 
	{
		return id;
	}

	@Override
	public void setId(Integer id) 
	{
		this.id = id;
	}

	public PreLancamentoContaPagar getPreLancamentoContaPagar()
	{
		return preLancamentoContaPagar;
	}

	public void setPreLancamentoContaPagar(PreLancamentoContaPagar preLancamentoContaPagar) 
	{
		this.preLancamentoContaPagar = preLancamentoContaPagar;
	}

	public EmpresaFisica getEmpresaFisica()
	{
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica)
	{
		this.empresaFisica = empresaFisica;
	}

	public BigDecimal getPorcentagemRateio()
	{
		return porcentagemRateio;
	}

	public void setPorcentagemRateio(BigDecimal porcentagemRateio)
	{
		this.porcentagemRateio = porcentagemRateio;
	}
	
	@Override
	public PreLancamentoContaPagarRateio clone() throws CloneNotSupportedException 
	{
		return (PreLancamentoContaPagarRateio) super.clone();
	}
	
	@Override
	public void validate() throws Exception { }
}