package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import java.text.DateFormat;
import java.text.Normalizer;
import java.text.SimpleDateFormat;

import javax.persistence.Lob;

import net.sf.jasperreports.components.list.HorizontalFillList;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name="caixadia")


public class CaixaDia extends GenericModelIGN implements Cloneable
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idCaixaDia")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@Column(name="valorInicial")
	private Double valorInicial;
	
	@Column(name="situacao")
	private Integer situacao;
	
	@Column(name="dataAbertura")
	private Date dataAbertura;
	
	@Column(name="horaAbertura")
	private Date horaAbertura;
	
	@Column(name="dataFechamento")
	private Date dataFechamento;
	
	@Column(name="horaFechamento")
	private Date horaFechamento;
	
	@Column(name="observacao")
	private String observacao;
	

	public CaixaDia()
	{
		super();
	}
	

	public CaixaDia
	(
	EmpresaFisica idEmpresaFisica,
	Usuario idUsuario,
		Double valorInicial,
		Integer situacao,
		Date dataAbertura,
		Date horaAbertura,
		Date dataFechamento,
		Date horaFechamento
	) 
	{
		super();
		this.empresaFisica = idEmpresaFisica;
		this.usuario = idUsuario;
		this.valorInicial = valorInicial;
		this.situacao = situacao;
		this.dataAbertura = dataAbertura;
		this.horaAbertura = horaAbertura;
		this.dataFechamento = dataFechamento;
		this.horaFechamento = horaFechamento;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setempresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Double getValorInicial() {
		return valorInicial;
	}

	public void setValorInicial(Double valorInicial) {
	
		if (valorInicial != null && Double.isNaN(valorInicial))
		{
			this.valorInicial = null;
		}
		else
		{
			this.valorInicial = valorInicial;
		}
		
	}	
	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}	
	public Date getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}
	public Date getHoraAbertura() {	 

		return horaAbertura;
	}

	public void setHoraAbertura(Date horaAbertura) {
		
		this.horaAbertura = horaAbertura;
	}
	
	public Date getDataFechamento() {
		return dataFechamento;
	}

	public void setDataFechamento(Date dataFechamento) {
		this.dataFechamento = dataFechamento;
	}
	public Date getHoraFechamento() {
		return horaFechamento;
	}

	public void setHoraFechamento(Date horaFechamento) {
		this.horaFechamento = horaFechamento;
	}
	
	public String getObservacao() {
		return observacao;
	}


	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
	}
	
	@Override
	public CaixaDia clone() throws CloneNotSupportedException 
	{
		return (CaixaDia) super.clone();
	}
}