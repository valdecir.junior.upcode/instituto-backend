package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="itemnotapisnfe")


public class ItemNotaPisNfe extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idItemNotaPISNFE")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idItemNota")
	private ItemNotaFiscal itemNota;
	@Column(name="codigoCSTPIS")
	private Integer codigoCSTPIS;
	
	@Column(name="valorBseCalcPIS")
	private Double valorBseCalcPIS;
	
	@Column(name="aliquotaPIS")
	private Double aliquotaPIS;
	
	@Column(name="quatVendPIS")
	private Double quatVendPIS;
	
	@Column(name="aliquotaPISValor")
	private Double aliquotaPISValor;
	
	@Column(name="valorPIS")
	private Double valorPIS;
	
	@Column(name="valorBaseCalcPISST")
	private Double valorBaseCalcPISST;
	
	@Column(name="aliquotaPISST")
	private Double aliquotaPISST;
	
	@Column(name="quantVendPISST")
	private Double quantVendPISST;
	
	@Column(name="aliquotaPISValorST")
	private Double aliquotaPISValorST;
	
	@Column(name="valorPISST")
	private Double valorPISST;
	

	public ItemNotaPisNfe()
	{
		super();
	}
	

	public ItemNotaPisNfe
	(
	ItemNotaFiscal itemNota,
		Integer codigoCSTPIS,
		Double valorBseCalcPIS,
		Double aliquotaPIS,
		Double quatVendPIS,
		Double aliquotaPISValor,
		Double valorPIS,
		Double valorBaseCalcPISST,
		Double aliquotaPISST,
		Double quantVendPISST,
		Double aliquotaPISValorST,
		Double valorPISST
	) 
	{
		super();
		this.itemNota = itemNota;
		this.codigoCSTPIS = codigoCSTPIS;
		this.valorBseCalcPIS = valorBseCalcPIS;
		this.aliquotaPIS = aliquotaPIS;
		this.quatVendPIS = quatVendPIS;
		this.aliquotaPISValor = aliquotaPISValor;
		this.valorPIS = valorPIS;
		this.valorBaseCalcPISST = valorBaseCalcPISST;
		this.aliquotaPISST = aliquotaPISST;
		this.quantVendPISST = quantVendPISST;
		this.aliquotaPISValorST = aliquotaPISValorST;
		this.valorPISST = valorPISST;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public ItemNotaFiscal getItemNota() {
		return itemNota;
	}

	public void setItemNota(ItemNotaFiscal itemNota) {
		this.itemNota = itemNota;
	}
	
	public Integer getCodigoCSTPIS() {
		return codigoCSTPIS;
	}

	public void setCodigoCSTPIS(Integer codigoCSTPIS) {
		this.codigoCSTPIS = codigoCSTPIS;
	}	
	public Double getValorBseCalcPIS() {
		return valorBseCalcPIS;
	}

	public void setValorBseCalcPIS(Double valorBseCalcPIS) {
	
		if (valorBseCalcPIS != null && Double.isNaN(valorBseCalcPIS))
		{
			this.valorBseCalcPIS = null;
		}
		else
		{
			this.valorBseCalcPIS = valorBseCalcPIS;
		}
		
	}	
	public Double getAliquotaPIS() {
		return aliquotaPIS;
	}

	public void setAliquotaPIS(Double aliquotaPIS) {
	
		if (aliquotaPIS != null && Double.isNaN(aliquotaPIS))
		{
			this.aliquotaPIS = null;
		}
		else
		{
			this.aliquotaPIS = aliquotaPIS;
		}
		
	}	
	public Double getQuatVendPIS() {
		return quatVendPIS;
	}

	public void setQuatVendPIS(Double quatVendPIS) {
	
		if (quatVendPIS != null && Double.isNaN(quatVendPIS))
		{
			this.quatVendPIS = null;
		}
		else
		{
			this.quatVendPIS = quatVendPIS;
		}
		
	}	
	public Double getAliquotaPISValor() {
		return aliquotaPISValor;
	}

	public void setAliquotaPISValor(Double aliquotaPISValor) {
	
		if (aliquotaPISValor != null && Double.isNaN(aliquotaPISValor))
		{
			this.aliquotaPISValor = null;
		}
		else
		{
			this.aliquotaPISValor = aliquotaPISValor;
		}
		
	}	
	public Double getValorPIS() {
		return valorPIS;
	}

	public void setValorPIS(Double valorPIS) {
	
		if (valorPIS != null && Double.isNaN(valorPIS))
		{
			this.valorPIS = null;
		}
		else
		{
			this.valorPIS = valorPIS;
		}
		
	}	
	public Double getValorBaseCalcPISST() {
		return valorBaseCalcPISST;
	}

	public void setValorBaseCalcPISST(Double valorBaseCalcPISST) {
	
		if (valorBaseCalcPISST != null && Double.isNaN(valorBaseCalcPISST))
		{
			this.valorBaseCalcPISST = null;
		}
		else
		{
			this.valorBaseCalcPISST = valorBaseCalcPISST;
		}
		
	}	
	public Double getAliquotaPISST() {
		return aliquotaPISST;
	}

	public void setAliquotaPISST(Double aliquotaPISST) {
	
		if (aliquotaPISST != null && Double.isNaN(aliquotaPISST))
		{
			this.aliquotaPISST = null;
		}
		else
		{
			this.aliquotaPISST = aliquotaPISST;
		}
		
	}	
	public Double getQuantVendPISST() {
		return quantVendPISST;
	}

	public void setQuantVendPISST(Double quantVendPISST) {
	
		if (quantVendPISST != null && Double.isNaN(quantVendPISST))
		{
			this.quantVendPISST = null;
		}
		else
		{
			this.quantVendPISST = quantVendPISST;
		}
		
	}	
	public Double getAliquotaPISValorST() {
		return aliquotaPISValorST;
	}

	public void setAliquotaPISValorST(Double aliquotaPISValorST) {
	
		if (aliquotaPISValorST != null && Double.isNaN(aliquotaPISValorST))
		{
			this.aliquotaPISValorST = null;
		}
		else
		{
			this.aliquotaPISValorST = aliquotaPISValorST;
		}
		
	}	
	public Double getValorPISST() {
		return valorPISST;
	}

	public void setValorPISST(Double valorPISST) {
	
		if (valorPISST != null && Double.isNaN(valorPISST))
		{
			this.valorPISST = null;
		}
		else
		{
			this.valorPISST = valorPISST;
		}
		
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
