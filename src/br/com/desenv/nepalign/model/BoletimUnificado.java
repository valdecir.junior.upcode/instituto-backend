package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="boletimunificado")


public class BoletimUnificado extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idBoletimUnificado")
	private Integer id;
		
	@Column(name="origem")
	private String origem;
	
	@Column(name="dataMovimento")
	private Date dataMovimento;
	
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="valorDebito")
	private Double valorDebito;
	
	@Column(name="valorCredito")
	private Double valorCredito;
	
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	

	public BoletimUnificado()
	{
		super();
	}
	

	public BoletimUnificado
	(
		String origem,
		Date dataMovimento,
		String descricao,
		Double valorDebito,
		Double valorCredito,
		EmpresaFisica empresaFisica
	) 
	{
		super();
		this.origem = origem;
		this.dataMovimento = dataMovimento;
		this.descricao = descricao;
		this.valorDebito = valorDebito;
		this.valorCredito = valorCredito;
		this.empresaFisica = empresaFisica;
		
	}

	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}


	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getOrigem() {
		String retorno = null;
		
		if (origem != null)
			retorno = origem.toUpperCase().trim();
		return retorno;
	}
	
	public void setOrigem(String origem) {
		if (origem != null)
		{
			this.origem = origem.toUpperCase().trim();
		}
		else
			this.origem = null;
			
		
	}
		
	public Date getDataMovimento() {
		return dataMovimento;
	}

	public void setDataMovimento(Date dataMovimento) {
		this.dataMovimento = dataMovimento;
	}
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public Double getValorDebito() {
		return valorDebito;
	}

	public void setValorDebito(Double valorDebito) {
	
		if (valorDebito != null && Double.isNaN(valorDebito))
		{
			this.valorDebito = null;
		}
		else
		{
			this.valorDebito = valorDebito;
		}
		
	}	
	public Double getValorCredito() {
		return valorCredito;
	}

	public void setValorCredito(Double valorCredito) {
	
		if (valorCredito != null && Double.isNaN(valorCredito))
		{
			this.valorCredito = null;
		}
		else
		{
			this.valorCredito = valorCredito;
		}
		
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
