package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="itemnotacofinsnfe")


public class ItemNotaCofinsNfe extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idItemNotaCOFINSNFE")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idItemNotaFiscal")
	private ItemNotaFiscal itemNotaFiscal;
	@Column(name="codigoCSTCOFINS")
	private Integer codigoCSTCOFINS;
	
	@Column(name="valorBaseCalcCOFINS")
	private Double valorBaseCalcCOFINS;
	
	@Column(name="aliquotaCOFINS")
	private Double aliquotaCOFINS;
	
	@Column(name="quantVendida")
	private Double quantVendida;
	
	@Column(name="aliquotaCOFINSValor")
	private Double aliquotaCOFINSValor;
	
	@Column(name="valorCOFINS")
	private Double valorCOFINS;
	
	@Column(name="valorBaseCalcCOFINSST")
	private Double valorBaseCalcCOFINSST;
	
	@Column(name="aliquotaCOFINSST")
	private Double aliquotaCOFINSST;
	
	@Column(name="quanVendidaCOFINSST")
	private Double quanVendidaCOFINSST;
	
	@Column(name="aliquotaCONFINSValorST")
	private Double aliquotaCONFINSValorST;
	
	@Column(name="valorCOFINSST")
	private Double valorCOFINSST;
	

	public ItemNotaCofinsNfe()
	{
		super();
	}
	

	public ItemNotaCofinsNfe
	(
	ItemNotaFiscal itemNotaFiscal,
		Integer codigoCSTCOFINS,
		Double valorBaseCalcCOFINS,
		Double aliquotaCOFINS,
		Double quantVendida,
		Double aliquotaCOFINSValor,
		Double valorCOFINS,
		Double valorBaseCalcCOFINSST,
		Double aliquotaCOFINSST,
		Double quanVendidaCOFINSST,
		Double aliquotaCONFINSValorST,
		Double valorCOFINSST
	) 
	{
		super();
		this.itemNotaFiscal = itemNotaFiscal;
		this.codigoCSTCOFINS = codigoCSTCOFINS;
		this.valorBaseCalcCOFINS = valorBaseCalcCOFINS;
		this.aliquotaCOFINS = aliquotaCOFINS;
		this.quantVendida = quantVendida;
		this.aliquotaCOFINSValor = aliquotaCOFINSValor;
		this.valorCOFINS = valorCOFINS;
		this.valorBaseCalcCOFINSST = valorBaseCalcCOFINSST;
		this.aliquotaCOFINSST = aliquotaCOFINSST;
		this.quanVendidaCOFINSST = quanVendidaCOFINSST;
		this.aliquotaCONFINSValorST = aliquotaCONFINSValorST;
		this.valorCOFINSST = valorCOFINSST;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public ItemNotaFiscal getItemNotaFiscal() {
		return itemNotaFiscal;
	}

	public void setItemNotaFiscal(ItemNotaFiscal itemNotaFiscal) {
		this.itemNotaFiscal = itemNotaFiscal;
	}
	
	public Integer getCodigoCSTCOFINS() {
		return codigoCSTCOFINS;
	}

	public void setCodigoCSTCOFINS(Integer codigoCSTCOFINS) {
		this.codigoCSTCOFINS = codigoCSTCOFINS;
	}	
	public Double getValorBaseCalcCOFINS() {
		return valorBaseCalcCOFINS;
	}

	public void setValorBaseCalcCOFINS(Double valorBaseCalcCOFINS) {
	
		if (valorBaseCalcCOFINS != null && Double.isNaN(valorBaseCalcCOFINS))
		{
			this.valorBaseCalcCOFINS = null;
		}
		else
		{
			this.valorBaseCalcCOFINS = valorBaseCalcCOFINS;
		}
		
	}	
	public Double getAliquotaCOFINS() {
		return aliquotaCOFINS;
	}

	public void setAliquotaCOFINS(Double aliquotaCOFINS) {
	
		if (aliquotaCOFINS != null && Double.isNaN(aliquotaCOFINS))
		{
			this.aliquotaCOFINS = null;
		}
		else
		{
			this.aliquotaCOFINS = aliquotaCOFINS;
		}
		
	}	
	public Double getQuantVendida() {
		return quantVendida;
	}

	public void setQuantVendida(Double quantVendida) {
	
		if (quantVendida != null && Double.isNaN(quantVendida))
		{
			this.quantVendida = null;
		}
		else
		{
			this.quantVendida = quantVendida;
		}
		
	}	
	public Double getAliquotaCOFINSValor() {
		return aliquotaCOFINSValor;
	}

	public void setAliquotaCOFINSValor(Double aliquotaCOFINSValor) {
	
		if (aliquotaCOFINSValor != null && Double.isNaN(aliquotaCOFINSValor))
		{
			this.aliquotaCOFINSValor = null;
		}
		else
		{
			this.aliquotaCOFINSValor = aliquotaCOFINSValor;
		}
		
	}	
	public Double getValorCOFINS() {
		return valorCOFINS;
	}

	public void setValorCOFINS(Double valorCOFINS) {
	
		if (valorCOFINS != null && Double.isNaN(valorCOFINS))
		{
			this.valorCOFINS = null;
		}
		else
		{
			this.valorCOFINS = valorCOFINS;
		}
		
	}	
	public Double getValorBaseCalcCOFINSST() {
		return valorBaseCalcCOFINSST;
	}

	public void setValorBaseCalcCOFINSST(Double valorBaseCalcCOFINSST) {
	
		if (valorBaseCalcCOFINSST != null && Double.isNaN(valorBaseCalcCOFINSST))
		{
			this.valorBaseCalcCOFINSST = null;
		}
		else
		{
			this.valorBaseCalcCOFINSST = valorBaseCalcCOFINSST;
		}
		
	}	
	public Double getAliquotaCOFINSST() {
		return aliquotaCOFINSST;
	}

	public void setAliquotaCOFINSST(Double aliquotaCOFINSST) {
	
		if (aliquotaCOFINSST != null && Double.isNaN(aliquotaCOFINSST))
		{
			this.aliquotaCOFINSST = null;
		}
		else
		{
			this.aliquotaCOFINSST = aliquotaCOFINSST;
		}
		
	}	
	public Double getQuanVendidaCOFINSST() {
		return quanVendidaCOFINSST;
	}

	public void setQuanVendidaCOFINSST(Double quanVendidaCOFINSST) {
	
		if (quanVendidaCOFINSST != null && Double.isNaN(quanVendidaCOFINSST))
		{
			this.quanVendidaCOFINSST = null;
		}
		else
		{
			this.quanVendidaCOFINSST = quanVendidaCOFINSST;
		}
		
	}	
	public Double getAliquotaCONFINSValorST() {
		return aliquotaCONFINSValorST;
	}

	public void setAliquotaCONFINSValorST(Double aliquotaCONFINSValorST) {
	
		if (aliquotaCONFINSValorST != null && Double.isNaN(aliquotaCONFINSValorST))
		{
			this.aliquotaCONFINSValorST = null;
		}
		else
		{
			this.aliquotaCONFINSValorST = aliquotaCONFINSValorST;
		}
		
	}	
	public Double getValorCOFINSST() {
		return valorCOFINSST;
	}

	public void setValorCOFINSST(Double valorCOFINSST) {
	
		if (valorCOFINSST != null && Double.isNaN(valorCOFINSST))
		{
			this.valorCOFINSST = null;
		}
		else
		{
			this.valorCOFINSST = valorCOFINSST;
		}
		
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
