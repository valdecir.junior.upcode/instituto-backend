package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="atendido")


public class Atendido extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idAtendido")
	private Integer id;
		
	@Column(name="nomeCompleto")
	private String nomeCompleto;
	
	@Column(name="dataNascimento")
	private Date dataNascimento;
	
	@Column(name="cpf")
	private String cpf;
	
	@Column(name="rg")
	private String rg;
	
	@Column(name="dataEmissaoRg")
	private Date dataEmissaoRg;
	
	@Column(name="orgaoEmissorRg")
	private String orgaoEmissorRg;
	
	@Column(name="ufRg")
	private String ufRg;
	
	@Column(name="logradouro")
	private String logradouro;
	
	@Column(name="numero")
	private String numero;
	
	@Column(name="complemento")
	private String complemento;
	
	@ManyToOne
	@JoinColumn(name = "idUf")
	private Uf idUf;
	@ManyToOne
	@JoinColumn(name = "idCidade")
	private Cidade cidadeo;
	@Column(name="nacionalidade")
	private String nacionalidade;
	
	@ManyToOne
	@JoinColumn(name = "idCidadeAtendido")
	private Cidade idCidadeAtendido;
	@ManyToOne
	@JoinColumn(name = "idFormaLocomocao")
	private FormaLocomocao idFormaLocomocao;
	@ManyToOne
	@JoinColumn(name = "idResidenciaAtendido")
	private ResidenciaAtendido idResidenciaAtendido;
	@Column(name="situacaoResidencia")
	private String situacaoResidencia;
	
	@Column(name="informacoesMedicas")
	private String informacoesMedicas;
	
	@Column(name="foneFixo")
	private String foneFixo;
	
	@Column(name="foneCelular")
	private String foneCelular;
	
	@Column(name="email")
	private String email;
	
	@Column(name="bairro")
	private String bairro;
	
	@Column(name="cep")
	private String cep;
	
	@Column(name="situacaoCadastral")
	private String situacaoCadasrtral;
	
	@Column(name = "foto")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] foto;
	
	@Column(name="dataVencimentoAtestado")
	private Date dataVencimentoAtestado;
	
	@Column(name="motivoUltimoAfastamento")
	private String motivoUltimoAfastamento;	
	
	@Column(name = "fotoCaminho")
	private String fotoCaminho;
		

	public String getFotoCaminho() {
		return fotoCaminho;
	}


	public void setFotoCaminho(String fotoCaminho) {
		this.fotoCaminho = fotoCaminho;
	}


	public String getMotivoUltimoAfastamento() {
		return motivoUltimoAfastamento;
	}


	public void setMotivoUltimoAfastamento(String motivoUltimoAfastamento) {
		this.motivoUltimoAfastamento = motivoUltimoAfastamento;
	}


	public Date getDataVencimentoAtestado() {
		return dataVencimentoAtestado;
	}


	public void setDataVencimentoAtestado(Date dataVencimentoAtestado) {
		this.dataVencimentoAtestado = dataVencimentoAtestado;
	}
	


	public Atendido()
	{
		super();
	}
	

	public Atendido
	(
		String nomeCompleto,
		Date dataNascimento,
		String cpf,
		String rg,
		Date dataEmissaoRg,
		String orgaoEmissorRg,
		String ufRg,
		String logradouro,
		String numero,
		String complemento,
	Uf idUf,
	Cidade cidadeo,
		String nacionalidade,
	Cidade idCidadeAtendido,
	FormaLocomocao idFormaLocomocao,
	ResidenciaAtendido idResidenciaAtendido,
		String situacaoResidencia,
		String informacoesMedicas,
		String foneFixo,
		String foneCelular,
		String email,
		String bairro,
		String cep,
		String situacaoCadasrtral
	) 
	{
		super();
		this.nomeCompleto = nomeCompleto;
		this.dataNascimento = dataNascimento;
		this.cpf = cpf;
		this.rg = rg;
		this.dataEmissaoRg = dataEmissaoRg;
		this.orgaoEmissorRg = orgaoEmissorRg;
		this.ufRg = ufRg;
		this.logradouro = logradouro;
		this.numero = numero;
		this.complemento = complemento;
		this.idUf = idUf;
		this.cidadeo = cidadeo;
		this.nacionalidade = nacionalidade;
		this.idCidadeAtendido = idCidadeAtendido;
		this.idFormaLocomocao = idFormaLocomocao;
		this.idResidenciaAtendido = idResidenciaAtendido;
		this.situacaoResidencia = situacaoResidencia;
		this.informacoesMedicas = informacoesMedicas;
		this.foneFixo = foneFixo;
		this.foneCelular = foneCelular;
		this.email = email;
		this.bairro = bairro;
		this.cep = cep;
		this.situacaoCadasrtral = situacaoCadasrtral;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public String getNomeCompleto() {
		String retorno = null;
		
		if (nomeCompleto != null)
			retorno = nomeCompleto.toUpperCase().trim();
		return retorno;
	}
	
	public void setNomeCompleto(String nomeCompleto) {
		if (nomeCompleto != null)
		{
			this.nomeCompleto = nomeCompleto.toUpperCase().trim();
		}
		else
			this.nomeCompleto = null;
			
		
	}
		
	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public String getCpf() {
		String retorno = null;
		
		if (cpf != null)
			retorno = cpf.toUpperCase().trim();
		return retorno;
	}
	
	public void setCpf(String cpf) {
		if (cpf != null)
		{
			this.cpf = cpf.toUpperCase().trim();
		}
		else
			this.cpf = null;
			
		
	}
		
	public String getRg() {
		String retorno = null;
		
		if (rg != null)
			retorno = rg.toUpperCase().trim();
		return retorno;
	}
	
	public void setRg(String rg) {
		if (rg != null)
		{
			this.rg = rg.toUpperCase().trim();
		}
		else
			this.rg = null;
			
		
	}
		
	public Date getDataEmissaoRg() {
		return dataEmissaoRg;
	}

	public void setDataEmissaoRg(Date dataEmissaoRg) {
		this.dataEmissaoRg = dataEmissaoRg;
	}
	public String getOrgaoEmissorRg() {
		String retorno = null;
		
		if (orgaoEmissorRg != null)
			retorno = orgaoEmissorRg.toUpperCase().trim();
		return retorno;
	}
	
	public void setOrgaoEmissorRg(String orgaoEmissorRg) {
		if (orgaoEmissorRg != null)
		{
			this.orgaoEmissorRg = orgaoEmissorRg.toUpperCase().trim();
		}
		else
			this.orgaoEmissorRg = null;
			
		
	}
		
	public String getUfRg() {
		String retorno = null;
		
		if (ufRg != null)
			retorno = ufRg.toUpperCase().trim();
		return retorno;
	}
	
	public void setUfRg(String ufRg) {
		if (ufRg != null)
		{
			this.ufRg = ufRg.toUpperCase().trim();
		}
		else
			this.ufRg = null;
			
		
	}
		
	public String getLogradouro() {
		String retorno = null;
		
		if (logradouro != null)
			retorno = logradouro.toUpperCase().trim();
		return retorno;
	}
	
	public void setLogradouro(String logradouro) {
		if (logradouro != null)
		{
			this.logradouro = logradouro.toUpperCase().trim();
		}
		else
			this.logradouro = null;
			
		
	}
		
	public String getNumero() {
		String retorno = null;
		
		if (numero != null)
			retorno = numero.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumero(String numero) {
		if (numero != null)
		{
			this.numero = numero.toUpperCase().trim();
		}
		else
			this.numero = null;
			
		
	}
		
	public String getComplemento() {
		String retorno = null;
		
		if (complemento != null)
			retorno = complemento.toUpperCase().trim();
		return retorno;
	}
	
	public void setComplemento(String complemento) {
		if (complemento != null)
		{
			this.complemento = complemento.toUpperCase().trim();
		}
		else
			this.complemento = null;
			
		
	}
		
	public Uf getIdUf() {
		return idUf;
	}

	public void setIdUf(Uf idUf) {
		this.idUf = idUf;
	}
	
	public Cidade getCidadeo() {
		return cidadeo;
	}

	public void setCidadeo(Cidade cidadeo) {
		this.cidadeo = cidadeo;
	}
	
	public String getNacionalidade() {
		String retorno = null;
		
		if (nacionalidade != null)
			retorno = nacionalidade.toUpperCase().trim();
		return retorno;
	}
	
	public void setNacionalidade(String nacionalidade) {
		if (nacionalidade != null)
		{
			this.nacionalidade = nacionalidade.toUpperCase().trim();
		}
		else
			this.nacionalidade = null;
			
		
	}
		
	public Cidade getIdCidadeAtendido() {
		return idCidadeAtendido;
	}

	public void setIdCidadeAtendido(Cidade idCidadeAtendido) {
		this.idCidadeAtendido = idCidadeAtendido;
	}
	
	public FormaLocomocao getIdFormaLocomocao() {
		return idFormaLocomocao;
	}

	public void setIdFormaLocomocao(FormaLocomocao idFormaLocomocao) {
		this.idFormaLocomocao = idFormaLocomocao;
	}
	
	public ResidenciaAtendido getIdResidenciaAtendido() {
		return idResidenciaAtendido;
	}

	public void setIdResidenciaAtendido(ResidenciaAtendido idResidenciaAtendido) {
		this.idResidenciaAtendido = idResidenciaAtendido;
	}
	
	public String getSituacaoResidencia() {
		String retorno = null;
		
		if (situacaoResidencia != null)
			retorno = situacaoResidencia.toUpperCase().trim();
		return retorno;
	}
	
	public void setSituacaoResidencia(String situacaoResidencia) {
		if (situacaoResidencia != null)
		{
			this.situacaoResidencia = situacaoResidencia.toUpperCase().trim();
		}
		else
			this.situacaoResidencia = null;
			
		
	}
		
	public String getInformacoesMedicas() {
		String retorno = null;
		
		if (informacoesMedicas != null)
			retorno = informacoesMedicas.toUpperCase().trim();
		return retorno;
	}
	
	public void setInformacoesMedicas(String informacoesMedicas) {
		if (informacoesMedicas != null)
		{
			this.informacoesMedicas = informacoesMedicas.toUpperCase().trim();
		}
		else
			this.informacoesMedicas = null;
			
		
	}
		
	public String getFoneFixo() {
		String retorno = null;
		
		if (foneFixo != null)
			retorno = foneFixo.toUpperCase().trim();
		return retorno;
	}
	
	public void setFoneFixo(String foneFixo) {
		if (foneFixo != null)
		{
			this.foneFixo = foneFixo.toUpperCase().trim();
		}
		else
			this.foneFixo = null;
			
		
	}
		
	public String getFoneCelular() {
		String retorno = null;
		
		if (foneCelular != null)
			retorno = foneCelular.toUpperCase().trim();
		return retorno;
	}
	
	public void setFoneCelular(String foneCelular) {
		if (foneCelular != null)
		{
			this.foneCelular = foneCelular.toUpperCase().trim();
		}
		else
			this.foneCelular = null;
			
		
	}
		
	public String getEmail() {
		String retorno = null;
		
		if (email != null)
			retorno = email.toUpperCase().trim();
		return retorno;
	}
	
	public void setEmail(String email) {
		if (email != null)
		{
			this.email = email.toUpperCase().trim();
		}
		else
			this.email = null;
			
		
	}
		
	public String getBairro() {
		String retorno = null;
		
		if (bairro != null)
			retorno = bairro.toUpperCase().trim();
		return retorno;
	}
	
	public void setBairro(String bairro) {
		if (bairro != null)
		{
			this.bairro = bairro.toUpperCase().trim();
		}
		else
			this.bairro = null;
			
		
	}
		
	public String getCep() {
		String retorno = null;
		
		if (cep != null)
			retorno = cep.toUpperCase().trim();
		return retorno;
	}
	
	public void setCep(String cep) {
		if (cep != null)
		{
			this.cep = cep.toUpperCase().trim();
		}
		else
			this.cep = null;
			
		
	}
		
	public String getSituacaoCadasrtral() {
		String retorno = null;
		
		if (situacaoCadasrtral != null)
			retorno = situacaoCadasrtral.toUpperCase().trim();
		return retorno;
	}
	
	public void setSituacaoCadasrtral(String situacaoCadasrtral) {
		if (situacaoCadasrtral != null)
		{
			this.situacaoCadasrtral = situacaoCadasrtral.toUpperCase().trim();
		}
		else
			this.situacaoCadasrtral = null;
			
		
	}
	
	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}	
	
	
	
	
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
