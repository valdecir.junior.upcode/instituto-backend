package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="perfilusuario")


public class PerfilUsuario extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idPerfilUsuario")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="supervisor")
	private String supervisor;
	

	public PerfilUsuario()
	{
		super();
	}
	

	public PerfilUsuario
	(
		String descricao,
		String supervisor
	) 
	{
		super();
		this.descricao = descricao;
		this.supervisor = supervisor;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public String getSupervisor() {
		String retorno = null;
		
		if (supervisor != null)
			retorno = supervisor.toUpperCase().trim();
		return retorno;
	}
	
	public void setSupervisor(String supervisor) {
		if (supervisor != null)
		{
			this.supervisor = supervisor.toUpperCase().trim();
		}
		else
			this.supervisor = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
