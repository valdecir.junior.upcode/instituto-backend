package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="departamento")


public class Departamento extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idDepartamento")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	
	@ManyToOne
	@JoinColumn(name = "idDepartamentoPai")
	private Departamento departamentoPai;
	
	@ManyToOne
	@JoinColumn(name = "idSituacaoEnvioLV")
	private SituacaoEnvioLV situacaoEnvioLV;
	
	@Column(name="ativo")
	private String ativo;
	
	@Column(name="codigo")
	private String codigo;
	
	@Column(name="ordemDiposicao")
	private Integer ordemDisposicao;
	
	@Column(name="descricaoCompleta")
	private String descricaoCompleta;
	
	@Column(name="idDepCache")
	private Integer idDepCache;
	
	@Column(name="idSubDepCache")
	private Integer idSubDepCache;
	

	public Departamento()
	{
		super();
	}
	

	public Departamento
	(
		String descricao,
	Departamento departamentoPai,
	SituacaoEnvioLV situacaoEnvioLV,
		String ativo,
		String codigo,
		Integer ordemDisposicao,
		String descricaoCompleta,
		Integer idDepCache,
		Integer idSubDepCache
	) 
	{
		super();
		this.descricao = descricao;
		this.departamentoPai = departamentoPai;
		this.situacaoEnvioLV = situacaoEnvioLV;
		this.ativo = ativo;
		this.codigo = codigo;
		this.ordemDisposicao = ordemDisposicao;
		this.descricaoCompleta = descricaoCompleta;
		this.idDepCache = idDepCache;
		this.idSubDepCache = idSubDepCache;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public Departamento getDepartamentoPai() {
		return departamentoPai;
	}

	public void setDepartamentoPai(Departamento departamentoPai) {
		this.departamentoPai = departamentoPai;
	}
	
	public SituacaoEnvioLV getSituacaoEnvioLV() {
		return situacaoEnvioLV;
	}

	public void setSituacaoEnvioLV(SituacaoEnvioLV situacaoEnvioLV) {
		this.situacaoEnvioLV = situacaoEnvioLV;
	}
	
	public String getAtivo() {
		String retorno = null;
		
		if (ativo != null)
			retorno = ativo.toUpperCase().trim();
		return retorno;
	}
	
	public void setAtivo(String ativo) {
		if (ativo != null)
		{
			this.ativo = ativo.toUpperCase().trim();
		}
		else
			this.ativo = null;
			
		
	}
		
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) 
	{
		this.codigo = codigo;
	}	
	
	public Integer getOrdemDisposicao() {
		return ordemDisposicao;
	}

	public void setOrdemDisposicao(Integer ordemDisposicao) {
		if (ordemDisposicao != null && ordemDisposicao == 0)
		{
			this.ordemDisposicao = null;
		}
		else
		{
			this.ordemDisposicao = ordemDisposicao;
		}
	}	
	public String getDescricaoCompleta() {
		String retorno = null;
		
		if (descricaoCompleta != null)
			retorno = descricaoCompleta.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricaoCompleta(String descricaoCompleta) {
		if (descricaoCompleta != null)
		{
			this.descricaoCompleta = descricaoCompleta.toUpperCase().trim();
		}
		else
			this.descricaoCompleta = null;
			
		
	}
		
	public Integer getIdDepCache() {
		return idDepCache;
	}

	public void setIdDepCache(Integer idDepCache) {
		if (idDepCache != null && idDepCache == 0)
		{
			this.idDepCache = null;
		}
		else
		{
			this.idDepCache = idDepCache;
		}
	}	
	public Integer getIdSubDepCache() {
		return idSubDepCache;
	}

	public void setIdSubDepCache(Integer idSubDepCache) {
		if (idSubDepCache != null && idSubDepCache == 0)
		{
			this.idSubDepCache = null;
		}
		else
		{
			this.idSubDepCache = idSubDepCache;
		}
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
