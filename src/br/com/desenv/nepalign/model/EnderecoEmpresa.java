package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="enderecoempresa")


public class EnderecoEmpresa extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idEnderecoEmpresa")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEmpresa")
	private Empresa empresa;
	@ManyToOne
	@JoinColumn(name = "idTipoLogradouro")
	private TipoLogradouro tipoLogradouro;
	@ManyToOne
	@JoinColumn(name = "idCidade")
	private Cidade cidade;
	@ManyToOne
	@JoinColumn(name = "idTipoEndereco")
	private TipoEndereco tipoEndereco;
	@Column(name="endereco")
	private String endereco;
	
	@Column(name="complemento")
	private String complemento;
	
	@Column(name="bairro")
	private String bairro;
	
	@Column(name="cep")
	private String cep;
	
	@Column(name="correspondencia")
	private Integer correspondencia;
	
	@Column(name="faturamento")
	private Integer faturamento;
	
	@Column(name="entrega")
	private Integer entrega;
	
	@Column(name="cobranca")
	private Integer cobranca;
	
	@Column(name="numero")
	private Integer numero;
	
	@ManyToOne
	@JoinColumn(name = "idPais")
	private Pais pais;

	public EnderecoEmpresa()
	{
		super();
	}
	

	public EnderecoEmpresa
	(
	Empresa empresa,
	TipoLogradouro tipoLogradouro,
	Cidade cidade,
	TipoEndereco tipoEndereco,
		String endereco,
		String complemento,
		String bairro,
		String cep,
		Integer correspondencia,
		Integer faturamento,
		Integer entrega,
		Integer cobranca,
		Integer numero,
	Pais pais
	) 
	{
		super();
		this.empresa = empresa;
		this.tipoLogradouro = tipoLogradouro;
		this.cidade = cidade;
		this.tipoEndereco = tipoEndereco;
		this.endereco = endereco;
		this.complemento = complemento;
		this.bairro = bairro;
		this.cep = cep;
		this.correspondencia = correspondencia;
		this.faturamento = faturamento;
		this.entrega = entrega;
		this.cobranca = cobranca;
		this.numero = numero;
		this.pais = pais;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public TipoLogradouro getTipoLogradouro() {
		return tipoLogradouro;
	}

	public void setTipoLogradouro(TipoLogradouro tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}
	
	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	
	public TipoEndereco getTipoEndereco() {
		return tipoEndereco;
	}

	public void setTipoEndereco(TipoEndereco tipoEndereco) {
		this.tipoEndereco = tipoEndereco;
	}
	
	public String getEndereco() {
		String retorno = null;
		
		if (endereco != null)
			retorno = endereco.toUpperCase().trim();
		return retorno;
	}
	
	public void setEndereco(String endereco) {
		if (endereco != null)
		{
			this.endereco = endereco.toUpperCase().trim();
		}
		else
			this.endereco = null;
			
		
	}
		
	public String getComplemento() {
		String retorno = null;
		
		if (complemento != null)
			retorno = complemento.toUpperCase().trim();
		return retorno;
	}
	
	public void setComplemento(String complemento) {
		if (complemento != null)
		{
			this.complemento = complemento.toUpperCase().trim();
		}
		else
			this.complemento = null;
			
		
	}
		
	public String getBairro() {
		String retorno = null;
		
		if (bairro != null)
			retorno = bairro.toUpperCase().trim();
		return retorno;
	}
	
	public void setBairro(String bairro) {
		if (bairro != null)
		{
			this.bairro = bairro.toUpperCase().trim();
		}
		else
			this.bairro = null;
			
		
	}
		
	public String getCep() {
		String retorno = null;
		
		if (cep != null)
			retorno = cep.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setCep(String cep) {
		if (cep != null)
		{
			this.cep = cep.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.cep = null;
			
		
	}
		
	public Integer getCorrespondencia() {
		return correspondencia;
	}

	public void setCorrespondencia(Integer correspondencia) {
		this.correspondencia = correspondencia;
	}	
	public Integer getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(Integer faturamento) {
		this.faturamento = faturamento;
	}	
	public Integer getEntrega() {
		return entrega;
	}

	public void setEntrega(Integer entrega) {
		this.entrega = entrega;
	}	
	public Integer getCobranca() {
		return cobranca;
	}

	public void setCobranca(Integer cobranca) {
		this.cobranca = cobranca;
	}	
	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}	
	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
