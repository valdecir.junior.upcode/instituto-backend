package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="comissaocobrador")


public class ComissaoCobrador extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idComissaoCobrador")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idDuplicataParcela")
	private DuplicataParcela duplicataParcela;
	@ManyToOne
	@JoinColumn(name = "idChequeRecebido")
	private ChequeRecebido chequeRecebido;
	@ManyToOne
	@JoinColumn(name = "idCobrador")
	private Cobrador cobrador;
	@Column(name="percentual")
	private Double percentual;
	
	@Column(name="dataPagamento")
	private Date dataPagamento;
	
	@Column(name="dataPagamentoComissao")
	private Date dataPagamentoComissao;
	
	@Column(name="valorRecebido")
	private Double valorRecebido;
	
	@Column(name="valorComissao")
	private Double valorComissao;
	

	public ComissaoCobrador()
	{
		super();
	}
	

	public ComissaoCobrador
	(
	DuplicataParcela duplicataParcela,
	ChequeRecebido chequeRecebido,
	Cobrador cobrador,
		Double percentual,
		Date dataPagamento,
		Date dataPagamentoComissao,
		Double valorRecebido,
		Double valorComissao
	) 
	{
		super();
		this.duplicataParcela = duplicataParcela;
		this.chequeRecebido = chequeRecebido;
		this.cobrador = cobrador;
		this.percentual = percentual;
		this.dataPagamento = dataPagamento;
		this.dataPagamentoComissao = dataPagamentoComissao;
		this.valorRecebido = valorRecebido;
		this.valorComissao = valorComissao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public DuplicataParcela getDuplicataParcela() {
		return duplicataParcela;
	}

	public void setDuplicataParcela(DuplicataParcela duplicataParcela) {
		this.duplicataParcela = duplicataParcela;
	}
	
	public ChequeRecebido getChequeRecebido() {
		return chequeRecebido;
	}

	public void setChequeRecebido(ChequeRecebido chequeRecebido) {
		this.chequeRecebido = chequeRecebido;
	}
	
	public Cobrador getCobrador() {
		return cobrador;
	}

	public void setCobrador(Cobrador cobrador) {
		this.cobrador = cobrador;
	}
	
	public Double getPercentual() {
		return percentual;
	}

	public void setPercentual(Double percentual) {
	
		if (percentual != null && Double.isNaN(percentual))
		{
			this.percentual = null;
		}
		else
		{
			this.percentual = percentual;
		}
		
	}	
	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}
	public Date getDataPagamentoComissao() {
		return dataPagamentoComissao;
	}

	public void setDataPagamentoComissao(Date dataPagamentoComissao) {
		this.dataPagamentoComissao = dataPagamentoComissao;
	}
	public Double getValorRecebido() {
		return valorRecebido;
	}

	public void setValorRecebido(Double valorRecebido) {
	
		if (valorRecebido != null && Double.isNaN(valorRecebido))
		{
			this.valorRecebido = null;
		}
		else
		{
			this.valorRecebido = valorRecebido;
		}
		
	}	
	public Double getValorComissao() {
		return valorComissao;
	}

	public void setValorComissao(Double valorComissao) {
	
		if (valorComissao != null && Double.isNaN(valorComissao))
		{
			this.valorComissao = null;
		}
		else
		{
			this.valorComissao = valorComissao;
		}
		
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
