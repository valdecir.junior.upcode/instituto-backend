package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="situacaoenviolv")
public class SituacaoEnvioLV extends GenericModelIGN
{
	public static final int SITUACAOENVIOLV_PENDENTE = 0x0001;
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idSituacaoEnvioLV")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="alerta")
	private String alerta;

	public SituacaoEnvioLV()
	{
		super();
	}
	
	public SituacaoEnvioLV
	(
		String descricao,
		String alerta
	) 
	{
		super();
		this.descricao = descricao;
		this.alerta = alerta;
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}	
	
	public String getDescricao()
	{
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) 
	{
		if (descricao != null)
			this.descricao = descricao.toUpperCase().trim();
		else
			this.descricao = null;
	}
		
	public String getAlerta() 
	{
		String retorno = null;
		
		if (alerta != null)
			retorno = alerta.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setAlerta(String alerta) 
	{
		if (alerta != null)
			this.alerta = alerta.toUpperCase().trim();
		else
			this.alerta = null;
	}
		
	@Override
	public void validate() throws Exception { }
}