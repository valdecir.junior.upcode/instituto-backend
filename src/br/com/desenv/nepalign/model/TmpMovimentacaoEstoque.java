package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="tmpmovimentacaoestoque")


public class TmpMovimentacaoEstoque extends GenericModelIGN 
{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="idTmpMovimentacaoEstoque")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresa;
	@ManyToOne
	@JoinColumn(name = "idEstoque")
	private Estoque estoque;
	@ManyToOne
	@JoinColumn(name = "idTipoMovimentacaoEstoque")
	private TipoMovimentacaoEstoque tipoMovimentacaoEstoque;
	@ManyToOne
	@JoinColumn(name = "idTipoDocumentoMovimentoEstoque")
	private TipoDocumentoMovimentoEstoque tipoDocumentoMovimento;
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@Column(name="entradaSaida")
	private String entradaSaida;
	
	@Column(name="numeroDocumento")
	private String numeroDocumento;
	
	@Column(name="dataMovimentacao")
	private Date dataMovimentacao;
	
	@Column(name="idDocumentoOrigem")
	private Integer idDocumentoOrigem;
	
	@Column(name="observacao")
	private String observacao;
	
	@Column(name="faturado")
	private String faturado;
	
	@Column(name="manual")
	private String manual;
	
	@Column(name="numeroVolume")
	private Integer numeroVolume;
	
	@Column(name="peso")
	private Double peso;
	
	@Column(name="valorFrete")
	private Double valorFrete;
	
	@ManyToOne
	@JoinColumn(name = "idFornecedor")
	private Fornecedor forncedor;
	@Column(name="imprimeEtiqueta")
	private String imprimeEtiqueta;
	
	@ManyToOne
	@JoinColumn(name = "idStatusMovimentacaoEstoque")
	private StatusMovimentacaoEstoque statusMovimentacaoEstoque;
	@ManyToOne
	@JoinColumn(name = "idEmpresaDestino")
	private EmpresaFisica empresaDestino;

	public TmpMovimentacaoEstoque()
	{
		super();
	}
	

	public TmpMovimentacaoEstoque
	(
	EmpresaFisica empresa,
	Estoque estoque,
	TipoMovimentacaoEstoque tipoMovimentacaoEstoque,
	TipoDocumentoMovimentoEstoque tipoDocumentoMovimento,
		Cliente cliente,
	Usuario usuario,
		String entradaSaida,
		String numeroDocumento,
		Date dataMovimentacao,
		Integer idDocumentoOrigem,
		String observacao,
		String faturado,
		String manual,
		Integer numeroVolume,
		Double peso,
		Double valorFrete,
	Fornecedor forncedor,
		String imprimeEtiqueta,
	StatusMovimentacaoEstoque statusMovimentacaoEstoque,
	EmpresaFisica empresaDestino
	) 
	{
		super();
		this.empresa = empresa;
		this.estoque = estoque;
		this.tipoMovimentacaoEstoque = tipoMovimentacaoEstoque;
		this.tipoDocumentoMovimento = tipoDocumentoMovimento;
		this.cliente = cliente;
		this.usuario = usuario;
		this.entradaSaida = entradaSaida;
		this.numeroDocumento = numeroDocumento;
		this.dataMovimentacao = dataMovimentacao;
		this.idDocumentoOrigem = idDocumentoOrigem;
		this.observacao = observacao;
		this.faturado = faturado;
		this.manual = manual;
		this.numeroVolume = numeroVolume;
		this.peso = peso;
		this.valorFrete = valorFrete;
		this.forncedor = forncedor;
		this.imprimeEtiqueta = imprimeEtiqueta;
		this.statusMovimentacaoEstoque = statusMovimentacaoEstoque;
		this.empresaDestino = empresaDestino;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public EmpresaFisica getEmpresa() {
		return empresa;
	}

	public void setEmpresa(EmpresaFisica empresa) {
		this.empresa = empresa;
	}
	
	public Estoque getEstoque() {
		return estoque;
	}

	public void setEstoque(Estoque estoque) {
		this.estoque = estoque;
	}
	
	public TipoMovimentacaoEstoque getTipoMovimentacaoEstoque() {
		return tipoMovimentacaoEstoque;
	}

	public void setTipoMovimentacaoEstoque(TipoMovimentacaoEstoque tipoMovimentacaoEstoque) {
		this.tipoMovimentacaoEstoque = tipoMovimentacaoEstoque;
	}
	
	public TipoDocumentoMovimentoEstoque getTipoDocumentoMovimento() {
		return tipoDocumentoMovimento;
	}

	public void setTipoDocumentoMovimento(TipoDocumentoMovimentoEstoque tipoDocumentoMovimento) {
		this.tipoDocumentoMovimento = tipoDocumentoMovimento;
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String getEntradaSaida() {
		String retorno = null;
		
		if (entradaSaida != null)
			retorno = entradaSaida.toUpperCase().trim();
		return retorno;
	}
	
	public void setEntradaSaida(String entradaSaida) {
		if (entradaSaida != null)
		{
			this.entradaSaida = entradaSaida.toUpperCase().trim();
		}
		else
			this.entradaSaida = null;
			
		
	}
		
	public String getNumeroDocumento() {
		String retorno = null;
		
		if (numeroDocumento != null)
			retorno = numeroDocumento.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroDocumento(String numeroDocumento) {
		if (numeroDocumento != null)
		{
			this.numeroDocumento = numeroDocumento.toUpperCase().trim();
		}
		else
			this.numeroDocumento = null;
			
		
	}
		
	public Date getDataMovimentacao() {
		return dataMovimentacao;
	}

	public void setDataMovimentacao(Date dataMovimentacao) {
		this.dataMovimentacao = dataMovimentacao;
	}
	public Integer getIdDocumentoOrigem() {
		return idDocumentoOrigem;
	}

	public void setIdDocumentoOrigem(Integer idDocumentoOrigem) {
		this.idDocumentoOrigem = idDocumentoOrigem;
	}	
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	public String getFaturado() {
		String retorno = null;
		
		if (faturado != null)
			retorno = faturado.toUpperCase().trim();
		return retorno;
	}
	
	public void setFaturado(String faturado) {
		if (faturado != null)
		{
			this.faturado = faturado.toUpperCase().trim();
		}
		else
			this.faturado = null;
			
		
	}
		
	public String getManual() {
		String retorno = null;
		
		if (manual != null)
			retorno = manual.toUpperCase().trim();
		return retorno;
	}
	
	public void setManual(String manual) {
		if (manual != null)
		{
			this.manual = manual.toUpperCase().trim();
		}
		else
			this.manual = null;
			
		
	}
		
	public Integer getNumeroVolume() {
		return numeroVolume;
	}

	public void setNumeroVolume(Integer numeroVolume) {
		this.numeroVolume = numeroVolume;
	}	
	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
	
		if (peso != null && Double.isNaN(peso))
		{
			this.peso = null;
		}
		else
		{
			this.peso = peso;
		}
		
	}	
	public Double getValorFrete() {
		return valorFrete;
	}

	public void setValorFrete(Double valorFrete) {
	
		if (valorFrete != null && Double.isNaN(valorFrete))
		{
			this.valorFrete = null;
		}
		else
		{
			this.valorFrete = valorFrete;
		}
		
	}	
	public Fornecedor getForncedor() {
		return forncedor;
	}

	public void setForncedor(Fornecedor forncedor) {
		this.forncedor = forncedor;
	}
	
	public String getImprimeEtiqueta() {
		String retorno = null;
		
		if (imprimeEtiqueta != null)
			retorno = imprimeEtiqueta.toUpperCase().trim();
		return retorno;
	}
	
	public void setImprimeEtiqueta(String imprimeEtiqueta) {
		if (imprimeEtiqueta != null)
		{
			this.imprimeEtiqueta = imprimeEtiqueta.toUpperCase().trim();
		}
		else
			this.imprimeEtiqueta = null;
			
		
	}
		
	public StatusMovimentacaoEstoque getStatusMovimentacaoEstoque() {
		return statusMovimentacaoEstoque;
	}

	public void setStatusMovimentacaoEstoque(StatusMovimentacaoEstoque statusMovimentacaoEstoque) {
		this.statusMovimentacaoEstoque = statusMovimentacaoEstoque;
	}
	
	public EmpresaFisica getEmpresaDestino() {
		return empresaDestino;
	}

	public void setEmpresaDestino(EmpresaFisica empresaDestino) {
		this.empresaDestino = empresaDestino;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
