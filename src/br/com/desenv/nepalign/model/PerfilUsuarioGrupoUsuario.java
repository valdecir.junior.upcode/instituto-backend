package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="perfilusuariogrupousuario")


public class PerfilUsuarioGrupoUsuario extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idPerfilUsuarioGrupoUsuario")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idPerfilUsuario")
	private PerfilUsuario perfilUsuario;
	@ManyToOne
	@JoinColumn(name = "idGrupo")
	private Grupo grupo;

	public PerfilUsuarioGrupoUsuario()
	{
		super();
	}
	

	public PerfilUsuarioGrupoUsuario
	(
	PerfilUsuario perfilUsuario,
	Grupo grupo
	) 
	{
		super();
		this.perfilUsuario = perfilUsuario;
		this.grupo = grupo;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public PerfilUsuario getPerfilUsuario() {
		return perfilUsuario;
	}

	public void setPerfilUsuario(PerfilUsuario perfilUsuario) {
		this.perfilUsuario = perfilUsuario;
	}
	
	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
