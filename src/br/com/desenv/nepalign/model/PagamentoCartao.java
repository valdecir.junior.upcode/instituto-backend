package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="pagamentocartao")


public class PagamentoCartao extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idPagamentoCartao")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@Column(name="dataEmissao")
	private Date dataEmissao;

	@Column(name="emitente")
	private String emitente;

	@ManyToOne
	@JoinColumn(name = "idFormaPagamento")
	private FormaPagamento formaPagamento;
	@Column(name="valorOriginal")
	private Double valorOriginal;
	@Column(name="valorTaxado")
	private Double valorTaxado;

	@Column(name="numeroParcelas")
	private Integer numeroParcelas;

	@Column(name="valorParcela")
	private Double valorParcela;

	@ManyToOne
	@JoinColumn(name = "idTaxaPagamento")
	private TaxaPagamento taxaPagamento;
	@ManyToOne
	@JoinColumn(name = "idCaixaDia")
	private CaixaDia caixaDia;
	@Column(name="alterado")
	private Boolean alterado;

	@ManyToOne
	@JoinColumn(name = "idOperadoraCartao")
	private OperadoraCartao operadoraCartao;

	public PagamentoCartao()
	{
		super();
	}


	public PagamentoCartao
	(
			EmpresaFisica empresaFisica,
			Date dataEmissao,
			String emitente,
			FormaPagamento formaPagamento,
			Double valorOriginal,
			Integer numeroParcelas,
			Double valorParcela,
			TaxaPagamento taxaPagamento,
			CaixaDia caixaDia,
			Boolean alterado,
			Double valorTaxado,
			OperadoraCartao operadoraCartao
			) 
	{
		super();
		this.empresaFisica = empresaFisica;
		this.dataEmissao = dataEmissao;
		this.emitente = emitente;
		this.formaPagamento = formaPagamento;
		this.valorOriginal = valorOriginal;
		this.numeroParcelas = numeroParcelas;
		this.valorParcela = valorParcela;
		this.taxaPagamento = taxaPagamento;
		this.caixaDia = caixaDia;
		this.alterado = alterado;
		this.valorTaxado = valorTaxado;
		this.operadoraCartao = operadoraCartao;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}

	public Date getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	public String getEmitente() {
		String retorno = null;

		if (emitente != null)
			retorno = emitente.toUpperCase().trim();
		return retorno;
	}

	public Boolean getAlterado() {
		return alterado;
	}


	public void setAlterado(Boolean alterado) {
		this.alterado = alterado;
	}


	public void setEmitente(String emitente) {
		if (emitente != null)
		{
			this.emitente = emitente.toUpperCase().trim();
		}
		else
			this.emitente = null;


	}

	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public Double getValorOriginal() {
		return valorOriginal;
	}

	public void setValorOriginal(Double valorOriginal) {

		if (valorOriginal != null && Double.isNaN(valorOriginal))
		{
			this.valorOriginal = null;
		}
		else
		{
			this.valorOriginal = valorOriginal;
		}

	}	
	public Integer getNumeroParcelas() {
		return numeroParcelas;
	}

	public void setNumeroParcelas(Integer numeroParcelas) {
		this.numeroParcelas = numeroParcelas;
	}	
	public Double getValorParcela() {
		return valorParcela;
	}

	public void setValorParcela(Double valorParcela) {

		if (valorParcela != null && Double.isNaN(valorParcela))
		{
			this.valorParcela = null;
		}
		else
		{
			this.valorParcela = valorParcela;
		}

	}	
	public TaxaPagamento getTaxaPagamento() {
		return taxaPagamento;
	}

	public void setTaxaPagamento(TaxaPagamento taxaPagamento) {
		this.taxaPagamento = taxaPagamento;
	}

	public CaixaDia getCaixaDia() {
		return caixaDia;
	}

	public void setCaixaDia(CaixaDia caixaDia) {
		this.caixaDia = caixaDia;
	}

	public Double getValorTaxado() {
		return valorTaxado;
	}


	public void setValorTaxado(Double valorTaxado) {
		this.valorTaxado = valorTaxado;
	}


	public OperadoraCartao getOperadoraCartao() {
		return operadoraCartao;
	}


	public void setOperadoraCartao(OperadoraCartao operadoraCartao) {
		this.operadoraCartao = operadoraCartao;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		 */

	}

}
