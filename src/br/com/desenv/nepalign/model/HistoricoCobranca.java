package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="historicocobranca")
public class HistoricoCobranca extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idhistoricoCobranca")
	private Integer id;
	@Column(name="dataCobranca")
	private Date dataCobranca;
	@Column(name="observacao")
	private String observacao;
	@ManyToOne
	@JoinColumn(name = "idTipoContato")
	private TipoContato tipoContato;
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@Column(name="alerta")
	private Integer alerta;

	public HistoricoCobranca()
	{
		super();
	}
	

	public HistoricoCobranca
	(
		Date dataCobranca,
		String observacao,
		TipoContato tipoContato,
		Cliente cliente,
		Integer alerta
	) 
	{
		super();
		this.dataCobranca = dataCobranca;
		this.observacao = observacao;
		this.tipoContato = tipoContato;
		this.cliente = cliente;
		this.alerta = alerta;	
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}	
	
	public Date getDataCobranca() 
	{
		return dataCobranca;
	}

	public void setDataCobranca(Date dataCobranca) 
	{
		this.dataCobranca = dataCobranca;
	}
	
	public String getObservacao() 
	{
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setObservacao(String observacao) 
	{
		if (observacao != null)
			this.observacao = observacao.toUpperCase().trim();
		else
			this.observacao = null;
	}
		
	public TipoContato getTipoContato() 
	{
		return tipoContato;
	}

	public void setTipoContato(TipoContato tipoContato) 
	{
		this.tipoContato = tipoContato;
	}
	
	public Cliente getCliente() 
	{
		return cliente;
	}

	public void setCliente(Cliente cliente) 
	{
		this.cliente = cliente;
	}
	
	public Integer getAlerta() 
	{
		return alerta;
	}

	public void setAlerta(Integer alerta) 
	{
		this.alerta = alerta;
	}

	@Override
	public void validate() throws Exception
	{		
	}
	
	public HistoricoCobranca clone() throws CloneNotSupportedException 
	{
		return (HistoricoCobranca) super.clone();
	}
}