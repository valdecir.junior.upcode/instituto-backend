package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name = "tipoetiquetavitrine")
public class TipoEtiquetaVitrine extends GenericModelIGN {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idTipoEtiquetaVitrine")
	private Integer id;
	@Column(name = "descricao")
	private String descricao;
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@Column(name = "jasper")
	private String jasper;
	@Column(name = "precoVista")
	private Integer precoVista;
	@Column(name = "precoPrazo")
	private Integer precoPrazo;
	@Column(name = "precoXY")
	private Integer precoXY;
	@Column(name = "parcelado")
	private Integer parcelado;

	public TipoEtiquetaVitrine() {
		super();
	}

	public TipoEtiquetaVitrine(String descricao, EmpresaFisica empresaFisica, String jasper, Integer precoVista, Integer precoPrazo, Integer precoXY, Integer parcelado) {
		super();
		this.descricao = descricao;
		this.empresaFisica = empresaFisica;
		this.jasper = jasper;
		this.precoVista = precoVista;
		this.precoPrazo = precoPrazo;
		this.precoXY = precoXY;
		this.parcelado = parcelado;
	}

	public String getJasper() {
		return jasper;
	}

	public void setJasper(String jasper) {
		this.jasper = jasper;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		String retorno = null;
		if (descricao != null) {
			retorno = descricao;
		}
		return retorno;
	}

	public void setDescricao(String descricao) {
		if (descricao != null) {
			this.descricao = descricao;
		} else {
			this.descricao = null;
		}
	}

	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}

	public Integer getPrecoVista() {
		return precoVista;
	}

	public void setPrecoVista(Integer precoVista) {
		this.precoVista = precoVista;
	}

	public Integer getPrecoPrazo() {
		return precoPrazo;
	}

	public void setPrecoPrazo(Integer precoPrazo) {
		this.precoPrazo = precoPrazo;
	}

	public Integer getPrecoXY() {
		return precoXY;
	}

	public void setPrecoXY(Integer precoXY) {
		this.precoXY = precoXY;
	}

	public Integer getParcelado() {
		return parcelado;
	}

	public void setParcelado(Integer parcelado) {
		this.parcelado = parcelado;
	}

	@Override
	public void validate() throws Exception {
		// Coloque aqui suas validações ignorantes
		/*
		 * if (this.descricao == null || this.descricao.trim().equals("")) {
		 * throw new Exception("Descrição é obrigatório."); }
		 */
	}

}
