package br.com.desenv.nepalign.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="arquivoBoletoBancario")


public class ArquivoBoletoBancario extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idArquivoBoletoBancario")
	private Integer id;
		
	@Column(name="nomeArquivo")
	private String nomeArquivo;
	
	@Column(name="tipoArquivo")
	private String tipoArquivo;
	
	@Column(name="dataHoraCriacao")
	private Date dataHoraCriacao;
	
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	
	@Column(name="caminhoSalvo")
	private String caminhoSalvo;
	
	public ArquivoBoletoBancario()
	{
		super();
	}
	

	public ArquivoBoletoBancario
	(
		String nomeArquivo,
		String tipoArquivo,
		Date dataHoraCriacao,
		Usuario usuario,
		String caminhoSalvo
	) 
	{
		super();
		this.nomeArquivo = nomeArquivo;
		this.tipoArquivo = tipoArquivo;
		this.dataHoraCriacao = dataHoraCriacao;
		this.usuario = usuario;
		this.caminhoSalvo = caminhoSalvo;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getNomeArquivo() {
		String retorno = null;
		
		if (nomeArquivo != null)
			retorno = nomeArquivo.toUpperCase().trim();
		return retorno;
	}
	
	public void setNomeArquivo(String nomeArquivo) {
		if (nomeArquivo != null)
		{
			this.nomeArquivo = nomeArquivo.toUpperCase().trim();
		}
		else
			this.nomeArquivo = null;
			
		
	}
		
	public String getTipoArquivo() {
		String retorno = null;
		
		if (tipoArquivo != null)
			retorno = tipoArquivo.toUpperCase().trim();
		return retorno;
	}
	
	public void setTipoArquivo(String tipoArquivo) {
		if (tipoArquivo != null)
		{
			this.tipoArquivo = tipoArquivo.toUpperCase().trim();
		}
		else
			this.tipoArquivo = null;
			
		
	}
		
	public Date getDataHoraCriacao() {
		return dataHoraCriacao;
	}

	public void setDataHoraCriacao(Date dataHoraCriacao) {
		this.dataHoraCriacao = dataHoraCriacao;
	}
	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


	public String getCaminhoSalvo() {
		return caminhoSalvo;
	}


	public void setCaminhoSalvo(String caminhoSalvo) {
		this.caminhoSalvo = caminhoSalvo;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
