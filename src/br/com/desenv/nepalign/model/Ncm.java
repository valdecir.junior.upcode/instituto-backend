package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="ncm")

public class Ncm extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idNcm")
	private Integer id;
		
	@Column(name="ncm")
	private String ncm;
	
	@Column(name="nome")
	private String nome;
	

	public Ncm()
	{
		super();
	}
	

	public Ncm
	(
		String ncm,
		String nome
	) 
	{
		super();
		this.ncm = ncm;
		this.nome = nome;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getNcm() {
		String retorno = null;
		
		if (ncm != null)
			retorno = ncm.toUpperCase().trim();
		return retorno;
	}
	
	public void setNcm(String ncm) {
		if (ncm != null)
		{
			this.ncm = ncm.toUpperCase().trim();
		}
		else
			this.ncm = null;
			
		
	}
		
	public String getNome() {
		String retorno = null;
		
		if (nome != null)
			retorno = nome.toUpperCase().trim();
		return retorno;
	}
	
	public void setNome(String nome) {
		if (nome != null)
		{
			this.nome = nome.toUpperCase().trim();
		}
		else
			this.nome = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
