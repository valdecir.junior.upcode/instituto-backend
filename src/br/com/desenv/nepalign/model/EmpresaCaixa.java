package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="empresaCaixa")
public class EmpresaCaixa extends GenericModelIGN 
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idEmpresaCaixa")
	private Integer id;
	
	@Column(name="nomeEmpresa")
	private String nomeEmpresa;

	public EmpresaCaixa()
	{
		super();
	}

	@Override
	public Integer getId() 
	{
		return this.id;
	}

	@Override
	public void setId(Integer id) 
	{
		this.id = id;
	}

	public String getNomeEmpresa() 
	{
		return nomeEmpresa == null ? null : nomeEmpresa.toUpperCase();
	}

	public void setNomeEmpresa(String nomeEmpresa) 
	{
		this.nomeEmpresa = (nomeEmpresa == null) ? null : nomeEmpresa.toUpperCase();
	}

	@Override
	public void validate() throws Exception  { }
}