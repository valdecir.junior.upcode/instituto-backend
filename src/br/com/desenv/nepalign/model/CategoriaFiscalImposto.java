package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="categoriafiscalimposto")


public class CategoriaFiscalImposto extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idCategoriaFiscalImposto")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idCategoriaFiscal")
	private CategoriaFiscal categoriaFiscal;
	@ManyToOne
	@JoinColumn(name = "idImpostosTaxasTarifasEstado")
	private ImpostosTaxasTarifasEstado impostosTaxasTarifasEstado;
	@Column(name="valorPercentual")
	private Double valorPercentual;
	
	@Column(name="reducaoImposto")
	private Double reducaoImposto;
	
	@Column(name="observacao")
	private String observacao;
	

	public CategoriaFiscalImposto()
	{
		super();
	}
	

	public CategoriaFiscalImposto
	(
	CategoriaFiscal categoriaFiscal,
	ImpostosTaxasTarifasEstado impostosTaxasTarifasEstado,
		Double valorPercentual,
		Double reducaoImposto,
		String observacao
	) 
	{
		super();
		this.categoriaFiscal = categoriaFiscal;
		this.impostosTaxasTarifasEstado = impostosTaxasTarifasEstado;
		this.valorPercentual = valorPercentual;
		this.reducaoImposto = reducaoImposto;
		this.observacao = observacao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public CategoriaFiscal getCategoriaFiscal() {
		return categoriaFiscal;
	}

	public void setCategoriaFiscal(CategoriaFiscal categoriaFiscal) {
		this.categoriaFiscal = categoriaFiscal;
	}
	
	public ImpostosTaxasTarifasEstado getImpostosTaxasTarifasEstado() {
		return impostosTaxasTarifasEstado;
	}

	public void setImpostosTaxasTarifasEstado(ImpostosTaxasTarifasEstado impostosTaxasTarifasEstado) {
		this.impostosTaxasTarifasEstado = impostosTaxasTarifasEstado;
	}
	
	public Double getValorPercentual() {
		return valorPercentual;
	}

	public void setValorPercentual(Double valorPercentual) {
	
		if (valorPercentual != null && Double.isNaN(valorPercentual))
		{
			this.valorPercentual = null;
		}
		else
		{
			this.valorPercentual = valorPercentual;
		}
		
	}	
	public Double getReducaoImposto() {
		return reducaoImposto;
	}

	public void setReducaoImposto(Double reducaoImposto) {
	
		if (reducaoImposto != null && Double.isNaN(reducaoImposto))
		{
			this.reducaoImposto = null;
		}
		else
		{
			this.reducaoImposto = reducaoImposto;
		}
		
	}	
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
