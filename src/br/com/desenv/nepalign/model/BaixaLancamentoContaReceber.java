package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="baixalancamentocontareceber")
public class BaixaLancamentoContaReceber extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idBaixaLancamentoContaReceber")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idLancamentoContaReceber")
	private LancamentoContaReceber lancamentoContaReceber;
	
	@Column(name="valor")
	private Double valor;
	
	@Column(name="dataBaixa")
	private Date dataBaixa;
	
	@Column(name="tipoBaixa")
	private Double tipoBaixa;
	
	@Column(name="observacao")
	private String observacao;
	
	@Override
	public Integer getId() 
	{
		return this.id;
	}

	@Override
	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public LancamentoContaReceber getLancamentoContaReceber() 
	{
		return lancamentoContaReceber;
	}

	public void setLancamentoContaReceber(LancamentoContaReceber lancamentoContaReceber) 
	{
		this.lancamentoContaReceber = lancamentoContaReceber;
	}

	public Double getValor() 
	{
		return valor;
	}

	public void setValor(Double valor) 
	{
		this.valor = valor;
	}

	public Date getDataBaixa() 
	{
		return dataBaixa;
	}

	public void setDataBaixa(Date dataBaixa) 
	{
		this.dataBaixa = dataBaixa;
	}

	public Double getTipoBaixa() 
	{
		return tipoBaixa;
	}

	public void setTipoBaixa(Double tipoBaixa) 
	{
		if(Double.isNaN(tipoBaixa))
			this.tipoBaixa = null;
		else
			this.tipoBaixa = tipoBaixa;
	}

	public String getObservacao() 
	{
		return observacao;
	}

	public void setObservacao(String observacao) 
	{
		this.observacao = observacao;
	}

	public BaixaLancamentoContaReceber clone() throws CloneNotSupportedException
	{
		return (BaixaLancamentoContaReceber) super.clone();
	}
	
	@Override
	public void validate() throws Exception
	{ }
}