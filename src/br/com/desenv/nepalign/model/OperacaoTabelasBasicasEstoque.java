package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="operacaotabelasbasicasestoque")


public class OperacaoTabelasBasicasEstoque extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idOperacaoTabelasBasicasEstoque")
	private Integer id;
		
	@Column(name="dataOperacao")
	private Date dataOperacao;
	
	@Column(name="flagSincronizacao")
	private String flagSincronizacao;
	
	@Column(name="nomeClasse")
	private String nomeClasse;
	
	@Column(name = "objeto")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] objeto;
	
	@Column(name="operacao")
	private String operacao;
	
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;

	public OperacaoTabelasBasicasEstoque()
	{
		super();
	}
	

	public OperacaoTabelasBasicasEstoque
	(
		Date dataOperacao,
		String flagSincronizacao,
		String nomeClasse,
		byte[] objeto,
		String operacao,
	Usuario usuario
	) 
	{
		super();
		this.dataOperacao = dataOperacao;
		this.flagSincronizacao = flagSincronizacao;
		this.nomeClasse = nomeClasse;
		this.objeto = objeto;
		this.operacao = operacao;
		this.usuario = usuario;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Date getDataOperacao() {
		return dataOperacao;
	}

	public void setDataOperacao(Date dataOperacao) {
		this.dataOperacao = dataOperacao;
	}
	public String getFlagSincronizacao() {
		String retorno = null;
		
		if (flagSincronizacao != null)
			retorno = flagSincronizacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setFlagSincronizacao(String flagSincronizacao) {
		if (flagSincronizacao != null)
		{
			this.flagSincronizacao = flagSincronizacao.toUpperCase().trim();
		}
		else
			this.flagSincronizacao = null;
			
		
	}
		
	public String getNomeClasse() {
		String retorno = null;
		
		if (nomeClasse != null)
			retorno = nomeClasse.toUpperCase().trim();
		return retorno;
	}
	
	public void setNomeClasse(String nomeClasse) {
		if (nomeClasse != null)
		{
			this.nomeClasse = nomeClasse.toUpperCase().trim();
		}
		else
			this.nomeClasse = null;
			
		
	}
		
	public byte[] getObjeto() {
		return objeto;
	}

	public void setObjeto(byte[] objeto) {
		this.objeto = objeto;
	}
	
	public String getOperacao() {
		String retorno = null;
		
		if (operacao != null)
			retorno = operacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setOperacao(String operacao) {
		if (operacao != null)
		{
			this.operacao = operacao.toUpperCase().trim();
		}
		else
			this.operacao = null;
			
		
	}
		
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
