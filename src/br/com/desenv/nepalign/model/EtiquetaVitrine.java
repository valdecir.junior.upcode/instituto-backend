package br.com.desenv.nepalign.model;

public class EtiquetaVitrine {
	public String codigoBarras;
	public Integer idMarca;
	public String descricaoMarca;
	public String nomeProduto;
	public Integer numeroParcelas;
	public String porcentagemVista;
	public Double precoParcela;
	public Double precoPrazo;
	public Double precoVista;
	public Integer quantidade;
	public String tamanho;

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public String getDescricaoMarca() {
		return descricaoMarca;
	}

	public void setDescricaoMarca(String descricaoMarca) {
		this.descricaoMarca = descricaoMarca;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	public Double getPrecoVista() {
		return precoVista;
	}

	public void setPrecoVista(Double precoVista) {
		this.precoVista = precoVista;
	}

	public Double getPrecoPrazo() {
		return precoPrazo;
	}

	public void setPrecoPrazo(Double precoPrazo) {
		this.precoPrazo = precoPrazo;
	}

	public Double getPrecoParcela() {
		return precoParcela;
	}

	public void setPrecoParcela(Double precoParcela) {
		this.precoParcela = precoParcela;
	}

	public String getPorcentagemVista() {
		return porcentagemVista;
	}

	public void setPorcentagemVista(String porcentagemVista) {
		this.porcentagemVista = porcentagemVista;
	}

	public Integer getNumeroParcelas() {
		return numeroParcelas;
	}

	public void setNumeroParcelas(Integer numeroParcelas) {
		this.numeroParcelas = numeroParcelas;
	}

	public String getCodigoBarras() {
		return codigoBarras;
	}

	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	public String getTamanho() {
		return tamanho;
	}

	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}

	public Integer getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(Integer idMarca) {
		this.idMarca = idMarca;
	}

}
