package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="prelancamentocontareceber")
public class PreLancamentoContaReceber extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;
	
	public final static String PENDENTE = "P";
	public final static String EFETUADO = "E";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idPreLancamentoContaReceber")
	private Integer id;
	@ManyToOne
	@JoinColumn(name = "idEmpresaFinanceiro")
	private EmpresaFinanceiro empresaFinanceiro;
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisicaOrigem")
	private EmpresaFisica empresaFisicaOrigem;
	@ManyToOne
	@JoinColumn(name = "idContaContabil")
	private ContaContabil contaContabil;
	@ManyToOne
	@JoinColumn(name="idFormaPagamentoCartao")
	private FormaPagamentoCartao formaPagamentoCartao;
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@ManyToOne
	@JoinColumn(name="idBanco")
	private Banco banco;
	@ManyToOne
	@JoinColumn(name = "idLancamentoContaReceber")
	private LancamentoContaReceber lancamentoContaReceber;
	@Column(name="emitente")
	private String emitente;
	@Column(name="cpfCnpjEmitente")
	private String cpfCnpjEmitente;
	@Column(name="idDocumentoOrigem")
	private Integer idDocumentoOrigem;
	@Column(name="dataLancamento")
	private Date dataLancamento;
	@Column(name="dataVencimento")
	private Date dataVencimento;
	@Column(name="numeroDocumento")
	private String numeroDocumento;
	@Column(name="valor")
	private Double valor;
	@Column(name="valorParcelaSemDesconto")
	private Double valorParcelaSemDesconto;
	@Column(name="valorTotal")
	private Double valorTotal;
	@Column(name="numeroParcela")
	private Integer numeroParcela;
	@Column(name="quantidadeParcelas")
	private Integer quantidadeParcelas;
	@Column(name="situacao")
	private String situacao;
	@Column(name="observacao")
	private String observacao;
	
	public PreLancamentoContaReceber()
	{
		super();
	}
	
	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public EmpresaFinanceiro getEmpresaFinanceiro() 
	{
		return empresaFinanceiro;
	}
	
	public void setEmpresaFinanceiro(EmpresaFinanceiro empresaFinanceiro) {
		this.empresaFinanceiro = empresaFinanceiro;
	}

	public FormaPagamentoCartao getFormaPagamentoCartao() {
		return formaPagamentoCartao;
	}

	public void setFormaPagamentoCartao(FormaPagamentoCartao formaPagamentoCartao) {
		this.formaPagamentoCartao = formaPagamentoCartao;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public String getEmitente() {
		return emitente;
	}

	public void setEmitente(String emitente) {
		this.emitente = emitente;
	}

	public String getCpfCnpjEmitente() {
		return cpfCnpjEmitente;
	}

	public void setCpfCnpjEmitente(String cpfCnpjEmitente) {
		this.cpfCnpjEmitente = cpfCnpjEmitente;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Double getValorParcelaSemDesconto() {
		return valorParcelaSemDesconto;
	}

	public void setValorParcelaSemDesconto(Double valorParcelaSemDesconto) {
		this.valorParcelaSemDesconto = valorParcelaSemDesconto;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Integer getNumeroParcela() {
		return numeroParcela;
	}

	public void setNumeroParcela(Integer numeroParcela) {
		this.numeroParcela = numeroParcela;
	}

	public Integer getQuantidadeParcelas() {
		return quantidadeParcelas;
	}

	public void setQuantidadeParcelas(Integer quantidadeParcelas) {
		this.quantidadeParcelas = quantidadeParcelas;
	}
	
	public EmpresaFisica getEmpresaFisicaOrigem() 
	{
		return empresaFisicaOrigem;
	}

	public void setEmpresaFisicaOrigem(EmpresaFisica empresaFisicaOrigem) 
	{
		this.empresaFisicaOrigem = empresaFisicaOrigem;
	}
	
	public ContaContabil getContaContabil() 
	{
		return contaContabil;
	}

	public void setContaContabil(ContaContabil contaContabil) 
	{
		this.contaContabil = contaContabil;
	}
	
	public Usuario getUsuario() 
	{
		return usuario;
	}

	public void setUsuario(Usuario usuario) 
	{
		this.usuario = usuario;
	}

	public LancamentoContaReceber getLancamentoContaReceber() 
	{
		return lancamentoContaReceber;
	}

	public void setLancamentoContaReceber(LancamentoContaReceber lancamentoContaReceber) 
	{
		this.lancamentoContaReceber = lancamentoContaReceber;
	}

	public Integer getIdDocumentoOrigem() 
	{
		return idDocumentoOrigem;
	}

	public void setIdDocumentoOrigem(Integer idDocumentoOrigem) 
	{
		this.idDocumentoOrigem = idDocumentoOrigem;
	}

	public Date getDataLancamento() 
	{
		return dataLancamento;
	}

	public void setDataLancamento(Date dataLancamento) 
	{
		this.dataLancamento = dataLancamento;
	}

	public String getNumeroDocumento() 
	{
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) 
	{
		this.numeroDocumento = numeroDocumento;
	}

	public Double getValor() 
	{
		return valor;
	}

	public void setValor(Double valor) 
	{
		if (valor != null && Double.isNaN(valor))
			this.valor = null;
		else
			this.valor = valor;
	}	
		
	public String getSituacao() 
	{
		return situacao;
	}

	public void setSituacao(String situacao) 
	{
		this.situacao = situacao;
	}

	public String getObservacao() 
	{
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setObservacao(String observacao) 
	{
		if (observacao != null)
			this.observacao = observacao.toUpperCase().trim();
		else
			this.observacao = null;
	}
	
	@Override
	public void validate() throws Exception
	{
		if(numeroDocumento == null || numeroDocumento.trim().equals(""))
			numeroDocumento = "SEM NUMERO DOCUMENTO";
	}
}