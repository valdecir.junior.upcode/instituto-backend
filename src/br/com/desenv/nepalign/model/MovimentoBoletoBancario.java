package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="movimentoBoletoBancario")


public class MovimentoBoletoBancario extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idMovimentoBoletoBancario")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idBoletoBancario")
	private BoletoBancario boletoBancario;
	@ManyToOne
	@JoinColumn(name = "idOcorrenciaBoletoBancario")
	private OcorrenciaBoletoBancario ocorrenciaBoleto;
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@Column(name="nossoNumero")
	private Integer nossoNumero;
	
	@Column(name="valorTitulo")
	private Double valorTitulo;
	
	@Column(name="data")
	private Date data;
	
	@Column(name="dataImportado")
	private Date dataImportado;
	
	@Column(name="tipoBoleto")
	private String tipoBoleto;
	
	@Column(name="nomeArquivo")
	private String nomeArquivo;
	
	@Column(name="valorPago")
	private Double valorPago;

	public MovimentoBoletoBancario()
	{
		super();
	}
	

	public MovimentoBoletoBancario
	(
	BoletoBancario boletoBancario,
	OcorrenciaBoletoBancario ocorrenciaBoleto,
	Usuario usuario,
		Integer nossoNumero,
		Double valorTitulo,
		Date data,
		String tipoBoleto,
		String nomeArquivo,
		Double valorPago,
		Date dataImportado
	) 
	{
		super();
		this.boletoBancario = boletoBancario;
		this.ocorrenciaBoleto = ocorrenciaBoleto;
		this.usuario = usuario;
		this.nossoNumero = nossoNumero;
		this.valorTitulo = valorTitulo;
		this.data = data;
		this.tipoBoleto = tipoBoleto;
		this.nomeArquivo = nomeArquivo;
		this.valorPago = valorPago;
		this.dataImportado = dataImportado;
		
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public BoletoBancario getBoletoBancario() {
		return boletoBancario;
	}

	public void setBoletoBancario(BoletoBancario boletoBancario) {
		this.boletoBancario = boletoBancario;
	}
	
	public OcorrenciaBoletoBancario getOcorrenciaBoleto() {
		return ocorrenciaBoleto;
	}

	public void setOcorrenciaBoleto(OcorrenciaBoletoBancario ocorrenciaBoleto) {
		this.ocorrenciaBoleto = ocorrenciaBoleto;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Integer getNossoNumero() {
		return nossoNumero;
	}

	public void setNossoNumero(Integer nossoNumero) {
		this.nossoNumero = nossoNumero;
	}	
	public Double getValorTitulo() {
		return valorTitulo;
	}

	public void setValorTitulo(Double valorTitulo) {
	
		if (valorTitulo != null && Double.isNaN(valorTitulo))
		{
			this.valorTitulo = null;
		}
		else
		{
			this.valorTitulo = valorTitulo;
		}
		
	}	
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	public String getTipoBoleto() {
		String retorno = null;
		
		if (tipoBoleto != null)
			retorno = tipoBoleto.toUpperCase().trim();
		return retorno;
	}
	
	public void setTipoBoleto(String tipoBoleto) {
		if (tipoBoleto != null)
		{
			this.tipoBoleto = tipoBoleto.toUpperCase().trim();
		}
		else
			this.tipoBoleto = null;
			
		
	}
		
	public String getNomeArquivo() {
		String retorno = null;
		
		if (nomeArquivo != null)
			retorno = nomeArquivo.toUpperCase().trim();
		return retorno;
	}
	
	public void setNomeArquivo(String nomeArquivo) {
		if (nomeArquivo != null)
		{
			this.nomeArquivo = nomeArquivo.toUpperCase().trim();
		}
		else
			this.nomeArquivo = null;
			
		
	}
		
	public Double getValorPago() {
		return valorPago;
	}


	public void setValorPago(Double valorPago) {
		this.valorPago = valorPago;
	}


	public Date getDataImportado() {
		return dataImportado;
	}
	
	
	public void setDataImportado(Date dataImportado) {
		this.dataImportado = dataImportado;
	}
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
