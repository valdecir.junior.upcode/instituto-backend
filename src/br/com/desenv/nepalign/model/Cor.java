package br.com.desenv.nepalign.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="cor")


public class Cor extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idCor")
	private Integer id;
		
	@Column(name="descricaoCorEmpresa")
	private String descricaoCorEmpresa;
	
	@Column(name="descricaoCorFabrica")
	private String descricaoCorFabrica;
	
	@Column(name="ultimaAlteracao")
	private Date ultimaAlteracao;
	
	@Column(name="codigo")
	private Integer codigo;
	
	@ManyToOne
	@JoinColumn(name = "idCorPrimaria")
	private CorBasica corBasica1;
	
	@ManyToOne
	@JoinColumn(name = "idCorSecundaria")
	private CorBasica corBasica2;
	
	@ManyToOne
	@JoinColumn(name = "idCorTerciaria")
	private CorBasica corBasica3;
	

	public Cor()
	{
		super();
	}
	

	public Cor(	String descricaoCorEmpresa,	String descricaoCorFabrica,	Date ultimaAlteracao,	Integer codigo) 
	{
		super();
		this.descricaoCorEmpresa = descricaoCorEmpresa;
		this.descricaoCorFabrica = descricaoCorFabrica;
		this.ultimaAlteracao = ultimaAlteracao;
		this.codigo = codigo;
		this.corBasica1 = corBasica1;
		this.corBasica2 = corBasica2;
		this.corBasica3 = corBasica3;
		
	}

	


	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	/*
	public void setId(Integer id) 
	{	
		
		this.id = id;
	}	*/
	
	
	public String getDescricaoCorEmpresa() {
		String retorno = null;
		
		if (descricaoCorEmpresa != null)
			retorno = descricaoCorEmpresa.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricaoCorEmpresa(String descricaoCorEmpresa) {
		if (descricaoCorEmpresa != null)
		{
			this.descricaoCorEmpresa = descricaoCorEmpresa.toUpperCase().trim();
		}
		else
			this.descricaoCorEmpresa = null;
			
		
	}
		
	public String getDescricaoCorFabrica() {
		String retorno = null;
		
		if (descricaoCorFabrica != null)
			retorno = descricaoCorFabrica.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricaoCorFabrica(String descricaoCorFabrica) {
		if (descricaoCorFabrica != null)
		{
			this.descricaoCorFabrica = descricaoCorFabrica.toUpperCase().trim();
		}
		else
			this.descricaoCorFabrica = null;
			
		
	}
		
	public Date getUltimaAlteracao() {
		return ultimaAlteracao;
	}

	public void setUltimaAlteracao(Date ultimaAlteracao) {
		this.ultimaAlteracao = ultimaAlteracao;
	}
	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
		
	}	
	
	public CorBasica getCorBasica1() {
		return corBasica1;
	}


	public void setCorBasica1(CorBasica corBasica1) {
		this.corBasica1 = corBasica1;
	}


	public CorBasica getCorBasica2() {
		return corBasica2;
	}


	public void setCorBasica2(CorBasica corBasica2) {
		this.corBasica2 = corBasica2;
	}


	public CorBasica getCorBasica3() {
		return corBasica3;
	}


	public void setCorBasica3(CorBasica corBasica3) {
		this.corBasica3 = corBasica3;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
