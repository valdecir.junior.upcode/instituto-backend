package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="operacao")


public class Operacao extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idOperacao")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "IdUsuario")
	private Usuario usuario;
	
	@Column(name="nomeClasse")
	private String nomeClasse;
	
	@Column(name="dataOperacao")
	private Date dataOperacao;
	
	@Column(name = "objeto")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] objeto;
	
	
	@Column(name="operacao")
	private String operacao;
	

	public Operacao()
	{
		super();
	}
	

	public Operacao
	(
	Usuario usuario,
		String nomeClasse,
		Date dataOperacao,
		byte[] objeto,
		String operacao
	) 
	{
		super();
		this.usuario = usuario;
		this.nomeClasse = nomeClasse;
		this.dataOperacao = dataOperacao;
		this.objeto = objeto;
		this.operacao = operacao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String getNomeClasse() {
		return this.nomeClasse;
	}
	
	public void setNomeClasse(String nomeClasse) {

			this.nomeClasse = nomeClasse;
			}
		
	public Date getDataOperacao() {
		return dataOperacao;
	}

	public void setDataOperacao(Date dataOperacao) {
		this.dataOperacao = dataOperacao;
	}
	public byte[] getObjeto() {
		return objeto;
	}

	public void setObjeto(byte[] objeto) {
		this.objeto = objeto;
	}	
	public String getOperacao() {
		String retorno = null;
		
		if (operacao != null)
			retorno = operacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setOperacao(String operacao) {
		if (operacao != null)
		{
			this.operacao = operacao.toUpperCase().trim();
		}
		else
			this.operacao = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
