package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="itemmovimentacaoestoque")


public class ItemMovimentacaoEstoque extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "mygenerator", strategy = "br.com.desenv.frameworkignorante.MyGenerator")
	@GeneratedValue(generator = "mygenerator")
	@Column(name="idItemMovimentacaoEstoque")
	private Integer id;
	@ManyToOne
	@JoinColumn(name = "idEstoqueProduto")
	private EstoqueProduto estoqueProduto;
	@ManyToOne
	@JoinColumn(name = "idMovimentacaoEstoque")
	private MovimentacaoEstoque movimentacaoEstoque;
	@ManyToOne
	@JoinColumn(name = "idUnidadeMedida")
	private UnidadeMedida unidadeMedida;
	@ManyToOne
	@JoinColumn(name = "idProduto")
	private Produto produto;
	@Column(name="idDocumentoOrigem")
	private String documentoOrigem;	
	@Column(name="sequencialItem")
	private Integer sequencialItem;
	@Column(name="quantidade")
	private Double quantidade;
	@Column(name="valor")
	private Double valor;
	@Column(name="venda")
	private Double venda;
	@Column(name="custo")
	private Double custo;
	@Column(name="numeroLote")
	private String numeroLote;
	@Column(name="dataValidade")
	private Date dataValidade;
	@Column(name="dataFabricacao")
	private Date dataFabricacao;
	

	public ItemMovimentacaoEstoque()
	{
		super();
	}
	

	public ItemMovimentacaoEstoque
	(
	EstoqueProduto estoqueProduto,
	MovimentacaoEstoque movimentacaoEstoque,
	UnidadeMedida unidadeMedida,
	Produto produto,
		String documentoOrigem,
		Integer sequencialItem,
		Double quantidade,
		Double valor,
		Double custo,
		String numeroLote,
		Date dataValidade,
		Date dataFabricacao
	) 
	{
		super();
		this.estoqueProduto = estoqueProduto;
		this.movimentacaoEstoque = movimentacaoEstoque;
		this.unidadeMedida = unidadeMedida;
		this.produto = produto;
		this.documentoOrigem = documentoOrigem;
		this.sequencialItem = sequencialItem;
		this.quantidade = quantidade;
		this.valor = valor;
		this.custo = custo;
		this.numeroLote = numeroLote;
		this.dataValidade = dataValidade;
		this.dataFabricacao = dataFabricacao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public EstoqueProduto getEstoqueProduto() {
		return estoqueProduto;
	}

	public void setEstoqueProduto(EstoqueProduto estoqueProduto) {
		this.estoqueProduto = estoqueProduto;
	}
	
	public MovimentacaoEstoque getMovimentacaoEstoque() {
		return movimentacaoEstoque;
	}

	public void setMovimentacaoEstoque(MovimentacaoEstoque movimentacaoEstoque) {
		this.movimentacaoEstoque = movimentacaoEstoque;
	}
	
	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	public String getDocumentoOrigem() {
		String retorno = null;
		
		if (documentoOrigem != null)
			retorno = documentoOrigem.toUpperCase().trim();
		return retorno;
	}
	
	public void setDocumentoOrigem(String documentoOrigem) {
		if (documentoOrigem != null)
		{
			this.documentoOrigem = documentoOrigem.toUpperCase().trim();
		}
		else
			this.documentoOrigem = null;
			
		
	}
		
	public Integer getSequencialItem() {
		return sequencialItem;
	}

	public void setSequencialItem(Integer sequencialItem) {
		if (sequencialItem != null && sequencialItem == 0)
		{
			this.sequencialItem = null;
		}
		else
		{
			this.sequencialItem = sequencialItem;
		}
	}	
	public Double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Double quantidade)
	{
		/*
		if (quantidade != null && quantidade == 0.0)
		{
			this.quantidade = null;
		}
		else
		{
			this.quantidade = quantidade;
		}*/
		this.quantidade = quantidade;
	}	
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor)
	{
		/*
		if (valor != null && valor == 0.0)
		{
			this.valor = null;
		}
		else
		{
			this.valor = valor;
		}*/
		this.valor = valor;
	}	
	public Double getCusto() {
		return custo;
	}
	
	public void setVenda(Double venda)
	{
		if (venda != null && Double.isNaN(venda))
		{
			this.venda = null; 
		}
		else
		{
			this.venda = venda;
		} 
	}
	
	public Double getVenda() 
	{
		return venda;
	}

	public void setCusto(Double custo) {
		if (custo != null && Double.isNaN(custo))
		{
			this.custo = null;
		}
		else
		{
			this.custo = custo;
		}
	}	
	public String getNumeroLote() {
		String retorno = null;
		
		if (numeroLote != null)
			retorno = numeroLote.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroLote(String numeroLote) {
		if (numeroLote != null)
		{
			this.numeroLote = numeroLote.toUpperCase().trim();
		}
		else
			this.numeroLote = null;
			
		
	}
		
	public Date getDataValidade() {
		return dataValidade;
	}

	public void setDataValidade(Date dataValidade) {
		this.dataValidade = dataValidade;
	}
	public Date getDataFabricacao() {
		return dataFabricacao;
	}

	public void setDataFabricacao(Date dataFabricacao) {
		this.dataFabricacao = dataFabricacao;
	}
	
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
