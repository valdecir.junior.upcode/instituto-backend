package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="atributovisualizacaolvproduto")


public class AtributoVisualizacaoLvProduto extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idAtributoVisualizacaoLvProduto")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idAtributoVisualizacaoLv")
	private AtributoVisualizacaoLv atributoVisualizacaoLv;
	@ManyToOne
	@JoinColumn(name = "idEstoqueProduto")
	private EstoqueProduto estoqueProduto;
	@ManyToOne
	@JoinColumn(name = "idSituacaoEnvioLv")
	private SituacaoEnvioLV situacaoEnvioLv;

	public AtributoVisualizacaoLvProduto()
	{
		super();
	}
	

	public AtributoVisualizacaoLvProduto
	(
	AtributoVisualizacaoLv atributoVisualizacaoLv,
	EstoqueProduto estoqueProduto,
	SituacaoEnvioLV situacaoEnvioLv
	) 
	{
		super();
		this.atributoVisualizacaoLv = atributoVisualizacaoLv;
		this.estoqueProduto = estoqueProduto;
		this.situacaoEnvioLv = situacaoEnvioLv;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public AtributoVisualizacaoLv getAtributoVisualizacaoLv() {
		return atributoVisualizacaoLv;
	}

	public void setAtributoVisualizacaoLv(AtributoVisualizacaoLv atributoVisualizacaoLv) {
		this.atributoVisualizacaoLv = atributoVisualizacaoLv;
	}
	
	public EstoqueProduto getEstoqueProduto() {
		return estoqueProduto;
	}

	public void setEstoqueProduto(EstoqueProduto estoqueProduto) {
		this.estoqueProduto = estoqueProduto;
	}
	
	public SituacaoEnvioLV getSituacaoEnvioLv() {
		return situacaoEnvioLv;
	}

	public void setSituacaoEnvioLv(SituacaoEnvioLV situacaoEnvioLv) {
		this.situacaoEnvioLv = situacaoEnvioLv;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
