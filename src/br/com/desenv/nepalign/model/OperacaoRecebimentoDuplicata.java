package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="operacaorecebimentoduplicata")


public class OperacaoRecebimentoDuplicata extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idoperacacaorecebimentoduplicata")
	private Integer id;
		
	@Column(name="dataOperacao")
	private Date dataOperacao;
	
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@Column(name="valorPago")
	private Double valorPago;
	
	@Column(name="valorOperacao")
	private Double valorOperacao;
	

	public OperacaoRecebimentoDuplicata()
	{
		super();
	}
	

	public OperacaoRecebimentoDuplicata
	(
		Date dataOperacao,
	Usuario usuario,
		Double valorPago,
		Double valorOperacao
	) 
	{
		super();
		this.dataOperacao = dataOperacao;
		this.usuario = usuario;
		this.valorPago = valorPago;
		this.valorOperacao = valorOperacao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Date getDataOperacao() {
		return dataOperacao;
	}

	public void setDataOperacao(Date dataOperacao) {
		this.dataOperacao = dataOperacao;
	}
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Double getValorPago() {
		return valorPago;
	}

	public void setValorPago(Double valorPago) {
	
		if (valorPago != null && Double.isNaN(valorPago))
		{
			this.valorPago = null;
		}
		else
		{
			this.valorPago = valorPago;
		}
		
	}	
	public Double getValorOperacao() {
		return valorOperacao;
	}

	public void setValorOperacao(Double valorOperacao) {
	
		if (valorOperacao != null && Double.isNaN(valorOperacao))
		{
			this.valorOperacao = null;
		}
		else
		{
			this.valorOperacao = valorOperacao;
		}
		
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
