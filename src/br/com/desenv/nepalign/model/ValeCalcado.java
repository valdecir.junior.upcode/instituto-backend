package br.com.desenv.nepalign.model;

import java.util.Date;

import br.com.desenv.nepalign.util.DsvNumeroExtenso;


public class ValeCalcado 
{
	private String empresa;
	private Double valorVale;
	private String nomeFuncionario;
	private Date vencimento;
	private Date dataEmissao;
	private Integer numeroParcelas;
	private Integer parcela;
	private String numeroControle;
	private Double valorParcela;
	private String cpfEmpresa;
	private String valorExtenso;
	private String cidade;
	private String cpfEmitente;
	private String enderecoEmitente;
	private String dataExtenso;
	
	public ValeCalcado(){}
	
	public String getEmpresa() {
		return empresa;
	}


	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}


	public Double getValorVale() {
		return valorVale;
	}


	public void setValorVale(Double valorVale) {
		this.valorVale = valorVale;
	}


	public String getNomeFuncionario() {
		return nomeFuncionario;
	}


	public void setNomeFuncionario(String nomeFuncionario) {
		this.nomeFuncionario = nomeFuncionario;
	}


	public Date getVencimento() {
		return vencimento;
	}


	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}


	public Date getDataEmissao() {
		return dataEmissao;
	}


	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}


	public Integer getNumeroParcelas() {
		return numeroParcelas;
	}


	public void setNumeroParcelas(Integer numeroParcelas) {
		this.numeroParcelas = numeroParcelas;
	}


	public Integer getParcela() {
		return parcela;
	}


	public void setParcela(Integer parcela) {
		this.parcela = parcela;
	}

	public String getNumeroControle() {
		return numeroControle;
	}

	public void setNumeroControle(String numeroControle) {
		this.numeroControle = numeroControle;
	}

	public Double getValorParcela() {
		return valorParcela;
	}

	public void setValorParcela(Double valorParcela) {
		this.valorParcela = valorParcela;
	}

	public String getValorExtenso() {
		return valorExtenso;
	}

	public void setValorExtenso(String valorExtenso) {
		this.valorExtenso = valorExtenso;
	}

	public String getCpfEmpresa() {
		return cpfEmpresa;
	}

	public void setCpfEmpresa(String cpfEmpresa) {
		this.cpfEmpresa = cpfEmpresa;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade; 
	}

	public String getCpfEmitente() {
		return cpfEmitente;
	}
 
	public void setCpfEmitente(String cpfEmitente) {
		this.cpfEmitente = cpfEmitente;
	}

	public String getEnderecoEmitente() {
		return enderecoEmitente;
	}

	public void setEnderecoEmitente(String enderecoEmitente) {
		this.enderecoEmitente = enderecoEmitente;
	}

	public String getDataExtenso() {
		return dataExtenso;
	}

	public void setDataExtenso(String dataExtenso) {
		this.dataExtenso = dataExtenso;
	}


	
	

}