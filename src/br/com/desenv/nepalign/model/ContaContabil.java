package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="contaContabil")
public class ContaContabil extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idContaContabil")
	private Integer id;
	
	@Column(name="idEmpresa")
	private Integer idEmpresa;
	
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="codigoContabil")
	private String codigoContabil;
	
	@Column(name="codigoContabilNivel1")
	private Integer codigoContabilNivel1;
	
	@Column(name="codigoContabilNivel2")
	private Integer codigoContabilNivel2;
	
	@Column(name="codigoContabilNivel3")
	private Integer codigoContabilNivel3;
	
	@Column(name="creditoDebito")
	private Integer creditoDebito;
	
	public ContaContabil() { super(); }

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}	
	
	public Integer getIdEmpresa() 
	{
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) 
	{
		this.idEmpresa = (idEmpresa == null || Double.isNaN(idEmpresa)) ? null : idEmpresa;
	}

	public String getDescricao() 
	{
		return descricao;
	}

	public void setDescricao(String descricao) 
	{
		this.descricao = descricao != null ? descricao.toUpperCase().trim() : null;
	}

	public String getCodigoContabil() 
	{
		return codigoContabil;
	}

	public void setCodigoContabil(String codigoContabil) 
	{
		this.codigoContabil = codigoContabil != null ? codigoContabil.toUpperCase().trim() : null;
	}

	public Integer getCodigoContabilNivel1() 
	{
		return codigoContabilNivel1;
	}

	public void setCodigoContabilNivel1(Integer codigoContabilNivel1) 
	{
		this.codigoContabilNivel1 = (codigoContabilNivel1 == null || Double.isNaN(codigoContabilNivel1)) ? null : codigoContabilNivel1;
	}

	public Integer getCodigoContabilNivel2() 
	{
		return codigoContabilNivel2;
	}

	public void setCodigoContabilNivel2(Integer codigoContabilNivel2) 
	{
		this.codigoContabilNivel2 = (codigoContabilNivel2 == null || Double.isNaN(codigoContabilNivel2)) ? null : codigoContabilNivel2;
	}

	public Integer getCodigoContabilNivel3() 
	{
		return codigoContabilNivel3;
	}

	public void setCodigoContabilNivel3(Integer codigoContabilNivel3) 
	{
		this.codigoContabilNivel3 = (codigoContabilNivel3 == null || Double.isNaN(codigoContabilNivel3)) ? null : codigoContabilNivel3;
	}

	public Integer getCreditoDebito() 
	{
		return creditoDebito;
	}

	public void setCreditoDebito(Integer creditoDebito) 
	{
		this.creditoDebito = (creditoDebito == null || Double.isNaN(creditoDebito)) ? null : creditoDebito;
	}

	@Override
	public void validate() throws Exception { }
}