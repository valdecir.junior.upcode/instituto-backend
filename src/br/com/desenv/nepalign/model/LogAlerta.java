package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="logalerta")


public class LogAlerta extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idLogAlerta")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idAlertaLoja")
	private AlertaLoja alertaLoja;
	
	@ManyToOne
	@JoinColumn(name = "idTipoLogAlerta")
	private TipoLogAlerta tipoLogAlerta;
	
	@Column(name="dataHora")
	private Date data;
	
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;

	public LogAlerta()
	{
		super();
	}
	

	public LogAlerta
	(
	AlertaLoja alertaLoja,
	TipoLogAlerta tipoLogAlerta,
		Date data,
	Usuario usuario
	) 
	{
		super();
		this.alertaLoja = alertaLoja;
		this.tipoLogAlerta = tipoLogAlerta;
		this.data = data;
		this.usuario = usuario;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public AlertaLoja getAlertaLoja() {
		return alertaLoja;
	}

	public void setAlertaLoja(AlertaLoja alertaLoja) {
		this.alertaLoja = alertaLoja;
	}
	
	public TipoLogAlerta getTipoLogAlerta() {
		return tipoLogAlerta;
	}

	public void setTipoLogAlerta(TipoLogAlerta tipoLogAlerta) {
		this.tipoLogAlerta = tipoLogAlerta;
	}
	
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
