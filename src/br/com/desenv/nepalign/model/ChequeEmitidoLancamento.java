package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="chequeemitidolancamento")


public class ChequeEmitidoLancamento extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idChequeEmitidoLancamento")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idLancamentoContaPagar")
	private LancamentoContaPagar lancamentoContaPagar;
	@ManyToOne
	@JoinColumn(name = "idChequeEmitido")
	private ChequeEmitido chequeEmitido;

	public ChequeEmitidoLancamento()
	{
		super();
	}
	

	public ChequeEmitidoLancamento
	(
	LancamentoContaPagar lancamentoContaPagar,
	ChequeEmitido chequeEmitido
	) 
	{
		super();
		this.lancamentoContaPagar = lancamentoContaPagar;
		this.chequeEmitido = chequeEmitido;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public LancamentoContaPagar getLancamentoContaPagar() {
		return lancamentoContaPagar;
	}

	public void setLancamentoContaPagar(LancamentoContaPagar lancamentoContaPagar) {
		this.lancamentoContaPagar = lancamentoContaPagar;
	}
	
	public ChequeEmitido getChequeEmitido() {
		return chequeEmitido;
	}

	public void setChequeEmitido(ChequeEmitido chequeEmitido) {
		this.chequeEmitido = chequeEmitido;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
