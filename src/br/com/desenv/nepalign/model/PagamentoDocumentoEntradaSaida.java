package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="pagamentodocumentoentradasaida")


public class PagamentoDocumentoEntradaSaida extends GenericModelIGN
{
	public static final String CADASTRADO = "1";
	public static final String REVISADO = "2";
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idpagamentodocumentoentradasaida")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresa;
	@ManyToOne
	@JoinColumn(name = "idDocumentoEntradaSaida")
	private DocumentoEntradaSaida documentoEntradaSaida;
	@ManyToOne
	@JoinColumn(name = "idFormaPagamento")
	private FormaPagamento formaPagamento;
	@Column(name="entradaSaida")
	private String entradaSaida;
	
	@Column(name="numeroFatura")
	private String numeroFatura;
	
	@Column(name="dataVencimento")
	private Date dataVencimento;
	
	@Column(name="valor")
	private Double valor;
	
	@Column(name="situacao")
	private String situacao;
	
	@Column(name="observacao")
	private String observacao;
	

	public PagamentoDocumentoEntradaSaida()
	{
		super();
	}
	

	public PagamentoDocumentoEntradaSaida
	(
	EmpresaFisica empresa,
	DocumentoEntradaSaida documentoEntradaSaida,
	FormaPagamento formaPagamento,
		String entradaSaida,
		String numeroFatura,
		Date dataVencimento,
		Double valor,
		String situacao,
		String observacao
	) 
	{
		super();
		this.empresa = empresa;
		this.documentoEntradaSaida = documentoEntradaSaida;
		this.formaPagamento = formaPagamento;
		this.entradaSaida = entradaSaida;
		this.numeroFatura = numeroFatura;
		this.dataVencimento = dataVencimento;
		this.valor = valor;
		this.situacao = situacao;
		this.observacao = observacao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public EmpresaFisica getEmpresa() {
		return empresa;
	}

	public void setEmpresa(EmpresaFisica empresa) {
		this.empresa = empresa;
	}
	
	public DocumentoEntradaSaida getDocumentoEntradaSaida() {
		return documentoEntradaSaida;
	}

	public void setDocumentoEntradaSaida(DocumentoEntradaSaida documentoEntradaSaida) {
		this.documentoEntradaSaida = documentoEntradaSaida;
	}
	
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
	public String getEntradaSaida() {
		String retorno = null;
		
		if (entradaSaida != null)
			retorno = entradaSaida.toUpperCase().trim();
		return retorno;
	}
	
	public void setEntradaSaida(String entradaSaida) {
		if (entradaSaida != null)
		{
			this.entradaSaida = entradaSaida.toUpperCase().trim();
		}
		else
			this.entradaSaida = null;
			
		
	}
		
	public String getNumeroFatura() {
		String retorno = null;
		
		if (numeroFatura != null)
			retorno = numeroFatura.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroFatura(String numeroFatura) {
		if (numeroFatura != null)
		{
			this.numeroFatura = numeroFatura.toUpperCase().trim();
		}
		else
			this.numeroFatura = null;
			
		
	}
		
	public Date getDataVencimento() 
	{
		return this.dataVencimento;
	}
	
	public void setDataVencimento(Date dataVencimento)
	{
		if (dataVencimento != null)
		{
			this.dataVencimento = dataVencimento;
		}
		else
			this.dataVencimento = null;
	}
		
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
	
		if (valor != null && Double.isNaN(valor))
		{
			this.valor = null;
		}
		else
		{
			this.valor = valor;
		}
		
	}	
	public String getSituacao() {
		String retorno = null;
		
		if (situacao != null)
			retorno = situacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setSituacao(String situacao) {
		if (situacao != null)
		{
			this.situacao = situacao.toUpperCase().trim();
		}
		else
			this.situacao = null;
			
		
	}
		
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
