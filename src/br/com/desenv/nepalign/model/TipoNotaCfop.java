package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="tiponotacfop")


public class TipoNotaCfop extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idTipoNotaCfop")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idTipoNotaFiscal")
	private TipoNotaFiscal tipoNotaFiscal;
	@ManyToOne
	@JoinColumn(name = "idCFOP")
	private Cfop cfop;
	@ManyToOne
	@JoinColumn(name = "idCFOPForaEstado")
	private Cfop cfopForaEstado;

	public TipoNotaCfop()
	{
		super();
	}
	

	public TipoNotaCfop
	(
	TipoNotaFiscal tipoNotaFiscal,
	Cfop cfop,
	Cfop cfopForaEstado
	) 
	{
		super();
		this.tipoNotaFiscal = tipoNotaFiscal;
		this.cfop = cfop;
		this.cfopForaEstado = cfopForaEstado;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public TipoNotaFiscal getTipoNotaFiscal() {
		return tipoNotaFiscal;
	}

	public void setTipoNotaFiscal(TipoNotaFiscal tipoNotaFiscal) {
		this.tipoNotaFiscal = tipoNotaFiscal;
	}
	
	public Cfop getCfop() {
		return cfop;
	}

	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	
	public Cfop getCfopForaEstado() {
		return cfopForaEstado;
	}

	public void setCfopForaEstado(Cfop cfopForaEstado) {
		this.cfopForaEstado = cfopForaEstado;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
