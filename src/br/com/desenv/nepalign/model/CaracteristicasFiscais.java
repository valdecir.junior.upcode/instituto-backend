package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="caracteristicasfiscais")


public class CaracteristicasFiscais extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idCaracteristicasFiscais")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idUf")
	private Uf uf;
	@ManyToOne
	@JoinColumn(name = "idCFOP")
	private Cfop cfop;
	@ManyToOne
	@JoinColumn(name = "idCodigoSituacaoTributaria")
	private CodigoSituacaoTributaria codigoSituacaoTributaria;
	@ManyToOne
	@JoinColumn(name = "idSubcodigosituacaotributaria")
	private SubCodigoSituacaoTributaria subCodigoSituacaoTributaria;
	@ManyToOne
	@JoinColumn(name = "idCstpis")
	private Cstpis cstpis;
	@ManyToOne
	@JoinColumn(name = "idCstcofins")
	private Cstcofins cstcofins;
	@ManyToOne
	@JoinColumn(name = "idCstipi")
	private Cstipi cstipi;
	@ManyToOne
	@JoinColumn(name = "idCFOPForaUF")
	private Cfop cfopForaUf;
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="aplicIdCfop")
	private Integer aplicIdCfop;
	
	@Column(name="icms")
	private Double icms;
	
	@Column(name="aplicIcms")
	private Integer aplicIcms;
	
	@Column(name="ipi")
	private Double ipi;
	
	@Column(name="aplicIpi")
	private Integer aplicIpi;
	
	@Column(name="pis")
	private Double pis;
	
	@Column(name="aplicPis")
	private Integer aplicPis;
	
	@Column(name="cofins")
	private Double cofins;
	
	@Column(name="aplicCofins")
	private Integer aplicCofins;
	
	@Column(name="csll")
	private Double csll;
	
	@Column(name="aplicCsll")
	private Integer aplicCsll;
	
	@Column(name="irrf")
	private Double irrf;
	
	@Column(name="aplicIrrf")
	private Integer aplicIRRF;
	
	@Column(name="reducaoBCIcms")
	private Double reducaoBCIcms;
	
	@Column(name="aplicReducaoBCIcms")
	private Integer aplicReducaoBCIcms;
	
	@Column(name="mva")
	private Double mva;
	
	@Column(name="aplicMva")
	private Integer aplicMva;
	
	@Column(name="aplicCodigoSitTrib")
	private Integer aplicCodigoSitTrib;
	
	@Column(name="exibirPisNota")
	private Integer exibirPisNota;
	
	@Column(name="exibirCofinsNota")
	private Integer exibirCofinsNota;
	
	@Column(name="exibirCsllNota")
	private Integer exibirCsllNota;
	
	@Column(name="exibirIrrfNota")
	private Integer exibirIrrfNota;
	
	@Column(name="observacao")
	private String observacao;
	
	@Column(name="sobreescreverobs")
	private Integer sobreescreverobs;
	
	@Column(name="localObs")
	private Integer localObs;
	
	@Column(name="ativo")
	private Integer ativo;
	
	@Column(name="descontoImpostos")
	private Integer descontoImpostos;
	
	@Column(name="origemNota")
	private Integer origemNota;
	
	@Column(name="suspCalcST")
	private Integer suspCalcSt;
	
	@Column(name="reducaoBCIcmsST")
	private Double reducaoBCIcmsST;
	
	@Column(name="aplicReducaoBCIcmsST")
	private Integer aplicReducaoBCIcmsST;
	
	@Column(name="aplicIdCfopForaUF")
	private Integer aplicCfopForaUf;
	
	@ManyToOne
	@JoinColumn(name = "idMva")
	private Mva objetoMva;

	public CaracteristicasFiscais()
	{
		super();
	}
	

	public CaracteristicasFiscais
	(
	Uf uf,
	Cfop cfop,
	CodigoSituacaoTributaria codigoSituacaoTributaria,
	SubCodigoSituacaoTributaria subCodigoSituacaoTributaria,
	Cstpis cstpis,
	Cstcofins cstcofins,
	Cstipi cstipi,
	Cfop cfopForaUf,
		String descricao,
		Integer aplicIdCfop,
		Double icms,
		Integer aplicIcms,
		Double ipi,
		Integer aplicIpi,
		Double pis,
		Integer aplicPis,
		Double cofins,
		Integer aplicCofins,
		Double csll,
		Integer aplicCsll,
		Double irrf,
		Integer aplicIRRF,
		Double reducaoBCIcms,
		Integer aplicReducaoBCIcms,
		Double mva,
		Integer aplicMva,
		Integer aplicCodigoSitTrib,
		Integer exibirPisNota,
		Integer exibirCofinsNota,
		Integer exibirCsllNota,
		Integer exibirIrrfNota,
		String observacao,
		Integer sobreescreverobs,
		Integer localObs,
		Integer ativo,
		Integer descontoImpostos,
		Integer origemNota,
		Integer suspCalcSt,
		Double reducaoBCIcmsST,
		Integer aplicReducaoBCIcmsST,
		Integer aplicCfopForaUf,
		Mva objetoMva
	) 
	{
		super();
		this.uf = uf;
		this.cfop = cfop;
		this.codigoSituacaoTributaria = codigoSituacaoTributaria;
		this.subCodigoSituacaoTributaria = subCodigoSituacaoTributaria;
		this.cstpis = cstpis;
		this.cstcofins = cstcofins;
		this.cstipi = cstipi;
		this.cfopForaUf = cfopForaUf;
		this.descricao = descricao;
		this.aplicIdCfop = aplicIdCfop;
		this.icms = icms;
		this.aplicIcms = aplicIcms;
		this.ipi = ipi;
		this.aplicIpi = aplicIpi;
		this.pis = pis;
		this.aplicPis = aplicPis;
		this.cofins = cofins;
		this.aplicCofins = aplicCofins;
		this.csll = csll;
		this.aplicCsll = aplicCsll;
		this.irrf = irrf;
		this.aplicIRRF = aplicIRRF;
		this.reducaoBCIcms = reducaoBCIcms;
		this.aplicReducaoBCIcms = aplicReducaoBCIcms;
		this.mva = mva;
		this.aplicMva = aplicMva;
		this.aplicCodigoSitTrib = aplicCodigoSitTrib;
		this.exibirPisNota = exibirPisNota;
		this.exibirCofinsNota = exibirCofinsNota;
		this.exibirCsllNota = exibirCsllNota;
		this.exibirIrrfNota = exibirIrrfNota;
		this.observacao = observacao;
		this.sobreescreverobs = sobreescreverobs;
		this.localObs = localObs;
		this.ativo = ativo;
		this.descontoImpostos = descontoImpostos;
		this.origemNota = origemNota;
		this.suspCalcSt = suspCalcSt;
		this.reducaoBCIcmsST = reducaoBCIcmsST;
		this.aplicReducaoBCIcmsST = aplicReducaoBCIcmsST;
		this.aplicCfopForaUf = aplicCfopForaUf;
		this.objetoMva = objetoMva;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}
	
	public Cfop getCfop() {
		return cfop;
	}

	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	
	public CodigoSituacaoTributaria getCodigoSituacaoTributaria() {
		return codigoSituacaoTributaria;
	}

	public void setCodigoSituacaoTributaria(CodigoSituacaoTributaria codigoSituacaoTributaria) {
		this.codigoSituacaoTributaria = codigoSituacaoTributaria;
	}
	
	public SubCodigoSituacaoTributaria getSubCodigoSituacaoTributaria() {
		return subCodigoSituacaoTributaria;
	}

	public void setSubCodigoSituacaoTributaria(SubCodigoSituacaoTributaria subCodigoSituacaoTributaria) {
		this.subCodigoSituacaoTributaria = subCodigoSituacaoTributaria;
	}
	
	public Cstpis getCstpis() {
		return cstpis;
	}

	public void setCstpis(Cstpis cstpis) {
		this.cstpis = cstpis;
	}
	
	public Cstcofins getCstcofins() {
		return cstcofins;
	}

	public void setCstcofins(Cstcofins cstcofins) {
		this.cstcofins = cstcofins;
	}
	
	public Cstipi getCstipi() {
		return cstipi;
	}

	public void setCstipi(Cstipi cstipi) {
		this.cstipi = cstipi;
	}
	
	public Cfop getCfopForaUf() {
		return cfopForaUf;
	}

	public void setCfopForaUf(Cfop cfopForaUf) {
		this.cfopForaUf = cfopForaUf;
	}
	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public Integer getAplicIdCfop() {
		return aplicIdCfop;
	}

	public void setAplicIdCfop(Integer aplicIdCfop) {
		this.aplicIdCfop = aplicIdCfop;
	}	
	public Double getIcms() {
		return icms;
	}

	public void setIcms(Double icms) {
	
		if (icms != null && Double.isNaN(icms))
		{
			this.icms = null;
		}
		else
		{
			this.icms = icms;
		}
		
	}	
	public Integer getAplicIcms() {
		return aplicIcms;
	}

	public void setAplicIcms(Integer aplicIcms) {
		this.aplicIcms = aplicIcms;
	}	
	public Double getIpi() {
		return ipi;
	}

	public void setIpi(Double ipi) {
	
		if (ipi != null && Double.isNaN(ipi))
		{
			this.ipi = null;
		}
		else
		{
			this.ipi = ipi;
		}
		
	}	
	public Integer getAplicIpi() {
		return aplicIpi;
	}

	public void setAplicIpi(Integer aplicIpi) {
		this.aplicIpi = aplicIpi;
	}	
	public Double getPis() {
		return pis;
	}

	public void setPis(Double pis) {
	
		if (pis != null && Double.isNaN(pis))
		{
			this.pis = null;
		}
		else
		{
			this.pis = pis;
		}
		
	}	
	public Integer getAplicPis() {
		return aplicPis;
	}

	public void setAplicPis(Integer aplicPis) {
		this.aplicPis = aplicPis;
	}	
	public Double getCofins() {
		return cofins;
	}

	public void setCofins(Double cofins) {
	
		if (cofins != null && Double.isNaN(cofins))
		{
			this.cofins = null;
		}
		else
		{
			this.cofins = cofins;
		}
		
	}	
	public Integer getAplicCofins() {
		return aplicCofins;
	}

	public void setAplicCofins(Integer aplicCofins) {
		this.aplicCofins = aplicCofins;
	}	
	public Double getCsll() {
		return csll;
	}

	public void setCsll(Double csll) {
	
		if (csll != null && Double.isNaN(csll))
		{
			this.csll = null;
		}
		else
		{
			this.csll = csll;
		}
		
	}	
	public Integer getAplicCsll() {
		return aplicCsll;
	}

	public void setAplicCsll(Integer aplicCsll) {
		this.aplicCsll = aplicCsll;
	}	
	public Double getIrrf() {
		return irrf;
	}

	public void setIrrf(Double irrf) {
	
		if (irrf != null && Double.isNaN(irrf))
		{
			this.irrf = null;
		}
		else
		{
			this.irrf = irrf;
		}
		
	}	
	public Integer getAplicIRRF() {
		return aplicIRRF;
	}

	public void setAplicIRRF(Integer aplicIRRF) {
		this.aplicIRRF = aplicIRRF;
	}	
	public Double getReducaoBCIcms() {
		return reducaoBCIcms;
	}

	public void setReducaoBCIcms(Double reducaoBCIcms) {
	
		if (reducaoBCIcms != null && Double.isNaN(reducaoBCIcms))
		{
			this.reducaoBCIcms = null;
		}
		else
		{
			this.reducaoBCIcms = reducaoBCIcms;
		}
		
	}	
	public Integer getAplicReducaoBCIcms() {
		return aplicReducaoBCIcms;
	}

	public void setAplicReducaoBCIcms(Integer aplicReducaoBCIcms) {
		this.aplicReducaoBCIcms = aplicReducaoBCIcms;
	}	
	public Double getMva() {
		return mva;
	}

	public void setMva(Double mva) {
	
		if (mva != null && Double.isNaN(mva))
		{
			this.mva = null;
		}
		else
		{
			this.mva = mva;
		}
		
	}	
	public Integer getAplicMva() {
		return aplicMva;
	}

	public void setAplicMva(Integer aplicMva) {
		this.aplicMva = aplicMva;
	}	
	public Integer getAplicCodigoSitTrib() {
		return aplicCodigoSitTrib;
	}

	public void setAplicCodigoSitTrib(Integer aplicCodigoSitTrib) {
		this.aplicCodigoSitTrib = aplicCodigoSitTrib;
	}	
	public Integer getExibirPisNota() {
		return exibirPisNota;
	}

	public void setExibirPisNota(Integer exibirPisNota) {
		this.exibirPisNota = exibirPisNota;
	}	
	public Integer getExibirCofinsNota() {
		return exibirCofinsNota;
	}

	public void setExibirCofinsNota(Integer exibirCofinsNota) {
		this.exibirCofinsNota = exibirCofinsNota;
	}	
	public Integer getExibirCsllNota() {
		return exibirCsllNota;
	}

	public void setExibirCsllNota(Integer exibirCsllNota) {
		this.exibirCsllNota = exibirCsllNota;
	}	
	public Integer getExibirIrrfNota() {
		return exibirIrrfNota;
	}

	public void setExibirIrrfNota(Integer exibirIrrfNota) {
		this.exibirIrrfNota = exibirIrrfNota;
	}	
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	public Integer getSobreescreverobs() {
		return sobreescreverobs;
	}

	public void setSobreescreverobs(Integer sobreescreverobs) {
		this.sobreescreverobs = sobreescreverobs;
	}	
	public Integer getLocalObs() {
		return localObs;
	}

	public void setLocalObs(Integer localObs) {
		this.localObs = localObs;
	}	
	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}	
	public Integer getDescontoImpostos() {
		return descontoImpostos;
	}

	public void setDescontoImpostos(Integer descontoImpostos) {
		this.descontoImpostos = descontoImpostos;
	}	
	public Integer getOrigemNota() {
		return origemNota;
	}

	public void setOrigemNota(Integer origemNota) {
		this.origemNota = origemNota;
	}	
	public Integer getSuspCalcSt() {
		return suspCalcSt;
	}

	public void setSuspCalcSt(Integer suspCalcSt) {
		this.suspCalcSt = suspCalcSt;
	}	
	public Double getReducaoBCIcmsST() {
		return reducaoBCIcmsST;
	}

	public void setReducaoBCIcmsST(Double reducaoBCIcmsST) {
	
		if (reducaoBCIcmsST != null && Double.isNaN(reducaoBCIcmsST))
		{
			this.reducaoBCIcmsST = null;
		}
		else
		{
			this.reducaoBCIcmsST = reducaoBCIcmsST;
		}
		
	}	
	public Integer getAplicReducaoBCIcmsST() {
		return aplicReducaoBCIcmsST;
	}

	public void setAplicReducaoBCIcmsST(Integer aplicReducaoBCIcmsST) {
		this.aplicReducaoBCIcmsST = aplicReducaoBCIcmsST;
	}	
	public Integer getAplicCfopForaUf() {
		return aplicCfopForaUf;
	}

	public void setAplicCfopForaUf(Integer aplicCfopForaUf) {
		this.aplicCfopForaUf = aplicCfopForaUf;
	}	
	
	
	public Mva getObjetoMva() {
		return objetoMva;
	}


	public void setObjetoMva(Mva objetoMva) {
		this.objetoMva = objetoMva;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
