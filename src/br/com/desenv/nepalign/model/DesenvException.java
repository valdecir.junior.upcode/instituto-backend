/**
 * 
 */
package br.com.desenv.nepalign.model;

public class DesenvException extends Exception 
{
	public DesenvException()
	{
		super();
	}
	
	public DesenvException(String message, Throwable cause)
	{
		super(message, cause);
	}
	
	public DesenvException(String message)
	{
		super(message);
	}
	
	public DesenvException(Throwable cause)
	{
		super(cause);
	}
}
