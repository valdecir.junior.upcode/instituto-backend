package br.com.desenv.nepalign.model;

import java.util.HashMap;



public class RelatorioMapaParesObjeto 
{
	
	private HashMap<String, Integer> hash = new HashMap<>(); // saldo, descricao
	private String descricao;
	private Integer totalDia; 
	private String dataMovimentacao;
	private Integer estoque;
	private String ES;
	
	public RelatorioMapaParesObjeto()
	{
		super();
		this.descricao = null;
	}
	
		
	public RelatorioMapaParesObjeto
	(
		HashMap<String,Integer> hash,
		Integer totalDia,
		String dataMovimentacao,
		String ES,
		Integer estoque	
	) 
	{
		super();
		this.hash = hash;
		this.totalDia = totalDia;
		this.dataMovimentacao = dataMovimentacao;
		this.estoque = estoque;
		this.ES = ES;
	}
	
	
	
	public HashMap<String, Integer> getHash() {
		return hash;
	}

	public void setHash(HashMap<String, Integer> hash) {
		this.hash = hash;
	}

	public Integer getTotalDia() {
		return totalDia;
	}

	public void setTotalDia(Integer totalDia) {
		this.totalDia = totalDia;
	}

	public String getDataMovimentacao() {
		return dataMovimentacao;
	}

	public void setDataMovimentacao(String dataMovimentacao) {
		this.dataMovimentacao = dataMovimentacao;
	}


	public Integer getEstoque() {
		return estoque;
	}


	public void setEstoque(Integer estoque) {
		this.estoque = estoque;
	}


	public String getES() {
		return ES;
	}


	public void setES(String eS) {
		ES = eS;
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	
}
