package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="atividadeoferecida")


public class AtividadeOferecida extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idAtividadeOferecida")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="manha")
	private String manha;
	
	@Column(name="tarde")
	private String tarde;
	
	@Column(name="noite")
	private String noite;
	
	@Column(name="observacao")
	private String observacao;
	
	@Column(name="duracao")
	private Integer duracao;
	

	public AtividadeOferecida()
	{
		super();
	}
	

	public AtividadeOferecida
	(
		String descricao,
		String manha,
		String tarde,
		String noite,
		String observacao,
		Integer duracao
	) 
	{
		super();
		this.descricao = descricao;
		this.manha = manha;
		this.tarde = tarde;
		this.noite = noite;
		this.observacao = observacao;
		this.duracao = duracao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public String getManha() {
		String retorno = null;
		
		if (manha != null)
			retorno = manha.toUpperCase().trim();
		return retorno;
	}
	
	public void setManha(String manha) {
		if (manha != null)
		{
			this.manha = manha.toUpperCase().trim();
		}
		else
			this.manha = null;
			
		
	}
		
	public String getTarde() {
		String retorno = null;
		
		if (tarde != null)
			retorno = tarde.toUpperCase().trim();
		return retorno;
	}
	
	public void setTarde(String tarde) {
		if (tarde != null)
		{
			this.tarde = tarde.toUpperCase().trim();
		}
		else
			this.tarde = null;
			
		
	}
		
	public String getNoite() {
		String retorno = null;
		
		if (noite != null)
			retorno = noite.toUpperCase().trim();
		return retorno;
	}
	
	public void setNoite(String noite) {
		if (noite != null)
		{
			this.noite = noite.toUpperCase().trim();
		}
		else
			this.noite = null;
			
		
	}
		
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	public Integer getDuracao() {
		return duracao;
	}

	public void setDuracao(Integer duracao) {
		if (duracao != null && duracao == 0)
		{
			this.duracao = null;
		}
		else
		{
			this.duracao = duracao;
		}
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
