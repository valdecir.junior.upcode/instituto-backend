package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="uf")


public class Uf extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idUf")
	private Integer id;
		
	@Column(name="sigla")
	private String sigla;
	
	@Column(name="nome")
	private String nome;
	
	@Column(name="codEstadoIBGE")
	private String codEstadoIBGE;
	

	public Uf()
	{
		super();
	}
	

	public Uf
	(
		String sigla,
		String nome,
		String codEstadoIBGE
	) 
	{
		super();
		this.sigla = sigla;
		this.nome = nome;
		this.codEstadoIBGE = codEstadoIBGE;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getSigla() {
		String retorno = null;
		
		if (sigla != null)
			retorno = sigla.toUpperCase().trim();
		return retorno;
	}
	
	public void setSigla(String sigla) {
		if (sigla != null)
		{
			this.sigla = sigla.toUpperCase().trim();
		}
		else
			this.sigla = null;
			
		
	}
		
	public String getNome() {
		String retorno = null;
		
		if (nome != null)
			retorno = nome.toUpperCase().trim();
		return retorno;
	}
	
	public void setNome(String nome) {
		if (nome != null)
		{
			this.nome = nome.toUpperCase().trim();
		}
		else
			this.nome = null;
			
		
	}
		
	public String getCodEstadoIBGE() {
		String retorno = null;
		
		if (codEstadoIBGE != null)
			retorno = codEstadoIBGE.toUpperCase().trim();
		return retorno;
	}
	
	public void setCodEstadoIBGE(String codEstadoIBGE) {
		if (codEstadoIBGE != null)
		{
			this.codEstadoIBGE = codEstadoIBGE.toUpperCase().trim();
		}
		else
			this.codEstadoIBGE = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
