package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="professor", schema="smartsig")


public class Professor extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idProfessor")
	private Integer id;
		
	@Column(name="nome")
	private String nome;
	
	@Column(name="foneContato")
	private String foneContato;
	
	@Column(name="email")
	private String email;
	

	public Professor()
	{
		super();
	}
	

	public Professor
	(
		String nome,
		String foneContato,
		String email
	) 
	{
		super();
		this.nome = nome;
		this.foneContato = foneContato;
		this.email = email;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public String getNome() {
		String retorno = null;
		
		if (nome != null)
			retorno = nome.toUpperCase().trim();
		return retorno;
	}
	
	public void setNome(String nome) {
		if (nome != null)
		{
			this.nome = nome.toUpperCase().trim();
		}
		else
			this.nome = null;
			
		
	}
		
	public String getFoneContato() {
		String retorno = null;
		
		if (foneContato != null)
			retorno = foneContato.toUpperCase().trim();
		return retorno;
	}
	
	public void setFoneContato(String foneContato) {
		if (foneContato != null)
		{
			this.foneContato = foneContato.toUpperCase().trim();
		}
		else
			this.foneContato = null;
			
		
	}
		
	public String getEmail() {
		String retorno = null;
		
		if (email != null)
			retorno = email.toUpperCase().trim();
		return retorno;
	}
	
	public void setEmail(String email) {
		if (email != null)
		{
			this.email = email.toUpperCase().trim();
		}
		else
			this.email = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
