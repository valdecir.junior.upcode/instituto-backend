package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="observcaractfiscempresa")


public class Observcaractfiscempresa extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idObservcaractfiscempresa")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEmpresa")
	private EmpresaFisica empresa;
	@ManyToOne
	@JoinColumn(name = "idCaracteristicasFiscais")
	private CaracteristicasFiscais caracteristicasFiscais;
	@Column(name="observacao")
	private String observacao;
	

	public Observcaractfiscempresa()
	{
		super();
	}
	

	public Observcaractfiscempresa
	(
	EmpresaFisica empresa,
	CaracteristicasFiscais caracteristicasFiscais,
		String observacao
	) 
	{
		super();
		this.empresa = empresa;
		this.caracteristicasFiscais = caracteristicasFiscais;
		this.observacao = observacao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public EmpresaFisica getEmpresa() {
		return empresa;
	}

	public void setEmpresa(EmpresaFisica empresa) {
		this.empresa = empresa;
	}
	
	public CaracteristicasFiscais getCaracteristicasFiscais() {
		return caracteristicasFiscais;
	}

	public void setCaracteristicasFiscais(CaracteristicasFiscais caracteristicasFiscais) {
		this.caracteristicasFiscais = caracteristicasFiscais;
	}
	
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
