package br.com.desenv.nepalign.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name = "pedidocompravenda")
public class PedidoCompraVenda extends GenericModelIGN implements Cloneable
{
	public static final String PRE_CADASTRO = "5";
	public static final String EFETIVADO = "1";
	public static final String ATENDIDO_PARCIALMENTE = "2";
	public static final String ATENDIDO_INTEGRALMENTE = "3";
	public static final String CANCELADO = "4";
	public static final String BAIXA_AUTORIZADA = "6";
	public static final String CANCELAMENTO_AUTORIZADO = "7";
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idPedidoCompraVenda")
	private Integer id;

	@Column(name = "tipo")
	private String tipo;

	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idFornecedorCliente")
	private Fornecedor fornecedorCliente;
	@ManyToOne
	@JoinColumn(name = "idRepresentante")
	private Representante representante;
	@Column(name = "dataPedido")
	private Date dataPedido;

	@Column(name = "numeroPedido")
	private Integer numeroPedido;

	@Column(name = "dataQuinzena")
	private Date dataQuinzena;

	@Column(name = "condicaoPagamento")
	private String condicaoPagamento;

	@Column(name = "percentualDesconto")
	private String percentualDesconto;

	@Column(name = "percentualFinanceiro")
	private String percentualFinanceiro;

	@Column(name = "percentualFornecedor")
	private Double percentualFornecedor;
	
	
	@Column(name = "cifFob")
	private String cifFob;

	@Column(name = "observacao")
	private String observacao;

	@Column(name = "situacao")
	private String situacao;

	@Column(name = "dataHoraCadastro")
	private Date dataHoraCadastro;

	@ManyToOne
	@JoinColumn(name = "idUsuarioCadastro")
	private Usuario usuarioCadastro;
	@Column(name = "dataHoraUltimaAlteracao")
	private Date dataHoraUltimaAlteracao;

	@ManyToOne
	@JoinColumn(name = "idUsuarioUltimaAlteracao")
	private Usuario usuarioUltimaAlteracao;
	
	@Column(name="pedidoCompraFornecedor")
	private String pedidoCompraFornecedor;

	public PedidoCompraVenda()
	{
		super();
	}

	public PedidoCompraVenda(String tipo, EmpresaFisica empresaFisica, Fornecedor fornecedorCliente, Representante representante, Date dataPedido, Integer numeroPedido, Date dataQuinzena, String condicaoPagamento, String percentualDesconto, String percentualFinanceiro, String cifFob,
			String observacao, String situacao, Date dataHoraCadastro, Usuario usuarioCadastro, Date dataHoraUltimaAlteracao, Usuario usuarioUltimaAlteracao)
	{
		super();
		this.tipo = tipo;
		this.empresaFisica = empresaFisica;
		this.fornecedorCliente = fornecedorCliente;
		this.representante = representante;
		this.dataPedido = dataPedido;
		this.numeroPedido = numeroPedido;
		this.dataQuinzena = dataQuinzena;
		this.condicaoPagamento = condicaoPagamento;
		this.percentualDesconto = percentualDesconto;
		this.percentualFinanceiro = percentualFinanceiro;
		this.cifFob = cifFob;
		this.observacao = observacao;
		this.situacao = situacao;
		this.dataHoraCadastro = dataHoraCadastro;
		this.usuarioCadastro = usuarioCadastro;
		this.dataHoraUltimaAlteracao = dataHoraUltimaAlteracao;
		this.usuarioUltimaAlteracao = usuarioUltimaAlteracao;

	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getTipo()
	{
		String retorno = null;

		if (tipo != null)
			retorno = tipo.toUpperCase().trim();
		return retorno;
	}

	public void setTipo(String tipo)
	{
		if (tipo != null)
		{
			this.tipo = tipo.toUpperCase().trim();
		}
		else
			this.tipo = null;

	}

	public EmpresaFisica getEmpresaFisica()
	{
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica)
	{
		this.empresaFisica = empresaFisica;
	}

	public Fornecedor getFornecedorCliente()
	{
		return fornecedorCliente;
	}

	public void setFornecedorCliente(Fornecedor fornecedorCliente)
	{
		this.fornecedorCliente = fornecedorCliente;
	}

	public Representante getRepresentante()
	{
		return representante;
	}

	public void setRepresentante(Representante representante)
	{
		this.representante = representante;
	}

	public Date getDataPedido()
	{
		return dataPedido;
	}

	public void setDataPedido(Date dataPedido)
	{
		this.dataPedido = dataPedido;
	}

	public Integer getNumeroPedido()
	{
		return numeroPedido;
	}

	public void setNumeroPedido(Integer numeroPedido)
	{
		this.numeroPedido = numeroPedido;

	}

	public Date getDataQuinzena()
	{
		return dataQuinzena;
	}

	public void setDataQuinzena(Date dataQuinzena)
	{
		this.dataQuinzena = dataQuinzena;
	}

	public String getCondicaoPagamento()
	{
		String retorno = null;

		if (condicaoPagamento != null)
			retorno = condicaoPagamento.toUpperCase().trim();
		return retorno;
	}

	public void setCondicaoPagamento(String condicaoPagamento)
	{
		if (condicaoPagamento != null)
		{
			this.condicaoPagamento = condicaoPagamento.toUpperCase().trim();
		}
		else
			this.condicaoPagamento = null;

	}

	public String getPercentualDesconto()
	{
		return percentualDesconto;
	}

	public void setPercentualDesconto(String percentualDesconto)
	{
		if (percentualDesconto != null)
		{
			this.percentualDesconto = percentualDesconto.toUpperCase().trim();
		}
		else
			this.percentualDesconto = null;
	}

	public String getPercentualFinanceiro()
	{
		return percentualFinanceiro;
	}

	public void setPercentualFinanceiro(String percentualFinanceiro)
	{
		if (percentualFinanceiro != null)
		{
			this.percentualFinanceiro = percentualFinanceiro.toUpperCase().trim();
		}
		else
			this.percentualFinanceiro = null;
	}

	public String getCifFob()
	{
		String retorno = null;

		if (cifFob != null)
			retorno = cifFob.toUpperCase().trim();
		return retorno;
	}

	public void setCifFob(String cifFob)
	{
		if (cifFob != null)
		{
			this.cifFob = cifFob.toUpperCase().trim();
		}
		else
			this.cifFob = null;

	}

	public String getObservacao()
	{
		String retorno = null;

		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}

	public void setObservacao(String observacao)
	{
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;

	}

	public String getSituacao()
	{
		String retorno = null;

		if (situacao != null)
			retorno = situacao.toUpperCase().trim();
		return retorno;
	}

	public void setSituacao(String situacao)
	{
		if (situacao != null)
		{
			this.situacao = situacao.toUpperCase().trim();
		}
		else
			this.situacao = null;

	}

	public Date getDataHoraCadastro()
	{
		return dataHoraCadastro;
	}

	public void setDataHoraCadastro(Date dataHoraCadastro)
	{
		this.dataHoraCadastro = dataHoraCadastro;
	}

	public Usuario getUsuarioCadastro()
	{
		return usuarioCadastro;
	}

	public void setUsuarioCadastro(Usuario usuarioCadastro)
	{
		this.usuarioCadastro = usuarioCadastro;
	}

	public Date getDataHoraUltimaAlteracao()
	{
		return dataHoraUltimaAlteracao;
	}

	public void setDataHoraUltimaAlteracao(Date dataHoraUltimaAlteracao)
	{
		this.dataHoraUltimaAlteracao = dataHoraUltimaAlteracao;
	}

	public Usuario getUsuarioUltimaAlteracao()
	{
		return usuarioUltimaAlteracao;
	}

	public void setUsuarioUltimaAlteracao(Usuario usuarioUltimaAlteracao)
	{
		this.usuarioUltimaAlteracao = usuarioUltimaAlteracao;
	}
	
	public Double getPercentualFornecedor()
	{
		return percentualFornecedor;
	}

	public void setPercentualFornecedor(Double percentualFornecedor)
	{
		if (percentualFornecedor != null && Double.isNaN(percentualFornecedor))
		{
			this.percentualFornecedor = null;
		}
		else
		{
			this.percentualFornecedor = percentualFornecedor;
		}
	}

	public String getPedidoCompraFornecedor() {
		return pedidoCompraFornecedor;
	}

	public void setPedidoCompraFornecedor(String pedidoCompraFornecedor) {
		this.pedidoCompraFornecedor = pedidoCompraFornecedor;
	}

	public PedidoCompraVenda clone() throws CloneNotSupportedException
	{
		return (PedidoCompraVenda) super.clone();
	}

	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		 * if (this.descricao == null || this.descricao.trim().equals("")) {
		 * throw new Exception("Descrição é obrigatório."); }
		 */

	}

}
