package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="lancamentocontapagarfixo")


public class LancamentoContaPagarFixo extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idLancamentoContaPagarFixo")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idFornecedor")
	private Fornecedor fornecedor;
	@ManyToOne
	@JoinColumn(name = "idCentroFinanceiro")
	private CentroFinanceiro centroFinanceiro;
	@ManyToOne
	@JoinColumn(name = "idContaGerencial")
	private ContaGerencial contaGerencial;
	@ManyToOne
	@JoinColumn(name = "idTipoConta")
	private TipoConta tipoConta;
	@ManyToOne
	@JoinColumn(name = "idContaCorrente")
	private ContaCorrente contaCorrente;
	@ManyToOne
	@JoinColumn(name = "idFormaPagamento")
	private FormaPagamento formaPagamento;
	@Column(name="valor")
	private Double valor;
	
	@Column(name="diaLancamento")
	private Integer diaLancamento;
	
	@Column(name="observacao")
	private String observacao;
	

	public LancamentoContaPagarFixo()
	{
		super();
	}
	

	public LancamentoContaPagarFixo
	(
	EmpresaFisica empresaFisica,
	Fornecedor fornecedor,
	CentroFinanceiro centroFinanceiro,
	ContaGerencial contaGerencial,
	TipoConta tipoConta,
	ContaCorrente contaCorrente,
	FormaPagamento formaPagamento,
		Double valor,
		Integer diaLancamento,
		String observacao
	) 
	{
		super();
		this.empresaFisica = empresaFisica;
		this.fornecedor = fornecedor;
		this.centroFinanceiro = centroFinanceiro;
		this.contaGerencial = contaGerencial;
		this.tipoConta = tipoConta;
		this.contaCorrente = contaCorrente;
		this.formaPagamento = formaPagamento;
		this.valor = valor;
		this.diaLancamento = diaLancamento;
		this.observacao = observacao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	public CentroFinanceiro getCentroFinanceiro() {
		return centroFinanceiro;
	}

	public void setCentroFinanceiro(CentroFinanceiro centroFinanceiro) {
		this.centroFinanceiro = centroFinanceiro;
	}
	
	public ContaGerencial getContaGerencial() {
		return contaGerencial;
	}

	public void setContaGerencial(ContaGerencial contaGerencial) {
		this.contaGerencial = contaGerencial;
	}
	
	public TipoConta getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}
	
	public ContaCorrente getContaCorrente() {
		return contaCorrente;
	}

	public void setContaCorrente(ContaCorrente contaCorrente) {
		this.contaCorrente = contaCorrente;
	}
	
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
	
		if (valor != null && Double.isNaN(valor))
		{
			this.valor = null;
		}
		else
		{
			this.valor = valor;
		}
		
	}	
	public Integer getDiaLancamento() {
		return diaLancamento;
	}

	public void setDiaLancamento(Integer diaLancamento) {
		this.diaLancamento = diaLancamento;
	}	
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
