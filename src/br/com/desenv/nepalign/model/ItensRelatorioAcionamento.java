package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="itensrelatorioacionamento")


public class ItensRelatorioAcionamento extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idItensRelatorioAcionamento")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idDuplicataParcela")
	private DuplicataParcela duplicataParcela;
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@Column(name="dataAcionamento")
	private Date dataAcionamento;
	
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;

	public ItensRelatorioAcionamento()
	{
		super();
	}
	

	public ItensRelatorioAcionamento
	(
	DuplicataParcela duplicataParcela,
	Cliente cliente,
		Date dataAcionamento,
	EmpresaFisica empresaFisica
	) 
	{
		super();
		this.duplicataParcela = duplicataParcela;
		this.cliente = cliente;
		this.dataAcionamento = dataAcionamento;
		this.empresaFisica = empresaFisica;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public DuplicataParcela getDuplicataParcela() {
		return duplicataParcela;
	}

	public void setDuplicataParcela(DuplicataParcela duplicataParcela) {
		this.duplicataParcela = duplicataParcela;
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Date getDataAcionamento() {
		return dataAcionamento;
	}

	public void setDataAcionamento(Date dataAcionamento) {
		this.dataAcionamento = dataAcionamento;
	}
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
