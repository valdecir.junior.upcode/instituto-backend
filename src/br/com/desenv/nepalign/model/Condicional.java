package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="condicional")


public class Condicional extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idCondicional")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idVendedor")
	private Vendedor vendedor;
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@Column(name="dataGeracao")
	private Date dataGeracao;
	
	@Column(name="valorTotal")
	private Double valorTotal;
	
	@Column(name="valorTotalProdutos")
	private Double valorTotalProdutos;
	
	@Column(name="dataExpiracaoCondicional")
	private Date dataExpiracaoCondicional;
	
	@Column(name="situacao")
	private String situacao;
	
	@Column(name="observacao")
	private String observacao;
	

	public Condicional()
	{
		super();
	}
	

	public Condicional
	(
	EmpresaFisica empresaFisica,
	Vendedor vendedor,
	Usuario usuario,
	Cliente cliente,
		Date dataGeracao,
		Double valorTotal,
		Double valorTotalProdutos,
		Date dataExpiracaoCondicional,
		String situacao,
		String observacao
	) 
	{
		super();
		this.empresaFisica = empresaFisica;
		this.vendedor = vendedor;
		this.usuario = usuario;
		this.cliente = cliente;
		this.dataGeracao = dataGeracao;
		this.valorTotal = valorTotal;
		this.valorTotalProdutos = valorTotalProdutos;
		this.dataExpiracaoCondicional = dataExpiracaoCondicional;
		this.situacao = situacao;
		this.observacao = observacao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public Vendedor getVendedor() {
		return vendedor;
	}

	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Date getDataGeracao() {
		return dataGeracao;
	}

	public void setDataGeracao(Date dataGeracao) {
		this.dataGeracao = dataGeracao;
	}
	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
	
		if (valorTotal != null && Double.isNaN(valorTotal))
		{
			this.valorTotal = null;
		}
		else
		{
			this.valorTotal = valorTotal;
		}
		
	}	
	public Double getValorTotalProdutos() {
		return valorTotalProdutos;
	}

	public void setValorTotalProdutos(Double valorTotalProdutos) {
	
		if (valorTotalProdutos != null && Double.isNaN(valorTotalProdutos))
		{
			this.valorTotalProdutos = null;
		}
		else
		{
			this.valorTotalProdutos = valorTotalProdutos;
		}
		
	}	
	public Date getDataExpiracaoCondicional() {
		return dataExpiracaoCondicional;
	}

	public void setDataExpiracaoCondicional(Date dataExpiracaoCondicional) {
		this.dataExpiracaoCondicional = dataExpiracaoCondicional;
	}
	public String getSituacao() {
		String retorno = null;
		
		if (situacao != null)
			retorno = situacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setSituacao(String situacao) {
		if (situacao != null)
		{
			this.situacao = situacao.toUpperCase().trim();
		}
		else
			this.situacao = null;
			
		
	}
		
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
