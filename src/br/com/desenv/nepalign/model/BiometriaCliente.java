package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="biometriacliente")


public class BiometriaCliente extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idBiometriaCliente")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@Column(name="biometria")
	private String biometria;
	

	public BiometriaCliente()
	{
		super();
	}
	

	public BiometriaCliente
	(
	Cliente cliente,
		String biometria
	) 
	{
		super();
		this.cliente = cliente;
		this.biometria = biometria;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public String getBiometria() {
		String retorno = null;
		
		if (biometria != null)
			retorno = biometria;
		return retorno;
	}
	
	public void setBiometria(String biometria) {
		if (biometria != null)
		{
			this.biometria = biometria;
		}
		else
			this.biometria = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
