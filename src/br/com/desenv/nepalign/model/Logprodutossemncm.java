package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="logprodutossemncm")


public class Logprodutossemncm extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idlogprodutossemncm")
	private Integer id;
		
	@Column(name="idproduto")
	private Integer produto;
	
	@Column(name="ncmAntigo")
	private String ncmAntigo;
	
	@Column(name="ncmNovo")
	private String ncmNovo;
	
	@Column(name="dataLog")
	private Date dataLog;
	
	@Column(name="idEmpresa")
	private Integer empresa;
	
	@Column(name="numeroNotaFiscal")
	private Integer numeroNotaFiscal;
	
	@Column(name="observacao")
	private String observacao;
	

	public Logprodutossemncm()
	{
		super();
	}
	

	public Logprodutossemncm
	(
		Integer produto,
		String ncmAntigo,
		String ncmNovo,
		Date dataLog,
		Integer empresa,
		Integer numeroNotaFiscal,
		String observacao
	) 
	{
		super();
		this.produto = produto;
		this.ncmAntigo = ncmAntigo;
		this.ncmNovo = ncmNovo;
		this.dataLog = dataLog;
		this.empresa = empresa;
		this.numeroNotaFiscal = numeroNotaFiscal;
		this.observacao = observacao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Integer getProduto() {
		return produto;
	}

	public void setProduto(Integer produto) {
		this.produto = produto;
	}	
	public String getNcmAntigo() {
		String retorno = null;
		
		if (ncmAntigo != null)
			retorno = ncmAntigo.toUpperCase().trim();
		return retorno;
	}
	
	public void setNcmAntigo(String ncmAntigo) {
		if (ncmAntigo != null)
		{
			this.ncmAntigo = ncmAntigo.toUpperCase().trim();
		}
		else
			this.ncmAntigo = null;
			
		
	}
		
	public String getNcmNovo() {
		String retorno = null;
		
		if (ncmNovo != null)
			retorno = ncmNovo.toUpperCase().trim();
		return retorno;
	}
	
	public void setNcmNovo(String ncmNovo) {
		if (ncmNovo != null)
		{
			this.ncmNovo = ncmNovo.toUpperCase().trim();
		}
		else
			this.ncmNovo = null;
			
		
	}
		
	public Date getDataLog() {
		return dataLog;
	}

	public void setDataLog(Date dataLog) {
		this.dataLog = dataLog;
	}
	public Integer getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}	
	public Integer getNumeroNotaFiscal() {
		return numeroNotaFiscal;
	}

	public void setNumeroNotaFiscal(Integer numeroNotaFiscal) {
		this.numeroNotaFiscal = numeroNotaFiscal;
	}	
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
