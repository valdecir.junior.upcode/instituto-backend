package br.com.desenv.nepalign.model;

import java.io.Serializable;

import br.com.desenv.frameworkignorante.GenericModelIGN;

public class CorProduto extends GenericModelIGN implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6729845062022169475L;
	
	private Integer id;
	private Cor cor;
	private Produto produto;
	private OrdemProduto ordemProduto;
	
			
			
			
	public Cor getCor() {
		return cor;
	}
	public void setCor(Cor cor) {
		this.cor = cor;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public OrdemProduto getOrdemProduto() {
		return ordemProduto;
	}
	public void setOrdemProduto(OrdemProduto ordemProduto) {
		this.ordemProduto = ordemProduto;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public void validate() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	
}
