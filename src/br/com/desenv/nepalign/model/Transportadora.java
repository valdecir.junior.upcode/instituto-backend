package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="transportadora")


public class Transportadora extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idTransportadora")
	private Integer id;
		
	@Column(name="dataCadastro")
	private Date dataCadastro;
	
	@Column(name="nomeFantasia")
	private String nomeFantasia;
	
	@Column(name="razaoSocial")
	private String razaoSocial;
	
	@Column(name="cnpjCpf")
	private String cnpjCpf;
	
	@Column(name="inscricaoEstadualRg")
	private String inscricaoEstadualRg;
	
	@Column(name="enderecoTransportadora")
	private String enderecoTransportadora;
	
	@Column(name="complementoEndereco")
	private String complementoEndereco;
	
	@Column(name="bairroTransportadora")
	private String bairroTransportadora;
	
	@Column(name="cepTransportadora")
	private String cepTransportadora;
	
	@ManyToOne
	@JoinColumn(name = "idCidade")
	private Cidade cidade;
	@Column(name="contato")
	private String contato;
	
	@Column(name="dddTelefoneTransportadora")
	private String dddTelefoneTransportadora;
	
	@Column(name="telefone")
	private String telefone;
	
	@Column(name="dddFaxTransportadora")
	private String dddFaxTransportadora;
	
	@Column(name="faxTransportadora")
	private String faxTransportadora;
	
	@Column(name="observacao")
	private String observacao;
	
	@Column(name="comentario")
	private String comentario;
	
	@Column(name="emailTransportadora")
	private String emailTransportadora;
	

	public Transportadora()
	{
		super();
	}
	

	public Transportadora
	(
		Date dataCadastro,
		String nomeFantasia,
		String razaoSocial,
		String cnpjCpf,
		String inscricaoEstadualRg,
		String enderecoTransportadora,
		String complementoEndereco,
		String bairroTransportadora,
		String cepTransportadora,
	Cidade cidade,
		String contato,
		String dddTelefoneTransportadora,
		String telefone,
		String dddFaxTransportadora,
		String faxTransportadora,
		String observacao,
		String comentario,
		String emailTransportadora
	) 
	{
		super();
		this.dataCadastro = dataCadastro;
		this.nomeFantasia = nomeFantasia;
		this.razaoSocial = razaoSocial;
		this.cnpjCpf = cnpjCpf;
		this.inscricaoEstadualRg = inscricaoEstadualRg;
		this.enderecoTransportadora = enderecoTransportadora;
		this.complementoEndereco = complementoEndereco;
		this.bairroTransportadora = bairroTransportadora;
		this.cepTransportadora = cepTransportadora;
		this.cidade = cidade;
		this.contato = contato;
		this.dddTelefoneTransportadora = dddTelefoneTransportadora;
		this.telefone = telefone;
		this.dddFaxTransportadora = dddFaxTransportadora;
		this.faxTransportadora = faxTransportadora;
		this.observacao = observacao;
		this.comentario = comentario;
		this.emailTransportadora = emailTransportadora;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public String getNomeFantasia() {
		String retorno = null;
		
		if (nomeFantasia != null)
			retorno = nomeFantasia.toUpperCase().trim();
		return retorno;
	}
	
	public void setNomeFantasia(String nomeFantasia) {
		if (nomeFantasia != null)
		{
			this.nomeFantasia = nomeFantasia.toUpperCase().trim();
		}
		else
			this.nomeFantasia = null;
			
		
	}
		
	public String getRazaoSocial() {
		String retorno = null;
		
		if (razaoSocial != null)
			retorno = razaoSocial.toUpperCase().trim();
		return retorno;
	}
	
	public void setRazaoSocial(String razaoSocial) {
		if (razaoSocial != null)
		{
			this.razaoSocial = razaoSocial.toUpperCase().trim();
		}
		else
			this.razaoSocial = null;
			
		
	}
		
	public String getCnpjCpf() {
		String retorno = null;
		
		if (cnpjCpf != null)
			retorno = cnpjCpf.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setCnpjCpf(String cnpjCpf) {
		if (cnpjCpf != null)
		{
			this.cnpjCpf = cnpjCpf.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.cnpjCpf = null;
			
		
	}
		
	public String getInscricaoEstadualRg() {
		String retorno = null;
		
		if (inscricaoEstadualRg != null)
			retorno = inscricaoEstadualRg.toUpperCase().trim();
		return retorno;
	}
	
	public void setInscricaoEstadualRg(String inscricaoEstadualRg) {
		if (inscricaoEstadualRg != null)
		{
			this.inscricaoEstadualRg = inscricaoEstadualRg.toUpperCase().trim();
		}
		else
			this.inscricaoEstadualRg = null;
			
		
	}
		
	public String getEnderecoTransportadora() {
		String retorno = null;
		
		if (enderecoTransportadora != null)
			retorno = enderecoTransportadora.toUpperCase().trim();
		return retorno;
	}
	
	public void setEnderecoTransportadora(String enderecoTransportadora) {
		if (enderecoTransportadora != null)
		{
			this.enderecoTransportadora = enderecoTransportadora.toUpperCase().trim();
		}
		else
			this.enderecoTransportadora = null;
			
		
	}
		
	public String getComplementoEndereco() {
		String retorno = null;
		
		if (complementoEndereco != null)
			retorno = complementoEndereco.toUpperCase().trim();
		return retorno;
	}
	
	public void setComplementoEndereco(String complementoEndereco) {
		if (complementoEndereco != null)
		{
			this.complementoEndereco = complementoEndereco.toUpperCase().trim();
		}
		else
			this.complementoEndereco = null;
			
		
	}
		
	public String getBairroTransportadora() {
		String retorno = null;
		
		if (bairroTransportadora != null)
			retorno = bairroTransportadora.toUpperCase().trim();
		return retorno;
	}
	
	public void setBairroTransportadora(String bairroTransportadora) {
		if (bairroTransportadora != null)
		{
			this.bairroTransportadora = bairroTransportadora.toUpperCase().trim();
		}
		else
			this.bairroTransportadora = null;
			
		
	}
		
	public String getCepTransportadora() {
		String retorno = null;
		
		if (cepTransportadora != null)
			retorno = cepTransportadora.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setCepTransportadora(String cepTransportadora) {
		if (cepTransportadora != null)
		{
			this.cepTransportadora = cepTransportadora.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.cepTransportadora = null;
			
		
	}
		
	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	
	public String getContato() {
		String retorno = null;
		
		if (contato != null)
			retorno = contato.toUpperCase().trim();
		return retorno;
	}
	
	public void setContato(String contato) {
		if (contato != null)
		{
			this.contato = contato.toUpperCase().trim();
		}
		else
			this.contato = null;
			
		
	}
		
	public String getDddTelefoneTransportadora() {
		String retorno = null;
		
		if (dddTelefoneTransportadora != null)
			retorno = dddTelefoneTransportadora.toUpperCase().trim();
		return retorno;
	}
	
	public void setDddTelefoneTransportadora(String dddTelefoneTransportadora) {
		if (dddTelefoneTransportadora != null)
		{
			this.dddTelefoneTransportadora = dddTelefoneTransportadora.toUpperCase().trim();
		}
		else
			this.dddTelefoneTransportadora = null;
			
		
	}
		
	public String getTelefone() {
		String retorno = null;
		
		if (telefone != null)
			retorno = telefone.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setTelefone(String telefone) {
		if (telefone != null)
		{
			this.telefone = telefone.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.telefone = null;
			
		
	}
		
	public String getDddFaxTransportadora() {
		String retorno = null;
		
		if (dddFaxTransportadora != null)
			retorno = dddFaxTransportadora.toUpperCase().trim();
		return retorno;
	}
	
	public void setDddFaxTransportadora(String dddFaxTransportadora) {
		if (dddFaxTransportadora != null)
		{
			this.dddFaxTransportadora = dddFaxTransportadora.toUpperCase().trim();
		}
		else
			this.dddFaxTransportadora = null;
			
		
	}
		
	public String getFaxTransportadora() {
		String retorno = null;
		
		if (faxTransportadora != null)
			retorno = faxTransportadora.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setFaxTransportadora(String faxTransportadora) {
		if (faxTransportadora != null)
		{
			this.faxTransportadora = faxTransportadora.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.faxTransportadora = null;
			
		
	}
		
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	public String getComentario() {
		String retorno = null;
		
		if (comentario != null)
			retorno = comentario.toUpperCase().trim();
		return retorno;
	}
	
	public void setComentario(String comentario) {
		if (comentario != null)
		{
			this.comentario = comentario.toUpperCase().trim();
		}
		else
			this.comentario = null;
			
		
	}
		
	public String getEmailTransportadora() {
		String retorno = null;
		
		if (emailTransportadora != null)
			retorno = emailTransportadora.toUpperCase().trim();
		return retorno;
	}
	
	public void setEmailTransportadora(String emailTransportadora) {
		if (emailTransportadora != null)
		{
			this.emailTransportadora = emailTransportadora.toUpperCase().trim();
		}
		else
			this.emailTransportadora = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
