package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="impostoempresa")
public class ImpostoEmpresa extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idImpostoEmpresa")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	
	@Column(name="impostoMunicipal")
	private Double impostoMunicipal;
	
	@Column(name="impostoEstadual")
	private Double impostoEstadual;
	
	@Column(name="impostoFederal")
	private Double impostoFederal;
	

	public ImpostoEmpresa()
	{
		super();
	}
	

	public ImpostoEmpresa
	(
		EmpresaFisica empresaFisica,
		Double impostoMunicipal,
		Double impostoEstadual,
		Double impostoFederal
	) 
	{
		super();
		this.empresaFisica = empresaFisica;
		this.impostoMunicipal = impostoMunicipal;
		this.impostoEstadual = impostoEstadual;
		this.impostoFederal = impostoFederal;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public Double getImpostoMunicipal() {
		return impostoMunicipal;
	}

	public void setImpostoMunicipal(Double impostoMunicipal) {
	
		if (impostoMunicipal != null && Double.isNaN(impostoMunicipal))
		{
			this.impostoMunicipal = null;
		}
		else
		{
			this.impostoMunicipal = impostoMunicipal;
		}
		
	}	
	public Double getImpostoEstadual() {
		return impostoEstadual;
	}

	public void setImpostoEstadual(Double impostoEstadual) {
	
		if (impostoEstadual != null && Double.isNaN(impostoEstadual))
		{
			this.impostoEstadual = null;
		}
		else
		{
			this.impostoEstadual = impostoEstadual;
		}
		
	}	
	public Double getImpostoFederal() {
		return impostoFederal;
	}

	public void setImpostoFederal(Double impostoFederal) {
	
		if (impostoFederal != null && Double.isNaN(impostoFederal))
		{
			this.impostoFederal = null;
		}
		else
		{
			this.impostoFederal = impostoFederal;
		}
		
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
