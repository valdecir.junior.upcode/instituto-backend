package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="logduplicata")


public class LogDuplicata extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idLogDuplicata")
	private Integer id;
		
	@Column(name="motivo")
	private String motivo;
	
	@Column(name="data")
	private Date data;
	
	@Column(name="valorOriginal")
	private String valorOriginal;
	
	@Column(name="valorAlterado")
	private String valorAlterado;
	
	@Column(name="nomeUsuario")
	private String nomeUsuario;
	
	@Column(name="nomeClasse")
	private String nomeClasse;
	@Column(name="idDocumentoOrigem")
	private String idDocumentoOrigem;
	@Column(name="idCliente")
	private Integer idCliente;
	@Column(name="idEmpresaFisica")
	private Integer idEmpresaFisica;
	public LogDuplicata()
	{
		super();
	}
	

	public LogDuplicata
	(
		String motivo,
		Date data,
		String valorOriginal,
		String valorAlterado,
		String nomeUsuario,
		String nomeClasse,
		String idDocumentoOrigem,
		Integer idCliente,
		Integer idEmpresaFisica
	) 
	{
		super();
		this.motivo = motivo;
		this.data = data;
		this.valorOriginal = valorOriginal;
		this.valorAlterado = valorAlterado;
		this.nomeUsuario = nomeUsuario;
		this.nomeClasse =  nomeClasse;
		this.idDocumentoOrigem = idDocumentoOrigem;
		this.idCliente = idCliente;
		this.idEmpresaFisica = idEmpresaFisica;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getMotivo() {
		String retorno = null;
		
		if (motivo != null)
			retorno = motivo.toUpperCase().trim();
		return retorno;
	}
	
	public void setMotivo(String motivo) {
		if (motivo != null)
		{
			this.motivo = motivo.toUpperCase().trim();
		}
		else
			this.motivo = null;
			
		
	}
		
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	public String getValorOriginal() {
		String retorno = null;
		
		if (valorOriginal != null)
			retorno = valorOriginal.toUpperCase().trim();
		return retorno;
	}
	
	public void setValorOriginal(String valorOriginal) {
		if (valorOriginal != null)
		{
			this.valorOriginal = valorOriginal.toUpperCase().trim();
		}
		else
			this.valorOriginal = null;
			
		
	}
		
	public String getValorAlterado() {
		String retorno = null;
		
		if (valorAlterado != null)
			retorno = valorAlterado.toUpperCase().trim();
		return retorno;
	}
	
	public void setValorAlterado(String valorAlterado) {
		if (valorAlterado != null)
		{
			this.valorAlterado = valorAlterado.toUpperCase().trim();
		}
		else
			this.valorAlterado = null;
			
		
	}
		
	public String getNomeUsuario() {
		String retorno = null;
		
		if (nomeUsuario != null)
			retorno = nomeUsuario.toUpperCase().trim();
		return retorno;
	}
	
	public void setNomeUsuario(String nomeUsuario) {
		if (nomeUsuario != null)
		{
			this.nomeUsuario = nomeUsuario.toUpperCase().trim();
		}
		else
			this.nomeUsuario = null;
			
		
	}
		
	public String getNomeClasse() {
		return nomeClasse;
	}


	public void setNomeClasse(String nomeClasse) {
		this.nomeClasse = nomeClasse;
	}


	public String getIdDocumentoOrigem() {
		return idDocumentoOrigem;
	}


	public void setIdDocumentoOrigem(String idDocumentoOrigem) {
		this.idDocumentoOrigem = idDocumentoOrigem;
	}


	public Integer getIdCliente() {
		return idCliente;
	}


	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}


	public Integer getIdEmpresaFisica() {
		return idEmpresaFisica;
	}


	public void setIdEmpresaFisica(Integer idEmpresaFisica) {
		this.idEmpresaFisica = idEmpresaFisica;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
