package br.com.desenv.nepalign.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name = "contagembalancounificada")
public class ContagemBalancoUnificada extends GenericModelIGN {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idContagemBalancoUnificada")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;

	@Column(name = "sequenciaBase")
	private Integer sequenciaBase;

	@Column(name = "itemContagem")
	private Integer itemContagem;

	@ManyToOne
	@JoinColumn(name = "idEstoqueProduto")
	private EstoqueProduto estoqueProduto;

	@Column(name = "quantidade")
	private Integer quantidade;
	
	@Column(name = "dataBalanco")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataBalanco;

	public ContagemBalancoUnificada() {
		super();
	}

	public ContagemBalancoUnificada(EmpresaFisica empresaFisica, Integer sequenciaBase, Integer itemContagem, EstoqueProduto estoqueProduto, Integer quantidade, Date dataBalanco) {
		super();
		this.empresaFisica = empresaFisica;
		this.sequenciaBase = sequenciaBase;
		this.itemContagem = itemContagem;
		this.estoqueProduto = estoqueProduto;
		this.quantidade = quantidade;
		this.dataBalanco = dataBalanco;
	}

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public EmpresaFisica getEmpresaFisica() {
		return this.empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}

	public Integer getSequenciaBase() {
		return this.sequenciaBase;
	}

	public void setSequenciaBase(Integer sequenciaBase) {
		this.sequenciaBase = sequenciaBase;
	}

	public Integer getItemContagem() {
		return this.itemContagem;
	}

	public void setItemContagem(Integer itemContagem) {
		this.itemContagem = itemContagem;
	}

	public EstoqueProduto getEstoqueProduto() {
		return this.estoqueProduto;
	}

	public void setEstoqueProduto(EstoqueProduto estoqueProduto) {
		this.estoqueProduto = estoqueProduto;
	}

	public Integer getQuantidade() {
		return this.quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Date getDataBalanco() {
		return dataBalanco;
	}

	public void setDataBalanco(Date dataBalanco) {
		this.dataBalanco = dataBalanco;
	}

	@Override
	public void validate() throws Exception {
	}

}
