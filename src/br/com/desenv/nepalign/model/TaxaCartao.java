package br.com.desenv.nepalign.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="taxacartao")
public class TaxaCartao extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idTaxaCartao")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "idEmpresaFinanceiro")
	private EmpresaFinanceiro empresaFinanceiro;
	
	@ManyToOne
	@JoinColumn(name = "idFormaPagamentoCartao")
	private FormaPagamentoCartao formaPagamentoCartao;
	
	@Column(name="parcela")
	private Integer parcela;
		
	@Column(name="taxa")
	private Double taxa;
	
	@Column(name="dataVigencia")
	private Date dataVigencia;
	
	@Transient
	private boolean vigente;
	
	public TaxaCartao()
	{
		super();
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}	
	
	public EmpresaFinanceiro getEmpresaFinanceiro() 
	{
		return empresaFinanceiro;
	}

	public void setEmpresaFinanceiro(EmpresaFinanceiro empresaFinanceiro) 
	{
		this.empresaFinanceiro = empresaFinanceiro;
	}

	public Double getTaxa() 
	{
		return taxa;
	}

	public void setTaxa(Double taxa) 
	{
		if (taxa != null && Double.isNaN(taxa))
			this.taxa = null;
		else
			this.taxa = taxa;	
	}	
	
	public Integer getParcela() 
	{
		return parcela;
	}

	public void setParcela(Integer parcela) 
	{
		this.parcela = parcela;
	}
	
	public FormaPagamentoCartao getFormaPagamentoCartao() 
	{
		return formaPagamentoCartao;
	}

	public void setFormaPagamentoCartao(FormaPagamentoCartao formaPagamentoCartao) 
	{
		this.formaPagamentoCartao = formaPagamentoCartao;
	}
	
	public Date getDataVigencia()
	{
		return dataVigencia;
	}
	
	public void setDataVigencia(Date dataVigencia)
	{
		this.dataVigencia = dataVigencia;
	}
	
	public boolean getVigente()
	{
		return vigente;
	} 
	
	public void setVigente(boolean vigente)
	{
		this.vigente = vigente;
	}

	@Override
	public void validate() throws Exception
	{

	}
}