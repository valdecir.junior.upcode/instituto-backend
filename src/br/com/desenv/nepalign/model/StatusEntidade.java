package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="statusentidade")


public class StatusEntidade extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idStatusEntidade")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="permiteConsignacao")
	private String permiteConsignacao;
	
	@Column(name="permiteEmprestimo")
	private String permiteEmprestimo;
	
	@Column(name="permitePagamentoParcelado")
	private String permitePagamentoParcelado;
	
	@Column(name="permitePagamentoCartaoCred")
	private String permitePagamentoCartaoCred;
	
	@Column(name="permitePagamentoCheque")
	private String permitePagamentoCheque;
	
	@Column(name="bloquearVenda")
	private String bloquearVenda;
	

	public StatusEntidade()
	{
		super();
	}
	

	public StatusEntidade
	(
		String descricao,
		String permiteConsignacao,
		String permiteEmprestimo,
		String permitePagamentoParcelado,
		String permitePagamentoCartaoCred,
		String permitePagamentoCheque,
		String bloquearVenda
	) 
	{
		super();
		this.descricao = descricao;
		this.permiteConsignacao = permiteConsignacao;
		this.permiteEmprestimo = permiteEmprestimo;
		this.permitePagamentoParcelado = permitePagamentoParcelado;
		this.permitePagamentoCartaoCred = permitePagamentoCartaoCred;
		this.permitePagamentoCheque = permitePagamentoCheque;
		this.bloquearVenda = bloquearVenda;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public String getPermiteConsignacao() {
		String retorno = null;
		
		if (permiteConsignacao != null)
			retorno = permiteConsignacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setPermiteConsignacao(String permiteConsignacao) {
		if (permiteConsignacao != null)
		{
			this.permiteConsignacao = permiteConsignacao.toUpperCase().trim();
		}
		else
			this.permiteConsignacao = null;
			
		
	}
		
	public String getPermiteEmprestimo() {
		String retorno = null;
		
		if (permiteEmprestimo != null)
			retorno = permiteEmprestimo.toUpperCase().trim();
		return retorno;
	}
	
	public void setPermiteEmprestimo(String permiteEmprestimo) {
		if (permiteEmprestimo != null)
		{
			this.permiteEmprestimo = permiteEmprestimo.toUpperCase().trim();
		}
		else
			this.permiteEmprestimo = null;
			
		
	}
		
	public String getPermitePagamentoParcelado() {
		String retorno = null;
		
		if (permitePagamentoParcelado != null)
			retorno = permitePagamentoParcelado.toUpperCase().trim();
		return retorno;
	}
	
	public void setPermitePagamentoParcelado(String permitePagamentoParcelado) {
		if (permitePagamentoParcelado != null)
		{
			this.permitePagamentoParcelado = permitePagamentoParcelado.toUpperCase().trim();
		}
		else
			this.permitePagamentoParcelado = null;
			
		
	}
		
	public String getPermitePagamentoCartaoCred() {
		String retorno = null;
		
		if (permitePagamentoCartaoCred != null)
			retorno = permitePagamentoCartaoCred.toUpperCase().trim();
		return retorno;
	}
	
	public void setPermitePagamentoCartaoCred(String permitePagamentoCartaoCred) {
		if (permitePagamentoCartaoCred != null)
		{
			this.permitePagamentoCartaoCred = permitePagamentoCartaoCred.toUpperCase().trim();
		}
		else
			this.permitePagamentoCartaoCred = null;
			
		
	}
		
	public String getPermitePagamentoCheque() {
		String retorno = null;
		
		if (permitePagamentoCheque != null)
			retorno = permitePagamentoCheque.toUpperCase().trim();
		return retorno;
	}
	
	public void setPermitePagamentoCheque(String permitePagamentoCheque) {
		if (permitePagamentoCheque != null)
		{
			this.permitePagamentoCheque = permitePagamentoCheque.toUpperCase().trim();
		}
		else
			this.permitePagamentoCheque = null;
			
		
	}
		
	public String getBloquearVenda() {
		String retorno = null;
		
		if (bloquearVenda != null)
			retorno = bloquearVenda.toUpperCase().trim();
		return retorno;
	}
	
	public void setBloquearVenda(String bloquearVenda) {
		if (bloquearVenda != null)
		{
			this.bloquearVenda = bloquearVenda.toUpperCase().trim();
		}
		else
			this.bloquearVenda = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
