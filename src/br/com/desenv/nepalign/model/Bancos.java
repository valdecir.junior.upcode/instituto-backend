package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="bancos")
public class Bancos extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idBancos")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="idEmpresaBancos")
	private EmpresaBancos empresaBancos;
	
	@ManyToOne
	@JoinColumn(name="idBancosRelacionado")
	private Bancos bancoRelacionado;
	
	@Column(name="nomeBanco")
	private String nomeBanco;
	
	@Column(name="mesAberto")
	private Integer mesAberto;
	
	@Column(name="anoAberto")
	private Integer anoAberto;
	
	public Bancos() { super(); }

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}	
	
	public EmpresaBancos getEmpresaBancos() 
	{
		return empresaBancos;
	}

	public void setEmpresaBancos(EmpresaBancos empresaBancos) 
	{
		this.empresaBancos = empresaBancos;
	}

	public Bancos getBancoRelacionado() 
	{
		return bancoRelacionado;
	}

	public void setBancoRelacionado(Bancos bancoRelacionado) 
	{
		this.bancoRelacionado = bancoRelacionado;
	}

	public String getNomeBanco() 
	{
		return nomeBanco != null ? nomeBanco.toUpperCase().trim() : null;
	}
	
	public void setNomeBanco(String nomeBanco) 
	{
		this.nomeBanco = nomeBanco != null ? nomeBanco.toUpperCase().trim() : null;
	}
		
	public Integer getMesAberto() 
	{
		return mesAberto;
	}

	public void setMesAberto(Integer mesAberto) 
	{
		this.mesAberto = (mesAberto == null || Double.isNaN(mesAberto)) ? null : mesAberto;
	}

	public Integer getAnoAberto() 
	{
		return anoAberto;
	}

	public void setAnoAberto(Integer anoAberto) 
	{
		this.anoAberto = (anoAberto == null || Double.isNaN(anoAberto)) ? null : anoAberto;
	}

	@Override
	public void validate() throws Exception { }
}