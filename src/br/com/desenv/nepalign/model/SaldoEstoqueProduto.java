package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.io.Serializable;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="saldoestoqueproduto")


public class SaldoEstoqueProduto extends GenericModelIGN implements Cloneable, Serializable
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idSaldoEstoqueProduto")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idEstoqueProduto")
	private EstoqueProduto estoqueProduto;
	@ManyToOne
	@JoinColumn(name = "idSituacaoEnvioLV")
	private SituacaoEnvioLV situacaoEnvioLV;
	@Column(name="mes")
	private Integer mes;
	
	@Column(name="ano")
	private Integer ano;
	
	@Column(name="saldo")
	private Double saldo;
	
	@Column(name="venda")
	private Double venda;
	
	
	//esse é o custo referencia - Dependendo da empresa pode ser o médio ou o ultimo custo. Ex: Jorrovi: Custo médio, Seralle: Ultimo Custo
	@Column(name="custo")
	private Double custo;
	
	@Column(name="ultimoCusto")
	private Double ultimoCusto;
	
	@Column(name="custoMedio")
	private Double custoMedio;
	
	
	@Column(name="dataUltimaAlteracao")
	private Date dataUltimaAlteracao;
	

	public SaldoEstoqueProduto()
	{
		super();
	}
	

	public SaldoEstoqueProduto
	(
	EmpresaFisica empresaFisica,
	EstoqueProduto estoqueProduto,
	SituacaoEnvioLV situacaoEnvioLV,
		Integer mes,
		Integer ano,
		Double saldo,
		Double venda,
		Double custo,
		Double ultimoCusto,
		Double custoMedio,
		Date dataUltimaAlteracao
	) 
	{
		super();
		this.empresaFisica = empresaFisica;
		this.estoqueProduto = estoqueProduto;
		this.situacaoEnvioLV = situacaoEnvioLV;
		this.mes = mes;
		this.ano = ano;
		this.saldo = saldo;
		this.venda = venda;
		this.dataUltimaAlteracao = dataUltimaAlteracao;
		this.custo = custo;
		this.ultimoCusto = ultimoCusto;
		this.custoMedio = custoMedio;
		
	}

	public Double getCusto() {
		return custo;
	}


	public void setCusto(Double custo) 
	{
		if(custo != null && Double.isNaN(custo))
			this.custo = null;
		else
			this.custo = custo;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
			this.id = id;
	}	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public EstoqueProduto getEstoqueProduto() {
		return estoqueProduto;
	}

	public void setEstoqueProduto(EstoqueProduto estoqueProduto) {
		this.estoqueProduto = estoqueProduto;
	}
	
	public SituacaoEnvioLV getSituacaoEnvioLV() {
		return situacaoEnvioLV;
	}

	public void setSituacaoEnvioLV(SituacaoEnvioLV situacaoEnvioLV) {
		this.situacaoEnvioLV = situacaoEnvioLV;
	}
	
	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}	
	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}	
	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo)
	{
		/*if(saldo != null && saldo == 0.0)
			this.saldo = null;
		else
			this.saldo = saldo;*/
		
		if (saldo!= null && Double.isNaN(saldo))
		{
			this.saldo = null;
		}
		else
		{
			this.saldo = saldo;
		}
				
	}	
	
	public Double getVenda() {
		return venda;
	}

	public void setVenda(Double venda)
	{
		if((venda != null && venda == 0.0) || (venda != null && Double.isNaN(venda)))
			this.venda = null;
		else
			this.venda = venda;
	}	
	public Date getDataUltimaAlteracao() {
		return dataUltimaAlteracao;
	}

	public void setDataUltimaAlteracao(Date dataUltimaAlteracao) {
		this.dataUltimaAlteracao = dataUltimaAlteracao;
	}
	
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}


	public Double getUltimoCusto() 
	{
		return ultimoCusto;
	}


	public void setUltimoCusto(Double ultimoCusto) 
	{	
		if(ultimoCusto != null && Double.isNaN(ultimoCusto))
			this.ultimoCusto = null;
		else
			this.ultimoCusto = custo;		
	}


	public Double getCustoMedio() 
	{
		return custoMedio;
	}


	public void setCustoMedio(Double custoMedio) 
	{
		if(custoMedio != null && Double.isNaN(custoMedio))
			this.custoMedio = null;
		else
			this.custoMedio = custo;
	}
}