package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="itemcondicional")


public class ItemCondicional extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idItemCondicional")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idCondicional")
	private Condicional condicional;
	@ManyToOne
	@JoinColumn(name = "idEstoqueProduto")
	private EstoqueProduto estoqueProduto;
	@Column(name="itemSequencial")
	private Integer itemSequencial;
	
	@Column(name="quantidade")
	private Integer quantidade;
	
	@Column(name="precoTabela")
	private Double precoTabela;
	
	@Column(name="precoVenda")
	private Double precoVenda;
	
	@Column(name="situacao")
	private String situacao;
	

	public ItemCondicional()
	{
		super();
	}
	

	public ItemCondicional
	(
	Condicional condicional,
	EstoqueProduto estoqueProduto,
		Integer itemSequencial,
		Integer quantidade,
		Double precoTabela,
		Double precoVenda,
		String situacao
	) 
	{
		super();
		this.condicional = condicional;
		this.estoqueProduto = estoqueProduto;
		this.itemSequencial = itemSequencial;
		this.quantidade = quantidade;
		this.precoTabela = precoTabela;
		this.precoVenda = precoVenda;
		this.situacao = situacao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Condicional getCondicional() {
		return condicional;
	}

	public void setCondicional(Condicional condicional) {
		this.condicional = condicional;
	}
	
	public EstoqueProduto getEstoqueProduto() {
		return estoqueProduto;
	}

	public void setEstoqueProduto(EstoqueProduto estoqueProduto) {
		this.estoqueProduto = estoqueProduto;
	}
	
	public Integer getItemSequencial() {
		return itemSequencial;
	}

	public void setItemSequencial(Integer itemSequencial) {
		this.itemSequencial = itemSequencial;
	}	
	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}	
	public Double getPrecoTabela() {
		return precoTabela;
	}

	public void setPrecoTabela(Double precoTabela) {
	
		if (precoTabela != null && Double.isNaN(precoTabela))
		{
			this.precoTabela = null;
		}
		else
		{
			this.precoTabela = precoTabela;
		}
		
	}	
	public Double getPrecoVenda() {
		return precoVenda;
	}

	public void setPrecoVenda(Double precoVenda) {
	
		if (precoVenda != null && Double.isNaN(precoVenda))
		{
			this.precoVenda = null;
		}
		else
		{
			this.precoVenda = precoVenda;
		}
		
	}	
	public String getSituacao() {
		String retorno = null;
		
		if (situacao != null)
			retorno = situacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setSituacao(String situacao) {
		if (situacao != null)
		{
			this.situacao = situacao.toUpperCase().trim();
		}
		else
			this.situacao = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
