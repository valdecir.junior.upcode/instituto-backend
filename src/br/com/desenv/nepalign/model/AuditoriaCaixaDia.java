package br.com.desenv.nepalign.model;

public class AuditoriaCaixaDia {

	private CaixaDia caixadia;
	private FormaPagamento formapagamento;
	private Double valorParcial;
	private Double valorInformado;
	private Double valorDivergencia;
	
	
	public AuditoriaCaixaDia() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	
	public CaixaDia getCaixadia() {
		return caixadia;
	}

	public void setCaixadia(CaixaDia caixadia) {				
		this.caixadia = caixadia;
	}

	public FormaPagamento getFormapagamento() {
		return formapagamento;
	}

	public void setFormapagamento(FormaPagamento formapagamento) {
		this.formapagamento = formapagamento;
	}

	public Double getValorParcial() {
		return valorParcial;
	}

	public void setValorParcial(Double valorParcial) {
		this.valorParcial = valorParcial;
	}

	public Double getValorInformado() {
		return valorInformado;
	}

	public void setValorInformado(Double valorInformado) {
		this.valorInformado = valorInformado;
	}

	public Double getValorDivergencia() {
		return valorDivergencia;
	}

	public void setValorDivergencia(Double valorDivergencia) {
		this.valorDivergencia = valorDivergencia;
	}	

}
