package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="avaliacaoatendimento")


public class AvaliacaoAtendimento extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idAvaliacaoAtendimento")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idPedidoVenda")
	private PedidoVenda pedidoVenda;
	@Column(name="nota")
	private Double nota;
	
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@ManyToOne
	@JoinColumn(name = "idDuplicata")
	private Duplicata duplicata;

	public AvaliacaoAtendimento()
	{
		super();
	}
	

	public AvaliacaoAtendimento
	(
	PedidoVenda pedidoVenda,
		Double nota,
	Usuario usuario,
	Duplicata duplicata
	) 
	{
		super();
		this.pedidoVenda = pedidoVenda;
		this.nota = nota;
		this.usuario = usuario;
		this.duplicata = duplicata;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public PedidoVenda getPedidoVenda() {
		return pedidoVenda;
	}

	public void setPedidoVenda(PedidoVenda pedidoVenda) {
		this.pedidoVenda = pedidoVenda;
	}
	
	public Double getNota() {
		return nota;
	}

	public void setNota(Double nota) {
	
		if (nota != null && Double.isNaN(nota))
		{
			this.nota = null;
		}
		else
		{
			this.nota = nota;
		}
		
	}	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Duplicata getDuplicata() {
		return duplicata;
	}

	public void setDuplicata(Duplicata duplicata) {
		this.duplicata = duplicata;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
