package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="atribuicaocaractfiscais")


public class AtribuicaoCaractFiscais extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idAtribuicaoCaractFiscais")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idCaracteristicasFiscais")
	private CaracteristicasFiscais caracteristicasFiscais;
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@ManyToOne
	@JoinColumn(name = "idProduto")
	private Produto produto;
	@ManyToOne
	@JoinColumn(name = "idAtividade")
	private Atividade atividade;
	@Column(name="codigoFiscal")
	private String codigoFiscal;
	
	@Column(name="tipo")
	private Integer tipo;
	
	@Column(name="excecoes")
	private String excecoes;
	
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idNaturezaFornecedor")
	private NaturezaFornecedor naturezaFornecedor;
	@Column(name="ativo")
	private Integer ativo;
	
	@ManyToOne
	@JoinColumn(name = "idCategoria")
	private CategoriaFiscal categoria;
	@ManyToOne
	@JoinColumn(name = "idGrupoCliente")
	private GrupoCliente grupoCliente;
	@ManyToOne
	@JoinColumn(name = "idCaracteristicaProduto")
	private CaracteristicaProduto caracteristicaProduto;
	@ManyToOne
	@JoinColumn(name = "idTipoNotaFiscal")
	private TipoNotaFiscal tipoNotaFiscal;

	public AtribuicaoCaractFiscais()
	{
		super();
	}
	

	public AtribuicaoCaractFiscais
	(
	CaracteristicasFiscais caracteristicasFiscais,
	Cliente cliente,
	Produto produto,
	Atividade atividade,
		String codigoFiscal,
		Integer tipo,
		String excecoes,
	EmpresaFisica empresaFisica,
	NaturezaFornecedor naturezaFornecedor,
		Integer ativo,
	CategoriaFiscal categoria,
	GrupoCliente grupoCliente,
	CaracteristicaProduto caracteristicaProduto,
	TipoNotaFiscal tipoNotaFiscal
	) 
	{
		super();
		this.caracteristicasFiscais = caracteristicasFiscais;
		this.cliente = cliente;
		this.produto = produto;
		this.atividade = atividade;
		this.codigoFiscal = codigoFiscal;
		this.tipo = tipo;
		this.excecoes = excecoes;
		this.empresaFisica = empresaFisica;
		this.naturezaFornecedor = naturezaFornecedor;
		this.ativo = ativo;
		this.categoria = categoria;
		this.grupoCliente = grupoCliente;
		this.caracteristicaProduto = caracteristicaProduto;
		this.tipoNotaFiscal = tipoNotaFiscal;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public CaracteristicasFiscais getCaracteristicasFiscais() {
		return caracteristicasFiscais;
	}

	public void setCaracteristicasFiscais(CaracteristicasFiscais caracteristicasFiscais) {
		this.caracteristicasFiscais = caracteristicasFiscais;
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}
	
	public String getCodigoFiscal() {
		String retorno = null;
		
		if (codigoFiscal != null)
			retorno = codigoFiscal.toUpperCase().trim();
		return retorno;
	}
	
	public void setCodigoFiscal(String codigoFiscal) {
		if (codigoFiscal != null)
		{
			this.codigoFiscal = codigoFiscal.toUpperCase().trim();
		}
		else
			this.codigoFiscal = null;
			
		
	}
		
	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}	
	public String getExcecoes() {
		String retorno = null;
		
		if (excecoes != null)
			retorno = excecoes.toUpperCase().trim();
		return retorno;
	}
	
	public void setExcecoes(String excecoes) {
		if (excecoes != null)
		{
			this.excecoes = excecoes.toUpperCase().trim();
		}
		else
			this.excecoes = null;
			
		
	}
		
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public NaturezaFornecedor getNaturezaFornecedor() {
		return naturezaFornecedor;
	}

	public void setNaturezaFornecedor(NaturezaFornecedor naturezaFornecedor) {
		this.naturezaFornecedor = naturezaFornecedor;
	}
	
	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}	
	public CategoriaFiscal getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaFiscal categoria) {
		this.categoria = categoria;
	}
	
	public GrupoCliente getGrupoCliente() {
		return grupoCliente;
	}

	public void setGrupoCliente(GrupoCliente grupoCliente) {
		this.grupoCliente = grupoCliente;
	}
	
	public CaracteristicaProduto getCaracteristicaProduto() {
		return caracteristicaProduto;
	}

	public void setCaracteristicaProduto(CaracteristicaProduto caracteristicaProduto) {
		this.caracteristicaProduto = caracteristicaProduto;
	}
	
	public TipoNotaFiscal getTipoNotaFiscal() {
		return tipoNotaFiscal;
	}

	public void setTipoNotaFiscal(TipoNotaFiscal tipoNotaFiscal) {
		this.tipoNotaFiscal = tipoNotaFiscal;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
