package br.com.desenv.nepalign.model;

import java.math.BigDecimal;

public class ItemRelatorioResumoCrediario {

	private String tipo;
	private String idEmpresa;
	private String razaoSocial;
	private Integer tempo_atrazo;
	private BigDecimal totalValorParcela;
	private Integer idEmpresaFisica;
	public ItemRelatorioResumoCrediario(){};
	public ItemRelatorioResumoCrediario(String tipo, String idEmpresa,
			String razaoSocial, Integer tempo_atrazo,
			BigDecimal totalValorParcela, Integer idEmpresaFisica) {
		super();
		this.tipo = tipo;
		this.idEmpresa = idEmpresa;
		this.razaoSocial = razaoSocial;
		this.tempo_atrazo = tempo_atrazo;
		this.totalValorParcela = totalValorParcela;
		this.idEmpresaFisica = idEmpresaFisica;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public Integer getTempo_atrazo() {
		return tempo_atrazo;
	}
	public void setTempo_atrazo(Integer tempo_atrazo) {
		this.tempo_atrazo = tempo_atrazo;
	}
	public BigDecimal getTotalValorParcela() {
		return totalValorParcela;
	}
	public void setTotalValorParcela(BigDecimal totalValorParcela) {
		this.totalValorParcela = totalValorParcela;
	}
	public Integer getIdEmpresaFisica() {
		return idEmpresaFisica;
	}
	public void setIdEmpresaFisica(Integer idEmpresaFisica) {
		this.idEmpresaFisica = idEmpresaFisica;
	}


}
