package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="clientebeneficiario")


public class ClienteBeneficiario extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idClienteBeneficiario")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@Column(name="localTrabalho")
	private String localTrabalho;
	
	@Column(name="nivelFixo")
	private String nivelFixo;
	
	@Column(name="nome")
	private String nome;
	
	@Column(name="salario")
	private Double salario;
	
	@Column(name="telefone")
	private String telefone;
	
	@Column(name="temposervico")
	private Date temposervico;
	

	public ClienteBeneficiario()
	{
		super();
	}
	

	public ClienteBeneficiario
	(
	Cliente cliente,
		String localTrabalho,
		String nivelFixo,
		String nome,
		Double salario,
		String telefone,
		Date temposervico
	) 
	{
		super();
		this.cliente = cliente;
		this.localTrabalho = localTrabalho;
		this.nivelFixo = nivelFixo;
		this.nome = nome;
		this.salario = salario;
		this.telefone = telefone;
		this.temposervico = temposervico;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public String getLocalTrabalho() {
		String retorno = null;
		
		if (localTrabalho != null)
			retorno = localTrabalho.toUpperCase().trim();
		return retorno;
	}
	
	public void setLocalTrabalho(String localTrabalho) {
		if (localTrabalho != null)
		{
			this.localTrabalho = localTrabalho.toUpperCase().trim();
		}
		else
			this.localTrabalho = null;
			
		
	}
		
	public String getNivelFixo() {
		String retorno = null;
		
		if (nivelFixo != null)
			retorno = nivelFixo.toUpperCase().trim();
		return retorno;
	}
	
	public void setNivelFixo(String nivelFixo) {
		if (nivelFixo != null)
		{
			this.nivelFixo = nivelFixo.toUpperCase().trim();
		}
		else
			this.nivelFixo = null;
			
		
	}
		
	public String getNome() {
		String retorno = null;
		
		if (nome != null)
			retorno = nome.toUpperCase().trim();
		return retorno;
	}
	
	public void setNome(String nome) {
		if (nome != null)
		{
			this.nome = nome.toUpperCase().trim();
		}
		else
			this.nome = null;
			
		
	}
		
	public Double getSalario() {
		return salario;
	}

	public void setSalario(Double salario) {
	
		if (salario != null && Double.isNaN(salario))
		{
			this.salario = null;
		}
		else
		{
			this.salario = salario;
		}
		
	}	
	public String getTelefone() {
		String retorno = null;
		
		if (telefone != null)
			retorno = telefone.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setTelefone(String telefone) {
		if (telefone != null)
		{
			this.telefone = telefone.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.telefone = null;
			
		
	}
		
	public Date getTemposervico() {
		return temposervico;
	}

	public void setTemposervico(Date temposervico) {
		this.temposervico = temposervico;
	}
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
