package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="alertasenhagerente")


public class AlertaSenhaGerente extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idAlertaSenhaGerente")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="liberado")
	private String situacao;
	@ManyToOne
	@JoinColumn(name = "idUsuarioAutorizador")
	private Usuario usuarioAutorizador;
	

	public AlertaSenhaGerente()
	{
		super();
	}
	

	public AlertaSenhaGerente
	(
		String descricao,
		String situacao,
		Usuario usuarioAutorizador
	) 
	{
		super();
		this.descricao = descricao;
		this.situacao = situacao;
		this.usuarioAutorizador = usuarioAutorizador;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public String getSituacao() {
		String retorno = null;
		
		if (situacao != null)
			retorno = situacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setSituacao(String situacao) {
		if (situacao != null)
		{
			this.situacao = situacao.toUpperCase().trim();
		}
		else
			this.situacao = null;
			
		
	}
		
	public Usuario getUsuarioAutorizador() {
		return usuarioAutorizador;
	}


	public void setUsuarioAutorizador(Usuario usuarioAutorizador) {
		this.usuarioAutorizador = usuarioAutorizador;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
