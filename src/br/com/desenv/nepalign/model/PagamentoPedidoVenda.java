package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="pagamentopedidovenda")


public class PagamentoPedidoVenda extends GenericModelIGN implements Cloneable
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idPagamentoPedidoVenda")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idPedidoVenda")
	private PedidoVenda pedidoVenda;
	@ManyToOne
	@JoinColumn(name = "idFormaPagamento")
	private FormaPagamento formaPagamento;
	@ManyToOne
	@JoinColumn(name = "idLancamentoContaReceber")
	private LancamentoContaReceber lancamentoContaReceber;
	@Column(name="dataPagamento")
	private Date dataPagamento;
	@Column(name="chequeDataEmetido")
	private Date chequeDataEmetido;
	@Column(name="valor")
	private Double valor;
	
	@Column(name="numeroDocumento")
	private String numeroDocumento;
	
	@Column(name="numeroCheque")
	private String numeroCheque;
	
	@Column(name="cnpjCpf")
	private String cnpjCpf;
	
	@Column(name="numeroDuplicata")
	private String numeroDuplicata;
	
	@Column(name="situacaoLancamento")
	private Integer situacaoLancamento;
	
	@Column(name="idNotaFiscal")
	private String idNotaFiscal;
	
	@Column(name="cpfCheque")
	private String cpfCheque;
	
	@Column(name="nomeCheque")
	private String nomeCheque;
	
	@Column(name="bancoCheque")
	private String bancoCheque;
	
	@Column(name="agenciaCheque")
	private String agenciaCheque;
	@Column(name="numeroParcelas")
	private Integer numeroParcelas;
	@Column(name="controlePagamentoCartao")
	private String controlePagamentoCartao;
	@Column(name="itemSequencial")
	private String itemSequencial;

	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@ManyToOne
	@JoinColumn(name = "idChequeRecebido")
	private ChequeRecebido chequeRecebido;
	@ManyToOne
	@JoinColumn(name = "idCaixaDia")
	private CaixaDia caixaDia;
	@ManyToOne
	@JoinColumn(name = "idPagamentoCartao")
	private PagamentoCartao pagamentoCartao;
	@Column(name = "entrada")
	private String entrada;
	
	@ManyToOne
	@JoinColumn(name = "idOperadoraCartao")
	private OperadoraCartao operadoraCartao;
	
	@Column(name = "valorAjustado")
	private Double valorAjustado;
	
	public Double getValorAjustado() {
		return valorAjustado;
	}

	public void setValorAjustado(Double valorAjustado) {
		this.valorAjustado = valorAjustado;
	}


	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


	public String getItemSequencial() {
		return itemSequencial;
	}


	public void setItemSequencial(String itemSequencial) {
		this.itemSequencial = itemSequencial;
	}


	public PagamentoPedidoVenda()
	{
		super();
	}
	

	public PagamentoPedidoVenda
	(
	PedidoVenda pedidoVenda,
	FormaPagamento formaPagamento,
	LancamentoContaReceber lancamentoContaReceber,
		Date dataPagamento,
		Double valor,
		String numeroDocumento,
		String numeroCheque,
		String cnpjCpf,
		String numeroDuplicata,
		Integer situacaoLancamento,
		String idNotaFiscal,
		String cpfCheque,
		String nomeCheque,
		String bancoCheque,
		String agenciaCheque,
		String controlePagamentoCartao,
		ChequeRecebido chequeRecebido,
		CaixaDia caixaDia,
		Integer numeroParcelas,
		 PagamentoCartao pagamentoCartao,
		 String entrada,
		 OperadoraCartao operadoraCartao
	) 
	{
		super();
		this.pedidoVenda = pedidoVenda;
		this.formaPagamento = formaPagamento;
		this.lancamentoContaReceber = lancamentoContaReceber;
		this.dataPagamento = dataPagamento;
		this.valor = valor;
		this.numeroDocumento = numeroDocumento;
		this.numeroCheque = numeroCheque;
		this.cnpjCpf = cnpjCpf;
		this.numeroDuplicata = numeroDuplicata;
		this.situacaoLancamento = situacaoLancamento;
		this.idNotaFiscal = idNotaFiscal;
		this.cpfCheque = cpfCheque;
		this.nomeCheque = nomeCheque;
		this.bancoCheque = bancoCheque;
		this.agenciaCheque = agenciaCheque;
		this.controlePagamentoCartao = controlePagamentoCartao;
		this.chequeRecebido = chequeRecebido;
		this.caixaDia =caixaDia;
		this.numeroParcelas = numeroParcelas;
		this.pagamentoCartao = pagamentoCartao;
		this.entrada = entrada;
		this.operadoraCartao = operadoraCartao;
	}

	public Date getChequeDataEmetido() {
		return chequeDataEmetido;
	}


	public Integer getNumeroParcelas() {
		return numeroParcelas;
	}


	public void setNumeroParcelas(Integer numeroParcelas) 
	{
		this.numeroParcelas = numeroParcelas;
	}


	public void setChequeDataEmetido(Date chequeDataEmetido) {
		this.chequeDataEmetido = chequeDataEmetido;
	}


	public ChequeRecebido getChequeRecebido() {
		return chequeRecebido;
	}


	public String getEntrada() {
		return entrada;
	}


	public void setEntrada(String entrada) {
		this.entrada = entrada;
	}


	public void setChequeRecebido(ChequeRecebido chequeRecebido) {
		this.chequeRecebido = chequeRecebido;
	}


	public CaixaDia getCaixaDia() {
		return caixaDia;
	}


	public void setCaixaDia(CaixaDia caixaDia) {
		this.caixaDia = caixaDia;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public PedidoVenda getPedidoVenda() {
		return pedidoVenda;
	}

	public PagamentoCartao getPagamentoCartao() {
		return pagamentoCartao;
	}


	public void setPagamentoCartao(PagamentoCartao pagamentoCartao) {
		this.pagamentoCartao = pagamentoCartao;
	}


	public void setPedidoVenda(PedidoVenda pedidoVenda) {
		this.pedidoVenda = pedidoVenda;
	}
	
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
	public LancamentoContaReceber getLancamentoContaReceber() {
		return lancamentoContaReceber;
	}

	public void setLancamentoContaReceber(LancamentoContaReceber lancamentoContaReceber) {
		this.lancamentoContaReceber = lancamentoContaReceber;
	}
	
	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
	
		if (valor != null && Double.isNaN(valor))
		{
			this.valor = null;
		}
		else
		{
			this.valor = valor;
		}
		
	}	
	public String getNumeroDocumento() {
		String retorno = null;
		
		if (numeroDocumento != null)
			retorno = numeroDocumento.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroDocumento(String numeroDocumento) {
		if (numeroDocumento != null)
		{
			this.numeroDocumento = numeroDocumento.toUpperCase().trim();
		}
		else
			this.numeroDocumento = null;
			
		
	}
		
	public String getNumeroCheque() {
		String retorno = null;
		
		if (numeroCheque != null)
			retorno = numeroCheque.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroCheque(String numeroCheque) {
		if (numeroCheque != null)
		{
			this.numeroCheque = numeroCheque.toUpperCase().trim();
		}
		else
			this.numeroCheque = null;
			
		
	}
		
	public String getCnpjCpf() {
		String retorno = null;
		
		if (cnpjCpf != null)
			retorno = cnpjCpf.toUpperCase().trim();
		return retorno;
	}
	
	public void setCnpjCpf(String cnpjCpf) {
		if (cnpjCpf != null)
		{
			this.cnpjCpf = cnpjCpf.toUpperCase().trim();
		}
		else
			this.cnpjCpf = null;
			
		
	}
		
	public String getNumeroDuplicata() {
		String retorno = null;
		
		if (numeroDuplicata != null)
			retorno = numeroDuplicata.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroDuplicata(String numeroDuplicata) {
		if (numeroDuplicata != null)
		{
			this.numeroDuplicata = numeroDuplicata.toUpperCase().trim();
		}
		else
			this.numeroDuplicata = null;
			
		
	}
		
	public Integer getSituacaoLancamento() {
		return situacaoLancamento;
	}

	public void setSituacaoLancamento(Integer situacaoLancamento) {
		this.situacaoLancamento = situacaoLancamento;
	}	
	public String getIdNotaFiscal() {
		String retorno = null;
		
		if (idNotaFiscal != null)
			retorno = idNotaFiscal.toUpperCase().trim();
		return retorno;
	}
	
	public void setIdNotaFiscal(String idNotaFiscal) {
		if (idNotaFiscal != null)
		{
			this.idNotaFiscal = idNotaFiscal.toUpperCase().trim();
		}
		else
			this.idNotaFiscal = null;
			
		
	}
		
	public String getCpfCheque() {
		String retorno = null;
		
		if (cpfCheque != null)
			retorno = cpfCheque.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setCpfCheque(String cpfCheque) {
		if (cpfCheque != null)
		{
			this.cpfCheque = cpfCheque.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.cpfCheque = null;
			
		
	}
		
	public String getNomeCheque() {
		String retorno = null;
		
		if (nomeCheque != null)
			retorno = nomeCheque.toUpperCase().trim();
		return retorno;
	}
	
	public void setNomeCheque(String nomeCheque) {
		if (nomeCheque != null)
		{
			this.nomeCheque = nomeCheque.toUpperCase().trim();
		}
		else
			this.nomeCheque = null;
			
		
	}
		
	public String getBancoCheque() {
		String retorno = null;
		
		if (bancoCheque != null)
			retorno = bancoCheque.toUpperCase().trim();
		return retorno;
	}
	
	public void setBancoCheque(String bancoCheque) {
		if (bancoCheque != null)
		{
			this.bancoCheque = bancoCheque.toUpperCase().trim();
		}
		else
			this.bancoCheque = null;
			
		
	}
		
	public String getAgenciaCheque() {
		String retorno = null;
		
		if (agenciaCheque != null)
			retorno = agenciaCheque.toUpperCase().trim();
		return retorno;
	}
	
	public void setAgenciaCheque(String agenciaCheque) {
		if (agenciaCheque != null)
		{
			this.agenciaCheque = agenciaCheque.toUpperCase().trim();
		}
		else
			this.agenciaCheque = null;
			
		
	}
		
	public String getControlePagamentoCartao() {
		String retorno = null;
		
		if (controlePagamentoCartao != null)
			retorno = controlePagamentoCartao.toUpperCase().trim();
		return retorno;
	}
	
	public void setControlePagamentoCartao(String controlePagamentoCartao) {
		if (controlePagamentoCartao != null)
		{
			this.controlePagamentoCartao = controlePagamentoCartao.toUpperCase().trim();
		}
		else
			this.controlePagamentoCartao = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

	public OperadoraCartao getOperadoraCartao() {
		return operadoraCartao;
	}


	public void setOperadoraCartao(OperadoraCartao operadoraCartao) {
		this.operadoraCartao = operadoraCartao;
	}


	@Override
	public PagamentoPedidoVenda clone() throws CloneNotSupportedException 
	{
		return (PagamentoPedidoVenda) super.clone();
	}
}