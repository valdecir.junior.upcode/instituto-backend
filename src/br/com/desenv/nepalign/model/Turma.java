package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="turma", schema="smartsig")


public class Turma extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idTurma")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="horarioInicial")
	private Date horarioInicial;
	
	@Column(name="horarioFinal")
	private Date horarioFinal;
	
	@Column(name="diaSemana")
	private String diaSemana;
	
	@ManyToOne
	@JoinColumn(name = "idProfessor")
	private Professor professor;
	@ManyToOne
	@JoinColumn(name = "idLocalAtividade")
	private LocalAtividade localAtividade;
	@Column(name="capacidadeAtendidos")
	private Integer capacidadeAtendidos;
	
	@ManyToOne
	@JoinColumn(name = "idAtividadeOferecida")
	private AtividadeOferecida atividadeOferecida;

	public Turma()
	{
		super();
	}
	

	public Turma
	(
		String descricao,
		Date horarioInicial,
		Date horarioFinal,
		String diaSemana,
	Professor professor,
	LocalAtividade localAtividade,
		Integer capacidadeAtendidos,
	AtividadeOferecida atividadeOferecida
	) 
	{
		super();
		this.descricao = descricao;
		this.horarioInicial = horarioInicial;
		this.horarioFinal = horarioFinal;
		this.diaSemana = diaSemana;
		this.professor = professor;
		this.localAtividade = localAtividade;
		this.capacidadeAtendidos = capacidadeAtendidos;
		this.atividadeOferecida = atividadeOferecida;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public Date getHorarioInicial() {
		return horarioInicial;
	}

	public void setHorarioInicial(Date horarioInicial) {
		this.horarioInicial = horarioInicial;
	}
	public Date getHorarioFinal() {
		return horarioFinal;
	}

	public void setHorarioFinal(Date horarioFinal) {
		this.horarioFinal = horarioFinal;
	}
	public String getDiaSemana() {
		String retorno = null;
		
		if (diaSemana != null)
			retorno = diaSemana.toUpperCase().trim();
		return retorno;
	}
	
	public void setDiaSemana(String diaSemana) {
		if (diaSemana != null)
		{
			this.diaSemana = diaSemana.toUpperCase().trim();
		}
		else
			this.diaSemana = null;
			
		
	}
		
	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}
	
	public LocalAtividade getLocalAtividade() {
		return localAtividade;
	}

	public void setLocalAtividade(LocalAtividade localAtividade) {
		this.localAtividade = localAtividade;
	}
	
	public Integer getCapacidadeAtendidos() {
		return capacidadeAtendidos;
	}

	public void setCapacidadeAtendidos(Integer capacidadeAtendidos) {
		if (capacidadeAtendidos != null && capacidadeAtendidos == 0)
		{
			this.capacidadeAtendidos = null;
		}
		else
		{
			this.capacidadeAtendidos = capacidadeAtendidos;
		}
	}	
	public AtividadeOferecida getAtividadeOferecida() {
		return atividadeOferecida;
	}

	public void setAtividadeOferecida(AtividadeOferecida atividadeOferecida) {
		this.atividadeOferecida = atividadeOferecida;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
