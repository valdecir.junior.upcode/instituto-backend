package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="atividadeatendido")


public class AtividadeAtendido extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idAtividadeAtendido")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idAtendido")
	private Atendido atendido;
	@ManyToOne
	@JoinColumn(name = "idAtividadeOferecida")
	private AtividadeOferecida atividadeOferecida;
	@Column(name="dataInicio")
	private Date dataInicio;
	
	@Column(name="dataFim")
	private Date dataFim;
	
	@Column(name="periodoPreferencial")
	private String periodoPreferencial;
	

	public AtividadeAtendido()
	{
		super();
	}
	

	public AtividadeAtendido
	(
	Atendido atendido,
	AtividadeOferecida atividadeOferecida,
		Date dataInicio,
		Date dataFim,
		String periodoPreferencial
	) 
	{
		super();
		this.atendido = atendido;
		this.atividadeOferecida = atividadeOferecida;
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
		this.periodoPreferencial = periodoPreferencial;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public Atendido getAtendido() {
		return atendido;
	}

	public void setAtendido(Atendido atendido) {
		this.atendido = atendido;
	}
	
	public AtividadeOferecida getAtividadeOferecida() {
		return atividadeOferecida;
	}

	public void setAtividadeOferecida(AtividadeOferecida atividadeOferecida) {
		this.atividadeOferecida = atividadeOferecida;
	}
	
	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public String getPeriodoPreferencial() {
		String retorno = null;
		
		if (periodoPreferencial != null)
			retorno = periodoPreferencial.toUpperCase().trim();
		return retorno;
	}
	
	public void setPeriodoPreferencial(String periodoPreferencial) {
		if (periodoPreferencial != null)
		{
			this.periodoPreferencial = periodoPreferencial.toUpperCase().trim();
		}
		else
			this.periodoPreferencial = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
