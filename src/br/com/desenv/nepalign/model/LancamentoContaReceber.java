package br.com.desenv.nepalign.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;
import br.com.desenv.frameworkignorante.IgnUtil;

@Entity
@Table(name="lancamentocontareceber")


public class LancamentoContaReceber extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idLancamentoContaReceber")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="idEmpresaFinanceiro")
	private EmpresaFinanceiro empresaFinanceiro;
	
	@ManyToOne
	@JoinColumn(name="idBandeiraCartao")
	private BandeiraCartao bandeiraCartao;
	
	@ManyToOne
	@JoinColumn(name="idTaxaCartao")
	private TaxaCartao taxaCartao;
	
	@ManyToOne
	@JoinColumn(name="idFormaPagamentoCartao")
	private FormaPagamentoCartao formaPagamentoCartao;
	
	@Column(name="dataLancamento")
	private Date dataLancamento;
	
	@Column(name="dataVencimento")
	private Date dataVencimento;
	
	@Column(name="dataQuitacao")
	private Date dataQuitacao;
	
	@Column(name="emitente")
	private String emitente;
	
	@Column(name="cpfCnpjEmitente")
	private String cpfCnpjEmitente;
	
	@ManyToOne
	@JoinColumn(name="idBanco")
	private Banco banco;
	
	@Column(name="numeroDocumento")
	private String numeroDocumento;
	
	@Column(name="valorLancamento")
	private Double valorLancamento;
	
	@Column(name="valorParcelaSemDesconto")
	private Double valorParcelaSemDesconto;
	
	@Column(name="percentualCartao")
	private Double percentualCartao;
	
	@Column(name="quantidadeParcelas")
	private Integer quantidadeParcelas;
	
	@Column(name="numeroParcela")
	private Integer numeroParcela;
	
	@Column(name="valorTotal")
	private Double valorTotal;
	
	@ManyToOne
	@JoinColumn(name="idSituacaoLancamento")
	private SituacaoLancamento situacaoLancamento;
	
	@Column(name="observacao")
	private String observacao;

	@Override
	public Integer getId() 
	{
		return this.id;
	}

	@Override
	public void setId(Integer id) 
	{
		if(id == null || Double.isNaN(id) || id == 0x00)
			this.id = null;
		else
			this.id = id;	
	}

	public EmpresaFinanceiro getEmpresaFinanceiro() 
	{
		return empresaFinanceiro;
	}

	public void setEmpresaFinanceiro(EmpresaFinanceiro empresaFinanceiro) 
	{
		this.empresaFinanceiro = empresaFinanceiro;
	}

	public BandeiraCartao getBandeiraCartao() 
	{
		return bandeiraCartao;
	}

	public void setBandeiraCartao(BandeiraCartao bandeiraCartao) 
	{
		this.bandeiraCartao = bandeiraCartao;
	}

	public TaxaCartao getTaxaCartao() 
	{
		return taxaCartao;
	}

	public void setTaxaCartao(TaxaCartao taxaCartao) 
	{
		this.taxaCartao = taxaCartao;
	}

	public FormaPagamentoCartao getFormaPagamentoCartao() 
	{
		return formaPagamentoCartao;
	}

	public void setFormaPagamentoCartao(FormaPagamentoCartao formaPagamentoCartao) 
	{
		this.formaPagamentoCartao = formaPagamentoCartao;
	}

	public Date getDataLancamento() 
	{
		return dataLancamento;
	}

	public void setDataLancamento(Date dataLancamento) 
	{
		this.dataLancamento = dataLancamento;
	}

	public Date getDataVencimento() 
	{
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) 
	{
		this.dataVencimento = dataVencimento;
	}

	public Date getDataQuitacao() 
	{
		return dataQuitacao;
	}

	public void setDataQuitacao(Date dataQuitacao) 
	{
		this.dataQuitacao = dataQuitacao;
	}

	public String getEmitente() 
	{
		return emitente;
	}

	public void setEmitente(String emitente) 
	{
		this.emitente = emitente;
	}

	public String getCpfCnpjEmitente() 
	{
		return cpfCnpjEmitente;
	}

	public void setCpfCnpjEmitente(String cpfCnpjEmitente) 
	{
		this.cpfCnpjEmitente = cpfCnpjEmitente;
	}

	public Banco getBanco() 
	{
		return banco;
	}

	public void setBanco(Banco banco) 
	{
		this.banco = banco;
	}

	public String getNumeroDocumento() 
	{
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) 
	{
		this.numeroDocumento = numeroDocumento;
	}

	public Double getValorLancamento() 
	{
		return valorLancamento;
	}

	public void setValorLancamento(Double valorLancamento) 
	{
		if(valorLancamento == null || Double.isNaN(valorLancamento))
			this.valorLancamento = null;
		else
			this.valorLancamento = valorLancamento;
	}

	public Double getValorParcelaSemDesconto() 
	{
		return valorParcelaSemDesconto;
	}

	public void setValorParcelaSemDesconto(Double valorParcelaSemDesconto) 
	{
		if(valorParcelaSemDesconto == null || Double.isNaN(valorParcelaSemDesconto))
			this.valorParcelaSemDesconto = null;
		else
			this.valorParcelaSemDesconto = valorParcelaSemDesconto;
	}

	public Double getPercentualCartao() 
	{
		return percentualCartao;
	}

	public void setPercentualCartao(Double percentualCartao) 
	{
		if(percentualCartao == null || Double.isNaN(percentualCartao))
			this.percentualCartao = null;
		else
			this.percentualCartao = percentualCartao;
	}

	public Integer getQuantidadeParcelas() 
	{
		return quantidadeParcelas;
	}

	public void setQuantidadeParcelas(Integer quantidadeParcelas) 
	{
		if(quantidadeParcelas == null || quantidadeParcelas == 0x00)
			this.quantidadeParcelas = null;
		else
			this.quantidadeParcelas = quantidadeParcelas;
	}

	public Integer getNumeroParcela() 
	{
		return numeroParcela;
	}

	public void setNumeroParcela(Integer numeroParcela) 
	{
		if(numeroParcela == null || Double.isNaN(numeroParcela) || numeroParcela == 0x00)
			this.numeroParcela = null;
		else
			this.numeroParcela = numeroParcela;
	}

	public Double getValorTotal() 
	{
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) 
	{
		if(valorTotal == null || Double.isNaN(valorTotal))
			this.valorTotal = null;
		else
			this.valorTotal = valorTotal;
	}

	public SituacaoLancamento getSituacaoLancamento() 
	{
		return situacaoLancamento;
	}

	public void setSituacaoLancamento(SituacaoLancamento situacaoLancamento) 
	{
		this.situacaoLancamento = situacaoLancamento;
	}

	public String getObservacao() 
	{
		return observacao;
	}

	public void setObservacao(String observacao) 
	{
		this.observacao = observacao;
	}

	@Override
	public void validate() throws Exception 
	{
		if(getValorLancamento() != null)
			setValorLancamento(IgnUtil.roundUpDecimalPlaces(getValorLancamento(), 0x02));
		
		if(getValorParcelaSemDesconto() != null)
			setValorParcelaSemDesconto(IgnUtil.roundUpDecimalPlaces(getValorParcelaSemDesconto(), 0x02));
		
		if(getValorTotal() != null)
			setValorTotal(IgnUtil.roundUpDecimalPlaces(getValorTotal(), 0x02));
		
		//Validação removida para cheque devolvido
		//Adicionada validação clientside
		/*if(dataVencimento.getTime() < dataLancamento.getTime())
			throw new Exception("Data de Vencimento não pode ser menor que Data de Emissão!");*/
		
		if(empresaFinanceiro.getTipoEmpresa() == EmpresaFinanceiro.TIPOEMPRESA_CHEQUE && banco == null)
			throw new Exception("Lançamento de Cheque exige um Banco!");
		
		if(empresaFinanceiro.getTipoEmpresa() == EmpresaFinanceiro.TIPOEMPRESA_CHEQUE || (banco != null && banco.getCodigo() != 0x00))
			if(IgnUtil.diferencaDias(dataVencimento, dataLancamento) > 360)
				throw new Exception("Cheques não podem ter Data de Vencimento maior que 360 da Data de Emissão!");
		
		if((empresaFinanceiro.getTipoEmpresa() == EmpresaFinanceiro.TIPOEMPRESA_CARTAO || bandeiraCartao != null) && banco == null) // cartao
		{
			if(numeroParcela == null)
				return;
			
			// diferença de vencimento maximo de dias entre as parcelas é 45 dias (2D).
			// ou seja, parcela 2 não pode ter diferenca entre dias de vencimento - emissao com mais de 90 dias (2*45)
			final int diferencaDiasMaxima = numeroParcela * 0x0000002D; 
			
			if(IgnUtil.diferencaDias(dataVencimento, dataLancamento) > diferencaDiasMaxima)
				throw new Exception("Parcelas do Cartão não podem ter Data de Vencimento maior que Numero Parcela x 45 dias da Data de Emissão!");
		}
	}
}