package br.com.desenv.nepalign.model;

public class ItemRelatorioCateiraCobranca 
{
 private String mesAno;
 private Double carteiraInicial;
 private Double obj;
 private Double recebimentoLiquido;
 private Double eficacia;
 private Double abertoInicial;
 private Double recebimentoMesVirgente;
 private Double abertoFinal;
 private Double faltaReceber;
 private Double acordo;
 private Integer idVendedor;
 private String vendedor;
 private Double mediaAritmeticaEficacia;
 
public String getMesAno() {
	return mesAno;
}
public void setMesAno(String mesAno) {
	this.mesAno = mesAno;
}
public Double getCarteiraInicial() {
	return carteiraInicial;
}
public void setCarteiraInicial(Double carteiraInicial) {
	this.carteiraInicial = carteiraInicial;
}
public Double getObj() {
	return obj;
}
public void setObj(Double obj) {
	this.obj = obj;
}
public Double getRecebimentoLiquido() {
	return recebimentoLiquido;
}
public void setRecebimentoLiquido(Double recebimentoLiquido) {
	this.recebimentoLiquido = recebimentoLiquido;
}
public Double getEficacia() {
	return eficacia;
}
public void setEficacia(Double eficacia) {
	this.eficacia = eficacia;
}
public Double getAbertoInicial() {
	return abertoInicial;
}
public void setAbertoInicial(Double abertoInicial) {
	this.abertoInicial = abertoInicial;
}
public Double getRecebimentoMesVirgente() {
	return recebimentoMesVirgente;
}
public void setRecebimentoMesVirgente(Double recebimentoMesVirgente) {
	this.recebimentoMesVirgente = recebimentoMesVirgente;
}
public Double getAbertoFinal() {
	return abertoFinal;
}
public void setAbertoFinal(Double abertoFinal) {
	this.abertoFinal = abertoFinal;
}
public Double getFaltaReceber() {
	return faltaReceber;
}
public void setFaltaReceber(Double faltaReceber) {
	this.faltaReceber = faltaReceber;
}
public Double getAcordo() {
	return acordo;
}
public void setAcordo(Double acordo) {
	this.acordo = acordo;
}
public Integer getIdVendedor() {
	return idVendedor;
}
public void setIdVendedor(Integer idVendedor) {
	this.idVendedor = idVendedor;
}
public String getVendedor() {
	return vendedor;
}
public void setVendedor(String vendedor) {
	this.vendedor = vendedor;
}
public Double getMediaAritmeticaEficacia() {
	return mediaAritmeticaEficacia;
}
public void setMediaAritmeticaEficacia(Double mediaAritmeticaEficacia) {
	this.mediaAritmeticaEficacia = mediaAritmeticaEficacia;
}

 
 
 
 
}
