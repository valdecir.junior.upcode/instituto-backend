package br.com.desenv.nepalign.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="duplicataparcelacobranca")


public class DuplicataParcelaCobranca extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idDuplicataParcelaCobranca")
	private Integer id;
		
	@Column(name="ocorrencia")
	private String ocorrencia;
	
	@Column(name="data")
	private Date data;
	@ManyToOne
	@JoinColumn(name = "idDuplicataParcela")
	private DuplicataParcela duplicataParcela;
	@Column(name="dataImpressao")
	private Date dataImpressao;
	
	public DuplicataParcelaCobranca()
	{
		super();
	}
	

	public DuplicataParcelaCobranca
	(
		String ocorrencia,
		Date data,
		DuplicataParcela duplicataParcela
	) 
	{
		super();
		this.ocorrencia = ocorrencia;
		this.data = data;
		this.duplicataParcela = duplicataParcela;
		
	}

	public DuplicataParcela getDuplicataParcela() {
		return duplicataParcela;
	}


	public void setDuplicataParcela(DuplicataParcela duplicataParcela) {
		this.duplicataParcela = duplicataParcela;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getOcorrencia() {
		String retorno = null;
		
		if (ocorrencia != null)
			retorno = ocorrencia.toUpperCase().trim();
		return retorno;
	}
	
	public void setOcorrencia(String ocorrencia) {
		if (ocorrencia != null)
		{
			this.ocorrencia = ocorrencia.toUpperCase().trim();
		}
		else
			this.ocorrencia = null;
			
		
	}
		
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	public Date getDataImpressao() {
		return dataImpressao;
	}


	public void setDataImpressao(Date dataImpressao) {
		this.dataImpressao = dataImpressao;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
