package br.com.desenv.nepalign.model;

public class FormaPagamentoDuplicataParcela {


	public FormaPagamento forma;
	public Double valor;
	public Double sobra;
	public Integer itemSequincial;
	public ChequeRecebido chequeRecebido ;
	public Integer numeroParcelas;
	public OperadoraCartao operadoraCartao;

	public OperadoraCartao getOperadoraCartao() {
		return operadoraCartao;
	}
	public void setOperadoraCartao(OperadoraCartao operadoraCartao) {
		this.operadoraCartao = operadoraCartao;
	}
	public Integer getNumeroParcelas() {
		return numeroParcelas;
	}
	public void setNumeroParcelas(Integer numeroParcelas) {
		this.numeroParcelas = numeroParcelas;
	}
	public Integer getItemSequincial() {
		return itemSequincial;
	}
	public void setItemSequincial(Integer itemSequincial) {
		this.itemSequincial = itemSequincial;
	}
	public FormaPagamento getForma() {
		return forma;
	}
	public void setForma(FormaPagamento forma) {
		this.forma = forma;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public Double getSobra() {
		return sobra;
	}
	public void setSobra(Double sobra) {
		this.sobra = sobra;
	}
	public ChequeRecebido getChequeRecebido() {
		return chequeRecebido;
	}
	public void setChequeRecebido(ChequeRecebido chequeRecebido) {
		this.chequeRecebido = chequeRecebido;
	}


}
