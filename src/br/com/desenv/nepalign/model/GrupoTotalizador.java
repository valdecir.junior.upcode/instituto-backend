package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="grupototalizador")
public class GrupoTotalizador extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idGrupoTotalizador")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	
	public GrupoTotalizador()
	{
		super();
	}
	

	public GrupoTotalizador(String descricao)
	{
		super();
		this.descricao = descricao;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		if (id != null && id == 0)
			this.id = null;
		else
			this.id = id;
	}	
	
	public String getDescricao() 
	{
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setDescricao(String descricao) 
	{
		if (descricao != null)
			this.descricao = descricao.toUpperCase().trim();
		else
			this.descricao = null;
	}

	@Override
	public void validate() throws Exception
	{
	}
	
	@Override
	public boolean equals(Object object) 
	{
		if(object == null)
			return false;
		if(!object.getClass().equals(this.getClass()))
			return false;
		
		if(((GrupoTotalizador) object).getDescricao().equals(getDescricao()))
			return true;
		
		return super.equals(object);
	}
}