package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import java.sql.Timestamp;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="MovimentoParcelaDuplicata")


public class MovimentoParcelaDuplicata extends GenericModelIGN implements Cloneable
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idMovimentoParcelaDuplicata")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idDuplicata")
	private Duplicata duplicata;
	@ManyToOne
	@JoinColumn(name = "idDuplicataParcela")
	private DuplicataParcela duplicataParcela;
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@ManyToOne
	@JoinColumn(name = "idFormaPagamento")
	private FormaPagamento formaPagamento;
	@Column(name="numeroDuplicata")
	private Long numeroDuplicata;
	
	@Column(name="numeroParcela")
	private Integer numeroParcela;
	
	@Column(name="dataMovimento")
	private Date dataMovimento;
	
	@Column(name="tipoPagamento")
	private String tipoPagamento;
	
	@Column(name="valorCompraPago")
	private Double valorCompraPago;
	
	@Column(name="valorEntradaJuros")
	private Double valorEntradaJuros;
	
	@Column(name="valorRecebido")
	private Double valorRecebido;
	
	@Column(name="dataVencimento")
	private Date dataVencimento;
	
	@Column(name="valorParcela")
	private Double valorParcela;
	
	@Column(name="historico")
	private String historico;
	
	@ManyToOne
	@JoinColumn(name = "idChequeRecebido")
	private ChequeRecebido chequeRecebido;
	@ManyToOne
	@JoinColumn(name = "idPagamentoCartao")
	private PagamentoCartao pagamentoCartao;
	@Column(name="valorDesconto")
	private Double valorDesconto;
	@Column(name="horaMovimento")
	private Timestamp horaMovimento;
	@ManyToOne
	@JoinColumn(name = "idSeprocadoReativados")
	private SeprocadoReativados seprocadoReativados;
	@ManyToOne
	@JoinColumn(name = "idDuplicataParcelaCobranca")
	private DuplicataParcelaCobranca duplicataParcelaCobranca;
	@ManyToOne
	@JoinColumn(name = "idOperadoraCartao")
	private OperadoraCartao operadoraCartao;
	public MovimentoParcelaDuplicata()
	{
		super();
	}
	

	public MovimentoParcelaDuplicata
	(
	Duplicata duplicata,
	DuplicataParcela duplicataParcela,
	EmpresaFisica empresaFisica,
	Cliente cliente,
	Usuario usuario,
		Long numeroDuplicata,
		Integer numeroParcela,
		Date dataMovimento,
		String tipoPagamento,
		Double valorCompraPago,
		Double valorEntradaJuros,
		Double valorRecebido,
		Date dataVencimento,
		Double valorParcela,
		String historico,
		FormaPagamento formaPagamento,
		ChequeRecebido chequeRecebido,
		PagamentoCartao pagamentoCartao,
		Double valorDesconto,
		 Timestamp horaMovimento,
		 SeprocadoReativados seprocadoReativados,
		 DuplicataParcelaCobranca duplicataParcelaCobranca
	) 
	{
		super();
		this.duplicata = duplicata;
		this.duplicataParcela = duplicataParcela;
		this.empresaFisica = empresaFisica;
		this.cliente = cliente;
		this.usuario = usuario;
		this.numeroDuplicata = numeroDuplicata;
		this.numeroParcela = numeroParcela;
		this.dataMovimento = dataMovimento;
		this.tipoPagamento = tipoPagamento;
		this.valorCompraPago = valorCompraPago;
		this.valorEntradaJuros = valorEntradaJuros;
		this.valorRecebido = valorRecebido;
		this.dataVencimento = dataVencimento;
		this.valorParcela = valorParcela;
		this.historico = historico;
		this.formaPagamento = formaPagamento;
		this.chequeRecebido =chequeRecebido;
		this.pagamentoCartao = pagamentoCartao;
		this.valorDesconto = valorDesconto;
		this.horaMovimento = horaMovimento;
		this.seprocadoReativados = seprocadoReativados;
		this.duplicataParcelaCobranca = duplicataParcelaCobranca;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		
			this.id = id;
		
	}	
	
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}


	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}


	public Duplicata getDuplicata() {
		return duplicata;
	}

	public void setDuplicata(Duplicata duplicata) {
		this.duplicata = duplicata;
	}
	
	public DuplicataParcela getDuplicataParcela() {
		return duplicataParcela;
	}

	public void setDuplicataParcela(DuplicataParcela duplicataParcela) {
		this.duplicataParcela = duplicataParcela;
	}
	
	public Double getValorDesconto() {
		return valorDesconto;
	}


	public void setValorDesconto(Double valorDesconto) {
		if(valorDesconto.isNaN())
		{
			this.valorDesconto = null;	
		}
		else
		{
		this.valorDesconto = valorDesconto;
		}
	}


	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Long getNumeroDuplicata() {
		return numeroDuplicata;
	}

	public void setNumeroDuplicata(Long numeroDuplicata) {
	
			this.numeroDuplicata = numeroDuplicata;
		
	}	
	public Integer getNumeroParcela() {
		return numeroParcela;
	}

	public void setNumeroParcela(Integer numeroParcela) {
		
			this.numeroParcela = numeroParcela;
		
	}	
	public Date getDataMovimento() {
		return dataMovimento;
	}

	public void setDataMovimento(Date dataMovimento) {
		this.dataMovimento = dataMovimento;
	}
	public String getTipoPagamento() {
		String retorno = null;
		
		if (tipoPagamento != null)
			retorno = tipoPagamento.toUpperCase().trim();
		return retorno;
	}
	
	public void setTipoPagamento(String tipoPagamento) {
		if (tipoPagamento != null)
		{
			this.tipoPagamento = tipoPagamento.toUpperCase().trim();
		}
		else
			this.tipoPagamento = null;
			
		
	}
		
	public Double getValorCompraPago() {
		return valorCompraPago;
	}

	public void setValorCompraPago(Double valorCompraPago) 
	{
		if (valorCompraPago != null && Double.isNaN(valorCompraPago))
		{
			this.valorCompraPago = null;
		}
		else
		{
			this.valorCompraPago = valorCompraPago;
		}
	}	
	public Double getValorEntradaJuros() {
		return valorEntradaJuros;
	}

	public void setValorEntradaJuros(Double valorEntradaJuros) 
	{
		
		if (valorEntradaJuros != null && valorEntradaJuros == 0.0)
		{
			this.valorEntradaJuros = null;
		}
		else if(valorEntradaJuros.isNaN())
		{
			this.valorEntradaJuros = null;
		}
		else
		{
			this.valorEntradaJuros = valorEntradaJuros;
		}
	}
	
	public Double getValorRecebido() 
	{
		return valorRecebido;
	}

	public void setValorRecebido(Double valorRecebido) {		
		this.valorRecebido = valorRecebido;
	}	
	
	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}
	public Double getValorParcela() {
		return valorParcela;
	}

	public void setValorParcela(Double valorParcela) {
		if (valorParcela != null && valorParcela == 0.0)
		{
			this.valorParcela = null;
		}
		else
		{
			this.valorParcela = valorParcela;
		}
	}	
	public String getHistorico() {
		String retorno = null;
		
		if (historico != null)
			retorno = historico.toUpperCase().trim();
		return retorno;
	}
	
	public void setHistorico(String historico) {
		if (historico != null)
		{
			this.historico = historico.toUpperCase().trim();
		}
		else
			this.historico = null;
			
		
	}
		
	public ChequeRecebido getChequeRecebido() {
		return chequeRecebido;
	}


	public void setChequeRecebido(ChequeRecebido chequeRecebido) {
		this.chequeRecebido = chequeRecebido;
	}


	public PagamentoCartao getPagamentoCartao() {
		return pagamentoCartao;
	}


	public void setPagamentoCartao(PagamentoCartao pagamentoCartao) {
		this.pagamentoCartao = pagamentoCartao;
	}


	public Timestamp getHoraMovimento() {
		return horaMovimento;
	}


	public void setHoraMovimento(Timestamp horaMovimento) {
		this.horaMovimento = horaMovimento;
	}


	public SeprocadoReativados getSeprocadoReativados() {
		return seprocadoReativados;
	}


	public void setSeprocadoReativados(SeprocadoReativados seprocadoReativados) {
		this.seprocadoReativados = seprocadoReativados;
	}


	public DuplicataParcelaCobranca getDuplicataParcelaCobranca() {
		return duplicataParcelaCobranca;
	}


	public void setDuplicataParcelaCobranca(
			DuplicataParcelaCobranca duplicataParcelaCobranca) {
		this.duplicataParcelaCobranca = duplicataParcelaCobranca;
	}


	public OperadoraCartao getOperadoraCartao() {
		return operadoraCartao;
	}


	public void setOperadoraCartao(OperadoraCartao operadoraCartao) {
		this.operadoraCartao = operadoraCartao;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
	}

	@Override
	public MovimentoParcelaDuplicata clone() throws CloneNotSupportedException 
	{
		return (MovimentoParcelaDuplicata) super.clone();
	}
}