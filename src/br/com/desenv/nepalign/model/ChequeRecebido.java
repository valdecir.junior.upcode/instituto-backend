package br.com.desenv.nepalign.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="chequerecebido")


public class ChequeRecebido extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idChequeRecebido")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@ManyToOne
	@JoinColumn(name = "idBanco")
	private Banco banco;
	@ManyToOne
	@JoinColumn(name = "idContaCorrente")
	private ContaCorrente contaCorrente;
	@ManyToOne
	@JoinColumn(name="idStatusCheque")
	private StatusCheque statusCheque;
	
	@Column(name="numeroConta")
	private String numeroConta;
	
	@Column(name="numeroCheque")
	private String numeroCheque;
	@Column(name="numeroAgencia")
	private String numeroAgencia;
	
	@Column(name="cpfCnpjPortador")
	private String cpfCnpjPortador;
	
	@Column(name="valor")
	private Double valor;
	
	@Column(name="dataEmissao")
	private Date dataEmissao;
	
	@Column(name="preDatado")
	private Integer preDatado;
	
	@Column(name="bomPara")
	private Date bomPara;
	
	@Column(name="observacao")
	private String observacao;
	@ManyToOne
	@JoinColumn(name="idCobrador")
	private Cobrador cobrador;
	@Column(name="dataCobranca")
	private Date dataCobranca;
	@Column(name="emitente")
	private String emitente;
	@Column(name = "saldo")
	private Double saldo; 
	@Column(name = "consistencia")
	private String consistencia; 
	@Column(name = "promisoria")
	private String promisoria;
	@Column(name = "dataHoraInclusao")
	private Date dataHoraInclusao;
	
	public ChequeRecebido()
	{
		super();
	}
	

	public ChequeRecebido
	(
	EmpresaFisica empresaFisica,
	Cliente cliente,
	Banco banco,
	ContaCorrente contaCorrente,
		String numeroConta,
		String numeroCheque,
		String cpfCnpjPortador,
		Double valor,
		Date dataEmissao,
		Integer preDatado,
		Date bomPara,
		String observacao,
		Cobrador cobrador,
		Date dataCobranca,
		String numeroAgencia,
		String emitente,
		Double saldo,
		String consistencia,
		String promisoria,
		Date dataHoraInclusao
	) 
	{
		super();
		this.empresaFisica = empresaFisica;
		this.cliente = cliente;
		this.banco = banco;
		this.contaCorrente = contaCorrente;
		this.numeroConta = numeroConta;
		this.numeroCheque = numeroCheque;
		this.cpfCnpjPortador = cpfCnpjPortador;
		this.valor = valor;
		this.dataEmissao = dataEmissao;
		this.preDatado = preDatado;
		this.bomPara = bomPara;
		this.observacao = observacao;
		this.cobrador = cobrador;
		this.dataCobranca = dataCobranca;
		this.numeroAgencia = numeroAgencia;
		this.emitente = emitente;
		this.saldo = saldo;
		this.consistencia = consistencia;
		this.promisoria = promisoria;
		this.dataHoraInclusao = dataHoraInclusao;
	}

	public Date getDataHoraInclusao() {
		return dataHoraInclusao;
	}


	public void setDataHoraInclusao(Date dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public String getNumeroAgencia() {
		return numeroAgencia;
	}


	public void setNumeroAgencia(String numeroAgencia) {
		this.numeroAgencia = numeroAgencia;
	}


	public String getEmitente() {
		return emitente;
	}


	public void setEmitente(String emitente) {
		this.emitente = emitente.toUpperCase();
	}


	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	
	public ContaCorrente getContaCorrente() {
		return contaCorrente;
	}

	public void setContaCorrente(ContaCorrente contaCorrente) {
		this.contaCorrente = contaCorrente;
	}
	
	public StatusCheque getStatusCheque()
	{
		return this.statusCheque;
	}
	
	public void setStatusCheque(StatusCheque statusCheque)
	{
		this.statusCheque = statusCheque;
	}
	
	public String getNumeroConta() {
		String retorno = null;
		
		if (numeroConta != null)
			retorno = numeroConta.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroConta(String numeroConta) {
		if (numeroConta != null)
		{
			this.numeroConta = numeroConta.toUpperCase().trim();
		}
		else
			this.numeroConta = null;
			
		
	}
		
	public String getNumeroCheque() {
		String retorno = null;
		
		if (numeroCheque != null)
			retorno = numeroCheque.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroCheque(String numeroCheque) {
		if (numeroCheque != null)
		{
			this.numeroCheque = numeroCheque.toUpperCase().trim();
		}
		else
			this.numeroCheque = null;
			
		
	}
		
	public String getCpfCnpjPortador() {
		String retorno = null;
		
		if (cpfCnpjPortador != null)
			retorno = cpfCnpjPortador.toUpperCase().trim();
		return retorno;
	}
	
	public void setCpfCnpjPortador(String cpfCnpjPortador) {
		if (cpfCnpjPortador != null)
		{
			this.cpfCnpjPortador = cpfCnpjPortador.toUpperCase().trim();
		}
		else
			this.cpfCnpjPortador = null;
			
		
	}
		
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
	
		if (valor != null && Double.isNaN(valor))
		{
			this.valor = null;
		}
		else
		{
			this.valor = valor;
		}
		
	}	
	public Integer getPreDatado() {
		return preDatado;
	}
	
	public Date getDataEmissao()
	{
		return this.dataEmissao;
	}
	
	public void setDataEmissao(Date dataEmissao)
	{
		this.dataEmissao = dataEmissao;
	}

	public void setPreDatado(Integer preDatado) {
		this.preDatado = preDatado;
	}
	
	public Date getBomPara() {
		Date retorno = null;
		
		if (bomPara != null)
			retorno = bomPara;
		return retorno;
	}
	
	public void setBomPara(Date bomPara) {
		if (bomPara != null)
		{
			this.bomPara = bomPara;
		}
		else 
		{
			this.bomPara = null;
		}
	}

	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	public Cobrador getCobrador() {
		return cobrador;
	}


	public void setCobrador(Cobrador cobrador) {
		this.cobrador = cobrador;
	}


	public Date getDataCobranca() {
		return dataCobranca;
	}


	public void setDataCobranca(Date dataCobranca) {
		this.dataCobranca = dataCobranca;
	}


	public Double getSaldo() {
		return saldo;
	}


	public void setSaldo(Double saldo) {
		if(saldo.isNaN())
		{
			this.saldo = null;
		}
		else
		{
			this.saldo = saldo;	
		}
	
	}


	public String getConsistencia() {
		return consistencia;
	}


	public void setConsistencia(String consistencia) {
		this.consistencia = consistencia;
	}


	public String getPromisoria() {
		return promisoria;
	}


	public void setPromisoria(String promisoria) {
		this.promisoria = promisoria;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
