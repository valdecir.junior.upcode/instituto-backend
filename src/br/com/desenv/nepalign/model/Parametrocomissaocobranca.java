package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="parametrocomissaocobranca")


public class Parametrocomissaocobranca extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idParametrocomissaocobranca")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idFormaPagamento")
	private FormaPagamento formaPagamento;
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idCobrador")
	private Cobrador cobrador;
	@Column(name="qtdDiasAte")
	private Integer qtdDiasAte;
	
	@Column(name="percentual")
	private Double percentual;
	
	@Column(name="internoExterno")
	private String internoExterno;
	

	public Parametrocomissaocobranca()
	{
		super();
	}
	

	public Parametrocomissaocobranca
	(
	FormaPagamento formaPagamento,
	EmpresaFisica empresaFisica,
	Cobrador cobrador,
		Integer qtdDiasAte,
		Double percentual,
		String internoExterno
	) 
	{
		super();
		this.formaPagamento = formaPagamento;
		this.empresaFisica = empresaFisica;
		this.cobrador = cobrador;
		this.qtdDiasAte = qtdDiasAte;
		this.percentual = percentual;
		this.internoExterno = internoExterno;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public Cobrador getCobrador() {
		return cobrador;
	}

	public void setCobrador(Cobrador cobrador) {
		this.cobrador = cobrador;
	}
	
	public Integer getQtdDiasAte() {
		return qtdDiasAte;
	}

	public void setQtdDiasAte(Integer qtdDiasAte) {
		this.qtdDiasAte = qtdDiasAte;
	}	
	public Double getPercentual() {
		return percentual;
	}

	public void setPercentual(Double percentual) {
	
		if (percentual != null && Double.isNaN(percentual))
		{
			this.percentual = null;
		}
		else
		{
			this.percentual = percentual;
		}
		
	}	
	public String getInternoExterno() {
		String retorno = null;
		
		if (internoExterno != null)
			retorno = internoExterno.toUpperCase().trim();
		return retorno;
	}
	
	public void setInternoExterno(String internoExterno) {
		if (internoExterno != null)
		{
			this.internoExterno = internoExterno.toUpperCase().trim();
		}
		else
			this.internoExterno = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
