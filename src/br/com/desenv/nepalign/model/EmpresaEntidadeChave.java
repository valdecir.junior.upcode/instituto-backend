package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name = "empresaentidadechave")
public class EmpresaEntidadeChave extends GenericModelIGN
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idEmpresaEntidadeChave")
    private Integer id;

    @Column(name = "idEmpresaFisica")
    private Integer idEmpresaFisica;

    @Column(name = "entidade")
    private String entidade;

    @Column(name = "chaveInicial")
    private Integer chaveInicial;

    @Column(name = "chaveFinal")
    private Integer chaveFinal;

    @Transient
    private Integer chaveAtual;

    public EmpresaEntidadeChave()
    {
    	super();
    }

    public EmpresaEntidadeChave(Integer idEmpresaFisica, String entidade, Integer chaveInicial, Integer chaveFinal)
    {
    	super();
		this.idEmpresaFisica = idEmpresaFisica;
		this.entidade = entidade;
		this.chaveInicial = chaveInicial;
		this.chaveFinal = chaveFinal;
    }

    public Integer getId()
    {
    	return id;
    }

    public void setId(Integer id)
    {
    	if (id != null && id == 0x00)
    		this.id = null;
    	else
    		this.id = id;
    }

    public Integer getIdEmpresaFisica()
    {
    	return idEmpresaFisica;
    }

    public void setIdEmpresaFisica(Integer idEmpresaFisica)
    {
		if (idEmpresaFisica != null && idEmpresaFisica == 0x00)
		    this.idEmpresaFisica = null;
		else
		    this.idEmpresaFisica = idEmpresaFisica;
    }

    public String getEntidade()
    {
    	String retorno = null;

		if (entidade != null)
			retorno = entidade.toUpperCase().trim();
		
		return retorno;
    }

    public void setEntidade(String entidade)
    {
		if (entidade != null)
		    this.entidade = entidade.toUpperCase().trim(); 
		else
		    this.entidade = null;
    }

    public Integer getChaveInicial()
    {
    	return chaveInicial;
    }

    public void setChaveInicial(Integer chaveInicial)
    {
		if (chaveInicial != null && chaveInicial == 0x00)
		    this.chaveInicial = null;
		else
		    this.chaveInicial = chaveInicial;
    }

    public Integer getChaveFinal()
    {
    	return chaveFinal;
    }

    public void setChaveFinal(Integer chaveFinal)
    {
		if (chaveFinal != null && chaveFinal == 0x00)
		    this.chaveFinal = null;
		else
		    this.chaveFinal = chaveFinal;
    }

    @Override
    public void validate() throws Exception { }

    public Integer getChaveAtual()
    {
        return chaveAtual;
    }

    public void setChaveAtual(Integer chaveAtual)
    {
        this.chaveAtual = chaveAtual;
    }
}