package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="grupoempresa")
public class GrupoEmpresa extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;
	
	public static final Integer GRUPOEMPRESA_LOJAS = 0x01;
	public static final Integer GRUPOEMPRESA_OUTRAS = 0x02;
	public static final Integer GRUPOEMPRESA_CHEQUE = 0x03;
	public static final Integer GRUPOEMPRESA_VALE_CALCADO = 0x04;
	public static final Integer GRUPOEMPRESA_CARTAO_REDECARD = 0x05;
	public static final Integer GRUPOEMPRESA_CARTAO_CIELO = 0x06;
	public static final Integer GRUPOEMPRESA_CARTAO_SICREDI = 0x07;
	public static final Integer GRUPOEMPRESA_CARTAO_COOPER = 0x08;
	public static final Integer GRUPOEMPRESA_CARTAO_HIPERCARD = 0x09;
	public static final Integer GRUPOEMPRESA_CARTAO_SENFF = 0x0A;
	public static final Integer GRUPOEMPRESA_CARTAO_ASEMPAR = 0x0B;
	public static final Integer GRUPOEMPRESA_CARTAO_BIN = 0x0C;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idgrupoEmpressa")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	
	public GrupoEmpresa()
	{
		super();
	}
	
	public GrupoEmpresa(String descricao) 
	{
		super();
		this.descricao = descricao;
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public String getDescricao() 
	{
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setDescricao(String descricao) 
	{
		if (descricao != null)
			this.descricao = descricao.toUpperCase().trim();
		else
			this.descricao = null;
	}
		
	@Override
	public void validate() throws Exception { }
}