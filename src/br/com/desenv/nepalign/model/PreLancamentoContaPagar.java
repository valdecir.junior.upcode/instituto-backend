package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="prelancamentocontapagar")


public class PreLancamentoContaPagar extends GenericModelIGN
{
	private static final long serialVersionUID = 1L;
	
	public final static String PENDENTE = "P";
	public final static String EFETUADO = "E";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idPreLancamentoContaPagar")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisicaOrigem")
	private EmpresaFisica empresaFisicaOrigem;
	@ManyToOne
	@JoinColumn(name = "idFornecedor")
	private Fornecedor fornecedor;
	@ManyToOne
	@JoinColumn(name = "idContaGerencial")
	private ContaGerencial contaGerencial;
	@ManyToOne
	@JoinColumn(name = "idTipoMovimentoCaixaDia")
	private TipoMovimentoCaixaDia tipoMovimentoCaixaDia;
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@ManyToOne
	@JoinColumn(name = "idLancamentoContaPagar")
	private LancamentoContaPagar lancamentoContaPagar;
	
	@Column(name="idDocumentoOrigem")
	private Integer idDocumentoOrigem;
	
	@Column(name="dataLancamento")
	private Date dataLancamento;
	
	@Column(name="numeroDocumento")
	private String numeroDocumento;
	
	@Column(name="valor")
	private Double valor;
	
	@Column(name="situacao")
	private String situacao;
	
	@Column(name="observacao")
	private String observacao;
	
	public PreLancamentoContaPagar()
	{
		super();
	}
	
	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	public EmpresaFisica getEmpresaFisica() 
	{
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) 
	{
		this.empresaFisica = empresaFisica;
	}
	
	public EmpresaFisica getEmpresaFisicaOrigem() 
	{
		return empresaFisicaOrigem;
	}

	public void setEmpresaFisicaOrigem(EmpresaFisica empresaFisicaOrigem) 
	{
		this.empresaFisicaOrigem = empresaFisicaOrigem;
	}

	public Fornecedor getFornecedor() 
	{
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) 
	{
		this.fornecedor = fornecedor;
	}

	public ContaGerencial getContaGerencial() 
	{
		return contaGerencial;
	}

	public void setContaGerencial(ContaGerencial contaGerencial) 
	{
		this.contaGerencial = contaGerencial;
	}
	
	public TipoMovimentoCaixaDia getTipoMovimentoCaixaDia() 
	{
		return tipoMovimentoCaixaDia;
	}

	public void setTipoMovimentoCaixaDia(TipoMovimentoCaixaDia tipoMovimentoCaixaDia) 
	{
		this.tipoMovimentoCaixaDia = tipoMovimentoCaixaDia;
	}

	public Usuario getUsuario() 
	{
		return usuario;
	}

	public void setUsuario(Usuario usuario) 
	{
		this.usuario = usuario;
	}

	public LancamentoContaPagar getLancamentoContaPagar() 
	{
		return lancamentoContaPagar;
	}

	public void setLancamentoContaPagar(LancamentoContaPagar lancamentoContaPagar) 
	{
		this.lancamentoContaPagar = lancamentoContaPagar;
	}

	public Integer getIdDocumentoOrigem() 
	{
		return idDocumentoOrigem;
	}

	public void setIdDocumentoOrigem(Integer idDocumentoOrigem) 
	{
		this.idDocumentoOrigem = idDocumentoOrigem;
	}

	public Date getDataLancamento() 
	{
		return dataLancamento;
	}

	public void setDataLancamento(Date dataLancamento) 
	{
		this.dataLancamento = dataLancamento;
	}

	public String getNumeroDocumento() 
	{
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) 
	{
		this.numeroDocumento = numeroDocumento;
	}

	public Double getValor() 
	{
		return valor;
	}

	public void setValor(Double valor) 
	{
		if (valor != null && Double.isNaN(valor))
			this.valor = null;
		else
			this.valor = valor;
	}	
		
	public String getSituacao() 
	{
		return situacao;
	}

	public void setSituacao(String situacao) 
	{
		this.situacao = situacao;
	}

	public String getObservacao() 
	{
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		
		return retorno;
	}
	
	public void setObservacao(String observacao) 
	{
		if (observacao != null)
			this.observacao = observacao.toUpperCase().trim();
		else
			this.observacao = null;
	}
	
	@Override
	public void validate() throws Exception
	{
		if(numeroDocumento == null || numeroDocumento.trim().equals(""))
			numeroDocumento = "SEM NUMERO DOCUMENTO";
	}
}