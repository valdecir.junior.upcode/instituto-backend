package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="residenciaatendido")


public class ResidenciaAtendido extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idResidenciaAtendido")
	private Integer id;
		
	@Column(name="descricao")
	private String descriicao;
	

	public ResidenciaAtendido()
	{
		super();
	}
	

	public ResidenciaAtendido
	(
		String descriicao
	) 
	{
		super();
		this.descriicao = descriicao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public String getDescriicao() {
		String retorno = null;
		
		if (descriicao != null)
			retorno = descriicao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescriicao(String descriicao) {
		if (descriicao != null)
		{
			this.descriicao = descriicao.toUpperCase().trim();
		}
		else
			this.descriicao = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
