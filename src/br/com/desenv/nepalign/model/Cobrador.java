package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="cobrador")


public class Cobrador extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idCobrador")
	private Integer id;
		
	@Column(name="nome")
	private String nome;
	
	@Column(name="endereco")
	private String endereco;
	
	@ManyToOne
	@JoinColumn(name = "idCidade")
	private Cidade cidade;
	
	@Column(name="ativo")
	private String ativo;
	
	@Column(name="bairro")
	private String bairro;
	
	@Column(name="cep")
	private String cep;
	
	@Column(name="telefone")
	private String telefone;
	
	@Column(name="celular")
	private String celular;
	@Column(name="internoExterno")
	private String internoExterno;

	public Cobrador()
	{
		super();
	}
	

	public Cobrador
	(
		String nome,
		String endereco,
	Cidade cidade,
		String ativo,
		String bairro,
		String cep,
		String telefone,
		String celular
	) 
	{
		super();
		this.nome = nome;
		this.endereco = endereco;
		this.cidade = cidade;
		this.ativo = ativo;
		this.bairro = bairro;
		this.cep = cep;
		this.telefone = telefone;
		this.celular = celular;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getNome() {
		String retorno = null;
		
		if (nome != null)
			retorno = nome.toUpperCase().trim();
		return retorno;
	}
	
	public void setNome(String nome) {
		if (nome != null)
		{
			this.nome = nome.toUpperCase().trim();
		}
		else
			this.nome = null;
			
		
	}
		
	public String getEndereco() {
		String retorno = null;
		
		if (endereco != null)
			retorno = endereco.toUpperCase().trim();
		return retorno;
	}
	
	public void setEndereco(String endereco) {
		if (endereco != null)
		{
			this.endereco = endereco.toUpperCase().trim();
		}
		else
			this.endereco = null;
			
		
	}
		
	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	
	public String getAtivo() {
		String retorno = null;
		
		if (ativo != null)
			retorno = ativo.toUpperCase().trim();
		return retorno;
	}
	
	public void setAtivo(String ativo) {
		if (ativo != null)
		{
			this.ativo = ativo.toUpperCase().trim();
		}
		else
			this.ativo = null;
			
		
	}
		
	public String getBairro() {
		String retorno = null;
		
		if (bairro != null)
			retorno = bairro.toUpperCase().trim();
		return retorno;
	}
	
	public void setBairro(String bairro) {
		if (bairro != null)
		{
			this.bairro = bairro.toUpperCase().trim();
		}
		else
			this.bairro = null;
			
		
	}
		
	public String getCep() {
		String retorno = null;
		
		if (cep != null)
			retorno = cep.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setCep(String cep) {
		if (cep != null)
		{
			this.cep = cep.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.cep = null;
			
		
	}
		
	public String getTelefone() {
		String retorno = null;
		
		if (telefone != null)
			retorno = telefone.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setTelefone(String telefone) {
		if (telefone != null)
		{
			this.telefone = telefone.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.telefone = null;
			
		
	}
		
	public String getCelular() {
		String retorno = null;
		
		if (celular != null)
			retorno = celular.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setCelular(String celular) {
		if (celular != null)
		{
			this.celular = celular.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.celular = null;
			
		
	}
		
	public String getInternoExterno() {
		return internoExterno;
	}


	public void setInternoExterno(String internoExterno) {
		this.internoExterno = internoExterno;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
