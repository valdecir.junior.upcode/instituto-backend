package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="pagamentonotafiscal")


public class PagamentoNotaFiscal extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idPagamentoNotaFiscal")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idNotaFiscal")
	private NotaFiscal notaFiscal;
	@ManyToOne
	@JoinColumn(name = "idFormaPagamento")
	private FormaPagamento formaPagamento;
	@ManyToOne
	@JoinColumn(name = "idLancamentoContaReceber")
	private LancamentoContaReceber lancamentoContaReceber;
	@Column(name="dataPagamento")
	private Date dataPagamento;
	
	@Column(name="valor")
	private Double valor;
	
	@Column(name="numeroDocumento")
	private String numeroDocumento;
	
	@Column(name="numeroCheque")
	private String numeroCheque;
	
	@Column(name="cnpjCpf")
	private String cnpjCpf;
	
	@Column(name="numeroDuplicata")
	private String numeroDuplicata;
	
	@ManyToOne
	@JoinColumn(name = "idSituacaoLancamento")
	private SituacaoLancamento situacaoLancamento;

	public PagamentoNotaFiscal()
	{
		super();
	}
	

	public PagamentoNotaFiscal
	(
	NotaFiscal notaFiscal,
	FormaPagamento formaPagamento,
	LancamentoContaReceber lancamentoContaReceber,
		Date dataPagamento,
		Double valor,
		String numeroDocumento,
		String numeroCheque,
		String cnpjCpf,
		String numeroDuplicata,
	SituacaoLancamento situacaoLancamento
	) 
	{
		super();
		this.notaFiscal = notaFiscal;
		this.formaPagamento = formaPagamento;
		this.lancamentoContaReceber = lancamentoContaReceber;
		this.dataPagamento = dataPagamento;
		this.valor = valor;
		this.numeroDocumento = numeroDocumento;
		this.numeroCheque = numeroCheque;
		this.cnpjCpf = cnpjCpf;
		this.numeroDuplicata = numeroDuplicata;
		this.situacaoLancamento = situacaoLancamento;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public NotaFiscal getNotaFiscal() {
		return notaFiscal;
	}

	public void setNotaFiscal(NotaFiscal notaFiscal) {
		this.notaFiscal = notaFiscal;
	}
	
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
	public LancamentoContaReceber getLancamentoContaReceber() {
		return lancamentoContaReceber;
	}

	public void setLancamentoContaReceber(LancamentoContaReceber lancamentoContaReceber) {
		this.lancamentoContaReceber = lancamentoContaReceber;
	}
	
	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
	
		if (valor != null && Double.isNaN(valor))
		{
			this.valor = null;
		}
		else
		{
			this.valor = valor;
		}
		
	}	
	public String getNumeroDocumento() {
		String retorno = null;
		
		if (numeroDocumento != null)
			retorno = numeroDocumento.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroDocumento(String numeroDocumento) {
		if (numeroDocumento != null)
		{
			this.numeroDocumento = numeroDocumento.toUpperCase().trim();
		}
		else
			this.numeroDocumento = null;
			
		
	}
		
	public String getNumeroCheque() {
		String retorno = null;
		
		if (numeroCheque != null)
			retorno = numeroCheque.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroCheque(String numeroCheque) {
		if (numeroCheque != null)
		{
			this.numeroCheque = numeroCheque.toUpperCase().trim();
		}
		else
			this.numeroCheque = null;
			
		
	}
		
	public String getCnpjCpf() {
		String retorno = null;
		
		if (cnpjCpf != null)
			retorno = cnpjCpf.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setCnpjCpf(String cnpjCpf) {
		if (cnpjCpf != null)
		{
			this.cnpjCpf = cnpjCpf.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.cnpjCpf = null;
			
		
	}
		
	public String getNumeroDuplicata() {
		String retorno = null;
		
		if (numeroDuplicata != null)
			retorno = numeroDuplicata.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroDuplicata(String numeroDuplicata) {
		if (numeroDuplicata != null)
		{
			this.numeroDuplicata = numeroDuplicata.toUpperCase().trim();
		}
		else
			this.numeroDuplicata = null;
			
		
	}
		
	public SituacaoLancamento getSituacaoLancamento() {
		return situacaoLancamento;
	}

	public void setSituacaoLancamento(SituacaoLancamento situacaoLancamento) {
		this.situacaoLancamento = situacaoLancamento;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
