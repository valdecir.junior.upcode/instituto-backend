package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="tiponotafiscal")


public class TipoNotaFiscal extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idTipoNotaFiscal")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="CfopDentroEstado")
	private Integer cfopDentroEstado;
	
	@Column(name="CfopForaEstado")
	private Integer cfopForaEstado;
	
	@Column(name="CfopForaPais")
	private Integer cfopForaPais;
	
	@Column(name="observacao")
	private String observacao;
	

	public TipoNotaFiscal()
	{
		super();
	}
	

	public TipoNotaFiscal
	(
		String descricao,
		Integer cfopDentroEstado,
		Integer cfopForaEstado,
		Integer cfopForaPais,
		String observacao
	) 
	{
		super();
		this.descricao = descricao;
		this.cfopDentroEstado = cfopDentroEstado;
		this.cfopForaEstado = cfopForaEstado;
		this.cfopForaPais = cfopForaPais;
		this.observacao = observacao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public Integer getCfopDentroEstado() {
		return cfopDentroEstado;
	}

	public void setCfopDentroEstado(Integer cfopDentroEstado) {
		if (cfopDentroEstado != null && cfopDentroEstado == 0)
		{
			this.cfopDentroEstado = null;
		}
		else
		{
			this.cfopDentroEstado = cfopDentroEstado;
		}
	}	
	public Integer getCfopForaEstado() {
		return cfopForaEstado;
	}

	public void setCfopForaEstado(Integer cfopForaEstado) {
		if (cfopForaEstado != null && cfopForaEstado == 0)
		{
			this.cfopForaEstado = null;
		}
		else
		{
			this.cfopForaEstado = cfopForaEstado;
		}
	}	
	public Integer getCfopForaPais() {
		return cfopForaPais;
	}

	public void setCfopForaPais(Integer cfopForaPais) {
		if (cfopForaPais != null && cfopForaPais == 0)
		{
			this.cfopForaPais = null;
		}
		else
		{
			this.cfopForaPais = cfopForaPais;
		}
	}	
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
