package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="itemnotaicmsnfe")


public class ItemNotaIcmsNfe extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idItemNotaICMSNFE")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idItemNota")
	private ItemNotaFiscal itemNota;
	@Column(name="codigoCSTICMS")
	private Integer codigoCSTICMS;
	
	@Column(name="origemMercadoria")
	private Integer origemMercadoria;
	
	@Column(name="modDetermBaseCalcICMS")
	private Integer modDetermBaseCalcICMS;
	
	@Column(name="valBaseCalcICMS")
	private Double valBaseCalcICMS;
	
	@Column(name="valICMS")
	private Double valICMS;
	
	@Column(name="modBaseCalcICMSST")
	private Double modBaseCalcICMSST;
	
	@Column(name="valBaseCalcICMSST")
	private Double valBaseCalcICMSST;
	
	@Column(name="aliqICMSST")
	private Double aliqICMSST;
	
	@Column(name="valICMSST")
	private Double valICMSST;
	
	@Column(name="percMVAICMSST")
	private Double percMVAICMSST;
	
	@Column(name="percRedBaseCalcICMSST")
	private Double percRedBaseCalcICMSST;
	
	@Column(name="ufStICMS")
	private String ufStICMS;
	
	@Column(name="pbcopIcms")
	private Double pbcopIcms;
	
	@Column(name="vbcstretIcms")
	private Double vbcstretIcms;
	
	@Column(name="motdesicms")
	private Integer motdesicms;
	
	@Column(name="vbcstdesticms")
	private Double vbcstdesticms;
	
	@Column(name="vbcstdest")
	private Double vbcstdest;
	
	@Column(name="pcredsn")
	private Double pcredsn;
	
	@Column(name="vcredicmssn")
	private Double vcredicmssn;
	
	@Column(name="vicmsstret")
	private Double vicmsstret;
	
	@Column(name="vicmsstdest")
	private Double vicmsstdest;
	

	public ItemNotaIcmsNfe()
	{
		super();
	}
	

	public ItemNotaIcmsNfe
	(
	ItemNotaFiscal itemNota,
		Integer codigoCSTICMS,
		Integer origemMercadoria,
		Integer modDetermBaseCalcICMS,
		Double valBaseCalcICMS,
		Double valICMS,
		Double modBaseCalcICMSST,
		Double valBaseCalcICMSST,
		Double aliqICMSST,
		Double valICMSST,
		Double percMVAICMSST,
		Double percRedBaseCalcICMSST,
		String ufStICMS,
		Double pbcopIcms,
		Double vbcstretIcms,
		Integer motdesicms,
		Double vbcstdesticms,
		Double vbcstdest,
		Double pcredsn,
		Double vcredicmssn,
		Double vicmsstret,
		Double vicmsstdest
	) 
	{
		super();
		this.itemNota = itemNota;
		this.codigoCSTICMS = codigoCSTICMS;
		this.origemMercadoria = origemMercadoria;
		this.modDetermBaseCalcICMS = modDetermBaseCalcICMS;
		this.valBaseCalcICMS = valBaseCalcICMS;
		this.valICMS = valICMS;
		this.modBaseCalcICMSST = modBaseCalcICMSST;
		this.valBaseCalcICMSST = valBaseCalcICMSST;
		this.aliqICMSST = aliqICMSST;
		this.valICMSST = valICMSST;
		this.percMVAICMSST = percMVAICMSST;
		this.percRedBaseCalcICMSST = percRedBaseCalcICMSST;
		this.ufStICMS = ufStICMS;
		this.pbcopIcms = pbcopIcms;
		this.vbcstretIcms = vbcstretIcms;
		this.motdesicms = motdesicms;
		this.vbcstdesticms = vbcstdesticms;
		this.vbcstdest = vbcstdest;
		this.pcredsn = pcredsn;
		this.vcredicmssn = vcredicmssn;
		this.vicmsstret = vicmsstret;
		this.vicmsstdest = vicmsstdest;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public ItemNotaFiscal getItemNota() {
		return itemNota;
	}

	public void setItemNota(ItemNotaFiscal itemNota) {
		this.itemNota = itemNota;
	}
	
	public Integer getCodigoCSTICMS() {
		return codigoCSTICMS;
	}

	public void setCodigoCSTICMS(Integer codigoCSTICMS) {
		this.codigoCSTICMS = codigoCSTICMS;
	}	
	public Integer getOrigemMercadoria() {
		return origemMercadoria;
	}

	public void setOrigemMercadoria(Integer origemMercadoria) {
		this.origemMercadoria = origemMercadoria;
	}	
	public Integer getModDetermBaseCalcICMS() {
		return modDetermBaseCalcICMS;
	}

	public void setModDetermBaseCalcICMS(Integer modDetermBaseCalcICMS) {
		this.modDetermBaseCalcICMS = modDetermBaseCalcICMS;
	}	
	public Double getValBaseCalcICMS() {
		return valBaseCalcICMS;
	}

	public void setValBaseCalcICMS(Double valBaseCalcICMS) {
	
		if (valBaseCalcICMS != null && Double.isNaN(valBaseCalcICMS))
		{
			this.valBaseCalcICMS = null;
		}
		else
		{
			this.valBaseCalcICMS = valBaseCalcICMS;
		}
		
	}	
	public Double getValICMS() {
		return valICMS;
	}

	public void setValICMS(Double valICMS) {
	
		if (valICMS != null && Double.isNaN(valICMS))
		{
			this.valICMS = null;
		}
		else
		{
			this.valICMS = valICMS;
		}
		
	}	
	public Double getModBaseCalcICMSST() {
		return modBaseCalcICMSST;
	}

	public void setModBaseCalcICMSST(Double modBaseCalcICMSST) {
	
		if (modBaseCalcICMSST != null && Double.isNaN(modBaseCalcICMSST))
		{
			this.modBaseCalcICMSST = null;
		}
		else
		{
			this.modBaseCalcICMSST = modBaseCalcICMSST;
		}
		
	}	
	public Double getValBaseCalcICMSST() {
		return valBaseCalcICMSST;
	}

	public void setValBaseCalcICMSST(Double valBaseCalcICMSST) {
	
		if (valBaseCalcICMSST != null && Double.isNaN(valBaseCalcICMSST))
		{
			this.valBaseCalcICMSST = null;
		}
		else
		{
			this.valBaseCalcICMSST = valBaseCalcICMSST;
		}
		
	}	
	public Double getAliqICMSST() {
		return aliqICMSST;
	}

	public void setAliqICMSST(Double aliqICMSST) {
	
		if (aliqICMSST != null && Double.isNaN(aliqICMSST))
		{
			this.aliqICMSST = null;
		}
		else
		{
			this.aliqICMSST = aliqICMSST;
		}
		
	}	
	public Double getValICMSST() {
		return valICMSST;
	}

	public void setValICMSST(Double valICMSST) {
	
		if (valICMSST != null && Double.isNaN(valICMSST))
		{
			this.valICMSST = null;
		}
		else
		{
			this.valICMSST = valICMSST;
		}
		
	}	
	public Double getPercMVAICMSST() {
		return percMVAICMSST;
	}

	public void setPercMVAICMSST(Double percMVAICMSST) {
	
		if (percMVAICMSST != null && Double.isNaN(percMVAICMSST))
		{
			this.percMVAICMSST = null;
		}
		else
		{
			this.percMVAICMSST = percMVAICMSST;
		}
		
	}	
	public Double getPercRedBaseCalcICMSST() {
		return percRedBaseCalcICMSST;
	}

	public void setPercRedBaseCalcICMSST(Double percRedBaseCalcICMSST) {
	
		if (percRedBaseCalcICMSST != null && Double.isNaN(percRedBaseCalcICMSST))
		{
			this.percRedBaseCalcICMSST = null;
		}
		else
		{
			this.percRedBaseCalcICMSST = percRedBaseCalcICMSST;
		}
		
	}	
	public String getUfStICMS() {
		String retorno = null;
		
		if (ufStICMS != null)
			retorno = ufStICMS.toUpperCase().trim();
		return retorno;
	}
	
	public void setUfStICMS(String ufStICMS) {
		if (ufStICMS != null)
		{
			this.ufStICMS = ufStICMS.toUpperCase().trim();
		}
		else
			this.ufStICMS = null;
			
		
	}
		
	public Double getPbcopIcms() {
		return pbcopIcms;
	}

	public void setPbcopIcms(Double pbcopIcms) {
	
		if (pbcopIcms != null && Double.isNaN(pbcopIcms))
		{
			this.pbcopIcms = null;
		}
		else
		{
			this.pbcopIcms = pbcopIcms;
		}
		
	}	
	public Double getVbcstretIcms() {
		return vbcstretIcms;
	}

	public void setVbcstretIcms(Double vbcstretIcms) {
	
		if (vbcstretIcms != null && Double.isNaN(vbcstretIcms))
		{
			this.vbcstretIcms = null;
		}
		else
		{
			this.vbcstretIcms = vbcstretIcms;
		}
		
	}	
	public Integer getMotdesicms() {
		return motdesicms;
	}

	public void setMotdesicms(Integer motdesicms) {
		this.motdesicms = motdesicms;
	}	
	public Double getVbcstdesticms() {
		return vbcstdesticms;
	}

	public void setVbcstdesticms(Double vbcstdesticms) {
	
		if (vbcstdesticms != null && Double.isNaN(vbcstdesticms))
		{
			this.vbcstdesticms = null;
		}
		else
		{
			this.vbcstdesticms = vbcstdesticms;
		}
		
	}	
	public Double getVbcstdest() {
		return vbcstdest;
	}

	public void setVbcstdest(Double vbcstdest) {
	
		if (vbcstdest != null && Double.isNaN(vbcstdest))
		{
			this.vbcstdest = null;
		}
		else
		{
			this.vbcstdest = vbcstdest;
		}
		
	}	
	public Double getPcredsn() {
		return pcredsn;
	}

	public void setPcredsn(Double pcredsn) {
	
		if (pcredsn != null && Double.isNaN(pcredsn))
		{
			this.pcredsn = null;
		}
		else
		{
			this.pcredsn = pcredsn;
		}
		
	}	
	public Double getVcredicmssn() {
		return vcredicmssn;
	}

	public void setVcredicmssn(Double vcredicmssn) {
	
		if (vcredicmssn != null && Double.isNaN(vcredicmssn))
		{
			this.vcredicmssn = null;
		}
		else
		{
			this.vcredicmssn = vcredicmssn;
		}
		
	}	
	public Double getVicmsstret() {
		return vicmsstret;
	}

	public void setVicmsstret(Double vicmsstret) {
	
		if (vicmsstret != null && Double.isNaN(vicmsstret))
		{
			this.vicmsstret = null;
		}
		else
		{
			this.vicmsstret = vicmsstret;
		}
		
	}	
	public Double getVicmsstdest() {
		return vicmsstdest;
	}

	public void setVicmsstdest(Double vicmsstdest) {
	
		if (vicmsstdest != null && Double.isNaN(vicmsstdest))
		{
			this.vicmsstdest = null;
		}
		else
		{
			this.vicmsstdest = vicmsstdest;
		}
		
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
