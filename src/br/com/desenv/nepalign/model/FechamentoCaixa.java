package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="fechamentocaixa")


public class FechamentoCaixa extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idFechamentoCaixa")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idCaixaDia")
	private CaixaDia caixadia;
	@Column(name="dataFechamento")
	private Date dataFechamento;
	
	@Column(name="valorCalculado")
	private Double valorCalculado;
	
	@Column(name="valorInformado")
	private Double valorInformado;
	

	public FechamentoCaixa()
	{
		super();
	}
	

	public FechamentoCaixa
	(
	CaixaDia caixadia,
		Date dataFechamento,
		Double valorCalculado,
		Double valorInformado
	) 
	{
		super();
		this.caixadia = caixadia;
		this.dataFechamento = dataFechamento;
		this.valorCalculado = valorCalculado;
		this.valorInformado = valorInformado;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public CaixaDia getCaixadia() {
		return caixadia;
	}

	public void setCaixadia(CaixaDia caixadia) {
		this.caixadia = caixadia;
	}
	
	public Date getDataFechamento() {
		return dataFechamento;
	}

	public void setDataFechamento(Date dataFechamento) {
		this.dataFechamento = dataFechamento;
	}
	public Double getValorCalculado() {
		return valorCalculado;
	}

	public void setValorCalculado(Double valorCalculado) {
	
		if (valorCalculado != null && Double.isNaN(valorCalculado))
		{
			this.valorCalculado = null;
		}
		else
		{
			this.valorCalculado = valorCalculado;
		}
		
	}	
	public Double getValorInformado() {
		return valorInformado;
	}

	public void setValorInformado(Double valorInformado) {
	
		if (valorInformado != null && Double.isNaN(valorInformado))
		{
			this.valorInformado = null;
		}
		else
		{
			this.valorInformado = valorInformado;
		}
		
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
