package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="MotivoMovimentoBoleto")


public class MotivoMovimentoBoleto extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idMotivoMovimentoBoleto")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idMotivoOcorrenciaBoletoBancario")
	private MotivoOcorrenciaBoletoBancario motivoOcorrenciaBoletoBancario;
	@ManyToOne
	@JoinColumn(name = "idMovimentoBoletoBancario")
	private MovimentoBoletoBancario movimentoBoletoBancario;
	@ManyToOne
	@JoinColumn(name = "idOcorrenciaBoletoBancario")
	private OcorrenciaBoletoBancario ocorrenciaBoletoBancario;
	public MotivoMovimentoBoleto()
	{
		super();
	}
	

	public MotivoMovimentoBoleto
	(
			MotivoOcorrenciaBoletoBancario motivoOcorrenciaBoletoBancario,
			MovimentoBoletoBancario movimentoBoletoBancario,
			OcorrenciaBoletoBancario ocorrenciaBoletoBancario
	) 
	{
		super();
		this.motivoOcorrenciaBoletoBancario = motivoOcorrenciaBoletoBancario;
		this.movimentoBoletoBancario = movimentoBoletoBancario;
		this.ocorrenciaBoletoBancario = ocorrenciaBoletoBancario;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public MotivoOcorrenciaBoletoBancario getMotivoOcorrenciaBoletoBancario() {
		return motivoOcorrenciaBoletoBancario;
	}

	public void setMotivoOcorrenciaBoletoBancario(MotivoOcorrenciaBoletoBancario motivoOcorrenciaBoletoBancario) {
		this.motivoOcorrenciaBoletoBancario = motivoOcorrenciaBoletoBancario;
	}
	
	public MovimentoBoletoBancario getMovimentoBoletoBancario() {
		return movimentoBoletoBancario;
	}

	public void setMovimentoBoletoBancario(MovimentoBoletoBancario movimentoBoletoBancario) {
		this.movimentoBoletoBancario = movimentoBoletoBancario;
	}
	
	public OcorrenciaBoletoBancario getOcorrenciaBoletoBancario() {
		return ocorrenciaBoletoBancario;
	}


	public void setOcorrenciaBoletoBancario(
			OcorrenciaBoletoBancario ocorrenciaBoletoBancario) {
		this.ocorrenciaBoletoBancario = ocorrenciaBoletoBancario;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
