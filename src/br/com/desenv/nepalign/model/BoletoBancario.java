package br.com.desenv.nepalign.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name = "boletobancario")
public class BoletoBancario extends GenericModelIGN {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idBoletoBancario")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "idNotaFiscal")
	private NotaFiscal notaFiscal;

	@ManyToOne
	@JoinColumn(name = "idContaCorrente")
	private ContaCorrente contaCorrente;

	@Column(name = "dataGeracao")
	private Date dataGeracao;

	@Column(name = "dataVencimento")
	private Date dataVencimento;

	@Column(name = "dataPagamento")
	private Date dataPagamento;

	@Column(name = "valorOriginal")
	private Double valorOriginal;

	@Column(name = "valorPago")
	private Double valorPago;

	@Column(name = "nossoNumero")
	private Integer nossoNumero;

	@Column(name = "numeroDocumento")
	private Integer numeroDocumento;

	@Column(name = "idCupomFiscal")
	private Integer cupomFiscal;

	@Column(name = "linhaDigitavel")
	private String linhaDigitavel;

	@Column(name = "codigoBarras")
	private String codigoBarras;

	@Column(name = "codigoPosto")
	private String codigoPosto;

	@Column(name = "codigoCedente")
	private String codigoCedente;

	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;

	@ManyToOne
	@JoinColumn(name = "idDuplicataParcela")
	private DuplicataParcela duplicataParcela;

	@Column(name = "clienteCache")
	private String clienteCache;

	@Column(name = "duplicataCache")
	private Integer duplicataCache;

	@Column(name = "parcelaDuplicataCache")
	private Integer parcelaDuplicataCache;

	@Column(name = "sequencialBoleto")
	private Integer sequencialBoleto;

	@Column(name = "nomeSacado")
	private String nomeSacado;

	@Column(name = "cpfCnpjSacado")
	private String cpfCnpjSacado;

	@Column(name = "logradouroSacado")
	private String logradouroSacado;

	@Column(name = "numeroLogradouroSacado")
	private Integer numeroLogradouroSacado;

	@Column(name = "complementoLogradoruSacado")
	private String complementoLogradoruSacado;

	@Column(name = "cepSacado")
	private Integer cepSacado;

	@Column(name = "observacao1")
	private String observacao1;

	@Column(name = "observacao2")
	private String observacao2;

	@Column(name = "observacao3")
	private String observacao3;

	@Column(name = "valorDesconto")
	private Double valorDesconto;

	@Column(name = "valorJurosDiario")
	private Double valorJurosDiario;

	@Column(name = "cidadeSacado")
	private String cidadeSacado;

	@Column(name = "estadoSacado")
	private String estadoSacado;

	@Column(name = "bairroSacado")
	private String bairroSacado;

	@Column(name = "situacaoEnvio")
	private Integer situacaoEnvio;
	
	@Column(name = "valorAcrescimos")
	private Double valorAcrescimos;	

	@ManyToOne 
	@JoinColumn(name = "idOcorrenciaBoletoBancario")
	private OcorrenciaBoletoBancario ocorrenciaBoletoBancario;
	
	public BoletoBancario() {
		super();
	}

	public BoletoBancario(NotaFiscal notaFiscal, ContaCorrente contaCorrente,
			Date dataGeracao, Date dataVencimento, Date dataPagamento,
			Double valorOriginal, Double valorPago, Integer nossoNumero,
			Integer numeroDocumento, Integer cupomFiscal,
			String linhaDigitavel, String codigoBarras, String codigoPosto,
			String codigoCedente, Cliente cliente,
			DuplicataParcela duplicataParcela, String clienteCache,
			Integer duplicataCache, Integer parcelaDuplicataCache,
			Integer sequencialBoleto, String nomeSacado, String cpfCnpjSacado,
			String logradouroSacado, Integer numeroLogradouroSacado,
			String complementoLogradoruSacado, Integer cepSacado,
			String observacao1, String observacao2, String observacao3,
			Double valorDesconto, Double valorJurosDiario, String cidadeSacado,
			String estadoSacado, String bairroSacado, Integer situacaoEnvio, Double valorAcrescimos,
			OcorrenciaBoletoBancario ocorrenciaBoletoBancario) {
		super();
		this.notaFiscal = notaFiscal;
		this.contaCorrente = contaCorrente;
		this.dataGeracao = dataGeracao;
		this.dataVencimento = dataVencimento;
		this.dataPagamento = dataPagamento;
		this.valorOriginal = valorOriginal;
		this.valorPago = valorPago;
		this.nossoNumero = nossoNumero;
		this.numeroDocumento = numeroDocumento;
		this.cupomFiscal = cupomFiscal;
		this.linhaDigitavel = linhaDigitavel;
		this.codigoBarras = codigoBarras;
		this.codigoPosto = codigoPosto;
		this.codigoCedente = codigoCedente;
		this.cliente = cliente;
		this.duplicataParcela = duplicataParcela;
		this.clienteCache = clienteCache;
		this.duplicataCache = duplicataCache;
		this.parcelaDuplicataCache = parcelaDuplicataCache;
		this.sequencialBoleto = sequencialBoleto;
		this.nomeSacado = nomeSacado;
		this.cpfCnpjSacado = cpfCnpjSacado;
		this.logradouroSacado = logradouroSacado;
		this.numeroLogradouroSacado = numeroLogradouroSacado;
		this.complementoLogradoruSacado = complementoLogradoruSacado;
		this.cepSacado = cepSacado;
		this.observacao1 = observacao1;
		this.observacao2 = observacao2;
		this.observacao3 = observacao3;
		this.valorDesconto = valorDesconto;
		this.valorJurosDiario = valorJurosDiario;
		this.cidadeSacado = cidadeSacado;
		this.estadoSacado = estadoSacado;
		this.bairroSacado = bairroSacado;
		this.situacaoEnvio = situacaoEnvio;
		this.valorAcrescimos = valorAcrescimos;
		this.ocorrenciaBoletoBancario = ocorrenciaBoletoBancario;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public NotaFiscal getNotaFiscal() {
		return notaFiscal;
	}

	public void setNotaFiscal(NotaFiscal notaFiscal) {
		this.notaFiscal = notaFiscal;
	}

	public ContaCorrente getContaCorrente() {
		return contaCorrente;
	}

	public void setContaCorrente(ContaCorrente contaCorrente) {
		this.contaCorrente = contaCorrente;
	}

	public Date getDataGeracao() {
		return dataGeracao;
	}

	public void setDataGeracao(Date dataGeracao) {
		this.dataGeracao = dataGeracao;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public Double getValorOriginal() {
		return valorOriginal;
	}

	public void setValorOriginal(Double valorOriginal) {

		if (valorOriginal != null && Double.isNaN(valorOriginal)) {
			this.valorOriginal = null;
		} else {
			this.valorOriginal = valorOriginal;
		}

	}

	public Double getValorPago() {
		return valorPago;
	}

	public void setValorPago(Double valorPago) {

		if (valorPago != null && Double.isNaN(valorPago)) {
			this.valorPago = null;
		} else {
			this.valorPago = valorPago;
		}

	}

	public Integer getNossoNumero() {
		return nossoNumero;
	}

	public void setNossoNumero(Integer nossoNumero) {
		this.nossoNumero = nossoNumero;
	}

	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(Integer numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public Integer getCupomFiscal() {
		return cupomFiscal;
	}

	public void setCupomFiscal(Integer cupomFiscal) {
		this.cupomFiscal = cupomFiscal;
	}

	public String getLinhaDigitavel() {
		String retorno = null;

		if (linhaDigitavel != null) {
			retorno = linhaDigitavel.toUpperCase().trim();
		}

		return retorno;
	}

	public void setLinhaDigitavel(String linhaDigitavel) {

		if (linhaDigitavel != null) {
			this.linhaDigitavel = linhaDigitavel.toUpperCase().trim();
		} else {
			this.linhaDigitavel = null;
		}

	}

	public String getCodigoBarras() {
		String retorno = null;

		if (codigoBarras != null) {
			retorno = codigoBarras.toUpperCase().trim();
		}

		return retorno;
	}

	public void setCodigoBarras(String codigoBarras) {

		if (codigoBarras != null) {
			this.codigoBarras = codigoBarras.toUpperCase().trim();
		} else {
			this.codigoBarras = null;
		}

	}

	public String getCodigoPosto() {
		String retorno = null;

		if (codigoPosto != null) {
			retorno = codigoPosto.toUpperCase().trim();
		}

		return retorno;
	}

	public void setCodigoPosto(String codigoPosto) {

		if (codigoPosto != null) {
			this.codigoPosto = codigoPosto.toUpperCase().trim();
		} else {
			this.codigoPosto = null;
		}

	}

	public String getCodigoCedente() {
		String retorno = null;

		if (codigoCedente != null) {
			retorno = codigoCedente.toUpperCase().trim();
		}

		return retorno;
	}

	public void setCodigoCedente(String codigoCedente) {

		if (codigoCedente != null) {
			this.codigoCedente = codigoCedente.toUpperCase().trim();
		} else {
			this.codigoCedente = null;
		}

	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public DuplicataParcela getDuplicataParcela() {
		return duplicataParcela;
	}

	public void setDuplicataParcela(DuplicataParcela duplicataParcela) {
		this.duplicataParcela = duplicataParcela;
	}

	public String getClienteCache() {
		return clienteCache;
	}

	public void setClienteCache(String clienteCache) {
		this.clienteCache = clienteCache;
	}

	public Integer getDuplicataCache() {
		return duplicataCache;
	}

	public void setDuplicataCache(Integer duplicataCache) {
		this.duplicataCache = duplicataCache;
	}

	public Integer getParcelaDuplicataCache() {
		return parcelaDuplicataCache;
	}

	public void setParcelaDuplicataCache(Integer parcelaDuplicataCache) {
		this.parcelaDuplicataCache = parcelaDuplicataCache;
	}

	public Integer getSequencialBoleto() {
		return sequencialBoleto;
	}

	public void setSequencialBoleto(Integer sequencialBoleto) {
		this.sequencialBoleto = sequencialBoleto;
	}

	public String getNomeSacado() {
		return nomeSacado;
	}

	public void setNomeSacado(String nomeSacado) {
		this.nomeSacado = nomeSacado;
	}

	public String getCpfCnpjSacado() {
		return cpfCnpjSacado;
	}

	public void setCpfCnpjSacado(String cpfCnpjSacado) {
		this.cpfCnpjSacado = cpfCnpjSacado;
	}

	public String getLogradouroSacado() {
		return logradouroSacado;
	}

	public void setLogradouroSacado(String logradouroSacado) {
		this.logradouroSacado = logradouroSacado;
	}

	public Integer getNumeroLogradouroSacado() {
		return numeroLogradouroSacado;
	}

	public void setNumeroLogradouroSacado(Integer numeroLogradouroSacado) {
		this.numeroLogradouroSacado = numeroLogradouroSacado;
	}

	public String getComplementoLogradoruSacado() {
		return complementoLogradoruSacado;
	}

	public void setComplementoLogradoruSacado(String complementoLogradoruSacado) {
		this.complementoLogradoruSacado = complementoLogradoruSacado;
	}

	public Integer getCepSacado() {
		return cepSacado;
	}

	public void setCepSacado(Integer cepSacado) {
		this.cepSacado = cepSacado;
	}

	public String getObservacao1() {
		return observacao1;
	}

	public void setObservacao1(String observacao1) {
		this.observacao1 = observacao1;
	}

	public String getObservacao2() {
		return observacao2;
	}

	public void setObservacao2(String observacao2) {
		this.observacao2 = observacao2;
	}

	public String getObservacao3() {
		return observacao3;
	}

	public void setObservacao3(String observacao3) {
		this.observacao3 = observacao3;
	}

	public Double getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(Double valorDesconto) {
		this.valorDesconto = valorDesconto;
	}

	public Double getValorJurosDiario() {
		return valorJurosDiario;
	}

	public void setValorJurosDiario(Double valorJurosDiario) {
		this.valorJurosDiario = valorJurosDiario;
	}

	public String getCidadeSacado() {
		return cidadeSacado;
	}

	public void setCidadeSacado(String cidadeSacado) {
		this.cidadeSacado = cidadeSacado;
	}

	public String getEstadoSacado() {
		return estadoSacado;
	}

	public void setEstadoSacado(String estadoSacado) {
		this.estadoSacado = estadoSacado;
	}

	public String getBairroSacado() {
		return bairroSacado;
	}

	public void setBairroSacado(String bairroSacado) {
		this.bairroSacado = bairroSacado;
	}

	public Integer getSituacaoEnvio() {
		return situacaoEnvio;
	}

	public void setSituacaoEnvio(Integer situacaoEnvio) {
		this.situacaoEnvio = situacaoEnvio;
	}

	public Double getValorAcrescimos() {
		return valorAcrescimos;
	}

	public void setValorAcrescimos(Double valorAcrescimos) {
		this.valorAcrescimos = valorAcrescimos;
	}	
	
	public OcorrenciaBoletoBancario getOcorrenciaBoletoBancario() {
		return ocorrenciaBoletoBancario;
	}

	public void setOcorrenciaBoletoBancario(
			OcorrenciaBoletoBancario ocorrenciaBoletoBancario) {
		this.ocorrenciaBoletoBancario = ocorrenciaBoletoBancario;
	}

	@Override
	public void validate() throws Exception {
		// Coloque aqui suas validações ignorantes
		/*
		 * if (this.descricao == null || this.descricao.trim().equals("")) {
		 * throw new Exception("Descrição é obrigatório."); }
		 */

	}

}
