package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="vitrinelvproduto")


public class VitrineLvProduto extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idVitrineLvProduto")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idVitrineLv")
	private VitrineLv vitrineLv;
	@ManyToOne
	@JoinColumn(name = "idEstoqueProduto")
	private EstoqueProduto estoqueProduto;
	@JoinColumn(name = "produtoDestaque")
	private String produtoDestaque;
	@JoinColumn(name = "orderExibicao")
	private Integer ordemExibicao; 
	@ManyToOne
	@JoinColumn(name = "idSituacaoEnvioLV")
	private SituacaoEnvioLV situacaoEnvioLv;

	public VitrineLvProduto()
	{
		super();
	}
	

	public VitrineLvProduto
	(
	VitrineLv vitrineLv,
	EstoqueProduto estoqueProduto,
	SituacaoEnvioLV situacaoEnvioLv
	) 
	{
		super();
		this.vitrineLv = vitrineLv;
		this.estoqueProduto = estoqueProduto;
		this.situacaoEnvioLv = situacaoEnvioLv;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public VitrineLv getVitrineLv() {
		return vitrineLv;
	}

	public void setVitrineLv(VitrineLv vitrineLv) {
		this.vitrineLv = vitrineLv;
	}
	
	public EstoqueProduto getEstoqueProduto() {
		return estoqueProduto;
	}

	public void setEstoqueProduto(EstoqueProduto estoqueProduto) {
		this.estoqueProduto = estoqueProduto;
	}
	
	public String getProdutoDestaque() {
		return produtoDestaque;
	}


	public void setProdutoDestaque(String produtoDestaque) {
		this.produtoDestaque = produtoDestaque;
	}


	public Integer getOrdemExibicao() {
		return ordemExibicao;
	}


	public void setOrdemExibicao(Integer ordemExibicao) {
		this.ordemExibicao = ordemExibicao;
	}


	public SituacaoEnvioLV getSituacaoEnvioLv() {
		return situacaoEnvioLv;
	}

	public void setSituacaoEnvioLv(SituacaoEnvioLV situacaoEnvioLv) {
		this.situacaoEnvioLv = situacaoEnvioLv;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
