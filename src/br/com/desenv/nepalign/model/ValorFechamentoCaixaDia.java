package br.com.desenv.nepalign.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="valorfechamentocaixadia")


public class ValorFechamentoCaixaDia extends GenericModelIGN implements Cloneable
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idValorFechamentoCaixaDia")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idCaixaDia")
	private CaixaDia caixadia;
	@ManyToOne
	@JoinColumn(name = "idFormaPagamento")
	private FormaPagamento formaPagamento;
	@Column(name="dataFechamento")
	private Date dataFechamento;
	
	@Column(name="valorCalculado")
	private Double valorCalculado;
	
	@Column(name="valorInformado")
	private Double valorInformado;
	

	public ValorFechamentoCaixaDia()
	{
		super();
	}
	

	public ValorFechamentoCaixaDia
	(
	CaixaDia caixadia,
	FormaPagamento formaPagamento,
		Date dataFechamento,
		Double valorCalculado,
		Double valorInformado
	) 
	{
		super();
		this.caixadia = caixadia;
		this.formaPagamento = formaPagamento;
		this.dataFechamento = dataFechamento;
		this.valorCalculado = valorCalculado;
		this.valorInformado = valorInformado;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public CaixaDia getCaixadia() {
		return caixadia;
	}

	public void setCaixadia(CaixaDia caixadia) {
		this.caixadia = caixadia;
	}
	
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
	public Date getDataFechamento() {
		
		return dataFechamento;
	}

	public void setDataFechamento(Date dataFechamento) {
		this.dataFechamento = dataFechamento;
	}
	
	public Double getValorCalculado() {
		return valorCalculado;
	}

	public void setValorCalculado(Double valorCalculado) {
	
		if (valorCalculado != null && Double.isNaN(valorCalculado))
		{
			this.valorCalculado = null;
		}
		else
		{
			this.valorCalculado = valorCalculado;
		}
		
	}	
	public Double getValorInformado() {
		return valorInformado;
	}

	public void setValorInformado(Double valorInformado) {
	
		if (valorInformado != null && Double.isNaN(valorInformado))
		{
			this.valorInformado = null;
		}
		else
		{
			this.valorInformado = valorInformado;
		}
		
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}
	
	public ValorFechamentoCaixaDia clone() throws CloneNotSupportedException
	{
		return (ValorFechamentoCaixaDia) super.clone();
	}
}