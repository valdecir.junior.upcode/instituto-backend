package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="itemnotafiscal")


public class ItemNotaFiscal extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idItemNotaFiscal")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idNotaFiscal")
	private NotaFiscal notaFiscal;
	@ManyToOne
	@JoinColumn(name = "idProduto")
	private Produto produto;
	@Column(name="quantidade")
	private Double quantidade;
	
	@ManyToOne
	@JoinColumn(name = "idUnidadeMedida")
	private UnidadeMedida unidadeMedida;
	@Column(name="valorUnitario")
	private Double valorUnitario;
	
	@Column(name="valorTotal")
	private Double valorTotal;
	
	@Column(name="aliquotaICMS")
	private Double aliquotaICMS;
	
	@Column(name="aliquotaIPI")
	private Double aliquotaIPI;
	
	@Column(name="valorIPI")
	private Double valorIPI;
	
	@Column(name="lote")
	private String lote;
	
	@Column(name="validade")
	private Date validade;
	
	@ManyToOne
	@JoinColumn(name = "idItemMovimentacaoEstoque")
	private ItemMovimentacaoEstoque itemMovimentacaoEstoque;
	@Column(name="percPis")
	private Double percPis;
	
	@Column(name="percCofins")
	private Double percCofins;
	
	@Column(name="percCsll")
	private Double percCsll;
	
	@Column(name="percIrrf")
	private Double percIrrf;
	
	@Column(name="percReducaoBaseCalc")
	private Double percReducaoBaseCalc;
	
	@Column(name="margemLucro")
	private Double margemLucro;
	
	@ManyToOne
	@JoinColumn(name = "idCodigoSituacaoTributaria")
	private CodigoSituacaoTributaria codigoSituacaoTributaria;
	@ManyToOne
	@JoinColumn(name = "idOrigemMercadoria")
	private OrigemMercadoria origemMercadoria;
	@Column(name="valorFrete")
	private Double valorFrete;
	
	@Column(name="valorSeguro")
	private Double valorSeguro;
	
	@Column(name="dataFabricacao")
	private Date dataFabricacao;
	
	@Column(name="valorBaseCalcICMS")
	private Double valorBaseCalcICMS;
	
	@Column(name="valorBaseCalcICMSST")
	private Double valorBaseCalcICMSST;
	
	@Column(name="valorICMS")
	private Double valorICMS;
	
	@Column(name="valorICMSST")
	private Double valorICMSST;
	
	@Column(name="PMC")
	private Double pMC;
	
	@Column(name="percReducaoBaseCalcST")
	private Double percReducaoBaseCalcST;
	
	@Column(name="codigoCFOP")
	private String codigoCFOP;
	
	@Column(name="sequenciaItem")
	private Integer sequenciaItem;
	
	@Column(name="cstPis")
	private Integer cstPis;
	
	@Column(name="cstCofins")
	private Integer cstCofins;
	
	@Column(name="valorDesconto")
	private Double valorDesconto;
	
	@ManyToOne
	@JoinColumn(name = "idCfop")
	private Cfop cfop;
	
	@ManyToOne
	@JoinColumn(name = "idCodigoSituacaoTributariaICMS")
	private CodigoSituacaoTributaria codigoSituacaoTributariaICMS;
	
	@ManyToOne
	@JoinColumn(name = "idCodigoSituacaoTributariaCOFINS")
	private Cstcofins codigoSituacaoTributariaCOFINS;
	
	@ManyToOne
	@JoinColumn(name = "idCodigoSituacaoTributariaPIS")
	private Cstpis codigoSituacaoTributariaPIS;
	
	@Column(name="aliquotaCOFINS")
	private Double aliquotaCOFINS;
	
	@Column(name="fabricacao")
	private Date fabricacao;
	
	@Column(name="aliquotaPIS")
	private Double aliquotaPIS;
	

	public ItemNotaFiscal()
	{
		super();
	}
	

	public ItemNotaFiscal
	(
	NotaFiscal notaFiscal,
	Produto produto,
		Double quantidade,
	UnidadeMedida unidadeMedida,
		Double valorUnitario,
		Double valorTotal,
		Double aliquotaICMS,
		Double aliquotaIPI,
		Double valorIPI,
		String lote,
		Date validade,
	ItemMovimentacaoEstoque itemMovimentacaoEstoque,
		Double percPis,
		Double percCofins,
		Double percCsll,
		Double percIrrf,
		Double percReducaoBaseCalc,
		Double margemLucro,
	CodigoSituacaoTributaria codigoSituacaoTributaria,
	OrigemMercadoria origemMercadoria,
		Double valorFrete,
		Double valorSeguro,
		Date dataFabricacao,
		Double valorBaseCalcICMS,
		Double valorBaseCalcICMSST,
		Double valorICMS,
		Double valorICMSST,
		Double pMC,
		Double percReducaoBaseCalcST,
		String codigoCFOP,
		Integer sequenciaItem,
		Integer cstPis,
		Integer cstCofins,
		Double valorDesconto,
		Cfop cfop,
		CodigoSituacaoTributaria codigoSituacaoTributariaICMS,
		Cstcofins codigoSituacaoTributariaCOFINS,
		Cstpis codigoSituacaoTributariaPIS,
		Double aliquotaCOFINS,
		Date fabricacao,
		Double aliquotaPIS
	) 
	{
		super();
		this.notaFiscal = notaFiscal;
		this.produto = produto;
		this.quantidade = quantidade;
		this.unidadeMedida = unidadeMedida;
		this.valorUnitario = valorUnitario;
		this.valorTotal = valorTotal;
		this.aliquotaICMS = aliquotaICMS;
		this.aliquotaIPI = aliquotaIPI;
		this.valorIPI = valorIPI;
		this.lote = lote;
		this.validade = validade;
		this.itemMovimentacaoEstoque = itemMovimentacaoEstoque;
		this.percPis = percPis;
		this.percCofins = percCofins;
		this.percCsll = percCsll;
		this.percIrrf = percIrrf;
		this.percReducaoBaseCalc = percReducaoBaseCalc;
		this.margemLucro = margemLucro;
		this.codigoSituacaoTributaria = codigoSituacaoTributaria;
		this.origemMercadoria = origemMercadoria;
		this.valorFrete = valorFrete;
		this.valorSeguro = valorSeguro;
		this.dataFabricacao = dataFabricacao;
		this.valorBaseCalcICMS = valorBaseCalcICMS;
		this.valorBaseCalcICMSST = valorBaseCalcICMSST;
		this.valorICMS = valorICMS;
		this.valorICMSST = valorICMSST;
		this.pMC = pMC;
		this.percReducaoBaseCalcST = percReducaoBaseCalcST;
		this.codigoCFOP = codigoCFOP;
		this.sequenciaItem = sequenciaItem;
		this.cstPis = cstPis;
		this.cstCofins = cstCofins;
		this.valorDesconto = valorDesconto;
		this.cfop = cfop;
		this.codigoSituacaoTributariaICMS = codigoSituacaoTributariaICMS;
		this.codigoSituacaoTributariaCOFINS = codigoSituacaoTributariaCOFINS;
		this.codigoSituacaoTributariaPIS = codigoSituacaoTributariaPIS;
		this.aliquotaCOFINS = aliquotaCOFINS;
		this.fabricacao = fabricacao;
		this.aliquotaPIS = aliquotaPIS;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public NotaFiscal getNotaFiscal() {
		return notaFiscal;
	}

	public void setNotaFiscal(NotaFiscal notaFiscal) {
		this.notaFiscal = notaFiscal;
	}
	
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	public Double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Double quantidade) {
	
		if (quantidade != null && Double.isNaN(quantidade))
		{
			this.quantidade = null;
		}
		else
		{
			this.quantidade = quantidade;
		}
		
	}	
	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	
	public Double getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(Double valorUnitario) {
	
		if (valorUnitario != null && Double.isNaN(valorUnitario))
		{
			this.valorUnitario = null;
		}
		else
		{
			this.valorUnitario = valorUnitario;
		}
		
	}	
	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
	
		if (valorTotal != null && Double.isNaN(valorTotal))
		{
			this.valorTotal = null;
		}
		else
		{
			this.valorTotal = valorTotal;
		}
		
	}	
	public Double getAliquotaICMS() {
		return aliquotaICMS;
	}

	public void setAliquotaICMS(Double aliquotaICMS) {
	
		if (aliquotaICMS != null && Double.isNaN(aliquotaICMS))
		{
			this.aliquotaICMS = null;
		}
		else
		{
			this.aliquotaICMS = aliquotaICMS;
		}
		
	}	
	public Double getAliquotaIPI() {
		return aliquotaIPI;
	}

	public void setAliquotaIPI(Double aliquotaIPI) {
	
		if (aliquotaIPI != null && Double.isNaN(aliquotaIPI))
		{
			this.aliquotaIPI = null;
		}
		else
		{
			this.aliquotaIPI = aliquotaIPI;
		}
		
	}	
	
	public Double getValorIPI() {
		return valorIPI;
	}


	public void setValorIPI(Double valorIPI) {
		if (valorIPI != null && Double.isNaN(valorIPI))
		{
			this.valorIPI = null;
		}
		else
		{
			this.valorIPI = valorIPI;
		}
	}


	public String getLote() {
		String retorno = null;
		
		if (lote != null)
			retorno = lote.toUpperCase().trim();
		return retorno;
	}
	
	public void setLote(String lote) {
		if (lote != null)
		{
			this.lote = lote.toUpperCase().trim();
		}
		else
			this.lote = null;
			
		
	}
		
	public Date getValidade() {
		return validade;
	}

	public void setValidade(Date validade) {
		this.validade = validade;
	}
	public ItemMovimentacaoEstoque getItemMovimentacaoEstoque() {
		return itemMovimentacaoEstoque;
	}

	public void setItemMovimentacaoEstoque(ItemMovimentacaoEstoque itemMovimentacaoEstoque) {
		this.itemMovimentacaoEstoque = itemMovimentacaoEstoque;
	}
	
	public Double getPercPis() {
		return percPis;
	}

	public void setPercPis(Double percPis) {
	
		if (percPis != null && Double.isNaN(percPis))
		{
			this.percPis = null;
		}
		else
		{
			this.percPis = percPis;
		}
		
	}	
	public Double getPercCofins() {
		return percCofins;
	}

	public void setPercCofins(Double percCofins) {
	
		if (percCofins != null && Double.isNaN(percCofins))
		{
			this.percCofins = null;
		}
		else
		{
			this.percCofins = percCofins;
		}
		
	}	
	public Double getPercCsll() {
		return percCsll;
	}

	public void setPercCsll(Double percCsll) {
	
		if (percCsll != null && Double.isNaN(percCsll))
		{
			this.percCsll = null;
		}
		else
		{
			this.percCsll = percCsll;
		}
		
	}	
	public Double getPercIrrf() {
		return percIrrf;
	}

	public void setPercIrrf(Double percIrrf) {
	
		if (percIrrf != null && Double.isNaN(percIrrf))
		{
			this.percIrrf = null;
		}
		else
		{
			this.percIrrf = percIrrf;
		}
		
	}	
	public Double getPercReducaoBaseCalc() {
		return percReducaoBaseCalc;
	}

	public void setPercReducaoBaseCalc(Double percReducaoBaseCalc) {
	
		if (percReducaoBaseCalc != null && Double.isNaN(percReducaoBaseCalc))
		{
			this.percReducaoBaseCalc = null;
		}
		else
		{
			this.percReducaoBaseCalc = percReducaoBaseCalc;
		}
		
	}	
	public Double getMargemLucro() {
		return margemLucro;
	}

	public void setMargemLucro(Double margemLucro) {
	
		if (margemLucro != null && Double.isNaN(margemLucro))
		{
			this.margemLucro = null;
		}
		else
		{
			this.margemLucro = margemLucro;
		}
		
	}	
	public CodigoSituacaoTributaria getCodigoSituacaoTributaria() {
		return codigoSituacaoTributaria;
	}

	public void setCodigoSituacaoTributaria(CodigoSituacaoTributaria codigoSituacaoTributaria) {
		this.codigoSituacaoTributaria = codigoSituacaoTributaria;
	}
	
	public OrigemMercadoria getOrigemMercadoria() {
		return origemMercadoria;
	}

	public void setOrigemMercadoria(OrigemMercadoria origemMercadoria) {
		this.origemMercadoria = origemMercadoria;
	}
	
	public Double getValorFrete() {
		return valorFrete;
	}

	public void setValorFrete(Double valorFrete) {
	
		if (valorFrete != null && Double.isNaN(valorFrete))
		{
			this.valorFrete = null;
		}
		else
		{
			this.valorFrete = valorFrete;
		}
		
	}	
	public Double getValorSeguro() {
		return valorSeguro;
	}

	public void setValorSeguro(Double valorSeguro) {
	
		if (valorSeguro != null && Double.isNaN(valorSeguro))
		{
			this.valorSeguro = null;
		}
		else
		{
			this.valorSeguro = valorSeguro;
		}
		
	}	
	public Date getDataFabricacao() {
		return dataFabricacao;
	}

	public void setDataFabricacao(Date dataFabricacao) {
		this.dataFabricacao = dataFabricacao;
	}
	public Double getValorBaseCalcICMS() {
		return valorBaseCalcICMS;
	}

	public void setValorBaseCalcICMS(Double valorBaseCalcICMS) {
	
		if (valorBaseCalcICMS != null && Double.isNaN(valorBaseCalcICMS))
		{
			this.valorBaseCalcICMS = null;
		}
		else
		{
			this.valorBaseCalcICMS = valorBaseCalcICMS;
		}
		
	}	
	public Double getValorBaseCalcICMSST() {
		return valorBaseCalcICMSST;
	}

	public void setValorBaseCalcICMSST(Double valorBaseCalcICMSST) {
	
		if (valorBaseCalcICMSST != null && Double.isNaN(valorBaseCalcICMSST))
		{
			this.valorBaseCalcICMSST = null;
		}
		else
		{
			this.valorBaseCalcICMSST = valorBaseCalcICMSST;
		}
		
	}	
	public Double getValorICMS() {
		return valorICMS;
	}

	public void setValorICMS(Double valorICMS) {
	
		if (valorICMS != null && Double.isNaN(valorICMS))
		{
			this.valorICMS = null;
		}
		else
		{
			this.valorICMS = valorICMS;
		}
		
	}	
	public Double getValorICMSST() {
		return valorICMSST;
	}

	public void setValorICMSST(Double valorICMSST) {
	
		if (valorICMSST != null && Double.isNaN(valorICMSST))
		{
			this.valorICMSST = null;
		}
		else
		{
			this.valorICMSST = valorICMSST;
		}
		
	}	
	public Double getPMC() {
		return pMC;
	}

	public void setPMC(Double pMC) {
	
		if (pMC != null && Double.isNaN(pMC))
		{
			this.pMC = null;
		}
		else
		{
			this.pMC = pMC;
		}
		
	}	
	public Double getPercReducaoBaseCalcST() {
		return percReducaoBaseCalcST;
	}

	public void setPercReducaoBaseCalcST(Double percReducaoBaseCalcST) {
	
		if (percReducaoBaseCalcST != null && Double.isNaN(percReducaoBaseCalcST))
		{
			this.percReducaoBaseCalcST = null;
		}
		else
		{
			this.percReducaoBaseCalcST = percReducaoBaseCalcST;
		}
		
	}	
	public String getCodigoCFOP() {
		String retorno = null;
		
		if (codigoCFOP != null)
			retorno = codigoCFOP.toUpperCase().trim();
		return retorno;
	}
	
	public void setCodigoCFOP(String codigoCFOP) {
		if (codigoCFOP != null)
		{
			this.codigoCFOP = codigoCFOP.toUpperCase().trim();
		}
		else
			this.codigoCFOP = null;
			
		
	}
		
	public Integer getSequenciaItem() {
		return sequenciaItem;
	}

	public void setSequenciaItem(Integer sequenciaItem) {
		this.sequenciaItem = sequenciaItem;
	}	
	public Integer getCstPis() {
		return cstPis;
	}

	public void setCstPis(Integer cstPis) {
		this.cstPis = cstPis;
	}	
	public Integer getCstCofins() {
		return cstCofins;
	}

	public void setCstCofins(Integer cstCofins) {
		this.cstCofins = cstCofins;
	}	
	public Double getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(Double valorDesconto) {
	
		if (valorDesconto != null && Double.isNaN(valorDesconto))
		{
			this.valorDesconto = null;
		}
		else
		{
			this.valorDesconto = valorDesconto;
		}
		
	}	
	public Cfop getCfop() {
		return cfop;
	}

	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	
	public CodigoSituacaoTributaria getCodigoSituacaoTributariaICMS() {
		return codigoSituacaoTributariaICMS;
	}

	public void setCodigoSituacaoTributariaICMS(CodigoSituacaoTributaria codigoSituacaoTributariaICMS) {
		this.codigoSituacaoTributariaICMS = codigoSituacaoTributariaICMS;
	}
	
	public Cstcofins getCodigoSituacaoTributariaCOFINS() {
		return codigoSituacaoTributariaCOFINS;
	}

	public void setCodigoSituacaoTributariaCOFINS(Cstcofins codigoSituacaoTributariaCOFINS) {
		this.codigoSituacaoTributariaCOFINS = codigoSituacaoTributariaCOFINS;
	}
	
	public Cstpis getCodigoSituacaoTributariaPIS() {
		return codigoSituacaoTributariaPIS;
	}

	public void setCodigoSituacaoTributariaPIS(Cstpis CodigoSituacaoTributariaPIS) {
		this.codigoSituacaoTributariaPIS = CodigoSituacaoTributariaPIS;
	}
	
	public Double getAliquotaCOFINS() {
		return aliquotaCOFINS;
	}

	public void setAliquotaCOFINS(Double aliquotaCOFINS) {
	
		if (aliquotaCOFINS != null && Double.isNaN(aliquotaCOFINS))
		{
			this.aliquotaCOFINS = null;
		}
		else
		{
			this.aliquotaCOFINS = aliquotaCOFINS;
		}
		
	}	
	public Date getFabricacao() {
		return fabricacao;
	}

	public void setFabricacao(Date fabricacao) {
		this.fabricacao = fabricacao;
	}
	public Double getAliquotaPIS() {
		return aliquotaPIS;
	}

	public void setAliquotaPIS(Double aliquotaPIS) {
	
		if (aliquotaPIS != null && Double.isNaN(aliquotaPIS))
		{
			this.aliquotaPIS = null;
		}
		else
		{
			this.aliquotaPIS = aliquotaPIS;
		}
		
	}	
	public Double getpMC() {
		return pMC;
	}


	public void setpMC(Double pMC) {
		if(pMC != null && Double.isNaN(pMC))
		{
			this.pMC = null;	
		}
		else
		{
			this.pMC = pMC;
		}
		
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
