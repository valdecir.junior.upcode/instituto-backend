package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="representante")


public class Representante extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idRepresentante")
	private Integer id;
		
	@Column(name="nome")
	private String nome;
	
	@Column(name = "foto")
	@Basic(fetch = FetchType.EAGER)
	@Lob
	byte[] foto;
	@Column(name="percentualComissao")
	private Double percentualComissao;
	

	public Representante()
	{
		super();
	}
	

	public Representante
	(
		String nome,
		byte[] foto,
		Double percentualComissao
	) 
	{
		super();
		this.nome = nome;
		this.foto = foto;
		this.percentualComissao = percentualComissao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public String getNome() {
		String retorno = null;
		
		if (nome != null)
			retorno = nome.toUpperCase().trim();
		return retorno;
	}
	
	public void setNome(String nome) {
		if (nome != null)
		{
			this.nome = nome.toUpperCase().trim();
		}
		else
			this.nome = null;
			
		
	}
		
	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}	
	public Double getPercentualComissao() {
		return percentualComissao;
	}

	public void setPercentualComissao(Double percentualComissao) {
		if (percentualComissao != null && percentualComissao == 0.0)
		{
			this.percentualComissao = null;
		}
		else
		{
			this.percentualComissao = percentualComissao;
		}
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
