package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="creditocliente")


public class Creditocliente extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idCreditocliente")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@Column(name="idDocumentoOrigem")
	private String idDocumentoOrigem;
	
	@Column(name="valor")
	private Double valor;
	
	@Column(name="dataDocumento")
	private Date dataDocumento;
	
	@Column(name="numeroDocumento")
	private Integer numeroDocumento;
	
	@Column(name="valorUtilizado")
	private Double valorUtilizado;
	
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@Column(name="observacao")
	private String observacao;
	
	@Column(name="origem")
	private String origem;
	

	public Creditocliente()
	{
		super();
	}
	

	public Creditocliente
	(
	Cliente cliente,
		String idDocumentoOrigem,
		Double valor,
		Date dataDocumento,
		Integer numeroDocumento,
		Double valorUtilizado,
	EmpresaFisica empresaFisica,
		String observacao,
		String origem
	) 
	{
		super();
		this.cliente = cliente;
		this.idDocumentoOrigem = idDocumentoOrigem;
		this.valor = valor;
		this.dataDocumento = dataDocumento;
		this.numeroDocumento = numeroDocumento;
		this.valorUtilizado = valorUtilizado;
		this.empresaFisica = empresaFisica;
		this.observacao = observacao;
		this.origem = origem;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public String getIdDocumentoOrigem() {
		String retorno = null;
		
		if (idDocumentoOrigem != null)
			retorno = idDocumentoOrigem.toUpperCase().trim();
		return retorno;
	}
	
	public void setIdDocumentoOrigem(String idDocumentoOrigem) {
		if (idDocumentoOrigem != null)
		{
			this.idDocumentoOrigem = idDocumentoOrigem.toUpperCase().trim();
		}
		else
			this.idDocumentoOrigem = null;
			
		
	}
		
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
	
		if (valor != null && Double.isNaN(valor))
		{
			this.valor = null;
		}
		else
		{
			this.valor = valor;
		}
		
	}	
	public Date getDataDocumento() {
		return dataDocumento;
	}

	public void setDataDocumento(Date dataDocumento) {
		this.dataDocumento = dataDocumento;
	}
	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(Integer numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}	
	public Double getValorUtilizado() {
		return valorUtilizado;
	}

	public void setValorUtilizado(Double valorUtilizado) {
	
		if (valorUtilizado != null && Double.isNaN(valorUtilizado))
		{
			this.valorUtilizado = null;
		}
		else
		{
			this.valorUtilizado = valorUtilizado;
		}
		
	}	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	public String getOrigem() {
		String retorno = null;
		
		if (origem != null)
			retorno = origem.toUpperCase().trim();
		return retorno;
	}
	
	public void setOrigem(String origem) {
		if (origem != null)
		{
			this.origem = origem.toUpperCase().trim();
		}
		else
			this.origem = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
