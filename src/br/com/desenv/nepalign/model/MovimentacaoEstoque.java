package br.com.desenv.nepalign.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="movimentacaoestoque")
public class MovimentacaoEstoque extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "mygenerator", strategy = "br.com.desenv.frameworkignorante.MyGenerator")
	@GeneratedValue(generator = "mygenerator")
	@Column(name = "idMovimentacaoEstoque")
	private Integer id;
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@ManyToOne
	@JoinColumn(name = "idEstoque")
	private Estoque estoque;
	@ManyToOne
	@JoinColumn(name = "idTipoMovimentacaoEstoque")
	private TipoMovimentacaoEstoque tipoMovimentacaoEstoque;
	@ManyToOne
	@JoinColumn(name = "idTipoDocumentoMovimentoEstoque")
	private TipoDocumentoMovimentoEstoque tipoDocumentoMovimento;
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@Column(name="entradaSaida")
	private String entradaSaida;
	@Column(name="numeroDocumento")
	private String numeroDocumento;
	@Column(name="dataMovimentacao")
	private Date dataMovimentacao;
	@Column(name="idDocumentoOrigem")
	private Integer idDocumentoOrigem;
	@Column(name="observacao")
	private String observacao;
	@Column(name="faturado")
	private String faturado;
	@Column(name="manual")
	private String manual;
	@Column(name="numeroVolume")
	private Integer numeroVolume;
	@Column(name="peso")
	private Double peso;
	@Column(name="valorFrete")
	private Double valorFrete;
	@ManyToOne
	@JoinColumn(name = "idFornecedor")
	private Fornecedor forncedor;
	@ManyToOne
	@JoinColumn(name = "idStatusMovimentacaoEstoque")
	private StatusMovimentacaoEstoque statusMovimentacaoEstoque;
	@ManyToOne
	@JoinColumn(name = "idEmpresaDestino")
	private EmpresaFisica empresaDestino;
	
	public MovimentacaoEstoque()
	{
		super();
	}
	

	public MovimentacaoEstoque
	(
	EmpresaFisica empresaFisica,
	Estoque estoque,
	TipoMovimentacaoEstoque tipoMovimentacaoEstoque,
	TipoDocumentoMovimentoEstoque tipoDocumentoMovimento,
	Cliente cliente,
	Usuario usuario,
		String entradaSaida,
		String numeroDocumento,
		Date dataMovimentacao,
		Integer idDocumentoOrigem,
		String observacao,
		String faturado,
		String manual,
		Integer numeroVolume,
		Double peso,
		Double valorFrete,
	Fornecedor forncedor,
	StatusMovimentacaoEstoque statusMovimentacaoEstoque,
	EmpresaFisica empresaDestino
	) 
	{
		super();
		this.empresaFisica = empresaFisica;
		this.estoque = estoque;
		this.tipoMovimentacaoEstoque = tipoMovimentacaoEstoque;
		this.tipoDocumentoMovimento = tipoDocumentoMovimento;
		this.cliente = cliente;
		this.usuario = usuario;
		this.entradaSaida = entradaSaida;
		this.numeroDocumento = numeroDocumento;
		this.dataMovimentacao = dataMovimentacao;
		this.idDocumentoOrigem = idDocumentoOrigem;
		this.observacao = observacao;
		this.faturado = faturado;
		this.manual = manual;
		this.numeroVolume = numeroVolume;
		this.peso = peso;
		this.valorFrete = valorFrete;
		this.forncedor = forncedor;
		this.statusMovimentacaoEstoque =statusMovimentacaoEstoque;
		this.empresaDestino = empresaDestino;
	}

	public Integer getId() 
	{
		return id;
	}

	public void setId(Integer id) 
	{
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	 
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public Estoque getEstoque() {
		return estoque;
	}

	public void setEstoque(Estoque estoque) {
		this.estoque = estoque;
	}
	
	public TipoMovimentacaoEstoque getTipoMovimentacaoEstoque() {
		return tipoMovimentacaoEstoque;
	}

	public void setTipoMovimentacaoEstoque(TipoMovimentacaoEstoque tipoMovimentacaoEstoque) {
		this.tipoMovimentacaoEstoque = tipoMovimentacaoEstoque;
	}
	
	public TipoDocumentoMovimentoEstoque getTipoDocumentoMovimento() {
		return tipoDocumentoMovimento;
	}

	public void setTipoDocumentoMovimento(TipoDocumentoMovimentoEstoque tipoDocumentoMovimento) {
		this.tipoDocumentoMovimento = tipoDocumentoMovimento;
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String getEntradaSaida() {
		String retorno = null;
		
		if (entradaSaida != null)
			retorno = entradaSaida.toUpperCase().trim();
		return retorno;
	}
	
	public void setEntradaSaida(String entradaSaida) {
		if (entradaSaida != null)
		{
			this.entradaSaida = entradaSaida.toUpperCase().trim();
		}
		else
			this.entradaSaida = null;
			
		
	}
		
	public String getNumeroDocumento() {
		String retorno = null;
		
		if (numeroDocumento != null)
			retorno = numeroDocumento.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroDocumento(String numeroDocumento) {
		if (numeroDocumento != null)
		{
			this.numeroDocumento = numeroDocumento.toUpperCase().trim();
		}
		else
			this.numeroDocumento = null;
			
		
	}
		
	public Date getDataMovimentacao() {
		return dataMovimentacao;
	}

	public void setDataMovimentacao(Date dataMovimentacao) {
		this.dataMovimentacao = dataMovimentacao;
	}
	public Integer getIdDocumentoOrigem() {
		return idDocumentoOrigem;
	}

	public void setIdDocumentoOrigem(Integer idDocumentoOrigem) {
		if (idDocumentoOrigem != null && idDocumentoOrigem == 0)
		{
			this.idDocumentoOrigem = null;
		}
		else
		{
			this.idDocumentoOrigem = idDocumentoOrigem;
		}
	}	
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	public String getFaturado() {
		String retorno = null;
		
		if (faturado != null)
			retorno = faturado.toUpperCase().trim();
		return retorno;
	}
	
	public void setFaturado(String faturado) {
		if (faturado != null)
		{
			this.faturado = faturado.toUpperCase().trim();
		}
		else
			this.faturado = null;
			
		
	}
		
	public String getManual() {
		String retorno = null;
		
		if (manual != null)
			retorno = manual.toUpperCase().trim();
		return retorno;
	}
	
	public void setManual(String manual) {
		if (manual != null)
		{
			this.manual = manual.toUpperCase().trim();
		}
		else
			this.manual = null;
			
		
	}
		
	public Integer getNumeroVolume() {
		return numeroVolume;
	}

	public void setNumeroVolume(Integer numeroVolume) {
		if (numeroVolume != null && numeroVolume == 0)
		{
			this.numeroVolume = null;
		}
		else
		{
			this.numeroVolume = numeroVolume;
		}
	}	
	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		if (peso != null && peso == 0.0)
		{
			this.peso = null;
		}
		else
		{
			this.peso = peso;
		}
	}	
	public Double getValorFrete() {
		return valorFrete;
	}

	public void setValorFrete(Double valorFrete) {
		if (valorFrete != null && valorFrete == 0.0)
		{
			this.valorFrete = null;
		}
		else
		{
			this.valorFrete = valorFrete;
		}
	}	
	public Fornecedor getForncedor() {
		return forncedor;
	}

	public void setForncedor(Fornecedor forncedor) {
		this.forncedor = forncedor;
	}
	
	
	public StatusMovimentacaoEstoque getStatusMovimentacaoEstoque() {
		return statusMovimentacaoEstoque;
	}


	public EmpresaFisica getEmpresaDestino() {
		return empresaDestino;
	}


	public void setEmpresaDestino(EmpresaFisica empresaDestino) {
		this.empresaDestino = empresaDestino;
	}


	public void setStatusMovimentacaoEstoque(
			StatusMovimentacaoEstoque statusMovimentacaoEstoque) {
		this.statusMovimentacaoEstoque = statusMovimentacaoEstoque;
	}

	public Object clone() throws CloneNotSupportedException 
	{
		return super.clone();
	}
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
