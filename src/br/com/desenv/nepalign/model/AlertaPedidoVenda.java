package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="alertapedidovenda")


public class AlertaPedidoVenda extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idAlertaPedidoVenda")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idItemPedidoVenda")
	private ItemPedidoVenda itemPedidoVenda;
	@ManyToOne
	@JoinColumn(name = "idEmpresaOrigemAlerta")
	private EmpresaFisica empresaOrigemAlerta;
	@ManyToOne
	@JoinColumn(name = "idStatusAlerta")
	private StatusAlerta statusAlerta;
	
	@ManyToOne
	@JoinColumn(name = "idEmpresaVencedora")
	private EmpresaFisica empresaVencedora;
	
	@Column(name="dataAlerta")
	private Date dataAlerta;
	
	@Column(name="dataResposta")
	private Date dataResposta;
	
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	
	@Column(name="observacao")
	private String observacao;
	
	@Column(name="flag")
	private String flag;
	

	public AlertaPedidoVenda()
	{
		super();
	}
	

	public AlertaPedidoVenda
	(
	ItemPedidoVenda itemPedidoVenda,
	EmpresaFisica empresaOrigemAlerta,
	StatusAlerta statusAlerta,
	EmpresaFisica empresaVencedora,
		Date dataAlerta,
		Date dataResposta,
	Usuario usuario,
		String observacao
	) 
	{
		super();
		this.itemPedidoVenda = itemPedidoVenda;
		this.empresaOrigemAlerta = empresaOrigemAlerta;
		this.statusAlerta = statusAlerta;
		this.empresaVencedora = empresaVencedora;
		this.dataAlerta = dataAlerta;
		this.dataResposta = dataResposta;
		this.usuario = usuario;
		this.observacao = observacao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public ItemPedidoVenda getItemPedidoVenda() {
		return itemPedidoVenda;
	}

	public void setItemPedidoVenda(ItemPedidoVenda itemPedidoVenda) {
		this.itemPedidoVenda = itemPedidoVenda;
	}
	
	public EmpresaFisica getEmpresaOrigemAlerta() {
		return empresaOrigemAlerta;
	}

	public void setEmpresaOrigemAlerta(EmpresaFisica empresaOrigemAlerta) {
		this.empresaOrigemAlerta = empresaOrigemAlerta;
	}
	
	public StatusAlerta getStatusAlerta() {
		return statusAlerta;
	}

	public void setStatusAlerta(StatusAlerta statusAlerta) {
		this.statusAlerta = statusAlerta;
	}
	
	public EmpresaFisica getEmpresaVencedora() {
		return empresaVencedora;
	}

	public void setEmpresaVencedora(EmpresaFisica empresaVencedora) {
		this.empresaVencedora = empresaVencedora;
	}
	
	public Date getDataAlerta() {
		return dataAlerta;
	}

	public void setDataAlerta(Date dataAlerta) {
		this.dataAlerta = dataAlerta;
	}
	public Date getDataResposta() {
		return dataResposta;
	}

	public void setDataResposta(Date dataResposta) {
		this.dataResposta = dataResposta;
	}
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
	}
	
	public String getFlag() {
		return flag;
	}


	public void setFlag(String flag) {
		this.flag = flag;
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
