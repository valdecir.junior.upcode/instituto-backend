package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="notafiscalcomplnfe")


public class NotaFiscaComplNfe extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idNotaFiscalComplNFE")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idNotaFiscal")
	private NotaFiscal notaFiscal;
	@Column(name="flLocalEntregaDif")
	private Integer flLocalEntregaDif;
	
	@Column(name="flLocalRetiradaDif")
	private Integer flLocalRetiradaDif;
	
	@Column(name="flInsideISSQN")
	private Integer flInsideISSQN;
	
	@Column(name="codigoMunicipioEmpr")
	private String codigoMunicipioEmpr;
	
	@Column(name="nomeMunicipioEmpr")
	private String nomeMunicipioEmpr;
	
	@Column(name="siglaUFEmpr")
	private String siglaUFEmpr;
	
	@Column(name="cepEmpr")
	private String cepEmpr;
	
	@Column(name="codigoPaisEmpr")
	private String codigoPaisEmpr;
	
	@Column(name="telefoneEmpr")
	private String telefoneEmpr;
	
	@Column(name="inscrMunicipalEmpr")
	private String inscrMunicipalEmpr;
	
	@Column(name="CNAEEmpr")
	private String cNAEEmpr;
	
	@Column(name="statusNFE")
	private Integer statusNFE;
	
	@Column(name="dataRetornoNFE")
	private Date dataRetornoNFE;
	
	@Column(name="numProtocoloREFAZ")
	private String numProtocoloREFAZ;
	
	@Column(name="chaveAcessoNFE")
	private String chaveAcessoNFE;
	
	@Column(name="motivoStatusNFE")
	private String motivoStatusNFE;
	
	@Column(name="statusCancelamentoNFE")
	private Integer statusCancelamentoNFE;
	
	@Column(name="motivoStatusCanc")
	private String motivoStatusCanc;
	
	@Column(name="statusInutilizacaoNFE")
	private Integer statusInutilizacaoNFE;
	
	@Column(name="motivoInutilizacaoNFE")
	private String motivoInutilizacaoNFE;
	
	@Column(name="statusImpressaoNFE")
	private Integer statusImpressaoNFE;
	
	@Column(name="tipoNotaNFE")
	private Integer tipoNotaNFE;
	
	@Column(name="flgLocalEtregaDiferente")
	private Integer flgLocalEntregaDiferente;
	
	@Column(name="flgLocalRetiradaDiferente")
	private Integer flgLocalRetiradaDiferente;
	
	@Column(name="flgIncideISSQN")
	private Integer flgIncideISSQN;
	
	@Column(name="finalidadeNota")
	private Integer finalidadeNota;
	
	@Column(name="nfeReferenciada")
	private String nfeReferenciada;
	
	@Column(name="codigoUFNFeReferenciada")
	private Integer codigoUFNFeReferenciada;
	
	@Column(name="anoMesEmissaoNFeRef")
	private String anoMesEmissaoNFeRef;
	
	@Column(name="cnpjEmitente")
	private String cnpjEmitente;
	
	@Column(name="modeloNfeRef")
	private String modeloNfeRef;
	
	@Column(name="serieNFeRef")
	private String serieNFeRef;
	
	@Column(name="numeroNFeRef")
	private String numeroNFeRef;
	

	public NotaFiscaComplNfe()
	{
		super();
	}
	

	public NotaFiscaComplNfe
	(
	NotaFiscal notaFiscal,
		Integer flLocalEntregaDif,
		Integer flLocalRetiradaDif,
		Integer flInsideISSQN,
		String codigoMunicipioEmpr,
		String nomeMunicipioEmpr,
		String siglaUFEmpr,
		String cepEmpr,
		String codigoPaisEmpr,
		String telefoneEmpr,
		String inscrMunicipalEmpr,
		String cNAEEmpr,
		Integer statusNFE,
		Date dataRetornoNFE,
		String numProtocoloREFAZ,
		String chaveAcessoNFE,
		String motivoStatusNFE,
		Integer statusCancelamentoNFE,
		String motivoStatusCanc,
		Integer statusInutilizacaoNFE,
		String motivoInutilizacaoNFE,
		Integer statusImpressaoNFE,
		Integer tipoNotaNFE,
		Integer flgLocalEntregaDiferente,
		Integer flgLocalRetiradaDiferente,
		Integer flgIncideISSQN,
		Integer finalidadeNota,
		String nfeReferenciada,
		Integer codigoUFNFeReferenciada,
		String anoMesEmissaoNFeRef,
		String cnpjEmitente,
		String modeloNfeRef,
		String serieNFeRef,
		String numeroNFeRef
	) 
	{
		super();
		this.notaFiscal = notaFiscal;
		this.flLocalEntregaDif = flLocalEntregaDif;
		this.flLocalRetiradaDif = flLocalRetiradaDif;
		this.flInsideISSQN = flInsideISSQN;
		this.codigoMunicipioEmpr = codigoMunicipioEmpr;
		this.nomeMunicipioEmpr = nomeMunicipioEmpr;
		this.siglaUFEmpr = siglaUFEmpr;
		this.cepEmpr = cepEmpr;
		this.codigoPaisEmpr = codigoPaisEmpr;
		this.telefoneEmpr = telefoneEmpr;
		this.inscrMunicipalEmpr = inscrMunicipalEmpr;
		this.cNAEEmpr = cNAEEmpr;
		this.statusNFE = statusNFE;
		this.dataRetornoNFE = dataRetornoNFE;
		this.numProtocoloREFAZ = numProtocoloREFAZ;
		this.chaveAcessoNFE = chaveAcessoNFE;
		this.motivoStatusNFE = motivoStatusNFE;
		this.statusCancelamentoNFE = statusCancelamentoNFE;
		this.motivoStatusCanc = motivoStatusCanc;
		this.statusInutilizacaoNFE = statusInutilizacaoNFE;
		this.motivoInutilizacaoNFE = motivoInutilizacaoNFE;
		this.statusImpressaoNFE = statusImpressaoNFE;
		this.tipoNotaNFE = tipoNotaNFE;
		this.flgLocalEntregaDiferente = flgLocalEntregaDiferente;
		this.flgLocalRetiradaDiferente = flgLocalRetiradaDiferente;
		this.flgIncideISSQN = flgIncideISSQN;
		this.finalidadeNota = finalidadeNota;
		this.nfeReferenciada = nfeReferenciada;
		this.codigoUFNFeReferenciada = codigoUFNFeReferenciada;
		this.anoMesEmissaoNFeRef = anoMesEmissaoNFeRef;
		this.cnpjEmitente = cnpjEmitente;
		this.modeloNfeRef = modeloNfeRef;
		this.serieNFeRef = serieNFeRef;
		this.numeroNFeRef = numeroNFeRef;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public NotaFiscal getNotaFiscal() {
		return notaFiscal;
	}

	public void setNotaFiscal(NotaFiscal notaFiscal) {
		this.notaFiscal = notaFiscal;
	}
	
	public Integer getFlLocalEntregaDif() {
		return flLocalEntregaDif;
	}

	public void setFlLocalEntregaDif(Integer flLocalEntregaDif) {
		this.flLocalEntregaDif = flLocalEntregaDif;
	}	
	public Integer getFlLocalRetiradaDif() {
		return flLocalRetiradaDif;
	}

	public void setFlLocalRetiradaDif(Integer flLocalRetiradaDif) {
		this.flLocalRetiradaDif = flLocalRetiradaDif;
	}	
	public Integer getFlInsideISSQN() {
		return flInsideISSQN;
	}

	public void setFlInsideISSQN(Integer flInsideISSQN) {
		this.flInsideISSQN = flInsideISSQN;
	}	
	public String getCodigoMunicipioEmpr() {
		String retorno = null;
		
		if (codigoMunicipioEmpr != null)
			retorno = codigoMunicipioEmpr.toUpperCase().trim();
		return retorno;
	}
	
	public void setCodigoMunicipioEmpr(String codigoMunicipioEmpr) {
		if (codigoMunicipioEmpr != null)
		{
			this.codigoMunicipioEmpr = codigoMunicipioEmpr.toUpperCase().trim();
		}
		else
			this.codigoMunicipioEmpr = null;
			
		
	}
		
	public String getNomeMunicipioEmpr() {
		String retorno = null;
		
		if (nomeMunicipioEmpr != null)
			retorno = nomeMunicipioEmpr.toUpperCase().trim();
		return retorno;
	}
	
	public void setNomeMunicipioEmpr(String nomeMunicipioEmpr) {
		if (nomeMunicipioEmpr != null)
		{
			this.nomeMunicipioEmpr = nomeMunicipioEmpr.toUpperCase().trim();
		}
		else
			this.nomeMunicipioEmpr = null;
			
		
	}
		
	public String getSiglaUFEmpr() {
		String retorno = null;
		
		if (siglaUFEmpr != null)
			retorno = siglaUFEmpr.toUpperCase().trim();
		return retorno;
	}
	
	public void setSiglaUFEmpr(String siglaUFEmpr) {
		if (siglaUFEmpr != null)
		{
			this.siglaUFEmpr = siglaUFEmpr.toUpperCase().trim();
		}
		else
			this.siglaUFEmpr = null;
			
		
	}
		
	public String getCepEmpr() {
		String retorno = null;
		
		if (cepEmpr != null)
			retorno = cepEmpr.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setCepEmpr(String cepEmpr) {
		if (cepEmpr != null)
		{
			this.cepEmpr = cepEmpr.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.cepEmpr = null;
			
		
	}
		
	public String getCodigoPaisEmpr() {
		String retorno = null;
		
		if (codigoPaisEmpr != null)
			retorno = codigoPaisEmpr.toUpperCase().trim();
		return retorno;
	}
	
	public void setCodigoPaisEmpr(String codigoPaisEmpr) {
		if (codigoPaisEmpr != null)
		{
			this.codigoPaisEmpr = codigoPaisEmpr.toUpperCase().trim();
		}
		else
			this.codigoPaisEmpr = null;
			
		
	}
		
	public String getTelefoneEmpr() {
		String retorno = null;
		
		if (telefoneEmpr != null)
			retorno = telefoneEmpr.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setTelefoneEmpr(String telefoneEmpr) {
		if (telefoneEmpr != null)
		{
			this.telefoneEmpr = telefoneEmpr.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.telefoneEmpr = null;
			
		
	}
		
	public String getInscrMunicipalEmpr() {
		String retorno = null;
		
		if (inscrMunicipalEmpr != null)
			retorno = inscrMunicipalEmpr.toUpperCase().trim();
		return retorno;
	}
	
	public void setInscrMunicipalEmpr(String inscrMunicipalEmpr) {
		if (inscrMunicipalEmpr != null)
		{
			this.inscrMunicipalEmpr = inscrMunicipalEmpr.toUpperCase().trim();
		}
		else
			this.inscrMunicipalEmpr = null;
			
		
	}
		
	public String getCNAEEmpr() {
		String retorno = null;
		
		if (cNAEEmpr != null)
			retorno = cNAEEmpr.toUpperCase().trim();
		return retorno;
	}
	
	public void setCNAEEmpr(String cNAEEmpr) {
		if (cNAEEmpr != null)
		{
			this.cNAEEmpr = cNAEEmpr.toUpperCase().trim();
		}
		else
			this.cNAEEmpr = null;
			
		
	}
		
	public Integer getStatusNFE() {
		return statusNFE;
	}

	public void setStatusNFE(Integer statusNFE) {
		this.statusNFE = statusNFE;
	}	
	public Date getDataRetornoNFE() {
		return dataRetornoNFE;
	}

	public void setDataRetornoNFE(Date dataRetornoNFE) {
		this.dataRetornoNFE = dataRetornoNFE;
	}
	public String getNumProtocoloREFAZ() {
		String retorno = null;
		
		if (numProtocoloREFAZ != null)
			retorno = numProtocoloREFAZ.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumProtocoloREFAZ(String numProtocoloREFAZ) {
		if (numProtocoloREFAZ != null)
		{
			this.numProtocoloREFAZ = numProtocoloREFAZ.toUpperCase().trim();
		}
		else
			this.numProtocoloREFAZ = null;
			
		
	}
		
	public String getChaveAcessoNFE() {
		String retorno = null;
		
		if (chaveAcessoNFE != null)
			retorno = chaveAcessoNFE.toUpperCase().trim();
		return retorno;
	}
	
	public void setChaveAcessoNFE(String chaveAcessoNFE) {
		if (chaveAcessoNFE != null)
		{
			this.chaveAcessoNFE = chaveAcessoNFE.toUpperCase().trim();
		}
		else
			this.chaveAcessoNFE = null;
			
		
	}
		
	public String getMotivoStatusNFE() {
		String retorno = null;
		
		if (motivoStatusNFE != null)
			retorno = motivoStatusNFE.toUpperCase().trim();
		return retorno;
	}
	
	public void setMotivoStatusNFE(String motivoStatusNFE) {
		if (motivoStatusNFE != null)
		{
			this.motivoStatusNFE = motivoStatusNFE.toUpperCase().trim();
		}
		else
			this.motivoStatusNFE = null;
			
		
	}
		
	public Integer getStatusCancelamentoNFE() {
		return statusCancelamentoNFE;
	}

	public void setStatusCancelamentoNFE(Integer statusCancelamentoNFE) {
		this.statusCancelamentoNFE = statusCancelamentoNFE;
	}	
	public String getMotivoStatusCanc() {
		String retorno = null;
		
		if (motivoStatusCanc != null)
			retorno = motivoStatusCanc.toUpperCase().trim();
		return retorno;
	}
	
	public void setMotivoStatusCanc(String motivoStatusCanc) {
		if (motivoStatusCanc != null)
		{
			this.motivoStatusCanc = motivoStatusCanc.toUpperCase().trim();
		}
		else
			this.motivoStatusCanc = null;
			
		
	}
		
	public Integer getStatusInutilizacaoNFE() {
		return statusInutilizacaoNFE;
	}

	public void setStatusInutilizacaoNFE(Integer statusInutilizacaoNFE) {
		this.statusInutilizacaoNFE = statusInutilizacaoNFE;
	}	
	public String getMotivoInutilizacaoNFE() {
		String retorno = null;
		
		if (motivoInutilizacaoNFE != null)
			retorno = motivoInutilizacaoNFE.toUpperCase().trim();
		return retorno;
	}
	
	public void setMotivoInutilizacaoNFE(String motivoInutilizacaoNFE) {
		if (motivoInutilizacaoNFE != null)
		{
			this.motivoInutilizacaoNFE = motivoInutilizacaoNFE.toUpperCase().trim();
		}
		else
			this.motivoInutilizacaoNFE = null;
			
		
	}
		
	public Integer getStatusImpressaoNFE() {
		return statusImpressaoNFE;
	}

	public void setStatusImpressaoNFE(Integer statusImpressaoNFE) {
		this.statusImpressaoNFE = statusImpressaoNFE;
	}	
	public Integer getTipoNotaNFE() {
		return tipoNotaNFE;
	}

	public void setTipoNotaNFE(Integer tipoNotaNFE) {
		this.tipoNotaNFE = tipoNotaNFE;
	}	
	public Integer getFlgLocalEntregaDiferente() {
		return flgLocalEntregaDiferente;
	}

	public void setFlgLocalEntregaDiferente(Integer flgLocalEntregaDiferente) {
		this.flgLocalEntregaDiferente = flgLocalEntregaDiferente;
	}	
	public Integer getFlgLocalRetiradaDiferente() {
		return flgLocalRetiradaDiferente;
	}

	public void setFlgLocalRetiradaDiferente(Integer flgLocalRetiradaDiferente) {
		this.flgLocalRetiradaDiferente = flgLocalRetiradaDiferente;
	}	
	public Integer getFlgIncideISSQN() {
		return flgIncideISSQN;
	}

	public void setFlgIncideISSQN(Integer flgIncideISSQN) {
		this.flgIncideISSQN = flgIncideISSQN;
	}	
	public Integer getFinalidadeNota() {
		return finalidadeNota;
	}

	public void setFinalidadeNota(Integer finalidadeNota) {
		this.finalidadeNota = finalidadeNota;
	}	
	public String getNfeReferenciada() {
		String retorno = null;
		
		if (nfeReferenciada != null)
			retorno = nfeReferenciada.toUpperCase().trim();
		return retorno;
	}
	
	public void setNfeReferenciada(String nfeReferenciada) {
		if (nfeReferenciada != null)
		{
			this.nfeReferenciada = nfeReferenciada.toUpperCase().trim();
		}
		else
			this.nfeReferenciada = null;
			
		
	}
		
	public Integer getCodigoUFNFeReferenciada() {
		return codigoUFNFeReferenciada;
	}

	public void setCodigoUFNFeReferenciada(Integer codigoUFNFeReferenciada) {
		this.codigoUFNFeReferenciada = codigoUFNFeReferenciada;
	}	
	public String getAnoMesEmissaoNFeRef() {
		String retorno = null;
		
		if (anoMesEmissaoNFeRef != null)
			retorno = anoMesEmissaoNFeRef.toUpperCase().trim();
		return retorno;
	}
	
	public void setAnoMesEmissaoNFeRef(String anoMesEmissaoNFeRef) {
		if (anoMesEmissaoNFeRef != null)
		{
			this.anoMesEmissaoNFeRef = anoMesEmissaoNFeRef.toUpperCase().trim();
		}
		else
			this.anoMesEmissaoNFeRef = null;
			
		
	}
		
	public String getCnpjEmitente() {
		String retorno = null;
		
		if (cnpjEmitente != null)
			retorno = cnpjEmitente.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setCnpjEmitente(String cnpjEmitente) {
		if (cnpjEmitente != null)
		{
			this.cnpjEmitente = cnpjEmitente.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.cnpjEmitente = null;
			
		
	}
		
	public String getModeloNfeRef() {
		String retorno = null;
		
		if (modeloNfeRef != null)
			retorno = modeloNfeRef.toUpperCase().trim();
		return retorno;
	}
	
	public void setModeloNfeRef(String modeloNfeRef) {
		if (modeloNfeRef != null)
		{
			this.modeloNfeRef = modeloNfeRef.toUpperCase().trim();
		}
		else
			this.modeloNfeRef = null;
			
		
	}
		
	public String getSerieNFeRef() {
		String retorno = null;
		
		if (serieNFeRef != null)
			retorno = serieNFeRef.toUpperCase().trim();
		return retorno;
	}
	
	public void setSerieNFeRef(String serieNFeRef) {
		if (serieNFeRef != null)
		{
			this.serieNFeRef = serieNFeRef.toUpperCase().trim();
		}
		else
			this.serieNFeRef = null;
			
		
	}
		
	public String getNumeroNFeRef() {
		String retorno = null;
		
		if (numeroNFeRef != null)
			retorno = numeroNFeRef.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroNFeRef(String numeroNFeRef) {
		if (numeroNFeRef != null)
		{
			this.numeroNFeRef = numeroNFeRef.toUpperCase().trim();
		}
		else
			this.numeroNFeRef = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
