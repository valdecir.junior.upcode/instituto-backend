package br.com.desenv.nepalign.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="grupoproduto")


public class GrupoProduto extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name="idGrupoProduto")
	private Integer id;
		
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="ultimaAlteracao")
	private Date ultimaAlteracao;
	
	@Column(name="codigo")
	private Integer codigo;
	
	@Column(name="ncm")
	private String ncm;
	

	public GrupoProduto()
	{
		super();
	}
	

	public GrupoProduto
	(
		String descricao,
		Date ultimaAlteracao,
		String ncm
	) 
	{
		super();
		this.descricao = descricao;
		this.ultimaAlteracao = ultimaAlteracao;
		this.ncm = ncm;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public Date getUltimaAlteracao() {
		return ultimaAlteracao;
	}

	public void setUltimaAlteracao(Date ultimaAlteracao) {
		this.ultimaAlteracao = ultimaAlteracao;
	}
	
	public Integer getCodigo() {
		return codigo;
	}


	public void setCodigo(Integer codigo) {
		if (codigo != null && codigo == 0)
		{
			this.codigo = null;
		}
		else
		{
			this.codigo = codigo;
		}
	}
	
	public String getNcm() {
		return ncm;
	}


	public void setNcm(String ncm) {
		this.ncm = ncm;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}
	
	@Override
	public boolean equals(Object object) 
	{
		if(object == null)
			return false;
		if(!object.getClass().equals(this.getClass()))
			return false;
		
		if(((GrupoProduto) object).getDescricao().equals(getDescricao()))
			return true;
		
		return super.equals(object);
	}

}
