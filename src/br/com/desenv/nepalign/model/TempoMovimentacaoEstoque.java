package br.com.desenv.nepalign.model;

import java.util.Date;

public class TempoMovimentacaoEstoque {
public Double quantidade;
public Double valor;
public String tipoMovimentacaoEstoque;
public String cliente;
public String entradaSaida;
public String numeroDocumento;
public Date data;
public String ordem;
public String fornecedor;
public String tamanho;
public String cor;
public String produto;
public String referencia;
public String codigoBarras;
public String idRelatorio;
public String saldoInicial;
public Integer idProduto;
public Integer idCor;
public Integer idTamanho;
public Integer idOrdem;
public Integer idEmpresa;
public Integer idEmpresaDestino;
public Integer idFornecedor;
public Integer idCliente;
public Integer idTipoMovimentacao;
public String empresa;
public String empresaDestino;
public Integer idEstoqueProduto;
public Integer idEstoque;

public Integer getIdEstoqueProduto() {
	return idEstoqueProduto;
}
public void setIdEstoqueProduto(Integer idEstoqueProduto) {
	this.idEstoqueProduto = idEstoqueProduto;
}
public Integer getIdEstoque() {
	return idEstoque;
}
public void setIdEstoque(Integer idEstoque) {
	this.idEstoque = idEstoque;
}
public String getEmpresa() {
	return empresa;
}
public void setEmpresa(String empresa) {
	this.empresa = empresa;
}
public String getEmpresaDestino() {
	return empresaDestino;
}
public void setEmpresaDestino(String empresaDestino) {
	this.empresaDestino = empresaDestino;
}
public Double getQuantidade() {
	return quantidade;
}
public void setQuantidade(Double quantidade) {
	this.quantidade = quantidade;
}
public Double getValor() {
	return valor;
}
public void setValor(Double valor) {
	this.valor = valor;
}
public String getTipoMovimentacaoEstoque() {
	return tipoMovimentacaoEstoque;
}
public void setTipoMovimentacaoEstoque(String tipoMovimentacaoEstoque) {
	this.tipoMovimentacaoEstoque = tipoMovimentacaoEstoque;
}
public String getCliente() {
	return cliente;
}
public void setCliente(String cliente) {
	this.cliente = cliente;
}
public String getEntradaSaida() {
	return entradaSaida;
}
public void setEntradaSaida(String entradaSaida) {
	this.entradaSaida = entradaSaida;
}
public String getNumeroDocumento() {
	return numeroDocumento;
}
public void setNumeroDocumento(String numeroDocumento) {
	this.numeroDocumento = numeroDocumento;
}
public Date getData() {
	return data;
}
public void setData(Date data) {
	this.data = data;
}
public String getOrdem() {
	return ordem;
}
public void setOrdem(String ordem) {
	this.ordem = ordem;
}
public String getFornecedor() {
	return fornecedor;
}
public void setFornecedor(String fornecedor) {
	this.fornecedor = fornecedor;
}
public String getTamanho() {
	return tamanho;
}
public void setTamanho(String tamanho) {
	this.tamanho = tamanho;
}
public String getCor() {
	return cor;
}
public void setCor(String cor) {
	this.cor = cor;
}
public String getProduto() {
	return produto;
}
public void setProduto(String produto) {
	this.produto = produto;
}
public String getReferencia() {
	return referencia;
}
public void setReferencia(String referencia) {
	this.referencia = referencia;
}
public String getCodigoBarras() {
	return codigoBarras;
}
public void setCodigoBarras(String codigoBarras) {
	this.codigoBarras = codigoBarras;
}
public String getIdRelatorio() {
	return idRelatorio;
}
public void setIdRelatorio(String idRelatorio) {
	this.idRelatorio = idRelatorio;
}
public String getSaldoInicial() {
	return saldoInicial;
}
public void setSaldoInicial(String saldoInicial) {
	this.saldoInicial = saldoInicial;
}
public Integer getIdProduto() {
	return idProduto;
}
public void setIdProduto(Integer idProduto) {
	this.idProduto = idProduto;
}
public Integer getIdCor() {
	return idCor;
}
public void setIdCor(Integer idCor) {
	this.idCor = idCor;
}
public Integer getIdTamanho() {
	return idTamanho;
}
public void setIdTamanho(Integer idTamanho) {
	this.idTamanho = idTamanho;
}
public Integer getIdOrdem() {
	return idOrdem;
}
public void setIdOrdem(Integer idOrdem) {
	this.idOrdem = idOrdem;
}
public Integer getIdEmpresa() {
	return idEmpresa;
}
public void setIdEmpresa(Integer idEmpresa) {
	this.idEmpresa = idEmpresa;
}
public Integer getIdEmpresaDestino() {
	return idEmpresaDestino;
}
public void setIdEmpresaDestino(Integer idEmpresaDestino) {
	this.idEmpresaDestino = idEmpresaDestino;
}
public Integer getIdFornecedor() {
	return idFornecedor;
}
public void setIdFornecedor(Integer idFornecedor) {
	this.idFornecedor = idFornecedor;
}
public Integer getIdCliente() {
	return idCliente;
}
public void setIdCliente(Integer idCliente) {
	this.idCliente = idCliente;
}
public Integer getIdTipoMovimentacao() {
	return idTipoMovimentacao;
}
public void setIdTipoMovimentacao(Integer idTipoMovimentacao) {
	this.idTipoMovimentacao = idTipoMovimentacao;
}





}
