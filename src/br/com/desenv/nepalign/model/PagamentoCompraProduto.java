package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="pagamentocompraproduto")


public class PagamentoCompraProduto extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idPagamentoCompraProduto")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idPedidoFornecedor")
	private PedidoCompraVenda pedidoFornecedor;
	@ManyToOne
	@JoinColumn(name = "idTipoDocumentoFinanceiro")
	private TipoDocumentoFinanceiro tipoDocumentoFinanceiro;
	@ManyToOne
	@JoinColumn(name = "idDocEntradaProduto")
	private DocumentoEntradaSaida documentoEntradaSaida;
	@ManyToOne
	@JoinColumn(name = "idFormaPagamento")
	private FormaPagamento formaPagamento;
	@Column(name="numeroDocumento")
	private String numeroDocumento;
	
	@Column(name="valor")
	private Double valor;
	
	@Column(name="dataPagamento")
	private Date dataPagamento;
	
	@Column(name="horaPagamento")
	private Date horaPagamento;
	
	@ManyToOne
	@JoinColumn(name = "idLancamentoContaPagar")
	private LancamentoContaPagar lancamentoContaPagar;

	public PagamentoCompraProduto()
	{
		super();
	}
	

	public PagamentoCompraProduto
	(
	PedidoCompraVenda pedidoFornecedor,
	TipoDocumentoFinanceiro tipoDocumentoFinanceiro,
	DocumentoEntradaSaida documentoEntradaSaida,
	FormaPagamento formaPagamento,
		String numeroDocumento,
		Double valor,
		Date dataPagamento,
		Date horaPagamento,
	LancamentoContaPagar lancamentoContaPagar
	) 
	{
		super();
		this.pedidoFornecedor = pedidoFornecedor;
		this.tipoDocumentoFinanceiro = tipoDocumentoFinanceiro;
		this.documentoEntradaSaida = documentoEntradaSaida;
		this.formaPagamento = formaPagamento;
		this.numeroDocumento = numeroDocumento;
		this.valor = valor;
		this.dataPagamento = dataPagamento;
		this.horaPagamento = horaPagamento;
		this.lancamentoContaPagar = lancamentoContaPagar;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public PedidoCompraVenda getPedidoFornecedor() {
		return pedidoFornecedor;
	}

	public void setPedidoFornecedor(PedidoCompraVenda pedidoFornecedor) {
		this.pedidoFornecedor = pedidoFornecedor;
	}
	
	public TipoDocumentoFinanceiro getTipoDocumentoFinanceiro() {
		return tipoDocumentoFinanceiro;
	}

	public void setTipoDocumentoFinanceiro(TipoDocumentoFinanceiro tipoDocumentoFinanceiro) {
		this.tipoDocumentoFinanceiro = tipoDocumentoFinanceiro;
	}
	
	public DocumentoEntradaSaida getDocumentoEntradaSaida() {
		return documentoEntradaSaida;
	}

	public void setDocumentoEntradaSaida(DocumentoEntradaSaida documentoEntradaSaida) {
		this.documentoEntradaSaida = documentoEntradaSaida;
	}
	
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
	public String getNumeroDocumento() {
		String retorno = null;
		
		if (numeroDocumento != null)
			retorno = numeroDocumento.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroDocumento(String numeroDocumento) {
		if (numeroDocumento != null)
		{
			this.numeroDocumento = numeroDocumento.toUpperCase().trim();
		}
		else
			this.numeroDocumento = null;
			
		
	}
		
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
	
		if (valor != null && Double.isNaN(valor))
		{
			this.valor = null;
		}
		else
		{
			this.valor = valor;
		}
		
	}	
	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}
	public Date getHoraPagamento() {
		return horaPagamento;
	}

	public void setHoraPagamento(Date horaPagamento) {
		this.horaPagamento = horaPagamento;
	}
	public LancamentoContaPagar getLancamentoContaPagar() {
		return lancamentoContaPagar;
	}

	public void setLancamentoContaPagar(LancamentoContaPagar lancamentoContaPagar) {
		this.lancamentoContaPagar = lancamentoContaPagar;
	}
	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
