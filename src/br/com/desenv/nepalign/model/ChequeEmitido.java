package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="chequeemitido")


public class ChequeEmitido extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idChequeEmitido")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idTalaoCheque")
	private TalaoCheque talaoCheque;
	@Column(name="valor")
	private Double valor;
	
	@Column(name="numeroCheque")
	private Integer numeroCheque;
	
	@Column(name="preDatado")
	private Integer preDatado;
	
	@Column(name="dataEmissao")
	private Date dataEmissao;
	
	@Column(name="dataPrevistaDeposito")
	private Date dataPrevistaDeposito;
	
	@Column(name="dataDeposito")
	private Date dataDeposito;
	
	@Column(name="responsavelEmissao")
	private String responsavelEmissao;
	
	@Column(name="observacao")
	private String observacao;
	
	@Column(name="situacao")
	private Integer situacao;
	

	public ChequeEmitido()
	{
		super();
	}
	

	public ChequeEmitido
	(
	TalaoCheque talaoCheque,
		Double valor,
		Integer numeroCheque,
		Integer preDatado,
		Date dataEmissao,
		Date dataPrevistaDeposito,
		Date dataDeposito,
		String responsavelEmissao,
		String observacao,
		Integer situacao
	) 
	{
		super();
		this.talaoCheque = talaoCheque;
		this.valor = valor;
		this.numeroCheque = numeroCheque;
		this.preDatado = preDatado;
		this.dataEmissao = dataEmissao;
		this.dataPrevistaDeposito = dataPrevistaDeposito;
		this.dataDeposito = dataDeposito;
		this.responsavelEmissao = responsavelEmissao;
		this.observacao = observacao;
		this.situacao = situacao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public TalaoCheque getTalaoCheque() {
		return talaoCheque;
	}

	public void setTalaoCheque(TalaoCheque talaoCheque) {
		this.talaoCheque = talaoCheque;
	}
	
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
	
		if (valor != null && Double.isNaN(valor))
		{
			this.valor = null;
		}
		else
		{
			this.valor = valor;
		}
		
	}	
	public Integer getNumeroCheque() {
		return numeroCheque;
	}

	public void setNumeroCheque(Integer numeroCheque) {
		this.numeroCheque = numeroCheque;
	}	
	public Integer getPreDatado() {
		return preDatado;
	}

	public void setPreDatado(Integer preDatado) {
		this.preDatado = preDatado;
	}	
	public Date getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	public Date getDataPrevistaDeposito() {
		return dataPrevistaDeposito;
	}

	public void setDataPrevistaDeposito(Date dataPrevistaDeposito) {
		this.dataPrevistaDeposito = dataPrevistaDeposito;
	}
	public Date getDataDeposito() {
		return dataDeposito;
	}

	public void setDataDeposito(Date dataDeposito) {
		this.dataDeposito = dataDeposito;
	}
	public String getResponsavelEmissao() {
		String retorno = null;
		
		if (responsavelEmissao != null)
			retorno = responsavelEmissao.toUpperCase().trim();
		return retorno;
	}
	
	public void setResponsavelEmissao(String responsavelEmissao) {
		if (responsavelEmissao != null)
		{
			this.responsavelEmissao = responsavelEmissao.toUpperCase().trim();
		}
		else
			this.responsavelEmissao = null;
			
		
	}
		
	public String getObservacao() {
		String retorno = null;
		
		if (observacao != null)
			retorno = observacao.toUpperCase().trim();
		return retorno;
	}
	
	public void setObservacao(String observacao) {
		if (observacao != null)
		{
			this.observacao = observacao.toUpperCase().trim();
		}
		else
			this.observacao = null;
			
		
	}
		
	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
