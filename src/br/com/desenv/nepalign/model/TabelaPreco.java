package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="tabelapreco")


public class TabelaPreco extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idTabelaPreco")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idEmpresa")
	private Empresa empresa;
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="restrita")
	private Integer restrita;
	
	@Column(name="percDescontoDefault")
	private Double percDescontoDefault;
	
	@Column(name="percLucroBrutoDefault")
	private Double percLucroBrutoDefault;
	

	public TabelaPreco()
	{
		super();
	}
	

	public TabelaPreco
	(
	Empresa empresa,
		String descricao,
		Integer restrita,
		Double percDescontoDefault,
		Double percLucroBrutoDefault
	) 
	{
		super();
		this.empresa = empresa;
		this.descricao = descricao;
		this.restrita = restrita;
		this.percDescontoDefault = percDescontoDefault;
		this.percLucroBrutoDefault = percLucroBrutoDefault;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public Integer getRestrita() {
		return restrita;
	}

	public void setRestrita(Integer restrita) {
		this.restrita = restrita;
	}	
	public Double getPercDescontoDefault() {
		return percDescontoDefault;
	}

	public void setPercDescontoDefault(Double percDescontoDefault) {
	
		if (percDescontoDefault != null && Double.isNaN(percDescontoDefault))
		{
			this.percDescontoDefault = null;
		}
		else
		{
			this.percDescontoDefault = percDescontoDefault;
		}
		
	}	
	public Double getPercLucroBrutoDefault() {
		return percLucroBrutoDefault;
	}

	public void setPercLucroBrutoDefault(Double percLucroBrutoDefault) {
	
		if (percLucroBrutoDefault != null && Double.isNaN(percLucroBrutoDefault))
		{
			this.percLucroBrutoDefault = null;
		}
		else
		{
			this.percLucroBrutoDefault = percLucroBrutoDefault;
		}
		
	}	
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
