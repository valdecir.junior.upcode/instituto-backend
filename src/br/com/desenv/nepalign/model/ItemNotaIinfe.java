package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="itemnotaiinfe")


public class ItemNotaIinfe extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idItemNotaIINfe")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idItemNota")
	private ItemNotaFiscal itemNota;
	@Column(name="identificadorDI")
	private String identificadorDI;
	
	@Column(name="dataRegistroDI")
	private Date dataRegistroDI;
	
	@Column(name="siglaUFDesembaraco")
	private String siglaUFDesembaraco;
	
	@Column(name="numeroAdicao")
	private String numeroAdicao;
	
	@Column(name="numSeqItemAdicao")
	private String numSeqItemAdicao;
	
	@Column(name="codFabricanteEstrangeiro")
	private String codFabricanteEstrangeiro;
	
	@Column(name="numeroDSI")
	private String numeroDSI;
	
	@Column(name="localDesembaraco")
	private String localDesembaraco;
	
	@Column(name="dataDesembaraco")
	private Date dataDesembaraco;
	
	@Column(name="valorBaseCalcII")
	private Double valorBaseCalcII;
	
	@Column(name="valorDespAduaneiras")
	private Double valorDespAduaneiras;
	
	@Column(name="valorImpostoImportacao")
	private Double valorImpostoImportacao;
	
	@Column(name="valorImpostoOperFinanceiras")
	private Double valorImpostoOperFinanceiras;
	
	@Column(name="codigoExportador")
	private String codigoExportador;
	

	public ItemNotaIinfe()
	{
		super();
	}
	

	public ItemNotaIinfe
	(
	ItemNotaFiscal itemNota,
		String identificadorDI,
		Date dataRegistroDI,
		String siglaUFDesembaraco,
		String numeroAdicao,
		String numSeqItemAdicao,
		String codFabricanteEstrangeiro,
		String numeroDSI,
		String localDesembaraco,
		Date dataDesembaraco,
		Double valorBaseCalcII,
		Double valorDespAduaneiras,
		Double valorImpostoImportacao,
		Double valorImpostoOperFinanceiras,
		String codigoExportador
	) 
	{
		super();
		this.itemNota = itemNota;
		this.identificadorDI = identificadorDI;
		this.dataRegistroDI = dataRegistroDI;
		this.siglaUFDesembaraco = siglaUFDesembaraco;
		this.numeroAdicao = numeroAdicao;
		this.numSeqItemAdicao = numSeqItemAdicao;
		this.codFabricanteEstrangeiro = codFabricanteEstrangeiro;
		this.numeroDSI = numeroDSI;
		this.localDesembaraco = localDesembaraco;
		this.dataDesembaraco = dataDesembaraco;
		this.valorBaseCalcII = valorBaseCalcII;
		this.valorDespAduaneiras = valorDespAduaneiras;
		this.valorImpostoImportacao = valorImpostoImportacao;
		this.valorImpostoOperFinanceiras = valorImpostoOperFinanceiras;
		this.codigoExportador = codigoExportador;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public ItemNotaFiscal getItemNota() {
		return itemNota;
	}

	public void setItemNota(ItemNotaFiscal itemNota) {
		this.itemNota = itemNota;
	}
	
	public String getIdentificadorDI() {
		String retorno = null;
		
		if (identificadorDI != null)
			retorno = identificadorDI.toUpperCase().trim();
		return retorno;
	}
	
	public void setIdentificadorDI(String identificadorDI) {
		if (identificadorDI != null)
		{
			this.identificadorDI = identificadorDI.toUpperCase().trim();
		}
		else
			this.identificadorDI = null;
			
		
	}
		
	public Date getDataRegistroDI() {
		return dataRegistroDI;
	}

	public void setDataRegistroDI(Date dataRegistroDI) {
		this.dataRegistroDI = dataRegistroDI;
	}
	public String getSiglaUFDesembaraco() {
		String retorno = null;
		
		if (siglaUFDesembaraco != null)
			retorno = siglaUFDesembaraco.toUpperCase().trim();
		return retorno;
	}
	
	public void setSiglaUFDesembaraco(String siglaUFDesembaraco) {
		if (siglaUFDesembaraco != null)
		{
			this.siglaUFDesembaraco = siglaUFDesembaraco.toUpperCase().trim();
		}
		else
			this.siglaUFDesembaraco = null;
			
		
	}
		
	public String getNumeroAdicao() {
		String retorno = null;
		
		if (numeroAdicao != null)
			retorno = numeroAdicao.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroAdicao(String numeroAdicao) {
		if (numeroAdicao != null)
		{
			this.numeroAdicao = numeroAdicao.toUpperCase().trim();
		}
		else
			this.numeroAdicao = null;
			
		
	}
		
	public String getNumSeqItemAdicao() {
		String retorno = null;
		
		if (numSeqItemAdicao != null)
			retorno = numSeqItemAdicao.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumSeqItemAdicao(String numSeqItemAdicao) {
		if (numSeqItemAdicao != null)
		{
			this.numSeqItemAdicao = numSeqItemAdicao.toUpperCase().trim();
		}
		else
			this.numSeqItemAdicao = null;
			
		
	}
		
	public String getCodFabricanteEstrangeiro() {
		String retorno = null;
		
		if (codFabricanteEstrangeiro != null)
			retorno = codFabricanteEstrangeiro.toUpperCase().trim();
		return retorno;
	}
	
	public void setCodFabricanteEstrangeiro(String codFabricanteEstrangeiro) {
		if (codFabricanteEstrangeiro != null)
		{
			this.codFabricanteEstrangeiro = codFabricanteEstrangeiro.toUpperCase().trim();
		}
		else
			this.codFabricanteEstrangeiro = null;
			
		
	}
		
	public String getNumeroDSI() {
		String retorno = null;
		
		if (numeroDSI != null)
			retorno = numeroDSI.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumeroDSI(String numeroDSI) {
		if (numeroDSI != null)
		{
			this.numeroDSI = numeroDSI.toUpperCase().trim();
		}
		else
			this.numeroDSI = null;
			
		
	}
		
	public String getLocalDesembaraco() {
		String retorno = null;
		
		if (localDesembaraco != null)
			retorno = localDesembaraco.toUpperCase().trim();
		return retorno;
	}
	
	public void setLocalDesembaraco(String localDesembaraco) {
		if (localDesembaraco != null)
		{
			this.localDesembaraco = localDesembaraco.toUpperCase().trim();
		}
		else
			this.localDesembaraco = null;
			
		
	}
		
	public Date getDataDesembaraco() {
		return dataDesembaraco;
	}

	public void setDataDesembaraco(Date dataDesembaraco) {
		this.dataDesembaraco = dataDesembaraco;
	}
	public Double getValorBaseCalcII() {
		return valorBaseCalcII;
	}

	public void setValorBaseCalcII(Double valorBaseCalcII) {
	
		if (valorBaseCalcII != null && Double.isNaN(valorBaseCalcII))
		{
			this.valorBaseCalcII = null;
		}
		else
		{
			this.valorBaseCalcII = valorBaseCalcII;
		}
		
	}	
	public Double getValorDespAduaneiras() {
		return valorDespAduaneiras;
	}

	public void setValorDespAduaneiras(Double valorDespAduaneiras) {
	
		if (valorDespAduaneiras != null && Double.isNaN(valorDespAduaneiras))
		{
			this.valorDespAduaneiras = null;
		}
		else
		{
			this.valorDespAduaneiras = valorDespAduaneiras;
		}
		
	}	
	public Double getValorImpostoImportacao() {
		return valorImpostoImportacao;
	}

	public void setValorImpostoImportacao(Double valorImpostoImportacao) {
	
		if (valorImpostoImportacao != null && Double.isNaN(valorImpostoImportacao))
		{
			this.valorImpostoImportacao = null;
		}
		else
		{
			this.valorImpostoImportacao = valorImpostoImportacao;
		}
		
	}	
	public Double getValorImpostoOperFinanceiras() {
		return valorImpostoOperFinanceiras;
	}

	public void setValorImpostoOperFinanceiras(Double valorImpostoOperFinanceiras) {
	
		if (valorImpostoOperFinanceiras != null && Double.isNaN(valorImpostoOperFinanceiras))
		{
			this.valorImpostoOperFinanceiras = null;
		}
		else
		{
			this.valorImpostoOperFinanceiras = valorImpostoOperFinanceiras;
		}
		
	}	
	public String getCodigoExportador() {
		String retorno = null;
		
		if (codigoExportador != null)
			retorno = codigoExportador.toUpperCase().trim();
		return retorno;
	}
	
	public void setCodigoExportador(String codigoExportador) {
		if (codigoExportador != null)
		{
			this.codigoExportador = codigoExportador.toUpperCase().trim();
		}
		else
			this.codigoExportador = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
