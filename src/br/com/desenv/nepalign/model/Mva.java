package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="mva")


public class Mva extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idMva")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idTipoLista")
	private TipoLista tipoLista;
	@Column(name="aliqInterEstadual")
	private Double aliqInterEstadual;
	
	@ManyToOne
	@JoinColumn(name = "idUf")
	private Uf uf;
	@Column(name="percMva")
	private Double percMva;
	
	@Column(name="icmsDestino")
	private Double icmsDestino;
	
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	

	public Mva()
	{
		super();
	}
	

	public Mva
	(
	TipoLista tipoLista,
		Double aliqInterEstadual,
	Uf uf,
		Double percMva,
		Double icmsDestino,
	EmpresaFisica empresaFisica
	) 
	{
		super();
		this.tipoLista = tipoLista;
		this.aliqInterEstadual = aliqInterEstadual;
		this.uf = uf;
		this.percMva = percMva;
		this.icmsDestino = icmsDestino;
		this.empresaFisica = empresaFisica;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public TipoLista getTipoLista() {
		return tipoLista;
	}

	public void setTipoLista(TipoLista tipoLista) {
		this.tipoLista = tipoLista;
	}
	
	public Double getAliqInterEstadual() {
		return aliqInterEstadual;
	}

	public void setAliqInterEstadual(Double aliqInterEstadual) {
	
		if (aliqInterEstadual != null && Double.isNaN(aliqInterEstadual))
		{
			this.aliqInterEstadual = null;
		}
		else
		{
			this.aliqInterEstadual = aliqInterEstadual;
		}
		
	}	
	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}
	
	public Double getPercMva() {
		return percMva;
	}

	public void setPercMva(Double percMva) {
	
		if (percMva != null && Double.isNaN(percMva))
		{
			this.percMva = null;
		}
		else
		{
			this.percMva = percMva;
		}
		
	}	
	public Double getIcmsDestino() {
		return icmsDestino;
	}

	public void setIcmsDestino(Double icmsDestino) {
	
		if (icmsDestino != null && Double.isNaN(icmsDestino))
		{
			this.icmsDestino = null;
		}
		else
		{
			this.icmsDestino = icmsDestino;
		}
		
	}
	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}

	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}
}
