package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name = "ordemproduto")
public class OrdemProduto extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "ignGenerator", strategy = "br.com.desenv.frameworkignorante.IgnGenerator")
	@GeneratedValue(generator = "ignGenerator")
	@Column(name = "idOrdemProduto")
	private Integer id;

	@Column(name = "descricao")
	private String descricao;

	@Column(name = "codigo")
	private Integer codigo;

	public OrdemProduto()
	{
		super();
	}

	public OrdemProduto(String descricao)
	{
		super();
		this.descricao = descricao;

	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getDescricao()
	{
		String retorno = null;

		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}

	public void setDescricao(String descricao)
	{
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;

	}

	public Integer getCodigo()
	{
		return codigo;
	}

	public void setCodigo(Integer codigo)
	{
		this.codigo = codigo;
	}

	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		 * if (this.descricao == null || this.descricao.trim().equals("")) {
		 * throw new Exception("Descrição é obrigatório."); }
		 */

	}

}
