package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="empresaFinanceiro")
public class EmpresaFinanceiro extends GenericModelIGN 
{
	private static final long serialVersionUID = 1L;
	
	public static final Integer TIPOEMPRESA_CHEQUE = 0x01;
	public static final Integer TIPOEMPRESA_CARTAO = 0x02;
	public static final Integer TIPOEMPRESA_VALE_CALCADO = 0x03;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idEmpresaFinanceiro")
	private Integer id;
	
	@Column(name="nomeEmpresa")
	private String nomeEmpresa;
	
	@ManyToOne
	@JoinColumn(name="idEmpresaFisica")
	private EmpresaFisica empresaFisicaReferente;
	
	@Column(name="mesAberto")
	private Integer mesAberto;
	
	@Column(name="anoAberto")
	private Integer anoAberto;
	
	@Column(name="tipoEmpresa")
	private Integer tipoEmpresa;

	public EmpresaFinanceiro()
	{
		super();
	}

	public Integer getId() 
	{
		return this.id;
	}

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public String getNomeEmpresa() 
	{
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) 
	{
		this.nomeEmpresa = nomeEmpresa;
	}

	public EmpresaFisica getEmpresaFisicaReferente() 
	{
		return empresaFisicaReferente;
	}

	public void setEmpresaFisicaReferente(EmpresaFisica empresaFisicaReferente) 
	{
		this.empresaFisicaReferente = empresaFisicaReferente;
	}

	public Integer getMesAberto() 
	{
		return mesAberto;
	}

	public void setMesAberto(Integer mesAberto) 
	{
		this.mesAberto = mesAberto;
	}

	public Integer getAnoAberto() 
	{
		return anoAberto;
	}

	public void setAnoAberto(Integer anoAberto) 
	{
		this.anoAberto = anoAberto;
	}

	/**
	 * [1] EMPRESAS_FINANCEIRO_CHEQUE
	 * [2] EMPRESAS_FINANCEIRO_CARTAO
	 * [3] EMPRESAS_FINANCEIRO_VALE
	 * @return
	 */
	public Integer getTipoEmpresa() 
	{
		return tipoEmpresa;
	}

	public void setTipoEmpresa(Integer tipoEmpresa) 
	{
		this.tipoEmpresa = tipoEmpresa;
	}
	
	@Override
	public void validate() throws Exception  { }
}