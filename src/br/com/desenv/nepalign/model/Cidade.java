package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="cidade")


public class Cidade extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idCidade")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idUf")
	private Uf uf;
	@Column(name="nome")
	private String nome;
	
	@Column(name="nomeSemAcento")
	private String nomeSemAcento;
	
	@Column(name="codigoIbge")
	private Integer codigoIbge;
	
	@Column(name="siglaUf")
	private String siglaUf;
	

	public Cidade()
	{
		super();
	}
	

	public Cidade
	(
	Uf uf,
		String nome,
		String nomeSemAcento,
		Integer codigoIbge,
		String siglaUf
	) 
	{
		super();
		this.uf = uf;
		this.nome = nome;
		this.nomeSemAcento = nomeSemAcento;
		this.codigoIbge = codigoIbge;
		this.siglaUf = siglaUf;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}
	
	public String getNome() {
		String retorno = null;
		
		if (nome != null)
			retorno = nome.toUpperCase().trim();
		return retorno;
	}
	
	public void setNome(String nome) {
		if (nome != null)
		{
			this.nome = nome.toUpperCase().trim();
		}
		else
			this.nome = null;
			
		
	}
		
	public String getNomeSemAcento() {
		String retorno = null;
		
		if (nomeSemAcento != null)
			retorno = nomeSemAcento.toUpperCase().trim();
		return retorno;
	}
	
	public void setNomeSemAcento(String nomeSemAcento) {
		if (nomeSemAcento != null)
		{
			this.nomeSemAcento = nomeSemAcento.toUpperCase().trim();
		}
		else
			this.nomeSemAcento = null;
			
		
	}
		
	public Integer getCodigoIbge() {
		return codigoIbge;
	}

	public void setCodigoIbge(Integer codigoIbge) {
		if (codigoIbge != null && codigoIbge == 0)
		{
			this.codigoIbge = null;
		}
		else
		{
			this.codigoIbge = codigoIbge;
		}
	}	
	public String getSiglaUf() {
		String retorno = null;
		
		if (siglaUf != null)
			retorno = siglaUf.toUpperCase().trim();
		return retorno;
	}
	
	public void setSiglaUf(String siglaUf) {
		if (siglaUf != null)
		{
			this.siglaUf = siglaUf.toUpperCase().trim();
		}
		else
			this.siglaUf = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
