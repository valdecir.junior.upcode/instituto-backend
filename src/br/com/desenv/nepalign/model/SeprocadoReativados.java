package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="seprocadosreativados")


public class SeprocadoReativados extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idSeprocadosReativados")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	@Column(name="reativadoSeprocado")
	private String seprocadoReativado;
	
	@Column(name="dataAntiga")
	private Date dataAntiga;
	
	@Column(name="dataNova")
	private Date dataNova;
	
	@ManyToOne
	@JoinColumn(name = "idDuplicataParcela")
	private DuplicataParcela duplicataParcela;
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	
	@Column(name="observacao")
	private String observacao;
	
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	@Column(name="dataImpressao")
	private Date dataImpressao;
	@Column(name="impresso")
	private String impresso;

	public SeprocadoReativados()
	{
		super();
	}
	
	public SeprocadoReativados(Cliente cliente, String seprocadoReativado,
			Date dataAntiga, Date dataNova, DuplicataParcela duplicataParcela,
			EmpresaFisica empresaFisica, String observacao, Usuario usuario,
			Date dataImpressao, String impresso) {
		super();
		this.cliente = cliente;
		this.seprocadoReativado = seprocadoReativado;
		this.dataAntiga = dataAntiga;
		this.dataNova = dataNova;
		this.duplicataParcela = duplicataParcela;
		this.empresaFisica = empresaFisica;
		this.observacao = observacao;
		this.usuario = usuario;
		this.dataImpressao = dataImpressao;
		this.impresso = impresso;
	}



	public Date getDataImpressao() {
		return dataImpressao;
	}


	public void setDataImpressao(Date dataImpressao) {
		this.dataImpressao = dataImpressao;
	}

	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public String getSeprocadoReativado() {
		String retorno = null;
		
		if (seprocadoReativado != null)
			retorno = seprocadoReativado.toUpperCase().trim();
		return retorno;
	}
	
	public void setSeprocadoReativado(String seprocadoReativado) {
		if (seprocadoReativado != null)
		{
			this.seprocadoReativado = seprocadoReativado.toUpperCase().trim();
		}
		else
			this.seprocadoReativado = null;
			
		
	}
		
	public Date getDataAntiga() {
		return dataAntiga;
	}

	public void setDataAntiga(Date dataAntiga) {
		this.dataAntiga = dataAntiga;
	}
	public Date getDataNova() {
		return dataNova;
	}

	public void setDataNova(Date dataNova) {
		this.dataNova = dataNova;
	}
	public DuplicataParcela getDuplicataParcela() {
		return duplicataParcela;
	}

	public void setDuplicataParcela(DuplicataParcela duplicataParcela) {
		this.duplicataParcela = duplicataParcela;
	}
	
	public EmpresaFisica getEmpresaFisica() {
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) {
		this.empresaFisica = empresaFisica;
	}
	
	public String getObservacao() {
		return observacao;
	}


	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}


	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


	public String getImpresso() {
		return impresso;
	}

	public void setImpresso(String impresso) {
		this.impresso = impresso;
	}

	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
