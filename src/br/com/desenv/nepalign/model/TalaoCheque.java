package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="talaocheque")


public class TalaoCheque extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idTalaoCheque")
	private Integer id;
		
	@ManyToOne
	@JoinColumn(name = "idContaCorrente")
	private ContaCorrente contaCorrente;
	@Column(name="numeroInicial")
	private Integer numeroInicial;
	
	@Column(name="numeroFinal")
	private Integer numeroFinal;
	
	@Column(name="identificadorTalao")
	private String identificadorTalao;
	

	public TalaoCheque()
	{
		super();
	}
	

	public TalaoCheque
	(
	ContaCorrente contaCorrente,
		Integer numeroInicial,
		Integer numeroFinal,
		String identificadorTalao
	) 
	{
		super();
		this.contaCorrente = contaCorrente;
		this.numeroInicial = numeroInicial;
		this.numeroFinal = numeroFinal;
		this.identificadorTalao = identificadorTalao;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public ContaCorrente getContaCorrente() {
		return contaCorrente;
	}

	public void setContaCorrente(ContaCorrente contaCorrente) {
		this.contaCorrente = contaCorrente;
	}
	
	public Integer getNumeroInicial() {
		return numeroInicial;
	}

	public void setNumeroInicial(Integer numeroInicial) {
		this.numeroInicial = numeroInicial;
	}	
	public Integer getNumeroFinal() {
		return numeroFinal;
	}

	public void setNumeroFinal(Integer numeroFinal) {
		this.numeroFinal = numeroFinal;
	}	
	public String getIdentificadorTalao() {
		String retorno = null;
		
		if (identificadorTalao != null)
			retorno = identificadorTalao.toUpperCase().trim();
		return retorno;
	}
	
	public void setIdentificadorTalao(String identificadorTalao) {
		if (identificadorTalao != null)
		{
			this.identificadorTalao = identificadorTalao.toUpperCase().trim();
		}
		else
			this.identificadorTalao = null;
			
		
	}
		
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
