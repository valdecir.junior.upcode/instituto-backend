package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="impressora")


public class Impressora extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idImpressora")
	private Integer id;
		
	@Column(name="nome")
	private String nome;
	
	@Column(name="caminho")
	private String caminho;
	
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="caixa")
	private String caixa;
	
	@Column(name="pdv")
	private String pdv;
	
	@Column(name="fiscal")
	private String fiscal;
	
	@Column(name="crediario")
	private String crediario;
	
	@Column(name="ip")
	private String ip;
	
	@Column(name="matricial")
	private String matricial;

	public Impressora()
	{
		super();
	}
	

	public Impressora
	(
		String nome,
		String caminho,
		String descricao,
		String caixa,
		String pdv,
		String fiscal,
		String crediario,
		String matricial
	) 
	{
		super();
		this.nome = nome;
		this.caminho = caminho;
		this.descricao = descricao;
		this.caixa = caixa;
		this.pdv = pdv;
		this.fiscal = fiscal;
		this.crediario = crediario;
		this.matricial = matricial;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id != null && id == 0)
		{
			this.id = null;
		}
		else
		{
			this.id = id;
		}
	}	
	public String getNome() {
		String retorno = null;
		
		if (nome != null)
			retorno = nome.toUpperCase().trim();
		return retorno;
	}
	
	public void setNome(String nome) {
		if (nome != null)
		{
			this.nome = nome.toUpperCase().trim();
		}
		else
			this.nome = null;
			
		
	}
		
	public String getCaminho() {
		String retorno = null;
		
		if (caminho != null)
			retorno = caminho.toUpperCase().trim();
		return retorno;
	}
	
	public void setCaminho(String caminho) {
		if (caminho != null)
		{
			this.caminho = caminho.toUpperCase().trim();
		}
		else
			this.caminho = null;
			
		
	}
		
	public String getDescricao() {
		String retorno = null;
		
		if (descricao != null)
			retorno = descricao.toUpperCase().trim();
		return retorno;
	}
	
	public void setDescricao(String descricao) {
		if (descricao != null)
		{
			this.descricao = descricao.toUpperCase().trim();
		}
		else
			this.descricao = null;
			
		
	}
		
	public String getCaixa() {
		String retorno = null;
		
		if (caixa != null)
			retorno = caixa.toUpperCase().trim();
		return retorno;
	}
	
	public void setCaixa(String caixa) {
		if (caixa != null)
		{
			this.caixa = caixa.toUpperCase().trim();
		}
		else
			this.caixa = null;
			
		
	}
		
	public String getPdv() {
		String retorno = null;
		
		if (pdv != null)
			retorno = pdv.toUpperCase().trim();
		return retorno;
	}
	
	public void setPdv(String pdv) {
		if (pdv != null)
		{
			this.pdv = pdv.toUpperCase().trim();
		}
		else
			this.pdv = null;
			
		
	}
		
	public String getFiscal() {
		String retorno = null;
		
		if (fiscal != null)
			retorno = fiscal.toUpperCase().trim();
		return retorno;
	}
	
	public void setFiscal(String fiscal) {
		if (fiscal != null)
		{
			this.fiscal = fiscal.toUpperCase().trim();
		}
		else
			this.fiscal = null;
			
		
	}
		
	public String getCrediario() {
		String retorno = null;
		
		if (crediario != null)
			retorno = crediario.toUpperCase().trim();
		return retorno;
	}
	
	public void setCrediario(String crediario) {
		if (crediario != null)
		{
			this.crediario = crediario.toUpperCase().trim();
		}
		else
			this.crediario = null;
			
		
	}
	
	public void setIp(String ip)
	{
		if(ip != null)
			this.ip = ip;
		else
			this.ip = null;
	}
	
	public String getIp()
	{
		return this.ip;
	}
		
	public String getMatricial() {
		return matricial;
	}


	public void setMatricial(String matricial) {
		this.matricial = matricial;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
