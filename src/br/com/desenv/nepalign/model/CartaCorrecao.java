package br.com.desenv.nepalign.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.text.Normalizer;
import javax.persistence.Lob;

import br.com.desenv.frameworkignorante.GenericModelIGN;

import java.util.Date;

@Entity
@Table(name="cartacorrecao")


public class CartaCorrecao extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idCartaCorrecao")
	private Integer id;
		
	@Column(name="cnpjEmitente")
	private String cnpjEmitente;
	
	@Column(name="serieNotaFiscal")
	private String serieNotaFiscal;
	
	@Column(name="numeroNotaFiscal")
	private Integer numeroNotaFiscal;
	
	@Column(name="textoCarta")
	private String textoCarta;
	
	@Column(name="identificador")
	private Integer identificador;
	
	@Column(name="dataHoraRec")
	private Date dataHoraRec;
	
	@Column(name="protocolo")
	private String protocolo;
	
	@Column(name="motivoStatus")
	private String motivoStatus;
	
	@Column(name="status")
	private Integer status;
	
	@Column(name="codEvento")
	private Integer codEvento;
	
	
	public CartaCorrecao()
	{
		super();
	}
	

	public CartaCorrecao
	(
		String cnpjEmitente,
		String serieNotaFiscal,
		Integer numeroNotaFiscal,
		String textoCarta,
		Integer identificador,
		Date dataHoraRec,
		String protocolo,
		String motivoStatus,
		Integer status,
		Integer codEvento
	) 
	{
		super();
		this.cnpjEmitente = cnpjEmitente;
		this.serieNotaFiscal = serieNotaFiscal;
		this.numeroNotaFiscal = numeroNotaFiscal;
		this.textoCarta = textoCarta;
		this.identificador = identificador;
		this.dataHoraRec = dataHoraRec;
		this.protocolo = protocolo;
		this.motivoStatus = motivoStatus;
		this.status = status;
		this.codEvento = codEvento;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	public String getCnpjEmitente() {
		String retorno = null;
		
		if (cnpjEmitente != null)
			retorno = cnpjEmitente.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setCnpjEmitente(String cnpjEmitente) {
		if (cnpjEmitente != null)
		{
			this.cnpjEmitente = cnpjEmitente.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.cnpjEmitente = null;
			
		
	}
		
	public String getSerieNotaFiscal() {
		String retorno = null;
		
		if (serieNotaFiscal != null)
			retorno = serieNotaFiscal.toUpperCase().trim();
		return retorno;
	}
	
	public void setSerieNotaFiscal(String serieNotaFiscal) {
		if (serieNotaFiscal != null)
		{
			this.serieNotaFiscal = serieNotaFiscal.toUpperCase().trim();
		}
		else
			this.serieNotaFiscal = null;
			
		
	}
		
	public Integer getNumeroNotaFiscal() {
		return numeroNotaFiscal;
	}

	public void setNumeroNotaFiscal(Integer numeroNotaFiscal) {
		this.numeroNotaFiscal = numeroNotaFiscal;
	}	
	public String getTextoCarta() {
		String retorno = null;
		
		if (textoCarta != null)
			retorno = textoCarta.toUpperCase().trim();
		return retorno;
	}
	
	public void setTextoCarta(String textoCarta) {
		if (textoCarta != null)
		{
			this.textoCarta = textoCarta.toUpperCase().trim();
		}
		else
			this.textoCarta = null;
			
		
	}
		
	public Integer getIdentificador() {
		return this.identificador;
	}
	
	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
		
	}
		
	public Date getDataHoraRec() {
		return dataHoraRec;
	}

	public void setDataHoraRec(Date dataHoraRec) {
		this.dataHoraRec = dataHoraRec;
	}
	public String getProtocolo() {
		String retorno = null;
		
		if (protocolo != null)
			retorno = protocolo.toUpperCase().trim();
		return retorno;
	}
	
	public void setProtocolo(String protocolo) {
		if (protocolo != null)
		{
			this.protocolo = protocolo.toUpperCase().trim();
		}
		else
			this.protocolo = null;
			
		
	}
		
	public String getMotivoStatus() {
		String retorno = null;
		
		if (motivoStatus != null)
			retorno = motivoStatus.toUpperCase().trim();
		return retorno;
	}
	
	public void setMotivoStatus(String motivoStatus) {
		if (motivoStatus != null)
		{
			this.motivoStatus = motivoStatus.toUpperCase().trim();
		}
		else
			this.motivoStatus = null;
			
		
	}
		
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}	
	
	
	public Integer getCodEvento() {
		return codEvento;
	}


	public void setCodEvento(Integer codEvento) {
		this.codEvento = codEvento;
	}


	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
