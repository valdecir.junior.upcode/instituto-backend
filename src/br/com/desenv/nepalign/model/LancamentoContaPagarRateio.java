package br.com.desenv.nepalign.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="lancamentocontapagarrateio")
public class LancamentoContaPagarRateio extends GenericModelIGN implements Cloneable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idLancamentoContaPagarRateio")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "idLancamentoContaPagar")
	private LancamentoContaPagar lancamentoContaPagar;
	@ManyToOne
	@JoinColumn(name = "idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	@Column(name="porcentagemRateio")
	private BigDecimal porcentagemRateio;
	
	@Override
	public Integer getId() 
	{
		return id;
	}

	@Override
	public void setId(Integer id) 
	{
		this.id = id;
	}

	public LancamentoContaPagar getLancamentoContaPagar()
	{
		return lancamentoContaPagar;
	}

	public void setLancamentoContaPagar(LancamentoContaPagar lancamentoContaPagar) 
	{
		this.lancamentoContaPagar = lancamentoContaPagar;
	}

	public EmpresaFisica getEmpresaFisica()
	{
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica)
	{
		this.empresaFisica = empresaFisica;
	}

	public BigDecimal getPorcentagemRateio()
	{
		return porcentagemRateio;
	}

	public void setPorcentagemRateio(BigDecimal porcentagemRateio)
	{
		this.porcentagemRateio = porcentagemRateio;
	}
	
	@Override
	public LancamentoContaPagarRateio clone() throws CloneNotSupportedException 
	{
		return (LancamentoContaPagarRateio) super.clone();
	}
	
	@Override
	public void validate() throws Exception { }
}