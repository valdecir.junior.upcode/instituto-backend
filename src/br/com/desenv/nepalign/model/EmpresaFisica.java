package br.com.desenv.nepalign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;

@Entity
@Table(name="EmpresaFisica")


public class EmpresaFisica extends GenericModelIGN
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idEmpresaFisica")
	private Integer id;
		
	@Column(name="razaoSocial")
	private String razaoSocial;
	
	@Column(name="nomeFantasia")
	private String nomeFantasia;
	
	@Column(name="cnpj")
	private String cnpj;
	
	@Column(name="anoAberto")
	private Integer anoAberto;
	
	@Column(name="mesAberto")
	private Integer mesAberto;
	
	@Column(name="taxaJuros")
	private Double taxaJuros;
	
	@Column(name="endereco")
	private String endereco;
	
	@Column(name="numero")
	private String numero;
	
	@ManyToOne
	@JoinColumn(name = "idCidade")
	private Cidade cidade;
	@ManyToOne
	@JoinColumn(name = "idGrupoEmpresa")
	private GrupoEmpresa grupoEmpresa;
	@Column(name="cidadeCache")
	private String cidadeCache;
	
	@Column(name="ufCache")
	private String ufCache;
	
	@Column(name="inscricaoEstadual")
	private String inscricaoEstadual;
	
	@ManyToOne
	@JoinColumn(name = "idOperadora")
	private Operadora operadora;
	@Column(name="telefone")
	private String telefone;
	
	@ManyToOne
	@JoinColumn(name = "idAtividade")
	private Atividade atividade;
	
	@Column (name="cep")
	private String cep;
	
	@Column(name="mesAbertoFin")
	private Integer mesAbertoFin;
	
	@Column(name="anoAbertoFin")
	private Integer anoAbertoFin;
	
	@Column(name="percImpostoConsumidor")
	private Double percImpostoConsumidor;

	public EmpresaFisica()
	{
		super();
	}
	

	public EmpresaFisica
	(
		String razaoSocial,
		String nomeFantasia,
		String cnpj,
		Integer anoAberto,
		Integer mesAberto,
		Double taxaJuros,
		String endereco,
		String numero,
	Cidade cidade,
		String cidadeCache,
		String ufCache,
		String inscricaoEstadual,
	Operadora operadora,
		String telefone,
		GrupoEmpresa grupoEmpresa,
		Atividade atividade,
		String cep,
		Integer mesAbertoFin,
		Integer anoAbertoFin,
		Double percImpostoConsumidor
	) 
	{
		super();
		this.razaoSocial = razaoSocial;
		this.nomeFantasia = nomeFantasia;
		this.cnpj = cnpj;
		this.anoAberto = anoAberto;
		this.mesAberto = mesAberto;
		this.taxaJuros = taxaJuros;
		this.endereco = endereco;
		this.numero = numero;
		this.cidade = cidade;
		this.cidadeCache = cidadeCache;
		this.ufCache = ufCache;
		this.inscricaoEstadual = inscricaoEstadual;
		this.operadora = operadora;
		this.telefone = telefone;
		this.grupoEmpresa =grupoEmpresa;
		this.atividade = atividade;
		this.cep = cep;
		this.mesAbertoFin = mesAbertoFin;
		this.anoAbertoFin = anoAbertoFin;
		this.percImpostoConsumidor = percImpostoConsumidor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if(id != null && id == 0)
			this.id = null;
		else
			this.id = id;
		
	}	
	public String getRazaoSocial() {
		String retorno = null;
		
		if (razaoSocial != null)
			retorno = razaoSocial.toUpperCase().trim();
		return retorno;
	}
	
	public void setRazaoSocial(String razaoSocial) {
		if (razaoSocial != null)
		{
			this.razaoSocial = razaoSocial.toUpperCase().trim();
		}
		else
			this.razaoSocial = null;
	}
	
	public String getNomeFantasia() {
		String retorno = null;
		
		if (razaoSocial != null)
			retorno = nomeFantasia.toUpperCase().trim();
		return retorno;
	}


	public void setNomeFantasia(String nomeFantasia) {
		if (nomeFantasia != null)
		{
			this.nomeFantasia = nomeFantasia.toUpperCase().trim();
		}
		else
			this.nomeFantasia = null;
	}
		
	public GrupoEmpresa getGrupoEmpresa() {
		return grupoEmpresa;
	}


	public void setGrupoEmpresa(GrupoEmpresa grupoEmpresa) {
		this.grupoEmpresa = grupoEmpresa;
	}


	public String getCnpj() {
		String retorno = null;
		
		if (cnpj != null)
			retorno = cnpj.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setCnpj(String cnpj) {
		if (cnpj != null)
		{
			this.cnpj = cnpj.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.cnpj = null;
			
		
	}
		
	public Integer getAnoAberto() {
		return anoAberto;
	}

	public void setAnoAberto(Integer anoAberto) {
		if (anoAberto != null && anoAberto == 0)
		{
			this.anoAberto = null;
		}
		else
		{
			this.anoAberto = anoAberto;
		}
		
	}	
	public Integer getMesAberto() {
		return mesAberto;
	}

	public void setMesAberto(Integer mesAberto) {
		if (mesAberto != null && mesAberto == 0)
		{
			this.mesAberto = null;
		}
		else
		{
			this.mesAberto = mesAberto;
		}
		
	}	
	public Double getTaxaJuros() {
		return taxaJuros;
	}

	public void setTaxaJuros(Double taxaJuros) {
		if (taxaJuros != null && Double.isNaN(taxaJuros))
		{
			this.taxaJuros = null;
		}
		else
		{
			this.taxaJuros = taxaJuros;
		}
	}	
	public String getEndereco() {
		String retorno = null;
		
		if (endereco != null)
			retorno = endereco.toUpperCase().trim();
		return retorno;
	}
	
	public void setEndereco(String endereco) {
		if (endereco != null)
		{
			this.endereco = endereco.toUpperCase().trim();
		}
		else
			this.endereco = null;
			
		
	}
		
	public String getNumero() {
		String retorno = null;
		
		if (numero != null)
			retorno = numero.toUpperCase().trim();
		return retorno;
	}
	
	public void setNumero(String numero) {
		if (numero != null)
		{
			this.numero = numero.toUpperCase().trim();
		}
		else
			this.numero = null;
			
		
	}
		
	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	
	public String getCidadeCache() {
		String retorno = null;
		
		if (cidadeCache != null)
			retorno = cidadeCache.toUpperCase().trim();
		return retorno;
	}
	
	public void setCidadeCache(String cidadeCache) {
		if (cidadeCache != null)
		{
			this.cidadeCache = cidadeCache.toUpperCase().trim();
		}
		else
			this.cidadeCache = null;
			
		
	}
		
	public String getUfCache() {
		String retorno = null;
		
		if (ufCache != null)
			retorno = ufCache.toUpperCase().trim();
		return retorno;
	}
	
	public void setUfCache(String ufCache) {
		if (ufCache != null)
		{
			this.ufCache = ufCache.toUpperCase().trim();
		}
		else
			this.ufCache = null;
			
		
	}
		
	public String getInscricaoEstadual() {
		String retorno = null;
		
		if (inscricaoEstadual != null)
			retorno = inscricaoEstadual.toUpperCase().trim();
		return retorno;
	}
	
	public void setInscricaoEstadual(String inscricaoEstadual) {
		if (inscricaoEstadual != null)
		{
			this.inscricaoEstadual = inscricaoEstadual.toUpperCase().trim();
		}
		else
			this.inscricaoEstadual = null;
			
		
	}
		
	public Operadora getOperadora() {
		return operadora;
	}

	public void setOperadora(Operadora operadora) {
		this.operadora = operadora;
	}
	
	public String getTelefone() {
		String retorno = null;
		
		if (telefone != null)
			retorno = telefone.toUpperCase().trim().replaceAll("[^0-9]", "");
		return retorno;
	}
	
	public void setTelefone(String telefone) {
		if (telefone != null)
		{
			this.telefone = telefone.toUpperCase().trim().replaceAll("[^0-9]", "");
		}
		else
			this.telefone = null;
			
		
	}
	
		
	public Atividade getAtividade() {
		return atividade;
	}


	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}


	public String getCep() {
		return cep;
	}


	public void setCep(String cep) {
		this.cep = cep;
	}


	public Integer getMesAbertoFin() {
		return mesAbertoFin;
	}


	public void setMesAbertoFin(Integer mesAbertoFin) {
		if (mesAbertoFin != null && mesAbertoFin == 0)
		{
			this.mesAbertoFin = null;
		}
		else
		{
			this.mesAbertoFin = mesAbertoFin;
		}
	}


	public Integer getAnoAbertoFin() {
		return anoAbertoFin;
	}


	public void setAnoAbertoFin(Integer anoAbertoFin) {
		if (anoAbertoFin != null && anoAbertoFin == 0)
		{
			this.anoAbertoFin = null;
		}
		else
		{
			this.anoAbertoFin = anoAbertoFin;
		}
	}
	
	public Double getPercImpostoConsumidor()
	{
		return percImpostoConsumidor;
	}
	
	public void setPercImpostoConsumidor(Double percImpostoConsumidor){
		if(percImpostoConsumidor != null && Double.isNaN(percImpostoConsumidor))
			this.percImpostoConsumidor = null;
		else
			this.percImpostoConsumidor = percImpostoConsumidor;
	}
	
	public String getIdFormatado()
	{
		return "F"+(getId().SIZE==1?"0"+getId():getId());
	}
	public String getNomeConcatenado()
	{
		return getIdFormatado()+" - "+getRazaoSocial();
	}
	@Override
	public void validate() throws Exception
	{
		// Coloque aqui suas validações ignorantes
		/*
		if (this.descricao == null || this.descricao.trim().equals(""))
		{
			throw new Exception("Descrição é obrigatório.");
		}
		*/
		
	}

}
