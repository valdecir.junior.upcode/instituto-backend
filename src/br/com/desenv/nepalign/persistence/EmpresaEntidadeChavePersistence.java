package br.com.desenv.nepalign.persistence;

import java.sql.Connection;
import java.sql.ResultSet;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.EmpresaEntidadeChave;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class EmpresaEntidadeChavePersistence extends GenericPersistenceIGN<EmpresaEntidadeChave>
{
    public Integer getUltimaChave(EmpresaEntidadeChave empresaEntidadeChave) throws Exception
    {
    	Connection connection = null;
    	Integer ultimaChave = null;

    	try
    	{
    		connection = ConexaoUtil.getConexaoPadrao();
	    
    		String[] listaParte = empresaEntidadeChave.getEntidade().split("\\.");
    		String entidadeApenas = listaParte[listaParte.length - 1];

    		String sql = "SELECT MAX(id" + entidadeApenas + ") FROM " + entidadeApenas;

    		ResultSet resultSet = connection.createStatement().executeQuery(sql);

    		if (resultSet.first())
    			ultimaChave = resultSet.getInt(1);
    	} 
    	catch (Exception ex)
    	{
    		ex.printStackTrace();
    	} 
    	finally
    	{
    		if (connection != null)
    		{
    			connection.close();
    			connection = null;
    		}
    	}
		return ultimaChave;
    }    	
    
    
    public Integer getUltimaChave(EmpresaEntidadeChave empresaEntidadeChave, Integer max) throws Exception
    {
    	Connection connection = null;
    	Integer ultimaChave = null;

    	try
    	{
    		connection = ConexaoUtil.getConexaoPadrao();
	    
    		String[] listaParte = empresaEntidadeChave.getEntidade().split("\\.");
    		String entidadeApenas = listaParte[listaParte.length - 1];

    		String sql = "SELECT MAX(id" + entidadeApenas + ") FROM " + entidadeApenas + " WHERE id" + entidadeApenas + " < " + max;

    		ResultSet resultSet = connection.createStatement().executeQuery(sql);

    		if (resultSet.first())
    			ultimaChave = resultSet.getInt(1);
    	} 
    	catch (Exception ex)
    	{
    		ex.printStackTrace();
    	} 
    	finally
    	{
    		if (connection != null)
    		{
    			connection.close();
    			connection = null;
    		}
    	}
		return ultimaChave;
    }    	
}