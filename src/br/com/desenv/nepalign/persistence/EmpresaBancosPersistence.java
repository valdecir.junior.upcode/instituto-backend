package br.com.desenv.nepalign.persistence;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.EmpresaBancos;

public class EmpresaBancosPersistence extends GenericPersistenceIGN<EmpresaBancos>
{
	public EmpresaBancosPersistence() { super(); }
}