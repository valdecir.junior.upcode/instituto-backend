package br.com.desenv.nepalign.persistence;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.ContaContabil;

public class ContaContabilPersistence extends GenericPersistenceIGN<ContaContabil>
{
	public ContaContabilPersistence() { super(); }
}