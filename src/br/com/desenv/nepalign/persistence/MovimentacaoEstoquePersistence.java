package br.com.desenv.nepalign.persistence;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.DsvException;
import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.MovimentacaoEstoque;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.service.ClienteService;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.EstoqueService;
import br.com.desenv.nepalign.service.FornecedorService;
import br.com.desenv.nepalign.service.LogOperacaoService;
import br.com.desenv.nepalign.service.StatusMovimentacaoEstoqueService;
import br.com.desenv.nepalign.service.TipoDocumentoMovimentoEstoqueService;
import br.com.desenv.nepalign.service.TipoMovimentacaoEstoqueService;
import br.com.desenv.nepalign.service.UsuarioService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class MovimentacaoEstoquePersistence extends GenericPersistenceIGN<MovimentacaoEstoque>
{
	public LogOperacaoService logOperacaoService;
	
	public List<MovimentacaoEstoque> listarPorObjetoFiltroProdutoSql(EntityManager manager, Produto produto, EmpresaFisica empresaFisica, String dataBase) throws Exception
	{
		return listarPorObjetoFiltroProdutoSql(manager, produto, empresaFisica, dataBase, false);
	}
	
	public List<MovimentacaoEstoque> listarPorObjetoFiltroProdutoSql(EntityManager manager, Produto produto, EmpresaFisica empresaFisica, String dataBase, Boolean trazerSomenteId) throws Exception
	{
		Boolean managerIndependente = false;
		List<MovimentacaoEstoque> listaMovimentacaoEstoque = new ArrayList<MovimentacaoEstoque>();
		Connection connection = null;
		try
		{
			if(manager == null)
				manager = ConexaoUtil.getEntityManager();
			
			connection = ConexaoUtil.getConexaoPadrao();
			
			
			StringBuilder query = new StringBuilder();
			
			query.append("select mov.* from movimentacaoEstoque mov where idMovimentacaoEstoque in ");
			query.append("(select idMovimentacaoEstoque from itemMovimentacaoEstoque where idproduto=" + produto.getId() + ")" );
			
			if(dataBase != null)
				query.append("and dataMovimentacao >= '"+ new SimpleDateFormat("yyyy-mm-dd").format(new SimpleDateFormat("dd/mm/yyyy").parse(dataBase))+" 00:00:00'");
			
			if(empresaFisica != null)
				query.append("and idEmpresaFisica = " + empresaFisica.getId());
			
			ResultSet rs = connection.createStatement().executeQuery(String.valueOf(query));

			while(rs.next())
			{
				MovimentacaoEstoque movimentacaoEstoqueBuffer = new MovimentacaoEstoque();
				movimentacaoEstoqueBuffer.setId(rs.getInt("idMovimentacaoEstoque"));
				
				if(!trazerSomenteId)
				{
					movimentacaoEstoqueBuffer.setEmpresaDestino(new EmpresaFisicaService().recuperarPorId((Integer)rs.getInt("idEmpresaFisica")));
					movimentacaoEstoqueBuffer.setEstoque(new EstoqueService().recuperarPorId((Integer)rs.getInt("idEstoque")));
					movimentacaoEstoqueBuffer.setTipoMovimentacaoEstoque(new TipoMovimentacaoEstoqueService().recuperarPorId((Integer)rs.getInt("idTipoMovimentacaoEstoque")));
					movimentacaoEstoqueBuffer.setTipoDocumentoMovimento(new TipoDocumentoMovimentoEstoqueService().recuperarPorId((Integer)rs.getInt("idTipoDocumentoMovimentoEstoque")));
					movimentacaoEstoqueBuffer.setCliente(new ClienteService().recuperarPorId((Integer)rs.getInt("idCliente")));
					movimentacaoEstoqueBuffer.setUsuario(new UsuarioService().recuperarPorId((Integer)rs.getInt("idUsuario")));
					movimentacaoEstoqueBuffer.setEntradaSaida(rs.getString("entradaSaida"));
					movimentacaoEstoqueBuffer.setNumeroDocumento(rs.getString("numeroDocumento"));
					movimentacaoEstoqueBuffer.setDataMovimentacao(rs.getDate("dataMovimentacao"));
					movimentacaoEstoqueBuffer.setIdDocumentoOrigem(rs.getInt("idDocumentoOrigem"));
					movimentacaoEstoqueBuffer.setObservacao(rs.getString("observacao"));
					movimentacaoEstoqueBuffer.setFaturado(rs.getString("faturado"));
					movimentacaoEstoqueBuffer.setManual(rs.getString("manual"));
					movimentacaoEstoqueBuffer.setNumeroVolume(rs.getInt("numeroVolume"));
					movimentacaoEstoqueBuffer.setPeso(rs.getDouble("peso"));
					movimentacaoEstoqueBuffer.setValorFrete(rs.getDouble("valorFrete"));
					movimentacaoEstoqueBuffer.setForncedor(new FornecedorService().recuperarPorId((Integer)rs.getInt("idFornecedor")));
					movimentacaoEstoqueBuffer.setStatusMovimentacaoEstoque(new StatusMovimentacaoEstoqueService().recuperarPorId((Integer)rs.getInt("idStatusMovimentacaoEstoque")));
					movimentacaoEstoqueBuffer.setEmpresaDestino(new EmpresaFisicaService().recuperarPorId((Integer)rs.getInt("idEmpresaDestino")));	
				}
				
				listaMovimentacaoEstoque.add(movimentacaoEstoqueBuffer);
			}
			
			return listaMovimentacaoEstoque;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
			connection.close();
			listaMovimentacaoEstoque = null;
			System.gc();
		}
	}
	
	public List<MovimentacaoEstoque> listarPorObjetoFiltroPorIdDocumentoOrigem(EntityManager manager, Integer idDocumentoOrigem) throws Exception
	{
		return listarPorObjetoFiltroPorIdDocumentoOrigem(manager, idDocumentoOrigem, null, false);
	}
	
	public List<MovimentacaoEstoque> listarPorObjetoFiltroPorIdDocumentoOrigem(EntityManager manager, Integer idDocumentoOrigem, Integer idEmpresaFisica) throws Exception
	{
		return listarPorObjetoFiltroPorIdDocumentoOrigem(manager, idDocumentoOrigem, idEmpresaFisica, false);
	}
	
	public List<MovimentacaoEstoque> listarPorObjetoFiltroPorIdDocumentoOrigem(EntityManager manager, Integer idDocumentoOrigem, Integer idEmpresaFisica, Boolean trazerSomenteId) throws Exception
	{
		Boolean managerIndependente = false;
		List<MovimentacaoEstoque> listaMovimentacaoEstoque = new ArrayList<MovimentacaoEstoque>();
		Connection connection = null;
		try
		{
			if(manager == null)
				manager = ConexaoUtil.getEntityManager();
			
			connection = ConexaoUtil.getConexaoPadrao();
			
			
			StringBuilder query = new StringBuilder();
			
			query.append("select " + (trazerSomenteId ? " mov.idMovimentacaoEstoque " : "mov.*") + " from movimentacaoEstoque mov where idDocumentoOrigem = " + idDocumentoOrigem + " ");

			if(idEmpresaFisica != null)
				query.append("and idEmpresaFisica = " + idEmpresaFisica);
			
			ResultSet rs = connection.createStatement().executeQuery(String.valueOf(query));

			while(rs.next())
			{
				MovimentacaoEstoque movimentacaoEstoqueBuffer = new MovimentacaoEstoque();
				movimentacaoEstoqueBuffer.setId(rs.getInt("idMovimentacaoEstoque"));
				
				if(!trazerSomenteId)
				{
					movimentacaoEstoqueBuffer.setEmpresaDestino(new EmpresaFisicaService().recuperarPorId((Integer)rs.getInt("idEmpresaFisica")));
					movimentacaoEstoqueBuffer.setEstoque(new EstoqueService().recuperarPorId((Integer)rs.getInt("idEstoque")));
					movimentacaoEstoqueBuffer.setTipoMovimentacaoEstoque(new TipoMovimentacaoEstoqueService().recuperarPorId((Integer)rs.getInt("idTipoMovimentacaoEstoque")));
					movimentacaoEstoqueBuffer.setTipoDocumentoMovimento(new TipoDocumentoMovimentoEstoqueService().recuperarPorId((Integer)rs.getInt("idTipoDocumentoMovimentoEstoque")));
					movimentacaoEstoqueBuffer.setCliente(new ClienteService().recuperarPorId((Integer)rs.getInt("idCliente")));
					movimentacaoEstoqueBuffer.setUsuario(new UsuarioService().recuperarPorId((Integer)rs.getInt("idUsuario")));
					movimentacaoEstoqueBuffer.setEntradaSaida(rs.getString("entradaSaida"));
					movimentacaoEstoqueBuffer.setNumeroDocumento(rs.getString("numeroDocumento"));
					movimentacaoEstoqueBuffer.setDataMovimentacao(rs.getDate("dataMovimentacao"));
					movimentacaoEstoqueBuffer.setIdDocumentoOrigem(rs.getInt("idDocumentoOrigem"));
					movimentacaoEstoqueBuffer.setObservacao(rs.getString("observacao"));
					movimentacaoEstoqueBuffer.setFaturado(rs.getString("faturado"));
					movimentacaoEstoqueBuffer.setManual(rs.getString("manual"));
					movimentacaoEstoqueBuffer.setNumeroVolume(rs.getInt("numeroVolume"));
					movimentacaoEstoqueBuffer.setPeso(rs.getDouble("peso"));
					movimentacaoEstoqueBuffer.setValorFrete(rs.getDouble("valorFrete"));
					movimentacaoEstoqueBuffer.setForncedor(new FornecedorService().recuperarPorId((Integer)rs.getInt("idFornecedor")));
					movimentacaoEstoqueBuffer.setStatusMovimentacaoEstoque(new StatusMovimentacaoEstoqueService().recuperarPorId((Integer)rs.getInt("idStatusMovimentacaoEstoque")));
					movimentacaoEstoqueBuffer.setEmpresaDestino(new EmpresaFisicaService().recuperarPorId((Integer)rs.getInt("idEmpresaDestino")));	
				}
				
				listaMovimentacaoEstoque.add(movimentacaoEstoqueBuffer);
			}
			
			return listaMovimentacaoEstoque;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
			connection.close();
			listaMovimentacaoEstoque = null;
			System.gc();
		}
	}


	@Override
	public MovimentacaoEstoque salvar(MovimentacaoEstoque entidade) throws DsvException
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		
		MovimentacaoEstoque entidadeSalva = null;

		try
		{
			transaction.begin();
			
			entidadeSalva = salvar(manager, transaction, entidade);

			transaction.commit();
		}
		catch (Exception e)
		{
			if(transaction != null && transaction.isActive())
				transaction.rollback();
			
			throw new DsvException(e);
		}
		finally
		{
			manager.close();
		}
		
		return entidadeSalva;
	} 
	
	@Override
	public MovimentacaoEstoque salvar(EntityManager manager, EntityTransaction transaction, MovimentacaoEstoque entidade) throws DsvException
	{
		MovimentacaoEstoque entidadeSalva = null;
		Boolean transacaoIndependente = false;

		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();	
				transacaoIndependente = true;
			}
			
			entidadeSalva = super.salvar(manager, transaction, entidade);
			
			if(logOperacaoService == null)
				new LogOperacaoService().gravarOperacaoInclusao(manager, transaction, entidadeSalva);
			else
				logOperacaoService.gravarOperacaoInclusao(manager, transaction, entidadeSalva);

			if(transacaoIndependente)
				transaction.commit();
		}
		catch (Exception e)
		{
			if(transacaoIndependente)
				transaction.rollback();
			
			throw new DsvException(e);
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
		
		return entidadeSalva;
	} 
	
	@Override
	public MovimentacaoEstoque atualizar(MovimentacaoEstoque entidade) throws DsvException
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		
		MovimentacaoEstoque entidadeSalva = null;

		try
		{
			transaction.begin();
			
			new LogOperacaoService().gravarOperacaoAlterar(manager, transaction, entidade);
			
			entidadeSalva = atualizar(manager, transaction, entidade);
			
			transaction.commit();
		}
		catch (Exception e)
		{
			if(transaction != null && transaction.isActive())
				transaction.rollback();
			
			throw new DsvException(e);
		}
		finally
		{
			manager.close();
		}
		
		return entidadeSalva;
	} 
	
	@Override
	public MovimentacaoEstoque atualizar(EntityManager manager, EntityTransaction transaction, MovimentacaoEstoque entidade) throws DsvException
	{
		MovimentacaoEstoque entidadeSalva = null;
		Boolean transacaoIndependente = false;
		
		try
		{
			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
			
			//transaction.begin();
			
			if(logOperacaoService == null)
				new LogOperacaoService().gravarOperacaoAlterar(manager, transaction, entidade);
			else
				logOperacaoService.gravarOperacaoAlterar(manager, transaction, entidade);
			
			entidadeSalva = super.atualizar(manager, transaction, entidade);
			
			if(transacaoIndependente)
				transaction.commit();
		}
		catch (Exception e)
		{
			if(transacaoIndependente)
				transaction.rollback();
			
			throw new DsvException(e);
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
		
		return entidadeSalva;
	} 

	public boolean existeTransferenciaPendente(EntityManager manager, final EmpresaFisica idEmpresaFisica, final Integer dias) throws Exception
	{
		return existeTransferenciaPendente(manager, idEmpresaFisica.getId(), dias);
	}
	
	public boolean existeTransferenciaPendente(EntityManager manager, final Integer idEmpresaFisica, final Integer dias) throws Exception
	{ 
		final StringBuilder sql = new StringBuilder();
		sql.append(" SELECT  ");
		sql.append(" 	COUNT(0) ");
		sql.append(" FROM ");
		sql.append("     movimentacaoestoque ");
		sql.append(" WHERE ");
		sql.append("     idtipomovimentacaoestoque IN (3 , 4) "); 
		sql.append("         AND DATEDIFF(NOW(), dataMovimentacao) > " + dias); 
		sql.append("         AND idStatusMovimentacaoEstoque = 1 ");
		sql.append("         AND idEmpresaFisica IN (" + idEmpresaFisica + ") ");
		
		return ((BigInteger) manager.createNativeQuery(sql.toString()).getSingleResult()).intValue() > 0x00;
	}
}