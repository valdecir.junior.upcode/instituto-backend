package br.com.desenv.nepalign.persistence;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.CaixaDia;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class CaixaDiaPersistence extends GenericPersistenceIGN<CaixaDia>
{
public CaixaDia recuperarCaixaDia(Usuario usu) throws Exception
{
	EmpresaFisica empresa = new EmpresaFisicaService().recuperarEmpresaFisicaPorEmpresa(usu.getEmpresaPadrao());
	SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");  
	String data = df.format(new Date());

	
	String str = "";
	str = str + " SELECT caixa.idCaixaDia as 'id' FROM caixadia caixa "; 
	str = str + " Inner join empresafisica empresa on empresa.idEmpresaFisica = caixa.idEmpresaFisica "; 
	str = str + " INNER JOIN usuario usu on usu.idUsuario = caixa.idUsuario "; 
	str = str + " where usu.idUsuario =  "+usu.getId(); 
	str = str + " and empresa.idEmpresaFisica = " + empresa.getId(); 
	str = str + " and caixa.dataAbertura = '"+data+"' and caixa.situacao = '1'"; 
	
	 Connection conn; 
	 conn = ConexaoUtil.getConexaoPadrao();
	 java.sql.Statement rec = conn.createStatement();    
     ResultSet rs = rec.executeQuery(str);
     Integer id = 0;
     if(rs.next()==true)
     {
    	 id = rs.getInt("id");
     }
	return this.recuperarPorId(id);
	
}

public CaixaDia recuperarCaixaDia(EmpresaFisica empresa,boolean hoje,boolean emAberto) throws Exception {
	SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");  
	String data = df.format(new Date());

	
	String str = "";
	str = str + " SELECT caixa.idCaixaDia as 'id' FROM caixadia caixa "; 
	str = str + " Inner join empresafisica empresa on empresa.idEmpresaFisica = caixa.idEmpresaFisica "; 
	str = str + " INNER JOIN usuario usu on usu.idUsuario = caixa.idUsuario "; 
 
	str = str + " where empresa.idEmpresaFisica = " + empresa.getId(); 
	if(hoje){
	str = str + " and caixa.dataAbertura = '"+data+"' ";
	}
	if(emAberto){
		str = str + " and situacao = 1";
	}
	
	 Connection conn; 
	 conn = ConexaoUtil.getConexaoPadrao();
	 java.sql.Statement rec = conn.createStatement();    
     ResultSet rs = rec.executeQuery(str);
     Integer id = 0;
     if(rs.next()==true)
     {
    	 id = rs.getInt("id");
     }
     CaixaDia caixa = new CaixaDia();
    if(id != 0)
    {
    	caixa =  this.recuperarPorId(id);
    }
	return caixa;
}
public Boolean verificaDiaFinalizado(EmpresaFisica empresa,Date data) throws SQLException
{
	SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");  
	String dataSql = df.format(data);

	
	String str = "";
	str = str + " SELECT caixa.idCaixaDia as 'id' FROM caixadia caixa "; 
	str = str + " Inner join empresafisica empresa on empresa.idEmpresaFisica = caixa.idEmpresaFisica "; 
	str = str + " INNER JOIN usuario usu on usu.idUsuario = caixa.idUsuario "; 
 
	str = str + " where empresa.idEmpresaFisica = " + empresa.getId(); 
	
	str = str + " and caixa.dataAbertura = '"+dataSql+"' ";
	
		str = str + " and situacao = 4";

	Boolean diaFechado = false;
	 Connection conn; 
	 conn = ConexaoUtil.getConexaoPadrao();
	 java.sql.Statement rec = conn.createStatement();    
     ResultSet rs = rec.executeQuery(str);
     Integer id = 0;
     if(rs.next()==true)
     {
    	 diaFechado = true;
     }
     return diaFechado;
}

public List<CaixaDia> recuperarUltimosCaixaFechado(EmpresaFisica empresa) throws Exception
{
	String str = "";
	str = str + " SELECT idCaixaDia as 'id'"; 
	str = str + " FROM caixadia  "; 
	str = str + " where situacao <> 1  "; 
	str = str + " and dataFechamento is not null "; 
	str = str + " and idEmpresaFisica = "+empresa.getId(); 
	str = str + " order by dataAbertura desc "; 
	Connection conn; 
	 conn = ConexaoUtil.getConexaoPadrao();
	 java.sql.Statement rec = conn.createStatement();    
    ResultSet rs = rec.executeQuery(str);
    List<Integer> ids = new ArrayList<>();
    while(rs.next())
    {
    	ids.add( rs.getInt("id"));
    }
    List<CaixaDia> dia =  new ArrayList<>();
   for (int i = 0; i < ids.size(); i++)
   {
	   dia.add( this.recuperarPorId(ids.get(i)));
   }
   return dia;
	
}
public List<CaixaDia> recuperarUltimosCaixa(EmpresaFisica empresa) throws Exception
{
	String str = "";
	str = str + " SELECT idCaixaDia as 'id'"; 
	str = str + " FROM caixadia  "; 	 
	str = str + "  where idEmpresaFisica = "+empresa.getId(); 
	str = str + " order by dataAbertura desc "; 
	Connection conn; 
	 conn = ConexaoUtil.getConexaoPadrao();
	 java.sql.Statement rec = conn.createStatement();    
    ResultSet rs = rec.executeQuery(str);
    List<Integer> ids = new ArrayList<>();
    while(rs.next())
    {
    	ids.add( rs.getInt("id"));
    }
    List<CaixaDia> dia =  new ArrayList<>();
   for (int i = 0; i < ids.size(); i++)
   {
	   dia.add( this.recuperarPorId(ids.get(i)));
   }
   return dia;
	
}
public Double recuperarTrocoDoDia(EmpresaFisica empresa,Date dataTroco) throws Exception
{
	int idCancelado = DsvConstante.getParametrosSistema().getInt("idSituacaoPedidoVendaCancelado");
	SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");  
	String dataSql = df.format(dataTroco);
	CaixaDia cxc = new CaixaDia();
	cxc.setempresaFisica(empresa);
	cxc.setDataAbertura(dataTroco);
	List<CaixaDia> lista = this.listarPorObjetoFiltro(cxc);
	if(lista.size()>0)
	{
		cxc = lista.get(0);
	}
	else
	{
		return null;
	}
	
	
	
	String str = "";
	str = str + " select 'TROCO DO DIA SEGUINTE' as descricao, sum(DEBITO) as debito, sum(credito) as credito  "; 
	str = str + " FROM ( (select 'VALOR INICIAL CAIXA' as descricao, valorInicial AS DEBITO,  0 as Credito "; 
	str = str + "  from caixadia where idcaixadia="+cxc.getId()+") union  "; 
	str = str + " (SELECT  'VENDA VISTA' as descricao,  ROUND(SUM(IFNULL(pedido.valorTotalPedido,0)),2) AS DEBITO,  "; 
	str = str + "  0 as credito  FROM pedidovenda pedido   "; 
	str = str + " INNER JOIN PlanoPagamento plano on plano.idPlanoPagamento = pedido.IdPlanoPagamento    "; 
	str = str + "  WHERE pedido.idEmpresaFisica = "+empresa.getId()+""; 
	str = str + " and pedido.dataVenda between '"+dataSql+" 00:00:00' AND '"+dataSql+" 23:59:59'  "; 
	str = str + " and plano.idTipoPagamento=1  and (pedido.idSituacaoPedidoVenda <> 5)  "; 
	str = str + " group by plano.descricao) union  "; 
	str = str + " ( select CONCAT('ENTRADA ',tipoPagamento.descricao) as descricao,  "; 
	str = str + "  SUM(IFNULL(pag.valor, 0)) AS DEBITO,  0 as credito  "; 
	str = str + "  FROM pedidovenda pedido   "; 
	str = str + " INNER JOIN PlanoPagamento plano on plano.idPlanoPagamento = pedido.IdPlanoPagamento "; 
	str = str + "   INNER JOIN pagamentopedidovenda pag on pag.idPedidoVenda = pedido.idPedidoVenda   "; 
	str = str + " INNER JOIN formapagamento forma on forma.idFormaPagamento = pag.idFormaPagamento   "; 
	str = str + " INNER JOIN planopagamento planoPagamento on planoPagamento.idPlanoPagamento = pedido.IdPlanoPagamento  "; 
	str = str + " INNER JOIN tipoPagamento tipoPagamento on tipoPagamento.idTipoPagamento = plano.idTipoPagamento  "; 
	str = str + " WHERE pedido.idEmpresaFisica ="+empresa.getId()+"  "; 
	str = str + " and pedido.dataVenda between '"+dataSql+" 00:00:00' AND '"+dataSql+" 23:59:59'   "; 
	str = str + " and pedido.idSituacaoPedidoVenda <> 5 and pag.entrada = 'S' and forma.tipo=1  "; 
	str = str + "  and plano.idtipopagamento <>1 group by plano.descricao)  "; 
	str = str + "  union  (SELECT CONCAT('ENTRADA CREDIARIO EM ',forma.descricao) as descricao,  "; 
	str = str + "  SUM(IFNULL(mov.valor, 0)) AS DEBITO,  0 as credito  "; 
	str = str + "  from MovimentoCaixaDia mov  "; 
	str = str + " inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia  "; 
	str = str + "  left outer join cliente on cliente.idCliente = mov.idcliente  "; 
	str = str + "  left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia  "; 
	str = str + "  left outer join pagamentopedidovenda on pagamentopedidovenda.idPagamentoPedidoVenda = mov.idPagamentoPedidoVenda  "; 
	str = str + "  left outer join tipoconta on tipoconta.idTipoConta = tipomovimento.idTipoConta  "; 
	str = str + "  left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento "; 
	str = str + "  left outer join duplicataparcela as parcelaCriada on parcelaCriada.idDuplicataParcela = mov.idDuplicataParcela  "; 
	str = str + "  left outer join duplicata as duplicataCriada on duplicataCriada.idDuplicata = parcelaCriada.idDuplicata  "; 
	str = str + "  left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata  "; 
	str = str + "  left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela   "; 
	str = str + " left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata "; 
	str = str + "   where mov.idCaixaDia = "+cxc.getId()+"  "; 
	str = str + " and (parcelaPaga.numeroParcela = 0)  "; 
	str = str + "  AND forma.tipo = 1  "; 
	str = str + "  GROUP BY tipomovimento.origem, forma.descricao order by tipomovimento.origem, forma.descricao) "; 
	str = str + "   union  (SELECT 'RECEB. DE CREDIARIO' as descricao,  "; 
	str = str + "  ROUND(SUM(IFNULL(movimentoparcela.valorRecebido, 0)-IFNULL(movimentoparcela.valorEntradaJuros, 0)+IFNULL(movimentoparcela.valorDesconto,0)),2) as debito, 0 as credito   "; 
	str = str + " from MovimentoCaixaDia mov inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia  "; 
	str = str + "  left outer join cliente on cliente.idCliente = mov.idcliente  "; 
	str = str + "  left outer join tipomovimentocaixadia tipomovimento  "; 
	str = str + " on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia   "; 
	str = str + " left outer join pagamentopedidovenda on pagamentopedidovenda.idPagamentoPedidoVenda = mov.idPagamentoPedidoVenda  "; 
	str = str + "  left outer join tipoconta on tipoconta.idTipoConta = tipomovimento.idTipoConta   "; 
	str = str + " left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento  "; 
	str = str + "  left outer join duplicataparcela as parcelaCriada on parcelaCriada.idDuplicataParcela = mov.idDuplicataParcela   "; 
	str = str + " left outer join duplicata as duplicataCriada on duplicataCriada.idDuplicata = parcelaCriada.idDuplicata   "; 
	str = str + " left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata   "; 
	str = str + " left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela  "; 
	str = str + "  left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata  "; 
	str = str + "  where mov.idCaixaDia = "+cxc.getId()+" "; 
	str = str + "  and tipoMovimento.origem = 2  "; 
	str = str + " and parcelapaga.numeroparcela<>0  "; 
	str = str + " and parcelaPaga.situacao <> '3'  "; 
	str = str + " and duplicatapaga.idempresafisica="+empresa.getId()+"  GROUP BY tipomovimento.origem)  union  "; 
	str = str + "  (SELECT CONCAT('RECEB. DE CREDIARIO ',forma.descricao) as descricao,  0 as debito,  "; 
	str = str + "  ROUND(SUM(COALESCE(movimentoparcela.valorRecebido,0)),2) as credito   "; 
	str = str + " from MovimentoCaixaDia mov inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia   "; 
	str = str + " left outer join cliente on cliente.idCliente = mov.idcliente  "; 
	str = str + "  left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia "; 
	str = str + "   left outer join pagamentopedidovenda on pagamentopedidovenda.idPagamentoPedidoVenda = mov.idPagamentoPedidoVenda   "; 
	str = str + " left outer join tipoconta on tipoconta.idTipoConta = tipomovimento.idTipoConta   "; 
	str = str + " left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento   "; 
	str = str + " left outer join duplicataparcela as parcelaCriada on parcelaCriada.idDuplicataParcela = mov.idDuplicataParcela  "; 
	str = str + "  left outer join duplicata as duplicataCriada on duplicataCriada.idDuplicata = parcelaCriada.idDuplicata  "; 
	str = str + " left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata   "; 
	str = str + " left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela   "; 
	str = str + " left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata   "; 
	str = str + " where mov.idCaixaDia = "+cxc.getId()+" and forma.tipo<>1  "; 
	str = str + " and parcelaPaga.numeroParcela <>0 and COALESCE(parcelaPaga.valorParcela,0) >0  "; 
	str = str + " and duplicatapaga.idempresafisica="+empresa.getId()+"  GROUP BY forma.descricao)   "; 
	str = str + " union  (select concat('RECEB. DE JUROS DE CREDIARIO') as descricao,  "; 
	str = str + "  COALESCE(ROUND(SUM(movimentoparcela.valorEntradaJuros),2),0) - COALESCE(ROUND(SUM(movimentoparcela.valorDesconto),2),0) as debito,  "; 
	str = str + "  0 as credito  from MovimentoCaixaDia mov inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia   "; 
	str = str + " left outer join cliente on cliente.idCliente = mov.idcliente  "; 
	str = str + " left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia  "; 
	str = str + "  left outer join pagamentopedidovenda on pagamentopedidovenda.idPagamentoPedidoVenda = mov.idPagamentoPedidoVenda  "; 
	str = str + "  left outer join tipoconta on tipoconta.idTipoConta = tipomovimento.idTipoConta  "; 
	str = str + "  left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento "; 
	str = str + "   left outer join duplicataparcela as parcelaCriada on parcelaCriada.idDuplicataParcela = mov.idDuplicataParcela   "; 
	str = str + " left outer join duplicata as duplicataCriada on duplicataCriada.idDuplicata = parcelaCriada.idDuplicata  "; 
	str = str + "  left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata "; 
	str = str + "   left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela   "; 
	str = str + " left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata  "; 
	str = str + "  where mov.idCaixaDia = "+cxc.getId()+" "; 
	str = str + "  and tipomovimento.origem=2  "; 
	str = str + " order by tipomovimento.origem, forma.descricao, dataPagamento)  "; 
	str = str + "  union  (SELECT tipoconta.descricao as descricao, "; 
	str = str + " SUM(IF (forma.tipo = 1 and mov.CreditoDebito ='C', mov.valor, 0)) as credito,   "; 
	str = str + "  SUM(IF (forma.tipo<>1, mov.valor, if(forma.tipo=1 and mov.CreditoDebito ='D',mov.valor,0))) as debito  "; 
	str = str + "  from MovimentoCaixaDia mov  inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia  "; 
	str = str + "  left outer join cliente on cliente.idCliente = mov.idcliente  "; 
	str = str + "  left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia  "; 
	str = str + "  left outer join pagamentopedidovenda on pagamentopedidovenda.idPagamentoPedidoVenda = mov.idPagamentoPedidoVenda  "; 
	str = str + "  left outer join tipoconta on tipoconta.idTipoConta = tipomovimento.idTipoConta  "; 
	str = str + "  left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento   "; 
	str = str + " left outer join duplicataparcela as parcelaCriada on parcelaCriada.idDuplicataParcela = mov.idDuplicataParcela  "; 
	str = str + "  left outer join duplicata as duplicataCriada on duplicataCriada.idDuplicata = parcelaCriada.idDuplicata   "; 
	str = str + " left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata   "; 
	str = str + " left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela   "; 
	str = str + " left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata  "; 
	str = str + "  where mov.idCaixaDia = "+cxc.getId()+" and diversos='S' and forma.tipo=1  "; 
	str = str + " GROUP BY tipomovimento.origem, tipoconta.descricao  order by tipomovimento.origem, tipoconta.descricao)  ) as TOTAL "; 
	
	
	
	

	Connection conn; 
	 conn = ConexaoUtil.getConexaoPadrao();
	 java.sql.Statement rec = conn.createStatement();    
   ResultSet rs = rec.executeQuery(str);
   System.out.println(str);
   if(!rs.next())
   {
	  return null; 
   }
   rs.beforeFirst();
   Double debito =0.0;
   Double credito =0.0;
   while (rs.next())
   {
	   debito = rs.getDouble("debito");
	   credito = rs.getDouble("credito");
   }
   Double troco = debito-credito;
   troco = new BigDecimal(troco).setScale(2,BigDecimal.ROUND_HALF_EVEN).doubleValue();
	return troco; 
}
}


