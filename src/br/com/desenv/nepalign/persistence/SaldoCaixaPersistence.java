package br.com.desenv.nepalign.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.Caixa;
import br.com.desenv.nepalign.model.SaldoCaixa;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class SaldoCaixaPersistence extends GenericPersistenceIGN<SaldoCaixa>
{
	private static Object locker = new Object();
	
	public SaldoCaixaPersistence() { super(); }
	
	public SaldoCaixa recuperarSaldoCaixaAtual(EntityManager manager, Caixa caixa) throws Exception
	{
		boolean managerIndependente = false;
		
		if((managerIndependente = (manager == null)))
			manager = ConexaoUtil.getEntityManager();
		
		try
		{
			synchronized(locker)
			{
				if(caixa == null)
					throw new RuntimeException("não foi possível carregar o saldo do caixa. Caixa null");
				
				Query query = manager.createNativeQuery("select * from saldoCaixa WHERE idCaixa = ? and mes = ? and ano = ?", SaldoCaixa.class);
				query.setParameter(0x01, caixa.getId());
				query.setParameter(0x02, caixa.getMesAberto());
				query.setParameter(0x03, caixa.getAnoAberto());
				
				@SuppressWarnings("unchecked")
				List<SaldoCaixa> resultList = query.getResultList();
				
				if(resultList.size() <= 0x00)
					throw new RuntimeException("não foi encontrado o saldo do mês vigente do caixa.");
				else
				{
					if(resultList.size() > 0x01)
						throw new RuntimeException("Encontrado mais de 1 saldo para o mês vigente do caixa!");
					else
						return resultList.get(0x00);
				}
			}	
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
	
	public boolean atualizarSaldoCaixa(EntityManager manager, Caixa caixa, Double valor) throws Exception
	{
		boolean managerIndependente = false;
		
		if((managerIndependente = (manager == null)))
			manager = ConexaoUtil.getEntityManager();
		
		try
		{
			synchronized(locker)
			{
				//EXECUTA AS VERIFICAções NECESsóRIAS PARA ATUALIZAção CORRETA DO SALDO ATUAL;
				recuperarSaldoCaixaAtual(manager, caixa);
				
				if(caixa == null)
					throw new RuntimeException("não foi possível atualizar o saldo do caixa. Caixa null");
				
				Query query = manager.createNativeQuery("update saldoCaixa set saldo = saldo + ? WHERE idCaixa = ? and mes = ? and ano = ?");
				query.setParameter(0x01, valor);
				query.setParameter(0x02, caixa.getId());
				query.setParameter(0x03, caixa.getMesAberto());
				query.setParameter(0x04, caixa.getAnoAberto());
				
				query.executeUpdate();
			}
			
			if(managerIndependente)
				manager.getTransaction().commit();
			
			return true;	
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
}