package br.com.desenv.nepalign.persistence;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import br.com.desenv.frameworkignorante.DsvException;
import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.BoletimUnificado;
import br.com.desenv.nepalign.util.DsvConstante;

public class BoletimUnificadoPersistence extends GenericPersistenceIGN<BoletimUnificado>
{
	
	public StringBuffer gerarRelatorioBoletimUnificado(String empresas, Date dataMovimento) throws Exception
	{
		try 
		{  
			DateFormat formatadorDataSql = new SimpleDateFormat("yyyy-MM-dd");
			String dataSql = formatadorDataSql.format(dataMovimento);
			Integer idSituacaoPedidoVendaCancelado = DsvConstante.getParametrosSistema().getIdSituacaoPedidoVendaCancelado();
						
	    	StringBuffer str = new StringBuffer();
	        
		    //VENDA GERAL DO DIA
           	str.append(" (SELECT  empresa.idEmpresaFisica as empresa, 'VENDA GERAL DO DIA' as descricao, ifnull(sum(pedido.valorTotalPedido),0) as valorOriginal,  ifnull(sum(pedido.valorTotalPedido),0) as debito, 0 as credito  ");
        	str.append(" FROM pedidovenda pedido ");
        	str.append(" INNER JOIN PlanoPagamento plano on plano.idPlanoPagamento = pedido.IdPlanoPagamento ");
        	str.append(" LEFT OUTER JOIN tipopagamento tipoPagamento ON tipoPagamento.idTipoPagamento = plano.idTipoPagamento ");
        	str.append(" INNER JOIN empresaFisica empresa on empresa.idEmpresaFisica = pedido.IdEmpresaFisica ");
        	str.append(" WHERE empresa.idEmpresaFisica in (" + empresas + ") and pedido.dataVenda between '"+dataSql+" 00:00:00' AND '"+dataSql+" 23:59:59'");
        	str.append(" and pedido.idSituacaoPedidoVenda <> "+ idSituacaoPedidoVendaCancelado + " group by empresa.idEmpresaFisica) ");
        	
        	
        	str.append(" union ");
        	str.append(" (select idEmpresaFisica as empresa,'TROCO DO DIA ANTERIOR' as descricao, ");
        	str.append(" sum(valorInicial) AS valorOriginal, ");
        	str.append(" sum(valorInicial) AS DEBITO, ");
        	str.append(" 0 as Credito from caixadia where dataAbertura between '"+dataSql+ " 00:00:00' and '"+dataSql+" 23:59:59' and idempresafisica in("+empresas+")");
        	str.append(" group by caixadia.idEmpresaFisica) ");
        	
        	//SAIDA DAS VENDAS CHEQUE PRE, CARTAO, VALE, OUTROS CARTOES
        	       	
        	str.append(" UNION ");
        	str.append("(SELECT pedido.idEmpresaFisica as empresa, CONCAT('VENDA ',tipo.descricao) as descricao ,sum(pedido.valortotalpedido) as valorOriginal, 0 as DEBITO, sum(pedido.valortotalpedido) as CREDITO ");
        	str.append("FROM pedidovenda pedido ");  
        	str.append("INNER JOIN planopagamento plano on plano.idPlanoPagamento = pedido.IdPlanoPagamento ");  
        	str.append("INNER JOIN tipopagamento tipo on tipo.idTipoPagamento = plano.idTipoPagamento ");
        	str.append("INNER JOIN empresafisica empresa on empresa.idEmpresaFisica = pedido.idEmpresaFisica "); 
        	str.append("where pedido.idSituacaoPedidoVenda <> "+idSituacaoPedidoVendaCancelado+ " and pedido.idEmpresaFisica in( "+empresas+") "); 
        	str.append("and pedido.dataVenda between '"+dataSql+" 00:00:00' AND '"+dataSql+" 23:59:59' ");
        	str.append("and plano.idTipoPagamento not in (1,4) ");
        	str.append("group by plano.idTipoPagamento )");
        	
        	//SAIDA DAS VENDAS A CREDITO
        	str.append(" union  (");
        	str.append(" SELECT  pedido.idEmpresaFisica as empresa, 'VENDA A CREDITO' as descricao,");  
        	str.append(" SUM(IFNULL(pedido.valorTotalPedido, 0)) as valorOriginal,");   
        	str.append(" 0 AS DEBITO," );
        	str.append(" SUM(IFNULL(pedido.valorTotalPedido, 0)) as credito ");  
        	str.append(" FROM pedidovenda pedido");  
        	str.append(" INNER JOIN PlanoPagamento plano on plano.idPlanoPagamento = pedido.IdPlanoPagamento");  
        	str.append(" LEFT OUTER JOIN tipopagamento tipoPagamento ON tipoPagamento.idTipoPagamento = plano.idTipoPagamento"); 
        	str.append(" INNER JOIN empresaFisica empresa on empresa.idEmpresaFisica = pedido.idEmpresaFisica");
        	str.append(" WHERE pedido.idEmpresaFisica in(" + empresas + ") " );
        	str.append(" and pedido.dataVenda between '"+dataSql+" 00:00:00' AND '"+dataSql+" 23:59:59'");
        	str.append(" and (pedido.idSituacaoPedidoVenda <> 5) and plano.idTipoPagamento = (4) ");
        	str.append(" group by pedido.idEmpresaFisica,plano.idTipoPagamento)");
         	
        	// ENTRADA CREDIARIO DEBITO
        	str.append(" union ");
        	str.append(" (SELECT duplicatapaga.idEmpresaFisica as empresa, 'ENTRADA CREDIARIO ' as descricao, ");
        	str.append("  SUM(COALESCE(mov.valor,0)) as valorOriginal, ");
        	str.append("  SUM(COALESCE(mov.valor,0)) as debito, 0 as credito ");
        	str.append(" from MovimentoCaixaDia mov inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ");
        	str.append(" left outer join cliente on cliente.idCliente = mov.idcliente ");
        	str.append(" left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ");
        	str.append(" left outer join pagamentopedidovenda on pagamentopedidovenda.idPagamentoPedidoVenda = mov.idPagamentoPedidoVenda ");
        	str.append(" left outer join tipoconta on tipoconta.idTipoConta = tipomovimento.idTipoConta ");
        	str.append(" left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ");
        	str.append(" left outer join duplicataparcela as parcelaCriada on parcelaCriada.idDuplicataParcela = mov.idDuplicataParcela ");
        	str.append(" left outer join duplicata as duplicataCriada on duplicataCriada.idDuplicata = parcelaCriada.idDuplicata ");
        	str.append(" left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata ");
        	str.append(" left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela ");
        	str.append(" left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicataPaga.idEmpresaFisica = caixa.idEmpresaFisica ");
        	str.append(" where mov.dataVencimento between '"+dataSql+" 00:00:00' AND '"+dataSql+" 23:59:59' ");
        	str.append(" and (parcelaPaga.numeroParcela = 0) and duplicataPaga.idEmpresaFisica in ("+empresas+")");
        	str.append(" GROUP BY duplicataPaga.idEmpresaFisica, tipomovimento.origem order by duplicataPaga.idEmpresaFisica,tipomovimento.origem) ");
        	
        	// ENTRADA CREDIARIO CREDITO
        	str.append(" union ");
        	str.append(" (SELECT caixa.idEmpresaFisica as empresa, CONCAT('ENT. CREDIARIO ',forma.descricao) as descricao, ");
        	str.append("  SUM(COALESCE(mov.valor,0)) as valorOriginal, ");
        	str.append("  0 as debito, SUM(COALESCE(mov.valor,0)) as credito ");
        	str.append(" from MovimentoCaixaDia mov inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ");
        	str.append(" left outer join cliente on cliente.idCliente = mov.idcliente ");
        	str.append(" left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ");
        	str.append(" left outer join pagamentopedidovenda on pagamentopedidovenda.idPagamentoPedidoVenda = mov.idPagamentoPedidoVenda ");
        	str.append(" left outer join tipoconta on tipoconta.idTipoConta = tipomovimento.idTipoConta ");
        	str.append(" left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ");
        	str.append(" left outer join duplicataparcela as parcelaCriada on parcelaCriada.idDuplicataParcela = mov.idDuplicataParcela ");
        	str.append(" left outer join duplicata as duplicataCriada on duplicataCriada.idDuplicata = parcelaCriada.idDuplicata ");
        	str.append(" left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata ");
        	str.append(" left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela ");
        	str.append(" left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicataPaga.idEmpresaFisica in ("+empresas+")");
        	str.append(" where mov.dataVencimento between '"+dataSql+" 00:00:00' AND '"+dataSql+" 23:59:59' ");
        	str.append(" and (parcelaPaga.numeroParcela = 0) AND forma.tipo<>1"); 
        	str.append(" GROUP BY caixa.idEmpresaFisica, tipomovimento.origem, forma.descricao order by caixa.idEmpresaFisica, tipomovimento.origem, forma.descricao) ");
        	
        	// ENTRADA VENDA DEBITO
        	str.append("union (");
        	str.append(" select pedido.idEmpresaFisica as empresa, CONCAT('ENTRADA ',tipoPagamento.descricao) as descricao, ");
        	str.append(" SUM(IFNULL(pag.valor, 0)) as valorOriginal, SUM(IFNULL(pag.valor, 0)) AS DEBITO, ");
        	str.append(" 0 as credito ");
        	str.append(" FROM pedidovenda pedido ");
        	str.append(" INNER JOIN PlanoPagamento plano on plano.idPlanoPagamento = pedido.IdPlanoPagamento ");
        	str.append(" INNER JOIN pagamentopedidovenda pag on pag.idPedidoVenda = pedido.idPedidoVenda "); 
        	str.append(" INNER JOIN formapagamento forma on forma.idFormaPagamento = pag.idFormaPagamento ");
        	str.append(" INNER JOIN planopagamento planoPagamento on planoPagamento.idPlanoPagamento = pedido.IdPlanoPagamento");
        	str.append(" INNER JOIN tipoPagamento tipoPagamento on tipoPagamento.idTipoPagamento = plano.idTipoPagamento");
        	str.append(" INNER JOIN empresaFisica empresa on empresa.idEmpresaFisica = pedido.idEmpresaFisica");
        	str.append(" WHERE pedido.idEmpresaFisica in(" + empresas + ") ");
        	str.append(" and pedido.dataVenda between '"+dataSql+" 00:00:00' AND '"+dataSql+" 23:59:59' ");
        	str.append(" and pedido.idSituacaoPedidoVenda <> 5 and pag.entrada = 'S' and tipoPagamento.idtipopagamento <>1 group by empresa.idEmpresaFisica, tipoPagamento.descricao) ");
        	
        	// ENTRADA VENDA CREDITO
        	str.append("union (");
        	str.append(" select pedido.idEmpresaFisica as empresa, CONCAT('ENTRADA ',tipoPagamento.descricao,' ',forma.descricao) as descricao, "); 
        	str.append(" SUM(IFNULL(pag.valor, 0)) as valorOriginal, 0 AS DEBITO, ");
        	str.append(" SUM(IFNULL(pag.valor, 0)) as credito ");
        	str.append(" FROM pedidovenda pedido ");
        	str.append(" INNER JOIN PlanoPagamento plano on plano.idPlanoPagamento = pedido.IdPlanoPagamento ");
        	str.append(" INNER JOIN pagamentopedidovenda pag on pag.idPedidoVenda = pedido.idPedidoVenda "); 
        	str.append(" INNER JOIN formapagamento forma on forma.idFormaPagamento = pag.idFormaPagamento ");
        	str.append(" INNER JOIN planopagamento planoPagamento on planoPagamento.idPlanoPagamento = pedido.IdPlanoPagamento");
        	str.append(" INNER JOIN tipoPagamento tipoPagamento on tipoPagamento.idTipoPagamento = plano.idTipoPagamento");
        	str.append(" WHERE pedido.idEmpresaFisica in(" + empresas + ") ");
        	str.append(" and pedido.dataVenda between '"+dataSql+" 00:00:00' AND '"+dataSql+" 23:59:59' ");
        	str.append(" and pedido.idSituacaoPedidoVenda <> 5 and pag.entrada = 'S' and forma.tipo<>1 group by pedido.idEmpresaFisica, plano.descricao) ");
        	
        	str.append(" union ");
        	str.append(" (SELECT duplicataPaga.idEmpresaFisica as empresa, 'RECEB. DE CREDIARIO' as descricao, "); 
        	str.append(" SUM(COALESCE(movimentoparcela.valorRecebido,0)) as valorOriginal, ");
        	str.append(" ROUND(SUM(IFNULL(movimentoparcela.valorRecebido, 0)-IFNULL(if(movimentoparcela.tipopagamento=2,0,movimentoparcela.valorEntradaJuros),0)+IFNULL(if(movimentoparcela.tipopagamento=2,0,movimentoparcela.valorDesconto),0)),2) as debito, 0 as credito ");
        	str.append(" from MovimentoCaixaDia mov inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ");
        	str.append(" left outer join cliente on cliente.idCliente = mov.idcliente "); 
        	str.append(" left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ");
        	str.append(" left outer join pagamentopedidovenda on pagamentopedidovenda.idPagamentoPedidoVenda = mov.idPagamentoPedidoVenda ");
        	str.append(" left outer join tipoconta on tipoconta.idTipoConta = tipomovimento.idTipoConta ");
        	str.append(" left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ");
        	str.append(" left outer join duplicataparcela as parcelaCriada on parcelaCriada.idDuplicataParcela = mov.idDuplicataParcela ");
        	str.append(" left outer join duplicata as duplicataCriada on duplicataCriada.idDuplicata = parcelaCriada.idDuplicata ");
        	str.append(" left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata ");
        	str.append(" left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela ");
        	str.append(" left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicataPaga.idEmpresaFisica=caixa.idEmpresaFisica ");
        	str.append(" where mov.dataVencimento between '"+dataSql+" 00:00:00' AND '"+dataSql+" 23:59:59' ");
        	str.append(" and tipoMovimento.origem = 2 and parcelapaga.numeroparcela<>0 and parcelaPaga.situacao <> '3' and duplicatapaga.idempresafisica in(" + empresas + ") " );
        	str.append(" GROUP BY duplicataPaga.idEmpresaFisica, tipomovimento.origem) ");
        	
        	str.append(" union ");
        	str.append(" (SELECT duplicatapaga.idEmpresaFisica as empresa, CONCAT('RECEB. DE CREDIARIO ',forma.descricao) as descricao, ");
        	str.append(" ROUND(SUM(COALESCE(movimentoparcela.valorRecebido,0)),2) as valorOriginal, ");
        	str.append(" 0 as debito, ");
        	str.append(" ROUND(SUM(COALESCE(movimentoparcela.valorRecebido,0)),2) as credito ");
        	str.append(" from MovimentoCaixaDia mov inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ");
        	str.append(" left outer join cliente on cliente.idCliente = mov.idcliente ");
        	str.append(" left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ");
        	str.append(" left outer join pagamentopedidovenda on pagamentopedidovenda.idPagamentoPedidoVenda = mov.idPagamentoPedidoVenda ");
        	str.append(" left outer join tipoconta on tipoconta.idTipoConta = tipomovimento.idTipoConta ");
        	str.append(" left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ");
        	str.append(" left outer join duplicataparcela as parcelaCriada on parcelaCriada.idDuplicataParcela = mov.idDuplicataParcela ");
        	str.append(" left outer join duplicata as duplicataCriada on duplicataCriada.idDuplicata = parcelaCriada.idDuplicata ");
        	str.append(" left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata ");
        	str.append(" left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela ");
        	str.append(" left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicataPaga.idEmpresaFisica = caixa.idEmpresaFisica ");
        	str.append(" where mov.dataVencimento between '"+dataSql+" 00:00:00' AND '"+dataSql+" 23:59:59' ");
        	str.append(" and forma.tipo<>1 and parcelaPaga.numeroParcela <>0 and COALESCE(parcelaPaga.valorParcela,0) >0 and duplicatapaga.idempresafisica in(" + empresas + ") ");
        	str.append(" GROUP BY forma.descricao) ");
        	 
        	str.append(" union ");   
        	str.append(" (select duplicatapaga.idEmpresaFisica as empresa, concat('RECEB. DE JUROS DE CREDIARIO') as descricao, ");
        	str.append(" COALESCE(sum(movimentoparcela.valorEntradaJuros),0) as valorOriginal, ");
        	str.append(" COALESCE(ROUND(SUM(if(movimentoparcela.tipopagamento=2,0,movimentoparcela.valorEntradaJuros)),2),0) - COALESCE(ROUND(SUM(if(movimentoparcela.tipopagamento=2,0,movimentoparcela.valorDesconto)),2),0) as debito, ");
        	str.append(" 0 as credito ");
        	str.append(" from MovimentoCaixaDia mov inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ");
        	str.append(" left outer join cliente on cliente.idCliente = mov.idcliente ");
        	str.append(" left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ");
        	str.append(" left outer join pagamentopedidovenda on pagamentopedidovenda.idPagamentoPedidoVenda = mov.idPagamentoPedidoVenda ");
        	str.append(" left outer join tipoconta on tipoconta.idTipoConta = tipomovimento.idTipoConta ");
        	str.append(" left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ");
        	str.append(" left outer join duplicataparcela as parcelaCriada on parcelaCriada.idDuplicataParcela = mov.idDuplicataParcela ");
        	str.append(" left outer join duplicata as duplicataCriada on duplicataCriada.idDuplicata = parcelaCriada.idDuplicata ");
        	str.append(" left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata ");
        	str.append(" left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela "); 
        	str.append(" left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicataPaga.idEmpresaFisica = caixa.idEmpresaFisica ");
        	str.append(" where mov.dataVencimento between '"+dataSql+" 00:00:00' AND '"+dataSql+" 23:59:59' ");
        	str.append(" and tipomovimento.origem=2 group by duplicataPaga.idEmpresaFisica order by duplicataPaga.idEmpresaFisica, tipomovimento.origem, forma.descricao, dataPagamento) ");
        	
        	str.append(" union ");
        	str.append(" (SELECT duplicatapaga.idEmpresaFisica as empresa, CONCAT(tipomovimento.descricao) as descricao, ");
        	str.append(" SUM(COALESCE(mov.valor,0)) as valorOriginal, SUM(IF (mov.CreditoDebito ='C',mov.valor,0)) as debito, ");
           	str.append(" SUM(IF (mov.CreditoDebito ='D',mov.valor,0)) as credito from MovimentoCaixaDia mov ");
        	str.append(" inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ");
        	str.append(" left outer join cliente on cliente.idCliente = mov.idcliente ");
        	str.append(" left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ");
        	str.append(" left outer join pagamentopedidovenda on pagamentopedidovenda.idPagamentoPedidoVenda = mov.idPagamentoPedidoVenda ");
        	str.append(" left outer join tipoconta on tipoconta.idTipoConta = tipomovimento.idTipoConta ");
        	str.append(" left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ");
        	str.append(" left outer join duplicataparcela as parcelaCriada on parcelaCriada.idDuplicataParcela = mov.idDuplicataParcela ");
        	str.append(" left outer join duplicata as duplicataCriada on duplicataCriada.idDuplicata = parcelaCriada.idDuplicata ");
        	str.append(" left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata ");
        	str.append(" left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela ");
        	str.append(" left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicataPaga.idEmpresaFisica = caixa.idEmpresaFisica ");
        	str.append(" where mov.dataVencimento between '"+dataSql+" 00:00:00' AND '"+dataSql+" 23:59:59' ");
        	str.append(" and diversos='S' and (mov.mostrarBoletim <> 'N' or mov.mostrarBoletim is null) ");
        	str.append(" GROUP BY duplicataPaga.idEmpresaFisica, tipomovimento.descricao order by tipomovimento.descricao) ");
        	
           	str.append(" union ");
        	str.append(" (SELECT duplicatapaga.idEmpresaFisica as empresa, CONCAT(tipomovimento.descricao,' ',forma.descricao) as descricao, ");
        	str.append(" SUM(COALESCE(mov.valor,0)) as valorOriginal, 0 as debito, ");
           	str.append(" SUM(IF (mov.CreditoDebito ='C',mov.valor,0)) as credito from MovimentoCaixaDia mov ");
        	str.append(" inner join CaixaDia caixa on caixa.idCaixaDia = mov.idCaixaDia ");
        	str.append(" left outer join cliente on cliente.idCliente = mov.idcliente ");
        	str.append(" left outer join tipomovimentocaixadia tipomovimento on tipomovimento.idTipoMovimentoCaixaDia = mov.idTipoMovimentoCaixaDia ");
        	str.append(" left outer join pagamentopedidovenda on pagamentopedidovenda.idPagamentoPedidoVenda = mov.idPagamentoPedidoVenda ");
        	str.append(" left outer join tipoconta on tipoconta.idTipoConta = tipomovimento.idTipoConta ");
        	str.append(" left outer join formapagamento forma on forma.idFormaPagamento = mov.idFormaPagamento ");
        	str.append(" left outer join duplicataparcela as parcelaCriada on parcelaCriada.idDuplicataParcela = mov.idDuplicataParcela ");
        	str.append(" left outer join duplicata as duplicataCriada on duplicataCriada.idDuplicata = parcelaCriada.idDuplicata ");
        	str.append(" left outer join movimentoparceladuplicata as movimentoparcela on movimentoparcela.idMovimentoParcelaDuplicata = mov.idMovimentoParcelaDuplicata ");
        	str.append(" left outer join duplicataparcela as parcelaPaga on parcelaPaga.idDuplicataParcela = movimentoparcela.idDuplicataParcela ");
        	str.append(" left outer join duplicata as duplicatapaga on duplicatapaga.idDuplicata = parcelapaga.idDuplicata and duplicataPaga.idEmpresaFisica = caixa.idEmpresaFisica ");
        	str.append(" where mov.dataVencimento between '"+dataSql+" 00:00:00' AND '"+dataSql+" 23:59:59' ");
        	str.append(" and diversos='S' and (mov.mostrarBoletim <> 'N' or mov.mostrarBoletim is null) and forma.tipo<>1 ");
        	str.append(" GROUP BY duplicataPaga.idEmpresaFisica, tipomovimento.descricao order by duplicataPaga.idEmpresaFisica, tipomovimento.descricao) ");
      
        	/*
        	str.append(" union (");
        	str.append(" SELECT pedido.idEmpresaFisica as empresa, 'VENDA A VISTA' as descricao, ");
        	str.append(" IFNULL(sum(pag.valor),0) as valorOriginal, 0 as debito, 0 as credito ");
        	str.append(" FROM pedidovenda pedido ");
        	str.append(" INNER JOIN PlanoPagamento plano on plano.idPlanoPagamento = pedido.IdPlanoPagamento ");
        	str.append(" LEFT OUTER JOIN tipopagamento tipoPagamento ON tipoPagamento.idTipoPagamento = plano.idTipoPagamento ");
        	str.append(" INNER JOIN pagamentopedidovenda pag on pag.idPedidoVenda = pedido.idPedidoVenda ");
        	str.append(" WHERE pedido.idEmpresaFisica in( "+empresas+") and pedido.dataVenda between '"+dataSql+" 00:00:00' AND '"+dataSql+" 23:59:59'");
        	str.append(" and tipoPagamento.idTipoPagamento = 1");
        	str.append(" and (pedido.idSituacaoPedidoVenda <> "+ idSituacaoPedidoVendaCancelado + "))");
        	*/
        	System.out.println(str);
        	 
        	return str;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new DsvException(e);  
		}  
	}
	
	public void deleteAllBoletimUnificado() throws Exception {
		executeUpdate("delete from boletimunificado where origem = 'G'");
	}


	
}


