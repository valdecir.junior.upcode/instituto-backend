package br.com.desenv.nepalign.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.PedidoVenda;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.service.CidadeService;
import br.com.desenv.nepalign.service.ClienteService;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.FormaEntregaService;
import br.com.desenv.nepalign.service.MetodoPagamentoService;
import br.com.desenv.nepalign.service.PaisService;
import br.com.desenv.nepalign.service.PedidoVendaService;
import br.com.desenv.nepalign.service.PlanoPagamentoService;
import br.com.desenv.nepalign.service.SituacaoPedidoVendaService;
import br.com.desenv.nepalign.service.StatusPedidoService;
import br.com.desenv.nepalign.service.TipoLogradouroService;
import br.com.desenv.nepalign.service.TipoMovimentacaoEstoqueService;
import br.com.desenv.nepalign.service.UfService;
import br.com.desenv.nepalign.service.UsuarioService;
import br.com.desenv.nepalign.service.VendedorService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class PedidoVendaPersistence extends GenericPersistenceIGN<PedidoVenda>
{
 public PedidoVenda buscarPorNumeroControle(String numero) throws Exception
 {
	 
	 String sql = "";
	 sql = sql + " SELECT idPedidoVenda FROM pedidovenda where numeroControle = "+numero+" order by idPedidoVenda desc";
	 Connection conn; 
     conn = ConexaoUtil.getConexaoPadrao();
	 java.sql.Statement rec = conn.createStatement();    
     ResultSet rs = rec.executeQuery(sql.toString());
     PedidoVenda pedido = new PedidoVenda();
    while(rs.next())
    {
     pedido = recuperarPorId(rs.getInt("idPedidoVenda"));
    }
    return pedido;
 }
 public List<PedidoVenda> listarPendendetes(Boolean crediario,EmpresaFisica empresa,Date data) throws SQLException, Exception
 {
	 SimpleDateFormat formatador = new SimpleDateFormat("yyyy-MM-dd");
	 String dataSql = "";
	 if(data !=null)
	 {
		 dataSql = formatador.format(data);
	 }
	 int x =0;
	 String str = "";
	 str = str + " SELECT pv.idPedidoVenda as 'id' "; 
	 str = str + "  FROM pedidovenda pv "; 
	 str = str + " inner join planopagamento pp on pp.idPlanoPagamento = pv.IdPlanoPagamento "; 
	 if(crediario)
	 {
		 if(x==0)
		 {
			 str = str + " where pp.crediario = 'S' ";
			 x++;
		 }
		 else
		 {
			 str = str + " and pp.crediario = 'S' "; 
		 }
	 }
	 else
	 {
		 if(x==0)
		 {
			 str = str + " where pp.crediario = 'N' ";
			 x++;
		 }
		 else
		 {
			 str = str + " and pp.crediario = 'N' "; 
		 } 
	 }
	 if(!dataSql.equals(""))
	 {
		 if(x==0)
		 {
			 str = str + " where pv.dataVenda >= '"+dataSql+" 00:00:00' "; 
			 str = str + " and pv.dataVenda <= '"+dataSql+" 23:59:59' "; 
			 x++;
		 }
		 else
		 {
			 str = str + " and pv.dataVenda >= '"+dataSql+" 00:00:00' "; 
			 str = str + " and pv.dataVenda <= '"+dataSql+" 23:59:59' ";  
		 }
		 
	 }
	 if(empresa!=null)
	 {
		 if(x==0)
		 {
			 str = str + " where pv.idEmpresaFisica =  "+empresa.getId();
			 x++;
		 }
		 else
		 {
			 str = str + " and pv.idEmpresaFisica =  "+empresa.getId(); 
		 }
		
	 }
	 str = str + " and pv.idSituacaoPedidoVenda = 2 "; 
	 str = str + " and pv.valorTotalPedido > 0";
	 List<Integer> listaIds = new ArrayList<>();
	 List<PedidoVenda> lista = new ArrayList<>();
	 Connection conn; 
     
     conn = ConexaoUtil.getConexaoPadrao();
	 java.sql.Statement rec = conn.createStatement();    
     ResultSet rs = rec.executeQuery(str.toString());
    
    while(rs.next())
    {
     listaIds.add(rs.getInt("id"));
    }
    for (int i = 0; i < listaIds.size(); i++) 
    {
	lista.add(new PedidoVendaService().recuperarPorId(listaIds.get(i)));	
	}
	return lista;
	 
 }
	public List<PedidoVenda> listarPorObjetoFiltroProdutoSql(EntityManager manager, Produto produto, EmpresaFisica empresaFisica, Date dataBase) throws Exception
	{
		Boolean managerIndependente = false;
		List<PedidoVenda> listaPedidoVenda = new ArrayList<PedidoVenda>();
		Connection connection = null;
		try
		{
			if(manager == null)
				manager = ConexaoUtil.getEntityManager();
			
			connection = ConexaoUtil.getConexaoPadrao();
			
			
			StringBuilder query = new StringBuilder();
			
			query.append("SELECT * FROM pedidoVenda as pedidoVenda ");
			query.append("INNER JOIN itemPedidoVenda item ON item.idPedidoVenda = pedidoVenda.idPedidoVenda ");
			query.append("INNER JOIN produto prod on item.idProduto = prod.idProduto ");
			query.append("WHERE prod.idProduto = " + produto.getId() + " ");
			query.append("and pedidoVenda.dataVenda >= '"+ new SimpleDateFormat("yyyy-MM-dd").format(dataBase)+" 00:00:00'");
			
			if(empresaFisica != null)
				query.append("and pedidoVenda.idEmpresaFisica = " + empresaFisica.getId());
			
			ResultSet rs = connection.createStatement().executeQuery(String.valueOf(query));

			while(rs.next())
			{
				PedidoVenda pedidoVendaBuffer = new PedidoVenda();
				pedidoVendaBuffer.setId(rs.getInt("idPedidoVenda"));
				pedidoVendaBuffer.setTipoMovimentacaoEstoque(new TipoMovimentacaoEstoqueService().recuperarPorId((Integer)rs.getInt("idTipoMovimentacaoEstoque")));
				pedidoVendaBuffer.setEmpresaFisica(new EmpresaFisicaService().recuperarPorId((Integer)rs.getInt("idEmpresaFisica")));
				pedidoVendaBuffer.setVendedor(new VendedorService().recuperarPorId((Integer)rs.getInt("idVendedor")));
				pedidoVendaBuffer.setCliente(new ClienteService().recuperarPorId((Integer)rs.getInt("idCliente")));
				pedidoVendaBuffer.setUsuario(new UsuarioService().recuperarPorId((Integer) rs.getInt("idUsuario")));
				pedidoVendaBuffer.setSituacaoPedidoVenda(new SituacaoPedidoVendaService().recuperarPorId((Integer) rs.getInt("idSituacaoPedidoVenda")));
				pedidoVendaBuffer.setPlanoPagamento(new PlanoPagamentoService().recuperarPorId((Integer)rs.getInt("idPlanoPagamento")));
				pedidoVendaBuffer.setMetodoPagamento(new MetodoPagamentoService().recuperarPorId((Integer) rs.getInt("idMetodoPagamento")));
				pedidoVendaBuffer.setCidadeEntrega(new CidadeService().recuperarPorId((Integer)rs.getInt("idCidadeEntrega")));
				pedidoVendaBuffer.setUfEntrega(new UfService().recuperarPorId((Integer) rs.getInt("idUfEntrega")));
				pedidoVendaBuffer.setPaisEntrega(new PaisService().recuperarPorId((Integer)rs.getInt("idPaisEntrega")));
				pedidoVendaBuffer.setFormaEntrega(new FormaEntregaService().recuperarPorId((Integer)rs.getInt("idFormaEntrega")));
				pedidoVendaBuffer.setStatusPedido(new StatusPedidoService().recuperarPorId((Integer)rs.getInt("idStatusPedido")));
				pedidoVendaBuffer.setTipoLogradouroEnderecoEntrega(new TipoLogradouroService().recuperarPorId((Integer)rs.getInt("idTipoLogradouroEnderecoEntrega")));
				pedidoVendaBuffer.setDataVenda(rs.getDate("dataVenda"));
				pedidoVendaBuffer.setDescontoTotal(rs.getDouble("descontoTotal"));
				pedidoVendaBuffer.setNumeroControle(rs.getString("numeroControle"));
				pedidoVendaBuffer.setValorTotalPedido(rs.getDouble("valorTotalPedido"));
				pedidoVendaBuffer.setCodigoPedidoEnviadoCliente(rs.getString("codigoPedidoEnviadoCliente"));
				pedidoVendaBuffer.setValorTotalProdutosPedidos(rs.getDouble("valorTotalProdutosPedidos"));
				pedidoVendaBuffer.setValorTotalCustoFrete(rs.getDouble("valorTotalCustoFrete"));
				pedidoVendaBuffer.setValorTotalDescProdutos(rs.getDouble("valorTotalDescProdutos"));
				pedidoVendaBuffer.setValorTotalDescFrete(rs.getDouble("valorTotalDescFrete"));
				pedidoVendaBuffer.setNumParcelas(rs.getInt("numParcelas"));
				pedidoVendaBuffer.setValorParcela(rs.getDouble("valorParcela"));
				pedidoVendaBuffer.setTotalDescPedido(rs.getDouble("totalDescPedido"));
				pedidoVendaBuffer.setDataExpiracaoPedido(rs.getDate("dataExpiracaoPedido"));
				pedidoVendaBuffer.setLogradouroEnderecoEntrega(rs.getString("logradouroEnderecoEntrega"));
				pedidoVendaBuffer.setBairroEnderecoEntrega(rs.getString("bairroEnderecoEntrega"));
				pedidoVendaBuffer.setNumEnderecoEntrega(rs.getString("numEnderecoEntrega"));
				pedidoVendaBuffer.setComplementoEnderecoEntrega(rs.getString("complementoEnderecoEntrega"));
				pedidoVendaBuffer.setCepEnderecoEntrega(rs.getString("cepEnderecoEntrega"));
				pedidoVendaBuffer.setNumTelefone(rs.getString("numTelefone"));
				pedidoVendaBuffer.setDddTelefone(rs.getString("dddTelefone"));
				pedidoVendaBuffer.setDataEntregaPedido(rs.getDate("dataEntregaPedido"));
				pedidoVendaBuffer.setValorEmbalagemPresente(rs.getDouble("valorEmbalagemPresente"));
				pedidoVendaBuffer.setValorJuros(rs.getDouble("valorJuros"));
				pedidoVendaBuffer.setValorCupomDesconto(rs.getDouble("valorCupomDesconto"));
				pedidoVendaBuffer.setNumCupomDesconto(rs.getInt("numCupomDesconto"));
				pedidoVendaBuffer.setReferenciaEnderecoEntrega(rs.getString("referenciaEnderecoEntrega"));
				pedidoVendaBuffer.setObservacao(rs.getString("observacao"));
				pedidoVendaBuffer.setUsuarioAutorizador(new UsuarioService().recuperarPorId((Integer)rs.getInt("idUsuarioAutorizador")));
				pedidoVendaBuffer.setCodigoExterno(rs.getInt("codigoExterno"));
				pedidoVendaBuffer.setDestinatarioPedido(rs.getString("destinatarioPedido"));
			
				listaPedidoVenda.add(pedidoVendaBuffer);
			}
			
			return listaPedidoVenda;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
			connection.close();
			listaPedidoVenda = null;
			System.gc();
		}
	}
		
}


