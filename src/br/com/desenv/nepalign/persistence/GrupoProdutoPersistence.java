package br.com.desenv.nepalign.persistence;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.desenv.frameworkignorante.Formatador;
import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.frameworkignorante.Page;
import br.com.desenv.nepalign.model.GrupoProduto;

public class GrupoProdutoPersistence extends GenericPersistenceIGN<GrupoProduto>
{
	public Page<GrupoProduto> listarPaginadoPorObjetoFiltroData(int tamanhoPagina, int numeroPagina, GrupoProduto entidadeFiltro) throws Exception 
	{
		SimpleDateFormat simpleDateFormatter = new SimpleDateFormat("dd/MM/yyyy");
		
		Calendar calendarDataPesquisa = Calendar.getInstance();
		calendarDataPesquisa.setTime(simpleDateFormatter.parse(simpleDateFormatter.format(entidadeFiltro.getUltimaAlteracao())));
		
		entidadeFiltro.setUltimaAlteracao(null);
		
		List<GrupoProduto> listaGrupoProduto = this.listarPorObjetoFiltro(entidadeFiltro);
		List<GrupoProduto> listaGrupoProdutoFinal = new ArrayList<GrupoProduto>();
		
		for(GrupoProduto grupoProduto : listaGrupoProduto)
		{
			Calendar calendarGrupoProduto = Calendar.getInstance();
			calendarGrupoProduto.setTime(simpleDateFormatter.parse(simpleDateFormatter.format(grupoProduto.getUltimaAlteracao())));
			
			if(calendarGrupoProduto.getTimeInMillis() == calendarDataPesquisa.getTimeInMillis())
				listaGrupoProdutoFinal.add(grupoProduto);
		}
		
		return Formatador.gerarPagina(listaGrupoProdutoFinal, numeroPagina, tamanhoPagina);
	}
}


