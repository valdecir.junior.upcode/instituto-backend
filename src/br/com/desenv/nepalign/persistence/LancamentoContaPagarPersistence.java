package br.com.desenv.nepalign.persistence;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.LancamentoContaPagar;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class LancamentoContaPagarPersistence extends GenericPersistenceIGN<LancamentoContaPagar>
{
	public LancamentoContaPagar recuperarPorNumeroDocumento(EntityManager manager, String numeroDocumento) throws Exception
	{
		Boolean managerIndependente = false;
		
		try
		{
			if(manager == null)
			{
				manager = ConexaoUtil.getEntityManager();
				managerIndependente = true;
			}
			
			Query query = manager.createQuery("from LancamentoContaPagar where numeroDocumento like '%" + numeroDocumento + "%'");
			
			return (LancamentoContaPagar) query.getSingleResult();
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
}