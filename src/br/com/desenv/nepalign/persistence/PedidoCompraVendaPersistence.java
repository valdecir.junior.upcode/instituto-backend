package br.com.desenv.nepalign.persistence;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.frameworkignorante.Page;
import br.com.desenv.frameworkignorante.SearchParameter;
import br.com.desenv.frameworkignorante.UtilAnotationIGN;
import br.com.desenv.nepalign.model.ItemPedidoCompraVenda;
import br.com.desenv.nepalign.model.Marca;
import br.com.desenv.nepalign.model.OrdemProduto;
import br.com.desenv.nepalign.model.PedidoCompraVenda;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class PedidoCompraVendaPersistence extends GenericPersistenceIGN<PedidoCompraVenda>
{
	public Integer buscarMaiorNumeroPedido() throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		StringBuffer sql = new StringBuffer("select max(numeroPedido) from PedidoCompraVenda ");
		Query query = manager.createQuery(sql.toString());

		Integer maiorNumeroPedido = (Integer) query.getSingleResult();
		
		if (maiorNumeroPedido == null)
			maiorNumeroPedido = 1;
		else
			maiorNumeroPedido++;
		
		manager.close();

		return maiorNumeroPedido;
	}

	public Page<PedidoCompraVenda> listarPaginadoPorObjetoFiltroOrdemProdutoMarca(PedidoCompraVenda pedidoCompraVenda, OrdemProduto ordemProduto, Marca marca, int maxResult, int pageNumber, String whereAdicional) throws Exception
	{
		final Page<PedidoCompraVenda> pagePedidoCompraVendaResultado = new Page<PedidoCompraVenda>();
		pagePedidoCompraVendaResultado.setRecordList(new ArrayList<PedidoCompraVenda>());
		final Map<String, Object> listaParametro = new HashMap<String, Object>();
		List<String> listaCriterio = new ArrayList<String>();
		
		for (Field campoPersistente : UtilAnotationIGN.recuperarListaAtributoPersistente(pedidoCompraVenda))
		{
			SearchParameter parametroPesquisa = UtilAnotationIGN.criarParametroPesquisa(campoPersistente, pedidoCompraVenda);
			
			if(parametroPesquisa != null)
			{
				if(parametroPesquisa.getOperator().equals("BETWEEN"))
				{
					try
					{
						String attributeNameMin = parametroPesquisa.getAttributeName().split(";")[0];
						String attributeNameMax = parametroPesquisa.getAttributeName().split(";")[1];
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						
						String attributeValueMin = ((String) parametroPesquisa.getValue()).split(";")[0];
						String attributeValueMax = ((String) parametroPesquisa.getValue()).split(";")[1];
						
						listaCriterio.add(attributeNameMin.replace("Min", "") + " " + parametroPesquisa.getOperator() + " :" + attributeNameMin + " AND :" + attributeNameMax + " ");
						listaParametro.put(attributeNameMin, simpleDateFormat.parse(attributeValueMin));
						listaParametro.put(attributeNameMax, simpleDateFormat.parse(attributeValueMax));
					}
					catch(Exception parseEx)
					{
						//SUPRESS
					}
				}
				else
				{
					listaParametro.put(parametroPesquisa.getAttributeName().replaceAll("\\.", "_"), parametroPesquisa.getValue());
					
					listaCriterio.add(parametroPesquisa.getAttributeName() + " " + parametroPesquisa.getOperator() + ":" + parametroPesquisa.getAttributeName().replaceAll("\\.", "_"));
				}
			}
		}
		
		EntityManager manager = ConexaoUtil.getEntityManager();
		StringBuffer sql = new StringBuffer("from ItemPedidoCompraVenda as itemPedidoCompraVenda WHERE");
		
		if(ordemProduto != null && ordemProduto.getId() != null)
			sql.append(" itemPedidoCompraVenda.ordemProduto.id = " + ordemProduto.getId().toString());
		
		if(marca != null && marca.getId() != null && marca.getId() != 0)
		{
			if(ordemProduto != null && ordemProduto.getId() != null && ordemProduto.getId() != 0)
				sql.append(" and ");
			sql.append(" itemPedidoCompraVenda.produto.marca.id = " + marca.getId().toString());
		}
		
		for (int i = 0; i < listaCriterio.size(); i++)
		{
			if((ordemProduto != null && ordemProduto.getId() != null)||
					marca != null && marca.getId() != null && marca.getId() != 0)
				sql.append(" and itemPedidoCompraVenda.pedidoCompraVenda." + listaCriterio.get(i));
			else
			{
				if(i == 0)
					sql.append(" itemPedidoCompraVenda.pedidoCompraVenda." + listaCriterio.get(i));
				else
					sql.append(" and itemPedidoCompraVenda.pedidoCompraVenda." + listaCriterio.get(i));
			}
		}
		
		if(whereAdicional != null)
		{
			if(sql.toString().toLowerCase().contains("where"))
				sql.append(" and " + whereAdicional + " ");
			else
				sql.append(" where " + whereAdicional + " ");
		}
		
		if(String.valueOf(sql).endsWith("WHERE"))
			sql.append(" itemPedidoCompraVenda.pedidoCompraVenda.id is not null ");
		
		sql.append(" group by itemPedidoCompraVenda.pedidoCompraVenda.id ");

		Query queryCount = manager.createQuery("select itemPedidoCompraVenda.pedidoCompraVenda.id " + sql.toString());
		setParameters(queryCount, listaParametro);
		pagePedidoCompraVendaResultado.setTotalRecords(queryCount.getResultList().size());
		
		Query query = manager.createQuery(sql.toString());
		
		query.setMaxResults(maxResult);
		query.setFirstResult((pageNumber - 1) * maxResult);

		for (Iterator<java.util.Map.Entry<String, Object>> iterator = listaParametro.entrySet().iterator(); iterator.hasNext();)
		{
			java.util.Map.Entry<String, Object> entry = (java.util.Map.Entry<String, Object>) iterator.next();
			query.setParameter((String) entry.getKey(), entry.getValue());
		}
		
		@SuppressWarnings("rawtypes")
		List resultList = query.getResultList();
		
		for(Object obj : resultList)
		{
			PedidoCompraVenda pedidoCompraVendaResultado = ((ItemPedidoCompraVenda) obj).getPedidoCompraVenda();
			
			if(!pagePedidoCompraVendaResultado.getRecordList().contains(pedidoCompraVendaResultado))
				pagePedidoCompraVendaResultado.getRecordList().add(pedidoCompraVendaResultado);
		}
		
		pagePedidoCompraVendaResultado.setPageNumber(pageNumber);
		pagePedidoCompraVendaResultado.setPageSize(maxResult);
		
		manager.close();

		return pagePedidoCompraVendaResultado;
	}

	public Page<PedidoCompraVenda> listarPaginadoPorObjetoFiltroOrdemProduto(PedidoCompraVenda pedidoCompraVenda, OrdemProduto ordemProduto, int maxResult, int pageNumber, String whereAdicional) throws Exception
	{
		final Page<PedidoCompraVenda> pagePedidoCompraVendaResultado = new Page<PedidoCompraVenda>();
		pagePedidoCompraVendaResultado.setRecordList(new ArrayList<PedidoCompraVenda>());
		final Map<String, Object> listaParametro = new HashMap<String, Object>();
		List<String> listaCriterio = new ArrayList<String>();
		
		for (Field campoPersistente : UtilAnotationIGN.recuperarListaAtributoPersistente(pedidoCompraVenda))
		{
			SearchParameter parametroPesquisa = UtilAnotationIGN.criarParametroPesquisa(campoPersistente, pedidoCompraVenda);
			
			if(parametroPesquisa != null)
			{
				if(parametroPesquisa.getOperator().equals("BETWEEN"))
				{
					try
					{
						String attributeNameMin = parametroPesquisa.getAttributeName().split(";")[0];
						String attributeNameMax = parametroPesquisa.getAttributeName().split(";")[1];
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						
						String attributeValueMin = ((String) parametroPesquisa.getValue()).split(";")[0];
						String attributeValueMax = ((String) parametroPesquisa.getValue()).split(";")[1];
						
						listaCriterio.add(attributeNameMin.replace("Min", "") + " " + parametroPesquisa.getOperator() + " :" + attributeNameMin + " AND :" + attributeNameMax + " ");
						listaParametro.put(attributeNameMin, simpleDateFormat.parse(attributeValueMin));
						listaParametro.put(attributeNameMax, simpleDateFormat.parse(attributeValueMax));
					}
					catch(Exception parseEx)
					{
						//SUPRESS
					}
				}
				else
				{
					listaParametro.put(parametroPesquisa.getAttributeName().replaceAll("\\.", "_"), parametroPesquisa.getValue());
					
					listaCriterio.add(parametroPesquisa.getAttributeName() + " " + parametroPesquisa.getOperator() + ":" + parametroPesquisa.getAttributeName().replaceAll("\\.", "_"));
				}
			}
		}
		
		EntityManager manager = ConexaoUtil.getEntityManager();
		StringBuffer sql = new StringBuffer("from ItemPedidoCompraVenda as item ");
		
		if(ordemProduto != null && ordemProduto.getId() != null && ordemProduto.getId() != 0)
			sql.append(" WHERE item.ordemProduto.id = " + ordemProduto.getId().toString());
		
		for (int i = 0; i < listaCriterio.size(); i++)
		{
			if(ordemProduto != null && ordemProduto.getId() != null && ordemProduto.getId() != 0)	
				sql.append(" and item.pedidoCompraVenda." + listaCriterio.get(i));
			else
			{
				if(i == 0)
					sql.append(" where item.pedidoCompraVenda." + listaCriterio.get(i));
				else
					sql.append(" and item.pedidoCompraVenda." + listaCriterio.get(i));
			}
		}
		
		if(whereAdicional != null)
		{
			if(sql.toString().toLowerCase().contains("where"))
				sql.append(" and " + whereAdicional + " ");
			else
				sql.append(" where " + whereAdicional + " ");
		}
		
		sql.append(" group by item.pedidoCompraVenda.id ");

		Query queryCount = manager.createQuery("select item.pedidoCompraVenda.id " + sql.toString());
		setParameters(queryCount, listaParametro);
		pagePedidoCompraVendaResultado.setTotalRecords(queryCount.getResultList().size());
		
		Query query = manager.createQuery(sql.toString());
		
		query.setMaxResults(maxResult);
		query.setFirstResult((pageNumber - 1) * maxResult);

		for (Iterator<java.util.Map.Entry<String, Object>> iterator = listaParametro.entrySet().iterator(); iterator.hasNext();)
		{
			java.util.Map.Entry<String, Object> entry = (java.util.Map.Entry<String, Object>) iterator.next();
			query.setParameter((String) entry.getKey(), entry.getValue());
		}
		
		@SuppressWarnings("rawtypes")
		List resultList = query.getResultList();
		
		for(Object obj : resultList)
		{
			PedidoCompraVenda pedidoCompraVendaResultado = ((ItemPedidoCompraVenda) obj).getPedidoCompraVenda();
			
			if(!pagePedidoCompraVendaResultado.getRecordList().contains(pedidoCompraVendaResultado))
				pagePedidoCompraVendaResultado.getRecordList().add(pedidoCompraVendaResultado);
		}
		
		pagePedidoCompraVendaResultado.setPageNumber(pageNumber);
		pagePedidoCompraVendaResultado.setPageSize(maxResult);
		
		manager.close();

		return pagePedidoCompraVendaResultado;
	}
}