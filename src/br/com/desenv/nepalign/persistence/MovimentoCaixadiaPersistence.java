package br.com.desenv.nepalign.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.MovimentoCaixadia;
import br.com.desenv.nepalign.model.OperacaoRecebimentoDuplicata;
import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.nepalign.service.MovimentoCaixadiaService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class MovimentoCaixadiaPersistence extends GenericPersistenceIGN<MovimentoCaixadia>
{
	public List<MovimentoCaixadia> listarPorOperacao(OperacaoRecebimentoDuplicata op) throws Exception
	{
		
		MovimentoCaixadia mov = new MovimentoCaixadia();
		mov.setOperacao(op);
		
		return new MovimentoCaixadiaService().listarPorObjetoFiltro(mov);
		
	}
	public List<Integer> listarPorEmpresaFisicaPersistence(EmpresaFisica empresa,Boolean porUsuario,Usuario usu,Date data) throws SQLException
	{
		 SimpleDateFormat dataFormatada = new SimpleDateFormat("yyyy-MM-dd");
		String sql = "";
		sql = sql + " SELECT mov.idMovimentoCaixaDia FROM movimentocaixadia mov "; 
		sql = sql + " inner join caixadia caixa on caixa.idCaixaDia = mov.idCaixaDia "; 
		sql = sql + " inner join empresafisica empresa on caixa.idEmpresaFisica = empresa.idEmpresaFisica ";
		sql = sql + " WHERE mov.diversos = 'S'" ;
		if(porUsuario)
		{
			sql = sql +	"AND mov.idUsuario = "+ usu.getId();
		}
		if(data!=null)
		{
			sql = sql +"AND mov.dataVencimento = "+dataFormatada.format(data); 	
		}
		sql = sql +"AND empresa.idEmpresaFisica = "+empresa.getId(); 
		List<Integer> listaIds = new ArrayList<>();
		 Connection conn; 
	     
	     conn = ConexaoUtil.getConexaoPadrao();
		 java.sql.Statement rec = conn.createStatement();    
	     ResultSet rs = rec.executeQuery(sql.toString()); 
	
	    while(rs.next())
	    {
	    listaIds.add(rs.getInt("idMovimentoCaixaDia"));	
	    }
		
		return listaIds;
		
	
		
	}
	public List<Integer> listarPorUsuarioGlobalPersistence(Usuario usu,Boolean porEmpresa,Boolean porUsuario,EmpresaFisica empresa,Date data) throws SQLException
	{
		 SimpleDateFormat dataFormatada = new SimpleDateFormat("yyyy-MM-dd");
		String sql = "";
		sql = sql + " SELECT mov.idMovimentoCaixaDia FROM movimentocaixadia mov "; 
		sql = sql + " inner join caixadia caixa on caixa.idCaixaDia = mov.idCaixaDia "; 
		sql = sql + " inner join empresafisica empresa on caixa.idEmpresaFisica = empresa.idEmpresaFisica ";
		sql = sql + " WHERE mov.diversos = 'S'" ;
		if(porUsuario)
		{
			sql = sql +	"AND mov.idUsuario = "+ usu.getId();
		}
		if(porEmpresa)
		{
			sql = sql +" AND empresa.idEmpresaFisica = "+empresa.getId(); 	
		}
		if(data!=null)
		{
			sql = sql +" AND mov.dataVencimento = '"+dataFormatada.format(data)+"'"; 	
		}
		sql = sql + "";
		List<Integer> listaIds = new ArrayList<>();
		 Connection conn; 
	     
	     conn = ConexaoUtil.getConexaoPadrao();
		 java.sql.Statement rec = conn.createStatement();    
	     ResultSet rs = rec.executeQuery(sql.toString()); 
	
	    while(rs.next())
	    {
	    listaIds.add(rs.getInt("idMovimentoCaixaDia"));	
	    }
		
		return listaIds;
		
	}
}


