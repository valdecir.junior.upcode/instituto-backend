package br.com.desenv.nepalign.persistence;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import flex.messaging.io.ArrayList;
import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.ItemPedidoVenda;
import br.com.desenv.nepalign.model.PedidoVenda;
import br.com.desenv.nepalign.service.ItemPedidoVendaService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ItemPedidoVendaPersistence extends GenericPersistenceIGN<ItemPedidoVenda>
{
	@SuppressWarnings("unchecked")
	public List<ItemPedidoVenda> buscarPorPedido(EntityManager manager, PedidoVenda pedidoVenda)
	{
		boolean managerIndependente = false;
		
		try
		{
			if(pedidoVenda == null || pedidoVenda.getId() == null)
				return null;
			
			if((managerIndependente = (manager == null)))
				manager = ConexaoUtil.getEntityManager();
			
			return manager.createNativeQuery("SELECT * FROM itemPedidoVenda WHERE idPedidoVenda = " + pedidoVenda.getId().intValue(), ItemPedidoVenda.class).getResultList();	
		}
		catch(Exception ex)
		{
			if(managerIndependente)
				manager.close();
			
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
	@SuppressWarnings("unchecked")
	public List<ItemPedidoVenda> buscarItensComplementaresFila (PedidoVenda pedidoVenda, String planosDesconsiderados, EntityManager manager,  EntityTransaction transaction) throws Exception
	{
		boolean managerIndependente = false;
		List<ItemPedidoVenda> itensEncontrados;
		List<ItemPedidoVenda> itensSelecionados = new ArrayList();
		List<ItemPedidoVenda> itensNotaFiscal = new ArrayList();
		
		Double valorAcumulado=0.0;
		Double restante = 0.0;
		Double valorAlvo = 0.0;
		Double valorTotalPedidoOriginal = 0.0;
		Double valorAjustadoPedidoOriginal = 0.0;
		ItemPedidoVendaService itemService = new ItemPedidoVendaService();
		Double somaValorConferencia = 0.0;
		
		/*if (valorAlvo==null || valorAlvo == 0.0)
			throw new Exception("Valor alvo deve ser maior que zero e diferente de nulo.");*/
		
		
		try
		{
			if(manager == null)
				manager = ConexaoUtil.getEntityManager();
			
			List<ItemPedidoVenda> itensPedidoOriginal = buscarPorPedido(manager, pedidoVenda);
			
			for (ItemPedidoVenda itemPedidoOriginal : itensPedidoOriginal) 
			{
				valorTotalPedidoOriginal = (itemPedidoOriginal.getPrecoVenda()*itemPedidoOriginal.getQuantidade()) + valorTotalPedidoOriginal;
				valorAjustadoPedidoOriginal = (itemPedidoOriginal.getPrecoVendaAjustado()==null?0.00:itemPedidoOriginal.getPrecoVendaAjustado() *itemPedidoOriginal.getQuantidade()) + valorAjustadoPedidoOriginal;
				
			}
			valorAlvo = valorTotalPedidoOriginal - valorAjustadoPedidoOriginal;
			
			String sqlSelecao = " WHERE itemPedidoVenda.situacaoFila = 1 AND (precoVendaAjustado*quantidade) <= " + valorAlvo + " " +    
					" AND idPedidoVenda IN (SELECT pedidovenda.idPedidoVenda FROM pedidovenda WHERE pedidovenda.idPedidoVenda <> " + pedidoVenda.getId().intValue() + 
					" AND pedidovenda.idPlanoPagamento NOT IN (" + planosDesconsiderados + ") AND pedidovenda.situacaoFila = " + 1 + ")";

			itensEncontrados = manager.createNativeQuery("SELECT * FROM itemPedidoVenda " + sqlSelecao + " ORDER BY precoVendaAjustado DESC ", ItemPedidoVenda.class).getResultList();
			
			for (ItemPedidoVenda itemPedidoVenda : itensEncontrados) 
			{
				if (itemPedidoVenda.getPrecoVendaAjustado()<=(valorAlvo-valorAcumulado))
				{
					itensSelecionados.add(itemPedidoVenda);
					valorAcumulado = valorAcumulado + (itemPedidoVenda.getPrecoVendaAjustado()*itemPedidoVenda.getQuantidade());
					if (Math.abs(valorAlvo-valorAcumulado)<=10)
					{
						restante = Math.abs(valorAlvo-valorAcumulado)/itemPedidoVenda.getQuantidade();
						itemPedidoVenda.setPrecoVendaAjustado(itemPedidoVenda.getPrecoVendaAjustado()+restante);
						valorAcumulado = valorAlvo;
					}
				}
			}
			
			itensNotaFiscal.addAll(itensPedidoOriginal);
			itensNotaFiscal.addAll(itensSelecionados);
			if (Math.abs(valorAlvo-valorAcumulado)>1)
			{
				restante = valorAlvo - valorAcumulado;
				for (ItemPedidoVenda itemPedidoNf : itensNotaFiscal) 
				{
					double valorAjuste = (itemPedidoNf.getPrecoVendaAjustado()==null?0.0:itemPedidoNf.getPrecoVendaAjustado() * itemPedidoNf.getQuantidade())/valorTotalPedidoOriginal*restante;
					itemPedidoNf.setPrecoVendaAjustado(itemPedidoNf.getPrecoVendaAjustado()==null?0.0:itemPedidoNf.getPrecoVendaAjustado() + (valorAjuste/itemPedidoNf.getQuantidade()));
				}
			}
			for (ItemPedidoVenda itemPedidoNf : itensNotaFiscal) 
			{
				itemPedidoNf.setSituacaoFila(2);
				itemService.atualizar(manager, transaction, itemPedidoNf);
				somaValorConferencia = somaValorConferencia + itemPedidoNf.getPrecoVendaAjustado();
				//são dá pra salvar o preco novo aqui pois vai invalidar o objeto
				//itemPedidoNf.setPrecoVenda(itemPedidoNf.getPrecoVendaAjustado());
			}
			
			if (somaValorConferencia.doubleValue()!=valorTotalPedidoOriginal.doubleValue())
				System.out.println("*** Valores diferentes no ajuste:  " + pedidoVenda.getNumeroControle() + " " + valorTotalPedidoOriginal + " " + somaValorConferencia);
			else
				System.out.println("*** Valores IGUAIS no ajuste:  " + pedidoVenda.getNumeroControle() + " " + valorTotalPedidoOriginal + " " + somaValorConferencia);
			
			return itensNotaFiscal;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			if(managerIndependente)
				manager.close();
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
	
	public static void main(String[] args) throws Exception
	{
		PedidoVenda pedido = new PedidoVenda();
		pedido.setId(60);
		List<ItemPedidoVenda> listaItens = new ItemPedidoVendaPersistence().buscarItensComplementaresFila(pedido, "0", null, null);
		for (ItemPedidoVenda itemPedidoVenda : listaItens) {
			
			System.out.println(itemPedidoVenda.getPedidoVenda().getNumeroControle() + " " + itemPedidoVenda.getId() + " " + itemPedidoVenda.getPrecoVendaAjustado()==null?0.0:itemPedidoVenda.getPrecoVendaAjustado()*itemPedidoVenda.getQuantidade());
		}
	}
}