package br.com.desenv.nepalign.persistence;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.TipoMovimentoBancos;

public class TipoMovimentoBancosPersistence extends GenericPersistenceIGN<TipoMovimentoBancos>
{
	public TipoMovimentoBancosPersistence() { super(); }
}