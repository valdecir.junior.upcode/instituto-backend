package br.com.desenv.nepalign.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.Cliente;
import br.com.desenv.nepalign.model.Duplicata;
import br.com.desenv.nepalign.model.DuplicataAcordo;
import br.com.desenv.nepalign.model.DuplicataParcela;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.service.ClienteService;
import br.com.desenv.nepalign.service.DuplicataParcelaService;
import br.com.desenv.nepalign.service.DuplicataService;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class DuplicataAcordoPersistence extends GenericPersistenceIGN<DuplicataAcordo>
{


	public List<DuplicataAcordo> recuperarAcordosAgupados(String data,Cliente cli,EmpresaFisica empresa,String numeroDuplicataNova) throws Exception
	{
		String str = "";
		str = str + " SELECT acordo.idCliente,acordo.idEmpresaFisica,acordo.numeroNovaDuplicata,acordo.idDuplicataNova "; 
		str = str + "  FROM duplicataacordo acordo "; 
		str = str + " INNER JOIN duplicata nova on nova.idDuplicata = acordo.idDuplicataNova "; 
	 
	 int x = 0;
		
		if(data!=null)
		{
			 SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	         Date datan = format.parse(data.toString());
	         SimpleDateFormat formatsql = new SimpleDateFormat("yyyy-MM-dd");
	         String dataSql = formatsql.format(datan);
	     	str = str + " where nova.dataCompra = '"+dataSql+"'";
	     	x++;
		}
		if(cli != null)
		{
			if(x != 0)
			{
				str = str + " AND nova.idCliente = "+cli.getId();
				
			}
			else
			{
				str = str + " where nova.idCliente = "+cli.getId();
				x++;
			}
		}
		if(numeroDuplicataNova != null)
		{
			if(x != 0)
			{
				str = str + " AND nova.numeroDuplicata = "+numeroDuplicataNova;
				
			}
			else
			{
				str = str + " Where nova.numeroDuplicata = "+numeroDuplicataNova;
				x++;
			}		
		}
		if(empresa!=null)
		{
			if(x != 0)
			{
				str = str + " AND nova.idEmpresaFisica = "+empresa.getId();
				
			}
			else
			{
				str = str + " Where nova.idEmpresaFisica = "+empresa.getId();
				x++;
			}	
		}
		
		str = str + " 		group by acordo.idDuplicataNova "; 
		 Connection conn; 
	     
	     conn = ConexaoUtil.getConexaoPadrao();
		 java.sql.Statement rec = conn.createStatement();    
	     ResultSet rs = rec.executeQuery(str);
	     List<DuplicataAcordo> lista = new ArrayList<>();
	     DuplicataAcordo acordo = new DuplicataAcordo();
	     while(rs.next())
	     {
	    	 acordo = new DuplicataAcordo();
	    	 acordo.setCliente(new ClienteService().recuperarPorId(rs.getInt("idCliente")));
	    	 acordo.setDuplicataNova(new DuplicataService().recuperarPorId(rs.getInt("idDuplicataNova")));
	    	 acordo.setEmpresaFisica(new EmpresaFisicaService().recuperarPorId(rs.getInt("idEmpresaFisica")));
	    	 acordo.setNumeroNovaDuplicata(rs.getLong("numeroNovaDuplicata"));
	    	 lista.add(acordo);
	     }
	     return lista;
	}
	public List<DuplicataParcela> verificarDuplicatasVencidasParaAcordo(Duplicata duplicata) throws Exception
	{
		String str = "";
		str = str + " SELECT par.idDuplicataParcela as 'id' "; 
		str = str + "  FROM duplicataparcela par "; 
		str = str + " where par.idDuplicata = " +duplicata.getId(); 
		str = str + " and situacao <> 1 "; 
		str = str + " and DATEDIFF(CURRENT_DATE,par.dataVencimento)<=0 "; 
		
        Connection conn; 	     
	    conn = ConexaoUtil.getConexaoPadrao();
		java.sql.Statement rec = conn.createStatement();    
	    ResultSet rs = rec.executeQuery(str);
	    List<DuplicataParcela> lista = new ArrayList<>();
	    while(rs.next())
	    {
	    	lista.add(new DuplicataParcelaService().recuperarPorId(rs.getInt("id")));
	    }
	    return lista;
	}
	
	public String listarNumerosNovasDuplicatas (String dataEmissaoInicial, String dataEmissaoFinal, String numeroDuplicata)throws Exception
	{
		String query = "";
		query += " SELECT acordo.numeroNovaDuplicata from duplicataacordo acordo ";
		query += " inner join duplicata dupAntiga on dupAntiga.numeroDuplicata = acordo.numeroDuplicataAntiga ";

		if(!dataEmissaoInicial.equals(""))
			query += " where (dupAntiga.dataCompra >= '"+dataEmissaoInicial+" 00:00:00'"; 
		if(!dataEmissaoFinal.equals(""))
			query += " and dupAntiga.dataCompra <= '"+dataEmissaoFinal+" 23:59:59')";
		if(!numeroDuplicata.equals(""))
		{
			if((numeroDuplicata.substring(numeroDuplicata.length()-1,numeroDuplicata.length()).equals(",")))
			{
				numeroDuplicata = numeroDuplicata.substring(0,numeroDuplicata.length()-1);
			}
					
			
		}
			query += " and acordo.numeroNovaDuplicata in ("+numeroDuplicata+")";

		query += " group by numeroduplicataAntiga ";
		System.out.println("Persit: "+query);
		ResultSet result = ConexaoUtil.getConexaoPadrao().createStatement().executeQuery(query);
		String numerosNovasDuplicatas = "";
		while(result.next())
		{
			if(result.last())
				numerosNovasDuplicatas = result.getString("numeroNovaDuplicata");
			else
				numerosNovasDuplicatas = result.getString("numeroNovaDuplicata") + ",";
			
		}
		
		result.beforeFirst();
		return numerosNovasDuplicatas;
	}
}


