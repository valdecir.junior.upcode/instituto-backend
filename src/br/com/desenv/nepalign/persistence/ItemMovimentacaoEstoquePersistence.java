package br.com.desenv.nepalign.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.DsvException;
import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.ItemMovimentacaoEstoque;
import br.com.desenv.nepalign.service.LogOperacaoService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ItemMovimentacaoEstoquePersistence extends GenericPersistenceIGN<ItemMovimentacaoEstoque>
{
	
	@Override
	public ItemMovimentacaoEstoque atualizar (ItemMovimentacaoEstoque entidade) throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		
		ItemMovimentacaoEstoque entidadeSalva = null;
		try
		{
			transaction.begin();
			
			new LogOperacaoService().gravarOperacaoAlterar(manager, transaction, entidade);			
			entidadeSalva = atualizar(manager, transaction, entidade);
			
			transaction.commit();
		}
		catch(Exception ex)
		{
			if(transaction != null && transaction.isActive())
				transaction.rollback();
			
			throw new DsvException(ex);
		}
		finally
		{
			manager.close();
		}
		return entidadeSalva;
	}

}


