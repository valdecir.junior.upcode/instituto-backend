package br.com.desenv.nepalign.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.SeprocadoReativados;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class SeprocadoReativadosPersistence extends GenericPersistenceIGN<SeprocadoReativados>
{
	public Date recuperarUltimaDataSeprocados(EmpresaFisica empresa) throws SQLException, ParseException
	{
		String str = "";
		str = str + " SELECT * FROM parametrosistema where chave = 'ULTIMADATASEPROCADOS' "; 
		str = str + " and idempresafisica = " +empresa.getId();
		Connection conn;
		 conn = ConexaoUtil.getConexaoPadrao(); 
		 System.out.println(str);
		 ResultSet rs = conn.createStatement().executeQuery(str); 
		 String valor = "";
		if(rs.next())
		{
			valor = rs.getString("valor");
		}
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		Date data = null;
		if(!valor.equals(""))
		{
			data = formatador.parse(valor);
		}
		return data;
		
	}
	public Date recuperarUltimaReativados(EmpresaFisica empresa) throws SQLException, ParseException
	{
		String str = "";
		str = str + " SELECT * FROM parametrosistema where chave = 'ULTIMADATAREATIVADOS' "; 
		str = str + " and idempresafisica = " +empresa.getId();
		Connection conn;
		 conn = ConexaoUtil.getConexaoPadrao(); 
		 System.out.println(str);
		 ResultSet rs = conn.createStatement().executeQuery(str); 
		 String valor = "";
		if(rs.next())
		{
			valor = rs.getString("valor");
		}
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		Date data = null;
		if(!valor.equals(""))
		{
			data = formatador.parse(valor);
		}
		return data;
	}
}


