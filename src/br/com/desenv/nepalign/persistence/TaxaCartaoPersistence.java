package br.com.desenv.nepalign.persistence;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.desenv.nepalign.model.EmpresaFinanceiro;
import br.com.desenv.nepalign.model.FormaPagamentoCartao;
import br.com.desenv.nepalign.model.TaxaCartao;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class TaxaCartaoPersistence extends GenericPersistenceIGN<TaxaCartao>
{	 
	 public TaxaCartao buscaTaxaCartaoPorEmpresaFinanceiro(EntityManager manager, EmpresaFinanceiro empresaFinanceiro, FormaPagamentoCartao formaPagamentoCartao, Integer quantidadeParcela, Date dataLancamento) throws Exception
	 {
		 Boolean managerIndependente = false;
		 
		 try
		 {
			 if(manager == null)
			 {
				 manager = ConexaoUtil.getEntityManager();
				 managerIndependente = true;
			 }
			 
			 String dataLancamentoFormatada = new SimpleDateFormat("yyyy-MM-dd").format(dataLancamento);
			
			 String str = "";
			 str = str + " SELECT taxa.idTaxaCartao as 'id' FROM taxaCartao taxa ";  
			 str = str + " where taxa.parcela =  " + quantidadeParcela.intValue(); 
			 str = str + " and taxa.idEmpresaFinanceiro = " + empresaFinanceiro.getId().intValue();
			 str = str + " and taxa.idFormaPagamentoCartao = " + formaPagamentoCartao.getId().intValue();

			 str = str + " ORDER BY (DATE(taxa.dataVigencia) <= '" + dataLancamentoFormatada + "') desc, ABS((datediff('" + dataLancamentoFormatada + "', DATE(taxa.dataVigencia)))) asc, (DATE(taxa.dataVigencia) = '" + dataLancamentoFormatada + "') ";
		 
			 @SuppressWarnings(value="rawtypes")
			 List obj = manager.createNativeQuery(str).getResultList();
			 
			 if(obj.size() == 0x00)
				 return null;
			 
			 return super.recuperarPorId(manager, (int) obj.get(0x00));
		 }
		 catch(Exception ex)
		 {
			 ex.printStackTrace();

			 throw ex;
		 }
		 finally
		 {
			 if(managerIndependente)
				 manager.close();
		 }
	 }
}