package br.com.desenv.nepalign.persistence;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.FormaPagamentoCartao;

public class FormaPagamentoCartaoPersistence extends GenericPersistenceIGN<FormaPagamentoCartao>
{
	public FormaPagamentoCartaoPersistence()
	{
		super();
	}
}