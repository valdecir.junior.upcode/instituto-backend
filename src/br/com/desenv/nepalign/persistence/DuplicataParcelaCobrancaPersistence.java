package br.com.desenv.nepalign.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.DuplicataParcelaCobranca;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class DuplicataParcelaCobrancaPersistence extends GenericPersistenceIGN<DuplicataParcelaCobranca>
{
	public Date retornaDataUltimaImpressao() throws SQLException
	{
		String sql ="select max(dataImpressao) as dt from duplicataparcelacobranca where ocorrencia ='P' ";
		Connection conn = ConexaoUtil.getConexaoPadrao();    
		ResultSet rs = conn.createStatement().executeQuery(sql);
		if(rs.next())
			return rs.getDate("dt");
		else
			return null;
		
	}
}


