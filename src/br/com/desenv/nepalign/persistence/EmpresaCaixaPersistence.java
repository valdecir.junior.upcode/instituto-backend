package br.com.desenv.nepalign.persistence;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.EmpresaCaixa;

public class EmpresaCaixaPersistence extends GenericPersistenceIGN<EmpresaCaixa>
{
	public EmpresaCaixaPersistence() { super(); }
}