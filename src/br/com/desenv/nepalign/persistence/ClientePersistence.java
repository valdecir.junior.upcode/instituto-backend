package br.com.desenv.nepalign.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.Cliente;
import br.com.desenv.nepalign.model.DuplicataParcela;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ClientePersistence extends GenericPersistenceIGN<Cliente>
{
public List<Cliente> recuperarClienteParaSeprocar(Date ultimaPesquisa,Integer idEmpresaFisica) throws Exception
{
	Integer dia = null;
	Integer diaSeprocar = DsvConstante.getParametrosSistema().get("diasSeprocar")==null?45:DsvConstante.getParametrosSistema().getInt("diasSeprocar");
	if(ultimaPesquisa!=null)
	{
		 dia = (int) ((new Date().getTime()-ultimaPesquisa.getTime())/(1000 * 60 * 60 * 24));
		 dia = dia+diaSeprocar;
	}
	
	String str = "";
	str = str + " SELECT cli.idCliente  "; 
	str = str + " FROM duplicataparcela par "; 
	str = str + " INNER JOIN cliente cli on cli.idCliente = par.idCliente "; 
	str = str + " where (DATEDIFF(par.dataVencimento,Current_DATE)*(-1))>= "+diaSeprocar;
	if(dia!=null)
		str = str + " and (DATEDIFF(par.dataVencimento,Current_DATE)*(-1))<=  "+dia;	
	str = str + " and par.situacao = 0";
	str = str + " and par.idEmpresaFisica = "+idEmpresaFisica;
	str = str + " group by cli.idCliente "; 
	List<Integer> listaIds = new ArrayList<>();
	List<Cliente> listaCliente = new ArrayList<>();
	List<Cliente> listaClientesParaSeprocar = new ArrayList<>();
	 Connection conn; 
    
    conn = ConexaoUtil.getConexaoPadrao();
	java.sql.Statement rec = conn.createStatement();  
	System.out.println(str);
    ResultSet rs = rec.executeQuery(str.toString()); 
   while(rs.next())
   {
   listaIds.add(rs.getInt("idCliente"));	
   }
   for (int i = 0; i < listaIds.size(); i++) 
   {
	listaCliente.add(this.recuperarPorId(listaIds.get(i)));
   }
   for (int i = 0; i <listaCliente.size(); i++)
   {
	Cliente cli = listaCliente.get(i);
	if(cli.getDataEntradaScpc()==null)
	{
		cli.setDataEntradaScpc(new Date());
		listaClientesParaSeprocar.add(cli);
	}
	else
	{
		if(cli.getDataBaixaScpc()!=null)
		{
		if(cli.getDataBaixaScpc().getTime()>cli.getDataEntradaScpc().getTime())
		{
			
			listaClientesParaSeprocar.add(cli);
		}
		}
	}
   }
   return listaClientesParaSeprocar;
}
public Date recuperarDataEmisaoRelacaoCobranca(EmpresaFisica empresa) throws SQLException, ParseException
{
	String str = "";
	str = str + " SELECT * FROM parametrosistema where chave = 'ultimaDataRelatorioEmissaoCobranca' "; 
	str = str + " and idempresafisica = " +empresa.getId();
	Connection conn;
	 conn = ConexaoUtil.getConexaoPadrao(); 
	 System.out.println(str);
	 ResultSet rs = conn.createStatement().executeQuery(str); 
	 String valor = "";
	if(rs.next())
	{
		valor = rs.getString("valor");
	}
	SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
	Date data = null;
	if(!valor.equals(""))
	{
		data = formatador.parse(valor);
	}
	return data;
}

	public ArrayList<Integer> recuperarDiasEmAtrasoParcelasPagas(Integer idCliente) throws Exception
	{
		Integer diasEmAtraso = 0;
		Integer totalParcelasPagas = 0;
		
		String sql = "";
		sql += " SELECT  count(idduplicataparcela) as qtd, par.numeroDuplicata, par.numeroParcela,";
		sql += " if(par.situacao='0',DATEDIFF(current_date,par.datavencimento), if(par.situacao='1'";
		sql += " or par.situacao='3' ,DATEDIFF(par.dataBaixa , par.dataVencimento),DATEDIFF(current_date, par.dataBaixa ))) as diasAtraso ";
		sql += " FROM duplicataParcela par ";
		sql += " WHERE par.idcliente = "+idCliente+" and par.situacao <>0 ";
		sql += " GROUP BY par.idDuplicataparcela; ";
		
		ResultSet rs = ConexaoUtil.getConexaoPadrao().createStatement().executeQuery(sql);
		
		while (rs.next())
		{
			diasEmAtraso += rs.getInt("diasAtraso");
			totalParcelasPagas += rs.getInt("qtd"); 
		}
		ArrayList<Integer> resultado = new ArrayList<>();
		resultado.add(diasEmAtraso);
		resultado.add(totalParcelasPagas);
		return resultado;
	}
}


