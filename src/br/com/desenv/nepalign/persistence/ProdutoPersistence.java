package br.com.desenv.nepalign.persistence;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import br.com.desenv.frameworkignorante.DsvException;
import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.frameworkignorante.Page;
import br.com.desenv.frameworkignorante.SearchParameter;
import br.com.desenv.frameworkignorante.UtilAnotationIGN;
import br.com.desenv.nepalign.model.EstoqueProduto;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ProdutoPersistence extends GenericPersistenceIGN<Produto>
{
	public Integer getUltimoCodigo() throws Exception
	{
		EntityManager entityManager = ConexaoUtil.getEntityManager();
		entityManager.getTransaction().begin();
		Query q = entityManager.createNativeQuery("SELECT valor FROM parametrosSistema WHERE descricao = 'ultimoIdProduto'");
		
		@SuppressWarnings("rawtypes")
		List resultCodigo = q.getResultList();
		
		Object codigoProdutoResult = (Object)resultCodigo.get(0);
		Integer idProduto = (Integer.parseInt(codigoProdutoResult.toString()) + 1);
		
		Produto produto = this.recuperarPorId(entityManager, idProduto);
		
		if(produto == null)
			return (idProduto - 1);
		else
		{
			entityManager.createNativeQuery("UPDATE parametrossistema set valor = " + (idProduto) + " WHERE descricao = 'ultimoIdProduto'").executeUpdate();
			entityManager.getTransaction().commit();
			entityManager.close();
			return this.getUltimoCodigo();
		}
	}
	
	public Produto salvar(Produto entidade) throws DsvException
	{
		Produto produto = super.salvar(entidade);
		
		ConexaoUtil.getEntityManager().createNativeQuery("UPDATE parametrossistema set valor = " + produto.getId() + " WHERE descricao = 'ultimoIdProduto'").executeUpdate();
		
		return produto;
	}
	
	public Produto salvar(EntityManager manager, EntityTransaction transaction, Produto entidade) throws Exception
	{
		Produto produto = super.salvar(manager, transaction, entidade);
		
		manager.createNativeQuery("UPDATE parametrossistema set valor = " + produto.getId() + " WHERE descricao = 'ultimoIdProduto'").executeUpdate();
		
		return produto;
	}
	
	public Page<Produto> listarPaginadoPorObjetoFiltroCodigoBarras(Produto produto, String codigoBarras, int maxResult, int pageNumber) throws Exception
	{
		final Page<Produto> pageProdutoResultado = new Page<Produto>();
		pageProdutoResultado.setRecordList(new ArrayList<Produto>());
		final Map<String, Object> listaParametro = new HashMap<String, Object>();
		List<String> listaCriterio = new ArrayList<String>();
		
		for (Field campoPersistente : UtilAnotationIGN.recuperarListaAtributoPersistente(produto))
		{
			SearchParameter parametroPesquisa = UtilAnotationIGN.criarParametroPesquisa(campoPersistente, produto);
			
			if(parametroPesquisa != null)
			{
				listaParametro.put(parametroPesquisa.getAttributeName().replaceAll("\\.", "_"), parametroPesquisa.getValue());
				
				listaCriterio.add(parametroPesquisa.getAttributeName() + " " + parametroPesquisa.getOperator() + ":" + parametroPesquisa.getAttributeName().replaceAll("\\.", "_"));	
			}
		}
		
		EntityManager manager = ConexaoUtil.getEntityManager();
		StringBuffer sql = new StringBuffer("from EstoqueProduto as estoq ");
		
		if(codigoBarras != null && codigoBarras.length() > 0)
			sql.append(" where estoq.codigoBarras = " + codigoBarras + " ");

		for (int i = 0; i < listaCriterio.size(); i++)
		{
			if(codigoBarras != null && codigoBarras.length() > 0)
				sql.append(" and estoq.produto." + listaCriterio.get(i));
			else
			{
				if(i == 0)
					sql.append(" where estoq.produto." + listaCriterio.get(i));
				else
					sql.append(" and estoq.produto." + listaCriterio.get(i));
			}
		}
		
		sql.append(" group by estoq.produto.id ");

		Query queryCount = manager.createQuery("select estoq.produto.id " + sql.toString());
		setParameters(queryCount, listaParametro);
		pageProdutoResultado.setTotalRecords(queryCount.getResultList().size());
		
		Query query = manager.createQuery(sql.toString());
		
		query.setMaxResults(maxResult);
		query.setFirstResult((pageNumber - 1) * maxResult);

		for (Iterator<java.util.Map.Entry<String, Object>> iterator = listaParametro.entrySet().iterator(); iterator.hasNext();)
		{
			java.util.Map.Entry<String, Object> entry = (java.util.Map.Entry<String, Object>) iterator.next();
			query.setParameter((String) entry.getKey(), entry.getValue());
		}

		List resultList = query.getResultList();
		
		for(Object obj : resultList)
		{
			Produto produtoResultado = ((EstoqueProduto) obj).getProduto();
			
			if(!pageProdutoResultado.getRecordList().contains(produtoResultado))
				pageProdutoResultado.getRecordList().add(produtoResultado);
		}

		pageProdutoResultado.setPageNumber(pageNumber);
		pageProdutoResultado.setPageSize(maxResult);

		return pageProdutoResultado;
	}
	
	public Page<Produto> listarPaginadoPorObjetoFiltroEstoqueProduto(Produto produto, EstoqueProduto estoqueProduto, int maxResult, int pageNumber) throws Exception
	{
		final Page<Produto> pageProdutoResultado = new Page<Produto>();
		pageProdutoResultado.setRecordList(new ArrayList<Produto>());
		final Map<String, Object> listaParametro = new HashMap<String, Object>();
		
		List<String> listaCriterio = new ArrayList<String>();
		List<String> listaCriterioEstoqueProduto = new ArrayList<String>();
		
		for (Field campoPersistente : UtilAnotationIGN.recuperarListaAtributoPersistente(produto))
		{
			SearchParameter parametroPesquisa = UtilAnotationIGN.criarParametroPesquisa(campoPersistente, produto);
			
			if(parametroPesquisa != null)
			{
				listaParametro.put(parametroPesquisa.getAttributeName().replaceAll("\\.", "_"), parametroPesquisa.getValue());
				
				listaCriterio.add(parametroPesquisa.getAttributeName() + " " + parametroPesquisa.getOperator() + ":" + parametroPesquisa.getAttributeName().replaceAll("\\.", "_"));	
			}
		}
		
		for (Field campoPersistente : UtilAnotationIGN.recuperarListaAtributoPersistente(estoqueProduto))
		{
			SearchParameter parametroPesquisa = UtilAnotationIGN.criarParametroPesquisa(campoPersistente, estoqueProduto);
			
			if(parametroPesquisa != null && !parametroPesquisa.getAttributeName().toLowerCase().equals("produto"))
			{
				listaParametro.put(parametroPesquisa.getAttributeName().replaceAll("\\.", "_"), parametroPesquisa.getValue());
				
				listaCriterioEstoqueProduto.add(parametroPesquisa.getAttributeName() + " " + parametroPesquisa.getOperator() + ":" + parametroPesquisa.getAttributeName().replaceAll("\\.", "_"));	
			}
		}
		
		EntityManager manager = ConexaoUtil.getEntityManager();
		StringBuffer sql = new StringBuffer("from EstoqueProduto as estoq ");
		
		Boolean ownerEstoqueProduto = listaCriterioEstoqueProduto.size() > 0;
		
		for (int i = 0; i < listaCriterioEstoqueProduto.size(); i++)
		{
			if(i == 0)
				sql.append(" where estoq." + listaCriterioEstoqueProduto.get(i));
			else
				sql.append(" and estoq." + listaCriterioEstoqueProduto.get(i));
		}
		
		if(!ownerEstoqueProduto)
			sql = new StringBuffer("from Produto as produto ");
		
		for (int i = 0; i < listaCriterio.size(); i++)
		{
			if(listaCriterioEstoqueProduto.size() == 0 && i == 0)
				sql.append(" where " + (!ownerEstoqueProduto ? "" : "estoq.") + "produto." + listaCriterio.get(i));
			else
				sql.append(" and " + (!ownerEstoqueProduto ? "" : "estoq.") + "produto." + listaCriterio.get(i));
		}
		
		if(ownerEstoqueProduto)
			sql.append(" group by estoq.produto.id order by estoq.produto.id desc ");

		Query queryCount = manager.createQuery("select " + (!ownerEstoqueProduto ? "" : "estoq.") + "produto.id " + sql.toString());
		setParameters(queryCount, listaParametro);
		pageProdutoResultado.setTotalRecords(queryCount.getResultList().size());
		
		Query query = manager.createQuery(sql.toString());
		
		query.setMaxResults(maxResult);
		query.setFirstResult((pageNumber - 1) * maxResult);

		for (Iterator<java.util.Map.Entry<String, Object>> iterator = listaParametro.entrySet().iterator(); iterator.hasNext();)
		{
			java.util.Map.Entry<String, Object> entry = (java.util.Map.Entry<String, Object>) iterator.next();
			query.setParameter((String) entry.getKey(), entry.getValue());
		}

		List resultList = query.getResultList();
		
		for(Object obj : resultList)
		{
			Produto produtoResultado = null;
			
			if(ownerEstoqueProduto)
				produtoResultado = ((EstoqueProduto) obj).getProduto();
			else
				produtoResultado = (Produto) obj;
			
			if(!pageProdutoResultado.getRecordList().contains(produtoResultado))
				pageProdutoResultado.getRecordList().add(produtoResultado);
		}

		pageProdutoResultado.setPageNumber(pageNumber);
		pageProdutoResultado.setPageSize(maxResult);

		return pageProdutoResultado;
	}

	public Produto recuperarProdutoPorCodigoBarras(EntityManager manager, final String codigoBarras) throws Exception
	{
		Boolean managerIndependente = false;
		
		try
		{
			if((managerIndependente = (manager == null)))
				manager = ConexaoUtil.getEntityManager();
			
			return (Produto) manager.createNativeQuery("SELECT produto.* FROM estoqueProduto " +
					"INNER JOIN produto ON produto.idProduto = estoqueProduto.idProduto " +
					"WHERE estoqueProduto.codigoBarras = '".concat(codigoBarras).concat("';"), Produto.class).getSingleResult();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
}