package br.com.desenv.nepalign.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.CaixaDia;
import br.com.desenv.nepalign.model.PagamentoCartao;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class PagamentoCartaoPersistence extends GenericPersistenceIGN<PagamentoCartao>
{
public List<PagamentoCartao> recuperarPagamentosCartaoSemEmitente(CaixaDia caixa) throws Exception
{
	String str = "";
	str = str + " SELECT idPagamentoCartao FROM pagamentocartao "; 
	str = str + " where emitente is null and idcaixaDia = "+caixa.getId();
	List<Integer> listaIds = new ArrayList<>();
	 Connection conn; 
   
   conn = ConexaoUtil.getConexaoPadrao();
	 java.sql.Statement rec = conn.createStatement();    
   ResultSet rs = rec.executeQuery(str); 
  while(rs.next())
  {
  listaIds.add(rs.getInt("idPagamentoCartao"));	
  }
  List<PagamentoCartao> lista = new ArrayList<>();
  for (int i = 0; i < listaIds.size(); i++) 
  {
	lista.add(this.recuperarPorId(listaIds.get(i)));
  }
  return lista;
}
}


