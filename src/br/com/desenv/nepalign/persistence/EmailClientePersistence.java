package br.com.desenv.nepalign.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.Cliente;
import br.com.desenv.nepalign.model.EmailCliente;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.MovimentacaoEstoque;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.service.ClienteService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class EmailClientePersistence extends GenericPersistenceIGN<EmailCliente>
{
	
	public List<Cliente> listarPorObjetoFiltroClienteQueCompraramSql(EntityManager manager, String where) throws Exception
	{
		Boolean managerIndependente = false;
		List<Cliente> listaCliente = new ArrayList<Cliente>();
		Connection connection = null;
		try
		{
			if(manager == null)
				manager = ConexaoUtil.getEntityManager();
			
			connection = ConexaoUtil.getConexaoPadrao();
			
			
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append(" c.idCliente AS idCliente, ");
			sql.append(" c.nome AS nomeCliente, ");
			sql.append(" c.dataNascimento AS nascimento, ");
			sql.append(" CONCAT(LPAD(DAY(c.dataNascimento), 2, '0'), '/', LPAD(MONTH(c.dataNascimento), 2, '0')) AS diaMes, ");
			sql.append(" GROUP_CONCAT(LOWER(ec.email) SEPARATOR '; ') AS emails ");

			sql.append(" FROM ");
			sql.append(" cliente c ");
			sql.append(" INNER JOIN emailcliente ec on ec.idcliente = c.idcliente ");
			
			if(!where.equals(""))
			{
				sql.append(" WHERE "+where+" AND ec.email is NOT NULL ");
			}
	
			sql.append("GROUP BY ");
			sql.append(" c.idCliente ");
			sql.append("ORDER BY ");
			sql.append(" c.nome ");
			
			
			System.out.println("Query Envio: "+sql.toString() );
			ResultSet rs = connection.createStatement().executeQuery(String.valueOf(sql));
			while(rs.next())
			{
				listaCliente.add(new ClienteService().recuperarPorId(rs.getInt("idCliente")));
			}
			
			return listaCliente;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
			connection.close();
			listaCliente = null;
			System.gc();
		}
	}

}


