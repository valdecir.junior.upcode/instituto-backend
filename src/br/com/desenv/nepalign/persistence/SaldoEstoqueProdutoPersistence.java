package br.com.desenv.nepalign.persistence;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import br.com.desenv.frameworkignorante.DsvException;
import br.com.desenv.frameworkignorante.Formatador;
import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.frameworkignorante.Page;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.EstoqueProduto;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.model.SaldoEstoqueProduto;
import br.com.desenv.nepalign.model.SaldoEstoqueProdutoReferenciaLoja;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.EstoqueProdutoService;
import br.com.desenv.nepalign.service.ProdutoService;
import br.com.desenv.nepalign.service.SituacaoEnvioLVService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class SaldoEstoqueProdutoPersistence extends GenericPersistenceIGN<SaldoEstoqueProduto>
{
	@Override
	public SaldoEstoqueProduto salvar(SaldoEstoqueProduto entidade) throws DsvException
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		
		SaldoEstoqueProduto entidadeSalva = null;

		try
		{
			if(entidade.getVenda() == null)
				throw new Exception("preço de venda zerado");
			
			transaction.begin();
			entidadeSalva = salvar(manager, transaction, entidade);
			transaction.commit();
		}
		catch (Exception e)
		{
			if(transaction != null && transaction.isActive())
				transaction.rollback();
			
			throw new DsvException(e);
		}
		finally
		{
			manager.close();
		}
		
		return entidadeSalva;
	}
	
	public Double buscarSaldoDia(EmpresaFisica empresa, EstoqueProduto estoqueProduto, Date dataSaldo, Connection con) throws ParseException, SQLException
	{
		Date date = null; 
		Calendar cal = null;
		Integer ano = null;
	    cal=Calendar.getInstance();
	    cal.setTime(dataSaldo);
	    Integer mes = cal.get(Calendar.MONTH)+1;
	   
	    if (mes-1==0)
	    {
	    	mes = 12;
	    	ano = cal.get(Calendar.YEAR)-1;
	    	System.out.println(""+ano);
	    }else{
	    	mes= mes-1;
	    }
		StringBuilder sql = new StringBuilder("SELECT");
		sql.append(" saldoe.saldo FROM nepalign.saldoestoqueproduto saldoe");
		sql.append(" WHERE idEstoqueProduto = "+estoqueProduto.getId());
		sql.append(" and mes ="+mes);
		if(ano != null){
			sql.append(" and ano ="+ano);
		}
		if(empresa != null){
			sql.append(" AND idEmpresaFisica = "+empresa.getId());
		}
		
		Double saldoAnterior = null;
		Statement stm = null;  
		ResultSet rs = null; 
		stm = con.createStatement(); 
		try
		{
			rs = stm.executeQuery(sql.toString());
			if(rs.next() == true)
			{    	 
				 saldoAnterior = rs.getDouble("saldo");
			}
			else
			{
				saldoAnterior = 0.0;
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		
		return (saldoAnterior.doubleValue() + buscarSaldoPeriodo(empresa, estoqueProduto, dataSaldo, con));

	}
	public Double buscarSaldoInicialDia(EmpresaFisica empresa, EstoqueProduto estoqueProduto, Date dataSaldo, Connection con) throws ParseException, SQLException
	{
		Date date = null; 
		Calendar cal = null;
		Integer ano = null;
	    cal = Calendar.getInstance();
	    cal.setTime(dataSaldo);
	    
	    Calendar dtBaseMovimentacao = Calendar.getInstance();
	    
	    Integer mes = cal.get(Calendar.MONTH)+1;
	   
	    if (mes-1==0)
	    {
	    	mes = 12;
	    	ano = cal.get(Calendar.YEAR)-1;
	    	System.out.println(""+ano);
	    }else{
	    	mes= mes-1;
	    }
		StringBuilder sql = new StringBuilder("SELECT");
		sql.append(" saldoe.saldo FROM nepalign.saldoestoqueproduto saldoe");
		sql.append(" WHERE idEstoqueProduto = "+estoqueProduto.getId());
		sql.append(" and mes ="+mes);
		if(ano != null){
			sql.append(" and ano ="+ano);
		}
		if(empresa != null){
			sql.append(" AND idEmpresaFisica = "+empresa.getId());
		}
		
		Double saldoAnterior = null;
		Statement stm = null;  
		ResultSet rs = null; 
		stm = con.createStatement(); 
		try
		{
			rs = stm.executeQuery(sql.toString());
			if(rs.next() == true)
			{    	 
				 saldoAnterior = rs.getDouble("saldo");
			}
			else
			{
				saldoAnterior = 0.0;
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		dtBaseMovimentacao.setTime(dataSaldo);
		if (dtBaseMovimentacao.get(Calendar.DAY_OF_MONTH)==1)
		{
			return saldoAnterior.doubleValue();
		}
		else
		{
			dtBaseMovimentacao.add(Calendar.DAY_OF_MONTH, -1);
			return (saldoAnterior.doubleValue() + buscarSaldoPeriodo(empresa, estoqueProduto, dtBaseMovimentacao.getTime(), con));
		}

	}
	public Double buscarSaldoInicialDiaReferencia(EmpresaFisica empresa, EstoqueProduto estoque, Produto produto, Date dataInicial, Date dataFinal, Connection con) throws ParseException, SQLException
	{
		Date date = null; 
		Calendar cal = null;
		Integer ano = null;
	    cal = Calendar.getInstance();
	    cal.setTime(dataInicial);
	    
	    Calendar dtBaseInicialMovimentacao = Calendar.getInstance();
	    Calendar dtBaseFinalMovimentacao = Calendar.getInstance();
	    
	    Integer mes = cal.get(Calendar.MONTH)+1;
	   
	    if (mes-1==0)
	    {
	    	mes = 12;
	    	ano = cal.get(Calendar.YEAR)-1;
	    	System.out.println(""+ano);
	    }else{
	    	mes= mes-1;
	    	ano = cal.get(Calendar.YEAR);
	    }
		StringBuilder sql = new StringBuilder("SELECT");
		sql.append(" sum(saldoe.saldo) as totalSaldo FROM nepalign.saldoestoqueproduto saldoe");
		sql.append(" INNER JOIN estoqueProduto estoque on estoque.idEstoqueProduto = saldoe.idEstoqueProduto ");
		sql.append(" INNER JOIN produto produto on produto.idProduto = estoque.idProduto ");
		
		if(produto != null)
			sql.append(" WHERE produto.idProduto = " + produto.getId());
		
		if(estoque!= null)
			sql.append(" WHERE estoque.idEstoqueProduto = " + estoque.getId());
		
		sql.append(" and mes ="+mes);
		if(ano != null){
			sql.append(" and ano ="+ano);
		}
		if(empresa != null){
			sql.append(" AND idEmpresaFisica = "+empresa.getId());
		}
		
		Double saldoAnterior = null;
		Statement stm = null;  
		ResultSet rs = null; 
		stm = con.createStatement(); 
		try
		{
			rs = stm.executeQuery(sql.toString());
			if(rs.next() == true)
			{    	 
				 saldoAnterior = rs.getDouble("totalSaldo");
			}
			else
			{
				saldoAnterior = 0.0;
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		dtBaseInicialMovimentacao.setTime(dataInicial);
		
		if (dtBaseInicialMovimentacao.get(Calendar.DAY_OF_MONTH)==1)
		{
			return saldoAnterior.doubleValue();
		}
		else
		{
			dtBaseFinalMovimentacao.setTime(dataFinal);
			return (saldoAnterior.doubleValue() + buscarSaldoPeriodo(empresa, produto, dtBaseInicialMovimentacao.getTime(), dtBaseFinalMovimentacao.getTime(), con));
		}

	}
	public double buscarSaldoPeriodo(EmpresaFisica empresa, Produto produto, Date dtBaseInicialMovimentacao, Date dtBaseFinalMovimentacao, Connection con) throws ParseException, SQLException
	{
		 DateFormat formatadorDataSql = new SimpleDateFormat("yyyy-MM-dd");
		 //DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
		 String strDateIncialsql = null;
		 String strDateFinalsql = null;
		 Date dateIncialSql = null;
		 Date dateFinalSql = null;
		 strDateIncialsql = formatadorDataSql.format(dtBaseInicialMovimentacao);
		 strDateFinalsql = formatadorDataSql.format(dtBaseFinalMovimentacao);
		 dateIncialSql = (java.util.Date)formatadorDataSql.parse(strDateIncialsql);
		 dateFinalSql = (java.util.Date)formatadorDataSql.parse(strDateFinalsql);
		 Date novaDataInicial = null;
		 Date novaDataFinal = null;
		 
		 Calendar calI = null;
		 Calendar calF = null;
		 Calendar calI2 = null;
		 Calendar calF2 = null;
		 
		 calI = Calendar.getInstance();
		 calF = Calendar.getInstance();
		 calI2 = Calendar.getInstance();
		 calF2 = Calendar.getInstance();
		 
		 calI.setTime(dtBaseInicialMovimentacao); 
		 calF.setTime(dtBaseFinalMovimentacao);
		 
	 
		 Integer dia = calI.get(Calendar.DAY_OF_MONTH);
	     Double valorSaida = 0.0;
	     Double valorEntrada = 0.0;
		 if(dia != 1)
		 {
			 calI2.set(calI.get(Calendar.YEAR),calI.get(Calendar.MONTH),1);
			 novaDataInicial = (calI2.getTime());
			 calF2.set(calI.get(Calendar.YEAR),calI.get(Calendar.MONTH), calI.get(Calendar.DAY_OF_MONTH)-1);
			 novaDataFinal = (calF2.getTime());
			 
			 StringBuilder sql = new StringBuilder("SELECT");
			 sql.append(" sum(if(mov.entradaSaida = 'E',item.quantidade,0)) as valorEntrada, " +
			   			  " sum(if(mov.entradaSaida = 'S',item.quantidade,0)) as valorSaida " +
			 			  " FROM nepalign.itemmovimentacaoestoque item" +
			 			  " INNER JOIN movimentacaoestoque mov ON mov.idMovimentacaoEstoque = item.idMovimentacaoEstoque" +
			 			  " INNER JOIN produto produto on produto.idProduto = item.idProduto " +
			 			  " WHERE mov.dataMovimentacao between   '" + formatadorDataSql.format(novaDataInicial) + " 00:00:00' AND  '" + formatadorDataSql.format(novaDataFinal) + " 23:59:59' " 
			 			  );
			 if(produto != null)
			 {
			  sql.append(" AND produto.idProduto = " + produto.getId());
			 }
			 if(empresa!=null)
			 {
				   sql.append(" AND mov.idEmpresaFisica = "+empresa.getId());
			 }
			   
			 sql.append(" group by produto.idProduto");
			 Statement stm = null;  
		     ResultSet rs = null; 
		     stm = con.createStatement();
		     try
		     {
		    	 rs = stm.executeQuery(sql.toString());
		     }
		     catch(Exception ex)
		     {
		    	 ex.printStackTrace();
		     }
		     
	     
		     while(rs.next())
		     {
		    	 valorSaida += rs.getDouble("valorSaida");
		    	 valorEntrada += rs.getDouble("valorEntrada"); 
		     }
		 }

		 return valorEntrada-valorSaida;

	     

	}
	public double buscarSaldoPeriodo(EmpresaFisica empresa, EstoqueProduto estoqueProduto, Date dataSaldo, Connection con) throws ParseException, SQLException
	{
		 DateFormat formatadorDataSql = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 //DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
		 DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		 Date date = null;
		 String strDatesql = null;
		 Date datesql = null;
		 strDatesql = formatadorDataSql.format(dataSaldo);
		 List<Double> lista = new ArrayList<Double>();
		 datesql = (java.util.Date)formatadorDataSql.parse(strDatesql);
		  
		 Calendar cal = Calendar.getInstance();
		 cal.setTime(datesql);
		 cal.set(Calendar.DAY_OF_MONTH, 1);
		    
		 StringBuilder sql = new StringBuilder("SELECT");
		 sql.append(" sum(if(mov.entradaSaida = 'E',item.quantidade,0)) as valorEntrada, " +
		   			  " sum(if(mov.entradaSaida = 'S',item.quantidade,0)) as valorSaida " +
		 			  " FROM nepalign.itemmovimentacaoestoque item" +
		 			  " INNER JOIN movimentacaoestoque mov ON mov.idMovimentacaoEstoque = item.idMovimentacaoEstoque" +
		 			  " WHERE mov.dataMovimentacao between   '"+formatadorDataSql.format(cal.getTime())+"' AND  '"+formatadorDataSql.format(datesql)+"' " 
		 			  );
		 if(estoqueProduto != null)
		 {
		  sql.append(" AND idEstoqueProduto = "+estoqueProduto.getId());
		 }
		 if(empresa!=null)
		 {
			   sql.append(" AND mov.idEmpresaFisica = "+empresa.getId());
		 }
		   
		 sql.append(" group by idEstoqueProduto");
		 Statement stm = null;  
	     ResultSet rs = null; 
	     stm = con.createStatement();
	     try
	     {
	    	 rs = stm.executeQuery(sql.toString());
	     }
	     catch(Exception ex)
	     {
	    	 ex.printStackTrace();
	     }
	     
	     Double valorSaida = 0.0;
	     Double valorEntrada = 0.0;
	     
	     while(rs.next())
	     {
	    	 valorSaida = rs.getDouble("valorSaida");
	    	 valorEntrada= rs.getDouble("valorEntrada"); 
	     }
	     return valorEntrada-valorSaida;

	}	
	public List<Double> buscarEntradaSaida(EmpresaFisica empresa, EstoqueProduto estoqueProduto, Date dataSaldo, Connection con) throws ParseException, SQLException
	{
		   DateFormat formatadorDataSql = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		   //DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
		   DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		   Date date = null;
		   String strDatesql = null;
		   Date datesql = null;
		   strDatesql = formatadorDataSql.format(dataSaldo);
		   List<Double> lista = new ArrayList<Double>();
		   datesql = (java.util.Date)formatadorDataSql.parse(strDatesql);
		   
		   
		   Calendar cal = Calendar.getInstance();
		   cal.setTime(datesql);
		   cal.set(Calendar.DAY_OF_MONTH, 1);
		    
		   StringBuilder sql = new StringBuilder("SELECT");
		   sql.append(" sum(if(mov.entradaSaida = 'E',item.quantidade,0))  as valorEntrada," +
		   			  " sum(if(mov.entradaSaida = 'S',item.quantidade,0)) as valorSaida " +
		   			  " FROM nepalign.itemmovimentacaoestoque item" +
		   			  " INNER JOIN movimentacaoestoque mov ON mov.idMovimentacaoEstoque = item.idMovimentacaoEstoque" +
		   			  " WHERE mov.dataMovimentacao between   '"+formatadorDataSql.format(cal.getTime())+"' AND  '"+formatadorDataSql.format(datesql)+"' " 
		   			  );
		   if(estoqueProduto != null)
		   {
			   sql.append(" AND idEstoqueProduto = "+estoqueProduto.getId());
		   }
		   if(empresa!=null)
		   {
			   sql.append(" AND mov.idEmpresaFisica = "+empresa.getId());
		   }
		   
		   sql.append(" group by idEstoqueProduto");
		   Statement stm = null;  
		     ResultSet rs = null; 
		     stm = con.createStatement();
		     try
		     {
		    	 rs = stm.executeQuery(sql.toString());
		     }
		     catch(Exception ex)
		     {
		    	 ex.printStackTrace();
		     }
		     
		     Double valorSaida = null;
		     Double valorEntrada = null;
		     
		     while(rs.next())
		     {
		    	 valorSaida = rs.getDouble("valorSaida");
		    	 valorEntrada= rs.getDouble("valorEntrada");
		    	 
		    	 if(valorEntrada!=0.0)
		    		 lista.add(valorEntrada); 
		    		 
		    	 if(valorSaida != 0.0)
		    		 lista.add(valorSaida); 
		     }
		     if(valorEntrada==null || valorEntrada==0.0)
		     {
		     valorEntrada = 0.0;
		     lista.add(0,valorEntrada);
		     }
		     if(valorSaida==null || valorSaida==0.0)
		     {
		    	 valorSaida = 0.0;
		    	 lista.add(1,valorSaida);
			 }
			return lista;
	}
	
	public EmpresaFisica recuperarEmpresaFisica(Integer idEmpresaFisica, List<EmpresaFisica> listaEmpresaFisica) throws Exception
	{
		if(listaEmpresaFisica == null)
			listaEmpresaFisica = new EmpresaFisicaService().listar();
		
		for(EmpresaFisica empresaFisica : listaEmpresaFisica)
		{
			if(empresaFisica.getId() == idEmpresaFisica)
				return empresaFisica;
		}
		
		return null;
	}

	public Page<SaldoEstoqueProduto> listarPaginadoPorObjetoFiltroSql(EntityManager manager, SaldoEstoqueProduto entidadeFiltro, int pageNumber, int pageSize) throws Exception
	{
		return Formatador.gerarPagina(this.listarPorObjetoFiltroSql(manager, entidadeFiltro), pageNumber, pageSize);
	}
	
	public List<SaldoEstoqueProduto> listarPorObjetoFiltroSql(EntityManager manager, SaldoEstoqueProduto entidadeFiltro) throws Exception
	{
		Boolean managerIndependente = false;
		List<SaldoEstoqueProduto> listaSaldoEstoqueProduto = new ArrayList<SaldoEstoqueProduto>();
		Connection connection = null;
		try
		{
			if(manager == null)
				manager = ConexaoUtil.getEntityManager();
			
			connection = ConexaoUtil.getConexaoPadrao();
			
			List<EmpresaFisica> listaEmpresaFisica = new EmpresaFisicaService().listarPorObjetoFiltro(manager, new EmpresaFisica(), null, null, null);
			
			StringBuilder query = new StringBuilder();
			
			if(entidadeFiltro.getSaldo() == 0.0)
				entidadeFiltro.setSaldo(null);
			if(entidadeFiltro.getCusto() == 0.0)
				entidadeFiltro.setCusto(null);
			
			query.append("SELECT * FROM saldoEstoqueProduto WHERE ");
			if(entidadeFiltro != null)
			{
				query.append(entidadeFiltro.getId() != null ? " idSaldoEstoqueProduto = " + entidadeFiltro.getId().toString() + " AND " : "");
				query.append(entidadeFiltro.getMes() != null ? " mes = " + entidadeFiltro.getMes().toString() + " AND " : "");
				query.append(entidadeFiltro.getAno() != null ? " ano = " + entidadeFiltro.getAno().toString() + " AND " : "");
				query.append(entidadeFiltro.getSaldo() != null ? " saldo = " + entidadeFiltro.getSaldo().toString() + " AND " : "");
				query.append(entidadeFiltro.getVenda() != null ? " venda = " + entidadeFiltro.getVenda().toString() + " AND " : "");
				query.append(entidadeFiltro.getCusto() != null ? " custo = " + entidadeFiltro.getCusto().toString() + " AND " : "");
				query.append(entidadeFiltro.getDataUltimaAlteracao() != null ? " dataUltimaAlteracao = " + new SimpleDateFormat("yyyy-MM-dd").format(entidadeFiltro.getDataUltimaAlteracao()) + " AND " : "");
				query.append(entidadeFiltro.getEmpresaFisica() != null ? " idEmpresaFisica = " + entidadeFiltro.getEmpresaFisica().getId().toString() + " AND " : "");
				query.append(entidadeFiltro.getEstoqueProduto() != null ? " idEstoqueProduto = " + entidadeFiltro.getEstoqueProduto().getId().toString() + " AND " : "");
				query.append(entidadeFiltro.getSituacaoEnvioLV() != null ? " idSituacaoEnvioLV = " + entidadeFiltro.getSituacaoEnvioLV().getId().toString() + " AND " : "");
			}
			if(String.valueOf(query).endsWith("AND ") || String.valueOf(query).endsWith(" WHERE "))
				query.append(" idSaldoEstoqueProduto IS NOT NULL ");
			
			ResultSet rs = connection.createStatement().executeQuery(String.valueOf(query));

			while(rs.next())
			{
				SaldoEstoqueProduto saldoEstoqueProdutoBuffer = new SaldoEstoqueProduto();
				saldoEstoqueProdutoBuffer.setId(rs.getInt("idSaldoEstoqueProduto"));
				saldoEstoqueProdutoBuffer.setAno(rs.getInt("ano"));
				saldoEstoqueProdutoBuffer.setMes(rs.getInt("mes"));
				saldoEstoqueProdutoBuffer.setCusto(rs.getDouble("custo"));
				saldoEstoqueProdutoBuffer.setVenda(rs.getDouble("venda"));
				saldoEstoqueProdutoBuffer.setSaldo(rs.getDouble("saldo"));
				saldoEstoqueProdutoBuffer.setEmpresaFisica(this.recuperarEmpresaFisica((Integer) rs.getInt("idEmpresaFisica"), listaEmpresaFisica));
				saldoEstoqueProdutoBuffer.setEstoqueProduto((Integer) rs.getInt("idEstoqueProduto") != null ? new EstoqueProdutoService().recuperarPorId(manager, rs.getInt("idEstoqueProduto")) : null);
		
				listaSaldoEstoqueProduto.add(saldoEstoqueProdutoBuffer);
			}
			
			return listaSaldoEstoqueProduto;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
			connection.close();
			listaSaldoEstoqueProduto = null;
			System.gc();
		}
	}
	
	public List<SaldoEstoqueProduto> listarPorObjetoFiltroProdutoSql(EntityManager manager, Produto produto, EmpresaFisica empresaFisica) throws Exception
	{
		Boolean managerIndependente = false;
		List<SaldoEstoqueProduto> listaSaldoEstoqueProduto = new ArrayList<SaldoEstoqueProduto>();
		Connection connection = null;
		try
		{
			if(manager == null)
				manager = ConexaoUtil.getEntityManager();
			
			connection = ConexaoUtil.getConexaoPadrao();
			
			
			StringBuilder query = new StringBuilder();
			
			List<EmpresaFisica> listaEmpresaFisica = new EmpresaFisicaService().listar();
			
			query.append("SELECT * FROM saldoEstoqueProduto as saldo ");
			query.append("INNER JOIN estoqueproduto estoque ON saldo.idEstoqueProduto = estoque.idEstoqueProduto ");
			query.append("INNER JOIN produto prod on estoque.idProduto = prod.idProduto ");
			query.append("WHERE prod.idProduto = " + produto.getId() + " ");
			
			if(empresaFisica != null)
				query.append("and saldo.idEmpresaFisica = " + empresaFisica.getId() + " and mes = " + empresaFisica.getMesAberto() + " and ano = " + empresaFisica.getAnoAberto());
			
			ResultSet rs = connection.createStatement().executeQuery(String.valueOf(query));

			while(rs.next()) 
			{
				SaldoEstoqueProduto saldoEstoqueProdutoBuffer = new SaldoEstoqueProduto();
				saldoEstoqueProdutoBuffer.setId(rs.getInt("idSaldoEstoqueProduto"));
				saldoEstoqueProdutoBuffer.setAno(rs.getInt("ano"));
				saldoEstoqueProdutoBuffer.setMes(rs.getInt("mes"));
				saldoEstoqueProdutoBuffer.setCusto(rs.getDouble("custo"));
				saldoEstoqueProdutoBuffer.setVenda(rs.getDouble("venda"));
				saldoEstoqueProdutoBuffer.setSaldo(rs.getDouble("saldo"));
				saldoEstoqueProdutoBuffer.setEmpresaFisica(this.recuperarEmpresaFisica((Integer) rs.getInt("idEmpresaFisica"), listaEmpresaFisica));
				saldoEstoqueProdutoBuffer.setSituacaoEnvioLV(new SituacaoEnvioLVService().recuperarPorId((Integer) rs.getInt("idSituacaoEnvioLV")));
				saldoEstoqueProdutoBuffer.setDataUltimaAlteracao(rs.getDate("dataUltimaAlteracao"));
				saldoEstoqueProdutoBuffer.setEstoqueProduto((Integer) rs.getInt("idEstoqueProduto") != null ? new EstoqueProdutoService().recuperarPorId(manager, rs.getInt("idEstoqueProduto")) : null);
		
				listaSaldoEstoqueProduto.add(saldoEstoqueProdutoBuffer);
			}
			
			return listaSaldoEstoqueProduto;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
			connection.close();
			listaSaldoEstoqueProduto = null;
			System.gc();
		}
	}
	
	public Page<SaldoEstoqueProduto> listarPaginadoPorObjetoFiltroIndexProdutoSql(EntityManager manager, SaldoEstoqueProduto entidadeFiltro, int pageNumber, int pageSize) throws Exception
	{
		return Formatador.gerarPagina(this.listarPorObjetoFiltroIndexProdutoSql(manager, entidadeFiltro), pageNumber, pageSize);
	}
	
	public List<SaldoEstoqueProduto> listarPorObjetoFiltroIndexProdutoSql(EntityManager manager, SaldoEstoqueProduto entidadeFiltro) throws Exception
	{
		Boolean managerIndependente = false;
		List<SaldoEstoqueProduto> listaSaldoEstoqueProduto = new ArrayList<SaldoEstoqueProduto>();
		Connection connection = null;
		try
		{
			if(manager == null)
				manager = ConexaoUtil.getEntityManager();
			
			connection = ConexaoUtil.getConexaoPadrao();
			
			List<EmpresaFisica> listaEmpresaFisica = new EmpresaFisicaService().listarPorObjetoFiltro(manager, new EmpresaFisica(), null, null, null);
			
			StringBuilder query = new StringBuilder();
			
			query.append("SELECT * FROM saldoEstoqueProduto WHERE ");
			if(entidadeFiltro != null)
			{
				query.append(entidadeFiltro.getId() != null ? " idSaldoEstoqueProduto = " + entidadeFiltro.getId().toString() + " AND " : "");
				query.append(entidadeFiltro.getMes() != null ? " mes = " + entidadeFiltro.getMes().toString() + " AND " : "");
				query.append(entidadeFiltro.getAno() != null ? " ano = " + entidadeFiltro.getAno().toString() + " AND " : "");
				query.append(entidadeFiltro.getSaldo() != null ? " saldo = " + entidadeFiltro.getSaldo().toString() + " AND " : "");
				query.append(entidadeFiltro.getVenda() != null ? " venda = " + entidadeFiltro.getVenda().toString() + " AND " : "");
				query.append(entidadeFiltro.getCusto() != null ? " custo = " + entidadeFiltro.getCusto().toString() + " AND " : "");
				query.append(entidadeFiltro.getDataUltimaAlteracao() != null ? " dataUltimaAlteracao = " + new SimpleDateFormat("yyyy-MM-dd").format(entidadeFiltro.getDataUltimaAlteracao()) + " AND " : "");
				query.append(entidadeFiltro.getEmpresaFisica() != null ? " idEmpresaFisica = " + entidadeFiltro.getEmpresaFisica().getId().toString() + " AND " : "");
				query.append(entidadeFiltro.getEstoqueProduto() != null ? " idEstoqueProduto IN (SELECT idEstoqueProduto FROM estoqueProduto WHERE idProduto = " + entidadeFiltro.getEstoqueProduto().getProduto().getId() + ") AND " : "");
				query.append(entidadeFiltro.getSituacaoEnvioLV() != null ? " idSituacaoEnvioLV = " + entidadeFiltro.getSituacaoEnvioLV().getId().toString() + " AND " : "");
			}
			if(String.valueOf(query).endsWith("AND ") || String.valueOf(query).endsWith(" WHERE "))
				query.append(" idSaldoEstoqueProduto IS NOT NULL ");
			
			ResultSet rs = connection.createStatement().executeQuery(String.valueOf(query));

			while(rs.next())
			{
				SaldoEstoqueProduto saldoEstoqueProdutoBuffer = new SaldoEstoqueProduto();
				saldoEstoqueProdutoBuffer.setId(rs.getInt("idSaldoEstoqueProduto"));
				saldoEstoqueProdutoBuffer.setAno(rs.getInt("ano"));
				saldoEstoqueProdutoBuffer.setMes(rs.getInt("mes"));
				saldoEstoqueProdutoBuffer.setCusto(rs.getDouble("custo"));
				saldoEstoqueProdutoBuffer.setVenda(rs.getDouble("venda"));
				saldoEstoqueProdutoBuffer.setSaldo(rs.getDouble("saldo"));
				saldoEstoqueProdutoBuffer.setEmpresaFisica(this.recuperarEmpresaFisica((Integer) rs.getInt("idEmpresaFisica"), listaEmpresaFisica));
				saldoEstoqueProdutoBuffer.setEstoqueProduto((Integer) rs.getInt("idEstoqueProduto") != null ? new EstoqueProdutoService().recuperarPorId(manager, rs.getInt("idEstoqueProduto")) : null);
		
				listaSaldoEstoqueProduto.add(saldoEstoqueProdutoBuffer);
			}
			
			return listaSaldoEstoqueProduto;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
			connection.close();
			listaSaldoEstoqueProduto = null;
			System.gc();
		}
	}
	
	/**
	 * Faz a exclusão do estoque (saldoEstoqueProduto) a partir de uma entidade filtro 
	 * @param manager
	 * @param entidadeFiltro
	 * @throws Exception
	 */
	public void excluirSql(EntityManager manager, SaldoEstoqueProduto entidadeFiltro) throws Exception
	{
		Boolean transacaoIndependente = false;
		
		try
		{
			if((transacaoIndependente = (manager == null)))
				manager = ConexaoUtil.getEntityManager();
			
			final StringBuilder query = new StringBuilder();
			
			query.append("DELETE FROM SaldoEstoqueProduto WHERE ");
			
			if(entidadeFiltro != null)
			{
				query.append(entidadeFiltro.getId() != null ? " idSaldoEstoqueProduto = " + entidadeFiltro.getId().toString() + " AND " : "");
				query.append(entidadeFiltro.getMes() != null ? " mes = " + entidadeFiltro.getMes().toString() + " AND " : "");
				query.append(entidadeFiltro.getAno() != null ? " ano = " + entidadeFiltro.getAno().toString() + " AND " : "");
				query.append(entidadeFiltro.getSaldo() != null ? " saldo = " + entidadeFiltro.getSaldo().toString() + " AND " : "");
				query.append(entidadeFiltro.getVenda() != null ? " venda = " + entidadeFiltro.getVenda().toString() + " AND " : "");
				query.append(entidadeFiltro.getCusto() != null ? " custo = " + entidadeFiltro.getCusto().toString() + " AND " : "");
				query.append(entidadeFiltro.getDataUltimaAlteracao() != null ? " dataUltimaAlteracao = " + new SimpleDateFormat("yyyy-MM-dd").format(entidadeFiltro.getDataUltimaAlteracao()) + " AND " : "");
				query.append(entidadeFiltro.getEmpresaFisica() != null ? " idEmpresaFisica = " + entidadeFiltro.getEmpresaFisica().getId().toString() + " AND " : "");
				query.append(entidadeFiltro.getEstoqueProduto() != null ? " idEstoqueProduto = " + entidadeFiltro.getEstoqueProduto().getId().toString() + " AND " : "");
				query.append(entidadeFiltro.getSituacaoEnvioLV() != null ? " idSituacaoEnvioLV = " + entidadeFiltro.getSituacaoEnvioLV().getId().toString() + " AND " : "");
			}
			
			if(String.valueOf(query).endsWith("AND ") || String.valueOf(query).endsWith(" WHERE "))
				query.append(" idSaldoEstoqueProduto IS NOT NULL ");
			
			manager.createNativeQuery(String.valueOf(query)).executeUpdate();
			
			if(transacaoIndependente)
				manager.getTransaction().commit();
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				manager.getTransaction().rollback();
			
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
	}
	
	public Double buscarMaiorPrecoProduto(Produto produto) throws ParseException, SQLException
	{
		return buscarMaiorPrecoProduto(produto, null);
	}

	public Double buscarMaiorPrecoProduto(Produto produto,Connection con) throws ParseException, SQLException
	{
			Double maiorPreco = null;
			boolean conexaoPropria=false;
			
			if (con==null)
			{
				con = ConexaoUtil.getConexaoPadrao();
				conexaoPropria=true;
			}
			
			String strSql = "select round(max(venda),2) maiorPreco" +
						" from SaldoEstoqueProduto inner join EstoqueProduto on EstoqueProduto.idEstoqueProduto = SaldoEstoqueProduto.idEstoqueProduto " +
						" where EstoqueProduto.idProduto = " + produto.getId();
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(strSql);
			
			if (rs.next())
				maiorPreco = rs.getDouble("maiorPreco");
			
			if (st!=null)
				st.close();
			if (conexaoPropria==true && con!=null)
				con.close();
			
			return maiorPreco;
	}	

	@SuppressWarnings("unchecked")
	public List<SaldoEstoqueProdutoReferenciaLoja> buscarSaldoPorCodigoBarrasLoja(final String codigoBarras) throws Exception
	{
		final EntityManager manager = ConexaoUtil.getEntityManager();

		try
		{
			final StringBuffer query = new StringBuffer();	
			query.append("SELECT saldo.idSaldoEstoqueProduto, estoque.idEstoqueProduto, empresa.idEmpresaFisica, prod.idProduto, ultimoPrecoVendaEstoqueProduto(estoque.idProduto, saldo.idEmpresaFisica) as venda, saldo.custo as custo, SUM(saldo.saldo) AS saldo ");
			query.append("FROM  saldoestoqueproduto saldo ");
			query.append("INNER JOIN estoqueproduto estoque ON saldo.idEstoqueProduto = estoque.idEstoqueProduto ");
			query.append("INNER JOIN empresafisica empresa ON empresa.idEmpresaFisica = saldo.idEmpresaFisica AND saldo.mes = empresa.mesAberto AND saldo.ano = empresa.anoAberto ");
			query.append("INNER JOIN produto prod on estoque.idProduto = prod.idProduto ");
			query.append("WHERE estoque.idEstoqueProduto = " + codigoBarras + " ");
			
			query.append("GROUP BY empresa.idEmpresaFisica, prod.idProduto ");
			query.append("order by  empresa.idEmpresaFisica, prod.idProduto ASC, saldo.idSaldoEstoqueProduto DESC; ");
			 
			List<SaldoEstoqueProdutoReferenciaLoja> tmp = (List<SaldoEstoqueProdutoReferenciaLoja>) manager.createNativeQuery(query.toString(), SaldoEstoqueProdutoReferenciaLoja.class).getResultList();
			
			for(SaldoEstoqueProdutoReferenciaLoja refLoja : tmp)
				refLoja.setQuantidadeUltimaEntrada(((Integer) manager.createNativeQuery("SELECT consultaQuantidadeUltimaCompra(".
						concat(String.valueOf(refLoja.getEstoqueProduto().getProduto().getId())).
						concat(",").concat(String.valueOf(refLoja.getEmpresaFisica().getId())).concat(")")).getSingleResult()).doubleValue());
			
			return tmp;
		} 
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			manager.close();
		}
	}
	 
	@SuppressWarnings("unchecked")
	public List<SaldoEstoqueProdutoReferenciaLoja> buscarSaldoPorReferenciaLoja(Produto produto, Connection con) throws Exception
	{  
		final EntityManager manager = ConexaoUtil.getEntityManager();

		try
		{ 
			final StringBuffer query = new StringBuffer();	
			query.append("SELECT saldo.idSaldoEstoqueProduto, empresa.idEmpresaFisica, estoque.idEstoqueProduto, ultimoPrecoVendaEstoqueProduto(estoque.idProduto, saldo.idEmpresaFisica) as venda, saldo.custo as custo, SUM(saldo.saldo) AS saldo, ");
			query.append(" consultaQuantidadeUltimaCompra(estoque.idProduto, empresa.idEmpresaFisica) as quantidadeUltimaEntrada ");
			query.append("FROM  saldoestoqueproduto saldo ");
			query.append("INNER JOIN estoqueproduto estoque ON saldo.idEstoqueProduto = estoque.idEstoqueProduto ");
			query.append("INNER JOIN empresafisica empresa ON empresa.idEmpresaFisica = saldo.idEmpresaFisica AND saldo.mes = empresa.mesAberto AND saldo.ano = empresa.anoAberto ");
			query.append("WHERE estoque.idProduto = " + produto.getId() + " ");
			
			query.append("GROUP BY empresa.idEmpresaFisica, estoque.idProduto ");
			query.append("order by  empresa.idEmpresaFisica, estoque.idProduto ASC, saldo.idSaldoEstoqueProduto DESC; ");
			
			List<SaldoEstoqueProdutoReferenciaLoja> tmp = (List<SaldoEstoqueProdutoReferenciaLoja>) manager.createNativeQuery(query.toString(), SaldoEstoqueProdutoReferenciaLoja.class).getResultList();
			
			for(SaldoEstoqueProdutoReferenciaLoja refLoja : tmp)
				refLoja.setQuantidadeUltimaEntrada(((Integer) manager.createNativeQuery("SELECT consultaQuantidadeUltimaCompra(".
						concat(String.valueOf(refLoja.getEstoqueProduto().getProduto().getId())).
						concat(",").concat(String.valueOf(refLoja.getEmpresaFisica().getId())).concat(")")).getSingleResult()).doubleValue());
			
			return tmp;
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			manager.close();
		}
	}
	
	public List<SaldoEstoqueProduto> buscarSaldoPorEmpresaReferencia(EmpresaFisica empresa, Produto produto, Connection con) throws Exception
	{
		List<SaldoEstoqueProduto> listaSaldos = new ArrayList<SaldoEstoqueProduto>();
		boolean conexaoPropria = false;
		Statement stm = null;
		ResultSet rs = null;
		StringBuffer str = new StringBuffer();
		SaldoEstoqueProduto sep;
		EmpresaFisicaService eService = new EmpresaFisicaService();
		EstoqueProduto ep;
		
		try
		{
			produto = new ProdutoService().recuperarPorId(produto.getId());
			
			str.append("SELECT saldo.venda,saldo.custo, saldo.saldo AS saldoProduto, empresa.razaoSocial, ");
			str.append("prod.nomeProduto, prod.codigoProduto, empresa.idEmpresaFisica, empresa.razaoSocial, estoque.idEstoqueProduto as idEstoqueProduto, ");
			str.append("FROM  saldoestoqueproduto saldo ");
			str.append("INNER JOIN estoqueproduto estoque ON saldo.idEstoqueProduto = estoque.idEstoqueProduto ");
			str.append("INNER JOIN tamanho tam ON estoque.idTamanho = tam.codigo ");
			str.append("INNER JOIN empresafisica empresa ON empresa.idEmpresaFisica = saldo.idEmpresaFisica ");
			str.append("INNER JOIN cor cor on estoque.idCor = cor.codigo ");
			str.append("INNER JOIN produto prod on estoque.idProduto = prod.idProduto ");
			str.append("INNER JOIN ordemproduto ordem on ordem.codigo = estoque.idOrdemProduto ");
			str.append("WHERE prod.idProduto = " + produto.getId() + " ");
			str.append("and saldo.idEmpresaFisica = " + empresa.getId() + " ");
			str.append("AND saldo.mes = " + empresa.getMesAberto() + " ");
			str.append("AND saldo.ano = " + empresa.getAnoAberto() + " ");
			 			
			if (con==null)
			{
				con = ConexaoUtil.getConexaoPadrao();
				conexaoPropria = true;
			}
			stm = con.createStatement();
			rs =  stm.executeQuery(str.toString());
			
			while (rs.next())
			{
				ep = new EstoqueProduto();
				ep.setId(rs.getInt("idEstoqueProduto"));
				sep = new SaldoEstoqueProduto();
				sep.setEstoqueProduto(ep);
				sep.setEmpresaFisica(empresa);
				sep.setSaldo(rs.getDouble("saldoProduto"));
				sep.setCusto(rs.getDouble("custo"));
				sep.setVenda(rs.getDouble("venda"));
				listaSaldos.add(sep);
			}			
			
			stm.close();
			if (conexaoPropria==true)
				con.close();
			
			return listaSaldos;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return listaSaldos;
		
	}	
	
	public List<SaldoEstoqueProduto> buscarSaldoPorListaStringEmpresaReferencia(String empresas, Produto produto, Connection con) throws Exception
	{
		List<SaldoEstoqueProduto> listaSaldos = new ArrayList<SaldoEstoqueProduto>();
		boolean conexaoPropria = false;
		Statement stm = null;
		ResultSet rs = null;
		StringBuffer str = new StringBuffer();
		SaldoEstoqueProduto sep;
		EstoqueProduto ep;

		try
		{
			produto = new ProdutoService().recuperarPorId(produto.getId());
			
			str.append("SELECT venda,custo, saldo AS saldoProduto, idEstoqueProduto ");
			str.append("FROM  saldoestoqueproduto where  ");
			str.append("idEmpresaFisica in( " + empresas + ") ");
			str.append("AND mes = " + new EmpresaFisicaService().mesAberto() + " ");
			str.append("AND ano = " + new EmpresaFisicaService().anoAberto() + " ");
			str.append("AND idestoqueProduto in ( select idestoqueproduto from estoqueproduto where idproduto = "+produto.getId()+") ");
			
			 			
			if (con==null)
			{
				con = ConexaoUtil.getConexaoPadrao();
				conexaoPropria = true;
			}
			stm = con.createStatement();
			rs =  stm.executeQuery(str.toString());
			
			while (rs.next())
			{
				ep = new EstoqueProdutoService().recuperarPorId(rs.getInt("idEstoqueProduto"));
				sep = new SaldoEstoqueProduto();
				sep.setSaldo(rs.getDouble("saldoProduto"));
				sep.setCusto(rs.getDouble("custo"));
				sep.setVenda(rs.getDouble("venda"));
				sep.setEstoqueProduto(ep);
				listaSaldos.add(sep);
			}			
			
			stm.close();
			if (conexaoPropria==true)
				con.close();
			
			return listaSaldos;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return listaSaldos;
	}	

	public Double calcularCustoMedioProduto(EmpresaFisica empresa, Produto produto, Double ultimoCusto, Double quantidade, EntityManager manager) throws Exception
	{
		boolean conexaoPropria = false;
		StringBuffer str = new StringBuffer();
		Double saldoAtual;
		Double somaCustoProduto;
		Double novoCusto = null;
		
		if (empresa.getId()==null || empresa.getId()==0 || produto.getId()==null || produto.getId()==0)
		{
			throw new Exception("Produto e empresa precisam ser informados para usar essa função.");
		}
		
		try
		{
			saldoAtual = 0.0;
			somaCustoProduto = 0.0;
			
			if (manager==null)
			{
				manager = ConexaoUtil.getEntityManager();
				conexaoPropria = true;
			}
			
			if (empresa.getAnoAberto()==null || empresa.getMesAberto()==null)
			{
				empresa = new EmpresaFisicaService().recuperarPorId(manager, empresa.getId());
			}
			
			str.append("SELECT sum(saldo.custo*saldo.saldo) as somaCustoProduto, SUM(saldo.saldo) AS saldoProduto ");
			str.append("FROM  saldoestoqueproduto saldo ");
			str.append("INNER JOIN estoqueproduto estoque ON saldo.idEstoqueProduto = estoque.idEstoqueProduto ");
			str.append("INNER JOIN tamanho tam ON estoque.idTamanho = tam.idTamanho ");
			str.append("INNER JOIN empresafisica empresa ON empresa.idEmpresaFisica = saldo.idEmpresaFisica ");
			str.append("INNER JOIN cor cor on estoque.idCor = cor.idCor ");
			str.append("INNER JOIN produto prod on estoque.idProduto = prod.idProduto ");
			str.append("INNER JOIN ordemproduto ordem on ordem.idOrdemProduto = estoque.idOrdemProduto ");
			str.append("WHERE prod.idProduto = " + produto.getId() + " ");
			str.append("and saldo.idEmpresaFisica = " + empresa.getId() + " ");
			str.append("AND saldo.mes = " + empresa.getMesAberto() + " ");
			str.append("AND saldo.ano = " + empresa.getAnoAberto() + " ");
			
			str.append("GROUP BY empresa.idEmpresaFisica, prod.idProduto ");
			str.append("order by  empresa.idEmpresaFisica, prod.idProduto ");
			Query query = manager.createNativeQuery(str.toString());
			

			try
			{
				Object[] resultados = (Object[]) query.getSingleResult();
	
				if (resultados!=null && resultados.length>0)
				{
					saldoAtual = Double.parseDouble(resultados[1].toString());
					somaCustoProduto = Double.parseDouble(resultados[0].toString());
					novoCusto = ((somaCustoProduto) + (ultimoCusto*quantidade))/(saldoAtual+quantidade);
				}
				else
				{
					novoCusto = ultimoCusto;
				}
			}
			catch(Exception ex)
			{
				novoCusto = ultimoCusto;
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			if (conexaoPropria==true)
				manager.close();
		}
		return Double.isNaN(novoCusto) || Double.isInfinite(novoCusto) ? 0 : novoCusto;
	}	
	
	public void atualizarCustoProduto(EntityManager manager, EmpresaFisica empresa, Produto produto, Double novoCusto, Double custoMedio, Double ultimoCusto, Double novoValorVenda) throws Exception
	{
		boolean managerIndependente = false;
		
		if (empresa == null || empresa.getId() == null || empresa.getId() == 0x00)
			throw new NullPointerException("Empresa cannot be null");
		if (produto == null || produto.getId() == null || produto.getId() == 0x00)
			throw new NullPointerException("Produto cannot be null");
		
		try
		{
			if ((managerIndependente = (manager == null)))
			{
				manager = ConexaoUtil.getEntityManager();
				manager.getTransaction().begin();
			}
			
			if (empresa.getAnoAberto() == null || empresa.getMesAberto() == null)
				empresa = new EmpresaFisicaService().recuperarPorId(manager, empresa.getId());
		
			final StringBuffer str = new StringBuffer();
			str.append(" UPDATE `saldoEstoqueProduto` SET `custo` = " + novoCusto + ", ");
			str.append(" `custoMedio` = " + custoMedio + ", ");
			str.append(" `ultimoCusto` = " + ultimoCusto + ", ");
			str.append(" `venda` = " + novoValorVenda + " ");
			str.append(" WHERE `idEstoqueProduto` in "); 
			str.append(" (SELECT `idEstoqueProduto` FROM `estoqueProduto` WHERE `idProduto` = " + produto.getId() + ") "); 
			str.append(" AND `idEmpresaFisica` = " + empresa.getId() + " ");
			str.append(" AND `ano` = " + empresa.getAnoAberto() + " AND `mes` = " + empresa.getMesAberto() + " ");			

			manager.createNativeQuery(str.toString()).executeUpdate();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			
			if(managerIndependente)
				manager.getTransaction().rollback();
			
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
	
	public Double recuperarCustoPorGrupoEmpresa(String grupoEmpresa,String referencia,Integer mesAberto,Integer anoAberto) throws Exception
	{
		try
		{
			String str = "";
			str = str + " select s.custo as custo from saldoestoqueproduto s "; 
			str = str + " inner join estoqueproduto p on p.idEstoqueProduto = s.idEstoqueProduto "; 
			str = str + " where s.idEmpresaFisica in("+grupoEmpresa+")"; 
			str = str + " and p.idProduto = ".concat(referencia);
			
			if(mesAberto!=null && mesAberto>0)
				str += " and s.mes = ".concat(String.valueOf(mesAberto));
			if(anoAberto!=null && anoAberto>0)
				str += " and s.ano = ".concat(String.valueOf(anoAberto)); 
			
			str += " order by s.idSaldoEstoqueProduto desc limit 1";
				
			Connection conn; 

			conn = ConexaoUtil.getConexaoPadrao();
			java.sql.Statement rec = conn.createStatement();    
			ResultSet rs = rec.executeQuery(str);
			Double custo = null;
			
			if(rs.next())
				custo = rs.getDouble("custo");

			rs.close();
			rec.close();
			conn.close();
			return custo;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}
	}

	public Double recuperarSaldoPorEmpresa(EntityManager manager, Integer idEmpresaFisica, String referencia)
	{
		Boolean managerIndependente = false;
		
		try
		{
			if((managerIndependente = (manager == null)))
				manager = ConexaoUtil.getEntityManager();
			
			String query = "SELECT COALESCE(SUM(saldo), 0) FROM saldoEstoqueProduto ";
		    query += " INNER JOIN empresaFisica ON empresaFisica.idEmpresaFisica = saldoEstoqueProduto.idEmpresaFisica ";
		    query += " AND saldoEstoqueProduto.mes = empresaFisica.mesAberto ";
		    query += " AND saldoEstoqueProduto.ano = empresaFisica.anoAberto ";
		    query += " INNER JOIN estoqueProduto ON estoqueProduto.idEstoqueProduto = saldoEstoqueProduto.idEstoqueProduto ";
		    query += " WHERE estoqueProduto.idProduto = ".concat(referencia).concat(" AND empresaFisica.idEmpresaFisica = ".concat(String.valueOf(idEmpresaFisica)).concat(";"));
		    
			return ((Double) manager.createNativeQuery(query).getSingleResult());
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
}