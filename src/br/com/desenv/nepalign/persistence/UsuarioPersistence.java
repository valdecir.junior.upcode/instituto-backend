package br.com.desenv.nepalign.persistence;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class UsuarioPersistence extends GenericPersistenceIGN<Usuario>
{
	public byte[] recuperarPreferencias(Usuario usuario) throws Exception
	{
		Connection connection = null;
		
		try
		{
			if(usuario == null || usuario.getId() == null)
				return new byte[0x0];
			
			connection = ConexaoUtil.getConexaoPadrao();
			
			ResultSet resultSet = connection.createStatement().executeQuery("SELECT preferencias FROM usuario WHERE idUsuario = " + usuario.getId().toString());
			
			resultSet.first();
			
			Blob blob = resultSet.getBlob(1);
			
			byte[] preferencias = blob.getBytes(1, (int) blob.length());
			
			if(preferencias == null)
				preferencias = new byte[0x0];
			
			return preferencias;
		}
		catch(Exception ex)
		{
			return new byte[0x0];
		}
		finally
		{
			if(connection != null)
			{
				connection.close();
				connection = null;	
			}
		}
	}
}