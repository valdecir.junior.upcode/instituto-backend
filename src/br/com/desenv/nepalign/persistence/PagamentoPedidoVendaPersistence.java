package br.com.desenv.nepalign.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.CaixaDia;
import br.com.desenv.nepalign.model.PagamentoPedidoVenda;
import br.com.desenv.nepalign.service.PagamentoPedidoVendaService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class PagamentoPedidoVendaPersistence extends GenericPersistenceIGN<PagamentoPedidoVenda>
{
public List<PagamentoPedidoVenda> recuperarPedidoVendaSemChequeOuCartao(CaixaDia caixa) throws Exception
{
	String str = "";
	str = str + " SELECT pag.idPagamentoPedidoVenda FROM pagamentopedidovenda pag "; 
	str = str + " INNER JOIN formapagamento forma on pag.idFormaPagamento = forma.idFormaPagamento "; 
	str = str + " WHERE (pag.idFormaPagamento = " +DsvConstante.getParametrosSistema().getIdFormaPagamentoChequePre()+
			" or pag.idFormaPagamento = "+DsvConstante.getParametrosSistema().getIdFormapagamentoCheque()+
			" or pag.idFormaPagamento = "+DsvConstante.getParametrosSistema().get("idFormaPagamentoPromissoria")+
			") and pag.idChequeRecebido is null  "; 
	str = str + " and pag.idCaixaDia = " + caixa.getId();
	Connection conn; 
    
    conn = ConexaoUtil.getConexaoPadrao();
	 java.sql.Statement rec = conn.createStatement();    
    ResultSet rs = rec.executeQuery(str.toString()); 
    List<Integer> listaIds = new ArrayList<>();
   while(rs.next())
   {
   listaIds.add(rs.getInt("idPagamentoPedidoVenda"));	
   }
   List<PagamentoPedidoVenda> listapag = new ArrayList<>();
   for (int i = 0; i < listaIds.size(); i++)
   {
	listapag.add(new PagamentoPedidoVendaService().recuperarPorId(listaIds.get(i)));
   }
   return listapag;
}
}


