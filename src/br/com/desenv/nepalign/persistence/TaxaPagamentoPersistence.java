package br.com.desenv.nepalign.persistence;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.TaxaPagamento;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.FormaPagamento;
import br.com.desenv.nepalign.model.TaxaPagamento;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;
public class TaxaPagamentoPersistence extends GenericPersistenceIGN<TaxaPagamento>
{
	 public TaxaPagamento buscarTaxaPorFormaDePagamento(EmpresaFisica empresa,FormaPagamento forma,Integer vezes) throws Exception
	 {
		 return this.buscarTaxaPorFormaDePagamento(empresa, forma, vezes, null, null);
	 }
	 public TaxaPagamento buscarTaxaPorFormaDePagamento(EmpresaFisica empresa,FormaPagamento forma,Integer vezes,EntityManager manager,	EntityTransaction transaction ) throws Exception
	 {
		
			Boolean transacaoIndependente = false;
			 TaxaPagamento taxa = new TaxaPagamento();
		 try
		 {
			 if(manager == null || transaction == null)
				{
					manager = ConexaoUtil.getEntityManager();
					transaction = manager.getTransaction();
					transaction.begin();
					transacaoIndependente = true;
				}
			
		 String str = "";
		 str = str + " SELECT taxa.idTaxaPagamento as 'id' FROM taxapagamento taxa "; 
		 str = str + " INNER JOIN formapagamento forma on taxa.idformapagamento = forma.idformapagamento "; 
		 str = str + " where taxa.parcelaAte >=  "+vezes; 
		 
		 str = str + " and forma.tipo = " +forma.getTipo()+
		 		"  and taxa.idFormaPagamento = " + forma.getId();
		 str= str + " and taxa.idEmpresaFisica = " + empresa.getId();
		 str = str + " order by taxa.parcelaAte "; 
	     Connection conn; 
	     
	     conn = ConexaoUtil.getConexaoPadrao();
		 java.sql.Statement rec = conn.createStatement();    
	     ResultSet rs = rec.executeQuery(str);
	     Integer id = 0;
	     if(rs.next()==true)
	     {
	    	 id = rs.getInt("id");
	     }
	     rs.close();
	     rec.close();
	     conn.close();
	     taxa = this.recuperarPorId(id);
	     if(taxa==null)
	     {
	    	throw new Exception("não foi possivel achar a taxa de pagamento cartao!"); 
	     }
	     if(transacaoIndependente)
				transaction.commit();
		 }
		catch(Exception ex)
		{
			ex.printStackTrace();
			if(transacaoIndependente)
				transaction.rollback();


			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
		}
		 
		
		return taxa;
	 }
}


