package br.com.desenv.nepalign.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.DocumentoEntradaSaida;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.FornecedorService;
import br.com.desenv.nepalign.service.PedidoCompraVendaService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class DocumentoEntradaSaidaPersistence extends GenericPersistenceIGN<DocumentoEntradaSaida>
{

	public List<DocumentoEntradaSaida> listarPorObjetoFiltroProdutoSql(EntityManager manager, Produto produto, EmpresaFisica empresaFisica, String dataBase) throws Exception
	{
		Boolean managerIndependente = false;
		List<DocumentoEntradaSaida> listaDocumentoEntradaSaida = new ArrayList<DocumentoEntradaSaida>();
		Connection connection = null;
		try
		{
			if(manager == null)
				manager = ConexaoUtil.getEntityManager();
			
			connection = ConexaoUtil.getConexaoPadrao();
			
			
			StringBuilder query = new StringBuilder();
			
			query.append("select doc.* from documentoentradasaida doc where idDocumentoEntradaSaida in ");
			query.append("(select iddocumentoentradasaida from itemdocumentoentradasaida where idproduto=" + produto.getId() + ")" );
			query.append("and dataEntradaSaida >= '"+ new SimpleDateFormat("yyyy-mm-dd").format(new SimpleDateFormat("dd/mm/yyyy").parse(dataBase))+" 00:00:00'");
			
			if(empresaFisica != null)
				query.append("and idEmpresaFisica = " + empresaFisica.getId());
			
			ResultSet rs = connection.createStatement().executeQuery(String.valueOf(query));

			while(rs.next())
			{
				DocumentoEntradaSaida documentoEntradaSaidaBuffer = new DocumentoEntradaSaida();
				documentoEntradaSaidaBuffer.setId(rs.getInt("idDocumentoEntradaSaida"));
				documentoEntradaSaidaBuffer.setTipo(rs.getString("tipo"));
				documentoEntradaSaidaBuffer.setEmpresaFisica(new EmpresaFisicaService().recuperarPorId((Integer)rs.getInt("idEmpresaFisica")));
				documentoEntradaSaidaBuffer.setFornecedor(new FornecedorService().recuperarPorId((Integer)rs.getInt("idFornecedor")));
				documentoEntradaSaidaBuffer.setPedidoCompraVenda(new PedidoCompraVendaService().recuperarPorId((Integer)rs.getInt("idPedidoCompraVenda")));
				documentoEntradaSaidaBuffer.setNumeroDocumentoEntradaSaida(rs.getString("numeroDocumentoEntradaSaida"));
				documentoEntradaSaidaBuffer.setDataEmissao(rs.getDate("dataEmissao"));
				documentoEntradaSaidaBuffer.setDataEntrada(rs.getDate("dataEntradaSaida"));
				documentoEntradaSaidaBuffer.setNumeroNotaFiscal(rs.getString("numeroNotaFiscal"));
				documentoEntradaSaidaBuffer.setValorTotalNotaFiscal(rs.getDouble("valorTotalNotaFiscal"));
				documentoEntradaSaidaBuffer.setSerie(rs.getInt("serie"));
				documentoEntradaSaidaBuffer.setValorTotal(rs.getDouble("valorTotal"));
				documentoEntradaSaidaBuffer.setQuantidadeTotal(rs.getInt("quantidadeTotal"));
				documentoEntradaSaidaBuffer.setSituacao(rs.getString("situacao"));
				documentoEntradaSaidaBuffer.setObservacao(rs.getString("observacao"));
				
				listaDocumentoEntradaSaida.add(documentoEntradaSaidaBuffer);
			}
			
			return listaDocumentoEntradaSaida;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
			connection.close();
			listaDocumentoEntradaSaida = null;
			System.gc();
		}
	}
}


