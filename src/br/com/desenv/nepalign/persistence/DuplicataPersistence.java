package br.com.desenv.nepalign.persistence;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.Duplicata;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class DuplicataPersistence extends GenericPersistenceIGN<Duplicata>
{
 public Long buscarNumeroDuplicata()
 { 
	 EntityManager manager = ConexaoUtil.getEntityManager();
	  StringBuffer sql = new StringBuffer("select max(numeroDuplicata) from Duplicata ");
	  Query query = manager.createQuery(sql.toString());
	  Long maiorNumeroDuplicata =  (Long) query.getSingleResult();	  
	  if (maiorNumeroDuplicata == null)
	  {
		  maiorNumeroDuplicata = (long) 1;
	  }
	  else
	  {
		  maiorNumeroDuplicata++;
	  }	  
	  manager.close();
	  return maiorNumeroDuplicata;
  }
 
}


