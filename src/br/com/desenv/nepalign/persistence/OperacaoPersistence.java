package br.com.desenv.nepalign.persistence;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.DsvException;
import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.Operacao;
import br.com.desenv.nepalign.service.OperacaoService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class OperacaoPersistence extends GenericPersistenceIGN<Operacao>
{
	@Override
	public Operacao salvar(Operacao entidade) throws DsvException
	{
				
		EntityManager manager = ConexaoUtil.getEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		
		
		Operacao entidadeSalva = null;

		try
		{
			transaction.begin();
			entidadeSalva = salvar(manager, transaction, entidade);
			transaction.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			transaction.rollback();
			throw new DsvException(e);
		}
		finally
		{
			manager.close();
		}
		
		return entidadeSalva;
	}
	
	@Override
	public Operacao salvar(EntityManager manager, EntityTransaction transaction, Operacao entidade) throws Exception
	{
		
		if (transaction == null || manager == null)
		{
			throw new Exception("Variáveis manager e transaction devem ser passadas");
		}

		if (transaction.isActive() == false)
		{
			throw new Exception("Transação deve ser iniciada fora deste método");
		}

		entidade.setId(null);
		manager.persist(entidade);
		
		return entidade;
	}
	
	@Override
	public Operacao atualizar(Operacao entidade) throws Exception
	{
		
		
		EntityManager manager = ConexaoUtil.getEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		Operacao entidadeAtualizada = null;
		try
		{
			transaction.begin();
			entidadeAtualizada = atualizar(manager, transaction, entidade);
			transaction.commit();
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			transaction.rollback();
			throw e;
		}
		finally
		{
			manager.close();
		}
		
		return entidadeAtualizada;
	}
	
	@Override
	public Operacao atualizar(EntityManager manager, EntityTransaction transaction, Operacao entidade) throws Exception
	{
		
		
		
		if (transaction == null || manager == null)
		{
			throw new Exception("Variáveis manager e transaction devem ser passadas");
		}

		if (transaction.isActive() == false)
		{
			throw new Exception("Transação deve ser iniciada fora deste método");
		}

		manager.merge(entidade);
		
		return entidade;
	}
	
	@Override
	public void excluir(Operacao entidade) throws Exception
	{
		
		EntityManager manager = ConexaoUtil.getEntityManager();
		EntityTransaction transaction = manager.getTransaction();

		try
		{
			transaction.begin();
			excluir(manager, transaction, entidade);
			transaction.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			transaction.rollback();
			throw e;
		}
		finally
		{
			manager.close();
		}
	}
	
	@Override
	public void excluir(EntityManager manager, EntityTransaction transaction, Operacao entidade) throws Exception
	{
				
		if (transaction == null || manager == null)
		{
			throw new Exception("Variáveis manager e transaction devem ser passadas");
		}

		if (transaction.isActive() == false)
		{
			throw new Exception("Transação deve ser iniciada fora deste método");
		}

		manager.remove(entidade); 
	}
}


