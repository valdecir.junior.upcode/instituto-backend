package br.com.desenv.nepalign.persistence;

import java.util.Date;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.BaixaLancamentoContaReceber;
import br.com.desenv.nepalign.model.LancamentoContaReceber;

public class BaixaLancamentoContaReceberPersistence extends GenericPersistenceIGN<BaixaLancamentoContaReceber>
{
	public BaixaLancamentoContaReceberPersistence()
	{
		super();
	}
	
	// TODO: criar com filtro de databaixa
	public Double getValorTotalBaixa(final EntityManager manager, final LancamentoContaReceber lancamentoContaReceber) throws Exception
	{
		String query = "SELECT IFNULL(SUM(`baixaLancamentoContaReceber`.`valor`), 0) FROM `baixaLancamentoContaReceber` WHERE `baixaLancamentoContaReceber`.`idLancamentoContaReceber` = " + lancamentoContaReceber.getId().intValue() + ";";
		
		return (Double) manager.createNativeQuery(query).getResultList().get(0x00);
	}
	
	public Date getDataUltimaBaixa(final EntityManager manager, final LancamentoContaReceber lancamentoContaReceber) throws Exception
	{
		String query = "SELECT `baixaLancamentoContaReceber`.`dataBaixa` FROM `baixaLancamentoContaReceber` WHERE `baixaLancamentoContaReceber`.`idLancamentoContaReceber` = " + lancamentoContaReceber.getId().intValue() + " ORDER BY `baixaLancamentoContaReceber`.`dataBaixa` DESC;";
		
		return (Date) manager.createNativeQuery(query).getResultList().get(0x00);
	}
}