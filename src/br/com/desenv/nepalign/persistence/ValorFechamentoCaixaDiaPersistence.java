package br.com.desenv.nepalign.persistence;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.lowagie.text.pdf.hyphenation.TernaryTree.Iterator;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.CaixaDia;
import br.com.desenv.nepalign.model.FormaPagamento;
import br.com.desenv.nepalign.model.MovimentoCaixadia;
import br.com.desenv.nepalign.model.ValorFechamentoCaixaDia;
import br.com.desenv.nepalign.service.CaixaDiaService;
import br.com.desenv.nepalign.service.FormaPagamentoService;
import br.com.desenv.nepalign.service.MovimentoCaixadiaService;
import br.com.desenv.nepalign.service.ValorFechamentoCaixaDiaService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ValorFechamentoCaixaDiaPersistence extends GenericPersistenceIGN<ValorFechamentoCaixaDia>
{
	public HashMap<FormaPagamento, Double> getListaMovimentoCaixaDia(int idCaixaDia, int idUsuario) throws SQLException, Exception
	{
		FormaPagamentoService formaPagamentoService = new FormaPagamentoService();
		String str = "";

		str = str + " SELECT formaPagamento.idFormaPagamento as idFormaPagamento,   "; 
		str = str + " COALESCE(ROUND(SUM(if(movimento.creditoDebito='C' or movimento.idFormaPagamento =" + DsvConstante.getParametrosSistema().get("idFormaPagamentoDuplicata") + ",movimento.valor,movimento.valor*(-1))), 2),0) as valor "; 
		str = str + "   FROM movimentoCaixaDia movimento   "; 
		str = str + " LEFT OUTER JOIN duplicataParcela duplicata ON movimento.idDuplicataParcela = duplicata.idDuplicataParcela  "; 
		str = str + "  LEFT OUTER JOIN duplicata duplic ON duplicata.idDuplicata = duplic.idDuplicata   "; 
		str = str + " LEFT OUTER JOIN formaPagamento formaPagamento ON movimento.idFormaPagamento = formaPagamento.idFormaPagamento  "; 
		str = str + "  where idCaixaDia = "+idCaixaDia+" and (duplicata.numeroParcela is null or (duplicata.numeroParcela <> 0 ))   "; 
		str = str + " and if(movimento.creditoDebito = 'D',diversos ='S' or movimento.idformapagamento = " + DsvConstante.getParametrosSistema().get("idFormaPagamentoDuplicata") + ",movimento.idmovimentoCaixaDia is not null) "; 
		str = str + " GROUP BY formaPagamento.idFormaPagamento "; 
		System.out.println(str);
		/*str = str + " SELECT fp.idFormaPagamento, fp.descricao, sum(mcd.valor) as valor from movimentocaixadia mcd "; 
		str = str + " inner join formapagamento fp on mcd.idformapagamento=fp.idformapagamento ";
		str = str + " WHERE mcd.idCaixaDia= "+ idCaixaDia;
		str = str + " AND mcd.idUsuario= "+ idUsuario;
		str = str + " group by fp.idformapagamento ";*/ 

		Connection con;
		con = ConexaoUtil.getConexaoPadrao();

		Statement stm = null;  
		ResultSet rs = null; 
		stm = con.createStatement();

		rs = stm.executeQuery(str);

		HashMap<FormaPagamento, Double> listamovcaixadia = new HashMap<FormaPagamento, Double>();
		if(rs.next())
		{
			while (rs.next())
			{
				listamovcaixadia.put(formaPagamentoService.recuperarPorId(Integer.valueOf(rs.getInt("idFormaPagamento"))), rs.getDouble("valor"));
			}	
		}
		else
		{
			listamovcaixadia.put(formaPagamentoService.recuperarPorId(DsvConstante.getParametrosSistema().getInt("idFormaPagamentoDinheiro")),0.0);
		}
		

		return calcularMovimentacaoCaixaDiaDiversos(listamovcaixadia, idCaixaDia);
	}

	public HashMap<FormaPagamento, Double> calcularMovimentacaoCaixaDiaDiversos( HashMap<FormaPagamento, Double> listamovcaixadia,int idCaixaDia) throws Exception
	{
	
		CaixaDia cx = new CaixaDiaService().recuperarPorId(idCaixaDia);
	
			java.util.Iterator iteratorValorFormaPagamento = listamovcaixadia.entrySet().iterator();
			while(iteratorValorFormaPagamento.hasNext())
			{
				Map.Entry<FormaPagamento, Double> pair = (Map.Entry<FormaPagamento, Double>)iteratorValorFormaPagamento.next();
				
				//Se for formaPagamento igual
				if( pair.getKey().getId() == DsvConstante.getParametrosSistema().getInt("idFormaPagamentoDinheiro"))
				{
					 pair.setValue(pair.getValue()+cx.getValorInicial()) ;
				}
				System.out.println(""+pair.getKey().getDescricao()+" - " + pair.getValue());
			}	
		
		return listamovcaixadia;
	}

	public Double getValorCalculado(Integer idCaixaDia, Integer idFormaPagamento) throws Exception
	{
		String str = "";
		str = str + " SELECT formaPagamento.idFormaPagamento as idFormaPagamento,   "; 
		str = str + " ROUND(SUM(if(movimento.creditoDebito='C' or movimento.idFormaPagamento =" + DsvConstante.getParametrosSistema().get("idFormaPagamentoDuplicata") + ",movimento.valor,movimento.valor*(-1))), 2) as valor "; 
		str = str + " FROM movimentoCaixaDia movimento   "; 
		str = str + " LEFT OUTER JOIN duplicataParcela duplicata ON movimento.idDuplicataParcela = duplicata.idDuplicataParcela  "; 
		str = str + " LEFT OUTER JOIN duplicata duplic ON duplicata.idDuplicata = duplic.idDuplicata   "; 
		str = str + " LEFT OUTER JOIN formaPagamento formaPagamento ON movimento.idFormaPagamento = formaPagamento.idFormaPagamento  "; 
		str = str + " where idCaixaDia = "+idCaixaDia+" and (duplicata.numeroParcela is null or (duplicata.numeroParcela <> 0))   "; 
		str = str + " and if(movimento.creditoDebito = 'D',diversos ='S' or movimento.idformapagamento = " + DsvConstante.getParametrosSistema().get("idFormaPagamentoDuplicata") + ",movimento.idmovimentoCaixaDia is not null) ";
		str = str + " and movimento.idFormaPagamento = "+idFormaPagamento;
		str = str + " GROUP BY formaPagamento.idFormaPagamento ";


		Connection con;
		con = ConexaoUtil.getConexaoPadrao();

		Statement stm = null;  
		ResultSet rs = null; 
		stm = con.createStatement();
		Double valor = 0.0;

		rs = stm.executeQuery(str);
		//rs.next();
		if(rs.next())
		{
			if(rs.getInt("idFormaPagamento") == DsvConstante.getParametrosSistema().getInt("idFormaPagamentoDinheiro"))
			{
				valor = rs.getDouble("valor")+new CaixaDiaService().recuperarPorId(idCaixaDia).getValorInicial();
				valor = new BigDecimal(valor).setScale(2,BigDecimal.ROUND_HALF_EVEN).doubleValue();
				return valor;
			}
			else
				return rs.getDouble("valor");
		}
		else
			return valor + new CaixaDiaService().recuperarPorId(idCaixaDia).getValorInicial();


	}

	public Integer getQuantidadeValores(ResultSet rs) throws Exception
	{
		Integer count = 0;
		while(rs.next())
		{
			count++;
		}
		rs.beforeFirst();
		return count;
	}

	public  List<ValorFechamentoCaixaDia> listaMovimentoCaixaDia(int idCaixaDia) throws SQLException, Exception
	{
		return this.listaMovimentoCaixaDia(idCaixaDia, null, null);
	}
	public  List<ValorFechamentoCaixaDia> listaMovimentoCaixaDia(int idCaixaDia, EntityManager manager, EntityTransaction transaction) throws SQLException, Exception
	{
		Boolean transacaoIndependente = false;

		try
		{


			if(manager == null || transaction == null)
			{
				manager = ConexaoUtil.getEntityManager();
				transaction = manager.getTransaction();
				transaction.begin();
				transacaoIndependente = true;
			}
	
			FormaPagamentoService formaPagamentoService = new FormaPagamentoService();
			String str = "";
	
			str = str + " SELECT formaPagamento.idFormaPagamento as idFormaPagamento,   "; 
			str = str + " ROUND(SUM(if(movimento.creditoDebito='C' or movimento.idFormaPagamento =" + DsvConstante.getParametrosSistema().get("idFormaPagamentoDuplicata") + ",movimento.valor,movimento.valor*(-1))), 2) as valor "; 
			str = str + "   FROM movimentoCaixaDia movimento   "; 
			str = str + " LEFT OUTER JOIN duplicataParcela duplicata ON movimento.idDuplicataParcela = duplicata.idDuplicataParcela  "; 
			str = str + "  LEFT OUTER JOIN duplicata duplic ON duplicata.idDuplicata = duplic.idDuplicata   "; 
			str = str + " LEFT OUTER JOIN formaPagamento formaPagamento ON movimento.idFormaPagamento = formaPagamento.idFormaPagamento  "; 
			str = str + "  where idCaixaDia = "+idCaixaDia+" and (duplicata.numeroParcela is null or (duplicata.numeroParcela <> 0 ))   "; 
			str = str + " and if(movimento.creditoDebito = 'D',diversos ='S' or movimento.idformapagamento = " + DsvConstante.getParametrosSistema().get("idFormaPagamentoDuplicata") + ",movimento.idmovimentoCaixaDia is not null) "; 
			str = str + " GROUP BY formaPagamento.idFormaPagamento "; 
			System.out.println(str);
			/*str = str + " SELECT fp.idFormaPagamento, fp.descricao, sum(mcd.valor) as valor from movimentocaixadia mcd "; 
			str = str + " inner join formapagamento fp on mcd.idformapagamento=fp.idformapagamento ";
			str = str + " WHERE mcd.idCaixaDia= "+ idCaixaDia;
			str = str + " group by fp.idformapagamento ";*/ 
	
			Connection con;
			con = ConexaoUtil.getConexaoPadrao();
	
			Statement stm = null;  
			ResultSet rs = null; 
			stm = con.createStatement();
			Double valorInicial = 0.0;
	
			try{
				valorInicial = new CaixaDiaService().recuperarPorId(manager,idCaixaDia).getValorInicial();
			}
			catch (Exception e){
				throw new Exception("Não foi possível recuperar VALOR INICIAL do Caixa: "+idCaixaDia) ;
			}
			rs = stm.executeQuery(str);
	
			List<ValorFechamentoCaixaDia> listaValorFechamentoCaixaDia = new ArrayList<ValorFechamentoCaixaDia>();
			if(rs.next())
			{
				while (rs.next())
				{
					ValorFechamentoCaixaDia valorFechamentoCaixaDia = new ValorFechamentoCaixaDia();
					valorFechamentoCaixaDia.setFormaPagamento(formaPagamentoService.recuperarPorId(manager,Integer.valueOf(rs.getInt("idFormaPagamento"))));
					valorFechamentoCaixaDia.setCaixadia(new CaixaDiaService().recuperarPorId(manager,idCaixaDia));
					if(valorFechamentoCaixaDia.getFormaPagamento().getId() == DsvConstante.getParametrosSistema().getInt("idFormaPagamentoDinheiro"))
						valorFechamentoCaixaDia.setValorCalculado(rs.getDouble("valor")+valorInicial);
					else
						valorFechamentoCaixaDia.setValorCalculado(rs.getDouble("valor"));
					valorFechamentoCaixaDia.setValorInformado(0.0);
	
					listaValorFechamentoCaixaDia.add(valorFechamentoCaixaDia);
				}
			}
			else
			{
				ValorFechamentoCaixaDia valorFechamentoCaixaDiaRecuperado = new ValorFechamentoCaixaDia();
				
				ValorFechamentoCaixaDia valorFechamentoCaixaDia = new ValorFechamentoCaixaDia();
				valorFechamentoCaixaDia.setFormaPagamento(formaPagamentoService.recuperarPorId(manager,DsvConstante.getParametrosSistema().getInt("idFormaPagamentoDinheiro")));
				valorFechamentoCaixaDia.setCaixadia(new CaixaDiaService().recuperarPorId(idCaixaDia));
				List<ValorFechamentoCaixaDia> listaValorFechamento = new ValorFechamentoCaixaDiaService().listarPorObjetoFiltro(manager, valorFechamentoCaixaDia, "idValorFechamentoCaixaDia", "ASC", Integer.MAX_VALUE);
				if(listaValorFechamento.size() > 0)
				{
					valorFechamentoCaixaDiaRecuperado = listaValorFechamento.get(0);
					valorFechamentoCaixaDiaRecuperado.setValorCalculado(valorInicial);
				}
				else
				{
					valorFechamentoCaixaDiaRecuperado.setCaixadia(new CaixaDiaService().recuperarPorId(manager,idCaixaDia));
					valorFechamentoCaixaDiaRecuperado.setFormaPagamento(formaPagamentoService.recuperarPorId(manager,DsvConstante.getParametrosSistema().getInt("idFormaPagamentoDinheiro")));
					valorFechamentoCaixaDiaRecuperado.setValorCalculado(valorInicial);	
					valorFechamentoCaixaDiaRecuperado.setDataFechamento(new Date());
					//this.salvar(manager,transaction,valorFechamentoCaixaDiaRecuperado);
				}
				listaValorFechamentoCaixaDia.add(valorFechamentoCaixaDiaRecuperado);
			
			}
			if(transacaoIndependente)
				transaction.commit();
			
			return listaValorFechamentoCaixaDia;
		}
		catch(Exception ex)
		{
			if(transacaoIndependente)
				transaction.rollback();
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(transacaoIndependente)
				manager.close();
	
		}
	}

}