package br.com.desenv.nepalign.persistence;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.frameworkignorante.Page;
import br.com.desenv.nepalign.model.BandeiraCartao;
import br.com.desenv.nepalign.model.EmpresaFinanceiro;
import br.com.desenv.nepalign.model.LancamentoContaReceber;
import br.com.desenv.nepalign.service.LogOperacaoService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class LancamentoContaReceberPersistence extends GenericPersistenceIGN<LancamentoContaReceber>
{
	private static final LogOperacaoService logOperacaoService = new LogOperacaoService();
	
	public LancamentoContaReceberPersistence()
	{
		super();
	}
	
	@Override
	public LancamentoContaReceber salvar(EntityManager manager, EntityTransaction transaction, LancamentoContaReceber entidade, boolean logar) throws Exception 
	{ 
		logOperacaoService.gravarOperacaoInclusao(manager, transaction, entidade);
		
		return super.salvar(manager, transaction, entidade, logar);
	}
	
	@Override
	public LancamentoContaReceber atualizar(EntityManager manager, EntityTransaction transaction, LancamentoContaReceber entidade, boolean logar) throws Exception 
	{
		logOperacaoService.gravarOperacaoAlterar(manager, transaction, entidade);
		
		return super.atualizar(manager, transaction, entidade, logar);
	}
	
	@Override
	public void excluir(EntityManager manager, EntityTransaction transaction, LancamentoContaReceber entidade, boolean logar) throws Exception 
	{
		logOperacaoService.gravarOperacaoExclusao(manager, transaction, entidade);
		
		super.excluir(manager, transaction, entidade, logar);
	}
	
	/**
	 * 
	 * @param manager
	 * @param dataVencimentoInicial
	 * @param dataVencimentoFinal
	 * @param dataEmissaoFinal
	 * @param situacaoLancamento String usada direto na Sql Query (1,2,3)
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<LancamentoContaReceber> listarLancamentosPeriodo(final EntityManager manager, final EmpresaFinanceiro empresaFinanceiro, final BandeiraCartao bandeiraCartao, final Date dataVencimentoInicial, final Date dataVencimentoFinal, final Date dataEmissaoFinal, final String situacaoLancamento) throws Exception
	{
		String query = " SELECT * FROM `lancamentoContaReceber` WHERE ".concat(" DATE(`lancamentoContaReceber`.`dataLancamento`) <= '" + IgnUtil.dateFormatSql.format(dataEmissaoFinal) + "' AND ")
								.concat("`lancamentoContaReceber`.`dataVencimento` BETWEEN '" + IgnUtil.dateFormatSql.format(dataVencimentoInicial) + " 00:00:00' AND '" + IgnUtil.dateFormatSql.format(dataVencimentoFinal) + " 23:59:59' ");
		
		if(situacaoLancamento != null && !situacaoLancamento.equals(""))
			query += " AND `lancamentoContaReceber`.`idSituacaoLancamento` IN (" +  situacaoLancamento + ") ";
		if(empresaFinanceiro != null)
			query += " AND `lancamentoContaReceber`.`idEmpresaFinanceiro` = " + empresaFinanceiro.getId().intValue();
		if(bandeiraCartao != null)
			query += " AND `lancamentoContaReceber`.`idBandeiraCartao` = " + bandeiraCartao.getId().intValue();
		
		return (List<LancamentoContaReceber>) manager.createNativeQuery(query, LancamentoContaReceber.class).getResultList();
	}
	
	public Double getValorPeriodo(final EntityManager manager, final EmpresaFinanceiro empresaFinanceiro, final BandeiraCartao bandeiraCartao, final Date dataVencimentoInicial, final Date dataVencimentoFinal, final Date dataEmissaoFinal, final String situacaoLancamento) throws Exception
	{
		String query = " SELECT ROUND(SUM(`lancamentoContaReceber`.`valorLancamento`) - SUM(COALESCE(`baixaLancamentoContaReceber`.`valor`, 0)), 2) FROM ";
		query = query + "     `lancamentoContaReceber` ";
		query = query + "         LEFT OUTER JOIN ";
		query = query + "     `baixaLancamentoContaReceber` ON `baixaLancamentoContaReceber`.`idLancamentoContaReceber` = `lancamentoContaReceber`.`idLancamentoContaReceber` ";
		//query = query + " WHERE "; 
		query = query + " WHERE ".concat(" DATE(`lancamentoContaReceber`.`dataLancamento`) <= '" + IgnUtil.dateFormatSql.format(dataEmissaoFinal) + "' AND ");
		query = query + "     `lancamentoContaReceber`.`dataVencimento` BETWEEN '" + IgnUtil.dateFormatSql.format(dataVencimentoInicial) + " 00:00:00' AND '" + IgnUtil.dateFormatSql.format(dataVencimentoFinal) + " 23:59:59' ";
		
		
		if(situacaoLancamento != null && !situacaoLancamento.equals(""))
			query += " AND `lancamentoContaReceber`.`idSituacaoLancamento` IN (" +  situacaoLancamento + ") ";
		if(empresaFinanceiro != null)
			query += " AND `lancamentoContaReceber`.`idEmpresaFinanceiro` = " + empresaFinanceiro.getId().intValue();
		if(bandeiraCartao != null)
			query += " AND `lancamentoContaReceber`.`idBandeiraCartao` = " + bandeiraCartao.getId().intValue();
		
		return (Double) manager.createNativeQuery(query).getSingleResult();
	}
	
	public List<LancamentoContaReceber> listarPorObjetoFiltro(final LancamentoContaReceber entidade, final Date dataBaixa) throws Exception
	{
		return listarPorObjetoFiltro(null, entidade, dataBaixa);
	}
	
	public List<LancamentoContaReceber> listarPorObjetoFiltro(EntityManager manager, final LancamentoContaReceber entidade, final Date dataBaixa) throws Exception
	{
		Boolean managerIndependente = false;
		
		try
		{
			if((managerIndependente = (manager == null)))
				manager = ConexaoUtil.getEntityManager();
			
			if(dataBaixa != null)
			{
				return super.listarPaginadoPorObjetoFiltro(Integer.MAX_VALUE, 0x01, entidade, null, null, " dataQuitacao BETWEEN '" + IgnUtil.dateFormatSql.format(dataBaixa) + " 00:00:00' AND '" + 
						IgnUtil.dateFormatSql.format(dataBaixa) + " 23:59:59'").getRecordList();	
			}
			else
				return super.listarPorObjetoFiltro(manager, entidade, null, null, null);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}

	public Page<LancamentoContaReceber> listarPaginadoPorObjetoFiltro(int tamanhoPagina, int numeroPagina, LancamentoContaReceber entidadeFiltro, Date dataBaixa) throws Exception 
	{
		if(dataBaixa != null)
		{
			return super.listarPaginadoPorObjetoFiltro(tamanhoPagina, numeroPagina, entidadeFiltro, null, null, " dataQuitacao BETWEEN '" + IgnUtil.dateFormatSql.format(dataBaixa) + " 00:00:00' AND '" + 
					IgnUtil.dateFormatSql.format(dataBaixa) + " 23:59:59'");	
		}
		else
			return super.listarPaginadoPorObjetoFiltro(tamanhoPagina, numeroPagina, entidadeFiltro);
	}
}