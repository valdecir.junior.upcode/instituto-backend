package br.com.desenv.nepalign.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.kscbrasil.movimentoCaixaDia.model.MovimentoCaixaDia;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.CaixaDia;
import br.com.desenv.nepalign.model.ChequeRecebido;
import br.com.desenv.nepalign.model.Cliente;
import br.com.desenv.nepalign.model.Duplicata;
import br.com.desenv.nepalign.model.DuplicataParcela;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.MovimentoCaixadia;
import br.com.desenv.nepalign.model.MovimentoParcelaDuplicata;
import br.com.desenv.nepalign.service.ChequeRecebidoService;
import br.com.desenv.nepalign.service.DuplicataService;
import br.com.desenv.nepalign.service.MovimentoParcelaDuplicataService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class DuplicataParcelaPersistence extends GenericPersistenceIGN<DuplicataParcela>
{
	public List<Integer> listarPorSituacoesPersistence(String[] situacoes,Cliente cli,Boolean porDuplicata,Duplicata dupli) throws SQLException
	{
		String situacao = new String();
		for (int i = 0; i < situacoes.length; i++)
		{
			if(i==0){
				situacao ="" +situacoes[i];
			}
			else
			{
				situacao = 	situacao +","+situacoes[i];

			}
		}
		String sql = "";
		sql = sql + " SELECT parcela.idDuplicataParcela FROM DuplicataParcela parcela "; 
		sql = sql + "  ";
		if(porDuplicata)
		{
			sql = sql + "WHERE parcela.idDuplicata =  " +dupli.getId();
		}
		else
		{
			sql = sql + "WHERE parcela.idCliente =  " +cli.getId();	
		}

		if(situacoes.length>0){
			sql = sql + " AND  parcela.situacao IN ("+situacao+")";
		}
		sql = sql + " order by parcela.numeroDuplicata asc, parcela.dataVencimento ASC";
		List<Integer> listaIds = new ArrayList<>();
		Connection conn; 

		conn = ConexaoUtil.getConexaoPadrao();
		java.sql.Statement rec = conn.createStatement();    
		ResultSet rs = rec.executeQuery(sql.toString()); 
		while(rs.next())
		{
			listaIds.add(rs.getInt("idDuplicataParcela"));	
		}

		return listaIds;

	}
	public List<ChequeRecebido> recuperarDuplicataParcelaSemChequeOuCartao(CaixaDia caixa) throws Exception
	{
		List<DuplicataParcela> lista = new ArrayList<>();
		List<ChequeRecebido> listaMov = new ArrayList<>();
		SimpleDateFormat formatsql = new SimpleDateFormat("yyyy-MM-dd");
		String dataSql = formatsql.format(caixa.getDataAbertura());
		String str = "";
		str = str + " select mov.idChequeRecebido from movimentoParceladuplicata mov "; 
		str = str + " left outer join chequerecebido ch on ch.idChequeRecebido = mov.idChequeRecebido "; 
		str = str + " where ch.consistencia = 'N' "; 
		str = str + "  and mov.idempresafisica =  " +caixa.getEmpresaFisica().getId(); 
		str = str + "  and mov.datamovimento = '"+dataSql+"'"; 
		str = str + " group by mov.idChequeRecebido; "; 
		Connection con = ConexaoUtil.getConexaoPadrao();    
		Statement stm = con.createStatement();
		ResultSet rs = stm.executeQuery(String.valueOf(str));
		while(rs.next())
		{
			listaMov.add(new ChequeRecebidoService().recuperarPorId(rs.getInt("idChequeRecebido")));
		}
		return listaMov;
	}
	public List<DuplicataParcela> retornaDuplicatasAtrazadasPorClienteEDias(Integer dias,Cliente cliente) throws Exception
	{
		String str = "";
		str = str + " SELECT idDuplicataParcela "; 
		str = str + " FROM DuplicataParcela  "; 		
		str = str + " where situacao<> 1 and situacao <> 3 "; 
		if(dias !=null && dias>0)
		{
			str = str + " and DATEDIFF(CURRENT_DATE,dataVencimento)> "+dias; 
		}
		else
		{
			str = str + " and dataVencimento < CURRENT_DATE "; 
		}
		if(cliente !=null)
		{
			str = str + " and idCliente = "+cliente.getId();
		}

		EntityManager manager = ConexaoUtil.getEntityManager();
		Query query = manager.createNativeQuery(str);
		List<DuplicataParcela> lista = new ArrayList<>();
		List<Integer> ids = query.getResultList(); 
		for (int i = 0; i < ids.size(); i++)
		{
			lista.add(this.recuperarPorId((Integer)ids.get(i)));
		}
		return lista;
	}
	public List<DuplicataParcela> retornaDuplicatasAtrazadasAbertasPorClienteEDias(Integer dias,Cliente cliente) throws Exception
	{
		String str = "";
		str = str + " SELECT idDuplicataParcela "; 
		str = str + " FROM DuplicataParcela  "; 		
		str = str + " where situacao = 0 "; 
		if(dias !=null && dias>0)
		{
			str = str + " and DATEDIFF(CURRENT_DATE,dataVencimento)>= "+dias; 
		}

		if(cliente !=null)
		{
			str = str + " and idCliente = "+cliente.getId();
		}

		EntityManager manager = ConexaoUtil.getEntityManager();
		Query query = manager.createNativeQuery(str);
		List<DuplicataParcela> lista = new ArrayList<>();
		List<Integer> ids = query.getResultList(); 
		for (int i = 0; i < ids.size(); i++)
		{
			lista.add(this.recuperarPorId((Integer)ids.get(i)));
		}
		return lista;
	}
	public List<DuplicataParcela> listarDuplicataPorClienteOrdenado(Cliente cliente) throws Exception
	{
		String str = "";
		str = str + " select idduplicataparcela as id from duplicataparcela where idcliente =   "+cliente.getId(); 
		str = str + " order by numeroDuplicata asc, dataVencimento asc "; 
		EntityManager manager = ConexaoUtil.getEntityManager();
		Query query = manager.createNativeQuery(str);
		List<DuplicataParcela> lista = new ArrayList<>();
		List<Integer> ids = query.getResultList(); 
		for (int i = 0; i < ids.size(); i++)
		{
			lista.add(this.recuperarPorId((Integer)ids.get(i)));
		}
		return lista;

	}

	public List<DuplicataParcela> retornarEntradasAcordoEmAberto(Date data,EmpresaFisica empresa) throws Exception
	{
		SimpleDateFormat formatsql = new SimpleDateFormat("yyyy-MM-dd");
		String dataSql = formatsql.format(data);
		String str = "";
		str = str + " select par.idDuplicataParcela as id from duplicataparcela par "; 
		str = str + " inner join duplicata du on du.idDuplicata = par.idDuplicata "; 
		str = str + " where du.dataCompra = '"+dataSql+"'"; 
		str = str + " and par.dataVencimento = '"+dataSql+"'";
		str = str + " and par.numeroparcela = 1  "; 
		str = str + " and du.idEmpresaFisica = "+empresa.getId();
		str = str + " and acordo = 'S' "; 
		str = str + " and situacao <> (1) ";

		Connection conn; 	     
		conn = ConexaoUtil.getConexaoPadrao();
		java.sql.Statement rec = conn.createStatement();    
		ResultSet rs = rec.executeQuery(str);
		List<DuplicataParcela> lista = new ArrayList<>();
		while(rs.next())
		{
			lista.add(this.recuperarPorId(rs.getInt("id")));
		}
		return lista;
	}

}