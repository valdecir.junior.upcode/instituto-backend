package br.com.desenv.nepalign.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.NumeroParcelasPromocional;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class NumeroParcelasPromocionalPersistence extends GenericPersistenceIGN<NumeroParcelasPromocional>
{
public NumeroParcelasPromocional verificarParcelasPromocionais(Date data,EmpresaFisica empresa) throws Exception
{
	
     SimpleDateFormat formatsql = new SimpleDateFormat("yyyy-MM-dd");
     String dataSql = formatsql.format(data);
	 String str = "";
	 str = str + " SELECT * FROM numeroParcelasPromocional "; 
	 str = str + " WHERE dataInicial <= ' "+dataSql+"'"; 
	 str = str + " and dataFinal >=' "+dataSql+"'"; 
	 str = str + " and idEmpresafisica =  " +empresa.getId();
	 
	 NumeroParcelasPromocional num = null;
	 
     Connection conn;      
     conn = ConexaoUtil.getConexaoPadrao();
	 java.sql.Statement rec = conn.createStatement();    
     ResultSet rs = rec.executeQuery(str.toString());
     if(rs.next())
     {
    	 num = this.recuperarPorId(rs.getInt("idnumeroParcelasPromocional"));
     }
     return num;
}
}


