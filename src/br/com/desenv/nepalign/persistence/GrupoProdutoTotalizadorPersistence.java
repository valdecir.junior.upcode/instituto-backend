package br.com.desenv.nepalign.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.GrupoProduto;
import br.com.desenv.nepalign.model.GrupoProdutoTotalizador;
import br.com.desenv.nepalign.model.GrupoTotalizador;

public class GrupoProdutoTotalizadorPersistence extends GenericPersistenceIGN<GrupoProdutoTotalizador>
{
	@SuppressWarnings("unchecked")
	public List<GrupoProduto> listarGrupoProdutoNotIn(EntityManager manager, GrupoTotalizador entidade) throws Exception
	{
		if(entidade == null)
			return new ArrayList<GrupoProduto>();
		
		return (List<GrupoProduto>) manager.createNativeQuery("SELECT * FROM `grupoProduto` WHERE"
				+ " `idGrupoProduto` NOT IN (SELECT `idGrupoProduto` FROM `grupoProdutoTotalizador` WHERE"
				+ " `idGrupoTotalizador` = " + entidade.getId().intValue() + ")", GrupoProduto.class).getResultList();
	}
}