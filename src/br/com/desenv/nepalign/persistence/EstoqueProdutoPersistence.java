package br.com.desenv.nepalign.persistence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import com.sun.corba.se.spi.legacy.connection.GetEndPointInfoAgainException;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.frameworkignorante.SearchParameter;
import br.com.desenv.nepalign.model.Cor;
import br.com.desenv.nepalign.model.CorProduto;
import br.com.desenv.nepalign.model.EstoqueProduto;
import br.com.desenv.nepalign.model.OrdemProduto;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.model.Tamanho;
import br.com.desenv.nepalign.service.CorService;
import br.com.desenv.nepalign.service.OperacaoService;
import br.com.desenv.nepalign.service.OrdemProdutoService;
import br.com.desenv.nepalign.service.ProdutoService;
import br.com.desenv.nepalign.service.TamanhoService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class EstoqueProdutoPersistence extends GenericPersistenceIGN<EstoqueProduto>
{
	
	public List<CorProduto> consultaCorProduto(int idProduto, String ordemPesquisa) throws Exception
	{
		EntityManager em = ConexaoUtil.getEntityManager();
		
		List<CorProduto> resultado = new ArrayList<CorProduto>();
		ProdutoService pService = new ProdutoService();
		CorService cService = new CorService();
		OrdemProdutoService opService = new OrdemProdutoService();
		Produto produto;
		Cor cor;
		OrdemProduto ordem;
		
		em.getTransaction().begin();
		Query q = em.createNativeQuery("SELECT idProduto, idCor, idOrdemProduto FROM EstoqueProduto WHERE idProduto=" + idProduto + " GROUP BY idProduto, idCor, idOrdemProduto ");
		List<Object[]> listaCores = q.getResultList();
		
		for (int i=0; i<listaCores.size(); i++)
		{
			Object[] res = (Object[]) listaCores.get(i);
			produto = pService.recuperarPorId(Integer.parseInt(res[0].toString()));
			cor = cService.recuperarPorId(Integer.parseInt(res[1].toString()));
			ordem = opService.recuperarPorId(Integer.parseInt(res[2].toString()));
			
			CorProduto corProduto = new CorProduto();
			corProduto.setCor(cor);
			corProduto.setProduto(produto);
			corProduto.setOrdemProduto(ordem);
			resultado.add(corProduto);
		}
		
		return resultado;
	}
	
	public List<Produto> listarProdutoPorSituacaoEstoque(int idSituacao) throws Exception
	{
		EntityManager em = ConexaoUtil.getEntityManager();
		ProdutoService produtoService = new ProdutoService();
		
		
		String sql = "SELECT idProduto as idProduto, count(*) from EstoqueProduto where idSituacaoEnvioLV = " + idSituacao + " GROUP BY id_produto";
		Query query = em.createNativeQuery(sql);
		
		List<Produto> result = new ArrayList<Produto>();
		
		ArrayList<Object[]> lst = new ArrayList<Object[]>(query.getResultList());
		int idProduto;
		Produto produto;
		
		for (int i=0;i<lst.size();i++)
		{
			idProduto = Integer.parseInt(lst.get(i)[0].toString());
			produto = produtoService.recuperarPorId(idProduto);
			result.add(produto);
		}
		
		return result;		
		
	}	
	
	public static void main(String[] args) throws Exception
	{
		EstoqueProdutoPersistence ep = new EstoqueProdutoPersistence();
		ep.consultaCorProduto(1, null);	
	}
}


