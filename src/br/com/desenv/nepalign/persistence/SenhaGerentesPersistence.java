package br.com.desenv.nepalign.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.SenhaGerentes;
import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class SenhaGerentesPersistence extends GenericPersistenceIGN<SenhaGerentes>
{
 public List<SenhaGerentes> listarUsuarioMaster(Usuario usu) throws SQLException, Exception
 {
	 String str = "";
	 str = str + " select idSenhaGerentes as id from senhagerentes s "; 
	 str = str + " inner join usuario u on u.idUsuario = s.idUsuario "; 
	 str = str + " where if(u.idUsuario = "+usu.getId()+",true,u.usuarioMaster = 'N') "; 
	 Connection conn; 
	 conn = ConexaoUtil.getConexaoPadrao();
	 java.sql.Statement rec = conn.createStatement();    
     ResultSet rs = rec.executeQuery(str);
     List<SenhaGerentes> lista = new ArrayList<>();
     while(rs.next())
     {
    	lista.add(this.recuperarPorId(rs.getInt("id")));
     }
     rs.close();
     rec.close();
     conn.close();
	return lista;
	 
 }
}


