package br.com.desenv.nepalign.persistence;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.NotaFiscal;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class NotaFiscalPersistence extends GenericPersistenceIGN<NotaFiscal>
{
	public Integer buscarMaiorNumeroNota() throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		StringBuffer sql = new StringBuffer("select max(numero) from NotaFiscal ");
		Query query = manager.createQuery(sql.toString());


		Integer maiorNumeroNota = (Integer) query.getSingleResult();
		
		if (maiorNumeroNota == null)
		{
			maiorNumeroNota = 1;
		}
		else
		{
			maiorNumeroNota++;
		}
		
		manager.close();

		return maiorNumeroNota;
	}
	
	}


