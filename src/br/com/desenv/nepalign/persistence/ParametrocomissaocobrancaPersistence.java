package br.com.desenv.nepalign.persistence;

import java.sql.Connection;
import java.sql.ResultSet;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.Cobrador;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.FormaPagamento;
import br.com.desenv.nepalign.model.Parametrocomissaocobranca;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ParametrocomissaocobrancaPersistence extends GenericPersistenceIGN<Parametrocomissaocobranca>
{
 public Parametrocomissaocobranca retornarParametro(FormaPagamento forma,Integer dias,EmpresaFisica empresa,Cobrador cob) throws Exception
 {
	 try
	 {
		 
	 
	 String str = "";
	 str = str + " SELECT param.idparametrocomissaocobranca as 'id' FROM parametrocomissaocobranca param "; 
	 str = str + " INNER JOIN formapagamento forma on param.idformapagamento = forma.idformapagamento "; 
	 str = str + " where param.qtdDiasAte >=  "+dias; 
	 str = str + " and forma.idformapagamento =  "+forma.getId(); 
	 str = str + " and param.idEmpresaFisica =  " +empresa.getId(); 
	 //str = str + " and param.internoExterno = '"+cob.getInternoExterno()+"'";
	 str = str + " and param.idCobrador = "+cob.getId();
	 str = str + " order by qtdDiasAte "; 
	 
	 Connection conn;      
     conn = ConexaoUtil.getConexaoPadrao();
	 java.sql.Statement rec = conn.createStatement();    
     ResultSet rs = rec.executeQuery(str);
     Integer id = 0;
     if(rs.next()==true)
     {
    	 id = rs.getInt("id");
     }
     Parametrocomissaocobranca param = new Parametrocomissaocobranca();
     param = this.recuperarPorId(id);
     if(param == null)
     {
    	 throw new Exception("Percentual do cobrador não encontrado!");
     }
     else
     {
     return param;	 
     }
	 }
	 catch(Exception ex)
	 {
		 ex.printStackTrace();
		 throw ex;
	 }
 }
}


