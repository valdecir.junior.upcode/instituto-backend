package br.com.desenv.nepalign.persistence;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.TipoMovimentoCaixa;

public class TipoMovimentoCaixaPersistence extends GenericPersistenceIGN<TipoMovimentoCaixa>
{
	public TipoMovimentoCaixaPersistence() { super(); }
}