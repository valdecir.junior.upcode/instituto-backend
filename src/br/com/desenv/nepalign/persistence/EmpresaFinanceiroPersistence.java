package br.com.desenv.nepalign.persistence;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.EmpresaFinanceiro;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.GrupoEmpresa;

public class EmpresaFinanceiroPersistence extends GenericPersistenceIGN<EmpresaFinanceiro> 
{
	public EmpresaFinanceiroPersistence()
	{
		super();
	}
	
	public final EmpresaFinanceiro recuperarEmpresaFinanceiro(final EntityManager manager, final Integer empresaFisica, final Integer grupoEmpresa) throws Exception
	{
		if(empresaFisica == null || empresaFisica.intValue() == 0x00)
			return null;
		if(grupoEmpresa == null || grupoEmpresa.intValue() == 0x00)
			return null;
		
		final String sqlQuery = "SELECT * FROM `empresaFinanceiro` WHERE `idEmpresaFisica` = " + empresaFisica.intValue() + " AND `idGrupoEmpresa` = " + grupoEmpresa.intValue();
		
		try
		{
			return (EmpresaFinanceiro) manager.createNativeQuery(sqlQuery, EmpresaFinanceiro.class).getResultList().get(0x00);	
		}
		catch(Exception ex)
		{
			System.err.println("Não foi possível recuperar Empresa Financeiro da Empresa " + empresaFisica + " do Grupo " + grupoEmpresa);
			throw ex;
		}
	}
	
	public final EmpresaFinanceiro recuperarEmpresaFinanceiro(final EntityManager manager, final EmpresaFisica empresaFisica, final GrupoEmpresa grupoEmpresa) throws Exception
	{
		return recuperarEmpresaFinanceiro(manager, empresaFisica.getId(), grupoEmpresa.getId());
	}
}