package br.com.desenv.nepalign.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.desenv.frameworkignorante.GenericPersistenceIGN;
import br.com.desenv.nepalign.model.Bancos;
import br.com.desenv.nepalign.model.SaldoBancos;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class SaldoBancosPersistence extends GenericPersistenceIGN<SaldoBancos>
{
	private static Object locker = new Object();
	
	public SaldoBancosPersistence() { super(); }
	
	public SaldoBancos recuperarSaldoBancosAtual(EntityManager manager, Bancos bancos) throws Exception
	{
		boolean managerIndependente = false;
		
		if((managerIndependente = (manager == null)))
			manager = ConexaoUtil.getEntityManager();
		
		try
		{
			synchronized(locker)
			{
				if(bancos == null)
					throw new RuntimeException("não foi possível carregar o saldo do banco. Banco null");
				
				Query query = manager.createNativeQuery("select * from saldoBancos WHERE idBancos = ? and mes = ? and ano = ?", SaldoBancos.class);
				query.setParameter(0x01, bancos.getId());
				query.setParameter(0x02, bancos.getMesAberto());
				query.setParameter(0x03, bancos.getAnoAberto());
				
				@SuppressWarnings("unchecked")
				List<SaldoBancos> resultList = query.getResultList();
				
				if(resultList.size() <= 0x00)
					throw new RuntimeException("não foi encontrado o saldo do mês vigente do banco.");
				else
				{
					if(resultList.size() > 0x01)
						throw new RuntimeException("Encontrado mais de 1 saldo para o mês vigente do banco!");
					else
						return resultList.get(0x00);
				}
			}	
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
	
	public boolean atualizarSaldoBanco(EntityManager manager, Bancos bancos, Double valor) throws Exception
	{
		boolean managerIndependente = false;
		
		if((managerIndependente = (manager == null)))
			manager = ConexaoUtil.getEntityManager();
		
		try
		{
			synchronized(locker)
			{
				if(bancos == null)
					throw new RuntimeException("não foi possível atualizar o saldo do banco. Banco null");
				
				Query query = manager.createNativeQuery("update saldoBancos set saldo = saldo + ? WHERE idBancos = ? and mes = ? and ano = ?");
				query.setParameter(0x01, valor);
				query.setParameter(0x02, bancos.getId());
				query.setParameter(0x03, bancos.getMesAberto());
				query.setParameter(0x04, bancos.getAnoAberto());
				
				query.executeUpdate();
			}
			
			if(managerIndependente)
				manager.getTransaction().commit();
			
			return true;	
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			if(managerIndependente)
				manager.close();
		}
	}
}