package br.com.desenv.nepalign.snapshot;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;
import br.com.desenv.nepalign.model.EmpresaFisica;

@Entity
@Table(name="snapshotVenda")
public class SnapshotVendaMaisVendido extends GenericModelIGN 
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idSnapshotVenda")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	
	@Column(name="idProduto")
	private Integer idProduto;

	@Column(name="quantidade")
	private Integer quantidade;
	
	@Column(name="margem")
	private Double margem;
	
	public SnapshotVendaMaisVendido()
	{
		super();
	}
	
	public SnapshotVendaMaisVendido(EmpresaFisica empresaFisica)
	{
		super();
		this.id = -0x01;
		this.empresaFisica = empresaFisica;
	}

	@Override
	public Integer getId() 
	{
		return this.id;
	}

	@Override
	public void setId(Integer id) 
	{
		this.id = id;
	}

	public EmpresaFisica getEmpresaFisica() 
	{
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) 
	{
		this.empresaFisica = empresaFisica;
	}

	public Integer getIdProduto() 
	{
		return idProduto;
	}

	public void setIdProduto(Integer idProduto) 
	{
		this.idProduto = idProduto;
	}

	public Double getMargem() 
	{
		return margem == null ? 0x00 : margem;
	}

	public void setMargem(Double margem) 
	{
		this.margem = margem;
	}

	public Integer getQuantidade() 
	{
		return quantidade == null ? 0x00 : quantidade;
	}

	public void setQuantidade(Integer quantidade) 
	{
		this.quantidade = quantidade;
	}

	@Override
	public void validate() throws Exception  { }
}