package br.com.desenv.nepalign.snapshot;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import br.com.desenv.frameworkignorante.Formatador;
import br.com.desenv.nepalign.integracao.dto.ItemPedidoVendaDTO;
import br.com.desenv.nepalign.model.SaldoEstoqueProduto;
import br.com.desenv.nepalign.service.SaldoEstoqueProdutoService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class SnapshotVendaRunner extends Snapshot implements ISnapshot, Runnable
{
	private static DecimalFormatSymbols decimalFormatSymbols;
	private static DecimalFormat decimalFormat;
	
	private static final SaldoEstoqueProdutoService saldoEstoqueProdutoService = new SaldoEstoqueProdutoService();
	
	public static void main(String[] args)
	{
		new SnapshotVendaRunner();
	}
	
	public SnapshotVendaRunner() 
	{
		this(null);
	}
	    
	public SnapshotVendaRunner(Integer idEmpresaFisica)
	{
		super(idEmpresaFisica);
		
		if(decimalFormatSymbols == null)
		{
			decimalFormatSymbols = new DecimalFormatSymbols();
			decimalFormatSymbols.setDecimalSeparator('.');
		}
		
		if(decimalFormat == null)
			decimalFormat = new DecimalFormat(".##", decimalFormatSymbols);
	}
	
	public synchronized void run()
	{
		takeSnapshot();
	}
	
	public final synchronized List<Object> takeSnapshot()
	{
		return takeSnapshot(idEmpresaFisica);
	}
	
	public final synchronized List<Object> takeSnapshot(Integer idEmpresaFisica)
	{
		return takeSnapshot(null, idEmpresaFisica);
	}
	
	public final synchronized List<Object> takeSnapshot(Connection connection, Integer idEmpresaFisica)
	{
		String sqlQuery = "";
		sqlQuery = sqlQuery + " SELECT `itemPedidoVenda`.`precoVenda`, `itemPedidoVenda`.`quantidade`, `itemPedidoVenda`.`idProduto`, `itemPedidoVenda`.`entradaSaida` FROM `itemPedidoVenda` ";
		sqlQuery = sqlQuery + " INNER JOIN `pedidovenda` ON `pedidoVenda`.`idPedidoVenda` = `itemPedidoVenda`.`idPedidoVenda` ";
		sqlQuery = sqlQuery + " WHERE DATE(`pedidoVenda`.`dataVenda`) = DATE(NOW()) AND `pedidoVenda`.`idEmpresaFisica` = " + idEmpresaFisica + " AND `pedidoVenda`.`idSituacaoPedidoVenda` <> 5; ";
		
		try
		{			
			if(connection == null)
				connection = getConnection(idEmpresaFisica);
			
			final Connection conexaoPrincipal = ConexaoUtil.getConexaoPadrao();
			
			int quantidadeTotal = 0x00;
			double precoVendaTotal = 0x00;
			double precoCustoTotal = 0x00;
			double margem = 0x00;
			
			final ResultSet rs = connection.createStatement(ResultSet.FETCH_UNKNOWN, ResultSet.CONCUR_READ_ONLY).executeQuery(sqlQuery);

			while(rs.next())
			{
				final int quantidade = rs.getInt("quantidade");
				final String entradaSaida = rs.getString("entradaSaida");
				
				final SaldoEstoqueProduto saldoEstoqueProduto = saldoEstoqueProdutoService.recuperarPrecoProduto(conexaoPrincipal, rs.getInt("idProduto"), idEmpresaFisica);
				final Double custo = saldoEstoqueProduto.getCusto() == null ? 0x00 : saldoEstoqueProduto.getCusto();
				
				precoVendaTotal += (rs.getDouble("precoVenda") * quantidade) * (entradaSaida.equals("E") ? -0x01 : 0x01);
				precoCustoTotal += (custo * quantidade)  * (entradaSaida.equals("E") ? -0x01 : 0x01);
				 
				quantidadeTotal += quantidade  * (entradaSaida.equals("E") ? -0x01 : 0x01);
			}	
			
			precoVendaTotal = Formatador.round(precoVendaTotal, 0x02);
			precoCustoTotal = Formatador.round(precoCustoTotal, 0x02);
			
			margem = Formatador.round(((precoVendaTotal / precoCustoTotal) - 0x01) * 0x64, 0x02);
			
			rs.close();
			
			conexaoPrincipal.createStatement().execute("DELETE FROM `snapshotVenda` WHERE HOUR(`dataSnapshot`) = HOUR(NOW()) AND `idEmpresaFisica` = " + idEmpresaFisica);
			
			PreparedStatement ps = conexaoPrincipal.prepareStatement("INSERT INTO `snapshotVenda` (`idEmpresaFisica`, `valorVenda`, `valorCusto`, `margem`, `quantidade`, `dataSnapshot`) VALUES (?, ?, ?, ?, ?, NOW());");
			ps.setInt(0x01, idEmpresaFisica);
			ps.setDouble(0x02, precoVendaTotal);
			ps.setDouble(0x03, precoCustoTotal);
			ps.setDouble(0x04, margem);
			ps.setInt(0x05, quantidadeTotal);
			
			ps.execute(); 
			ps.close();
			
			connection.close();
			conexaoPrincipal.close();
			
			System.out.println("================= EMP " + idEmpresaFisica);
			System.out.println(">> snapshot archieved");
		}
		catch(Exception ex)
		{
			System.out.println("================= EMP " + idEmpresaFisica);
			System.out.println(">> cannot take a snapshot");
			System.out.println();
		}
		
		return null;
	}
	
	public final synchronized List<Object> takeOfflineSnapshot(List<ItemPedidoVendaDTO> listaItemPedidoVenda, Integer idEmpresaFisica)
	{
		try
		{			
			BigDecimal quantidadeTotal = new BigDecimal("0");
			BigDecimal precoVendaTotal = new BigDecimal("0");
			BigDecimal precoCustoTotal = new BigDecimal("0");
			BigDecimal margem = new BigDecimal("0");
			
			final HashMap<Integer, BigDecimal[]> produtosMaisVendidos = new HashMap<Integer, BigDecimal[]>();
			
			final Connection conexaoPrincipal = ConexaoUtil.getConexaoPadrao();
			
			for (final ItemPedidoVendaDTO itemPedidoVenda : listaItemPedidoVenda)
			{
				final SaldoEstoqueProduto saldoEstoqueProduto = saldoEstoqueProdutoService.recuperarPrecoProduto(conexaoPrincipal, itemPedidoVenda.getProduto(), idEmpresaFisica);
				final BigDecimal custo = new BigDecimal(decimalFormat.format(saldoEstoqueProduto.getCusto() == null ? 0x00 : saldoEstoqueProduto.getCusto()));
				final BigDecimal venda = new BigDecimal(decimalFormat.format(saldoEstoqueProduto.getVenda() == null ? 0x00 : saldoEstoqueProduto.getVenda()));
				
				final BigDecimal quantidade = new BigDecimal(itemPedidoVenda.getQuantidade());
				final String entradaSaida = itemPedidoVenda.getEntradaSaida();
				
				precoVendaTotal = precoVendaTotal.add(new BigDecimal(decimalFormat.format(itemPedidoVenda.getPrecoVenda())).multiply(quantidade).multiply(new BigDecimal(entradaSaida.equals("E") ? -0x01 : 0x01)));
				precoCustoTotal = precoCustoTotal.add((custo.multiply(quantidade)).multiply(new BigDecimal(entradaSaida.equals("E") ? -0x01 : 0x01)));
				quantidadeTotal = quantidadeTotal.add(quantidade.multiply(new BigDecimal(entradaSaida.equals("E") ? -0x01 : 0x01)));
				
				try
				{
					BigDecimal[] dadosProduto = produtosMaisVendidos.get(itemPedidoVenda.getProduto());
					
					if(dadosProduto == null)
					{
						dadosProduto = new BigDecimal[0x05];
						
						dadosProduto[0x00] = quantidade.multiply(new BigDecimal(entradaSaida.equals("E") ? -0x01 : 0x01));
						dadosProduto[0x01] = new BigDecimal(decimalFormat.format(itemPedidoVenda.getPrecoVenda())).multiply(quantidade).multiply(new BigDecimal(entradaSaida.equals("E") ? -0x01 : 0x01));
					}
					else
					{
						dadosProduto[0x00] = dadosProduto[0x00].add(quantidade.multiply(new BigDecimal(entradaSaida.equals("E") ? -0x01 : 0x01)));
						dadosProduto[0x01] = dadosProduto[0x01].add(new BigDecimal(decimalFormat.format(itemPedidoVenda.getPrecoVenda())).multiply(quantidade).multiply(new BigDecimal(entradaSaida.equals("E") ? -0x01 : 0x01)));
					}
					
					dadosProduto[0x02] = custo;
					dadosProduto[0x03] = venda;
					
					if(custo.compareTo(new BigDecimal(0)) != 0x00 && dadosProduto[0x00].compareTo(new BigDecimal(0)) != 0x00) /** caso não haja custo, não calcula margem **/
						dadosProduto[0x04] = dadosProduto[0x01].divide(dadosProduto[0x02].multiply(dadosProduto[0x00]), 0x02, RoundingMode.HALF_UP).subtract(new BigDecimal(1)).multiply(new BigDecimal(0x64));
					else dadosProduto[0x04] = new BigDecimal(0);
					
					produtosMaisVendidos.put(itemPedidoVenda.getProduto(), dadosProduto);	
				}
				catch(Exception ex) 
				{
					ex.printStackTrace();
				}	
			}	
			
			if(precoCustoTotal.compareTo(new BigDecimal(0)) != 0x00) /** caso não haja custo, não calcula margem **/
				margem = precoVendaTotal.divide(precoCustoTotal, 0x02, RoundingMode.HALF_UP).subtract(new BigDecimal(0x01)).multiply(new BigDecimal(0x64));
			else
				margem = new BigDecimal(0);
			
			List<Map.Entry<Integer, BigDecimal[]>> list = new LinkedList<Map.Entry<Integer, BigDecimal[]>>(produtosMaisVendidos.entrySet());
			Collections.sort(list, new Comparator<Map.Entry<Integer, BigDecimal[]>>()
			{
	            public int compare(Map.Entry<Integer, BigDecimal[]> o1, Map.Entry<Integer, BigDecimal[]> o2)
	            {
	            	if((o2.getValue()[0x00]).compareTo(o1.getValue()[0x00]) == 0x00) 
	            		return (o2.getValue()[0x04]).compareTo(o1.getValue()[0x04]);
	            	else
	            		return (o2.getValue()[0x00]).compareTo(o1.getValue()[0x00]);
	            }
			});
			
			list = list.subList(0x00, list.size() < 0x03 ? list.size() : 0x03);
			produtosMaisVendidos.clear();
			
			conexaoPrincipal.createStatement().execute("DELETE FROM `snapshotVenda` WHERE HOUR(`dataSnapshot`) = HOUR(NOW()) AND `idEmpresaFisica` = " + idEmpresaFisica);
			conexaoPrincipal.createStatement().execute("DELETE FROM `snapshotVendaMaisVendido` WHERE HOUR(`dataSnapshot`) = HOUR(NOW()) AND `idEmpresaFisica` = " + idEmpresaFisica);
			
			final PreparedStatement ps = conexaoPrincipal.prepareStatement("INSERT INTO `snapshotVenda` (`idEmpresaFisica`, `valorVenda`, `valorCusto`, `margem`, `quantidade`, `dataSnapshot`) VALUES (?, ?, ?, ?, ?, NOW());");
			ps.setInt(0x01, idEmpresaFisica);
			ps.setDouble(0x02, precoVendaTotal.doubleValue());
			ps.setDouble(0x03, precoCustoTotal.doubleValue());
			ps.setDouble(0x04, margem.doubleValue());
			ps.setInt(0x05, quantidadeTotal.intValue());
			ps.execute(); 
			
			final PreparedStatement psMaisVendido = conexaoPrincipal.prepareStatement("INSERT INTO `snapshotVendaMaisVendido` (`idEmpresaFisica`, `idProduto`, `quantidade`, `margem`, `valorVenda`, `valorCusto`, `totalVenda`, `dataSnapshot`) VALUES (?, ?, ?, ?, ?, ?, ?, NOW());");
			
	        for (final Map.Entry<Integer, BigDecimal[]> entry : list)
	        {
	        	psMaisVendido.setInt(0x01, idEmpresaFisica);
				psMaisVendido.setDouble(0x02,entry.getKey());
				psMaisVendido.setInt(0x03, entry.getValue()[0x00].intValue());
				psMaisVendido.setDouble(0x04, entry.getValue()[0x04].doubleValue());
				psMaisVendido.setDouble(0x05, entry.getValue()[0x03].doubleValue());
				psMaisVendido.setDouble(0x06, entry.getValue()[0x02].doubleValue());
				psMaisVendido.setDouble(0x07, entry.getValue()[0x01].doubleValue());

				try
				{
					psMaisVendido.execute();
				}
				catch(Exception ex)
				{
					Logger.getRootLogger().error("Não foi possível salvar SnapshotVendaMaisVendido", ex);
				}
	        }
	        
	        ps.close();
	        psMaisVendido.close();
			conexaoPrincipal.close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return null;
	}
}