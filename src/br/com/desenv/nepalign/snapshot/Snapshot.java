package br.com.desenv.nepalign.snapshot;

import java.sql.DriverManager;
import java.util.ArrayList;

import br.com.desenv.nepalign.integracao.Util;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.util.DsvConstante;

public class Snapshot 
{
	public static final ArrayList<Integer> lojasSnapshot = new ArrayList<Integer>() {
		private static final long serialVersionUID = 1L;
	{
		try
		{
			for(final String loja : DsvConstante.getParametrosSistema().get("EMPRESAS_DISPONIVEIS").split(","))
				add(Integer.parseInt(loja));	
		}
		catch(Exception ex)
		{
			
		}
	}};
	
	public static final ArrayList<Integer> lojasSnapshotJorrovi = new ArrayList<Integer>() {
		private static final long serialVersionUID = 1L;
	{
		try
		{
			for(final String loja : DsvConstante.getParametrosSistema().get("EMPRESAS_DISPONIVEIS_GRUPO").split(","))
				add(Integer.parseInt(loja));	
		}
		catch(Exception ex)
		{
			
		}
	}};
	
	protected Integer idEmpresaFisica;
	
	public Snapshot() { }
	
	public Snapshot(Integer idEmpresaFisica)
	{
		this.idEmpresaFisica = idEmpresaFisica;
	}
	
	public Snapshot(EmpresaFisica empresaFisica)
	{
		
	}
	
	protected final synchronized java.sql.Connection getConnection(Integer idEmpresaFisica) throws Exception
	{
		Class.forName("com.mysql.jdbc.Driver");
		
		return DriverManager.getConnection(Util.getDatabaseUrl(idEmpresaFisica), "desenv", "desenv@3377");
	}
	
	public static final synchronized boolean hasConnection(Integer idEmpresaFisica) throws Exception
	{
		try 
		{
			DriverManager.getConnection(Util.getDatabaseUrl(idEmpresaFisica), "desenv", "desenv@3377").close();
			return true;
		}
		catch(Exception ex)
		{
			return false;
		}
	}
}