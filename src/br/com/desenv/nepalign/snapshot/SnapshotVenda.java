package br.com.desenv.nepalign.snapshot;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.desenv.frameworkignorante.GenericModelIGN;
import br.com.desenv.nepalign.model.EmpresaFisica;

@Entity
@Table(name="snapshotVenda")
public class SnapshotVenda extends GenericModelIGN 
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idSnapshotVenda")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="idEmpresaFisica")
	private EmpresaFisica empresaFisica;
	
	@Column(name="valorVenda")
	private Double valorVenda;
	
	@Column(name="valorCusto")
	private Double valorCusto;
	
	@Column(name="margem")
	private Double margem;
	
	@Column(name="quantidade")
	private Integer quantidade;

	public SnapshotVenda()
	{
		super();
	}
	
	public SnapshotVenda(EmpresaFisica empresaFisica)
	{
		super();
		this.id = -0x01;
		this.empresaFisica = empresaFisica;
	}

	@Override
	public Integer getId() 
	{
		return this.id;
	}

	@Override
	public void setId(Integer id) 
	{
		this.id = id;
	}

	public EmpresaFisica getEmpresaFisica() 
	{
		return empresaFisica;
	}

	public void setEmpresaFisica(EmpresaFisica empresaFisica) 
	{
		this.empresaFisica = empresaFisica;
	}

	public Double getValorVenda() 
	{
		return valorVenda == null ? 0x00 : valorVenda;
	}

	public void setValorVenda(Double valorVenda) 
	{
		this.valorVenda = valorVenda;
	}

	public Double getValorCusto() 
	{
		return valorCusto == null ? 0x00 : valorCusto;
	}

	public void setValorCusto(Double valorCusto) 
	{
		this.valorCusto = valorCusto;
	}

	public Double getMargem() 
	{
		return margem == null ? 0x00 : margem;
	}

	public void setMargem(Double margem) 
	{
		this.margem = margem;
	}

	public Integer getQuantidade() 
	{
		return quantidade == null ? 0x00 : quantidade;
	}

	public void setQuantidade(Integer quantidade) 
	{
		this.quantidade = quantidade;
	}

	@Override
	public void validate() throws Exception  { }
}