package br.com.desenv.nepalign.snapshot;

import java.util.List;

public interface ISnapshot 
{
	public void run();
	public List<Object> takeSnapshot();
	public List<Object> takeSnapshot(Integer idEmpresaFisica);
}
