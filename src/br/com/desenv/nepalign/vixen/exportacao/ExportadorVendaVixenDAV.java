package br.com.desenv.nepalign.vixen.exportacao;

import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import br.com.desenv.frameworkignorante.DesenvMSSQLConnector;
import br.com.desenv.nepalign.model.ItemPedidoVenda;
import br.com.desenv.nepalign.model.PedidoVenda;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.vixen.exportacao.service.PvIntegraDavService;
import br.com.desenv.nepalign.vixen.exportacao.service.PvIntegraItensDavService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ExportadorVendaVixenDAV 
{
	public static boolean exportaCodigoBarras = false;
	private static ExportadorVendaVixenDAV instance;
	
	public static ExportadorVendaVixenDAV getInstance() throws Exception
	{
		if(instance == null)
		{
			exportaCodigoBarras = DsvConstante.getParametrosSistema().get("VIXEN_EXPORTAR_COMO_CODIGOBARRAS") != null && DsvConstante.getParametrosSistema().get("VIXEN_EXPORTAR_COMO_CODIGOBARRAS").equals("S");
			instance = new ExportadorVendaVixenDAV();	
		}
		
		return instance;
	}
	
	public static final Logger logger = Logger.getLogger(ExportadorVendaVixenDAV.class);
	
	private static final PvIntegraDavService pvIntegraDavService = new PvIntegraDavService();
	private static final PvIntegraItensDavService pvIntegraItensDavService = new PvIntegraItensDavService();
	
	public static void main(String[] args) throws Exception
	{
		if(args.length == 1)
			exportaCodigoBarras = args[0x00].equals("EXPORTAR_CODIGOBARRAS");
		
		final EntityManager manager = ConexaoUtil.getEntityManager();
		final DesenvMSSQLConnector msSQLConnector = new DesenvMSSQLConnector(DsvConstante.getParametrosSistema().get("SQL_HOSTNAME_VIXEN"),
				DsvConstante.getParametrosSistema().get("SQL_USERNAME_VIXEN"),
				DsvConstante.getParametrosSistema().get("SQL_PASSWORD_VIXEN"),
				DsvConstante.getParametrosSistema().get("SQL_DATABASE_VIXEN"));
		
		final PvIntegraDavService pvIntegraDavService = new PvIntegraDavService();
		final PvIntegraItensDavService pvIntegraItensDavService = new PvIntegraItensDavService();
		//final PvIntegraFinanceiroDavService pvIntegraFinanceiroDavService = new PvIntegraFinanceiroDavService();
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Digite o número de controle: ");
		String numeroControle = scanner.nextLine();
		System.out.print("Digite o número do CPF: ");
		String cpf = scanner.nextLine();
		
		System.out.print("Deseja exportar o controle " + numeroControle + " ? (S/N) ");
		
		String confirmacao = scanner.nextLine();
		
		if(confirmacao.equals("S") || confirmacao.equals("s"))
		{
			PedidoVenda pedidoVenda = (PedidoVenda) manager.createNativeQuery("SELECT * FROM pedidoVenda WHERE numeroControle = '" + numeroControle + "';", PedidoVenda.class).getResultList().get(0);
			@SuppressWarnings("unchecked")
			List<ItemPedidoVenda> listaItemPedidoVenda = (List<ItemPedidoVenda>) manager.createNativeQuery("SELECT * FROM itemPedidoVenda WHERE idPedidoVenda = " + pedidoVenda.getId(), ItemPedidoVenda.class).getResultList();
			//@SuppressWarnings("unchecked")
			//List<PagamentoPedidoVenda> listaPagamentoPedidoVenda = (List<PagamentoPedidoVenda>) manager.createNativeQuery("SELECT * FROM pagamentoPedidoVenda WHERE idPedidoVenda = " + pedidoVenda.getId(), PagamentoPedidoVenda.class).getResultList();
			
			System.out.println("Importando Pedido");
			pvIntegraDavService.gravarVixen(pedidoVenda, cpf.equals("") ? null : cpf);
			System.out.println("Importando Itens");
			pvIntegraItensDavService.gravarVixen(listaItemPedidoVenda);
			//System.out.println("Importando Pagamentos");
			//pvIntegraFinanceiroDavService.gravarVixen(listaPagamentoPedidoVenda);
			
			try
			{
				msSQLConnector.executeStoredProcedure("SPU_PV_INTEGRADAV");	
			}
			catch(Exception ex)
			{
				// supress exception caused by result
			}
			
			System.out.println("Controle " + numeroControle + " importado");
		}
		
		scanner.close();
	}

	/**
	 * Exporta uma venda para o sistema Vixen (DAV)
	 * @param pedidoVenda Venda a ser exportada
	 * @param cpfCnpj CPF do Cliente
	 * @throws Exception
	 */
	public void exportarVendaSemPagamentos(final PedidoVenda pedidoVenda, final String cpfCnpj) throws Exception
	{
		logger.info("Exportando Pedido " + pedidoVenda.getNumeroControle() + " com CPF/CNPJ " + (cpfCnpj == null ? "SEM CPF" : cpfCnpj));
		try
		{
			final EntityManager manager = ConexaoUtil.getEntityManager();
			final DesenvMSSQLConnector msSQLConnector = new DesenvMSSQLConnector(DsvConstante.getParametrosSistema().get("SQL_HOSTNAME_VIXEN"),
					DsvConstante.getParametrosSistema().get("SQL_USERNAME_VIXEN"),
					DsvConstante.getParametrosSistema().get("SQL_PASSWORD_VIXEN"),
					DsvConstante.getParametrosSistema().get("SQL_DATABASE_VIXEN"));
		
			@SuppressWarnings("unchecked")
			List<ItemPedidoVenda> listaItemPedidoVenda = (List<ItemPedidoVenda>) manager.createNativeQuery("SELECT * FROM itemPedidoVenda WHERE idPedidoVenda = " + pedidoVenda.getId(), ItemPedidoVenda.class).getResultList();
			
			pvIntegraDavService.openConnection();
			pvIntegraDavService.gravarVixen(pedidoVenda, cpfCnpj);
			pvIntegraDavService.dispose();
			pvIntegraItensDavService.openConnection();
			pvIntegraItensDavService.gravarVixen(listaItemPedidoVenda);
			pvIntegraItensDavService.dispose();
			
			msSQLConnector.executeStoredProcedure("SPU_PV_INTEGRADAV"); // stored procedure do Vixen
			msSQLConnector.disposeConn();
			
			logger.info("Pedido " + pedidoVenda.getNumeroControle() + " exportado com sucesso");	
		}
		catch(Exception ex)
		{
			logger.error("Não foi possível exportar o Pedido " + pedidoVenda.getNumeroControle(), ex);	
			throw ex;
		}
	}
}