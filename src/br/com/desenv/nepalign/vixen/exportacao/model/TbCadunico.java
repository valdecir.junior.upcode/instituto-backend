package br.com.desenv.nepalign.vixen.exportacao.model;

public class TbCadunico 
{
	private String cdImportacao;
	private String dsFantasia;
	private String dsRazao;
	private String dsEndereco;
	private String dsCidade;
	private String dsUf;
	private String dsBairro;
	private String dsCep;
	private String dsPais;
	private String dsDdd;
	private String dsFone;
	private String dsFax;
	private String dsEmail;
	private int fkTipo;
	private String tgPessoa;
	private double nrCnpj;
	private String dhInclusao;
	private String dhAlteracao;
	private int tgInativo;
	private int tgPdvclientefidelidade;
	private String dsCodcartaofid;
	
	public String getCdImportacao() 
	{
		return cdImportacao;
	}
	
	public void setCdImportacao(String cdImportacao) {
		this.cdImportacao = cdImportacao;
	}
	
	public String getDsFantasia() 
	{
		return dsFantasia;
	}
	
	public void setDsFantasia(String dsFantasia) 
	{
		this.dsFantasia = dsFantasia;
	}
	
	public String getDsEndereco() 
	{
		return dsEndereco;
	}
	
	public void setDsEndereco(String dsEndereco) 
	{
		this.dsEndereco = dsEndereco;
	}
	
	public String getDsCidade() 
	{
		return dsCidade;
	}
	
	public void setDsCidade(String dsCidade) 
	{
		this.dsCidade = dsCidade;
	}
	
	public String getDsUf() 
	{
		return dsUf;
	}
	
	public void setDsUf(String dsUf) 
	{
		this.dsUf = dsUf;
	}
	
	public String getDsBairro() 
	{
		return dsBairro;
	}
	
	public void setDsBairro(String dsBairro) 
	{
		this.dsBairro = dsBairro;
	}
	
	public String getDsCep() 
	{
		return dsCep;
	}
	
	public void setDsCep(String dsCep) 
	{
		this.dsCep = dsCep;
	}
	
	public String getDsPais() 
	{
		return dsPais;
	}
	
	public void setDsPais(String dsPais) 
	{
		this.dsPais = dsPais;
	}
	
	public String getDsDdd() 
	{
		return dsDdd;
	}
	
	public void setDsDdd(String dsDdd) 
	{
		this.dsDdd = dsDdd;
	}
	
	public String getDsFone() 
	{
		return dsFone;
	}
	
	public void setDsFone(String dsFone) 
	{
		this.dsFone = dsFone;
	}
	
	public String getDsFax() 
	{
		return dsFax;
	}
	
	public void setDsFax(String dsFax) 
	{
		this.dsFax = dsFax;
	}
	
	public String getDsEmail() 
	{
		return dsEmail;
	}
	
	public void setDsEmail(String dsEmail) 
	{
		this.dsEmail = dsEmail;
	}
	
	public int getFkTipo() 
	{
		return fkTipo;
	}
	
	public void setFkTipo(int fkTipo)
	{
		this.fkTipo = fkTipo;
	}
	
	public String getTgPessoa() 
	{
		return tgPessoa;
	}
	
	public void setTgPessoa(String tgPessoa) 
	{
		this.tgPessoa = tgPessoa;
	}
	
	public double getNrCnpj() 
	{
		return nrCnpj;
	}
	
	public void setNrCnpj(double nrCnpj) 
	{
		this.nrCnpj = nrCnpj;
	}
	
	public String getDhInclusao() 
	{
		return dhInclusao;
	}
	
	public void setDhInclusao(String dhInclusao) 
	{
		this.dhInclusao = dhInclusao;
	}
	
	public String getDhAlteracao() 
	{
		return dhAlteracao;
	}
	
	public void setDhAlteracao(String dhAlteracao) 
	{
		this.dhAlteracao = dhAlteracao;
	}
	
	public int getTgInativo() 
	{
		return tgInativo;
	}
	
	public void setTgInativo(int tgInativo) 
	{
		this.tgInativo = tgInativo;
	}
	public int getTgPdvclientefidelidade() 
	{
		return tgPdvclientefidelidade;
	}
	
	public void setTgPdvclientefidelidade(int tgPdvclientefidelidade) 
	{
		this.tgPdvclientefidelidade = tgPdvclientefidelidade;
	}
	
	public String getDsCodcartaofid() 
	{
		return dsCodcartaofid;
	}
	
	public void setDsCodcartaofid(String dsCodcartaofid) 
	{
		this.dsCodcartaofid = dsCodcartaofid;
	}
	
	public String getDsRazao() 
	{
		return dsRazao;
	}
	
	public void setDsRazao(String dsRazao) 
	{
		this.dsRazao = dsRazao;
	}
}