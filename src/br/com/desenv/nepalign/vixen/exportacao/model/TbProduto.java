package br.com.desenv.nepalign.vixen.exportacao.model;

public class TbProduto 
{
	private String cdImportacao;
	private String pkId;
	private String dsModelo;
	private String dsUnidade;
	private String cdBarras;
	private String dsCodfabrica;
	private double vlPreta1;
	private double vlPoricms;
	private double vlPoriss;
	private int tgServico;
	private double vlDescmax;
	private double vlDescpromo;
	private int tgBrinde;
	private String dhInclusao;
	private String dhAlteracao;
	private int tgInativo;
	private String dsEmpresa;
	private String tgOrigemICMS;
	private String nrSittribpdv;
	private String nrSittribpdvaux;
	private int tgPdvnaogerapontuacao;
	private int tgNaof4venda;

	public String getCdImportacao() 
	{
		return cdImportacao;
	}

	public void setCdImportacao(String cdImportacao) 
	{
		this.cdImportacao = cdImportacao;
	}

	public String getPkId() 
	{
		return pkId;
	}

	public void setPkId(String pkId) 
	{
		this.pkId = pkId;
	}

	public String getDsModelo() 
	{
		return dsModelo;
	}

	public void setDsModelo(String dsModelo) 
	{
		this.dsModelo = dsModelo;
	}

	public String getDsUnidade() 
	{
		return dsUnidade;
	}

	public void setDsUnidade(String dsUnidade) 
	{
		this.dsUnidade = dsUnidade;
	}

	public String getCdBarras() 
	{
		return cdBarras;
	}

	public void setCdBarras(String cdBarras) 
	{
		this.cdBarras = cdBarras;
	}

	public String getDsCodfabrica() 
	{
		return dsCodfabrica;
	}

	public void setDsCodfabrica(String dsCodfabrica) 
	{
		this.dsCodfabrica = dsCodfabrica;
	}

	public double getVlPreta1() 
	{
		return vlPreta1;
	}

	public void setVlPreta1(double vlPreta1) 
	{
		this.vlPreta1 = vlPreta1;
	}

	public double getVlPoricms() 
	{
		return vlPoricms;
	}

	public void setVlPoricms(double vlPoricms) 
	{
		this.vlPoricms = vlPoricms;
	}

	public double getVlPoriss() 
	{
		return vlPoriss;
	}

	public void setVlPoriss(double vlPoriss) 
	{
		this.vlPoriss = vlPoriss;
	}

	public int getTgServico() 
	{
		return tgServico;
	}

	public void setTgServico(int tgServico) 
	{
		this.tgServico = tgServico;
	}

	public double getVlDescmax() 
	{
		return vlDescmax;
	}

	public void setVlDescmax(double vlDescmax) 
	{
		this.vlDescmax = vlDescmax;
	}

	public double getVlDescpromo() 
	{
		return vlDescpromo;
	}

	public void setVlDescpromo(double vlDescpromo) 
	{
		this.vlDescpromo = vlDescpromo;
	}

	public int getTgBrinde() 
	{
		return tgBrinde;
	}

	public void setTgBrinde(int tgBrinde) 
	{
		this.tgBrinde = tgBrinde;
	}

	public String getDhInclusao() 
	{
		return dhInclusao;
	}

	public void setDhInclusao(String dhInclusao) 
	{
		this.dhInclusao = dhInclusao;
	}

	public String getDhAlteracao() 
	{
		return dhAlteracao;
	}

	public void setDhAlteracao(String dhAlteracao) 
	{
		this.dhAlteracao = dhAlteracao;
	}

	public int getTgInativo() 
	{
		return tgInativo;
	}

	public void setTgInativo(int thInativo) 
	{
		this.tgInativo = thInativo;
	}

	public String getDsEmpresa() 
	{
		return dsEmpresa;
	}

	public void setDsEmpresa(String dsEmpresa) 
	{
		this.dsEmpresa = dsEmpresa;
	}

	public String getTgOrigemICMS() 
	{
		return tgOrigemICMS;
	}

	public void setTgOrigemICMS(String tgOrigemICMS) 
	{
		this.tgOrigemICMS = tgOrigemICMS;
	}

	public String getNrSittribpdv() 
	{
		return nrSittribpdv;
	}

	public void setNrSittribpdv(String nrSittribpdv) 
	{
		this.nrSittribpdv = nrSittribpdv;
	}

	public String getNrSittribpdvaux() 
	{
		return nrSittribpdvaux;
	}

	public void setNrSittribpdvaux(String nrSittribpdvaux) 
	{
		this.nrSittribpdvaux = nrSittribpdvaux;
	}

	public int getTgPdvnaogerapontuacao() 
	{
		return tgPdvnaogerapontuacao;
	}

	public void setTgPdvnaogerapontuacao(int tgPdvnaogerapontuacao) 
	{
		this.tgPdvnaogerapontuacao = tgPdvnaogerapontuacao;
	}

	public int getTgNaof4venda() 
	{
		return tgNaof4venda;
	}

	public void setTgNaof4venda(int tgNaof4venda) 
	{
		this.tgNaof4venda = tgNaof4venda;
	}
}