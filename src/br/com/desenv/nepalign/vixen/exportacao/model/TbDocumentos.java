package br.com.desenv.nepalign.vixen.exportacao.model;

public class TbDocumentos 
{
	private String cdImportacao;
	private String pkId;
	private String dsDocumento;
	private int tgPdv;
	private int nrParcmin;
	private int nrParcmax;
	private int nrDiavenc;
	private int tgTef;
	private String fkTefrede;
	private int fkFinalizadoras;
	private String fkTipodoc;
	private String dhInclusao;
	private String dhAlteracao;
	private int tgInativo;
	
	public String getCdImportacao() 
	{
		return cdImportacao;
	}
	public void setCdImportacao(String cdImportacao) 
	{
		this.cdImportacao = cdImportacao;
	}
	
	public String getPkId() 
	{
		return pkId;
	}
	
	public void setPkId(String pkId) 
	{
		this.pkId = pkId;
	}
	
	public String getDsDocumento() 
	{
		return dsDocumento;
	}
	
	public void setDsDocumento(String dsDocumento) 
	{
		this.dsDocumento = dsDocumento;
	}
	
	public int getTgPdv() 
	{
		return tgPdv;
	}
	
	public void setTgPdv(int tgPdv) 
	{
		this.tgPdv = tgPdv;
	}
	
	public int getNrParcmin() 
	{
		return nrParcmin;
	}
	
	public void setNrParcmin(int nrParcmin) 
	{
		this.nrParcmin = nrParcmin;
	}
	
	public int getNrParcmax() 
	{
		return nrParcmax;
	}
	
	public void setNrParcmax(int nrParcmax) 
	{
		this.nrParcmax = nrParcmax;
	}
	
	public int getNrDiavenc() 
	{
		return nrDiavenc;
	}
	
	public void setNrDiavenc(int nrDiavenc) 
	{
		this.nrDiavenc = nrDiavenc;
	}
	
	public int getTgTef() 
	{
		return tgTef;
	}
	
	public void setTgTef(int tgTef) 
	{
		this.tgTef = tgTef;
	}
	
	public String getFkTefrede() 
	{
		return fkTefrede;
	}
	
	public void setFkTefrede(String fkTefrede) 
	{
		this.fkTefrede = fkTefrede;
	}
	
	public int getFkFinalizadoras() 
	{
		return fkFinalizadoras;
	}
	
	public void setFkFinalizadoras(int fkFinalizadoras) 
	{
		this.fkFinalizadoras = fkFinalizadoras;
	}
	
	public String getFkTipodoc() 
	{
		return fkTipodoc;
	}
	
	public void setFkTipodoc(String fkTipodoc) 
	{
		this.fkTipodoc = fkTipodoc;
	}
	
	public String getDhInclusao() 
	{
		return dhInclusao;
	}
	
	public void setDhInclusao(String dhInclusao) 
	{
		this.dhInclusao = dhInclusao;
	}
	
	public String getDhAlteracao() 
	{
		return dhAlteracao;
	}
	
	public void setDhAlteracao(String dhAlteracao) 
	{
		this.dhAlteracao = dhAlteracao;
	}
	
	public int getTgInativo()
	{
		return tgInativo;
	}
	
	public void setTgInativo(int tgInativo) 
	{
		this.tgInativo = tgInativo;
	}
}