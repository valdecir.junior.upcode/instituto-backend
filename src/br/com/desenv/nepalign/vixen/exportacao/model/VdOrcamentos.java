package br.com.desenv.nepalign.vixen.exportacao.model;

public class VdOrcamentos 
{
	private String nrOrcamento;
	private String fkEmpresa;
	private int fkCadunico;
	private String fkVendedor;
	private double vlTotal;
	private double vlDesconto;
	private String dtOrcamento;
	private String dhInclusao;
	private String dhAlteracao;
	private String tgOrigem;
	private int tgPdv;
	private double vlPordescprecisao;
	private double vlDescprevisao;
	private int tgInativo;
	private int fkPedido;
	private int tgModointegridade;
	
	public String getNrOrcamento() 
	{
		return nrOrcamento;
	}
	
	public void setNrOrcamento(String nrOrcamento) 
	{
		this.nrOrcamento = nrOrcamento;
	}
	
	public String getFkEmpresa() 
	{
		return fkEmpresa;
	}
	
	public void setFkEmpresa(String fkEmpresa) 
	{
		this.fkEmpresa = fkEmpresa;
	}
	
	public int getFkCadunico() 
	{
		return fkCadunico;
	}
	
	public void setFkCadunico(int fkCadunico) 
	{
		this.fkCadunico = fkCadunico;
	}
	
	public String getFkVendedor() 
	{
		return fkVendedor;
	}
	
	public void setFkVendedor(String fkVendedor) 
	{
		this.fkVendedor = fkVendedor;
	}
	
	public double getVlTotal() 
	{
		return vlTotal;
	}
	
	public void setVlTotal(double vlTotal) 
	{
		this.vlTotal = vlTotal;
	}
	
	public double getVlDesconto() 
	{
		return vlDesconto;
	}
	
	public void setVlDesconto(double vlDesconto) 
	{
		this.vlDesconto = vlDesconto;
	}
	
	public String getDtOrcamento() 
	{
		return dtOrcamento;
	}
	
	public void setDtOrcamento(String dtOrcamento) 
	{
		this.dtOrcamento = dtOrcamento;
	}
	
	public String getDhInclusao() 
	{
		return dhInclusao;
	}
	
	public void setDhInclusao(String dhInclusao) 
	{
		this.dhInclusao = dhInclusao;
	}
	
	public String getDhAlteracao() 
	{
		return dhAlteracao;
	}
	
	public void setDhAlteracao(String dhAlteracao) 
	{
		this.dhAlteracao = dhAlteracao;
	}
	
	public String getTgOrigem() 
	{
		return tgOrigem;
	}
	
	public void setTgOrigem(String tgOrigem) 
	{
		this.tgOrigem = tgOrigem;
	}
	
	public int getTgPdv() 
	{
		return tgPdv;
	}
	
	public void setTgPdv(int tgPdv) 
	{
		this.tgPdv = tgPdv;
	}
	
	public double getVlPordescprecisao() 
	{
		return vlPordescprecisao;
	}
	
	public void setVlPordescprecisao(double vlPordescprecisao) 
	{
		this.vlPordescprecisao = vlPordescprecisao;
	}
	
	public double getVlDescprevisao() 
	{
		return vlDescprevisao;
	}
	
	public void setVlDescprevisao(double vlDescprevisao) {
		
		this.vlDescprevisao = vlDescprevisao;
	}
	
	public int getTgInativo() 
	{
		return tgInativo;
	}
	
	public void setTgInativo(int tgInativo) 
	{
		this.tgInativo = tgInativo;
	}
	
	public int getFkPedido() 
	{
		return fkPedido;
	}
	
	public void setFkPedido(int fkPedido) 
	{
		this.fkPedido = fkPedido;
	}
	
	public int getTgModointegridade() 
	{
		return tgModointegridade;
	}
	
	public void setTgModointegridade(int tgModointegridade) 
	{
		this.tgModointegridade = tgModointegridade;
	}
}