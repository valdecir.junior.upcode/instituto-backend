package br.com.desenv.nepalign.vixen.exportacao.model;

public class VdItensOrcamentos 
{
	private int fkOrcamento;
	private String fkProduto;
	private String fkCor;
	private String dsCor;
	private String fkTamanho;
	private String dsUnidade;
	private String dsModelo;
	private double qtOrcamento;
	private double vlUnitario;
	private double vlDesconto;
	private double vlAcrescimo;
	private double vlPretot;
	private String dhInclusao;
	private double vlPrecobase;
	private String dhAlteracao;
	private String fkEmpresa;
	
	public int getFkOrcamento() 
	{
		return fkOrcamento;
	}
	
	public void setFkOrcamento(int fkOrcamento) 
	{
		this.fkOrcamento = fkOrcamento;
	}
	
	public String getFkProduto() 
	{
		return fkProduto;
	}
	
	public void setFkProduto(String fkProduto) 
	{
		this.fkProduto = fkProduto;
	}
	
	public String getFkCor() 
	{
		return fkCor;
	}
	
	public void setFkCor(String fkCor) 
	{
		this.fkCor = fkCor;
	}
	
	public String getDsCor() 
	{
		return dsCor;
	}
	
	public void setDsCor(String dsCor) 
	{
		this.dsCor = dsCor;
	}
	
	public String getFkTamanho() 
	{
		return fkTamanho;
	}
	
	public void setFkTamanho(String fkTamanho) 
	{
		this.fkTamanho = fkTamanho;
	}
	
	public String getDsUnidade() 
	{
		return dsUnidade;
	}
	
	public void setDsUnidade(String dsUnidade) 
	{
		this.dsUnidade = dsUnidade;
	}
	
	public String getDsModelo() 
	{
		return dsModelo;
	}
	
	public void setDsModelo(String dsModelo) 
	{
		this.dsModelo = dsModelo;
	}
	
	public double getQtOrcamento() 
	{
		return qtOrcamento;
	}
	
	public void setQtOrcamento(double qtOrcamento) 
	{
		this.qtOrcamento = qtOrcamento;
	}
	
	public double getVlUnitario() 
	{
		return vlUnitario;
	}
	
	public void setVlUnitario(double vlUnitario) 
	{
		this.vlUnitario = vlUnitario;
	}
	
	public double getVlDesconto() 
	{
		return vlDesconto;
	}
	
	public void setVlDesconto(double vlDesconto) 
	{
		this.vlDesconto = vlDesconto;
	}
	
	public double getVlAcrescimo() 
	{
		return vlAcrescimo;
	}
	
	public void setVlAcrescimo(double vlAcrescimo) 
	{
		this.vlAcrescimo = vlAcrescimo;
	}
	
	public double getVlPretot() 
	{
		return vlPretot;
	}
	
	public void setVlPretot(double vlPretot) 
	{
		this.vlPretot = vlPretot;
	}
	
	public String getDhInclusao() 
	{
		return dhInclusao;
	}
	
	public void setDhInclusao(String dhInclusao) 
	{
		this.dhInclusao = dhInclusao;
	}
	
	public double getVlPrecobase() 
	{
		return vlPrecobase;
	}
	
	public void setVlPrecobase(double vlPrecobase) 
	{
		this.vlPrecobase = vlPrecobase;
	}
	
	public String getDhAlteracao() 
	{
		return dhAlteracao;
	}
	
	public void setDhAlteracao(String dhAlteracao) 
	{
		this.dhAlteracao = dhAlteracao;
	}
	
	public String getFkEmpresa()
	{
		return fkEmpresa;
	}
	
	public void setFkEmpresa(String fkEmpresa) 
	{
		this.fkEmpresa = fkEmpresa;
	}
}