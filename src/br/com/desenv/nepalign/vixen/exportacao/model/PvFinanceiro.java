package br.com.desenv.nepalign.vixen.exportacao.model;

public class PvFinanceiro 
{
	private int fkOrigem;
	private String fkDocumento;
	private String dsReferencia;
	private int nrDocid;
	private String dtEmissao;
	private String tgPessoa;
	private double nrCnpj;
	private String dsCliente;
	private double vlAcrescimo;
	private double vlTotal;
	private double vlMovimento;
	private int nrParcelas;
	private String dsCmc7;
	private int nrBanco;
	private int nrAgencia;
	private int nrConta;
	private int nrDigconta;
	private int nrCheque;
	private int nrDigc1;
	private int nrDigc2;
	private int nrDigc3;
	private String dsAutorizacao;
	private String nrNsu;
	private int tgInativo;
	private String tgOrigem;
	private String dhInclusao;
	private String dhAlteracao;
	private String dsFinalizadora;
	private String fkTipodoc;
	private String dsDocumento;
	private int tgModointegridadeorc;
	private String dsPagamento;
	private String dtVencimento;
	
	public int getFkOrigem() 
	{
		return fkOrigem;
	}
	
	public void setFkOrigem(int fkOrigem) 
	{
		this.fkOrigem = fkOrigem;
	}
	
	public String getFkDocumento() 
	{
		return fkDocumento;
	}
	
	public void setFkDocumento(String fkDocumento) 
	{
		this.fkDocumento = fkDocumento;
	}
	
	public String getDsReferencia() 
	{
		return dsReferencia;
	}
	
	public void setDsReferencia(String dsReferencia) 
	{
		this.dsReferencia = dsReferencia;
	}
	
	public int getNrDocid() 
	{
		return nrDocid;
	}
	
	public void setNrDocid(int nrDocid) 
	{
		this.nrDocid = nrDocid;
	}
	
	public String getDtEmissao() 
	{
		return dtEmissao;
	}
	
	public void setDtEmissao(String dtEmissao) 
	{
		this.dtEmissao = dtEmissao;
	}
	
	public String getTgPessoa() 
	{
		return tgPessoa;
	}
	
	public void setTgPessoa(String tgPessoa) 
	{
		this.tgPessoa = tgPessoa;
	}
	
	public double getNrCnpj() 
	{
		return nrCnpj;
	}
	
	public void setNrCnpj(double nrCnpj) 
	{
		this.nrCnpj = nrCnpj;
	}
	
	public String getDsCliente() 
	{
		return dsCliente;
	}
	
	public void setDsCliente(String dsCliente) 
	{
		this.dsCliente = dsCliente;
	}
	
	public double getVlAcrescimo() 
	{
		return vlAcrescimo;
	}
	
	public void setVlAcrescimo(double vlAcrescimo) 
	{
		this.vlAcrescimo = vlAcrescimo;
	}
	
	public double getVlTotal() 
	{
		return vlTotal;
	}
	
	public void setVlTotal(double vlTotal) 
	{
		this.vlTotal = vlTotal;
	}
	
	public double getVlMovimento() 
	{
		return vlMovimento;
	}
	
	public void setVlMovimento(double vlMovimento) 
	{
		this.vlMovimento = vlMovimento;
	}
	
	public int getNrParcelas() 
	{
		return nrParcelas;
	}
	
	public void setNrParcelas(int nrParcelas) 
	{
		this.nrParcelas = nrParcelas;
	}
	
	public String getDsCmc7() 
	{
		return dsCmc7;
	}
	
	public void setDsCmc7(String dsCmc7) 
	{
		this.dsCmc7 = dsCmc7;
	}
	
	public int getNrBanco() 
	{
		return nrBanco;
	}
	
	public void setNrBanco(int nrBanco) 
	{
		this.nrBanco = nrBanco;
	}
	
	public int getNrAgencia() 
	{
		return nrAgencia;
	}
	
	public void setNrAgencia(int nrAgencia)
	{
		this.nrAgencia = nrAgencia;
	}
	
	public int getNrConta() 
	{
		return nrConta;
	}
	
	public void setNrConta(int nrConta) 
	{
		this.nrConta = nrConta;
	}
	
	public int getNrDigconta() 
	{
		return nrDigconta;
	}
	
	public void setNrDigconta(int nrDigconta) 
	{
		this.nrDigconta = nrDigconta;
	}
	
	public int getNrCheque() 
	{
		return nrCheque;
	}
	
	public void setNrCheque(int nrCheque) 
	{
		this.nrCheque = nrCheque;
	}
	
	public int getNrDigc1() 
	{
		return nrDigc1;
	}
	
	public void setNrDigc1(int nrDigc1) 
	{
		this.nrDigc1 = nrDigc1;
	}
	
	public int getNrDigc2() 
	{
		return nrDigc2;
	}
	
	public void setNrDigc2(int nrDigc2) 
	{
		this.nrDigc2 = nrDigc2;
	}
	
	public int getNrDigc3() 
	{
		return nrDigc3;
	}
	
	public void setNrDigc3(int nrDigc3) 
	{
		this.nrDigc3 = nrDigc3;
	}
	
	public String getDsAutorizacao() 
	{
		return dsAutorizacao;
	}
	
	public void setDsAutorizacao(String dsAutorizacao) 
	{
		this.dsAutorizacao = dsAutorizacao;
	}
	
	public String getNrNsu() 
	{
		return nrNsu;
	}
	
	public void setNrNsu(String nrNsu) 
	{
		this.nrNsu = nrNsu;
	}
	
	public int getTgInativo() 
	{
		return tgInativo;
	}
	
	public void setTgInativo(int tgInativo) 
	{
		this.tgInativo = tgInativo;
	}
	
	public String getTgOrigem() 
	{
		return tgOrigem;
	}
	
	public void setTgOrigem(String tgOrigem) 
	{
		this.tgOrigem = tgOrigem;
	}
	
	public String getDhInclusao() 
	{
		return dhInclusao;
	}
	
	public void setDhInclusao(String dhInclusao) 
	{
		this.dhInclusao = dhInclusao;
	}
	
	public String getDhAlteracao() 
	{
		return dhAlteracao;
	}
	
	public void setDhAlteracao(String dhAlteracao) 
	{
		this.dhAlteracao = dhAlteracao;
	}

	public String getDsFinalizadora() 
	{
		return dsFinalizadora;
	}
	
	public void setDsFinalizadora(String dsFinalizadora) 
	{
		this.dsFinalizadora = dsFinalizadora;
	}
	
	public String getFkTipodoc() 
	{
		return fkTipodoc;
	}
	
	public void setFkTipodoc(String fkTipodoc) 
	{
		this.fkTipodoc = fkTipodoc;
	}
	
	public String getDsDocumento() 
	{
		return dsDocumento;
	}
	
	public void setDsDocumento(String dsDocumento) 
	{
		this.dsDocumento = dsDocumento;
	}
	
	public int getTgModointegridadeorc() 
	{
		return tgModointegridadeorc;
	}
	
	public void setTgModointegridadeorc(int tgModointegridadeorc) 
	{
		this.tgModointegridadeorc = tgModointegridadeorc;
	}
	
	public String getDsPagamento() 
	{
		return dsPagamento;
	}
	
	public void setDsPagamento(String dsPagamento) 
	{
		this.dsPagamento = dsPagamento;
	}
	
	public String getDtVencimento() 
	{
		return dtVencimento;
	}
	
	public void setDtVencimento(String dtVencimento)
	{
		this.dtVencimento = dtVencimento;
	}
}