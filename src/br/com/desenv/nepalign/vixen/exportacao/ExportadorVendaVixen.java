package br.com.desenv.nepalign.vixen.exportacao;

import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.DesenvMSSQLConnector;
import br.com.desenv.nepalign.model.ItemPedidoVenda;
import br.com.desenv.nepalign.model.PagamentoPedidoVenda;
import br.com.desenv.nepalign.model.PedidoVenda;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.vixen.exportacao.service.PvFinanceiroService;
import br.com.desenv.nepalign.vixen.exportacao.service.VdItensOrcamentosService;
import br.com.desenv.nepalign.vixen.exportacao.service.VdOrcamentosService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class ExportadorVendaVixen
{
	public static boolean exportaCodigoBarras = false;
	
	public static void main(String[] args) throws Exception
	{
		if(args.length == 1)
			exportaCodigoBarras = args[0x00].equals("EXPORTAR_CODIGOBARRAS");
		
		final EntityManager manager = ConexaoUtil.getEntityManager();
		final DesenvMSSQLConnector msSQLConnector = new DesenvMSSQLConnector(DsvConstante.getParametrosSistema().get("SQL_HOSTNAME_VIXEN"),
				DsvConstante.getParametrosSistema().get("SQL_USERNAME_VIXEN"),
				DsvConstante.getParametrosSistema().get("SQL_PASSWORD_VIXEN"),
				DsvConstante.getParametrosSistema().get("SQL_DATABASE_VIXEN"));
		
		final VdOrcamentosService vdOrcamentosService = new VdOrcamentosService();
		final VdItensOrcamentosService vdItensOrcamentosService = new VdItensOrcamentosService();
		
		final PvFinanceiroService pvFinanceiroService = new PvFinanceiroService();
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Digite o número de controle: ");
		String numeroControle = scanner.nextLine();
		
		System.out.println("Deseja exportar o controle " + numeroControle + " ? (S/N) ");
		
		String confirmacao = scanner.nextLine(); 
		
		if(confirmacao.equals("S") || confirmacao.equals("s"))
		{
			PedidoVenda pedidoVenda = (PedidoVenda) manager.createNativeQuery("SELECT * FROM pedidoVenda WHERE numeroControle = '" + numeroControle + "';", PedidoVenda.class).getResultList().get(0);
			@SuppressWarnings("unchecked")
			List<ItemPedidoVenda> listaItemPedidoVenda = (List<ItemPedidoVenda>) manager.createNativeQuery("SELECT * FROM itemPedidoVenda WHERE idPedidoVenda = " + pedidoVenda.getId(), ItemPedidoVenda.class).getResultList();
			@SuppressWarnings("unchecked")
			List<PagamentoPedidoVenda> listaPagamentoPedidoVenda = (List<PagamentoPedidoVenda>) manager.createNativeQuery("SELECT * FROM pagamentoPedidoVenda WHERE idPedidoVenda = " + pedidoVenda.getId(), PagamentoPedidoVenda.class).getResultList();;
			
			System.out.println("Importando Pedido");
			vdOrcamentosService.gravarVixen(pedidoVenda);
			System.out.println("Importando Itens");
			vdItensOrcamentosService.gravarVixen(listaItemPedidoVenda);
			System.out.println("Importando Pagamentos");
			pvFinanceiroService.gravarVixen(listaPagamentoPedidoVenda);
			
			msSQLConnector.executeStoredProcedure("SPU_PV_INTEGRADAV");
			
			System.out.println("Controle " + numeroControle + " importado");
		}
		 
		msSQLConnector.disposeConn();
		scanner.close();
	}
}
