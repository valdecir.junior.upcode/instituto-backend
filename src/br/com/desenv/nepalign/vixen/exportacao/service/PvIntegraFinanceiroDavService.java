package br.com.desenv.nepalign.vixen.exportacao.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.PagamentoPedidoVenda;

public class PvIntegraFinanceiroDavService extends ImportacaoVixenService
{
	public PvIntegraFinanceiroDavService()
	{
		super();
	}
	
	public void gravarVixen(PagamentoPedidoVenda pPedidoVenda) throws SQLException
	{
		String systemQuery = "";
		try
		{
			int PK_ID_DAV = recuperarIdPedidoVenda(pPedidoVenda.getPedidoVenda().getNumeroControle());
			//int PK_ID_DOC = recuperarIdDocumento(pPedidoVenda.getPedidoVenda().getNumeroControle() + "" + pPedidoVenda.getItemSequencial());
			 
			systemQuery = "INSERT INTO PV_INTEGRAFINANCEIRO (FK_ORIGEM, FK_DOCUMENTO, DT_EMISSAO, VL_TOTAL, VL_ACRESCIMO, NR_PARCELAS, DS_AUTORIZACAO, DS_ULTDIGCARTAO) VALUES (" +
					"" + PK_ID_DAV + ", " + 
					"'" + getCodigoPagamento(pPedidoVenda.getFormaPagamento().getTipo()) + "'," + 
					"'" + IgnUtil.dateFormatSql.format(new Date()) + "'," + 
					"" + pPedidoVenda.getValor() + "," +
					"0.0," +
					"" + (pPedidoVenda.getFormaPagamento().getTipo() == 2 ? "1" : "null") + "," +
					"" + (pPedidoVenda.getFormaPagamento().getTipo() == 2 ? "1" : "null") + "," +
					"" + (pPedidoVenda.getFormaPagamento().getTipo() == 2 ? "0000" : "null") + ");";
			
			msSQLConnector.resultQuery(systemQuery, msSQLConnector.getNewStatement());
			System.out.println("Pagamento " + getCodigoPagamento(pPedidoVenda.getFormaPagamento().getTipo()) + " incluso no Vixen!");
		}
		catch(SQLException sqlExc)
		{
			sqlExc.printStackTrace();
			throw sqlExc;
		}
	}
	
	public void gravarVixen(List<PagamentoPedidoVenda> data) throws SQLException
	{
		try
		{
			for(final PagamentoPedidoVenda pagamentoPedidoVenda : data)
				gravarVixen(pagamentoPedidoVenda);
		}
		catch(SQLException sqlExc)
		{
			sqlExc.printStackTrace();
			throw new SQLException(sqlExc);
		}
	}
	
	@SuppressWarnings("unused")
	private int recuperarIdDocumento(String nr_orcamento) throws SQLException
	{
		Statement sqlStatement = msSQLConnector.getNewStatement();
		ResultSet sqlResult = sqlStatement.executeQuery("SELECT PK_ID FROM PV_INTEGRADOCUMENTOS WHERE CD_IMPORTACAO = '" + nr_orcamento + "'");
		
		if(sqlResult.next())
		{
			return Integer.parseInt(sqlResult.getObject("PK_ID").toString());
		}
		else
			return 0;
	}
	
	private int recuperarIdPedidoVenda(String nr_orcamento) throws SQLException
	{
		Statement sqlStatement = msSQLConnector.getNewStatement();
		ResultSet sqlResult = sqlStatement.executeQuery("SELECT PK_ID FROM PV_INTEGRADAV WHERE NR_ORCAMENTO = '" + nr_orcamento + "'");
		
		if(sqlResult.next())
		{
			return Integer.parseInt(sqlResult.getObject("PK_ID").toString());
		}
		else
			return 0;
	}
	
	private String getCodigoPagamento(int tipo)
	{
		//CT CARTAO DEBITO
		//CV CARTAO CREDITO
		//DI DINHEIRO
		
		if(tipo == 0x04) // debito
			return "CT";
		else if(tipo == 0x02) // credito
			return "CV";
		else
			return "DI"; //dinheiro
	}
}
