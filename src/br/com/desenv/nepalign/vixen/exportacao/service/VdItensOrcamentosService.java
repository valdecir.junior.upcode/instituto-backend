package br.com.desenv.nepalign.vixen.exportacao.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.ItemPedidoVenda;
import br.com.desenv.nepalign.vixen.exportacao.ExportadorVendaVixen;
import br.com.desenv.nepalign.vixen.exportacao.model.VdItensOrcamentos;

public class VdItensOrcamentosService extends ImportacaoVixenService 
{
	public VdItensOrcamentosService()
	{
		super();
	}
	
	public void gravarVixen(List<ItemPedidoVenda> data) throws SQLException
	{
		try
		{
			for(final ItemPedidoVenda itemPedidoVenda : data)
				gravarVixen(itemPedidoVenda);
		}
		catch(SQLException sqlExc)
		{
			sqlExc.printStackTrace();
		}
	}
	
	public void gravarVixen(ItemPedidoVenda itemPedidoVenda) throws SQLException
	{
		String systemQuery = "";
		try
		{
			if(!existeRegistro(itemPedidoVenda))
			{
				VdItensOrcamentos pedido = new VdItensOrcamentos();
				pedido.setDhAlteracao(IgnUtil.dateFormatSql.format(new Date()));
				pedido.setDhInclusao(IgnUtil.dateFormatSql.format(new Date()));
				pedido.setDsCor(itemPedidoVenda.getEstoqueProduto().getCor().getDescricaoCorEmpresa());
				pedido.setDsModelo(itemPedidoVenda.getEstoqueProduto().getProduto().getNomeProduto());
				pedido.setDsUnidade(itemPedidoVenda.getEstoqueProduto().getProduto().getUnidadeMedida().getSigla().substring(0, 1));
				pedido.setFkCor(String.valueOf(itemPedidoVenda.getEstoqueProduto().getCor().getId()));
				pedido.setFkEmpresa("");			
				pedido.setFkOrcamento(recuperarIdPedidoVenda(itemPedidoVenda.getPedidoVenda().getNumeroControle()));
				
				if(ExportadorVendaVixen.exportaCodigoBarras)
					pedido.setFkProduto(String.valueOf(itemPedidoVenda.getEstoqueProduto().getId()));
				else
					pedido.setFkProduto(String.valueOf(itemPedidoVenda.getEstoqueProduto().getProduto().getId()));
				
				pedido.setFkTamanho(String.valueOf(itemPedidoVenda.getEstoqueProduto().getTamanho().getId()));
				pedido.setQtOrcamento(itemPedidoVenda.getQuantidade());
				pedido.setVlAcrescimo(0);
				pedido.setVlDesconto(0);
				pedido.setVlPrecobase(itemPedidoVenda.getPrecoVenda());
				pedido.setVlPretot((itemPedidoVenda.getPrecoVenda() * itemPedidoVenda.getQuantidade()));
				pedido.setVlUnitario(itemPedidoVenda.getPrecoVenda());
				
				systemQuery = "INSERT INTO VD_ITENSORCAMENTOS(fk_orcamento, fk_produto,fk_cor,ds_cor," +
						"fk_tamanho,ds_unidade,ds_modelo,qt_orcamento,vl_unitario,vl_desconto,vl_acrescimo," +
						"vl_pretot,dh_inclusao,vl_precobase,dh_alteracao,fk_empresa)" +
						" values(" + 
				pedido.getFkOrcamento() + ",'" +
				pedido.getFkProduto() + "','" +
				(pedido.getFkCor() != null ? ((pedido.getFkCor().length() < 1) ? "" : pedido.getFkCor()) : "") + "','" + 
				pedido.getDsCor() + "','" +
				(pedido.getFkTamanho() != null ? ((pedido.getFkTamanho().length() > 3) ? pedido.getFkTamanho().substring(0,3) : pedido.getFkTamanho()) : "") + "','" +
				pedido.getDsUnidade() + "','" +
				(pedido.getDsModelo() != null ? ((pedido.getDsModelo().length() > 40) ? pedido.getDsModelo().substring(0,40) : pedido.getDsModelo()) : "") + "'," +
				pedido.getQtOrcamento() + "," +
				pedido.getVlUnitario() + "," +
				pedido.getVlDesconto() + "," + 
				pedido.getVlAcrescimo() + "," +
				pedido.getVlPretot() + "," +
				pedido.getDhInclusao() + "," +
				pedido.getVlPrecobase() + "," +
				pedido.getDhAlteracao() + ",'" + 
				pedido.getFkEmpresa() + "')";
				msSQLConnector.resultQuery(systemQuery, msSQLConnector.getNewStatement());
				System.out.println("Item do Pedido [" + itemPedidoVenda.getPedidoVenda().getNumeroControle() + "] incluso no Vixen!");
			}
		}
		catch(SQLException sqlExc)
		{
			Logger.getRootLogger().error(systemQuery, sqlExc);
		}
		finally
		{
			msSQLConnector.disposeConn();
		}
	}
	
	public Boolean existeRegistro(ItemPedidoVenda iPedidoVenda) throws SQLException
	{
		try
		{
			Statement sqlStatement = msSQLConnector.getNewStatement();
			ResultSet sqlResult = sqlStatement.executeQuery("SELECT FK_ORCAMENTO FROM VD_ITENSORCAMENTOS WHERE FK_ORCAMENTO = " + iPedidoVenda.getPedidoVenda().getNumeroControle());
			
			if(sqlResult.next())
				return true;
			else
				return false;
		}
		catch(Exception newEXC)
		{
			newEXC.printStackTrace();
			return true;
		}
	}
	
	public int recuperarIdPedidoVenda(String nr_orcamento) throws SQLException
	{
		Statement sqlStatement = msSQLConnector.getNewStatement();
		ResultSet sqlResult = sqlStatement.executeQuery("SELECT PK_ID FROM VD_ORCAMENTOS WHERE NR_ORCAMENTO = '" + nr_orcamento + "'");
		
		if(sqlResult.next())
		{
			return Integer.parseInt(sqlResult.getObject("PK_ID").toString());
		}
		else
			return 0;
	}
	
	public String recuperarIdProduto(int idInterno) throws SQLException
	{
		Statement sqlStatement = msSQLConnector.getNewStatement();
		ResultSet sqlResult = sqlStatement.executeQuery("SELECT PK_ID FROM TB_PRODUTOS WHERE DS_CODFABRICA = '" + idInterno + "'");
		
		if(sqlResult.next())
		{
			return sqlResult.getObject("PK_ID").toString();
		}
		else
			return "0";
	}
}