package br.com.desenv.nepalign.vixen.exportacao.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.PedidoVenda;
import br.com.desenv.nepalign.vixen.exportacao.model.VdOrcamentos;

public class VdOrcamentosService extends ImportacaoVixenService 
{
	public VdOrcamentosService()
	{
		super();
	}
	
	public void gravarVixen(List<PedidoVenda> data) throws SQLException
	{
		Boolean sucess = true;
		
		try
		{
			for(final PedidoVenda pedidoVenda : data)
				gravarVixen(pedidoVenda);
		}
		catch(SQLException sqlExc)
		{
			sucess = false;
			sqlExc.printStackTrace();
		}
		finally
		{
			msSQLConnector.disposeConn();
			
			if(sucess)
				System.out.println("Sucesso! Todos os itens foram importados!");
		}
	}
	
	public void gravarVixen(PedidoVenda pedidoVenda) throws SQLException
	{
		String systemQuery = "";
		
		try
		{
			if(!existeRegistro(pedidoVenda))
			{
				VdOrcamentos vdOrcamentos = new VdOrcamentos();
				vdOrcamentos.setDhAlteracao(IgnUtil.dateFormatSql.format(new Date()));
				vdOrcamentos.setDhInclusao(IgnUtil.dateFormatSql.format(new Date()));
				vdOrcamentos.setDtOrcamento(IgnUtil.dateFormatSql.format(pedidoVenda.getDataVenda()));
				vdOrcamentos.setFkCadunico(1); //codigo cliente
				vdOrcamentos.setFkPedido(0);
				vdOrcamentos.setFkVendedor("1");
				//vdOrcamentos.setNrOrcamento(String.valueOf(pedidoVenda.getId()));
				vdOrcamentos.setNrOrcamento(pedidoVenda.getNumeroControle());
				vdOrcamentos.setTgInativo(0);
				vdOrcamentos.setTgModointegridade(0);
				vdOrcamentos.setTgOrigem("O");
				vdOrcamentos.setTgPdv(1);
				vdOrcamentos.setVlDesconto(0);
				vdOrcamentos.setVlDescprevisao(0);
				vdOrcamentos.setVlPordescprecisao(0);
				vdOrcamentos.setVlTotal(pedidoVenda.getValorTotalPedido());	
				vdOrcamentos.setFkEmpresa("01");
				
				
				systemQuery = "INSERT INTO VD_ORCAMENTOS(nr_orcamento," +
				"fk_empresa,fk_cadunico,fk_vendedor,vl_total,vl_desconto,dt_orcamento,dh_inclusao," +
				"dh_alteracao,tg_origem,tg_pdv,VL_PORDESCPREVISAO,vl_descprevisao,tg_inativo,fk_pedido," +
				"tg_modointegridade) values('" +
				((vdOrcamentos.getNrOrcamento() != null) ? ((vdOrcamentos.getNrOrcamento().length() > 12) ? vdOrcamentos.getNrOrcamento().substring(0,12) : vdOrcamentos.getNrOrcamento()) : "0" ) + "','" + 
				((vdOrcamentos.getFkEmpresa() != null) ? ((vdOrcamentos.getFkEmpresa().length() > 3) ? vdOrcamentos.getFkEmpresa().substring(0,3): vdOrcamentos.getFkEmpresa()) : "0") + "'," +
				vdOrcamentos.getFkCadunico() + ",'" +
				vdOrcamentos.getFkVendedor() + "'," +
				vdOrcamentos.getVlTotal() + "," + 
				vdOrcamentos.getVlDesconto() + "," + 
				vdOrcamentos.getDtOrcamento() + "," +
				vdOrcamentos.getDhInclusao() + "," +
				vdOrcamentos.getDhAlteracao() + ",'" +
				vdOrcamentos.getTgOrigem() + "'," +
				vdOrcamentos.getTgPdv() + "," +
				vdOrcamentos.getVlPordescprecisao() + "," + 
				vdOrcamentos.getVlDescprevisao() + "," +
				vdOrcamentos.getTgInativo() + "," +
				vdOrcamentos.getFkPedido() + "," +
				vdOrcamentos.getTgModointegridade() + ")";
				
				try
				{
					msSQLConnector.resultQuery(systemQuery, msSQLConnector.getNewStatement());	
				}
				catch(Exception ex)
				{
					Logger.getRootLogger().error(systemQuery, ex);
				}
				
				System.out.println("Orçamento [" + vdOrcamentos.getNrOrcamento() + "] incluso no Vixen!");
			}
		}
		catch(SQLException sqlExc)
		{
			sqlExc.printStackTrace();
		}
	}
	
	public Boolean existeRegistro(PedidoVenda bufferVenda) throws SQLException
	{
		Statement sqlStatement = msSQLConnector.getNewStatement();
		ResultSet sqlResult = sqlStatement.executeQuery("SELECT NR_ORCAMENTO FROM VD_ORCAMENTOS WHERE NR_ORCAMENTO = '" + bufferVenda.getNumeroControle() + "'");
		
		if(sqlResult.next())
			return true;
		else
			return false;
	}
}
