package br.com.desenv.nepalign.vixen.exportacao.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.PagamentoPedidoVenda;
import br.com.desenv.nepalign.model.PedidoVenda;
import br.com.desenv.nepalign.vixen.exportacao.model.PvFinanceiro;

public class PvFinanceiroService extends ImportacaoVixenService
{
	public PvFinanceiroService()
	{
		super();
	}
	
	public void gravarVixen(PagamentoPedidoVenda pPedidoVenda) throws SQLException
	{
		String systemQuery = "";
		try
		{
			/* ------------------*/
			if(!existeRegistro(pPedidoVenda.getPedidoVenda()))
			{
				PvFinanceiro ppvFinanceiro = new PvFinanceiro();
				ppvFinanceiro.setDhAlteracao(IgnUtil.dateFormatSql.format(new Date()));
				ppvFinanceiro.setDhInclusao(IgnUtil.dateFormatSql.format(new Date()));
				ppvFinanceiro.setDsAutorizacao("");
				ppvFinanceiro.setDsCliente("CONSUMIDOR"); //??
				ppvFinanceiro.setDsCmc7("");
				ppvFinanceiro.setDsDocumento(pPedidoVenda.getFormaPagamento().getDescricao());
				ppvFinanceiro.setDsFinalizadora(pPedidoVenda.getFormaPagamento().getDescricao());
				ppvFinanceiro.setDsPagamento(pPedidoVenda.getFormaPagamento().getDescricao());
				ppvFinanceiro.setDsReferencia(pPedidoVenda.getFormaPagamento().getDescricao());
				ppvFinanceiro.setDtEmissao(IgnUtil.dateFormatSql.format(pPedidoVenda.getDataPagamento()));
				ppvFinanceiro.setDtVencimento(IgnUtil.dateFormatSql.format(pPedidoVenda.getDataPagamento()));
				ppvFinanceiro.setFkDocumento(getCodigoPagamento(pPedidoVenda.getFormaPagamento().getDescricao()).toString());
				ppvFinanceiro.setFkOrigem(recuperarIdPedidoVenda(pPedidoVenda.getPedidoVenda().getNumeroControle()));
				ppvFinanceiro.setFkTipodoc(getCodigoPagamento(pPedidoVenda.getFormaPagamento().getDescricao()).toString());
				ppvFinanceiro.setNrAgencia(0);
				ppvFinanceiro.setNrBanco(0);
				ppvFinanceiro.setNrCheque(0);
				ppvFinanceiro.setNrCnpj(0); //cpf
				ppvFinanceiro.setNrConta(0);
				ppvFinanceiro.setNrDigc1(0);
				ppvFinanceiro.setNrDigc2(0);
				ppvFinanceiro.setNrDigc3(0);
				ppvFinanceiro.setNrDigconta(0);
				ppvFinanceiro.setNrDocid(Integer.parseInt(pPedidoVenda.getNumeroDocumento().replace("-", "")));
				ppvFinanceiro.setNrNsu("0");
				ppvFinanceiro.setNrParcelas(0);
				ppvFinanceiro.setTgInativo(0);
				ppvFinanceiro.setTgModointegridadeorc(0);
				ppvFinanceiro.setTgOrigem("O");
				ppvFinanceiro.setTgPessoa("");
				ppvFinanceiro.setVlAcrescimo(0);
				ppvFinanceiro.setVlMovimento(pPedidoVenda.getValor());
				ppvFinanceiro.setVlTotal(pPedidoVenda.getValor());				
				
				systemQuery = "INSERT INTO PV_FINANCEIRO(fk_origem,fk_documento,ds_referencia," +
						"nr_docid,dt_emissao,tg_pessoa,ds_cliente,vl_acrescimo,vl_total," +
						"vl_movimento,nr_parcelas,ds_cmc7,nr_banco,nr_agencia,nr_conta,nr_digconta," +
						"nr_cheque,nr_digc1,nr_digc2,nr_digc3,ds_autorizacao,nr_nsu,tg_inativo," +
						"tg_origem,dh_inclusao,dh_alteracao,ds_finalizadora,fk_tipodoc,ds_documento," +
						"tg_modointegridadeorc,ds_pagamento,dt_vencimento)" +
						" values(" + 
				ppvFinanceiro.getFkOrigem() + ",'" +
				ppvFinanceiro.getFkDocumento() + "','" +
				ppvFinanceiro.getDsReferencia() + "'," + 
				ppvFinanceiro.getNrDocid() + "," + 
				ppvFinanceiro.getDtEmissao() + ",'" +
				ppvFinanceiro.getTgPessoa() + "','" +
				ppvFinanceiro.getDsCliente() + "'," +
				ppvFinanceiro.getVlAcrescimo() + "," +
				ppvFinanceiro.getVlTotal() + "," +
				ppvFinanceiro.getVlMovimento() + "," +
				ppvFinanceiro.getNrParcelas() + ",'" +
				ppvFinanceiro.getDsCmc7() + "'," +
				ppvFinanceiro.getNrBanco() + "," +
				ppvFinanceiro.getNrAgencia() + "," +
				ppvFinanceiro.getNrConta() + "," + 
				ppvFinanceiro.getNrDigconta() + "," +
				ppvFinanceiro.getNrCheque() + "," +
				ppvFinanceiro.getNrDigc1() + "," +
				ppvFinanceiro.getNrDigc2() + "," + 
				ppvFinanceiro.getNrDigc3() + ",'" +
				ppvFinanceiro.getDsAutorizacao() + "','" +
				ppvFinanceiro.getNrNsu() + "'," +
				ppvFinanceiro.getTgInativo() + ",'" +
				ppvFinanceiro.getTgOrigem() + "'," +
				ppvFinanceiro.getDhInclusao() + "," + 
				ppvFinanceiro.getDhAlteracao() + ",'" +
				ppvFinanceiro.getDsFinalizadora() + "','" +
				ppvFinanceiro.getFkTipodoc() + "','" + 
				ppvFinanceiro.getDsDocumento() + "'," +
				ppvFinanceiro.getTgModointegridadeorc() + ",'" +
				ppvFinanceiro.getDsPagamento() + "'," +
				ppvFinanceiro.getDtVencimento() + ")";
				msSQLConnector.resultQuery(systemQuery, msSQLConnector.getNewStatement());
				System.out.println("Pagamento " + ppvFinanceiro.getNrDocid() + "] incluso no Vixen!");
			}
		}
		catch(SQLException sqlExc)
		{
			sqlExc.printStackTrace();
			throw sqlExc;
		}
	}
	
	public void gravarVixen(List<PagamentoPedidoVenda> data) throws SQLException
	{
		try
		{
			for(final PagamentoPedidoVenda pagamentoPedidoVenda : data)
				gravarVixen(pagamentoPedidoVenda);
		}
		catch(SQLException sqlExc)
		{
			sqlExc.printStackTrace();
			throw new SQLException(sqlExc);
		}
	}
	
	public Integer getCodigoPagamento(String descriSirius)
	{
		Hashtable<String, Integer> formas = new Hashtable<String, Integer>();
		formas.put("DINHEIRO", 1);
		formas.put("CHEQUE", 2);
		formas.put("CARTAO", 3);
		formas.put("VALE-TROCA", 4);
		
		if(formas.containsKey(descriSirius))
		{
			return formas.get(descriSirius);
		}
		else
			return 5;
	}
	
	public Boolean existeRegistro(PedidoVenda bufferVenda) throws SQLException
	{
		Statement sqlStatement = msSQLConnector.getNewStatement();
		ResultSet sqlResult = sqlStatement.executeQuery("SELECT FK_ORIGEM FROM PV_FINANCEIRO WHERE FK_ORIGEM = " + recuperarIdPedidoVenda(bufferVenda.getNumeroControle()) + "");
		
		if(sqlResult.next())
			return true;
		else
			return false;
	}
	
	public int recuperarIdPedidoVenda(String nr_orcamento) throws SQLException
	{
		Statement sqlStatement = msSQLConnector.getNewStatement();
		ResultSet sqlResult = sqlStatement.executeQuery("SELECT PK_ID FROM VD_ORCAMENTOS WHERE NR_ORCAMENTO = '" + nr_orcamento + "'");
		
		if(sqlResult.next())
		{
			return Integer.parseInt(sqlResult.getObject("PK_ID").toString());
		}
		else
			return 0;
	}
}
