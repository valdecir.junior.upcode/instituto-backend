package br.com.desenv.nepalign.vixen.exportacao.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.desenv.nepalign.model.ItemPedidoVenda;
import br.com.desenv.nepalign.vixen.exportacao.ExportadorVendaVixenDAV;

public class PvIntegraItensDavService extends ImportacaoVixenService 
{
	public PvIntegraItensDavService()
	{
		super();
	}
	
	public void gravarVixen(List<ItemPedidoVenda> data) throws SQLException
	{
		try
		{
			for(final ItemPedidoVenda itemPedidoVenda : data)
				gravarVixen(itemPedidoVenda);
			
			dispose();
		}
		catch(SQLException sqlExc)
		{
			sqlExc.printStackTrace();
		}
	}
	
	public void gravarVixen(ItemPedidoVenda itemPedidoVenda) throws SQLException
	{
		String systemQuery = "";
		try
		{
			int PK_ID = recuperarIdPedidoVenda(itemPedidoVenda.getPedidoVenda().getNumeroControle());
			
			if(PK_ID == 0)
				Logger.getRootLogger().error("Não foi possível achar o orçamento do " + itemPedidoVenda.getPedidoVenda().getNumeroControle());
			else
			{
				systemQuery = "INSERT INTO PV_INTEGRAITENSDAV (FK_ORCAMENTO, FK_PRODUTO, QT_ORCAMENTO, VL_UNITARIO, VL_DESCONTO, VL_ACRESCIMO) VALUES (" +
						PK_ID + "," + 
						(ExportadorVendaVixenDAV.exportaCodigoBarras ? itemPedidoVenda.getEstoqueProduto().getId() : itemPedidoVenda.getProduto().getId()) + "," +
						String.valueOf(itemPedidoVenda.getQuantidade()) + "," +
						String.valueOf(itemPedidoVenda.getPrecoVenda()) + "," +
						"0.0" + "," +
						"0.0)";
				msSQLConnector.resultQuery(systemQuery, msSQLConnector.getNewStatement());
				System.out.println("Item do Pedido [" + itemPedidoVenda.getPedidoVenda().getNumeroControle() + "] incluso no Vixen!");	
			}
		}
		catch(SQLException sqlExc)
		{
			Logger.getRootLogger().error(systemQuery, sqlExc);
		}
	}
	
	private int recuperarIdPedidoVenda(String nr_orcamento) throws SQLException
	{
		Statement sqlStatement = msSQLConnector.getNewStatement();
		ResultSet sqlResult = sqlStatement.executeQuery("SELECT PK_ID FROM PV_INTEGRADAV WHERE NR_ORCAMENTO = '" + nr_orcamento + "'");
		
		if(sqlResult.next())
		{
			return Integer.parseInt(sqlResult.getObject("PK_ID").toString());
		}
		else
			return 0;
	}
}