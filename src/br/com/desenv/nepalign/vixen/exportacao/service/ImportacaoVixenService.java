package br.com.desenv.nepalign.vixen.exportacao.service;

import java.sql.SQLException;

import br.com.desenv.frameworkignorante.DesenvMSSQLConnector;
import br.com.desenv.nepalign.util.DsvConstante;

public abstract class ImportacaoVixenService
{
	protected DesenvMSSQLConnector msSQLConnector;
	
	public ImportacaoVixenService()
	{
	}
	
	public void openConnection() throws Exception
	{
		msSQLConnector = new DesenvMSSQLConnector(DsvConstante.getParametrosSistema().get("SQL_HOSTNAME_VIXEN"),
				DsvConstante.getParametrosSistema().get("SQL_USERNAME_VIXEN"),
				DsvConstante.getParametrosSistema().get("SQL_PASSWORD_VIXEN"),
				DsvConstante.getParametrosSistema().get("SQL_DATABASE_VIXEN"));
	}
	
	public void dispose() throws SQLException
	{
		msSQLConnector.disposeConn();
	}
}
