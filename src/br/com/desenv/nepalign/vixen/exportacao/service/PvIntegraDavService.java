package br.com.desenv.nepalign.vixen.exportacao.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.PedidoVenda;

public class PvIntegraDavService extends ImportacaoVixenService 
{
	public PvIntegraDavService()
	{
		super();
	}
	
	public void gravarVixen(List<PedidoVenda> data) throws SQLException
	{
		Boolean sucess = true;
		
		try
		{
			for(final PedidoVenda pedidoVenda : data)
				gravarVixen(pedidoVenda);
		}
		catch(SQLException sqlExc)
		{
			sucess = false;
			sqlExc.printStackTrace();
		}
		finally
		{
			msSQLConnector.disposeConn();
			
			if(sucess)
				System.out.println("Sucesso! Todos os itens foram importados!");
		}
	}
	
	public void gravarVixen(PedidoVenda pedidoVenda) throws SQLException
	{
		gravarVixen(pedidoVenda, null);
	}
	
	public void gravarVixen(PedidoVenda pedidoVenda, String cpfCnpj) throws SQLException
	{
		String systemQuery = "";
		
		try
		{
			if(cpfCnpj != null && cpfCnpj.equals(""))
				cpfCnpj = null;
			
			if(!existeRegistro(pedidoVenda))
			{	
				systemQuery = "INSERT INTO PV_INTEGRADAV (nr_orcamento, fk_Empresa, dt_orcamento, tg_inativo, tg_sysacao, tg_origem, NR_CNPJ) VALUES (" + 
						"'" + pedidoVenda.getNumeroControle() + "'," +
						"'01'," + 
						"'" + IgnUtil.dateFormatSql.format(pedidoVenda.getDataVenda()) + "'," + 
						"0," +
						"'A'," +
						"'O'," +
						"" + (cpfCnpj == null ? "null" : cpfCnpj.replace("\\.", "").replace("-", "")) + ");";
				try
				{
					msSQLConnector.resultQuery(systemQuery, msSQLConnector.getNewStatement());
					System.out.println("Orçamento [" + pedidoVenda.getNumeroControle() + "] incluso no Vixen!");
				}
				catch(Exception ex)
				{
					throw ex;
				}
			}
		}
		catch(SQLException sqlExc)
		{
			throw sqlExc;
		}
	}
	
	private final Boolean existeRegistro(final PedidoVenda bufferVenda) throws SQLException
	{
		Statement sqlStatement = msSQLConnector.getNewStatement();
		ResultSet sqlResult = sqlStatement.executeQuery("SELECT NR_ORCAMENTO FROM PV_INTEGRADAV WHERE NR_ORCAMENTO = '" + bufferVenda.getNumeroControle() + "'");
		
		if(sqlResult.next())
			return true;
		else
			return false;
	}
}