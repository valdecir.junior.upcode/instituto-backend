package br.com.desenv.nepalign.vixen.exportacao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.desenv.frameworkignorante.DesenvMSSQLConnector;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

import com.desenvtec.vixen.exportacao.TbProdutos;
import com.kscbrasil.constante.KscConstante;
import com.kscbrasil.lib.type.KscDate;
import com.kscbrasil.lib.type.KscDouble;
import com.kscbrasil.lib.type.KscString;
import com.kscbrasil.produto.model.Produto;

public class ExportarProdutoVixen 
{
	static boolean exportaCodigoBarras = false;
	
	public static DesenvMSSQLConnector msSQLConnector;
	public static final String NCM_PADRAO = "64059000";
	
	public static void main(String[] args) throws Exception
	{
		if(args.length == 0x00)
		{
			System.out.println("Escolha o método de importação: ARQUIVO_CACHE,BANCO_NEPAL");
			System.exit(0);
			return;
		}
		
		if(args.length == 2)
			exportaCodigoBarras = args[0x01].equals("EXPORTAR_CODIGOBARRAS");
		
		new ExportarProdutoVixen().run(args[0x00]);
	}
	
	public void run(final String metodo)
	{
		try
		{
			System.out.println("Carregando constantes...");
			System.out.println("Usa código de barras ? " + exportaCodigoBarras);
			
			if(metodo.equals("BANCO_NEPAL"))
				DsvConstante.getParametrosSistema();
			else
				KscConstante.carregarConstante();
			
			System.out.println("Iniciando conexão com o Vixen...");
			
			try
			{
				if(metodo.equals("BANCO_NEPAL"))
				{
					//"PC2\\MSSQLSERVER:1433"
					msSQLConnector = new DesenvMSSQLConnector(DsvConstante.getParametrosSistema().get("SQL_HOSTNAME_VIXEN"),
							DsvConstante.getParametrosSistema().get("SQL_USERNAME_VIXEN"),
							DsvConstante.getParametrosSistema().get("SQL_PASSWORD_VIXEN"),
							DsvConstante.getParametrosSistema().get("SQL_DATABASE_VIXEN"));
				}
				else
				{
					msSQLConnector = new DesenvMSSQLConnector(KscConstante.propriedadesSistema.getProperty("SERVIDOR_SQL_VIXEN"),
							KscConstante.propriedadesSistema.getProperty("SQL_USERNAME_VIXEN"),
							KscConstante.propriedadesSistema.getProperty("SQL_PASSWORD_VIXEN"),
							KscConstante.propriedadesSistema.getProperty("SQL_DATABASE_VIXEN"));
				}
			}
			catch(Exception ex)
			{
				System.out.println("Não foi possível fazer conexão com o Vixen... Verifique o arquivo de constantes.");
				return; 
			}

			System.out.println("Conexão OK.");
			
			if(metodo.equals("BANCO_NEPAL"))
			{
				if(DsvConstante.getParametrosSistema().get("LIMPAR_PRODUTOS_VIXEN_ANTES_EXPORTACAO").equals("S"))
				{
					System.out.println("Limpando produtos antes de começar a exportação.");
					msSQLConnector.getNewStatement().execute("DELETE FROM TB_PRODUTOS");
					msSQLConnector.getNewStatement().execute("DELETE FROM TB_CLAFIS");
					System.out.println("Tabela de produtos zerada.");
				}
			}
			else
			{
				if(KscConstante.propriedadesSistema.getProperty("LIMPAR_PRODUTOS_VIXEN_ANTES_EXPORTACAO").equals("S"))
				{
					System.out.println("Limpando produtos antes de começar a exportação.");
					msSQLConnector.getNewStatement().execute("DELETE FROM TB_PRODUTOS");
					msSQLConnector.getNewStatement().execute("DELETE FROM TB_CLAFIS");
					System.out.println("Tabela de produtos zerada.");
				}
			}

			
			final List<Produto> data = new ArrayList<Produto>();
			
			if(metodo.equals("ARQUIVO_CACHE"))
				data.addAll(buscarProdutosArquivoCache());
			else if(metodo.equals("BANCO_NEPAL"))
				data.addAll(buscarProdutosNepal());
			else
				System.out.println("Método de busca de produtos não encontrado!!!");
			
			System.out.println(data.size() + " Produtos encontrados no Nepal...");
			
			if(data.size() > 0x00)
				gravarProdutosVixen(data);
		}
		catch(Exception exc)
		{
			exc.printStackTrace();
		}
		finally
		{
			System.out.println("Finalizado");
		}
	}
	
	private final List<Produto> buscarProdutosArquivoCache() throws Exception
	{
		final ArrayList<Produto> data = new ArrayList<Produto>();
		
		System.out.println("Começaando escaneamento do arquivo...");
		
		if(!new File("C:/CADASTROS/PRODUTOS").exists())
		{
			System.out.println("Arquivo [C:/CADASTROS/PRODUTOS] não encontrado.");
			return data;
		}
		
		BufferedReader bufferedReader = new BufferedReader(new FileReader("C:/CADASTROS/PRODUTOS"));
		
		System.out.println("Arquivo encontrado...");
		
		String produto = "";
		
		System.out.println("Lendo arquivo....");
		
		while((produto = bufferedReader.readLine()) != null)
		{
			if(!produto.equals("FIM"))
			{
				try
				{
					String[] infoProdutos = produto.split(";");
					
					String codBarras = infoProdutos[0];
					String referencia = infoProdutos[1];
					String nomeProduto = infoProdutos[2];
					String valorVenda = infoProdutos[3];
					String ncmProduto = null;
					
					if(infoProdutos.length == 0x05)
						ncmProduto = infoProdutos[4];
					else
						ncmProduto = NCM_PADRAO;
					
					if(ncmProduto.equals("") || ncmProduto.equals("0") || ncmProduto.length() != 8)
						ncmProduto = NCM_PADRAO;
					 
					Produto kscProduto = new Produto();
					
					if(exportaCodigoBarras)
						kscProduto.setIdProduto(new KscString(codBarras));
					else
						kscProduto.setIdProduto(new KscString(referencia));
					
					kscProduto.setDescricao(new KscString(nomeProduto));
					kscProduto.setCodigoEan(new KscString(codBarras));
					kscProduto.setCodigoInterno(new KscString(referencia));
					kscProduto.setCodigoFornecedor(new KscString(ncmProduto));
					kscProduto.setValorPadraoVenda(new KscDouble(valorVenda.replace(".", ",")));
					
					data.add(kscProduto);	
				}
				catch(Exception exr)
				{
					System.out.println(">>" + produto);
					bufferedReader.close();
					throw exr;
				}
			}
			else
				break;
		}
		
		bufferedReader.close();
		
		return data;
	}
	
	private final List<Produto> buscarProdutosNepal() throws Exception
	{
		final ArrayList<Produto> data = new ArrayList<Produto>();
		
		System.out.println("Começando escaneamento do banco de dados...");
		
		ResultSet resultSet = ConexaoUtil.getConexaoPadrao().createStatement().executeQuery(
			"	SELECT " + 
			"		estoqueProduto.idEstoqueProduto as 'codigobarras', " +
			"		estoqueProduto.idProduto as 'referencia', " +
			"		produto.nomeProduto as 'descricao', " +
			"		saldoestoqueproduto.venda as 'venda', " +
			"		produto.codigoNcm as 'ncm' " +
			"	FROM " +
			"	saldoestoqueproduto " +
			"	INNER JOIN " +
			"		estoqueProduto ON estoqueProduto.idEstoqueProduto = saldoEstoqueProduto.idEstoqueProduto " +
			"	INNER JOIN " +
			"		produto ON produto.idProduto = estoqueProduto.idProduto " +
			"	WHERE " +
			"		saldoEstoqueProduto.ultimoEvento BETWEEN DATE_SUB(NOW(), INTERVAL 5 DAY) AND NOW();");
		
		System.out.println("Lendo registros....");
		
		while(resultSet.next())
		{
			String ncmProduto = resultSet.getString("ncm");
			
			if(ncmProduto == null || ncmProduto.isEmpty() || ncmProduto.equals("0"))
				ncmProduto = NCM_PADRAO;
			 
			Produto kscProduto = new Produto();
			
			if(exportaCodigoBarras)
				kscProduto.setIdProduto(new KscString(resultSet.getString("codigobarras")));
			else
				kscProduto.setIdProduto(new KscString(resultSet.getString("referencia")));
			
			kscProduto.setDescricao(new KscString(resultSet.getString("descricao")));
			kscProduto.setCodigoEan(new KscString(resultSet.getString("codigobarras")));
			
			if(exportaCodigoBarras)
				kscProduto.setCodigoInterno(new KscString(resultSet.getString("codigobarras")));
			else
				kscProduto.setCodigoInterno(new KscString(resultSet.getString("referencia")));
			
			kscProduto.setCodigoFornecedor(new KscString(ncmProduto));
			kscProduto.setValorPadraoVenda(new KscDouble(resultSet.getString("venda").replace(".", ",")));
			
			data.add(kscProduto);	
		}
		
		return data;
	}
	
	private void gravarProdutosVixen(List<Produto> data) throws Exception
	{
		System.out.println("Gravando no Vixen...");
		
		int countProdutosGravados = 0;
		int countProdutosJaExistentes = 0;
		
		String systemQuery = "";
			
		for(int i = 0x00; i < data.size(); i++)
		{
			Produto p = (Produto) data.get(i);
			
			int idClafis = -0x01;
			
			if(!p.getCodigoFornecedor().getValue().equals(""))
				idClafis = tratarNcm(p.getCodigoFornecedor());
			
			if(p.getValorPadraoVenda().getValue() > 999)
				p.setValorPadraoVenda(new KscDouble(999));
			
			TbProdutos bufferProduto = new TbProdutos();
			bufferProduto.setCdBarras(p.getCodigoEan().getValue());
			bufferProduto.setCdImportacao(p.getCodigoInterno().getValue());
			bufferProduto.setPkId(p.getCodigoInterno().getValue());
			bufferProduto.setDsModelo(p.getDescricao().getValue());
			bufferProduto.setDsUnidade("UN");
			bufferProduto.setDhAlteracao(new KscDate(new java.util.Date()).getSqlValue());
			bufferProduto.setDhInclusao(new KscDate(new java.util.Date()).getSqlValue());
			bufferProduto.setDsCodfabrica(p.getCodigoInterno().getValue());
			bufferProduto.setDsEmpresa("");
			bufferProduto.setNrSittribpdv("40");
			bufferProduto.setNrSittribpdvaux("40");
			bufferProduto.setTgBrinde(0);
			bufferProduto.setTgInativo(0);
			bufferProduto.setTgNaof4venda(0);
			bufferProduto.setTgOrigemICMS("0");
			bufferProduto.setTgPdvnaogerapontuacao(1);
			bufferProduto.setTgServico(0);
			bufferProduto.setVlDescmax(p.getValorPadraoVenda().doubleValue());
			bufferProduto.setVlDescpromo(0.0);
			bufferProduto.setVlPoricms(0);
			bufferProduto.setVlPoriss(0);
			bufferProduto.setVlPreta1(p.getValorPadraoVenda().doubleValue());
			
			if(!existeRegistro(p))
			{	
				try
				{
					systemQuery = "INSERT INTO tb_produtos (cd_importacao,pk_id,ds_modelo,ds_unidade, FK_FAMILIA, FK_TIPO," +
							"ds_codfabrica, DS_CODBARRAS, vl_pretab1, vl_poricms,vl_poriss,tg_servico,vl_descmax,vl_descpromo,tg_brinde,tg_inativo," +
							"ds_empresa,tg_origemicms,Nr_Sittribpdv, Nr_Sittribpdvaux,Tg_Pdvnaogerapontuacao,Tg_Naof4venda, FK_CLAFIS)" +
							"VALUES('" + bufferProduto.getCdImportacao()  + "','" +
						bufferProduto.getPkId() + "','" + ((bufferProduto.getDsModelo().length() > 40) ? bufferProduto.getDsModelo().substring(0, 40) : bufferProduto.getDsModelo()).replace("'", " ") + "','" + 
						//bufferProduto.getDsUnidade() + "','" + bufferProduto.getDsCodfabrica() + "', '" + bufferProduto.getCdBarras() + "', " +
						bufferProduto.getDsUnidade() + "', 1, '000001', NULL, '" + bufferProduto.getCdBarras() + "', " +
						(bufferProduto.getVlPreta1() + "").replace(",", ".") + "," + bufferProduto.getVlPoricms() + "," + 
						bufferProduto.getVlPoriss() + "," + bufferProduto.getTgServico() + "," + 
						bufferProduto.getVlDescmax() + "," + bufferProduto.getVlDescpromo() + "," + 
						bufferProduto.getTgBrinde() + "," + bufferProduto.getTgInativo() + ",'" +
						bufferProduto.getDsEmpresa() + "','" + bufferProduto.getTgOrigemICMS() + "','" +
						bufferProduto.getNrSittribpdv() + "','" + bufferProduto.getNrSittribpdvaux() + "'," + 
						bufferProduto.getTgPdvnaogerapontuacao() + "," + bufferProduto.getTgNaof4venda() + ", " + (idClafis == -0x01 ? "NULL" : idClafis) + ")"; 
					msSQLConnector.resultQuery(systemQuery , msSQLConnector.getNewStatement());
					
					System.out.println("Produto [" + bufferProduto.getCdImportacao() + "] incluso no Vixen!");
					
					countProdutosGravados++;	
				}
				catch(Exception exr)
				{
					System.out.println(systemQuery);
					throw exr;
				}
			}
			else
			{
				try
				{ 
					systemQuery = "UPDATE tb_produtos SET vl_pretab1 = " + (bufferProduto.getVlPreta1() + "").replace(",", ".") + ",  vl_descmax = " + bufferProduto.getVlDescmax() + ", " +
							"vl_descpromo = 0.0, FK_CLAFIS = " + idClafis + " where cd_importacao = '" + bufferProduto.getCdImportacao() + "';";

							msSQLConnector.resultQuery(systemQuery , msSQLConnector.getNewStatement());
							
							countProdutosJaExistentes++; 	
				}
				catch(Exception exr)
				{
					System.out.println(systemQuery);
					throw exr;
				}
			}
		}
		
		
		System.out.println("Processo terminado...");
		System.out.println(countProdutosGravados + " produtos gravados no Vixen");
		System.out.println(countProdutosJaExistentes + " produtos que já existiam no Vixen.");
	}
	
	private Boolean existeRegistro(Produto p) throws SQLException
	{
		Statement sqlStatement = msSQLConnector.getNewStatement();
		ResultSet sqlResult = sqlStatement.executeQuery("SELECT PK_ID FROM TB_PRODUTOS WHERE PK_ID = " + p.getCodigoInterno().getSqlValue());
		
		return sqlResult.next();
	}
	
	private int tratarNcm(KscString ncm) throws Exception
	{
		Statement sqlStatement = msSQLConnector.getNewStatement();
		ResultSet sqlResult = sqlStatement.executeQuery("SELECT PK_ID FROM TB_CLAFIS WHERE NR_CLAFIS = '" + ncm.getValue() + "'");
		
		if(sqlResult.next())
			return sqlResult.getInt("PK_ID"); 
		else
		{
			int registryKey = 0x01;
			
			try
			{
				sqlResult = sqlStatement.executeQuery("SELECT MAX(CAST(PK_ID AS INT)) AS PK_ID FROM TB_CLAFIS");
				
				if(sqlResult.next()) 
					registryKey = sqlResult.getInt("PK_ID") + 0x01;
				
				msSQLConnector.getNewStatement().execute("INSERT INTO TB_CLAFIS (PK_ID, DS_CLAFIS, NR_CLAFIS) VALUES (" + registryKey + ", NULL, '" + ncm.getValue() + "');");	
			}
			catch(Exception ex)
			{
				/*System.out.println("VALOR REGISTRY_KEY > " + registryKey);
				System.out.println("VALOR NCM          > " + ncm.getValue());
				
				sqlResult = sqlStatement.executeQuery("SELECT NR_CLAFIS FROM TB_CLAFIS WHERE PK_ID = " + registryKey + "");
				
				if(sqlResult.next())
					System.out.println("NCM COM REGISTRY KEY > " + sqlResult.getInt("NR_CLAFIS"));*/
				
				System.out.println("REGISTROS EXISTENTES ");
				
				sqlResult = sqlStatement.executeQuery("SELECT NR_CLAFIS, CAST(PK_ID AS INT) AS PK_ID FROM TB_CLAFIS");
				
				while(sqlResult.next())
					System.out.println("NCM COM REGISTRY KEY > " + sqlResult.getInt("PK_ID") + " VALOR NCM > " + sqlResult.getInt("NR_CLAFIS"));				
				
				System.out.println("TENTANDO INCLUIR COM ");
				System.out.println("INSERT INTO TB_CLAFIS (PK_ID, DS_CLAFIS, NR_CLAFIS) VALUES (" + registryKey + ", NULL, '" + ncm.getValue() + "');");
				
				throw ex;
			}
			
			sqlResult = sqlStatement.executeQuery("SELECT PK_ID FROM TB_CLAFIS WHERE NR_CLAFIS = '" + ncm.getValue() + "'");
			
			if(sqlResult.next())
				return sqlResult.getInt("PK_ID");
			else
				throw new Exception("não foi possível recuperar o cadastro do NCM " + ncm.getValue());
		}
	}
}