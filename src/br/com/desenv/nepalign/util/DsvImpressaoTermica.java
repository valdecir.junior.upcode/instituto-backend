package br.com.desenv.nepalign.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;

import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.*;

import br.com.adilson.util.PrinterMatrix;
import br.com.desenv.nepalign.model.Impressora;
import br.com.desenv.nepalign.model.PedidoVenda;
import br.com.desenv.nepalign.service.PedidoVendaService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class DsvImpressaoTermica
{	
	public void imprimirPedidoLoja(Integer idPedido, String sql, String reportName, Connection con, Impressora impressora) throws Exception
	{
		try
		{
			if(con == null)
				con = ConexaoUtil.getConexaoPadrao();
			
			HashMap<String, Object> parametros = new HashMap<String, Object>();
			
			DsvImpressaoTermica t = new DsvImpressaoTermica();
			String file = this.getClass().getResource("/relatorios/" + reportName).getPath();
			parametros.put("DIRETORIO_IMAGENS", DsvConstante.getParametrosSistema().getDiretorioImagens());	
			
			t.gerarTextoPedido(file, sql, parametros, "IMPRESSAO_" + new Date().getTime() + ".txt", impressora, idPedido, con);
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	public void imprimirComprovanteCondicional(Integer idPedido, String tituloCondicional, String sql, String reportName, Connection con, Impressora impressora) throws Exception
	{
		try
		{
			if(con == null)
				con = ConexaoUtil.getConexaoPadrao();
			
			HashMap<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("DIRETORIO_IMAGENS", DsvConstante.getParametrosSistema().getDiretorioImagens());	
			parametros.put("TERMOS_CONSUMIDOR", tituloCondicional);
			
			this.gerarTextoCondicional(this.getClass().getResource("/relatorios/" + reportName).getPath(), sql, parametros, impressora, con);
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	public void imprimirCupom(Integer idPedido,String interno, String sql, String reportName, Connection con, Impressora impressora) throws Exception
	{
		try
		{
			if(con == null)
				con = ConexaoUtil.getConexaoPadrao();
			
			HashMap<String, Object> parametros = new HashMap<String, Object>();
			
			parametros.put("DIRETORIO_IMAGENS", DsvConstante.getParametrosSistema().getDiretorioImagens());	
			parametros.put("idPedidoVenda", idPedido);
			parametros.put("interno", interno);
			parametros.put("REPORT_CONNECTION", con);
			parametros.put("SUBREPORT_DIR", this.getClass().getResource("/relatorios/").getPath());
			parametros.put("idChequePre", DsvConstante.getParametrosSistema().getIdFormaPagamentoChequePre());
			this.gerarTexto(this.getClass().getResource("/relatorios/" + reportName).getPath(), sql, parametros, "", impressora, con);

		}
		catch(Exception e)
		{
			throw e;
		}
	}
	public void imprimirDuplicata(Integer idDuplicata,String interno,Integer numeroParcelas ,String sql, String reportName, Connection con, Impressora impressora) throws Exception
	{
		try
		{
			if(con == null)
				con = ConexaoUtil.getConexaoPadrao();
			
			HashMap<String, Object> parametros = new HashMap<String, Object>();
			
			DsvImpressaoTermica t = new DsvImpressaoTermica();
			String file = this.getClass().getResource("/relatorios/" + reportName).getPath();
			parametros.put("DIRETORIO_IMAGENS", DsvConstante.getParametrosSistema().getDiretorioImagens());	
			parametros.put("idDuplicata", idDuplicata);
			parametros.put("numeroParcelas", numeroParcelas);
			parametros.put("interno", interno);
			parametros.put("SUBREPORT_DIR", this.getClass().getResource("/relatorios/").getPath());
			parametros.put("REPORT_CONNECTION", con);
			
			t.gerarTextoDuplicata(file, sql, parametros, "IMPRESSAO_" + new Date().getTime() + ".txt", impressora, con);
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	public void imprimirComprovanteParcela(String sql, Double valorTotal, String reportName, Connection con, Impressora impressora) throws Exception
	{
		try
		{
			if(con == null)
				con = ConexaoUtil.getConexaoPadrao();
			
			String file = this.getClass().getResource("/relatorios/" + reportName).getPath();
			
			HashMap<String, Object> parametros = new HashMap<String, Object>();			
			parametros.put("DIRETORIO_IMAGENS", DsvConstante.getParametrosSistema().get(""));	
			parametros.put("SUBREPORT_DIR", this.getClass().getResource("/").getPath());
			parametros.put("VALOR_TOTAL", valorTotal);
			parametros.put("REPORT_CONNECTION", con);
			
			gerarTextoDuplicata(file, sql, parametros, "F:/tmpRelatorios/Dup " + new Date().getTime() + ".txt", impressora, con);
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	public void imprimirComprovanteVale(String reportName,JRBeanCollectionDataSource dataSource, Impressora impressora) throws Exception
	{
		try
		{
			String file = this.getClass().getResource("/relatorios/" + reportName).getPath();
			HashMap<String, Object> parametros = new HashMap<String, Object>();			
			parametros.put("SUBREPORT_DIR", this.getClass().getResource("/").getPath());
			
			gerarTextoDuplicata(file, parametros, "IMPRESSAOVALE_ " + new Date().getTime() + ".txt", impressora, dataSource);
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	protected void gerarTextoDuplicata(String file, String sql, HashMap<String, Object> parametros, String arquivoSaida, Impressora impressora, Connection con) throws Exception, IOException, net.sf.jasperreports.engine.JRException 
	{
		try
		{
			net.sf.jasperreports.engine.JasperReport jr = JasperCompileManager.compileReport(file);
	        
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();	

		    JRResultSetDataSource jrRS = new JRResultSetDataSource(con.createStatement().executeQuery(sql));
		    JasperPrint jasperPrint = JasperFillManager.fillReport(jr, parametros, jrRS);
		    JRTextExporter exporterTxt = new JRTextExporter();
			exporterTxt.setParameter(JRTextExporterParameter.JASPER_PRINT, jasperPrint);
			//exporterTxt.setParameter(JRTextExporterParameter.OUTPUT_FILE_NAME, arquivoSaida);  //Antigamente usado, criava um .txt para depois ser lido
			exporterTxt.setParameter(JRTextExporterParameter.OUTPUT_STREAM, baos);  
		    exporterTxt.setParameter(JRTextExporterParameter.PAGE_HEIGHT,new Integer(1200));   
		    exporterTxt.setParameter(JRTextExporterParameter.PAGE_WIDTH,new Integer(200));   
		    exporterTxt.setParameter(JRTextExporterParameter.CHARACTER_WIDTH,new Float(5));   
		    exporterTxt.setParameter(JRTextExporterParameter.CHARACTER_HEIGHT,new Float(7));    
		    exporterTxt.exportReport();
		        
	        this.imprimir(baos.toByteArray(), false, impressora, con);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	protected void gerarTextoDuplicata(String file, HashMap<String, Object> parametros, String arquivoSaida, Impressora impressora,JRBeanCollectionDataSource dataSource) throws Exception, IOException, net.sf.jasperreports.engine.JRException 
	{
		try
		{
			net.sf.jasperreports.engine.JasperReport jr = JasperCompileManager.compileReport(file);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();	
			
		
			JasperPrint jasperPrint = JasperFillManager.fillReport(jr, parametros, dataSource);
			JRTextExporter exporterTxt = new JRTextExporter();
			exporterTxt.setParameter(JRTextExporterParameter.JASPER_PRINT, jasperPrint);
			exporterTxt.setParameter(JRTextExporterParameter.OUTPUT_STREAM, baos);  
			exporterTxt.setParameter(JRTextExporterParameter.PAGE_HEIGHT,new Integer(1200));   
			exporterTxt.setParameter(JRTextExporterParameter.PAGE_WIDTH,new Integer(200));   
			exporterTxt.setParameter(JRTextExporterParameter.CHARACTER_WIDTH,new Float(5));   
			exporterTxt.setParameter(JRTextExporterParameter.CHARACTER_HEIGHT,new Float(7));    
			exporterTxt.exportReport();
			
			this.imprimir(baos.toByteArray(), false, impressora, null);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	protected void gerarTexto(String file, String sql, HashMap<String, Object> parametros, String arquivoSaida, Impressora impressora, Connection con) throws Exception, IOException, net.sf.jasperreports.engine.JRException 
	{
		try
		{
			net.sf.jasperreports.engine.JasperReport jr = JasperCompileManager.compileReport(file);
	        Statement stm = con.createStatement();  
	        ResultSet rs = stm.executeQuery(sql);  
			
	        
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();	

	        
		    JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);
		    JasperPrint jasperPrint = JasperFillManager.fillReport(jr, parametros, jrRS);
		    JRTextExporter exporterTxt = new JRTextExporter();
			exporterTxt.setParameter(JRTextExporterParameter.JASPER_PRINT, jasperPrint);
			//exporterTxt.setParameter(JRTextExporterParameter.OUTPUT_FILE_NAME, arquivoSaida);  //Antigamente usado, criava um .txt para depois ser lido
			exporterTxt.setParameter(JRTextExporterParameter.OUTPUT_STREAM, baos);  
		    exporterTxt.setParameter(JRTextExporterParameter.PAGE_HEIGHT,new Integer(150));   
		    exporterTxt.setParameter(JRTextExporterParameter.PAGE_WIDTH,new Integer(320));   
		    exporterTxt.setParameter(JRTextExporterParameter.CHARACTER_WIDTH,new Float(4.5));   
		    exporterTxt.setParameter(JRTextExporterParameter.CHARACTER_HEIGHT,new Float(7));   
		    //exporterTxt.setParameter(JRTextExporterParameter.CHARACTER_ENCODING,"UTF-8");
		    exporterTxt.exportReport();
		        
	        this.imprimir(baos.toByteArray(), false, impressora, con);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	protected void gerarTextoCondicional(String file, String sql, HashMap<String, Object> parametros, Impressora impressora, Connection con) throws Exception, IOException, net.sf.jasperreports.engine.JRException 
	{
		try
		{
			net.sf.jasperreports.engine.JasperReport jr = JasperCompileManager.compileReport(file);
	        Statement stm = con.createStatement();  
	        ResultSet rs = stm.executeQuery(sql);  
	        
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();	
	        String[] linhasTermo = ((String) parametros.get("TERMOS_CONSUMIDOR")).split("\n");
	        
	        for(String linhaTermo:linhasTermo)
	        {
		        baos.write((linhaTermo + "\r").getBytes(Charset.defaultCharset()));	
	        }
	        
		    JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);
		    JasperPrint jasperPrint = JasperFillManager.fillReport(jr, parametros, jrRS);
		    JRTextExporter exporterTxt = new JRTextExporter();
			exporterTxt.setParameter(JRTextExporterParameter.JASPER_PRINT, jasperPrint);
			exporterTxt.setParameter(JRTextExporterParameter.OUTPUT_STREAM, baos);  
		    exporterTxt.setParameter(JRTextExporterParameter.PAGE_WIDTH, 100);
		    exporterTxt.setParameter(JRTextExporterParameter.PAGE_HEIGHT, 400);
		    exporterTxt.setParameter(JRTextExporterParameter.CHARACTER_WIDTH, new Float(6.0)); 
		    exporterTxt.setParameter(JRTextExporterParameter.CHARACTER_HEIGHT, new Float(12.0));  
		    //exporterTxt.setParameter(JRTextExporterParameter.CHARACTER_HEIGHT, new Float(15.0));   
		    exporterTxt.exportReport();
		    
		    this.imprimir(baos.toByteArray(), true, impressora, con);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}	
	
	protected void gerarTextoPedido(String file, String sql, HashMap<String, Object> parametros, String arquivoSaida, Impressora impressora, int idPedido, Connection con) throws Exception, IOException, net.sf.jasperreports.engine.JRException 
	{
		try
		{
			PedidoVenda pedidoVenda = new PedidoVendaService().recuperarPorId(idPedido);
			
			if(pedidoVenda.getCliente().getId().intValue() == Integer.parseInt(DsvConstante.getParametrosSistema().get("idClienteConsumidor")))
				parametros.put("MOSTRAR_CLIENTE", false);
			else
				parametros.put("MOSTRAR_CLIENTE", true);
			
			parametros.put("TROCA", pedidoVenda.getTipoMovimentacaoEstoque().getId().intValue() == Integer.parseInt(DsvConstante.getParametrosSistema().get(("idTipoMovimentacaoEstoqueTrocaEntrada"))) ? "S" : "N");
			
			net.sf.jasperreports.engine.JasperReport jr = JasperCompileManager.compileReport(file);
	        Statement stm = con.createStatement();  
	        ResultSet rs = stm.executeQuery(sql);  
	        
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();	

	        
		    JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);
		    JasperPrint jasperPrint = JasperFillManager.fillReport(jr, parametros, jrRS);
		    
		    JRTextExporter exporterTxt = new JRTextExporter();
			exporterTxt.setParameter(JRTextExporterParameter.JASPER_PRINT, jasperPrint);
			//exporterTxt.setParameter(JRTextExporterParameter.OUTPUT_FILE_NAME, arquivoSaida);  //Antigamente usado, criava um .txt para depois ser lido
			exporterTxt.setParameter(JRTextExporterParameter.OUTPUT_STREAM, baos);  
		    exporterTxt.setParameter(JRTextExporterParameter.PAGE_HEIGHT,new Integer(60));   
		    exporterTxt.setParameter(JRTextExporterParameter.PAGE_WIDTH,new Integer(100));   
		    exporterTxt.setParameter(JRTextExporterParameter.CHARACTER_WIDTH,new Float(6.0));   
		    exporterTxt.setParameter(JRTextExporterParameter.CHARACTER_ENCODING,"UTF-8");
		    exporterTxt.exportReport();
		    
		    baos.write(retornaCodigoBarras(pedidoVenda.getNumeroControle()));
		    
		    this.imprimir(baos.toByteArray(), false, impressora, con);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}	
	
	public void imprimir(byte[] data, Boolean condicional, Impressora impressora, Connection con) throws Exception
	{
		try
		{
	        String str = new String(data, "UTF8"); // for UTF8 encoding
	        String[] outputStrings = str.split("\n");
	        
			PrinterMatrix printer = new PrinterMatrix();
			
			StringBuffer sb = new StringBuffer();
			sb.append(""+(char) 29 +(char) 249+(char)32+(char)0+(char)27+(char)15 + (char) 29 + (char) 97 + (char) 0);
			
			for(String outString:outputStrings)
			{
				if(!outString.trim().equals("") && !outString.trim().replace(" ", "").equals(""))
					sb.append(outString);
			}
			
			String linhas[] = sb.toString().split("\r"); 
			printer.setOutSize(linhas.length, 50);
			 
			byte[] escCommands = new byte[6];
			escCommands[0] = 27;
			escCommands[1] = 15;
			escCommands[2] = 27;
			escCommands[3] = 77;
			escCommands[4] = 27;
			escCommands[5] = 109;
			
			for (int i=0;i<linhas.length;i++)
			{
				printer.printTextLinCol(i+1, 1, linhas[i].replace("\n", "").replace("\r", ""));
			}
 
		//	printer.show();
			printer.toPrinter(impressora.getCaminho());
			picote(impressora); 
		}
		catch(Exception e)
		{
			throw e;
		}
	}

	public void picote(Impressora impressora) throws Exception
	{
		PrinterMatrix printer = new PrinterMatrix();
		printer.setOutSize(1, 1);
		printer.printTextLinCol(1, 1, ""+(char) 29 +(char) 249+(char)32+(char)0+(char)27+(char)109);
		printer.toPrinter(impressora.getCaminho());
	}
	
	public void imprimirCodigoBarras(Impressora impressora, int codigoBarras) throws Exception
	{
		PrinterMatrix printer = new PrinterMatrix();
		printer.setOutSize(1, 1);
		printer.printTextLinCol(1, 1, ""+retornaCodigoBarras(String.valueOf(codigoBarras)));
		printer.toPrinter(impressora.getCaminho());
	}	
	
	public byte[] retornaCodigoBarras(String codigo)
	{
		return (""+(char)27+(char)97+(char)1+(char)29+(char)104+(char)50+(char)29+(char)107+(char)73+(char)10+String.format("%" + (codigo.length() + 1) + "d", Long.parseLong(codigo))+(char)0).getBytes();
	}
	
	public static void main(String[] args) 
	{
		try
		{
			PrinterMatrix printer = new PrinterMatrix();
			
			int cont = 0 ;
			
			StringBuffer sb = new StringBuffer();
			sb.append(""+(char) 29 +(char) 249+(char)32+(char)0+(char)27+(char)15);
			sb.append("teste 1\n");
			sb.append("teste 2\n");
			sb.append("teste 3\n");
			sb.append("teste 4\n");
			sb.append("teste 5\n");
			sb.append("teste 6\n");
			sb.append((char)18);
			
			printer.setOutSize(cont + 1, 50);
			
			String linhas[] = sb.toString().split("\r");
			byte[] escCommands = new byte[6];
			escCommands[0] = 27;//condensado
			escCommands[1] = 15;
			escCommands[2] = 27;
			escCommands[3] = 77;//font 12CPI
			escCommands[4] = 27;
			escCommands[5] = 109;
			
			for (int i=0;i<linhas.length;i++)
			{
				printer.printTextLinCol(i+1, 1, linhas[i]);
			}

			printer.toPrinter("LPT1");
		}
		catch(Exception e)
		{
			throw e;
		}		
	}
}