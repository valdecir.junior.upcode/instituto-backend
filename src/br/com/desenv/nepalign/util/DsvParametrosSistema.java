
package br.com.desenv.nepalign.util;

import java.util.Date;

import br.com.desenv.frameworkignorante.IgnParametrosSistema;

public class DsvParametrosSistema extends IgnParametrosSistema 
{		
	private int idTipoContaTranferenciaEscritorio = 49;
	
	private int idTipoContaPagamentosDivercos = 50;
	private int idSituacaoEnvioLVAguardandoAprov = 1;
	private int idSituacaoEnvioLVProntoEnvio = 2;
	private int idSituacaoEnvioLVEnvioCompleto = 3;
	private int idSituacaoEnvioLVErroEnvio = 4;
	private int idSituacaoEnvioLVItemInativo = 3;
	private int idSituacaoEnvioLVAguardandoEvSistema = 6;
	private int idSituacaoEnvioLVAguardandoSerInativado = 8;
	private int idSituacaoEnvioLVAguardandoImagens = 9;
	private int idSituacaoEnvioLVAguardandoConfRemocao = 20;
	private int idSituacaoEnvioLVProntoRemocao = 21;
	private int idSituacaoEnvioLVErroRemocao = 22;
	private int idSituacaoEnvioLVRemovidoSite = 23;
	private int idTipoContaCompraMercadoria = 48;
	private int idContaGerencialCompraMercadoria = 1;
	private int idCentroFinanceiroCompras = 3;
	private int idStatusAlertaAlertaFinalizado = 6;
	private int idStatusAlertaAlertaCancelado = 8;
	private int idStatusAlertaAlertaAtivo = 1;
	private int idStatusAlertaAlertaApropriadoParaLoja = 4;
	private int idStatusAlertaAlertaRemovidoPorTimeOut = 5;
	private int idStatusAlertaAlertaRemovidoPeloOperador = 2;
	private int idStatusAlertaAlertaDesativado = 3;
	private int idStatusAlertaAlertaNaoEncontradoPorNenhumaLoja = 10;
	private int idSituacaoPedidoVendaFinalizado = 4;
	private int idSituacaoPedidoVendaAguardandoAprovacao = 2;
	private int idSituacaoPedidoVendaAberto = 1;
	private int idSituacaoPedidoVendaAprovado = 3;
	private int idSituacaoPedidoVendaCancelado = 5;
	private int idStatusItemPedidoAberto = 2;
	private int idStatusItemPedidoEnviado = 3;
	private int idStatusItemPedidoNaoEncontrado = 4;
	private int idStatusItemPedidoProntoParaEnvio = 1;
	private int idStatusItemPedidoCancelado = 5;
	private int idClienteConsumidor = 0;
	private int idPaisPadrao = 1; //BRASIL
	private int idTipoLogAlertaAlertaGerado = 1;
	private int idTipoLogAlertaRespostaPositiva = 2;
	private int idTipoLogAlertaRespostaNegativa = 3;
	private int idTipoLogAlertaPedidoAprovado = 4;
	private int idTipoLogAlertaItemEnviado = 5;
	private int idTipoLogAlertaPedidoFinalizado = 6;
	private int idTipoLogAlertaRespostaDepoisEnvio = 7;
	private int idTipoLogAlertaAlertaGeradoProdutoLojaOrigem = 8;
	private int idTipoLogAlertaAlertaCancelado = 9;
	private int SleepTimeThreadIntegrarPedidos = 5000;
	private int SleepTimeThreadVerificarAlertas = 5000;
	private int SleepTimeTaskAlertaSonoro = 15000;
	private int idSituacaoLancamentoEmAberto = 1;
	private int idSituacaoLancamentoBaixado = 2;
	private int idTipoContaCredito = 46;
	private int idTipoContaDebito = 47;
	private int Empresa_AlertaSonoro = 8;
	private int tempoPedidoPendente = 1; // Em horas
	private String pedidovenda_situacoesPermitidasConsulta = "2,3"; 
	private int itemPedidoVenda_statusInicialItemLojaVirtual = 8;
	private Boolean alertaSonoroPedidoPendenteEnvio = false;
	private Boolean alertaSonoroPedidoRespondido = true;
	private Boolean alertaSonoroPedidoNovo = false;
	private int pedidoVenda_statusParaGerarAlertaLV = 100;	
	private int pedidoVenda_integracao_vendedorPadrao = 319;
	private int pedidoVenda_integracao_empresaFisicaPadrao = 10;
	private int pedidoVenda_integracao_situacaoPedidoVendaPadrao = 1;
	private int pedidoVenda_integracao_tipoMovimentacaoEstoquePadrao = 2;
	private String caminhoSistemaAlertaSonoro = "C:/Desenv/Alertas/AlertaSonoro.jar";
	private int statusAlerta_ProdutoNaoEncontrado = 10;
	private int statusItem_NaoEncontrado = 4;
	private int idTipoMovimentacaoEstoqueEntrada = 1;
	private String emailDeEnvioAlertaEmail = "alerta@manamaue.com.br";
	private String senhaEmailDeEnvioAlertaEmail = "alerta@mana";
	private String urlLocalFotosEstoqueProduto = "http://www.manamaue.com.br/images/product/";
	private String usuarioServicoSMS = "anderprf";
	private String senhaServicoSMS = "vitoria7";
	private int idEstoquePadrao = 1;
	private int idTipoMovimentacaoCaixaDiaDuplicata = 2;
	private int idFormaPagamentoDuplicata = 30;
	private String diretorioRelatorios =  System.getProperty("user.dir") + "/WebContent/relatorios/"; //TODO
	private String diretorioImagens = System.getProperty("user.dir") + "/WebContent/imagem/";//TODO
	private String servidorSmptEmailDeEnvioAlertaEmail = "smtp.manamaue.com.br";
	private int portaSmtpEmailDeEnvioAlertaEmail = 567;
	private int valorAcrescimoPermitidoPedidoVenda = 20;
	private boolean considerarEstoqueNegativo = true;
	private int idTipoMovimentacaoCaixaDiaRecibimentoPedido =1;
	private int idTipoMovimentacaoCaixaDiaRecebimentoDuplicata = 3;
	//Parametros Nota Fiscal
	private int idSituacaoNotaFiscalGerada = 15;
	private int idSituacaoNotaFiscalRejeitada = 21;
	private int idSituacaoNotaFiscalEnviadaDfe = 13;
	private int idSituacaoNotaFiscalImportadaDfe = 17;
	private int idSituacaoNotaFiscalEntregaEncerrada = 12;
	private int idSituacaoNotaFiscalCancelamentoSolicitado = 5;
	private int idSituacaoNotaFiscalCancelada = 6;
	private int idSituacaoNotaFiscalImpressaoSolicitada = 18;
	private int idSituacaoNotaFiscalImpressa = 19;
	private int idSituacaoNotaFiscalAutorizadaSefaz = 1;
	private int idSituacaoNotaFiscalImpressaContingencia = 20; 
	private int idCfopVenda = 6;
	private int idCfopVendaNaoContribuinte = 23;
	private int idCstCofinsPadrao = 5;
	private int idCstPisPadrao = 5;
	private int idCodigoSituacaoTributariaPadrao = 28;
	private int idCodigoSituacaoTributariaIsenta = 28;
	private int mostrarBaseCalcReduzida = 1;
	private Double valorPorBoleto = 0.0;
	private int idFormaPagamentoBoleto = 4;
	private Double percDas = 0.0;
	private int idSituacaoPedidoVendaFaturado = 6;
	private int idTipoNotaFiscalVendaConsignacao = 9;
	private int idTipoNotaFiscalVenda = 7;
	private int idTipoNotaFiscalVendaAtivoImobilizado=8;
	private String serieNotaFiscal = "1";
	private String codigoPaisBrasil = "1058"; 
	private String telfoneEmpresaNota = "4433236535";
	private int codigoCstIcmsSimplesNacional = 102;
	
	private int idOrigemMercadoriaNacional = 1;
	private int idOrigemMercadoriaEstrangeiraImportacao = 2;
	private int idOrigemMercadoriaEstrangeiraInterna = 3;
	private int idEmpresaLV = 10;
	private int tipoOperacaoIncAltMarca = 1;
	private int tipoOperacaoIncAltDepartamento = 2;
	private int tipoOperacaoIncAltProduto = 3;
	private int tipoOperacaoIncAltProdutoSKU = 4;
	private int tipoOperacaoIncAltEstoqueProduto = 5;
	private int tipoOperacaoIncAltPrecoProduto = 6;
	private String statusModificarStatusAlerta = "6,4";
	private int condicaoFinalizarAlertaPedidoVenda = 3;
	private int tipoOperacaoValidacaoImagens = 21;
	private int tipoOperacaoImportProdutos = 22;
	private int tipoOperacaoImportDepartamentos = 23;
	private int tipoOperacaoImportProdsDepts = 24;
	private int tipoOperacaoImportCodBarras = 25;
	private int tipoOperacaoImportEstoque = 26;
	private int tipoOperacaoImportPedidoVenda = 27;
	private int tipoOperacaoImportItemPedidoVenda = 28;
	private String caminhoAbsolutoFotos = "C:/tmp/mana/TodasFotosManaVer01";
	private int tipoOperacaoIndefinida = 100;
	private String strConexaoCache = "jdbc:Cache://200.150.70.163:1972/JORROVI";
	private String usuarioCache = "_SYSTEM";
	private String senhaCache = "SYS";
	private String strConexaoWebServiceCatalog = "http://200.155.17.122/catalog/catalog.asmx?WSDL";
	private String usuarioWebServiceCatalog = "integracao";
	private String senhaWebServiceCatalog = "woo5#7";
	private String strConexaoWebServiceOrder = "http://200.155.17.122/order/order.asmx?WSDL";
	private String usuarioWebServiceOrder = "integracao";
	private String senhaWebServiceOrder = "woo5#7";
	private int idFormaPagamentoChequePre = 13; 
	private int tipoFormaPagamentoAVista = 1;
	private int tipoFormaPagamentoAPrazo = 2;
	private int tipoFormaPagamentoCartaoCredito = 2;
	private int tipoFormaPagamentoCartaoDebito = 4;
	private int tipoFormaPagamentoValeCalcados = 5;
	private String url_Sistema = "http://192.168.0.242:8080/nepalign/nepalignflx-debug/index.html";
	private int valorDescontoPermitidoPedidoVenda = 3;
	private String pedidovenda_dataGrid = "1,2,3";
	private int idTipoContaMovimentacaoCaixaDiaRecibimentoPedido = 31;
	private int idTipoContaRecebimentoDuplicata = 54;
	private int idParametrosSistemaNumeroDocumentoMovimentacaoEstoque = 3;
	private int idFormapagamentoCheque = 3;
	private int idPlanoPagamentoValeCalcadoConvenio = 24;
	private int idPlanoPagamentoAVista = 1;
	private int idFormaPagamentoValeCalcado = 33;
	private int idFormaPagamentoEntradaValeCalcado = 45;
	private String PesquisaPorUsuarioLancamentosDiversos = "N";
	private int idParametrosSistemaNumeroControle = 5;
	private int idSituacaoCaixaDiaAberto = 1;
	private int modeloNotaFiscalConsumidor = 65;
	
	

	
	public int getModeloNotaFiscalConsumidor(){
		return this.modeloNotaFiscalConsumidor;
	}
	
	public int getIdSituacaoCaixaDiaAberto(){ 
		return this.idSituacaoCaixaDiaAberto;
	}
	
	public String getPesquisaPorUsuarioLancamentosDiversos() {
		return PesquisaPorUsuarioLancamentosDiversos;
	}

	public void setPesquisaPorUsuarioLancamentosDiversos(
			String pesquisaPorUsuarioLancamentosDiversos) {
		PesquisaPorUsuarioLancamentosDiversos = pesquisaPorUsuarioLancamentosDiversos;
	}

	public int getIdFormapagamentoCheque() {
		return idFormapagamentoCheque;
	}
	
	public int getIdPlanoPagamentoAVista() {
		return idPlanoPagamentoAVista;
	}

	public void setIdPlanoPagamentoAVista(int idPlanoPagamentoAVista) {
		this.idPlanoPagamentoAVista = idPlanoPagamentoAVista;
	}

	public void setIdFormapagamentoCheque(int idFormapagamentoCheque) {
		this.idFormapagamentoCheque = idFormapagamentoCheque;
	}
	
	public int getTipoFormaPagamentoValeCalcados() {
		return tipoFormaPagamentoValeCalcados;
	}

	public int getIdTipoContaRecebimentoDuplicata() {
		return idTipoContaRecebimentoDuplicata;
	}

	public void setIdTipoContaRecebimentoDuplicata(
			int idTipoContaRecebimentoDuplicata) {
		this.idTipoContaRecebimentoDuplicata = idTipoContaRecebimentoDuplicata;
	}

	public int getIdFormaPagamentoValeCalcado() {
		return idFormaPagamentoValeCalcado;
	}

	public void setIdFormaPagamentoValeCalcado(int idFormaPagamentoValeCalcado) {
		this.idFormaPagamentoValeCalcado = idFormaPagamentoValeCalcado;
	}

	public int getIdFormaPagamentoEntradaValeCalcado() {
		return idFormaPagamentoEntradaValeCalcado;
	}

	public void setIdFormaPagamentoEntradaValeCalcado(
			int idFormaPagamentoEntradaValeCalcado) {
		this.idFormaPagamentoEntradaValeCalcado = idFormaPagamentoEntradaValeCalcado;
	}

	public void setTipoFormaPagamentoValeCalcados(int tipoFormaPagamentoValeCalcados) {
		this.tipoFormaPagamentoValeCalcados = tipoFormaPagamentoValeCalcados;
	}

	public int getIdTipoMovimentacaoCaixaDiaRecebimentoDuplicata() {
		return idTipoMovimentacaoCaixaDiaRecebimentoDuplicata;
	}
	public void setIdTipoMovimentacaoCaixaDiaRecebimentoDuplicata(
			int idTipoMovimentacaoCaixaDiaRecebimentoDuplicata) {
		this.idTipoMovimentacaoCaixaDiaRecebimentoDuplicata = idTipoMovimentacaoCaixaDiaRecebimentoDuplicata;
	}
	public int getIdCfopVenda() {
		return idCfopVenda;
	}
	
	public int getIdPlanoPagamentoValeCalcadoConvenio() {
		return idPlanoPagamentoValeCalcadoConvenio;
	}
	public void setIdPlanoPagamentoValeCalcadoConvenio(
			int idPlanoPagamentoValeCalcadoConvenio) {
		this.idPlanoPagamentoValeCalcadoConvenio = idPlanoPagamentoValeCalcadoConvenio;
	}
	public int getIdTipoContaTranferenciaEscritorio() {
		return idTipoContaTranferenciaEscritorio;
	}
	public void setIdTipoContaTranferenciaEscritorio(
			int idTipoContaTranferenciaEscritorio) {
		this.idTipoContaTranferenciaEscritorio = idTipoContaTranferenciaEscritorio;
	}
	public int getIdCfopVendaNaoContribuinte() {
		return idCfopVendaNaoContribuinte;
	}

	public int getIdCstCofinsPadrao() {
		return idCstCofinsPadrao;
	}


	public int getIdCodigoSituacaoTributariaPadrao() {
		return idCodigoSituacaoTributariaPadrao;
	}


	public int getIdCodigoSituacaoTributariaIsenta() {
		return idCodigoSituacaoTributariaIsenta;
	}


	public int getIdCstPisPadrao() {
		return idCstPisPadrao;
	}


	public int getMostrarBaseCalcReduzida() {
		return mostrarBaseCalcReduzida;
	}


	public Double getValorPorBoleto() {
		return valorPorBoleto;
	}


	public int getIdFormaPagamentoBoleto() {
		return idFormaPagamentoBoleto;
	}


	public Double getPercDas() {
		return percDas;
	}


	public String getTelfoneEmpresaNota() {
		return telfoneEmpresaNota;
	}
	
	public int getIdOrigemMercadoriaNacional() {
		return idOrigemMercadoriaNacional;
	}
	public int getIdOrigemMercadoriaEstrangeiraImportacao() {
		return idOrigemMercadoriaEstrangeiraImportacao;
	}
	public int getIdOrigemMercadoriaEstrangeiraInterna() {
		return idOrigemMercadoriaEstrangeiraInterna;
	}
	public int getIdSituacaoPedidoVendaFaturado() {
		return idSituacaoPedidoVendaFaturado;
	}


	public int getIdSituacaoNotaFiscalEnviadaDfe() {
		return idSituacaoNotaFiscalEnviadaDfe;
	}
	public int getIdSituacaoNotaFiscalEntregaEncerrada() {
		return idSituacaoNotaFiscalEntregaEncerrada;
	}
	public int getIdSituacaoNotaFiscalCancelamentoSolicitado() {
		return idSituacaoNotaFiscalCancelamentoSolicitado;
	}
	public int getIdSituacaoNotaFiscalCancelada() {
		return idSituacaoNotaFiscalCancelada;
	}
	public int getIdSituacaoNotaFiscalImpressaoSolicitada() {
		return idSituacaoNotaFiscalImpressaoSolicitada;
	}
	public int getIdSituacaoNotaFiscalImpressa() {
		return idSituacaoNotaFiscalImpressa;
	}
	public int getIdSituacaoNotaFiscalAutorizadaSefaz() {
		return idSituacaoNotaFiscalAutorizadaSefaz;
	}
	public int getIdSituacaoNotaFiscalImportadaDfe() {
		return idSituacaoNotaFiscalImportadaDfe;
	}

	public int getIdSituacaoNotaFiscalImpressaContingencia() {
		return idSituacaoNotaFiscalImpressaContingencia;
	}
	public int getIdTipoNotaFiscalVendaConsignacao() {
		return idTipoNotaFiscalVendaConsignacao;
	}
	public int getIdTipoNotaFiscalVenda() {
		return idTipoNotaFiscalVenda;
	}
	public String getSerieNotaFiscal() {
		return serieNotaFiscal;
	}
	public int getIdTipoNotaFiscalVendaAtivoImobilizado() {
		return idTipoNotaFiscalVendaAtivoImobilizado;
	}
	
	public String getCodigoPaisBrasil() {
		return codigoPaisBrasil;
	}
	
	public int getIdTipoMovimentacaoCaixaDiaRecibimentoPedido() {
		return idTipoMovimentacaoCaixaDiaRecibimentoPedido;
	}
	public void setIdTipoMovimentacaoCaixaDiaRecibimentoPedido(
			int idTipoMovimentacaoCaixaDiaRecibimentoPedido) {
		this.idTipoMovimentacaoCaixaDiaRecibimentoPedido = idTipoMovimentacaoCaixaDiaRecibimentoPedido;
	}
	public int getIdTipoContaMovimentacaoCaixaDiaRecibimentoPedido() {
		return idTipoContaMovimentacaoCaixaDiaRecibimentoPedido;
	}
	public void setIdTipoContaMovimentacaoCaixaDiaRecibimentoPedido(
			int idTipoContaMovimentacaoCaixaDiaRecibimentoPedido) {
		this.idTipoContaMovimentacaoCaixaDiaRecibimentoPedido = idTipoContaMovimentacaoCaixaDiaRecibimentoPedido;
	}

	
	
	public boolean isConsiderarEstoqueNegativo() {
		return considerarEstoqueNegativo;
	}
	public void setConsiderarEstoqueNegativo(boolean considerarEstoqueNegativo) {
		this.considerarEstoqueNegativo = considerarEstoqueNegativo;
	}
	public int getValorAcrescimoPermitidoPedidoVenda() {
		return valorAcrescimoPermitidoPedidoVenda;
	}
	public void setValorAcrescimoPermitidoPedidoVenda(
			int valorAcrescimoPermitidoPedidoVenda) {
		this.valorAcrescimoPermitidoPedidoVenda = valorAcrescimoPermitidoPedidoVenda;
	}

	
	
	
	public int getValorDescontoPermitidoPedidoVenda() {
		return valorDescontoPermitidoPedidoVenda;
	}
	public void setValorDescontoPermitidoPedidoVenda(
			int valorDescontoPermitidoPedidoVenda) {
		this.valorDescontoPermitidoPedidoVenda = valorDescontoPermitidoPedidoVenda;
	}
	public String getServidorSmptEmailDeEnvioAlertaEmail() {
		return servidorSmptEmailDeEnvioAlertaEmail;
	}
	public void setServidorSmptEmailDeEnvioAlertaEmail(
			String servidorSmptEmailDeEnvioAlertaEmail) {
		this.servidorSmptEmailDeEnvioAlertaEmail = servidorSmptEmailDeEnvioAlertaEmail;
	}
	public int getPortaSmtpEmailDeEnvioAlertaEmail() {
		return portaSmtpEmailDeEnvioAlertaEmail;
	}
	public void setPortaSmtpEmailDeEnvioAlertaEmail(
			int portaSmtpEmailDeEnvioAlertaEmail) {
		this.portaSmtpEmailDeEnvioAlertaEmail = portaSmtpEmailDeEnvioAlertaEmail;
	}
	
	public int getStatusItem_NaoEncontrado() {
		return statusItem_NaoEncontrado;
	}
	public int getStatusAlerta_ProdutoNaoEncontrado() {
		return statusAlerta_ProdutoNaoEncontrado;
	}
	
	
	
	public int getSleepTimeTaskAlertaSonoro() {
		return SleepTimeTaskAlertaSonoro;
	}
	public void setSleepTimeTaskAlertaSonoro(int sleepTimeTaskAlertaSonoro) {
		SleepTimeTaskAlertaSonoro = sleepTimeTaskAlertaSonoro;
	}
	
	public int getEmpresa_AlertaSonoro() {
		return Empresa_AlertaSonoro;
	}
	public void setEmpresa_AlertaSonoro(int empresa_AlertaSonoro) {
		Empresa_AlertaSonoro = empresa_AlertaSonoro;
	}
	
	public int getPedidoVenda_integracao_vendedorPadrao() {
		return pedidoVenda_integracao_vendedorPadrao;
	}
	public int getIdTipoContaPagamentosDivercos() {
		return idTipoContaPagamentosDivercos;
	}
	public void setIdTipoContaPagamentosDivercos(int idTipoContaPagamentosDivercos) {
		this.idTipoContaPagamentosDivercos = idTipoContaPagamentosDivercos;
	}
	public void setPedidoVenda_integracao_vendedorPadrao(
			int pedidoVenda_integracao_vendedorPadrao) {
		this.pedidoVenda_integracao_vendedorPadrao = pedidoVenda_integracao_vendedorPadrao;
	}
	public int getPedidoVenda_integracao_empresaFisicaPadrao() {
		return pedidoVenda_integracao_empresaFisicaPadrao;
	}
	public void setPedidoVenda_integracao_empresaFisicaPadrao(
			int pedidoVenda_integracao_empresaFisicaPadrao) {
		this.pedidoVenda_integracao_empresaFisicaPadrao = pedidoVenda_integracao_empresaFisicaPadrao;
	}
	public int getPedidoVenda_integracao_situacaoPedidoVendaPadrao() {
		return pedidoVenda_integracao_situacaoPedidoVendaPadrao;
	}
	public void setPedidoVenda_integracao_situacaoPedidoVendaPadrao(
			int pedidoVenda_integracao_situacaoPedidoVendaPadrao) {
		this.pedidoVenda_integracao_situacaoPedidoVendaPadrao = pedidoVenda_integracao_situacaoPedidoVendaPadrao;
	}
	public int getPedidoVenda_integracao_tipoMovimentacaoEstoquePadrao() {
		return pedidoVenda_integracao_tipoMovimentacaoEstoquePadrao;
	}
	public void setPedidoVenda_integracao_tipoMovimentacaoEstoquePadrao(
			int pedidoVenda_integracao_tipoMovimentacaoEstoquePadrao) {
		this.pedidoVenda_integracao_tipoMovimentacaoEstoquePadrao = pedidoVenda_integracao_tipoMovimentacaoEstoquePadrao;
	}
	
	public int getPedidoVenda_statusParaGerarAlertaLV() {
		return pedidoVenda_statusParaGerarAlertaLV;
	}
	public void setPedidoVenda_statusParaGerarAlertaLV(
			int pedidoVenda_statusParaGerarAlertaLV) {
		this.pedidoVenda_statusParaGerarAlertaLV = pedidoVenda_statusParaGerarAlertaLV;
	}
	public int getItemPedidoVenda_statusInicialItemLojaVirtual() {
		return itemPedidoVenda_statusInicialItemLojaVirtual;
	}
	public void setItemPedidoVenda_statusInicialItemLojaVirtual(
			int itemPedidoVenda_statusInicialItemLojaVirtual) {
		this.itemPedidoVenda_statusInicialItemLojaVirtual = itemPedidoVenda_statusInicialItemLojaVirtual;
	}
	public String getPedidovenda_dataGrid() {
		return pedidovenda_dataGrid;
	}
	public void setPedidovenda_dataGrid(String pedidovenda_dataGrid) {
		this.pedidovenda_dataGrid = pedidovenda_dataGrid;
	}

	
	public String getPedidovenda_situacoesPermitidasConsulta() {
		return pedidovenda_situacoesPermitidasConsulta;
	}
	public void setPedidovenda_situacoesPermitidasConsulta(
			String pedidovenda_situacoesPermitidasConsulta) {
		this.pedidovenda_situacoesPermitidasConsulta = pedidovenda_situacoesPermitidasConsulta;
	}	
	
	
	public int getSleepTimeThreadIntegrarPedidos() {
		return SleepTimeThreadIntegrarPedidos;
	}
	public void setSleepTimeThreadIntegrarPedidos(int sleepTimeThreadIntegrarPedidos) {
		SleepTimeThreadIntegrarPedidos = sleepTimeThreadIntegrarPedidos;
	}
	public int getSleepTimeThreadVerificarAlertas() {
		return SleepTimeThreadVerificarAlertas;
	}
	public void setSleepTimeThreadVerificarAlertas(
			int sleepTimeThreadVerificarAlertas) {
		SleepTimeThreadVerificarAlertas = sleepTimeThreadVerificarAlertas;
	}

	public int getIdSituacaoEnvioLVAguardandoConfRemocao() {
		return idSituacaoEnvioLVAguardandoConfRemocao;
	}
	public int getIdSituacaoEnvioLVProntoRemocao() {
		return idSituacaoEnvioLVProntoRemocao;
	}
	public int getIdSituacaoEnvioLVErroRemocao() {
		return idSituacaoEnvioLVErroRemocao;
	}
	public int getIdSituacaoEnvioLVRemovidoSite() {
		return idSituacaoEnvioLVRemovidoSite;
	}


	public int getTipoFormaPagamentoAVista() {
		return tipoFormaPagamentoAVista;
	}
	public void setTipoFormaPagamentoAVista(int tipoFormaPagamentoAVista) {
		this.tipoFormaPagamentoAVista = tipoFormaPagamentoAVista;
	}
	public int getTipoFormaPagamentoAPrazo() {
		return tipoFormaPagamentoAPrazo;
	}
	public void setTipoFormaPagamentoAPrazo(int tipoFormaPagamentoAPrazo) {
		this.tipoFormaPagamentoAPrazo = tipoFormaPagamentoAPrazo;
	}
	public int getTipoFormaPagamentoCartaoCredito() {
		return tipoFormaPagamentoCartaoCredito;
	}
	public void setTipoFormaPagamentoCartaoCredito(
			int tipoFormaPagamentoCartaoCredito) {
		this.tipoFormaPagamentoCartaoCredito = tipoFormaPagamentoCartaoCredito;
	}
	public int getTipoFormaPagamentoCartaoDebito() {
		return tipoFormaPagamentoCartaoDebito;
	}
	public void setTipoFormaPagamentoCartaoDebito(int tipoFormaPagamentoCartaoDebito) {
		this.tipoFormaPagamentoCartaoDebito = tipoFormaPagamentoCartaoDebito;
	}
	public int getIdFormaPagamentoChequePre() {
		return idFormaPagamentoChequePre;
	}
	public void setIdFormaPagamentoChequePre(int idFormaPagamentoChequePre) {
		this.idFormaPagamentoChequePre = idFormaPagamentoChequePre;
	}
	public String getStrConexaoWebServiceCatalog() {
		return strConexaoWebServiceCatalog;
	}
	public void setStrConexaoWebServiceCatalog(String strConexaoWebServiceCatalog) {
		this.strConexaoWebServiceCatalog = strConexaoWebServiceCatalog;
	}
	public String getUsuarioWebServiceCatalog() {
		return usuarioWebServiceCatalog;
	}
	public void setUsuarioWebServiceCatalog(String usuarioWebServiceCatalog) {
		this.usuarioWebServiceCatalog = usuarioWebServiceCatalog;
	}
	public String getSenhaWebServiceCatalog() {
		return senhaWebServiceCatalog;
	}
	public void setSenhaWebServiceCatalog(String senhaWebServiceCatalog) {
		this.senhaWebServiceCatalog = senhaWebServiceCatalog;
	}
	public String getStrConexaoWebServiceOrder() {
		return strConexaoWebServiceOrder;
	}
	public void setStrConexaoWebServiceOrder(String strConexaoWebServiceOrder) {
		this.strConexaoWebServiceOrder = strConexaoWebServiceOrder;
	}
	public String getUsuarioWebServiceOrder() {
		return usuarioWebServiceOrder;
	}
	public void setUsuarioWebServiceOrder(String usuarioWebServiceOrder) {
		this.usuarioWebServiceOrder = usuarioWebServiceOrder;
	}
	public String getSenhaWebServiceOrder() {
		return senhaWebServiceOrder;
	}
	public void setSenhaWebServiceOrder(String senhaWebServiceOrder) {
		this.senhaWebServiceOrder = senhaWebServiceOrder;
	}
	
	public String getStatusModificarStatusAlerta() {
		return statusModificarStatusAlerta;
	}
	public void setStatusModificarStatusAlerta(String statusModificarStatusAlerta) {
		this.statusModificarStatusAlerta = statusModificarStatusAlerta;
	}

	public int getCondicaoFinalizarAlertaPedidoVenda() {
		return condicaoFinalizarAlertaPedidoVenda;
	}
	public void setCondicaoFinalizarAlertaPedidoVenda(
			int condicaoFinalizarAlertaPedidoVenda) {
		this.condicaoFinalizarAlertaPedidoVenda = condicaoFinalizarAlertaPedidoVenda;
	}
	public int getIdSituacaoEnvioLVAguardandoSerInativado() {
		return idSituacaoEnvioLVAguardandoSerInativado;
	}
	public int getIdSituacaoEnvioLVAguardandoImagens() {
		return idSituacaoEnvioLVAguardandoImagens;
	}
	
	public int getTipoOperacaoImportPedidoVenda() {
		return tipoOperacaoImportPedidoVenda;
	}
	public void setTipoOperacaoImportPedidoVenda(int tipoOperacaoImportPedidoVenda) {
		this.tipoOperacaoImportPedidoVenda = tipoOperacaoImportPedidoVenda;
	}
	public int getTipoOperacaoImportItemPedidoVenda() {
		return tipoOperacaoImportItemPedidoVenda;
	}
	public void setTipoOperacaoImportItemPedidoVenda(
			int tipoOperacaoImportItemPedidoVenda) {
		this.tipoOperacaoImportItemPedidoVenda = tipoOperacaoImportItemPedidoVenda;
	}
	public String getStrConexaoCache() {
		return strConexaoCache;
	}
	public String getUsuarioCache() {
		return usuarioCache;
	}
	public String getSenhaCache() {
		return senhaCache;
	}
	public int getTipoOperacaoValidacaoImagens() {
		return tipoOperacaoValidacaoImagens;
	}
	public int getTipoOperacaoImportProdutos() {
		return tipoOperacaoImportProdutos;
	}
	public int getTipoOperacaoImportDepartamentos() {
		return tipoOperacaoImportDepartamentos;
	}
	public int getTipoOperacaoImportProdsDepts() {
		return tipoOperacaoImportProdsDepts;
	}
	public int getTipoOperacaoImportCodBarras() {
		return tipoOperacaoImportCodBarras;
	}
	public int getTipoOperacaoImportEstoque() {
		return tipoOperacaoImportEstoque;
	}
	
	public int getIdSituacaoEnvioLVAguardandoEvSistema() {
		return idSituacaoEnvioLVAguardandoEvSistema;
	}	
	public String getCaminhoAbsolutoFotos() {
		return caminhoAbsolutoFotos;
	}

	public int getTipoOperacaoIncAltMarca() {
		return tipoOperacaoIncAltMarca;
	}

	public int getTipoOperacaoIncAltDepartamento() {
		return tipoOperacaoIncAltDepartamento;
	}

	public int getTipoOperacaoIncAltProduto() {
		return tipoOperacaoIncAltProduto;
	}

	public int getTipoOperacaoIncAltProdutoSKU() {
		return tipoOperacaoIncAltProdutoSKU;
	}

	public int getTipoOperacaoIncAltEstoqueProduto() {
		return tipoOperacaoIncAltEstoqueProduto;
	}

	public int getTipoOperacaoIncAltPrecoProduto() {
		return tipoOperacaoIncAltPrecoProduto;
	}

	public int getTipoOperacaoIndefinida() {
		return tipoOperacaoIndefinida;
	}
	
	public int getIdSituacaoEnvioLVAguardandoAprov() {
		return idSituacaoEnvioLVAguardandoAprov;
	}

	public int getIdSituacaoEnvioLVProntoEnvio() {
		return idSituacaoEnvioLVProntoEnvio;
	}

	public int getIdSituacaoEnvioLVEnvioCompleto() {
		return idSituacaoEnvioLVEnvioCompleto;
	}

	public int getIdSituacaoEnvioLVErroEnvio() {
		return idSituacaoEnvioLVErroEnvio;
	}

	public int getIdSituacaoEnvioLVItemInativo() {
		return idSituacaoEnvioLVItemInativo;
	}

	public int getIdEmpresaLV() {
		return idEmpresaLV;
	}
	public Boolean getAlertaSonoroPedidoPendenteEnvio() {
		return alertaSonoroPedidoPendenteEnvio;
	}
	public void setAlertaSonoroPedidoPendenteEnvio(Boolean alertaSonoroPedidoPendenteEnvio) {
		this.alertaSonoroPedidoPendenteEnvio = alertaSonoroPedidoPendenteEnvio;
	}
	public Boolean getAlertaSonoroPedidoRespondido() {
		return alertaSonoroPedidoRespondido;
	}
	public void setAlertaSonoroPedidoRespondido(Boolean alertaSonoroPedidoRespondido) {
		this.alertaSonoroPedidoRespondido = alertaSonoroPedidoRespondido;
	}
	public Boolean getAlertaSonoroPedidoNovo() {
		return alertaSonoroPedidoNovo;
	}
	public void setAlertaSonoroPedidoNovo(Boolean alertaSonoroPedidoNovo) {
		this.alertaSonoroPedidoNovo = alertaSonoroPedidoNovo;
	}
	public String getUrl_Sistema() {
		return url_Sistema;
	}
	public void setUrl_Sistema(String url_Sistema) {
		this.url_Sistema = url_Sistema;
	}
	public int getTempoPedidoPendente() {
		return tempoPedidoPendente;
	}
	public void setTempoPedidoPendente(int tempoPedidoPendente) {
		this.tempoPedidoPendente = tempoPedidoPendente;
	}
	public String getCaminhoSistemaAlertaSonoro() {
		return caminhoSistemaAlertaSonoro;
	}
	public void setCaminhoSistemaAlertaSonoro(String caminhoSistemaAlertaSonoro) {
		this.caminhoSistemaAlertaSonoro = caminhoSistemaAlertaSonoro;
	}
	public String getEmailDeEnvioAlertaEmail() {
		return emailDeEnvioAlertaEmail;
	}
	public void setEmailDeEnvioAlertaEmail(String emailDeEnvioAlertaEmail) {
		this.emailDeEnvioAlertaEmail = emailDeEnvioAlertaEmail;
	}
	public String getSenhaEmailDeEnvioAlertaEmail() {
		return senhaEmailDeEnvioAlertaEmail;
	}
	public void setSenhaEmailDeEnvioAlertaEmail(String senhaEmailDeEnvioAlertaEmail) {
		this.senhaEmailDeEnvioAlertaEmail = senhaEmailDeEnvioAlertaEmail;
	}
	public String getUrlLocalFotosEstoqueProduto() {
		return urlLocalFotosEstoqueProduto;
	}
	public void setUrlLocalFotosEstoqueProduto(String urlLocalFotosEstoqueProduto) {
		this.urlLocalFotosEstoqueProduto = urlLocalFotosEstoqueProduto;
	}
	public String getUsuarioServicoSMS() {
		return usuarioServicoSMS;
	}
	public void setUsuarioServicoSMS(String usuarioServicoSMS) {
		this.usuarioServicoSMS = usuarioServicoSMS;
	}
	public String getSenhaServicoSMS() {
		return senhaServicoSMS;
	}
	public void setSenhaServicoSMS(String senhaServicoSMS) {
		this.senhaServicoSMS = senhaServicoSMS;
	}
	public int getIdTipoMovimentacaoEstoqueEntrada() {
		return idTipoMovimentacaoEstoqueEntrada;
	}
	public void setIdTipoMovimentacaoEstoqueEntrada(
			int idTipoMovimentacaoEstoqueEntrada) {
		this.idTipoMovimentacaoEstoqueEntrada = idTipoMovimentacaoEstoqueEntrada;
	}
	public int getIdEstoquePadrao() {
		return idEstoquePadrao;
	}
	public void setIdEstoquePadrao(int idEstoquePadrao) {
		this.idEstoquePadrao = idEstoquePadrao;
	}
	public String getDiretorioRelatorios() {
		return diretorioRelatorios;
	}
	public void setDiretorioRelatorios(String diretorioRelatorios) {
		this.diretorioRelatorios = diretorioRelatorios;
	}
	public String getDiretorioImagens() {
		return diretorioImagens;
	}
	public void setDiretorioImagens(String diretorioImagens) {
		this.diretorioImagens = diretorioImagens;
	}
	public int getIdSituacaoLancamentoBaixado() {
		return idSituacaoLancamentoBaixado;
	}
	public void setIdSituacaoLancamentoBaixado(int idSituacaoLancamentoBaixado) {
		this.idSituacaoLancamentoBaixado = idSituacaoLancamentoBaixado;
	}
	public int getIdTipoContaCredito() {
		return idTipoContaCredito;
	}
	public void setIdTipoContaCredito(int idTipoContaCredito) {
		this.idTipoContaCredito = idTipoContaCredito;
	}
	public int getIdTipoContaDebito() {
		return idTipoContaDebito;
	}
	public void setIdTipoContaDebito(int idTipoContaDebito) {
		this.idTipoContaDebito = idTipoContaDebito;
	}
	public int getIdTipoContaCompraMercadoria() {
		return idTipoContaCompraMercadoria;
	}
	public void setIdTipoContaCompraMercadoria(int idTipoContaCompraMercadoria) {
		this.idTipoContaCompraMercadoria = idTipoContaCompraMercadoria;
	}
	public int getIdContaGerencialCompraMercadoria() {
		return idContaGerencialCompraMercadoria;
	}
	public void setIdContaGerencialCompraMercadoria(
			int idContaGerencialCompraMercadoria) {
		this.idContaGerencialCompraMercadoria = idContaGerencialCompraMercadoria;
	}
	public int getIdCentroFinanceiroCompras() {
		return idCentroFinanceiroCompras;
	}
	public void setIdCentroFinanceiroCompras(int idCentroFinanceiroCompras) {
		this.idCentroFinanceiroCompras = idCentroFinanceiroCompras;
	}
	public int getIdSituacaoLancamentoEmAberto() {
		return idSituacaoLancamentoEmAberto;
	}
	public void setIdSituacaoLancamentoEmAberto(int idSituacaoLancamentoEmAberto) {
		this.idSituacaoLancamentoEmAberto = idSituacaoLancamentoEmAberto;
	}
	public int getIdSituacaoNotaFiscalGerada() {
		return idSituacaoNotaFiscalGerada;
	}
	public int getIdSituacaoNotaFiscalRejeitada() {
		return idSituacaoNotaFiscalRejeitada;
	}
	public int getIdStatusAlertaAlertaFinalizado() {
		return idStatusAlertaAlertaFinalizado;
	}
	public void setIdStatusAlertaAlertaFinalizado(int idStatusAlertaAlertaFinalizado) {
		this.idStatusAlertaAlertaFinalizado = idStatusAlertaAlertaFinalizado;
	}
	public int getIdStatusAlertaAlertaCancelado() {
		return idStatusAlertaAlertaCancelado;
	}
	public void setIdStatusAlertaAlertaCancelado(int idStatusAlertaAlertaCancelado) {
		this.idStatusAlertaAlertaCancelado = idStatusAlertaAlertaCancelado;
	}
	public int getIdStatusAlertaAlertaAtivo() {
		return idStatusAlertaAlertaAtivo;
	}
	public void setIdStatusAlertaAlertaAtivo(int idStatusAlertaAlertaAtivo) {
		this.idStatusAlertaAlertaAtivo = idStatusAlertaAlertaAtivo;
	}
	public int getIdStatusAlertaAlertaApropriadoParaLoja() {
		return idStatusAlertaAlertaApropriadoParaLoja;
	}
	public void setIdStatusAlertaAlertaApropriadoParaLoja(
			int idStatusAlertaAlertaApropriadoParaLoja) {
		this.idStatusAlertaAlertaApropriadoParaLoja = idStatusAlertaAlertaApropriadoParaLoja;
	}
	public int getIdSituacaoPedidoVendaFinalizado() {
		return idSituacaoPedidoVendaFinalizado;
	}
	public void setIdSituacaoPedidoVendaFinalizado(
			int idSituacaoPedidoVendaFinalizado) {
		this.idSituacaoPedidoVendaFinalizado = idSituacaoPedidoVendaFinalizado;
	}
	public int getIdSituacaoPedidoVendaAguardandoAprovacao() {
		return idSituacaoPedidoVendaAguardandoAprovacao;
	}
	public void setIdSituacaoPedidoVendaAguardandoAprovacao(
			int idSituacaoPedidoVendaAguardandoAprovacao) {
		this.idSituacaoPedidoVendaAguardandoAprovacao = idSituacaoPedidoVendaAguardandoAprovacao;
	}
	public int getIdSituacaoPedidoVendaAberto() {
		return idSituacaoPedidoVendaAberto;
	}
	public void setIdSituacaoPedidoVendaAberto(int idSituacaoPedidoVendaAberto) {
		this.idSituacaoPedidoVendaAberto = idSituacaoPedidoVendaAberto;
	}
	public int getIdSituacaoPedidoVendaAprovado() {
		return idSituacaoPedidoVendaAprovado;
	}
	public void setIdSituacaoPedidoVendaAprovado(int idSituacaoPedidoVendaAprovado) {
		this.idSituacaoPedidoVendaAprovado = idSituacaoPedidoVendaAprovado;
	}
	public int getIdSituacaoPedidoVendaCancelado() {
		return idSituacaoPedidoVendaCancelado;
	}
	public void setIdSituacaoPedidoVendaCancelado(int idSituacaoPedidoVendaCancelado) {
		this.idSituacaoPedidoVendaCancelado = idSituacaoPedidoVendaCancelado;
	}
	public int getIdStatusItemPedidoAberto() {
		return idStatusItemPedidoAberto;
	}
	public void setIdStatusItemPedidoAberto(int idStatusItemPedidoAberto) {
		this.idStatusItemPedidoAberto = idStatusItemPedidoAberto;
	}
	public int getIdStatusItemPedidoEnviado() {
		return idStatusItemPedidoEnviado;
	}
	public void setIdStatusItemPedidoEnviado(int idStatusItemPedidoEnviado) {
		this.idStatusItemPedidoEnviado = idStatusItemPedidoEnviado;
	}
	public int getIdStatusItemPedidoNaoEncontrado() {
		return idStatusItemPedidoNaoEncontrado;
	}
	public void setIdStatusItemPedidoNaoEncontrado(
			int idStatusItemPedidoNaoEncontrado) {
		this.idStatusItemPedidoNaoEncontrado = idStatusItemPedidoNaoEncontrado;
	}
	public int getIdStatusItemPedidoProntoParaEnvio() {
		return idStatusItemPedidoProntoParaEnvio;
	}
	public void setIdStatusItemPedidoProntoParaEnvio(
			int idStatusItemPedidoProntoParaEnvio) {
		this.idStatusItemPedidoProntoParaEnvio = idStatusItemPedidoProntoParaEnvio;
	}
	public int getIdStatusItemPedidoCancelado() {
		return idStatusItemPedidoCancelado;
	}
	public void setIdStatusItemPedidoCancelado(int idStatusItemPedidoCancelado) {
		this.idStatusItemPedidoCancelado = idStatusItemPedidoCancelado;
	}
		public int getIdFormaPagamentoDuplicata() {
		return idFormaPagamentoDuplicata;
	}
	public void setIdFormaPagamentoDuplicata(int idFormaPagamentoDuplicata) {
		this.idFormaPagamentoDuplicata = idFormaPagamentoDuplicata;
	}
	public int getIdTipoMovimentacaoCaixaDiaDuplicata() {
		return idTipoMovimentacaoCaixaDiaDuplicata;
	}
	public void setIdTipoMovimentacaoCaixaDiaDuplicata(
			int idTipoMovimentacaoCaixaDiaDuplicata) {
		this.idTipoMovimentacaoCaixaDiaDuplicata = idTipoMovimentacaoCaixaDiaDuplicata;
	}
	public int getIdClienteConsumidor() {
		return idClienteConsumidor;
	}
	public void setIdClienteConsumidor(int idClienteConsumidor) {
		this.idClienteConsumidor = idClienteConsumidor;
	}
	public int getIdPaisPadrao() {
		return idPaisPadrao;
	}
	public void setIdPaisPadrao(int idPaisPadrao) {
		this.idPaisPadrao = idPaisPadrao;
	}
	public int getIdTipoLogAlertaAlertaGerado() {
		return idTipoLogAlertaAlertaGerado;
	}
	public void setIdTipoLogAlertaAlertaGerado(int idTipoLogAlertaAlertaGerado) {
		this.idTipoLogAlertaAlertaGerado = idTipoLogAlertaAlertaGerado;
	}
	public int getIdTipoLogAlertaRespostaPositiva() {
		return idTipoLogAlertaRespostaPositiva;
	}
	public void setIdTipoLogAlertaRespostaPositiva(
			int idTipoLogAlertaRespostaPositiva) {
		this.idTipoLogAlertaRespostaPositiva = idTipoLogAlertaRespostaPositiva;
	}
	public int getIdTipoLogAlertaRespostaNegativa() {
		return idTipoLogAlertaRespostaNegativa;
	}
	public void setIdTipoLogAlertaRespostaNegativa(
			int idTipoLogAlertaRespostaNegativa) {
		this.idTipoLogAlertaRespostaNegativa = idTipoLogAlertaRespostaNegativa;
	}
	public int getIdTipoLogAlertaPedidoAprovado() {
		return idTipoLogAlertaPedidoAprovado;
	}
	public void setIdTipoLogAlertaPedidoAprovado(int idTipoLogAlertaPedidoAprovado) {
		this.idTipoLogAlertaPedidoAprovado = idTipoLogAlertaPedidoAprovado;
	}
	public int getIdTipoLogAlertaItemEnviado() {
		return idTipoLogAlertaItemEnviado;
	}
	public void setIdTipoLogAlertaItemEnviado(int idTipoLogAlertaItemEnviado) {
		this.idTipoLogAlertaItemEnviado = idTipoLogAlertaItemEnviado;
	}
	public int getIdTipoLogAlertaPedidoFinalizado() {
		return idTipoLogAlertaPedidoFinalizado;
	}
	public void setIdTipoLogAlertaPedidoFinalizado(
			int idTipoLogAlertaPedidoFinalizado) {
		this.idTipoLogAlertaPedidoFinalizado = idTipoLogAlertaPedidoFinalizado;
	}
	public int getIdTipoLogAlertaRespostaDepoisEnvio() {
		return idTipoLogAlertaRespostaDepoisEnvio;
	}
	public void setIdTipoLogAlertaRespostaDepoisEnvio(
			int idTipoLogAlertaRespostaDepoisEnvio) {
		this.idTipoLogAlertaRespostaDepoisEnvio = idTipoLogAlertaRespostaDepoisEnvio;
	}
	public int getIdTipoLogAlertaAlertaGeradoProdutoLojaOrigem() {
		return idTipoLogAlertaAlertaGeradoProdutoLojaOrigem;
	}
	public void setIdTipoLogAlertaAlertaGeradoProdutoLojaOrigem(
			int idTipoLogAlertaAlertaGeradoProdutoLojaOrigem) {
		this.idTipoLogAlertaAlertaGeradoProdutoLojaOrigem = idTipoLogAlertaAlertaGeradoProdutoLojaOrigem;
	}
	public int getIdTipoLogAlertaAlertaCancelado() {
		return idTipoLogAlertaAlertaCancelado;
	}
	public void setIdTipoLogAlertaAlertaCancelado(int idTipoLogAlertaAlertaCancelado) {
		this.idTipoLogAlertaAlertaCancelado = idTipoLogAlertaAlertaCancelado;
	}
	public int getIdStatusAlertaAlertaNaoEncontradoPorNenhumaLoja() {
		return idStatusAlertaAlertaNaoEncontradoPorNenhumaLoja;
	}
	public void setIdStatusAlertaAlertaNaoEncontradoPorNenhumaLoja(
			int idStatusAlertaAlertaNaoEncontradoPorNenhumaLoja) {
		this.idStatusAlertaAlertaNaoEncontradoPorNenhumaLoja = idStatusAlertaAlertaNaoEncontradoPorNenhumaLoja;
	}
	public int getIdStatusAlertaAlertaRemovidoPorTimeOut() {
		return idStatusAlertaAlertaRemovidoPorTimeOut;
	}
	public void setIdStatusAlertaAlertaRemovidoPorTimeOut(
			int idStatusAlertaAlertaRemovidoPorTimeOut) {
		this.idStatusAlertaAlertaRemovidoPorTimeOut = idStatusAlertaAlertaRemovidoPorTimeOut;
	}
	public int getIdStatusAlertaAlertaRemovidoPeloOperador() {
		return idStatusAlertaAlertaRemovidoPeloOperador;
	}
	public void setIdStatusAlertaAlertaRemovidoPeloOperador(
			int idStatusAlertaAlertaRemovidoPeloOperador) {
		this.idStatusAlertaAlertaRemovidoPeloOperador = idStatusAlertaAlertaRemovidoPeloOperador;
	}
	public int getIdStatusAlertaAlertaDesativado() {
		return idStatusAlertaAlertaDesativado;
	}
	public void setIdStatusAlertaAlertaDesativado(int idStatusAlertaAlertaDesativado) {
		this.idStatusAlertaAlertaDesativado = idStatusAlertaAlertaDesativado;
	}
	public int getIdParametrosSistemaNumeroDocumentoMovimentacaoEstoque() {
		return idParametrosSistemaNumeroDocumentoMovimentacaoEstoque;
	}
	
	public int getIdParametrosSistemaNumeroControle(){
		return idParametrosSistemaNumeroControle;
	}
	
	public void setIdParamentrosSistemaNumeroControle(int idParametrosSistemaNumeroControle){
		this.idParametrosSistemaNumeroControle = idParametrosSistemaNumeroControle;
	}
	
	public void setIdParametrosSistemaNumeroDocumentoMovimentacaoEstoque(
			int idParametrosSistemaNumeroDocumentoMovimentacaoEstoque) {
		this.idParametrosSistemaNumeroDocumentoMovimentacaoEstoque = idParametrosSistemaNumeroDocumentoMovimentacaoEstoque;
	}
	public int getCodigoCstIcmsSimplesNacional() {
		return codigoCstIcmsSimplesNacional;
	}
	private int idFormaPagamentoAcordo = 26;
	
	public int getIdFormaPagamentoAcordo() {
		return idFormaPagamentoAcordo;
	}
	public void setIdFormaPagamentoAcordo(int idFormaPagamentoAcordo) {
		this.idFormaPagamentoAcordo = idFormaPagamentoAcordo;
	}
	
	public Date getDataAtual()
	{
		return new Date();
	}
}