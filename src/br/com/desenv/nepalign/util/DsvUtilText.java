package br.com.desenv.nepalign.util;

import java.text.Normalizer;

public class DsvUtilText 
{

	public static String firstUpperCase(String str)
	{
		String arr[] = str.trim().split(" ");
		StringBuffer res = new StringBuffer();
		String piece;
		for (int i=0;i<arr.length;i++)
		{
			piece = arr[i].trim();
			if (!piece.equals(""))
			{
				res.append(piece.substring(0, 1).toUpperCase());
				res.append(piece.substring(1).toLowerCase());
				res.append(" ");
			}
		}
		return res.toString().trim();
	}
	public static String addZerosLeft(Integer val, int qt)
	{
		String strAut="%0"+qt+"d";
		return String.format(strAut, val);
	}
	public static String addZerosLeft(String val, int qt)
	{
		String strAut="%0"+qt+"d";
		return String.format(strAut, val);
	}
	 public static String retirarAcento(String texto)
	 {
		  String temp = Normalizer.normalize(texto, java.text.Normalizer.Form.NFD);
		  return temp.replaceAll("[^\\p{ASCII}]", "");
	 }	
}
