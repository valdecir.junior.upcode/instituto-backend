package br.com.desenv.nepalign.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;

import javax.persistence.EntityManagerFactory;

import org.apache.log4j.Logger;

import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

/**
 * 
 * @author lleit
 */
public class UtilDatabase 
{
	public void generateBackupFile(String database, String tables, String outputFile) throws Exception
	{
		try
		{
			final File file = new File(outputFile);
			final EntityManagerFactory managerFactory = ConexaoUtil.getFactory();
			
			final String connectionUsername  = managerFactory.getProperties().get("javax.persistence.jdbc.user").toString();
			final String connectionPassword	 = managerFactory.getProperties().get("javax.persistence.jdbc.password").toString();
			final String connectionUrl		 = managerFactory.getProperties().get("javax.persistence.jdbc.url").toString();

			if(file.exists())
				throw new Exception("Esse arquivo já existe."); 
			if(database == null)
				database = connectionUrl.split("//")[0x00000001].split("/")[0x00000001];
			
			//split na database para remover ?autoReconnect=true
			final String dumpCommand = "mysqldump " + database.split("\\?")[0x00] + " -h " + connectionUrl.split("//")[0x00000001].split("/")[0x00000000].split(":")[0x00000000] + " " + " -u " + connectionUsername + " -p" + connectionPassword + " --routines --hex-blob --default-character-set=utf8 " + tables;
			
			Logger.getRootLogger().info("Iniciando backup do Banco de Dados " + database + " no diretório " + file.getPath());
			
			try
			{ 
				final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"), 1024*8);
		        final BufferedReader reader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec(dumpCommand).getInputStream(), "UTF-8"));

		        String outData;
		        
		        while((outData=reader.readLine())!=null)
		        {
		        	writer.write(outData + "\r\n");
		        }
		       
		        reader.close();
		        
		        writer.flush();
		        writer.close();
			}
			catch(Exception exc)
			{
				throw exc;
			}
			
			Logger.getRootLogger().info("Backup terminado.");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}
	
	public void restoreBackupFile(String inputFile, String database) throws Exception
	{
		try 
		{
			Logger.getRootLogger().info("Initializing restore...");
			
			final EntityManagerFactory managerFactory = ConexaoUtil.getFactory();
			
			final String connectionUsername = managerFactory.getProperties().get("javax.persistence.jdbc.user").toString();
			final String connectionPassword = managerFactory.getProperties().get("javax.persistence.jdbc.password").toString();

			final String[] executeCmd = new String[]{"mysql", "-u" + connectionUsername, "-p" + connectionPassword, database, "-e", " source " + inputFile};

            final Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            final int processComplete = runtimeProcess.waitFor();
            
            Logger.getRootLogger().info("Restore terminated with process code " + processComplete);
        } 
		catch (Exception ex) 
        {
            ex.printStackTrace();
            throw ex;
        }
	}

	public void generateBackupFileEscritorioLoja(String database, String tables, String outputFile) throws Exception
	{
		try
		{
			final File file = new File(outputFile);
			
			if(file.exists())
				throw new Exception("Esse arquivo já existe.");
			
			final EntityManagerFactory managerFactory = ConexaoUtil.getFactory();
			
			final String connectionUsername = managerFactory.getProperties().get("javax.persistence.jdbc.user").toString();
			final String connectionPassword = managerFactory.getProperties().get("javax.persistence.jdbc.password").toString();
			final String connectionUrl = managerFactory.getProperties().get("javax.persistence.jdbc.url").toString();
			
			//split na database para remover ?autoReconnect=true
			final String dumpCommand = "mysqldump " + database.split("\\?")[0x00] + 
					" -h " + connectionUrl.split("//")[1].split("/")[0].split(":")[0] + " " + 
					" -u " + connectionUsername + " -p" + connectionPassword + " --tables " + tables;
			
			Logger.getRootLogger().info("Iniciando backup do Banco de Dados " + database + " no diretório " + file.getPath());
			
			try
			{
				final Process child = Runtime.getRuntime().exec(dumpCommand);
				
				final PrintStream ps = new PrintStream(file);
				final InputStream in = child.getInputStream();
				
				int ch;
				
				while ((ch = in.read()) != -1) 
				{
					ps.write(ch);
				}
				
				child.destroy();
				ps.close();
				in.close();
			}
			catch(Exception exc)
			{
				throw exc;
			}
			
			Logger.getRootLogger().info("Backup terminado.");	
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}
	
	public void generateBackupFileEscritorioLojaDireto(String database, String tables, String outputFile) throws Exception
	{
		try
		{
			final File file = new File(outputFile);
			
			if(file.exists())
				throw new Exception("Esse arquivo ja existe.");
						
			final String dumpCommand = "mysqldump " + database + 
					" -h " + "200.150.70.163" + " " +
					" -P "+ "33307" + 
					" -u " + "desenv" + " -p" + "desenv@3377" + " --tables " + tables;
			
			Logger.getRootLogger().info("Iniciando backup do Banco de Dados " + database + " no diretorio " + file.getPath());
			
			try
			{
				final Process child = Runtime.getRuntime().exec(dumpCommand);
				
				final PrintStream ps = new PrintStream(file);
				final InputStream in = child.getInputStream();
				
				int ch;
				
				while ((ch = in.read()) != -1) 
				{
					ps.write(ch);
				}
				
				child.destroy();
				ps.close();
				in.close();
			}
			catch(Exception exc)
			{
				throw exc;
			}
			
			Logger.getRootLogger().info("Backup terminado.");	
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}	
}
