package br.com.desenv.nepalign.util;



import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

/*
Seguinte essa class é responsável de fazer a impressão nas matriciais da epson que utilizam  a linguagem ESC/P.
Essa classe eu peguei na internet, eu fiz traduzi o nome dos métodos pra ficar mais fácil de entender e coloquei um métodos a mais que não tinham e eram necessários pra imprimir as duplicas na forma da serrale.

Eu testei na lx-350 e a na fx-890 deu certo os comandos, os comando funcionam a assim a classe faz um stream de impressão na impressora pelo caminho que é passado no método construtor na classe, pode ser cominho do compartilhamento se for pela rede, ou a porta que impressora ta conectada se for localmente.

Os comando funcionam na seguinte maneira a impressora recebe o código como fosse uma impressão, mais pra ele saber que é um comando e não uma impressão normal  todos os comandos  precisão começar com o Comando ESC,  esses comandos são  códigos ASCII.
Portanto pra enviar esse códigos par impressora podemos mandar com o valor decimal e transformando em um caracter ou com o código hexa decimal, eu usei o decimal por que o exemplo que eu peguei ja tava com decimal.

Exemplo de como usar:

o código para avançar a impressora verticalmente é o "ESC J n"
- O código Hexa seria: 1B 4A n
- O código decimal seria: 27 74 n  Com o código Hexa. Seria assim:
ps.print("\u001B\u004A" + (char)n); 
e com decimal seria assim :

ps.print( (char)27 (char)74 (char) n)
ps = instância de PrintStream
print = método do objeto
\u001B = hexa para ESC \u004A = hexa para J (char)n = o valor de ’n’.

(char)27  = comando decimal par o ESC
(char)74  = comando decimal para o J
(char) n  = o valor decimal  de ’n’.

n  = é o valor em polegadas que vc quer avançar(a impressora trabalha em polegadas  por isso que é preciso converter os centímetros pra polegadas nos métodos)

links úteis:
lista de comandos ESC/P
https://files.support.epson.com/pdf/general/escp2ref.pdf

lista de códigos ASCII pra decimal e hexa:
http://web.alfredstate.edu/weimandn/miscellaneous/ascii/ASCII%20Conversion%20Chart.gif

*/

public class DsvImpressoraMatricial {
	// metodo construtro precisa passar o caminho da impressora e tipo de pinagem da impressora se é 24 pino ou nao
    public DsvImpressoraMatricial(String printer, boolean escp24pin) {
        this.impressora = printer;
        this.escp24pin = escp24pin;
    }
    //fecha o strem da impressora precisa fazer depois de cada impressao pq senao a impressora fica com status ocupada
    public void close() {
       
        try {
            pstream.close();
            ostream.close();
        } 
        catch (IOException e) { e.printStackTrace(); }
    }
    // retorna true se o strem de impressora foi aberto com sucesso, e faz as configuracoes iniciais    
    public boolean inicializar() {
        boolean straemAbriuComSucesso = false;
        
        try {
            //crira o stream para a imperssão
            ostream = new FileOutputStream(impressora);
            pstream = new PrintStream(ostream);
            
            //reseta as configuração padrao da impressora
            pstream.print(ESC);
            pstream.print(AT);
             
            this.modoProporcional(true); // ativa o modo proporcional da impressora
            
            //seleciona o qualidade de impressão draft
            selecionarQualidadeDraft();
            
            //seta o  char set da impressora 
            setCharacterSet(BRAZIL);
            straemAbriuComSucesso = true;
        } 
        catch (FileNotFoundException e) { e.printStackTrace(); }
        
        return straemAbriuComSucesso;
    }
    public void resetaConfiguracoesPadrao(){
        pstream.print(ESC);
        pstream.print(AT);
    }
    
    public void seleciona10CPI() { //10 caracter por polegada
        pstream.print(ESC);
        pstream.print(P);
    }
    
    public void seleciona15CPI() { //15s caracter por polegada
        pstream.print(ESC);
        pstream.print(g);
    }
    
    public void selecionarQualidadeDraft() { //seleciona qualidade draft na impressão
        pstream.print(ESC);
        pstream.print(x);
        pstream.print((char) 48);
    }
    
    public void selecionarQualidadeLetter() { //seleciona qualidade letter na impressão
        pstream.print(ESC);
        pstream.print(x);
        pstream.print((char) 49);
    }
    
    //selectio o char set da impressora
    public void setCharacterSet(char charset) {
        pstream.print(ESC);
        pstream.print(PARENTESES_ESQUERDA);
        pstream.print(t);
        pstream.print(ARGUMENTO_3); //sempre 3
        pstream.print(ARGUMENTO_0); //sempre 0
        pstream.print(ARGUMENTO_1); //caracteres selecionaveis da tabela 1
        pstream.print(charset); //tabela de caracteres registrados (arg_25 é brascii)
        pstream.print(ARGUMENTO_0); //sempre 0
        
        //selecina a tabela de caracter
        pstream.print(ESC);
        pstream.print(t);
        pstream.print(ARGUMENTO_1); //caracteres selecionaveis da tabela 1
    }
    
    //vai pra proxima linha
    public void proximaLinha() {
        pstream.print(CR); //De acordo como o manual sempre que foi ir proxima linha munualmente tem que voltar o cartucho antes de ir pra proxima linha
       
        pstream.print(PROXIMA_LINHA);
    }
    
    //termina Pagina e vai pra proxima
    public void terminaPagina() {
        pstream.print(CR);  //De acordo como o manual sempre que foi ir proxima pagina munualmente tem que voltar o cartucho antes de ir pra proxima linha
        pstream.print(FF);
    }
    //ativa ou desativa o negrito
    public void negrito(boolean negrito) {
        pstream.print(ESC);
        if (negrito)
            pstream.print(E);
        else
            pstream.print(F);
    }
    //ativa o modo condensado (não sei pq mais nao funcionou na fx-890)
    public void modoCondensado(boolean condensed) {
    	
    	    pstream.print(ESC);
    	if (condensed)
    		pstream.print(SI);
    	else
    		pstream.print(DC2);
    }
    // ativa o modo proporcional
    public void modoProporcional(boolean proportional) {
        pstream.print(ESC);
        pstream.print(p);
        if (proportional)
            pstream.print((char) 49);
        else
            pstream.print((char) 48);
    }
    //ativa o modo de 8 linhas por polegada
    public void modo8LinhasPorPolegada(){
    	 pstream.print(ESC);
    	 pstream.print(ZERO);
    }
    //ativa o modo de 6 linhas por polegada
    public void modo6LinhasPorPolegada(){
    	pstream.print(ESC);
    	pstream.print(DOIS);
    }
    //avança a impressora verticalmente(proximidamente por causa do arrendodamento pq converte o centimetros para polegadas)
    public void avancoVertical(float centimetros) {
    	
        float polegadas = centimetros / CENTIMETROS_POR_POLEGADA;
        int unidades = (int) (polegadas * (escp24pin ? AVANCO_MAXIMO_24PINOS : AVANCO_MAXIMO_9PINOS));
        
        while (unidades > 0) {
            char n;
            if (unidades > MAXIMO_UNIDADES)
                n = (char) MAXIMO_UNIDADES; //se quando o avanco for maior que o permitido(255) seta o avanco para 255
            else
                n = (char) unidades; //se o avanco fico dentro dos limites 
                        
            pstream.print(ESC);
            pstream.print(J);
            pstream.print(n);
            
            unidades -= MAXIMO_UNIDADES;
        }
    }
    //Seleciona o tamanho da pagina em centimetros
    public void selecionaTamanhoPaginaCentimetros(float centimetros) {
    	
    	float polegadas = centimetros / CENTIMETROS_POR_POLEGADA;
        int unidades = (int) (polegadas * (escp24pin ? AVANCO_MAXIMO_24PINOS : AVANCO_MAXIMO_9PINOS));
    	StringBuilder sb = new StringBuilder();
    		sb.append(ESC);
    		sb.append(C);
    		sb.append(NUL);
    		sb.append(unidades);
    		pstream.print(sb.toString());
    }
    //Seleciona o tamanho da pagina em linhas
    public void selecionaTamanhoPaginaLinhas(int linhas ) {
    	
    		StringBuilder sb = new StringBuilder();
    		sb.append(ESC);
    		sb.append(C);
    		sb.append((char)linhas);
    		pstream.print(sb.toString());
    		
    	
    }
    public void avancoHorizontal(float centimetros) {
        float polegadas = centimetros / CENTIMETROS_POR_POLEGADA;
        int unidades_baixa = (int) (polegadas * 120) % 256;
        int unidades_alta = (int) (polegadas * 120) / 256;
        
        pstream.print(ESC);       
        pstream.print(BACKSPACE);
        pstream.print((char) unidades_baixa);
        pstream.print((char) unidades_alta);
    }
    
    // seleciona o posicionamento a x centimetros da margem esquerda 
    public void selecionaPosicaoAbsolutaHorizontal(float centimetros) {
        float polegadas = centimetros / CENTIMETROS_POR_POLEGADA;
        int unidades_baixa = (int) (polegadas * 60) % 256;
        int unidades_alta = (int) (polegadas * 60) / 256;
        
        pstream.print(ESC);
        pstream.print($);
        pstream.print((char) unidades_baixa);
        pstream.print((char) unidades_alta);
    }
    //faz tabs horizontais
    public void tabHorizontal(int tabs) {
        for (int i = 0; i < tabs; i++)
            pstream.print(TAB);
    }
    //seta as margens
    public void setMargins(int colunasEsqueda, int colunasDireita) {
        
        //esquerda
        pstream.print(ESC);
        pstream.print(l);
        pstream.print((char) colunasEsqueda);
        
        //direita
        pstream.print(ESC);
        pstream.print(Q);
        pstream.print((char) colunasDireita);
    }
    //imprimi
    public void print(String text) {
        pstream.print(text);
    }
    //imprimi com avanco horizontal
    public void print(String text,float avancoEmCentimetros) {
    	this.avancoHorizontal(avancoEmCentimetros);
    	pstream.print(text);
    }
    //imprimir na proxima linha 
    public void println(String texto) {
    	this.proximaLinha();
    	pstream.print(texto);
    }
    //imprimir na proxima linha com o avanco horizontal
    public void println(String texto,float avancoEmCentimetros) {
    	this.proximaLinha();
    	this.avancoHorizontal(avancoEmCentimetros);
    	pstream.print(texto);
    }
    
    //retorna verdadeira se a imperssora foi inicializada
    public boolean estaInicializada() {
        return streamOpenSuccess;
    }
    
    //retorna o nome de compartilhamento 
    public String getShare() {
        return impressora;
    }
    
    //retorna uma representacao string da impressora
    public String toString() {
        StringBuilder strb = new StringBuilder();
        strb.append("<ESCPrinter[share=").append(impressora).append(", 24pin=").append(escp24pin).append("]>");
        return strb.toString();
    }
    
    /* fields */
    private String impressora;
    private boolean escp24pin; //boolean para indicar se é  24 pinos esc/p2 epson
    private FileOutputStream ostream;
    private PrintStream pstream;
    private boolean streamOpenSuccess;
    private static int AVANCO_MAXIMO_9PINOS = 216; //se for  24/48 pinos esc/p2 tem que ser 180
    private static int AVANCO_MAXIMO_24PINOS = 180;
    private static int MAXIMO_UNIDADES = 127; 
    private static final float CENTIMETROS_POR_POLEGADA = 2.54f;
    
    /* comandos ESCP/P de decimal para ascii    */
    private static final char ESC = 27; //ESC
    private static final char SI = 15; //usado para impressao condensada
    private static final char DC2 = 18; //usado para impressao condensada
    private static final char AT = 64; //@
    private static final char PROXIMA_LINHA = 10; //usado par terminar pagina e proxima linha
    private static final char PARENTESES_ESQUERDA = 40;
    private static final char BACKSPACE = 92;
    private static final char CR = 13; //retonra o cartucho
    private static final char TAB = 9; //tab horizontal
    private static final char FF = 12; //termina pagina
    private static final char g = 103; //15cpi 
    private static final char p = 112; //usado para modo proporcional
    private static final char t = 116; //usado para o selecionar o char set
    private static final char l = 108; //usado para definir magem esquerda
    private static final char x = 120; //usado para definir qualidade de impressao draft ou letter
    private static final char E = 69; //negrito ligado
    private static final char F = 70; //negrito desligado
    private static final char J = 74; //usado para o avança vertical
    private static final char P = 80; //10cpi 
    private static final char Q = 81; //usado para definir magem direita
    private static final char $ = 36; //usado para o posicionamente absoluto horizontal
    private static final char ZERO = 48; //usado para modo 8 linhas por polegada
    private static final char DOIS = 50; //usado para modo 6 linhas por polegada
    private static final char NUL = 0; //usado para o selecionar o tamanho da pagina
    private static final char C = 67; //usado para o selecionar o tamanho da pagina
    private static final char ARGUMENTO_0 = 0;
    private static final char ARGUMENTO_1 = 1;
    private static final char ARGUMENTO_2 = 2;
    private static final char ARGUMENTO_3 = 3;
    private static final char ARGUMENTO_4 = 4;
    private static final char ARGUMENTO_5 = 5;
    private static final char ARGUMENTO_6 = 6;
    private static final char ARGUMENTO_7 = 7;
    private static final char ARGUMENTO_25 = 25;
    
    /* char sets */
    public static final char USA = ARGUMENTO_1;
    public static final char BRAZIL = ARGUMENTO_25;
}
