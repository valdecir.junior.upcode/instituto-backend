package br.com.desenv.nepalign.util;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

import br.com.desenv.nepalign.model.Impressora;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRTextExporterParameter;

public class DsvImpressaoMatricial {


	public static void imprimirRelatorio(JasperPrint relatorio,Impressora impressora) throws JRException, UnsupportedEncodingException
	{
		DsvImpressoraMatricial matricial = null; 
		byte[] bytes = null;
		String rela = "";
		try
		{
			matricial = new DsvImpressoraMatricial(impressora.getCaminho(), false); 

			ByteArrayOutputStream baos = new ByteArrayOutputStream();	

			JRTextExporter exporterTxt = new JRTextExporter();
			exporterTxt.setParameter(JRTextExporterParameter.JASPER_PRINT, relatorio);
			exporterTxt.setParameter(JRTextExporterParameter.OUTPUT_STREAM, baos);  
			exporterTxt.setParameter(JRTextExporterParameter.PAGE_HEIGHT,relatorio.getPageHeight()/2);   
			exporterTxt.setParameter(JRTextExporterParameter.PAGE_WIDTH,relatorio.getPageWidth()/2);   
			exporterTxt.setParameter(JRTextExporterParameter.CHARACTER_WIDTH,new Float(5));   
			exporterTxt.setParameter(JRTextExporterParameter.CHARACTER_HEIGHT,new Float(7));  
			exporterTxt.setParameter(JRTextExporterParameter.CHARACTER_ENCODING,"UTF-8");  


			exporterTxt.exportReport();
			bytes = baos.toByteArray();
			rela = new String(bytes, "UTF8"); // for UTF8 encoding

		}
		catch(Exception ex){
			ex.printStackTrace();
			if(matricial != null)
			{
				matricial.resetaConfiguracoesPadrao();
				matricial.close();
			}
		}
		finally{
			try{
				String[] outputStrings = rela.split("\n");
				if(matricial.inicializar()){
					matricial.modoProporcional(false);
					matricial.modo8LinhasPorPolegada();
					matricial.selecionaTamanhoPaginaLinhas(72);
					boolean primeiraLinha = true;
					for (int i = 0; i < outputStrings.length; i++) {
						String linha = outputStrings[i];
						if(!linha.trim().equals(""))
						{
							if(primeiraLinha)
							{
								matricial.print(linha);
								primeiraLinha = false;
							}
							else
							{
								matricial.println(linha);
							}
						}
					}
					matricial.resetaConfiguracoesPadrao();
					matricial.close();
				}
			}
			catch(Exception ex){
				ex.printStackTrace();
				if(matricial != null)
				{
					matricial.resetaConfiguracoesPadrao();
					matricial.close();
				}
			}
		}
	}
	public static void main(String[] args) {

	}

}
