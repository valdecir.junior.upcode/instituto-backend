package br.com.desenv.nepalign.util;

import org.apache.log4j.Logger;

public class DsvConstante
{   
	private static DsvParametrosSistema parametrosSistema=null;
	
	public static enum Grupo
	{
		JORROVI,
		SERALLE
	}
	
    private DsvConstante() 
    {    
   
    }
    
    public synchronized static DsvParametrosSistema getParametrosSistema() throws Exception
    {
        if (parametrosSistema == null) 
        {   
        	parametrosSistema = new DsvParametrosSistema();
        	parametrosSistema.carregarParametros();
        }   
        return parametrosSistema; 
    }
    
    public synchronized static void recarregarParametrosSistema() throws Exception
    {
    	if(parametrosSistema == null)
    		parametrosSistema = new DsvParametrosSistema();
    	
    	parametrosSistema.carregarParametros();
    }
       
    @Deprecated
    public synchronized static DsvParametrosSistema getInstanceParametrosSistema() throws Exception
    {   
        if (parametrosSistema == null) 
        {   
        	parametrosSistema = new DsvParametrosSistema();
        	parametrosSistema.carregarParametros();
        }   
        return parametrosSistema;
    }
    
    /**
     * Verifica em qual grupo instância está sendo executada
     * @param grupo para verificação (JORROVI, SERALLE)
     * @return True se a instância que está sendo executada for do grupo informado por paramêtro
     * @throws NullPointerException Caso não encontre o parâmetro NOME_GRUPO
     */
    public synchronized static boolean is(final String grupo) throws NullPointerException
    {
    	return is(Grupo.valueOf(grupo));
    }
    
    /**
     * Verifica em qual grupo instância está sendo executada
     * @param grupo Objeto {@link Grupo} para verificação
     * @return True se a instância que está sendo executada for do grupo informado por paramêtro
     * @throws NullPointerException Caso não encontre o parâmetro NOME_GRUPO
     */
    public synchronized static boolean is(final Grupo grupo) throws NullPointerException
    {
    	try
    	{
        	if(getParametrosSistema().get("NOME_GRUPO").equals(grupo.toString()))
        		return true;
        	else
        		return false;	
    	}
    	catch(Exception ex)
    	{
    		Logger.getRootLogger().error("Não foi possível determinar se a instância que está sendo executada é do grupo.", ex);
    		throw new NullPointerException("Não foi possível carregar o parâmetro NOME_GRUPO");
    	}
    }
    
    /**
     * Verifica se a instância está sendo executada em uma filial ou no escritório
     * @return True se a instância que está sendo executada for uma filial
     * @throws NullPointerException Caso não encontre o parâmetro EMPRESA_LOCAL e a variável de ambiente LOJA
     */
    public synchronized static boolean isLoja() throws NullPointerException
    {
    	try
    	{
        	if(System.getenv("LOJA") == null)
        	{
        		if(getParametrosSistema().get("EMPRESA_LOCAL").equals("52"))
        			return false;
        		
        		return true;
        	}

        	if(System.getenv("LOJA").equals("") || System.getenv("LOJA").equals("ESC"))
        		return false;
        	else
        		return true;
    	}
    	catch(Exception ex)
    	{
    		Logger.getRootLogger().error("Não foi possível determinar se a instância que está sendo executada é de uma filial.", ex);
    		throw new NullPointerException("Não foi possível carregar a variável de ambiente LOJA ou o parâmetro EMPRESA_LOCAL");
    	}
    }
}