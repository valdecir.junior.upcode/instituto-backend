package br.com.desenv.nepalign.util;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.HtmlEmail;

public class MailUtil 
{
	private int port = 465;
	public boolean sendMail(String from, String fromPassword, String fromAlias, String to, String hostFrom, String mailSubject, String mailMessage) throws Exception
	{
		HtmlEmail email = new HtmlEmail();
		email.setHostName(hostFrom);
		email.setSmtpPort(465);
		email.setAuthenticator(new DefaultAuthenticator(from, fromPassword));
		
		for (String toEmail : to.split(";"))
			email.addTo(toEmail);
		
		email.setFrom(from, fromAlias);
		email.setCharset("UTF-8");
		email.setSubject(mailSubject);
		email.setSSLOnConnect(true);
		email.setHtmlMsg(mailMessage);
		email.send();
		
		return true;
	}
	
	public void setPort(int port)
	{
		this.port = port;
	}
}