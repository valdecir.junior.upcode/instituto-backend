package br.com.desenv.nepalign.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Logger;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.io.CopyStreamAdapter;

public class FtpUtil 
{
	public void uploadFile(String filePath, String remoteFilePath, String serverUrl, String serverUser, String serverPass) throws Exception
	{
		Logger.getGlobal().info("Starting file transfer...");
		
		FTPClient ftpClient = new FTPClient();
		OutputStream outputStream = null;
        byte[] uploadBuffer = new byte[4096];

		try 
		{ 
			ftpClient.connect(serverUrl);

			boolean login = ftpClient.login(serverUser, serverPass); 

			if (login) 
			{
				Logger.getGlobal().info("FTP Connected");
				
				ftpClient.enterLocalPassiveMode();
				 
	            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
				
	            outputStream = ftpClient.storeFileStream(remoteFilePath);
				
	            File file = new File(filePath);
	            
				FileInputStream inputStream = new FileInputStream(file);
	            long fileSize = file.length();
	            
	            int bytesRead = -1;
	            long totalBytesRead = 0;
	            int lastPercentCompleted = 0;
	 
	            while ((bytesRead = inputStream.read(uploadBuffer)) != -1) 
	            {
	            	outputStream.write(uploadBuffer, 0, bytesRead);
	            	totalBytesRead += bytesRead;
	            	
	            	if(((int) (totalBytesRead * 100 / fileSize)) - lastPercentCompleted >= 10)
	            	{
	            		lastPercentCompleted = (int) (totalBytesRead * 100 / fileSize); 
		                
		                Logger.getGlobal().info("Upload Progress " + lastPercentCompleted + "%");
	            	}
	            }
	            
				inputStream.close();
				outputStream.close();
				 
				ftpClient.completePendingCommand();
				
				Logger.getGlobal().info("File uploaded");

				boolean logout = ftpClient.logout();
				
				if (logout) 
				{
					Logger.getGlobal().info("FTP Logoff successful");
				}
			} 
			else 
			{
				Logger.getGlobal().severe("Connect to FTP Server fail");
			}
		} 
		catch (Exception ex)
		{
			throw ex;
		}
		finally 
		{
			try 
			{
				ftpClient.disconnect();
			} 
			catch (IOException e) 
			{
				Logger.getGlobal().warning("Error on disconnect from server " + e.getMessage());
			}
		}
	}
		
	public void downloadFile(String remoteFilePath, String filePath, String serverUrl, String serverUser, String serverPass) throws Exception
	{
		Logger.getGlobal().info("Starting file transfer on URL " + serverUrl);
		
		FTPClient ftpClient = new FTPClient();

		try 
		{
			ftpClient.connect(serverUrl);

			boolean login = ftpClient.login(serverUser, serverPass);

			if (login) 
			{
				Logger.getGlobal().info("FTP Connected. Downloading");
				
				FTPFile file = ftpClient.mlistFile(remoteFilePath);
				
				final long size = file.getSize();

				ftpClient.enterLocalPassiveMode();
				 
	            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
	            
	            CopyStreamAdapter streamListener = new CopyStreamAdapter()
	            {
	            	int lastprogress = 0;
	            	
	                @Override
	                public void bytesTransferred(long totalBytesTransferred, int bytesTransferred, long streamSize) 
	                {
	                	try
	                	{
	                		int percent = (int)(totalBytesTransferred*100/size);
	                		
	                		if((percent - lastprogress) >= 10)
	                		{ 
		 	                   	lastprogress = percent;
		 	                   	Logger.getGlobal().info("Download Progress " + percent + "%");               	
	                		}
	                	}
	                    catch(Exception ex)
		                {
		                	   
		                }
	                }
	            };
	             
	            ftpClient.setCopyStreamListener(streamListener);
				
				OutputStream outputStream = new FileOutputStream(filePath); 
				
				ftpClient.retrieveFile(remoteFilePath, outputStream);
				
				outputStream.close();
				
				Logger.getGlobal().info("File downloaded");

				boolean logout = ftpClient.logout();
				
				if (logout) 
					Logger.getGlobal().info("FTP Logoff successful");
			} 
			else 
				Logger.getGlobal().severe("Connect to FTP Server fail");
		} 
		catch (Exception ex)
		{
			throw ex;
		}
		finally 
		{
			try 
			{
				ftpClient.disconnect();
			} 
			catch (IOException e) 
			{
				Logger.getGlobal().warning("Error on disconnect from server " + e.getMessage());
			}
		}
	}
	public void downloadFileAlternativo(String remoteFilePath, String filePath, String serverUrl, String serverUser, String serverPass) throws Exception
	{
		Logger.getGlobal().info("Starting file transfer...");
		FTPClient ftpClient = new FTPClient();

		try 
		{
			ftpClient.connect(serverUrl);
			boolean login = ftpClient.login(serverUser, serverPass);
			if (login) 
			{
				Logger.getGlobal().info("FTP Connected. Downloading");
				ftpClient.enterLocalPassiveMode();
	            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
				OutputStream outputStream = new FileOutputStream(new File(filePath));
				ftpClient.retrieveFile(remoteFilePath, outputStream);
				outputStream.close();
				Logger.getGlobal().info("File downloaded");
				boolean logout = ftpClient.logout();
				
				if (logout) 
				{
					Logger.getGlobal().info("FTP Logoff successful");
				}
			} 
			else 
			{
				Logger.getGlobal().severe("Connect to FTP Server fail");
			}
		} 
		catch (Exception ex)
		{
			throw ex;
		}
		finally 
		{
			try 
			{
				ftpClient.disconnect();
			} 
			catch (IOException e) 
			{
				Logger.getGlobal().warning("Error on disconnect from server " + e.getMessage());
			}
		}
	}	
}