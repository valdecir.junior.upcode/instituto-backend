/**
 * ServiceRecuperaInformacoesEstoque.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.desenv.webService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.desenv.frameworkignorante.DsvException;
import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.Cor;
import br.com.desenv.nepalign.model.EstoqueProduto;
import br.com.desenv.nepalign.model.Fornecedor;
import br.com.desenv.nepalign.model.GrupoProduto;
import br.com.desenv.nepalign.model.Marca;
import br.com.desenv.nepalign.model.OrdemProduto;
import br.com.desenv.nepalign.model.Produto;
import br.com.desenv.nepalign.model.SaldoEstoqueProduto;
import br.com.desenv.nepalign.model.Tamanho;
import br.com.desenv.nepalign.service.CorService;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.EstoqueProdutoService;
import br.com.desenv.nepalign.service.FornecedorService;
import br.com.desenv.nepalign.service.GrupoProdutoService;
import br.com.desenv.nepalign.service.MarcaService;
import br.com.desenv.nepalign.service.OrdemProdutoService;
import br.com.desenv.nepalign.service.ProdutoService;
import br.com.desenv.nepalign.service.SaldoEstoqueProdutoService;
import br.com.desenv.nepalign.service.TamanhoService;

public class ServiceRecuperaInformacoesEstoque
{
	//METODO QUE É REQUISITADO VIA WEBSERVICE E RETORNA O OBJETO SALDO NO FORMA DE UM OBJETO DE BYTES BASE64

	public byte[] recuperarVenda(String codigoBarras,String idempresa,int mes,int ano) throws DsvException
	{
		
		String where = "";
		List<SaldoEstoqueProduto> lista  = new ArrayList<>();
		try	// TENTA RECUPERAR O SALDO DO MESMO ANO, MES E GRUPO DA MESAS SOLICITANTE
		{
		
			where = "";
			where = where + " idEmpresaFisica in(" + EmpresaFisicaService.recuperarGrupoEmpresa(Integer.parseInt(idempresa)) + ")"; 
			where = where + " and idestoqueproduto = "+codigoBarras; 
			where = where + " and ano = "+ano;
			where = where + " and mes = "+mes;
			where = where + " and venda is not null and venda > 0  ";
			lista = new SaldoEstoqueProdutoService().listarPaginadoSqlLivreGenerico(1, 1, where).getRecordList();
			
			if(lista.size()>0)
			{
				final SaldoEstoqueProduto saldoEstoqueProduto = lista.get(0);
				saldoEstoqueProduto.setCusto(null);
				saldoEstoqueProduto.setCustoMedio(null);
				saldoEstoqueProduto.setUltimoCusto(null);
				
				return new IgnUtil().transformaObjetoByteArray(saldoEstoqueProduto);
			}
			else// SE CASO A NAO ACHAR O SALDO ELE TENTA ACHAR O SALDO NO OUTRO GRUPO
			{
				where = "";
				 
				where = where + "  idestoqueproduto = "+codigoBarras; 
				where = where + " and ano = "+ano;
				where = where + " and mes = "+mes;
				where = where + " and venda is not null and venda > 0  ";		
				lista = new SaldoEstoqueProdutoService().listarPaginadoSqlLivreGenerico(1, 1, where).getRecordList();
				
				if(lista.size()>0)
				{
					final SaldoEstoqueProduto saldoEstoqueProduto = lista.get(0);
					saldoEstoqueProduto.setCusto(null);
					saldoEstoqueProduto.setCustoMedio(null);
					saldoEstoqueProduto.setUltimoCusto(null);
					
					return new IgnUtil().transformaObjetoByteArray(saldoEstoqueProduto);
				}
			}
			if(lista.size()<1)// CASO NAO ACHE ELE TENA ACHAR O SALDO DO MES E ANO MAIS PROXIMO DO ABERTO DA EMPRESA SOLICITANTE E DO GRUPO
			{
				where = "";
				where = where + " idEmpresaFisica in(" + EmpresaFisicaService.recuperarGrupoEmpresa(Integer.parseInt(idempresa)) + ")"; 
				where = where + " and idestoqueproduto = "+codigoBarras; 
				where = where + " and venda is not null and venda > 0  ";
				lista = new SaldoEstoqueProdutoService().listarPaginadoSqlLivreGenerico(1, 1, where,"ano desc,mes","desc").getRecordList();
				
				if(lista.size()>0)
				{
					final SaldoEstoqueProduto saldoEstoqueProduto = lista.get(0);
					saldoEstoqueProduto.setCusto(null);
					saldoEstoqueProduto.setCustoMedio(null);
					saldoEstoqueProduto.setUltimoCusto(null);
					
					return new IgnUtil().transformaObjetoByteArray(saldoEstoqueProduto);					
				}
				else // CASO NAO ACHE ELE TENA ACHAR O SALDO DO MES E ANO MAIS PROXIMO DO ABERTO DA EMPRESA SOLICITANTE E DO OUTRO GRUPO
				{
					where = "";
					where = where + " idestoqueproduto = "+codigoBarras; 
					where = where + " and venda is not null and venda > 0  ";
					lista = new SaldoEstoqueProdutoService().listarPaginadoSqlLivreGenerico(1, 1, where,"ano desc,mes","desc").getRecordList();
					
					if(lista.size()>0)
					{
						final SaldoEstoqueProduto saldoEstoqueProduto = lista.get(0);
						saldoEstoqueProduto.setCusto(null);
						saldoEstoqueProduto.setCustoMedio(null);
						saldoEstoqueProduto.setUltimoCusto(null);
						
						return new IgnUtil().transformaObjetoByteArray(saldoEstoqueProduto);
					}
				}
				return null;

			}
		}
		catch(Exception ex)
		{
			throw new DsvException(ex);
		}
		return null;

	}
	public String recupecarEstoqueProduto(Integer idEstoqueProduto) throws Exception
	{
		String retorno = null;
		EstoqueProduto estoqueProduto = null;

		estoqueProduto = new EstoqueProdutoService().recuperarPorId(idEstoqueProduto);
		if(idEstoqueProduto != null  && idEstoqueProduto >0)
		{

			if(estoqueProduto!=null)
			{
				retorno = ""+estoqueProduto.getCodigoBarras();
				retorno += ","+estoqueProduto.getCor().getId();
				retorno += ","+estoqueProduto.getObservacao();
				retorno += ","+estoqueProduto.getOrdemProduto().getId();
				retorno += ","+estoqueProduto.getProduto().getId();
				retorno += ","+estoqueProduto.getSituacaoEnvioLV().getId();
				retorno += ","+estoqueProduto.getTamanho().getId();
				return retorno;
			}
			else
			{
				return null;
			}

		}
		else
		{
			return null;
		}
	}
	public String recuperarCor(Integer idCor) throws Exception
	{
		String retorno = null;
		Cor cor = null;
		cor = new CorService().recuperarPorId(idCor);
		if(idCor != null  && idCor >0)
		{
			if(cor != null)
			{
				retorno = ""+cor.getCodigo();
				retorno += ","+cor.getCorBasica1().getId();
				retorno += ","+cor.getCorBasica2().getId();
				retorno += ","+cor.getCorBasica3().getId();
				retorno += ","+cor.getDescricaoCorEmpresa();
				retorno += ","+cor.getDescricaoCorFabrica();
				return retorno;
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}
	public String recuperarOrdemProduto(Integer idOrdemProduto) throws Exception
	{
		String retorno = null;
		OrdemProduto ordemProduto = null;
		ordemProduto = new OrdemProdutoService().recuperarPorId(idOrdemProduto);
		if(idOrdemProduto != null  && idOrdemProduto >0)
		{
			if(ordemProduto != null)
			{
				retorno = ""+ordemProduto.getCodigo();
				retorno += ","+ordemProduto.getDescricao();
				return retorno;
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}
	public String recuperarProduto(Integer idProduto) throws Exception
	{

		String retorno = null;
		Produto produto = null;
		produto = new ProdutoService().recuperarPorId(idProduto);
		if(idProduto != null  && idProduto >0)
		{
			if(produto != null)
			{
				retorno = ""+produto.getAliquotaIcms();
				retorno += ","+produto.getAltura();
				retorno += ","+produto.getAplicacao();
				retorno += ","+produto.getAtivoInativo();
				retorno += ","+produto.getCodigoCatalogo();
				retorno += ","+produto.getCodigoEan();
				retorno += ","+produto.getCodigoFabricante();
				retorno += ","+produto.getCodigoNcm();
				retorno += ","+produto.getCodigoProduto();
				retorno += ","+produto.getExptipi();
				retorno += ","+produto.getFornecedor().getId();
				retorno += ","+produto.getGarantia();
				retorno += ","+produto.getGenero().getId();
				retorno += ","+produto.getGrupo().getId();
				retorno += ","+produto.getLargura();
				retorno += ","+produto.getLocalfoto();
				retorno += ","+produto.getMarca().getId();
				retorno += ","+produto.getNome1Produto();
				retorno += ","+produto.getNome2Produto();
				retorno += ","+produto.getNome3Produto();
				retorno += ","+produto.getNome4Produto();
				retorno += ","+produto.getNome5Produto();
				retorno += ","+produto.getNomeProduto();
				retorno += ","+produto.getOrigemMercadoria().getId();
				retorno += ","+produto.getPesoProduto();
				retorno += ","+produto.getPrecoPromocao();
				retorno += ","+produto.getProdutoLV();
				retorno += ","+produto.getProfundidade();
				retorno += ","+produto.getSituacaoTributaria();
				retorno += ","+produto.getUnidadeMedida().getId();
				retorno += ","+produto.getValoresDescritivo1();
				retorno += ","+produto.getValoresDescritivo2();
				retorno += ","+produto.getValoresDescritivo3();

				return retorno;
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}
	public String recuperaFornecedor(Integer idFornecedor) throws Exception
	{
		String retorno = null;
		Fornecedor fornecedor = null;
		fornecedor = new FornecedorService().recuperarPorId(idFornecedor);
		if(idFornecedor != null  && idFornecedor >0)
		{
			if(fornecedor != null)
			{
				retorno = ""+fornecedor.getAtividade().getId();
				retorno += ","+fornecedor.getBairro();
				retorno += ","+fornecedor.getCategoriaFornecedor().getId();
				retorno += ","+fornecedor.getCep();
				retorno += ","+fornecedor.getCidade().getId();
				retorno += ","+fornecedor.getClassificacaoFornecedor().getId();
				retorno += ","+fornecedor.getCnpjCpf();
				retorno += ","+fornecedor.getComentario();
				retorno += ","+fornecedor.getComplementoEndereco();
				retorno += ","+fornecedor.getContato();
				retorno += ","+fornecedor.getEmail();
				retorno += ","+fornecedor.getEnderecoFornecedor();
				retorno += ","+fornecedor.getFax();
				retorno += ","+fornecedor.getInscricaoEstadualRg();
				retorno += ","+fornecedor.getInscricaoMunicipal();
				retorno += ","+fornecedor.getNomeEtiqueta();
				retorno += ","+fornecedor.getNomeFantasia();
				retorno += ","+fornecedor.getObservacao();
				retorno += ","+fornecedor.getRazaoSocial();
				retorno += ","+fornecedor.getTelefone();
				retorno += ","+fornecedor.getTipoFornecedor();

				return retorno;
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}
	public Marca recuperaMarca(int idMarca) throws Exception
	{
		Marca retorno = null;
		Marca marca = null;
		marca = new MarcaService().recuperarPorId(idMarca);
		if( idMarca >0)
		{
			if(marca != null)
			{
//				retorno = ""+marca.getCodigo();
//				retorno += ","+marca.getDescricao();
//				retorno += ","+marca.getFranquia();
				return marca;
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}
	public String recuperarGrupoProduto(Integer idGrupoProduto) throws Exception
	{
		String retorno = null;
		GrupoProduto grupoProduto = null;
		grupoProduto = new GrupoProdutoService().recuperarPorId(idGrupoProduto);
		if(idGrupoProduto != null  && idGrupoProduto >0)
		{
			if(grupoProduto != null)
			{
				retorno = ""+grupoProduto.getCodigo();
				retorno += ","+grupoProduto.getDescricao();
				return retorno;
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
		
	}
	public String recuperarTamanho(Integer idTamanho) throws Exception
	{
		String retorno = null;
		Tamanho tamanho = null;
		tamanho = new TamanhoService().recuperarPorId(idTamanho);
		if(idTamanho != null  && idTamanho >0)
		{
			if(tamanho != null)
			{
				retorno = ""+tamanho.getCodigo();
				retorno += ","+tamanho.getDescricao();
				return retorno;
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
		
	}
}
