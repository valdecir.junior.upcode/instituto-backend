package br.com.desenv.frameworkignorante;

import java.io.IOException;

import flex.messaging.io.SerializationContext;
import flex.messaging.io.amf.Amf0Input;

public class AMF0Input extends Amf0Input 
{
    public AMF0Input(SerializationContext context) 
    {
        super(context);
    }

    @Override
    public Object readObject() throws ClassNotFoundException, IOException 
    {
        if (avmPlusInput == null) 
        {
            avmPlusInput = new AMF3Input(context);
            avmPlusInput.setDebugTrace(trace);
            avmPlusInput.setInputStream(in);
        }
        return super.readObject();
    }
}