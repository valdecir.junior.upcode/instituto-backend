package br.com.desenv.frameworkignorante;

public class SearchParameter
{
	private String attributeName;
	private String operator;
	private Object value;
	
	public SearchParameter()
	{
		super();
	}


	public SearchParameter(String attributeName, String operator, Object value)
	{
		super();
		this.attributeName = attributeName;
		this.operator = operator;
		this.value = value;
	}

	public String getAttributeName()
	{
		return attributeName;
	}

	public void setAttributeName(String attributeName)
	{
		this.attributeName = attributeName;
	}

	public String getOperator()
	{
		return operator;
	}

	public void setOperator(String operator)
	{
		this.operator = operator;
	}

	public Object getValue()
	{
		return value;
	}

	public void setValue(Object value)
	{
		this.value = value;
	}

}
