package br.com.desenv.frameworkignorante;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import br.com.desenv.nepalign.model.Acao;
import br.com.desenv.nepalign.model.Funcionalidade;
import br.com.desenv.nepalign.model.Usuario;
import flex.messaging.FlexContext;
import flex.messaging.FlexSession;
import flex.messaging.endpoints.AMFEndpoint;
import flex.messaging.messages.CommandMessage;
import flex.messaging.messages.Message;
import flex.messaging.messages.RemotingMessage;
import flex.messaging.security.SecurityException;

public class IgnAMFEndpoint extends AMFEndpoint 
{
	static
	{
		PropertyConfigurator.configureAndWatch("log4j.properties", 30 * 1000);
	}
	
	private static final Logger loggerSecurity = Logger.getLogger("NEPAL.SECURITY");
	private static final Logger loggerActionTracer = Logger.getLogger("NEPAL.ACTION.TRACE");
	
	@Override
    public Message serviceMessage(Message message)
	{
		if(message instanceof CommandMessage)
		{
			final CommandMessage cmdMessage = (CommandMessage) message;
			
			switch(cmdMessage.getOperation())
			{
				case CommandMessage.CLIENT_PING_OPERATION:
					return super.serviceMessage(message);
				case CommandMessage.DISCONNECT_OPERATION:
					return super.serviceMessage(message);
					default:
						System.out.println("Unknow operation on CommandMessage " + cmdMessage.getOperation());
						return super.serviceMessage(cmdMessage);
			}
		}
		else if(message instanceof RemotingMessage)
		{
			final FlexSession session = FlexContext.getFlexSession();
			final Usuario usuario = (Usuario) session.getAttribute("usuarioLogado");
			final String operation = ((RemotingMessage) message).getOperation();
			final String service = ((RemotingMessage) message).getDestination();
			
			try
			{
				if (service.equalsIgnoreCase("usuarioService") && 
						(operation.equalsIgnoreCase("logoutUsuario") || operation.equalsIgnoreCase("loginUsuario") || 
						operation.equalsIgnoreCase("validaLogin")    || operation.equalsIgnoreCase("iniciarComoVendedor") ||
						operation.equalsIgnoreCase("colocarDadosFlexSession")))
						return super.serviceMessage(message);
					if (service.equalsIgnoreCase("dsvParametrosSistemaService") && (operation.equalsIgnoreCase("listaParametrosCarregados")))
						return super.serviceMessage(message);
					else if (usuario == null || !session.isValid())
					{
						final Map<String , String> info = new HashMap<String, String>();
						info.put("mensagem", "Usuário não conectado! Faça o log-in para completar a operação");
						info.put("servico", service);
						info.put("operacao", operation);
						
						final SecurityException se = new SecurityException();
						se.setExtendedData(info);
						
						loggerSecurity.fatal("Unauthorized access to ".concat(operation).concat(" in ").concat(service).concat(" was requested!!!! "), se);
						
						throw se;
					}
					/* usuários com anotação desenv@master tem acesso privilegiado */
					else if (usuario.getUsuarioDesenv() != null && usuario.getUsuarioDesenv().equalsIgnoreCase("desenv@master"))
					{
						/* verificacao da anotação IgnSegurancaAcao */
						//IgnSegurancaService.isMethodSigned(operation, service);
						
						return super.serviceMessage(message);	
					}			
					else
					{
						/* verificacao da anotação IgnSegurancaAcao */
						//IgnSegurancaService.isMethodSigned(operation, service);
						
						if (IgnSegurancaService.getListaUsuarios().contains(usuario))
						{
							final int index = IgnSegurancaService.getListaUsuarios().indexOf(usuario);
							final Usuario usuarioAcesso = IgnSegurancaService.getListaUsuarios().get(index);
					
							final Funcionalidade funcionalidade = new Funcionalidade();
							funcionalidade.setNomeServico(service);

							final Acao criterioAcao = new Acao();
							criterioAcao.setFuncionalidade(funcionalidade);
							criterioAcao.setNomeMetodo(operation);
						
							if (usuarioAcesso.getAcoesLiberadas().contains(criterioAcao))
								return super.serviceMessage(message);							
							else if (usuario.getUsuarioDesenv()!=null && usuario.getUsuarioDesenv().toString().equals("desenv@trace"))
							{
								loggerActionTracer.info("[ACTION.TRACE] Usuário [" + usuario.getLogin() + "] Destination [" + service + "] Operation [" + operation + "]");
								return super.serviceMessage(message);
							}						
							else
							{
								loggerSecurity.warn("[SEM PERMISSÃO] Usuário [" + usuario.getLogin() + "] Destination [" + service + "] Operation [" + operation + "]");
								
								final Map<String , String> info = new HashMap<String, String>();
								info.put("mensagem", "Você não possui permissão para realizar essa operação!");
								info.put("servico", service);
								info.put("operacao", operation.toString());
								
								final SecurityException se = new SecurityException();
								se.setExtendedData(info);
								se.setMessage(info.get("mensagem")+"\n\nDados da operação\n\nServiço : "+info.get("servico")+"\nOperação : "+info.get("operacao")+"\n");
								throw se;
							}
						}
						else
						{
							loggerSecurity.warn("[SEM PERMISSÃO] Usuário [" + usuario.getLogin() + "] Destination [" + service + "] Operation [" + operation + "]");
							
							final Map<String , String> info = new HashMap<String, String>();
							info.put("mensagem", "Você não possui permissão para realizar essa operação!");
							info.put("servico", service);
							info.put("operacao", operation.toString());
							
							final SecurityException se = new SecurityException();
							se.setExtendedData(info);
							se.setMessage(info.get("mensagem")+"\n\nDados da operação\n\nServiço : "+info.get("servico")+"\nOperação : "+info.get("operacao")+"\n");
							throw se;		
						}
					}	
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				throw ex;
			}
		}
		else
		{
			System.out.println("Unknow message type received");
			return super.serviceMessage(message);
		}
	}
	
    @Override
    protected String getSerializerClassName() 
    {
        return Serializer.class.getName();
    }

    @Override
    protected String getDeserializerClassName() 
    {
        return Deserializer.class.getName(); 
    }	
}