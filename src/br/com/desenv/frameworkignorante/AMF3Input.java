package br.com.desenv.frameworkignorante;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import flex.messaging.io.SerializationContext;
import flex.messaging.io.amf.Amf3Input;

public class AMF3Input extends Amf3Input 
{
    public AMF3Input(SerializationContext context) 
    {
        super(context);
    }
    
    @SuppressWarnings("unchecked")
	@Override
    protected Date readDate() throws IOException 
    {
        final int ref = readUInt29();

        if ((ref & 1) == 0)
        {
            // This is a reference
            return (Date)getObjectReference(ref >> 1);
        }
        else
        {
            final long time = (long)in.readDouble();
            
            final Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(time);
            
            if(calendar.getTimeZone().useDaylightTime() && calendar.get(Calendar.YEAR) < 1986)
            	calendar.setTimeInMillis(time + calendar.getTimeZone().getDSTSavings());

            final Date d = calendar.getTime();

            //Remember Date
            objectTable.add(d);

            if (isDebug)
                trace.write(d);

            return d;
        }
    } 
}