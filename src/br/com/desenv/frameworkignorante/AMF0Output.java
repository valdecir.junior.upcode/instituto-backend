package br.com.desenv.frameworkignorante;

import flex.messaging.io.SerializationContext;
import flex.messaging.io.amf.Amf0Output;

public class AMF0Output extends Amf0Output 
{
    public AMF0Output(SerializationContext context) 
    {
        super(context);
    }

    @Override
    protected void createAMF3Output()
    {
        avmPlusOutput = new AMF3Output(context);
        avmPlusOutput.setOutputStream(out);
        avmPlusOutput.setDebugTrace(trace);
    }
}