package br.com.desenv.frameworkignorante;

import java.util.List;

import org.apache.log4j.Logger;

import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.nepalign.service.UsuarioService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.wsdl.SessionInfo;

import com.desenv.nepal.integracao.util.DesenvXMLUtil;

public class IgnAuthenticationServiceInterface 
{
	private static UsuarioService usuarioService;
	private static DesenvXMLUtil desenvXMLUtil; 
	
	public IgnAuthenticationServiceInterface()
	{
		try 
		{
			usuarioService = new UsuarioService();
			desenvXMLUtil = new DesenvXMLUtil();
		}
		catch (Exception e) 
		{
			Logger.getRootLogger().error("Não foi possível instânciar IgnAuthenticationServiceIntercace.", e);
		}
	}
	
	public String processLogin(String login, String senha) throws DsvException, Exception
	{
		return processLoginFromPort(login, senha, null);
	}

	public String processLoginFromPort(String login, String senha, String port) throws DsvException, Exception
	{
		final SessionInfo sessionInfo = new SessionInfo();

		try
		{
			@SuppressWarnings("rawtypes")
			final List usuarioValidado = usuarioService.validaLoginWebService(login, senha);
		
			sessionInfo.setUsuario((Usuario) usuarioValidado.get(0));
			sessionInfo.setIsLoja(DsvConstante.isLoja() || ((Usuario) usuarioValidado.get(0)).getEmpresaPadrao().getId().intValue() != 52);
			sessionInfo.setGrupo(DsvConstante.getParametrosSistema().get("NOME_GRUPO"));
		}
		catch(Exception ex)
		{
			throw new DsvException(ex);
		}
		 
		if(port == null)
		{
			sessionInfo.setAmfChannelUrl(DsvConstante.getParametrosSistema().get(sessionInfo.getUsuario().getEmpresaPadrao().getId(), "URL_AMF_CHANNEL"));
			//sessionInfo.setAmfChannelUrl("http://localhost:8080/smartsig/messagebroker/amf/");
		}
		else
		{
			sessionInfo.setAmfChannelUrl(DsvConstante.getParametrosSistema().get(sessionInfo.getUsuario().getEmpresaPadrao().getId(), "URL_AMF_CHANNEL_REMOTE"));
			
			if(sessionInfo.getAmfChannelUrl() == null)
			{
				Logger.getRootLogger().warn("Tentativa de conexão remota sem o parâmetro de AMF REMOTO!");
				sessionInfo.setAmfChannelUrl(DsvConstante.getParametrosSistema().get(sessionInfo.getUsuario().getEmpresaPadrao().getId(), "URL_AMF_CHANNEL"));
			}
		}

		return desenvXMLUtil.objectToXML(sessionInfo, SessionInfo.class);
	}
}