package br.com.desenv.frameworkignorante;

import java.io.*;
import java.util.*;

public class FileAgeFilter implements FilenameFilter 
{
    private Date startDate;
	
    public FileAgeFilter(Date _startDate) 
    {
        startDate = _startDate;
    }

    public boolean accept(File dir, String name) 
    {
        return new Date(new File(dir, name).lastModified()).after(startDate);        	
    }
}