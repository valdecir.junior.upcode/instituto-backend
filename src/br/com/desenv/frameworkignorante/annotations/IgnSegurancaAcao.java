package br.com.desenv.frameworkignorante.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.persistence.EntityManager;

import common.Logger;

import br.com.desenv.nepalign.service.GrupoService;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface IgnSegurancaAcao 
{
	
	public enum Grupo 
	{ 
		Administrador(0x00000001),
		UsuarioPadrao(0x00000002),
		Publico(0x00000003),
		Compras(0x00000004),
		Crediario(0x00000005),
		Vendedor(0x00000006),
		Gerente(0x00000007),
		Estoque(0x00000008),
		Caixa(0x00000009),
		Faturamento(0x0000000A),
		Financeiro(0x0000000B);
		
		private static final GrupoService grupoService = new GrupoService();
		
		private final int id;
		
		Grupo(final int id)
		{
			this.id = id;
		}
		
		public final br.com.desenv.nepalign.model.Grupo toGrupo(EntityManager manager) throws Exception
		{
			java.util.logging.Logger.getGlobal().info("Seguran�a: Recupera grupo de usu�rio: " + id);
			return grupoService.recuperarPorCodigo(manager, id);
		}
	};
	
	public boolean enabled() default true;
	
	public String nomeAcao() default "unk";
	
	public Grupo[] gruposAutorizados() default {};
}