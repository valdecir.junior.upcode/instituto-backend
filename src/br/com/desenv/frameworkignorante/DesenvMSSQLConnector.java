package br.com.desenv.frameworkignorante;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
public class DesenvMSSQLConnector 
{
	private Connection sqlConn;
	
	public Connection getSqlConnection()
	{
		return this.sqlConn;
	}
	
	public DesenvMSSQLConnector(String host, String user, String password, String databaseName)
	{
		try
		{
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		}
		catch(java.lang.ClassNotFoundException e)
		{
				System.err.print("ClassNotFoundException: ");
				System.err.println(e.getMessage());
		} 
		
		try
		{
			sqlConn = DriverManager.getConnection("jdbc:sqlserver://" + host + "; databaseName=" + databaseName + ";", user, password);
		}
		catch(SQLException exc)
		{
			exc.printStackTrace();
		}
	}
	
	public void setAutoCommit(Boolean bool) throws SQLException
	{
		this.sqlConn.setAutoCommit(bool);
	}
	
	public void disposeConn() throws SQLException
	{
		if(sqlConn != null)
		{
			sqlConn.close();
		}
	}
			
	public Statement getNewStatement() throws SQLException 
	{
		try
		{
			return sqlConn.createStatement();
		}
		catch(SQLException exc)
		{
			exc.printStackTrace();
			throw exc;
		}
	}
	
	public void resultQuery(String query, Statement sqlStatement) throws SQLException
	{
		try
		{
			sqlStatement.executeUpdate(query);
			
		}
		catch(SQLException exc)
		{
			exc.printStackTrace();
			throw exc;
		}
	}
	
	public void executeStoredProcedure(String procName) throws SQLException
	{
		try
		{
			sqlConn.prepareStatement("exec " + procName).execute();
		}
		catch(SQLException exc)
		{
			exc.printStackTrace();
			throw exc;
		}
	}
}
