package br.com.desenv.frameworkignorante;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.swing.filechooser.FileSystemView;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import br.com.desenv.nepalign.model.ParametrosSistema;
import br.com.desenv.nepalign.service.ParametrosSistemaService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.util.UtilDatabase;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

/**
 * Runnable para execução de Backup
 * @author lleit
 */
public class BackupRunner implements Runnable 
{
	private static final Logger logger = Logger.getLogger(BackupRunner.class);
	
	public static final short POLICY_COMPLETE_BACKUP = 0x01;
	public static final short POLICY_DATABASE_ONLY = 0x02;
	public static final short POLICY_PHOTO_ONLY = 0x03;
	
	private String fileName;
	private String numeroEmpresaFisica;
	private String parametroLog;
	private File dispositivo;
	
	private Date dataHoraBackup;
	
	private boolean overwriteFile = false;
	private boolean manualTask = false;
	private boolean databaseOnly = false;
	
	/**
	 * Define o nome do arquivo de Backup.
	 * @param name Nome do Arquivo
	 */
	public void setFileName(final String name)
	{
		manualTask = true;
		fileName = name;
	}
	
	/**
	 * Define o tipo de Backup. Por padrão é definido POLICY_COMPLETE_BACKUP
	 * @param policy 
	 * POLICY_COMPLETE_BACKUP Faz backup de Fotos e Database; 
	 * POLICY_DATABASE_ONLY Faz backup apenas da Database; 
	 * POLICY_PHOTO_ONLY Faz backup apenas da foto
	 */
	public void setBackupPolicy(final short policy)
	{
		databaseOnly = policy == POLICY_DATABASE_ONLY;
	}
	
	/**
	 * Define se os arquivos de backup podem ser sobreescrevidos.
	 * @param overwrite True se sim, False se não.
	 */
	public void setOverwritePolicy(final boolean overwrite)
	{
		overwriteFile = overwrite;
	}
	
	/**
	 * Construtor. 
	 * @param dispositivo Caminho do Backup
	 */
	public BackupRunner(File dispositivo)
	{
		super();
		
		this.dispositivo = dispositivo;
		
		try
		{
			numeroEmpresaFisica = System.getenv("LOJA");
			
			if (numeroEmpresaFisica == null)
				numeroEmpresaFisica = String.format("%02d", DsvConstante.getParametrosSistema().getInt("EMPRESA_LOCAL"));
			
			parametroLog = "LOG_ULTIMO_BACKUP_".concat(numeroEmpresaFisica);
			
			dataHoraBackup = new Date();
		}
		catch(Exception ex)
		{
			logger.error("Não foi possível instanciar classe de Backup", ex);
		}
	}
	
	private void atualizarParametro(EntityManager manager, EntityTransaction transaction, String nomeParametro, String valor) throws Exception
	{
		ParametrosSistema parametrosSistemaFiltro = new ParametrosSistema();
		parametrosSistemaFiltro.setDescricao(nomeParametro);
		
		ParametrosSistema parametroUltimaAtualizacao = null;
		
		ParametrosSistemaService parametrosSistemaService = new ParametrosSistemaService();
		
		List<ParametrosSistema> listaParametros = parametrosSistemaService.listarPorObjetoFiltro(manager, parametrosSistemaFiltro, null, null, null);
		
		if(listaParametros.size() == 0x01)
		{
			parametroUltimaAtualizacao = listaParametros.get(0x00);
			parametroUltimaAtualizacao.setValor(valor);
		}
		else if(listaParametros.size() == 0x00)
		{
			parametroUltimaAtualizacao = new ParametrosSistema();
			parametroUltimaAtualizacao.setDescricao(nomeParametro);
			parametroUltimaAtualizacao.setValor(valor);
		}
		
		parametrosSistemaService.atualizar(manager, transaction, parametroUltimaAtualizacao);	
	}
	
	private String nomeArquivoDump()
	{
		return manualTask ? fileName : "SYS_BACKUP_" + new SimpleDateFormat("dd-MM-yyyy-mm-ss").format(dataHoraBackup) + ".NEPALDUMP";
	}
	
	@Override
	public void run() 
	{
		EntityManager manager = null;
		
		try
		{
			final String tables = DsvConstante.getParametrosSistema().get("TABELAS_BACKUP_SISTEMA");
			
			manager = ConexaoUtil.getEntityManager();
			manager.getTransaction().begin();
			 
			String backupVerbose = "[" + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()) + "] Iniciando Backup..." +
					"\n[" + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()) + "] Unidade de Backup [" + FileSystemView.getFileSystemView().getSystemDisplayName(dispositivo) + "]" +
					"\n[" + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()) + "] Backup em andamento...";
			
			logger.info("Backup iniciado na Unidade " + FileSystemView.getFileSystemView().getSystemDisplayName(dispositivo));
			
			atualizarParametro(manager, manager.getTransaction(), parametroLog, backupVerbose);
			
			manager.getTransaction().commit();
			manager.getTransaction().begin();
			
			final File backupFile = new File(nomeArquivoDump());
			
			if(overwriteFile)
			{
				if(backupFile.exists())
					backupFile.delete();
			}
			
			new UtilDatabase().generateBackupFile(numeroEmpresaFisica.equals("52") ? "nepalign" : null, tables, nomeArquivoDump().concat(manualTask ? "" : ".tmp"));
			IgnUtil.compactarArquivo(nomeArquivoDump().concat(manualTask ? "" : ".tmp"), nomeArquivoDump() + ".rar");
			
			if(!manualTask)
				FileUtils.copyFile(new File(nomeArquivoDump() + ".rar"), new File(dispositivo.getPath() + "/" + nomeArquivoDump() + ".rar"));
			
			if(!manualTask)
			{
				new File(nomeArquivoDump() + ".tmp").delete();
				new File(nomeArquivoDump() + ".rar").delete();	
			}
			else
			{
				if(backupFile.exists())
					backupFile.delete();
			}

			if(!databaseOnly)
			{
				
				backupVerbose += "\n[" + IgnUtil.dateFormat.format(new Date()) + "] Backup das informações finalizado!" +
						"\n[" + IgnUtil.dateFormat.format(new Date()) + "] Iniciando Backup das fotos dos Clientes....";
				
				atualizarParametro(manager, manager.getTransaction(), parametroLog, backupVerbose);
				manager.getTransaction().commit();
				manager.getTransaction().begin();
				
				if(DsvConstante.getParametrosSistema().get("CAMINHO_IMAGENS_USUARIO") != null && !DsvConstante.getParametrosSistema().get("CAMINHO_IMAGENS_USUARIO").equals(""))
				{
					final File diretorioFotos = new File(DsvConstante.getParametrosSistema().get("CAMINHO_IMAGENS_USUARIO"));

					final Calendar calendar = Calendar.getInstance(); 
					calendar.setTime(new Date());
					calendar.add(Calendar.DAY_OF_YEAR, DsvConstante.getParametrosSistema().getInt("DIAS_BACKUP_FOTOS") * -1);
					
					final File dispositivoFotos = new File(dispositivo.getAbsolutePath() + "Fotos/Cliente/");
					
					for(final File photo : diretorioFotos.listFiles(new FileAgeFilter(calendar.getTime())))
						FileUtils.copyFileToDirectory(photo, dispositivoFotos);	
				}	
				else
					logger.warn("Não foi possível carregar diretório de imagens");	
			}
			
			backupVerbose += "\n[" + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()) + "] Backup finalizado!!";
			
			atualizarParametro(manager, manager.getTransaction(), parametroLog, backupVerbose);
			manager.getTransaction().commit();
			manager.getTransaction().begin();
			
			logger.info("Backup Finalizado!");
		}
		catch(Exception ex)
		{
			logger.error("Não foi possível completar o Backup", ex);
			
			try 
			{
				if(!manager.getTransaction().isActive()) manager.getTransaction().begin();  
				
				atualizarParametro(manager, manager.getTransaction(), parametroLog, "[" + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()) + "] Backup não foi efetuado! Favor entrar em contato com o suporte!");
				manager.getTransaction().commit();
				manager.getTransaction().begin();
				
				super.finalize();
			}
			catch (Throwable e) 
			{
				logger.error(null, ex);
			}
		}
		finally
		{
			try
			{
				if(manager.getTransaction().isActive())
					manager.getTransaction().commit();	
			}
			catch(Exception ex) { }
			
			manager.close();
			
			dispositivo = null;
		}
	}	
}