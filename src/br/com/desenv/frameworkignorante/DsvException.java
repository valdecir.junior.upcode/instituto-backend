package br.com.desenv.frameworkignorante;

import java.util.logging.Logger;

import javax.persistence.PersistenceException;

import org.hibernate.exception.ConstraintViolationException;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import com.mysql.jdbc.exceptions.MySQLSyntaxErrorException;

public class DsvException extends Exception 
{
	private static final long serialVersionUID = -7391166936438558442L;
	
	private Exception cause; 
	
	public DsvException(Exception thrownException)
	{
		super();
		this.cause = thrownException;
	}
	
	public void printOriginalStackTrace()
	{
		if(cause != null)
			cause.printStackTrace();
		else
			Logger.getGlobal().severe("Original stacktrace not found! Exception is null.");
	}
	
	@Override
	public String getMessage()
	{
		try
		{
			Throwable initialExceptionCause = this.cause.getCause();
			Throwable secundaryCause = initialExceptionCause.getCause();
			
			if(initialExceptionCause.getClass().equals(NullPointerException.class))
			{
				return "Erro ao tentar utilizar um objeto de referência nula";
			}
			if(initialExceptionCause.getClass().equals(PersistenceException.class))
			{
				if(secundaryCause != null && secundaryCause.getClass().equals(ConstraintViolationException.class))
				{
					switch(((ConstraintViolationException) secundaryCause).getErrorCode())
					{
						case 1451:
							return "Erro ao excluir registro por que existem ligações com a tabela : " + (((ConstraintViolationException) secundaryCause).getSQLException().getMessage().substring(70).split(",")[0]).split("\\.")[1].replace("`", "").toUpperCase();
						case 1062:
							return "Já existe um registro com essa chave!";
					}
				}
			}
			else if(initialExceptionCause.getClass().equals(MySQLSyntaxErrorException.class))
			{
				switch(((MySQLSyntaxErrorException) initialExceptionCause).getErrorCode())
				{
					case 1054:
						return "Não foi possível encontrar a coluna " + initialExceptionCause.getMessage().split("Unknown column ")[1].split(" in ")[0].replace("'", "");
				}
			}
			else if(initialExceptionCause.getCause().getClass().equals(MySQLIntegrityConstraintViolationException.class))
			{
				if(secundaryCause != null && secundaryCause.getClass().equals(MySQLIntegrityConstraintViolationException.class))
				{
					switch(((MySQLIntegrityConstraintViolationException) secundaryCause).getErrorCode())
					{
						case 1062:
							return "Não foi possível salvar! Já existe um registro com essas informações!.\n\n\nDetalhes : Chave única : [" + ((MySQLIntegrityConstraintViolationException) secundaryCause).getMessage().split("key")[1].replace("'", "").trim() + "]";
						case 1048:
							return "Ocorreu um erro! Campo " + ((MySQLIntegrityConstraintViolationException) secundaryCause).getMessage().split("'")[1] + " não pode ser nulo!";
					}
				}
			}
			else if(initialExceptionCause.getClass().equals(org.hibernate.exception.GenericJDBCException.class))
			{
				if(secundaryCause != null && secundaryCause.getClass().equals(java.sql.SQLException.class))
				{
					switch(((java.sql.SQLException) secundaryCause).getErrorCode())
					{
						case 1205:
							return "Não foi possível gravar as informações no banco de dados.\nTempo de resposta excedido.";
					}
				}
			}
		}
		catch(Exception ex)
		{
			if(this.cause.getMessage() == null)
			{
				if(this.cause.getCause() == null || this.cause.getCause().getMessage() == null)
					return "Erro desconhecido";
				else
					return this.cause.getCause().getMessage();	
			}
			else
				return this.cause.getMessage();
		}
		
		if(this.cause.getMessage() == null)
		{
			if(this.cause.getCause() == null || this.cause.getCause().getMessage() == null)
				return "Erro desconhecido";
			else
				return this.cause.getCause().getMessage();	
		}
		else
			return this.cause.getMessage();
	}
	
	@Override
	public String toString() 
	{
		return this.getMessage();
	}
}
