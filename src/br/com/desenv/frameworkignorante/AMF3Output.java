package br.com.desenv.frameworkignorante;

import flex.messaging.FlexContext;
import flex.messaging.MessageException;
import flex.messaging.io.PagedRowSet;
import flex.messaging.io.PropertyProxy;
import flex.messaging.io.PropertyProxyRegistry;
import flex.messaging.io.SerializationContext;
import flex.messaging.io.StatusInfoProxy;
import flex.messaging.io.amf.ASObject;
import flex.messaging.io.amf.Amf3Output;
import flex.messaging.io.amf.TraitsInfo;

import java.io.Externalizable;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sql.RowSet;

import org.w3c.dom.Document;

import br.com.desenv.nepalign.model.Cliente;
import br.com.desenv.nepalign.model.DocumentoEntradaSaida;
import br.com.desenv.nepalign.model.DuplicataParcela;
import br.com.desenv.nepalign.model.PagamentoDocumentoEntradaSaida;

public class AMF3Output extends Amf3Output 
{
    public AMF3Output(SerializationContext context) 
    {
        super(context);
    }

    /**
     * Serialize AMF3 Date with timezone offset adjust
     * @param d
     * @throws IOException
     */
    protected void writeAMF3Date(Date d) throws IOException  
    {
        if (d != null && FlexContext.getFlexSession().getAttribute("clientTimezoneOffset") != null) 
        {
            Long clientOffset = (Long) FlexContext.getFlexSession().getAttribute("clientTimezoneOffset");
            Long serverOffset = (Long) (-d.getTimezoneOffset() * 60L * 1000);   
            
            if(clientOffset != serverOffset && (clientOffset - serverOffset) < 0)
            {
            	//System.out.println("Before " + d.toString());
            	//System.out.println("O;Server Offset " + serverOffset + ";Client Offset " + clientOffset + "; Diff +" + (serverOffset - clientOffset) + ";DOC " + docEntrada.getNumeroNotaFiscal());
            	d.setTime(d.getTime() + (serverOffset - clientOffset));
            	//System.out.println("After " + d.toString()); 
            } 
        }
        
        super.writeAMFDate(d); 
    }
    
    //
    // java.io.ObjectOutput IMPLEMENTATIONS
    //

    /**
     * Serialize an Object using AMF 3.
     */
    @Override
    public void writeObject(Object o) throws IOException
    { 
        if (o == null)
        {
            writeAMFNull();
            return;
        }
    
        if (!context.legacyExternalizable && o instanceof Externalizable)
        {
            writeCustomObject(o);
        }
        else if (o instanceof String || o instanceof Character)
        {
            String s = o.toString();
            writeAMFString(s);
        }
        else if (o instanceof Number)
        {
            if (o instanceof Integer || o instanceof Short || o instanceof Byte)
            {
                int i = ((Number)o).intValue();
                writeAMFInt(i);
            }
            else if (!context.legacyBigNumbers &&
                    (o instanceof BigInteger || o instanceof BigDecimal))
            {
                // Using double to write big numbers such as BigInteger or
                // BigDecimal can result in information loss so we write
                // them as String by default...
                writeAMFString(((Number)o).toString());
            }
            else
            {
                double d = ((Number)o).doubleValue();
                writeAMFDouble(d);
            }
        }
        else if (o instanceof Boolean)
        {
            writeAMFBoolean(((Boolean)o).booleanValue());
        }
        // We have a complex type...
        else if (o instanceof Date)
        {
            writeAMFDate((Date)o);
        }
        else if (o instanceof Calendar)
        {
            writeAMFDate(((Calendar)o).getTime());
        }
        else if (o instanceof Document) 
        { 
            if (context.legacyXMLDocument) 
                out.write(kXMLType); // Legacy flash.xml.XMLDocument Type 
            else 
                out.write(kAvmPlusXmlType); // New E4X XML Type 
            if (!byReference(o)) 
            { 
                String xml = documentToString(o); 
                if (isDebug) 
                    trace.write(xml); 

                writeAMFUTF(xml); 
            } 
        } 
        else
        {
            // We have an Object or Array type...
            Class cls = o.getClass();

            if (context.legacyMap && o instanceof Map && !(o instanceof ASObject))
            {
                writeMapAsECMAArray((Map)o);
            }
            else if (o instanceof Collection)
            {
                if (context.legacyCollection)
                    writeCollection((Collection)o, null);
                else
                    writeArrayCollection((Collection)o, null);
            }
            else if (cls.isArray())
            {
                writeAMFArray(o, cls.getComponentType());
            }
            else
            {
                //Special Case: wrap RowSet in PageableRowSet for Serialization
                if (o instanceof RowSet)
                {
                    o = new PagedRowSet((RowSet)o, Integer.MAX_VALUE, false);
                }
                else if (context.legacyThrowable && o instanceof Throwable)
                {
                    o = new StatusInfoProxy((Throwable)o); 
                }

                writeCustomObject(o);
            }
        }
    }
    
    /**
     * @exclude
     */
    protected void writePropertyProxy(PropertyProxy proxy, Object instance) throws IOException
    {
        /*
         * At this point we substitute the instance we want to serialize.
         */
        Object newInst = proxy.getInstanceToSerialize(instance);
        if (newInst != instance)
        {
            // We can't use writeAMFNull here I think since we already added this object
            // to the object table on the server side.  The player won't have any way
            // of knowing we have this reference mapped to null.
            if (newInst == null)
                throw new MessageException("PropertyProxy.getInstanceToSerialize class: " + proxy.getClass() + " returned null for instance class: " + instance.getClass().getName());

            // Grab a new proxy if necessary for the new instance
            proxy = PropertyProxyRegistry.getProxyAndRegister(newInst);
            instance = newInst;
        }

        List propertyNames = null;
        boolean externalizable = proxy.isExternalizable(instance);

        if (!externalizable)
            propertyNames = proxy.getPropertyNames(instance);

        TraitsInfo ti = new TraitsInfo(proxy.getAlias(instance), proxy.isDynamic(), externalizable, propertyNames);
        writeObjectTraits(ti);

        if (externalizable)
        {
            // Call user defined serialization
            ((Externalizable)instance).writeExternal(this);
        }
        else if (propertyNames != null)
        {
            Iterator it = propertyNames.iterator();
            while (it.hasNext())
            {
                String propName = (String)it.next();
                Object value = null;
                value = proxy.getValue(instance, propName);
                
                if(value instanceof Date && needDateAdjust(instance))
                	writeAMF3Date((Date)value);
                else
                	writeObjectProperty(propName, value);
            }
        }
 
        writeObjectEnd();
    }

    /**
     * Put all classes that need a date timezone offset adjust
     * @param obj
     * @return
     */
    protected boolean needDateAdjust(Object obj)
    {
    	if(obj instanceof DocumentoEntradaSaida)
    		return true;
    	if(obj instanceof PagamentoDocumentoEntradaSaida)
    		return true;
    	if(obj instanceof Cliente)
    		return true;
    	if(obj instanceof DuplicataParcela )
    		return true;
    	
    	return false;
    }
}