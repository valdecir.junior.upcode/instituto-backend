package br.com.desenv.frameworkignorante.excecoes.relatorios;

public class IntegracaoException extends ReportException 
{
	private static final long serialVersionUID = -4291508548674559599L;

	public IntegracaoException(String description)
	{
		super(new Throwable(description));
	}
}