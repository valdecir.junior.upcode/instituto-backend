package br.com.desenv.frameworkignorante.excecoes.relatorios;

public class ReportException extends Exception 
{
	private static final long serialVersionUID = -5137397542988679421L;

	public ReportException(Throwable throwable)
	{
		super(throwable);
	}
}
