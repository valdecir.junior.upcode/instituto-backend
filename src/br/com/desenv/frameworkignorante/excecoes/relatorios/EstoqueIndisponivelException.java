package br.com.desenv.frameworkignorante.excecoes.relatorios;

public class EstoqueIndisponivelException extends ReportException 
{
	private static final long serialVersionUID = -4291508548674559599L;

	public EstoqueIndisponivelException(String description)
	{
		super(new Throwable(description));
	}
}