package br.com.desenv.frameworkignorante;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.text.MaskFormatter;

import br.com.desenv.nepalign.model.AlertaPedidoVenda;

public class Formatador
{//
	public static String formatarCnpjCpf(String cpfCnpj) throws Exception
	{
		if (cpfCnpj == null)
		{
			return null;
		}
		
		cpfCnpj = cpfCnpj.replaceAll("[^0-9]", "");
		
		if (cpfCnpj.length() == 11)
		{
			return formatarCpf(cpfCnpj);
		}
		else if (cpfCnpj.length() == 14)
		{
			return formatarCnpj(cpfCnpj);
		}
		
		return cpfCnpj;
	}
	
	public static String formatarCpf(String cpf) throws Exception
	{
		if (cpf == null)
		{
			return null;
		}
		
		cpf = cpf.replaceAll("[^0-9]", "");

		NumberFormat nf = NumberFormat.getIntegerInstance();
		nf.setMinimumIntegerDigits(11);
		nf.setGroupingUsed(false);
		cpf = nf.format(Long.parseLong(cpf));

		MaskFormatter mf = new MaskFormatter("###.###.###-##");
		mf.setValueContainsLiteralCharacters(false);
		return mf.valueToString(cpf);
	}

	public static String formatarCnpj(String cnpj) throws Exception
	{
		if (cnpj == null)
		{
			return null;
		}
		
		cnpj = cnpj.replaceAll("[^0-9]", "");

		NumberFormat nf = NumberFormat.getIntegerInstance();
		nf.setMinimumIntegerDigits(14);
		nf.setGroupingUsed(false);
		cnpj = nf.format(Long.parseLong(cnpj));

		// 00.000.000/0001-91
		MaskFormatter mf = new MaskFormatter("##.###.###/####-##");
		mf.setValueContainsLiteralCharacters(false);
		return mf.valueToString(cnpj);
	}
	
	public static String formatarCep(String cep) throws Exception
	{
		if (cep == null)
		{
			return null;
		}
		
		cep = cep.replaceAll("[^0-9]", "");

		NumberFormat nf = NumberFormat.getIntegerInstance();
		nf.setMinimumIntegerDigits(8);
		nf.setGroupingUsed(false);
		cep = nf.format(Long.parseLong(cep));

		// 00.000.000/0001-91
		MaskFormatter mf = new MaskFormatter("#####-###");
		mf.setValueContainsLiteralCharacters(false);
		return mf.valueToString(cep);
	}

	public static String formatarFone(String telefone) throws Exception
	{
		if (telefone == null)
		{
			return null;
		}
		
		telefone = telefone.replaceAll("[^0-9]", "");

		// 00.000.000/0001-91

		// Formato 9999-9999
		if (telefone.length() <= 8)
		{
			NumberFormat nf = NumberFormat.getIntegerInstance();
			nf.setMinimumIntegerDigits(8);
			nf.setGroupingUsed(false);
			telefone = nf.format(Long.parseLong(telefone));
			telefone = telefone.substring(0, 4) + "-" + telefone.substring(4, 8);
		}
		// Formato Sao Paulo 99999-9999
		else if (telefone.length() == 9)
		{
			telefone = telefone.substring(0, 5) + "-" + telefone.substring(5, 9);
		}
		// Formato (41) 9999-9999 (ddd com dois digitos)
		else if (telefone.length() == 10)
		{
			telefone = "(" + telefone.substring(0, 2) + ") " + telefone.substring(2, 6) + "-" + telefone.substring(6, 10);
		}
		// Formato (041) 9999-9999 (ddd com dois digitos e zero na frente)
		else if (telefone.length() == 11 && telefone.charAt(0) == '0')
		{
			telefone = "(" + telefone.substring(1, 3) + ") " + telefone.substring(3, 7) + "-" + telefone.substring(7, 11);
		}
		// Formato Sao Paulo (41) 99999-9999 (ddd com dois digitos)
		else if (telefone.length() == 11 && telefone.charAt(0) != '0')
		{
			telefone = "(" + telefone.substring(0, 2) + ") " + telefone.substring(2, 7) + "-" + telefone.substring(7, 11);
		}
		// Formato Sao Paulo (041) 99999-9999 (ddd com dois digitos e zero na
		// frente)
		else if (telefone.length() == 12 && telefone.charAt(0) == '0')
		{
			telefone = "(" + telefone.substring(1, 3) + ") " + telefone.substring(3, 8) + "-" + telefone.substring(8, 12);
		}

		return telefone;

	}
	
	public static BufferedImage getScaledImage(BufferedImage image, int width, int height) throws IOException
	{
	    int imageWidth  = image.getWidth();
	    int imageHeight = image.getHeight();

	    double scaleX = (double)width/imageWidth;
	    double scaleY = (double)height/imageHeight;
	    AffineTransform scaleTransform = AffineTransform.getScaleInstance(scaleX, scaleY);
	    AffineTransformOp bilinearScaleOp = new AffineTransformOp(scaleTransform, AffineTransformOp.TYPE_BILINEAR);

	    return bilinearScaleOp.filter(image,new BufferedImage(width, height, image.getType()));
	}
	
	/* Arredonda um valor com o numero de decimais especificado */
	public static double round(double value, int decimais) 
	{
		double p = Math.pow(10, decimais);
		return Math.round(value * p) / p;
	}
	
	public static <T> Page<T> gerarPagina(List<T> lista, int pageNumber, int pageSize) throws Exception
	{
		List<T> resultList = new ArrayList<T>();
		
		int numberPageLoop = pageNumber - 1;
		
		if((numberPageLoop * pageSize) < lista.size())
		{
			for(int i = (numberPageLoop * pageSize); i < lista.size(); i++)
			{
				if(resultList.size() < pageSize)
					resultList.add(lista.get(i));
			}
		}
		else
			resultList = lista;
		
		Page<T> page = new Page<T>();
		page.setPageNumber(pageNumber);
	    page.setPageSize(pageSize);
	    page.setRecordList(resultList);
	    page.setTotalRecords(lista.size());
		return page;
	}
}
