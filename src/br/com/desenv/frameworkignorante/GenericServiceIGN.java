package br.com.desenv.frameworkignorante;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class GenericServiceIGN<T extends GenericModelIGN, P extends GenericPersistenceIGN<T>>
{

	private GenericPersistenceIGN<T> persistence;

	public GenericServiceIGN()
	{
		try
		{
			this.persistence = (GenericPersistenceIGN<T>) getP().newInstance();
		}
		catch (InstantiationException e)
		{
			e.printStackTrace();
		}
		catch (IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}

	public GenericPersistenceIGN<T> getPersistence()
	{
		return persistence;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	private Class<T> getT()
	{
		return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@SuppressWarnings("unchecked")
	private Class<P> getP()
	{
		return (Class<P>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
	}

	public T salvar(T entidade) throws Exception
	{
		entidade.validate();
		return this.persistence.salvar(entidade);
	}
	
	public T salvar(EntityManager manager, EntityTransaction transaction, T entidade) throws Exception
	{
		try
		{
			entidade.validate();
			return this.persistence.salvar(manager, transaction, entidade);	
		}
		catch(Exception ex)
		{
			throw new DsvException(ex);
		}
	}

	
	public T salvar(T entidade, boolean logar) throws Exception
	{
		entidade.validate();
		return this.persistence.salvar(entidade, logar);
	}


	
	public T salvar(EntityManager manager, EntityTransaction transaction, T entidade, boolean logar) throws Exception
	{
		try
		{
			entidade.validate();
			return this.persistence.salvar(manager, transaction, entidade, logar);	
		}
		catch(Exception ex)
		{
			throw new DsvException(ex);
		}
	}
	
	

	public T atualizar(T entidade) throws Exception
	{
		entidade.validate();
		return this.persistence.atualizar(entidade);
	}

	public T atualizar(EntityManager manager, EntityTransaction transaction, T entidade) throws Exception
	{
		entidade.validate();
		return this.persistence.atualizar(manager, transaction, entidade);
	}
	
	public T atualizar(T entidade, boolean logar) throws Exception
	{
		entidade.validate();
		return this.persistence.atualizar(entidade, logar);
	}

	public T atualizar(EntityManager manager, EntityTransaction transaction, T entidade, boolean logar) throws Exception
	{
		entidade.validate();
		return this.persistence.atualizar(manager, transaction, entidade, logar);
	}

	public void excluir(T entidade) throws Exception
	{
		this.persistence.excluir(entidade);
	}

	public void excluir(EntityManager manager, EntityTransaction transaction, T entidade) throws Exception
	{
		this.persistence.excluir(manager, transaction, entidade);
	}

	public void excluir(T entidade, boolean logar) throws Exception
	{
		this.persistence.excluir(entidade, logar);
	}

	public void excluir(EntityManager manager, EntityTransaction transaction, T entidade, boolean logar) throws Exception
	{
		this.persistence.excluir(manager, transaction, entidade, logar);
	}
	
	

	public T recuperarPorId(Integer id) throws Exception
	{
		return this.persistence.recuperarPorId(id);
	}

	public T recuperarPorId(EntityManager manager, Integer id) throws Exception
	{
		return this.persistence.recuperarPorId(manager, id);
	}

	public List<T> listar() throws Exception
	{
		return persistence.listar();
	}

	public List<T> listar(String ordemPesquisa, String tipoOrdenacao) throws Exception
	{
		return persistence.listar(ordemPesquisa, tipoOrdenacao);
	}
	
	public List<T> listar(String ordemPesquisa, String tipoOrdenacao, Boolean campoNumerico) throws Exception
	{
		return persistence.listar(ordemPesquisa, tipoOrdenacao, campoNumerico);
	}


	public List<T> listar(String ordemPesquisa, String tipoOrdenacao, Integer numeroMaximoRegistro) throws Exception
	{
		return persistence.listar(ordemPesquisa, tipoOrdenacao, numeroMaximoRegistro);
	}

	public List<T> listar(EntityManager manager, String ordemPesquisa, String tipoOrdenacao, Integer numeroMaximoRegistro) throws Exception
	{
		return persistence.listar(manager, ordemPesquisa, tipoOrdenacao, numeroMaximoRegistro);
	}

	public Page<T> listarPaginadoGenerico(int tamanhoPagina, int numeroPagina, List<SearchParameter> listaParametroPesquisa) throws Exception
	{
		return persistence.listarPaginadoGenerico(tamanhoPagina, numeroPagina , listaParametroPesquisa);
	}

	public Page<T> listarPaginadoGenerico(int tamanhoPagina, int numeroPagina, List<SearchParameter> listaParametroPesquisa, String ordemPesquisa, String tipoOrdenacao) throws Exception
	{
		return persistence.listarPaginadoGenerico(tamanhoPagina, numeroPagina, listaParametroPesquisa, ordemPesquisa, tipoOrdenacao);
	}

	public Page<T> listarPaginadoGenerico(EntityManager manager, int tamanhoPagina, int numeroPagina, List<SearchParameter> listaParametroPesquisa, String ordemPesquisa, String tipoOrdenacao) throws Exception
	{
		return persistence.listarPaginadoGenerico(manager, tamanhoPagina, numeroPagina, listaParametroPesquisa, ordemPesquisa, tipoOrdenacao);
	}

	public Page<T> listarPaginadoPorObjetoFiltro(int tamanhoPagina, int numeroPagina, T entidadeFiltro) throws Exception
	{
		return persistence.listarPaginadoPorObjetoFiltro(tamanhoPagina, numeroPagina, entidadeFiltro);
	}

	public Page<T> listarPaginadoPorObjetoFiltro(int tamanhoPagina, int numeroPagina, T entidadeFiltro, String ordemPesquisa, String tipoOrdenacao) throws Exception
	{
		return persistence.listarPaginadoPorObjetoFiltro(tamanhoPagina, numeroPagina, entidadeFiltro, ordemPesquisa, tipoOrdenacao);
	}

	public Page<T> listarPaginadoPorObjetoFiltro(int tamanhoPagina, int numeroPagina, T entidadeFiltro, String ordemPesquisa, String tipoOrdenacao, Boolean campoNumerico) throws Exception
	{
		return persistence.listarPaginadoPorObjetoFiltro(tamanhoPagina, numeroPagina, entidadeFiltro, ordemPesquisa, tipoOrdenacao, campoNumerico);
	}
	
	public Page<T> listarPaginadoPorObjetoFiltro(int tamanhoPagina, int numeroPagina, T entidadeFiltro, String ordemPesquisa, String tipoOrdenacao, String whereAdicional) throws Exception
	{
		return persistence.listarPaginadoPorObjetoFiltro(tamanhoPagina, numeroPagina, entidadeFiltro, ordemPesquisa, tipoOrdenacao, whereAdicional);
	}
	
	public Page<T> listarPaginadoPorObjetoFiltro(EntityManager manager, int tamanhoPagina, int numeroPagina, T entidadeFiltro, String ordemPesquisa, String tipoOrdenacao) throws Exception
	{
		return persistence.listarPaginadoPorObjetoFiltro(manager, tamanhoPagina, numeroPagina, entidadeFiltro, ordemPesquisa, tipoOrdenacao);
	}

	public List<T> listarGenerico(List<SearchParameter> listaParametroPesquisa)
	{
		return persistence.listarGenerico(listaParametroPesquisa);
	}

	public List<T> listarGenerico(List<SearchParameter> listaParametroPesquisa, String ordemPesquisa, String tipoOrdenacao)
	{
		return persistence.listarGenerico(listaParametroPesquisa, ordemPesquisa, tipoOrdenacao);
	}

	public List<T> listarGenerico(List<SearchParameter> listaParametroPesquisa, String ordemPesquisa, String tipoOrdenacao, Integer numeroMaximoRegistro)
	{
		return persistence.listarGenerico(listaParametroPesquisa, ordemPesquisa, tipoOrdenacao, numeroMaximoRegistro);
	}

	public List<T> listarGenerico(EntityManager manager, List<SearchParameter> listaParametroPesquisa, String ordemPesquisa, String tipoOrdenacao, Integer numeroMaximoRegistro)
	{
		return persistence.listarGenerico(manager, listaParametroPesquisa, ordemPesquisa, tipoOrdenacao, numeroMaximoRegistro);
	}

	public List<T> listarPorObjetoFiltro(T entidadeFiltro) throws Exception
	{
		try
		{
			return persistence.listarPorObjetoFiltro(entidadeFiltro);
		}
		catch(Exception ex)
		{
			throw new DsvException(ex);
		}
	}

	public List<T> listarPorObjetoFiltro(T entidadeFiltro, String ordemPesquisa, String tipoOrdenacao) throws Exception
	{
		return persistence.listarPorObjetoFiltro(entidadeFiltro, ordemPesquisa, tipoOrdenacao);
	}

	public List<T> listarPorObjetoFiltro(T entidadeFiltro, String ordemPesquisa, String tipoOrdenacao, Integer numeroMaximoRegistro) throws Exception
	{
		return persistence.listarPorObjetoFiltro(entidadeFiltro, ordemPesquisa, tipoOrdenacao, numeroMaximoRegistro);
	}

	public List<T> listarPorObjetoFiltro(EntityManager manager, T entidadeFiltro, String ordemPesquisa, String tipoOrdenacao, Integer numeroMaximoRegistro) throws Exception
	{
		try
		{
			return persistence.listarPorObjetoFiltro(manager, entidadeFiltro, ordemPesquisa, tipoOrdenacao, numeroMaximoRegistro);	
		}
		catch(Exception ex)
		{
			throw new DsvException(ex);
		}
	}

	public Page<T> listarPaginadoSqlLivreGenerico(EntityManager manager, int tamanhoPagina, int numeroPagina, String where, String ordemPesquisa, String tipoOrdenacao) throws Exception
	{
		return persistence.listarPaginadoSqlLivreGenerico(manager,  tamanhoPagina,  numeroPagina,  where,  ordemPesquisa, tipoOrdenacao);
	}	
	public Page<T> listarPaginadoSqlLivreGenerico(int tamanhoPagina, int numeroPagina, String where) throws Exception
	{
		return persistence.listarPaginadoSqlLivreGenerico(tamanhoPagina, numeroPagina,where);
	}

	public Page<T> listarPaginadoSqlLivreGenerico(int tamanhoPagina, int numeroPagina, String where, String ordemPesquisa, String tipoOrdenacao) throws Exception
	{
		return persistence.listarPaginadoSqlLivreGenerico(tamanhoPagina, numeroPagina, where, ordemPesquisa, tipoOrdenacao);
	}

}
