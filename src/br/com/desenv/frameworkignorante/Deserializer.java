package br.com.desenv.frameworkignorante;

import flex.messaging.io.SerializationContext;
import flex.messaging.io.amf.AmfMessageDeserializer;
import flex.messaging.io.amf.AmfTrace;

import java.io.InputStream;

public class Deserializer extends AmfMessageDeserializer 
{
    @Override
    public void initialize(SerializationContext context, InputStream in, AmfTrace trace) 
    {
        amfIn = new AMF0Input(context);
        amfIn.setInputStream(in);

        debugTrace = trace;
        isDebug = debugTrace != null;
        amfIn.setDebugTrace(debugTrace);
    }
}