package br.com.desenv.frameworkignorante;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.logging.Logger;

import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IncrementGenerator;

import br.com.desenv.nepalign.model.EmpresaEntidadeChave;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.ItemMovimentacaoEstoque;
import br.com.desenv.nepalign.model.MovimentacaoEstoque;

public class MyGenerator extends IncrementGenerator
{
    @Override
    public Serializable generate(SessionImplementor session, Object obj)
    {    	
    	Object objHasEmpresa = null;
    	
    	if(obj == null)
    	{
    		Logger.getGlobal().severe("Couldn't generate key to null obj");
    		return 0;
    	}
    	else if(obj instanceof MovimentacaoEstoque)
    	{
    		MovimentacaoEstoque movimentacaoEstoque = (MovimentacaoEstoque) obj;
    		
    		if (movimentacaoEstoque.getId() != null)
    		    return movimentacaoEstoque.getId();
    		
    		objHasEmpresa = obj;
    	}
    	else if(obj instanceof ItemMovimentacaoEstoque)
    	{
    		ItemMovimentacaoEstoque itemMovimentacaoEstoque = (ItemMovimentacaoEstoque) obj;
    		
    		if(itemMovimentacaoEstoque.getId() != null)
    			return itemMovimentacaoEstoque.getId();
    		
        	try
        	{
        		Field movimentacaoEstoqueField = obj.getClass().getDeclaredField("movimentacaoEstoque");
        		
        		if(movimentacaoEstoqueField != null)
        		{
        			movimentacaoEstoqueField.setAccessible(true);
        			
        			MovimentacaoEstoque movimentacaoEstoque = (MovimentacaoEstoque) movimentacaoEstoqueField.get(obj);
        			
        			if(movimentacaoEstoque != null)
        				objHasEmpresa = movimentacaoEstoque;
        			
        			movimentacaoEstoqueField.setAccessible(false);
        		}
        	}
        	catch(Exception ex)
        	{
        		
        	}
    	}
    	
    	Integer idEmpresaFisica = 0;
    	
    	try
    	{
    		Field empresaFisicaField = objHasEmpresa.getClass().getDeclaredField("empresaFisica");
    		
    		if(empresaFisicaField != null)
    		{
    			empresaFisicaField.setAccessible(true);
    			
    			EmpresaFisica empresaFisica = (EmpresaFisica) empresaFisicaField.get(objHasEmpresa);
    			
    			if(empresaFisica != null)
    				idEmpresaFisica = empresaFisica.getId();
    			
    			empresaFisicaField.setAccessible(false);
    		}
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    	}
	 
		try
		{
		    EmpresaEntidadeChave empresaEntidadeChave = ListaEmpresaEntidadeChave.getHashChaveEntidadeAtual(idEmpresaFisica).get(idEmpresaFisica.toString() + "_" + obj.getClass().getName().toUpperCase());
	 
		    Integer chaveAtual = empresaEntidadeChave.getChaveAtual();
		    chaveAtual++;
		    empresaEntidadeChave.setChaveAtual(chaveAtual);
	
		    return chaveAtual;
		} 
		catch (Exception e)
		{
			Logger.getGlobal().severe("Exception on generate key to " + obj.getClass().getName().toUpperCase());
		    e.printStackTrace();
		}
		return 0;
    }
}