package br.com.desenv.frameworkignorante;

import java.math.BigDecimal;
import java.math.BigInteger;

import flex.messaging.io.amf.translator.ASTranslator;
import flex.messaging.io.amf.translator.decoder.NumberDecoder;

public class NaNHandlingTypeMarshaller extends ASTranslator 
{
    private final NaNHandlingNumberDecoder numberDecoder = new NaNHandlingNumberDecoder();

    @Override
    public Object convert(Object source, @SuppressWarnings("rawtypes") Class desiredClass) 
    {
        if (Number.class.isAssignableFrom(desiredClass)) 
        {
            return numberDecoder.decodeObject(source, desiredClass);
        }
        else 
        {
            return super.convert(source, desiredClass);
        }
    }
 
    class NaNHandlingNumberDecoder extends NumberDecoder
    {
        @Override
        public Object decodeObject(Object shell, Object encodedObject, @SuppressWarnings("rawtypes") Class desiredClass) 
        {
            if (encodedObject instanceof Double && Double.isNaN((Double)encodedObject) && needsNaNConversion(desiredClass)) 
                return null;
            else 
                return super.decodeObject(shell,encodedObject, desiredClass);
        }
 
        public boolean needsNaNConversion(@SuppressWarnings("rawtypes") Class desiredClass) 
        {
            return Integer.class.equals(desiredClass)
                || Long.class.equals(desiredClass)
                || BigDecimal.class.equals(desiredClass)
                || BigInteger.class.equals(desiredClass);
        }
    }
}