package br.com.desenv.frameworkignorante;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.logging.Logger;

import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IncrementGenerator;

import br.com.desenv.nepalign.model.EmpresaEntidadeChave;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.ItemMovimentacaoEstoque;
import br.com.desenv.nepalign.model.MovimentacaoEstoque;

public class IgnGenerator extends IncrementGenerator
{
    @Override
    public Serializable generate(SessionImplementor session, Object obj)
    {    	
    	if(((GenericModelIGN) obj).getId() != null && ((GenericModelIGN) obj).getId() > 0x00)
    		return ((GenericModelIGN) obj).getId();
    	else
    		return super.generate(session, obj);
    }
}