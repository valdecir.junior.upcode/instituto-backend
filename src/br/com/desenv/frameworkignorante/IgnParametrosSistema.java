package br.com.desenv.frameworkignorante;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import br.com.desenv.nepalign.model.ParametroSistema;
import br.com.desenv.nepalign.service.ParametroSistemaService;

public class IgnParametrosSistema
{
	private Properties parametros;
	
	public Properties getParametros()
	{
		return this.parametros;
	}
	
	public String get(String chave)
	{
		try
		{
			return parametros.getProperty(chave.toUpperCase());
		}
		catch(Exception ex)
		{
			return null;
		}
	}
	public String get(int idEmpresa, String chave)
	{
		try
		{
			return parametros.getProperty(idEmpresa+"_"+chave.toUpperCase());
		}
		catch(Exception ex)
		{
			return null;
		}
	}
	
	public double getDouble(String chave)
	{
		return new Double(get(chave)).doubleValue();
	}
	
	public double getDouble(int idEmpresa, String chave)
	{
		return new Double(get(idEmpresa, chave)).doubleValue();
	}
	
	public int getInt(String chave)
	{
		return new Integer(get(chave)).intValue();
	}
	
	public int geDouble(int idEmpresa, String chave)
	{
		return new Integer(get(idEmpresa, chave)).intValue();
	}	
	
	public void carregarParametros() throws Exception
	{
		try
		{
			ParametroSistemaService psService = new ParametroSistemaService();
			List<ParametroSistema> lista = psService.listar();
			parametros = new Properties();
			
			for (ParametroSistema parametroSistema : lista) 
			{
				if (parametroSistema.getEmpresaFisica()!=null && parametroSistema.getEmpresaFisica().getId()!=null && parametroSistema.getEmpresaFisica().getId().intValue()>0)
					parametros.put(parametroSistema.getEmpresaFisica().getId().intValue()+"_"+parametroSistema.getChave().toUpperCase(), parametroSistema.getValor());
				else
					parametros.put(parametroSistema.getChave().toUpperCase(), parametroSistema.getValor());
			}
			
			if (parametros.containsKey("IGN_ARQUIVO_PROPRIEDADES_LOCAIS")) 
			{
				String arqPropLocais = parametros.getProperty("IGN_ARQUIVO_PROPRIEDADES_LOCAIS");
				
				try
				{
					parametros.load(new FileInputStream(new File(arqPropLocais)));
				}
				catch(FileNotFoundException fnfe)
				{
					Logger.getRootLogger().info("Arquivo com propriedades locais não foi encontrado.");
				}
				catch(Exception fnfe)
				{
					Logger.getRootLogger().error("Ocorreu um erro e arquivo com propriedades locais não foi carregado.", fnfe);
				}				
			}
			else
			{
				Logger.getRootLogger().info("Parametro com arquivo de propriedades locais NAO FOI ENCONTRADO.");
			}
		}
		catch(Exception ex)
		{
			Logger.getRootLogger().error("Ocorreu um erro geral na carga dos parametros do sistema.", ex);
			throw ex;
		}
	}
}
