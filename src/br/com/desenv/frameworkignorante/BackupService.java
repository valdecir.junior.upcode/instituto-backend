package br.com.desenv.frameworkignorante;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.filechooser.FileSystemView;

import br.com.desenv.nepalign.model.ParametrosSistema;
import br.com.desenv.nepalign.service.ParametrosSistemaService;
import br.com.desenv.nepalign.util.DsvConstante;

public class BackupService 
{
	public final List<String> recuperarDispositivosBackup()
	{
		List<String> returnDevices = new ArrayList<String>();
		
		FileSystemView fsv = FileSystemView.getFileSystemView();
		File[] paths = File.listRoots();

		for(File path:paths)
		{
			if(fsv.getSystemDisplayName(path).isEmpty())
				continue;
			returnDevices.add(fsv.getSystemDisplayName(path));
		}
		return returnDevices;
	}
	
	public final File recuperarDispositivoBackup(String backupName)
	{
		FileSystemView fsv = FileSystemView.getFileSystemView();
		File[] paths = File.listRoots();

		for(File path:paths)
		{
			if(fsv.getSystemDisplayName(path).isEmpty())
				continue;
			if(backupName.equals(fsv.getSystemDisplayName(path)))
				return path;
		}
		return null; 
	}
	
	private void verificarDispositivoBackup(File dispositivo) throws Exception
	{
		File bdisk = new File(dispositivo.getPath().concat("\\bdisk"));
		
		if(!bdisk.exists())
			throw new Exception("Não é possível fazer backup nesse dispositivo!");
		
		BufferedReader reader = new BufferedReader(new FileReader(bdisk));
		
		String buffer = reader.readLine();
		
		reader.close();
		
		if(!buffer.equals(DsvConstante.getParametrosSistema().get("SENHA_PENDRIVE_BACKUP")))
		{
			Logger.getGlobal().warning("Tentativa de Backup em dispositivo sem senha de liberação!");
			
			throw new Exception("Não é possível fazer backup nesse dispositivo! =S=");
		}
	}
	
	public final String logBackup() throws Exception
	{
		ParametrosSistema parametrosSistemaFiltro = new ParametrosSistema();
		parametrosSistemaFiltro.setDescricao("LOG_ULTIMO_BACKUP_".concat(String.format("%02d", DsvConstante.getParametrosSistema().getInt("EMPRESA_LOCAL"))));
		
		ParametrosSistemaService parametrosSistemaService = new ParametrosSistemaService();
		
		List<ParametrosSistema> listaParametros = parametrosSistemaService.listarPorObjetoFiltro(parametrosSistemaFiltro);
		
		if(listaParametros.size() < 0x01)
			return "";
		else
			return listaParametros.get(0x00).getValor();
	}
	
	public final Boolean backupRunning() throws Exception
	{
		return br.com.desenv.nepalign.integracao.Util.isBackupRunning(); 
	}
	
	public void iniciarBackup(String _dispositivo) throws Exception
	{
		File dispositivo = recuperarDispositivoBackup(_dispositivo);
		
		verificarDispositivoBackup(dispositivo);
		
		BackupRunner runner = new BackupRunner(dispositivo);
		
		Thread thread = new Thread(runner);
		thread.setName("BackupRunner");
		thread.setPriority(Thread.MAX_PRIORITY);
		thread.start();
	}
}
