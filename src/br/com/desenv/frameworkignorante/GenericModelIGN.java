package br.com.desenv.frameworkignorante;

import java.io.Serializable;



public abstract class GenericModelIGN implements Serializable, Comparable<GenericModelIGN>
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7762296489264486772L;

	/**
	 * 
	 */
	

	//TODO: validateIGN(type="id")
	public abstract Integer getId();

	public abstract void setId(Integer id);
	
	public abstract void validate() throws Exception;
	
	@Override
	public boolean equals(Object object)  
	{  
		GenericModelIGN obj = (GenericModelIGN) object;
		
		if (obj!=null && obj.getId()!=null && this.getId()!=null)
		{
			if (obj.getId().intValue() == this.getId().intValue()) return true;
		}
		return false;  
	}  	
	@Override
	public int compareTo(GenericModelIGN object)  
	{  
		
		if (object!=null && object.getId()!=null && this.getId()!=null)
		{
			if (object.getId().intValue() == this.getId().intValue()) return 0;
			if (object.getId().intValue() > this.getId().intValue()) return 1;
		}
		return -1;  
	}	
	
	
}
