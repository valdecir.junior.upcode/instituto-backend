package br.com.desenv.frameworkignorante;

import java.sql.Connection;
import java.sql.Statement;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import com.mchange.v2.log.MLevel;

import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

import flex.messaging.MessageBrokerServlet;
import flex.messaging.log.Log;

public class IgnMessageBrokerServlet extends MessageBrokerServlet 
{
	private static final Logger logger = Logger.getLogger("NEPAL.SECURITY");
	
	static final long serialVersionUID = 1L;
	@Override
	public void init(ServletConfig servletConfig) throws ServletException
	{ 
		try
		{
			Logger.getRootLogger().info("IGN MESSAGEBROKER SERVLET Initializing... ");
			
			System.setProperty("com.mchange.v2.log.MLog", "com.mchange.v2.log.FallbackMLog");
			System.setProperty("com.mchange.v2.log.FallbackMLog.DEFAULT_CUTOFF_LEVEL", "WARNING");
			
			Logger.getRootLogger().info("ENVIRONMENT VARIABLE LOJA: " + System.getenv("LOJA"));
			Logger.getRootLogger().info("ENVIRONMENT VARIABLE NOME_GRUPO: " + System.getenv("NOME_GRUPO"));
			
		    ListaEmpresaEntidadeChave.getInstance();
			IgnSegurancaService.atualizaListaUsuarios();
            efetuaOutrasInicializacoes(); 
		}
		catch (Exception ex)
		{
			logger.warn("Erro ao carregar credenciais de acesso.", ex);
		}
		finally
		{
			super.init(servletConfig);
			
            new Thread(new Runnable() 
            {
				@Override
				public void run() 
				{
					try 
					{
						IgnSegurancaService.verifySignedMethods();
					}
					catch (Exception e) 
					{
						logger.warn("Não foi possível verificar os métodos assinados para acesso remoto!", e);
					}
				}
			}, "verifySignedMethods").run();
		}
	}
	
	
	private void efetuaOutrasInicializacoes() throws Exception
	{
		try
		{
			Connection con = ConexaoUtil.getConexaoPadrao();
			Statement stm = con.createStatement();
			stm.execute("set global log_bin_trust_function_creators = 1");
			Log.getLogger("INICIALIZACAO_NEPAL").info("Fun??o para uso de relat?rios inicializada.");
			stm.close();
			con.close();
		}
		catch(Exception ex)
		{
			Log.getLogger("INICIALIZACAO_NEPAL").info("Erro na inicializa??o de fun??o de relat?rios..");
		}
	}
	
}