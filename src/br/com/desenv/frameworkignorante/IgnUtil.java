
package br.com.desenv.frameworkignorante;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class IgnUtil 
{ 
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	public static final SimpleDateFormat dateFormatSql = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat dateFormatAnoMes = new SimpleDateFormat("yyMM");

	/**
	 * Check if the Field is a ForeignKey Field
	 * @param obj Instance of class with a field value
	 * @return true if field is a foreign key false if not
	 * @throws Exception
	 */
	public Boolean isForeignKeyField(Field field, Object obj) throws Exception
	{
		try
		{
			field.get(obj).getClass().getDeclaredField("id");
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}

	/**
	 * Deserializa o objeto transformando em uma String
	 * @param obj
	 * @return String
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	public String deserializarObjeto(Object obj) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException
	{
		try
		{
			StringBuilder xmlString = new StringBuilder();

			xmlString.append("<classname>" + obj.getClass().getName() + "</classname>\n");

			for (Field field : obj.getClass().getDeclaredFields())
			{
				if(!java.lang.reflect.Modifier.isStatic(field.getModifiers()))
				{
					xmlString.append("<field> " + field.getName() + " </field>\n");

					try
					{
						if(obj.getClass().getMethod("get" +  Character.toUpperCase(field.getName().charAt(0)) + field.getName().substring(1), (Class<?>) null).invoke(obj, (Object[]) null).getClass() != null 
								&& obj.getClass().getMethod("get" +  Character.toUpperCase(field.getName().charAt(0)) + field.getName().substring(1), (Class<?>) null).invoke(obj, (Object[]) null).getClass().getPackage() != null 
								&& obj.getClass().getMethod("get" +  Character.toUpperCase(field.getName().charAt(0)) + field.getName().substring(1), (Class<?>) null).invoke(obj, (Object[]) null).getClass().getPackage().getName().contains("br.com.desenv.nepalign.model"))
						{
							Object bufferObj = obj.getClass().getMethod("get" +  Character.toUpperCase(field.getName().charAt(0)) + field.getName().substring(1), (Class<?>) null).invoke(obj, (Object[]) null);

							xmlString.append("<fieldValue> " + bufferObj.getClass().getMethod("getId", (Class<?>) null).invoke(bufferObj, (Object[]) null).toString() + " </fieldValue>\n");
						}
						else
							xmlString.append("<fieldValue> " + obj.getClass().getMethod("get" +  Character.toUpperCase(field.getName().charAt(0)) + field.getName().substring(1), (Class<?>) null).invoke(obj, (Object[]) null) + " </fieldValue>\n");
					}
					catch(NullPointerException nullPointerE)
					{
						xmlString.append("<fieldValue> NULL </fieldValue>\n");
					}
					catch(NoSuchMethodException nsme)
					{
						xmlString.append("<fieldValue> NULL </fieldValue>");
					}
				}
			}

			return xmlString.toString();
		}
		catch(IllegalAccessException ie)
		{
			throw ie;
		}
		catch(InvocationTargetException ive)
		{
			throw ive;
		}
		catch(Exception e)
		{
			throw e;
		}
	}

	/**
	 * Serializa o objeto de uma String para um Object
	 * @param data
	 * @return Object
	 * @throws ClassNotFoundException
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws InstantiationException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object serializarObjeto(String data) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException
	{
		String[] metaDataList = data.split(";");

		Class classe =  Class.forName(metaDataList[0].split("=")[1]);

		Constructor objectConstructor = null;
		objectConstructor = classe.getConstructor();
		Object object = objectConstructor.newInstance();

		for (int i = 1; i < metaDataList.length; i++)
		{
			String strData = metaDataList[i];
			Field field;
			try
			{
				field = classe.getDeclaredField(strData.split("=")[0]);
			}
			catch(NoSuchFieldException ns)
			{
				i++;
				continue;
			}

			field.setAccessible(true);

			if(field.getType().equals(Integer.class))
			{
				field.set(object, Integer.parseInt(strData.split("=")[1]));
			}
			else if (field.getType().equals(String.class))
			{
				field.set(object, strData.split("=")[1]);
			}
			else if (field.getType().equals(Boolean.class))
			{
				field.set(object, Boolean.parseBoolean(strData.split("=")[1]));
			}
			else
			{
				Class foreingKeyClass = Class.forName(field.getType().getName());
				Constructor classConstructor = foreingKeyClass.getConstructor();
				Object foreingKeyObject = classConstructor.newInstance();

				Field foreingKeyIdField = foreingKeyClass.getDeclaredField("id");
				foreingKeyIdField.setAccessible(true);
				foreingKeyIdField.set(foreingKeyObject, Integer.parseInt(strData.split("=")[1]));

				field.set(object, foreingKeyObject);
			}
		}

		return object;
	}

	/**
	 * Verifica se todos os campos do objetos são nulos
	 * @param obj
	 * @return Boolean
	 * @throws Exception
	 */
	public Boolean isParameterEmpty(Object obj) throws Exception
	{
		Boolean returnValue = true;
		try
		{
			for (Field field : obj.getClass().getDeclaredFields())
			{
				field.setAccessible(true);

				if(field.getModifiers() != 26 && field.get(obj) != null)
				{
					returnValue = false;
					break;
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		return returnValue;
	}

	/**
	 * Verifica se os campos de valores primitivos são iguais
	 * @param obj
	 * @param objEqual
	 * @return
	 * @throws Exception
	 */
	public Boolean primitiveFieldEquals(Object obj, Object objEqual) throws Exception
	{
		Boolean returnValue = true;
		try
		{
			for (Field field : obj.getClass().getDeclaredFields())
			{
				field.setAccessible(true);

				if(field.getModifiers() != 26 && field.get(obj) != null)
				{
					if(field.get(objEqual) == null)
					{
						returnValue = false;
						break;	
					}

					Object value1 = field.get(obj);
					Object value2 = field.get(objEqual);

					if(!value1.equals(value2))
					{
						returnValue = false;
						break;	
					}
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
		return returnValue;
	}

	/**
	 * Retorna uma string que são todos os ids dos objetos contidos na lista separado por ,
	 * @param list
	 * @return String
	 */
	@SuppressWarnings("rawtypes")
	public String getIdList(List list)
	{
		StringBuilder idList = new StringBuilder();

		for(int i = 0; i < list.size(); i++)
		{
			Object obj = list.get(i);

			if(obj != null)
			{
				try
				{
					if(idList.length() > 0)
						idList.append(",");

					idList.append(obj.getClass().getMethod("getId").invoke(obj));
				}
				catch(Exception ex)
				{
					idList.substring(0, idList.length() - 1);
				}
			}
		}
		return String.valueOf(idList);
	}

	/**
	 * Verifica se na lista se algum objeto contem o id que é passado como parametro.
	 * @param id
	 * @param list
	 * @return Boolean
	 */
	@SuppressWarnings("rawtypes")
	public Boolean containId(Integer id, List list)
	{
		for(int i = 0; i < list.size(); i++)
		{
			Object obj = list.get(i);

			if(obj != null)
			{
				try
				{
					Field idField = obj.getClass().getDeclaredField("id");
					idField.setAccessible(true);

					Integer value = (Integer) idField.get(obj);

					idField.setAccessible(false);
					idField = null;

					if(value.intValue() == id.intValue())
						return true;
				}
				catch(Exception ex)
				{
					//SUPRESS
				}
			}
		}
		return false;
	}

	/**
	 * Verifica se todos os valores de uma lista existem na outra
	 * @param idList Lista principal
	 * @param list Lista de valores a ser verificados
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Boolean existList(List<Integer> idList, List list)
	{
		for(int i = 0; i < list.size(); i++)
		{
			Object obj = list.get(i);

			if(obj != null)
			{
				try
				{
					Field idField = obj.getClass().getDeclaredField("id");
					idField.setAccessible(true);

					Integer value = (Integer) idField.get(obj);

					idField.setAccessible(false);
					idField = null;

					for(Integer id : idList)
					{
						if(id.intValue() == value.intValue())
							return true;
					}
				}
				catch(Exception ex)
				{
					//SUPRESS
				}
			}
		}
		return false;
	}

	/**
	 * Remove os objetos com Id igual ao do parâmetro passado
	 * @param id Id do objeto
	 * @param list Lista de objetos
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List removeById(Integer id, List list)
	{
		List finalList = new ArrayList();

		for(int i = 0; i < list.size(); i++)
		{
			Object obj = list.get(i);

			if(obj != null)
			{
				try
				{
					Field idField = obj.getClass().getDeclaredField("id");
					idField.setAccessible(true);

					Integer value = (Integer) idField.get(obj);

					idField.setAccessible(false);
					idField = null;

					if(id.intValue() != value.intValue())
						finalList.add(obj);
				}
				catch(Exception ex)
				{
					//SUPRESS
				}
			}
		}
		return finalList;
	}

	/**
	 * Verifica se na lista existem uma string igual
	 * @param value
	 * @param list
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Boolean contains(String value, List list)
	{
		for(int i = 0; i < list.size(); i++)
		{
			Object obj = list.get(i);

			if(obj != null)
			{
				try
				{
					if(((String)obj).equals(value))
						return true;
				}
				catch(Exception ex)
				{
					//SUPRESS
				}
			}
		}
		return false;
	}

	/**
	 * Retorna um único objeto com o Id igual ao do parâmetro passado
	 * @param id Id do objeto
	 * @param list Lista de objetos
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Object getById(int id, List list)
	{
		for(int i = 0; i < list.size(); i++)
		{
			Object obj = list.get(i);

			if(obj != null)
			{
				try
				{
					if(((Integer)obj.getClass().getMethod("getId").invoke(obj)) == id)
						return obj;
				}
				catch(Exception ex)
				{
					//SUPRESS
				}
			}
		}
		return null;
	}

	/**
	 * Recebe um Calendar e trata ele para que ignore o fim de semana, ou seja, só caia em dias úteis.
	 * @param calendar
	 * @return calendar
	 * @throws Exception
	 */
	public Calendar ignoreWeekend(Calendar calendar) throws Exception
	{
		if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
			calendar.add(Calendar.DAY_OF_YEAR, 2);
		else if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
			calendar.add(Calendar.DAY_OF_WEEK, 1);

		return calendar;
	}

	/**
	 * Gera um número randomico de 10 caracteres.
	 * Retorna em uma String
	 * @return String
	 */
	public String getRandomNumberToString()
	{
		return String.valueOf(this.getRandomNumber());
	}

	/**
	 * Gera um número randomico de 10 caracteres.
	 * Retorna em long.
	 * @return long
	 */
	public long getRandomNumber()
	{
		return new Random().nextInt(1000000000);
	}

	/**
	 * Recupera um número randômico
	 * @param size Tamanho do número
	 * @return Objeto do tipo {@link String}
	 */
	public String getRandomNumberToString(int size)
	{
		return String.valueOf(this.getRandomNumber(size));
	}

	/**
	 * Recupera um número randômico
	 * @param size Tamanho do número
	 * @return Objeto do tipo long
	 */
	public long getRandomNumber(int size)
	{
		return (size < 1 ? 0 : new Random().nextInt((9 * (int) Math.pow(10, size - 1)) - 1) + (int) Math.pow(10, size - 1));
	}

	/**
	 * Remove acentos de uma String
	 * @param str
	 * @return String
	 */
	public String removeAcentos(String str) 
	{
		str = Normalizer.normalize(str, Normalizer.Form.NFD);
		str = str.replaceAll("[^\\p{ASCII}]", "");
		return str;	 
	}

	/**
	 * Remove um Char se estiver no final da {@link String} passada como parâmetro
	 * @param data
	 * @param charToTrim
	 * @return
	 */
	public String trimEnd(String data, String charToTrim)
	{
		while(data.endsWith(charToTrim))
		{
			data = data.substring(0, data.length() - 1);
		}

		return data;
	}

	/**
	 * Adiciona um separador ' ' a cada letra em Caixa Alta encontrada na {@link String}
	 * @param data
	 * @return
	 */
	public static String separeEveryUpperCase(String data)
	{
		String returnData = "";

		for(int i = 0; i < data.length(); i++)
		{
			if(((int) data.charAt(i)) == ((int) ((data.charAt(i) + "").toUpperCase().charAt(0))))
				returnData = returnData + " ";

			returnData = returnData + (data.charAt(i) + "");
		}	  

		return returnData;
	}

	/**
	 * Transforma o primeiro Char de uma String em Caixa Alta
	 * @param data
	 * @return
	 */
	public static String firstCharUpperCase(String data)
	{
		return (data.charAt(0x0) + "").toUpperCase() + data.substring(0x1, data.length());
	}

	/**
	 * Arredonda valores
	 * @param data
	 * @param placesCount número de casas decimais
	 * @return
	 */
	public static double roundUpDecimalPlaces(double data, int placesCount)
	{
		return new BigDecimal(data).setScale(placesCount, BigDecimal.ROUND_HALF_EVEN).doubleValue();
	}

	/**
	 * Verifica se o disco C: do servidor está com o espaço esgotado
	 * @throws Exception
	 */
	@Deprecated()
	public static void handleHardDiskSpace() throws Exception
	{
		ConexaoUtil.getConexaoPadrao();

		File diskPartition = new File("C:");

		long freePartitionSpace = diskPartition.getFreeSpace() / (1024 * 1024);

		freePartitionSpace = 2000;

		diskPartition = null;

		if(freePartitionSpace < 3000)
		{
			Logger.getGlobal().severe("LEVEL OF FREE HARD DISK SPACE IS CRITICAL. HANDLING...");

			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH, -15);

			Logger.getGlobal().info("CLEANING BINARY LOGS.");

			ConexaoUtil.getConexaoPadrao().createStatement().executeQuery("PURGE BINARY LOGS BEFORE '" + new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()) + "';");

			Logger.getGlobal().info("BINARY LOGS CLEANED.");
			Logger.getGlobal().info("CLEANING MYSQL LOG FILE.");

			//File file = new File("C:/desenv/mysql/mysql server 5.6/data/SRV-NEPAL.log");

			File file = new File("D:/Desenv/MySQL/MySQL 5.5/Data Files/data/SRV-NEPAL.log");        	
			if(file.exists())
			{
				Files.move(Paths.get(file.getPath()), Paths.get("F:/Desenv/SRV-NEPAL.log"), StandardCopyOption.REPLACE_EXISTING);

				Logger.getGlobal().info("FILE SRV-NEPAL.log HAS " + (file.getTotalSpace() / (1024 * 1024 * 1024)) + " GB. MOVED TO F:/Desenv");

				file = null;

				file = new File("F:/Desenv/SRV-NEPAL.log");
				File newFile = new File("F:/Desenv/SRV-NEPAL " + new SimpleDateFormat("dd-MM-yyyy").format(new Date()) + ".log");

				file.renameTo(newFile);

				Logger.getGlobal().info("FILE RENAMED TO " + newFile.getName());

				file = null;
				newFile = null;
			}

			Logger.getGlobal().info("PROCESS COMPLETE.");

			diskPartition = new File("C:");
			freePartitionSpace = diskPartition.getFreeSpace() / (1024 * 1024);

			Logger.getGlobal().info("HARD DISK CURRENT FREE SPACE : " + freePartitionSpace + " MB.");

			if(freePartitionSpace >= 7000)
				Logger.getGlobal().info("SYSTEM STATUS NORMALIZED.");
			else
				Logger.getGlobal().info("SYSTEM STATUS STABLE. WARNING STATUS IS ONLINE.");
		}
		else if(freePartitionSpace < 5000)
		{
			Logger.getGlobal().warning("FREE HARD DISK SPACE IS ENDING.");
		}
		else
			Logger.getGlobal().severe("PROCEDURE [CLEAN MYSQL LOGS] TAKEN. HARD DISK STILL IN CRITICAL STATUS");
	}

	/**
	 * Compacta arquivo
	 * @param arqEntrada diretório do arquivo para ser compactado
	 * @param arqSaida diretório do arquivo compactado
	 * @throws Exception
	 */
	public static void compactarArquivo(String arqEntrada, String arqSaida) throws Exception
	{
		int cont;   	
		int TAMANHO_BUFFER = 4096;

		byte[] data = new byte[TAMANHO_BUFFER];                            

		BufferedInputStream bufferedInputStream = null;        
		FileInputStream fileInputStream = null;        
		FileOutputStream fileOutputStream = null;        
		ZipOutputStream zipOutputStream = null;        
		ZipEntry entry = null;                       

		try 
		{             
			fileOutputStream = new FileOutputStream(new File(arqSaida));           

			zipOutputStream = new ZipOutputStream(new BufferedOutputStream(fileOutputStream));             
			File file = new File(arqEntrada);             
			fileInputStream = new FileInputStream(file);             
			bufferedInputStream = new BufferedInputStream(fileInputStream, TAMANHO_BUFFER);        

			entry = new ZipEntry(file.getName()); 

			zipOutputStream.putNextEntry(entry);      	

			while((cont = bufferedInputStream.read(data, 0x0, TAMANHO_BUFFER)) != -0x1) 
			{                 
				zipOutputStream.write(data, 0x0, cont);             				
			}             

			bufferedInputStream.close();             
			zipOutputStream.close();         
		} 
		catch(IOException e) 
		{             
			throw new IOException(e.getMessage());         
		}
	}

	/**
	 * Descompacta arquivo
	 * @param origem diretório arquivo compactado
	 * @param destino diretório para descompactar 
	 * @throws Exception
	 */
	public static void descompactarArquivo(String origem, String destino) throws Exception 
	{  
		if (origem == null || destino == null) 
		{  
			return;  
		}  

		int TAMANHO_BUFFER = 4096;

		FileInputStream fileInputStream = new FileInputStream(origem);  

		ZipInputStream zipInputStream = new ZipInputStream(fileInputStream); 
		FileOutputStream fileOutputStream = null;  
		BufferedOutputStream bufferedOutputStream = null;  
		ZipEntry ze = null;  

		String name = "";

		while ((ze = zipInputStream.getNextEntry()) != null) 
		{  	
			name = destino + "/" + ze.getName();  

			try 
			{  
				fileOutputStream = new FileOutputStream(name);  
			} 
			catch (FileNotFoundException exc) 
			{  
				File file = new File(name);

				file.getParentFile().mkdir();

				fileOutputStream = new FileOutputStream(file);  
			}  

			bufferedOutputStream = new BufferedOutputStream(fileOutputStream, TAMANHO_BUFFER);  

			int bytes;  
			byte buffer[] = new byte[TAMANHO_BUFFER];  

			while ((bytes = zipInputStream.read(buffer, 0, TAMANHO_BUFFER)) != -1) 
			{  
				bufferedOutputStream.write(buffer, 0, bytes);  
			}  

			bufferedOutputStream.flush();  
			bufferedOutputStream.close(); 
		}  
		zipInputStream.close(); 
	}  

	/**
	 * Cria um diretório
	 * @param name Caminho do diretório
	 * @throws Exception
	 */
	public static void makeDir(String name) throws Exception 
	{  
		try
		{
			File dir = new File(name); 

			if(!dir.exists())
				dir.mkdirs();  	
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}  

	/**
	 * Transforma uma {@link String} em uma {@link String} encryptada no padrão MD5
	 * @param pwd String a ser encryptada
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static String criptografarMd5(String pwd)  throws NoSuchAlgorithmException
	{  
		return new String(hexCodes(MessageDigest.getInstance("MD5").digest(pwd.getBytes())));      
	}  

	/**
	 * Recupera o código Hexadecimal de um byte[]
	 * @param text byte[] a ser convertido
	 * @return
	 */
	private static char[] hexCodes(byte[] text) 
	{  
		char[] hexOutput = new char[text.length * 2];  
		String hexString;  

		for (int i = 0; i < text.length; i++) 
		{  
			hexString = "00" + Integer.toHexString(text[i]);  
			hexString.getChars(hexString.length() - 2, hexString.length(), hexOutput, i * 2);  
		}  
		return hexOutput;  
	}  

	/**
	 * Retorna apenas valores diferentes, ignora valores iguais de uma lista
	 * @param lista Lista dos valores
	 * @return
	 */
	public List<Integer> retornarDiferentes(List<Integer> lista)
	{
		List<Integer> listaNova = new ArrayList<>();
		for (int i = 0; i < lista.size(); i++) 
		{
			if(!listaNova.contains(lista.get(i)))
			{
				listaNova.add(lista.get(i));
			}
		}
		return listaNova;
	}

	/**
	 * Recuperar objeto de um byte[]
	 * @param objeto
	 * @return
	 * @throws Exception
	 */
	public Object recuperarObjectoPorByteArray(byte[] objeto) throws Exception
	{
		Object obj = null;

		try
		{
			ByteArrayInputStream bis = new ByteArrayInputStream(objeto);

			ObjectInput in = null;

			try
			{
				in = new ObjectInputStream(bis);
				obj = in.readObject();
			} 
			finally
			{
				bis.close();
				in.close();
			}
		} 
		catch (Exception e)
		{
			throw e;
		}
		return obj;
	}

	public byte[] transformaObjetoByteArray(Object obj) throws Exception
	{
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		ObjectOutputStream o = new ObjectOutputStream(b);
		o.writeObject(obj);
		return b.toByteArray();
	}

	public static List<Date> gerarListDataPorPeriodo(String dataInicial,String dataFinal,int excluirDia) throws ParseException
	{
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		Date ini = formatador.parse(dataInicial);
		Date fin = formatador.parse(dataFinal);
		return gerarListDataPorPeriodo(ini, fin,excluirDia);
	}
	public static List<Date> gerarListDataPorPeriodo(Date dataInicial,Date dataFinal,Integer excluirDia)
	{
		List<Date> lista = new ArrayList<Date>();
		Calendar cal = Calendar.getInstance();
		cal.setTime(dataInicial);
		if(excluirDia==null || !(cal.get(Calendar.DAY_OF_WEEK)== excluirDia))
		{
			lista.add(cal.getTime());
		}
		while(cal.getTime().getTime()<dataFinal.getTime())
		{
			cal.add(Calendar.DAY_OF_MONTH, +1);
			if(excluirDia==null || !(cal.get(Calendar.DAY_OF_WEEK)== excluirDia))
			{
				lista.add(cal.getTime());
			}

		}
		return lista;
	}
	public static final Integer diferencaDias(Date data1, Date data2) throws ParseException
	{
		long dt = (data1.getTime() - data2.getTime()) + 3600000;        
		Long dias = (dt / 86400000L);
		return dias.intValue();
	}
	public static String zeroEsquerda(String numero,Integer quantidadeZeros)
	{
		return String.format("%0"+quantidadeZeros.intValue()+"d",Integer.parseInt(numero.replace(".", "")));
	}
	public static String zeroEsquerda(Long numero,Integer quantidadeZeros)
	{
		return String.format("%0"+quantidadeZeros.intValue()+"d",numero);
	}
	public static String zeroEsquerda(Double numero,Integer quantidadeZeros)
	{
		return String.format("%0"+quantidadeZeros.intValue()+"d",Integer.parseInt(numero.toString().replace(".", "")));
	}
	public static String espacoDireita(String texto,Integer quantidadeEspacos)
	{
		return String.format("%1$-"+quantidadeEspacos.intValue()+"s", texto);
	}
	public static void main(String[] args) throws Exception
	{
		System.out.println(dateFormatAnoMes.format(new Date()));
	}
	
	public static String retornarMesExtenso(Integer mes)
	{
		switch(mes)
		{
		case 1: return "Janeiro";
		case 2: return "Fevereiro";
		case 3: return "Março";
		case 4: return "Abril";
		case 5: return "Maio";
		case 6: return "Junho";
		case 7: return "Julho";
		case 8: return "Agosto";
		case 9: return "Setembro";
		case 10: return "Outubro";
		case 11: return "Novembro";
		case 12: return "Dezembro";

		default: return "";
		}
	}
	/**
	 * Retorna diferenca entre as datas conforme o parametro tipo 
	 * @param dataDe data inicial.
	 * @param dataAte data final.
	 * @param tipo é o tipo de retorno: 1:Segundos 2:Minutos 3:Horas 4:Dias 5:Meses 6:Anos
	 * @return Double
	 */
	public  Double diferencaDatas(Date dataDe,Date dataAte,int tipo)
	{
		Long diferenca = null;
		switch (tipo){
		case 1: 
			diferenca = (dataAte.getTime() - dataDe.getTime()) / (1000);//segundos
			break;
		case 2:
			diferenca = (dataAte.getTime() - dataDe.getTime()) / (1000*60);//Minutos
			break;
		case 3:
			diferenca = (dataAte.getTime() - dataDe.getTime()) / (1000*60*60);//Horas
			break;
		case 4:
			diferenca = (dataAte.getTime() - dataDe.getTime()) / (1000*60*60*24);//Dias 
			break;
		case 5:
			diferenca = (dataAte.getTime() - dataDe.getTime()) / (1000*60*60*24) / 30;//Meses
			break;
		case 6:
			diferenca = ((dataAte.getTime() - dataDe.getTime()) / (1000*60*60*24) / 30) / 12;//Anos
			break;
		default:
			diferenca = (dataAte.getTime() - dataDe.getTime()) / (1000*60*60*24);//Dias 
		}
		return diferenca.doubleValue();
	}
	/**
	 * Retorna uma string sem os caracter especiais e subistitui letras com acento para sem acento
	 * @param texto é o texto que vai ser convertido.
	 * @return String
	 */
	public static String removerCatacterEspeciais(String texto){
		texto = Normalizer.normalize(texto, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
		texto = texto.replaceAll("[^a-zA-Z0-9, :;?@\\[\\]_-{}<>=!#$%&()+]", "").replaceAll("°", "");

		return texto;
	}
	public static String fomatadorCpfCnpj(String cpfCnpj ) throws Exception
	{

		try
		{
			if(cpfCnpj.length()==11)
			{
				cpfCnpj = cpfCnpj.substring(0,3)+"."+cpfCnpj.substring(3, 6)+"."+cpfCnpj.substring(6,9)+"-"+cpfCnpj.substring(9,11);
				return cpfCnpj;
			}
			else if(cpfCnpj.length()==14)
			{
				cpfCnpj = cpfCnpj.substring(0,2)+"."+cpfCnpj.substring(2, 5)+"."+cpfCnpj.substring(5,8)+"/"+cpfCnpj.substring(8,12)+"-"+cpfCnpj.substring(12,14);
				return cpfCnpj;
			}
			else
			{
				throw new Exception("O valor precisa ter 11 ou 15 caracteres.");
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}
	}
public static ArrayList<String> quebrarEmLinhas(String texto,int tamanhoLinha){
		
	    ArrayList<String> linhas = new ArrayList<String>();
		if(texto.length()>tamanhoLinha){
		 
		   int index = texto.substring(0,tamanhoLinha).lastIndexOf(" ");
		   linhas.add(texto.substring(0, index));
		   String segundaLinha =  texto.substring(index+1);
		   if(segundaLinha.length()>tamanhoLinha)
		   {
			   return quebrarEmLinhas(segundaLinha,tamanhoLinha, linhas);
		   }
		   else
		   {
			   linhas.add(segundaLinha);
			   return linhas; 
		   }
		}
		else
		{
			 linhas.add(texto);
			 return linhas;
		}
		
	}
	private static  ArrayList<String> quebrarEmLinhas(String texto,int tamanhoLinha, ArrayList<String> linhas){
		
		if(texto.length()>tamanhoLinha)
		{
		 
		   int index = texto.substring(0,tamanhoLinha).lastIndexOf(" ");
		   linhas.add(texto.substring(0, index));
		   String segundaLinha =  texto.substring(index+1);
		   if(segundaLinha.length()>tamanhoLinha)
		   {
			   return quebrarEmLinhas(segundaLinha,tamanhoLinha, linhas);
		   }
		   else
		   {
			   linhas.add(segundaLinha);
			   return linhas; 
		   }
		}
		return linhas;
	}
}