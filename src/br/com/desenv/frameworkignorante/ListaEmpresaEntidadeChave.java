package br.com.desenv.frameworkignorante;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.desenv.nepalign.model.EmpresaEntidadeChave;
import br.com.desenv.nepalign.service.EmpresaEntidadeChaveService;
import br.com.desenv.nepalign.util.DsvConstante;

public final class ListaEmpresaEntidadeChave
{
    private static HashMap<String, EmpresaEntidadeChave> hashChaveEntidadeAtual = null;
    private static ListaEmpresaEntidadeChave instance = null;

    public static ListaEmpresaEntidadeChave getInstance()  throws Exception
    {
		if (instance == null)
		    return (instance = new ListaEmpresaEntidadeChave());
		else
		    return instance;
    }
    
    public static void reloadKeys() throws Exception
    {
    	unloadKeys();
    	
    	instance = new ListaEmpresaEntidadeChave();
    }
    
    private static void unloadKeys()
    {
    	hashChaveEntidadeAtual = null;
    	instance = null;
    }
    
    private ListaEmpresaEntidadeChave() throws Exception
    {
    	try
    	{
    		if (hashChaveEntidadeAtual == null)
        	{   
        		hashChaveEntidadeAtual = new HashMap<String, EmpresaEntidadeChave>();

        		EmpresaEntidadeChaveService empresaEntidadeChaveService = new EmpresaEntidadeChaveService();	
        		
        		try
        		{
            		DsvConstante.getParametrosSistema().getInt("EMPRESA_LOCAL");	
        		}
        		catch(Exception ex)
        		{
        			Logger.getRootLogger().error("Não foi possível carregar o parâmetro EMPRESA_LOCAL para a geração da chaves das entidades.", ex);
        			throw ex;
        		}
    	
        		Integer idEmpresaFisica = DsvConstante.getParametrosSistema().getInt("EMPRESA_LOCAL");
    	
    		    EmpresaEntidadeChave empresaEntidadeChaveFiltro = new EmpresaEntidadeChave();
    		    
    		    if(idEmpresaFisica != 52)
    		    	empresaEntidadeChaveFiltro.setIdEmpresaFisica(idEmpresaFisica); 
    		    
    		    List<EmpresaEntidadeChave> listaEmpresaEntidadeChave = empresaEntidadeChaveService.listarPorObjetoFiltro(empresaEntidadeChaveFiltro);
    		    
    		    for (EmpresaEntidadeChave empresaEntidadeChave: listaEmpresaEntidadeChave)
    		    {
    				Integer ultimaChave = empresaEntidadeChaveService.getUltimaChave(empresaEntidadeChave, empresaEntidadeChave.getChaveFinal());
    				
    				if (ultimaChave == null)
    				    throw new Exception("Verificar ultima chave na entidade " + empresaEntidadeChave.getEntidade());
    				else if (ultimaChave == 0x00)
    				    empresaEntidadeChave.setChaveAtual(empresaEntidadeChave.getChaveInicial());
    				else if (ultimaChave < empresaEntidadeChave.getChaveInicial())
    				    empresaEntidadeChave.setChaveAtual(empresaEntidadeChave.getChaveInicial());
    				else if(ultimaChave > empresaEntidadeChave.getChaveFinal())
    					empresaEntidadeChave.setChaveAtual(empresaEntidadeChaveService.getUltimaChave(empresaEntidadeChave));
    				else
    				    empresaEntidadeChave.setChaveAtual(ultimaChave);
    				
    				hashChaveEntidadeAtual.put(empresaEntidadeChave.getIdEmpresaFisica() + "_" + empresaEntidadeChave.getEntidade(), empresaEntidadeChave);
    		    }
        	} 	
    	}
    	catch(Exception ex)
    	{
    		Logger.getRootLogger().error("Não foi possível carregar as chaves das entidades!", ex);
    	}
    }
    

    public static HashMap<String, EmpresaEntidadeChave> getHashChaveEntidadeAtual()
    {
        return hashChaveEntidadeAtual;
    }
    
    public static HashMap<String, EmpresaEntidadeChave> getHashChaveEntidadeAtual(Integer idEmpresaFisica)
    {
    	if(idEmpresaFisica.intValue() == 0x00)
    		return hashChaveEntidadeAtual;
    	
    	HashMap<String, EmpresaEntidadeChave> hashChaveEntidadeAtualBuffer = new HashMap<String, EmpresaEntidadeChave>();
    	
    	for(Object entidadeChaveKey : hashChaveEntidadeAtual.keySet().toArray())
    	{
    		EmpresaEntidadeChave entidadeChave = hashChaveEntidadeAtual.get(entidadeChaveKey);
    		
    		if(entidadeChave.getIdEmpresaFisica().intValue() == idEmpresaFisica.intValue())
    			hashChaveEntidadeAtualBuffer.put(((String) entidadeChaveKey), entidadeChave);
    	}
    	
        return hashChaveEntidadeAtualBuffer;
    }
}