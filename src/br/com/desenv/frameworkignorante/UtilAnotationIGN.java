package br.com.desenv.frameworkignorante;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.JoinColumn;

public class UtilAnotationIGN
{
	public static void main(String[] args)
	{
		/*UtilAnotationIGN t = new UtilAnotationIGN();
		Livro t2 = new Livro();
		Object[] res = t.recuperaParametrosComAnotacaoColumn(t2.getClass());
		for (int i = 0; i < res.length; i++)
		{
			System.out.println(res[i].toString());
		}

		Object[] res2 = t.recuperaParametrosComAnotacao(t2.getClass(), "Column");
		for (int i = 0; i < res2.length; i++)
		{
			System.out.println(res2[i].toString());
		}

		t.imprimeAnotacoes(t2.getClass());*/

	}

	public static void imprimeAnotacoes(Class classe)
	{
		ArrayList resultado = new ArrayList();

		Field[] atributos = classe.getDeclaredFields();

		for (Field f : atributos)
		{
			Annotation a[] = f.getAnnotations();
			for (Annotation a2 : a)
			{
				System.out.println(a2.annotationType().getSimpleName());
			}
		}

	}

	public static Object[] recuperaParametrosComAnotacao(Class classe, String nomeAnotacao)
	{
		ArrayList resultado = new ArrayList();

		Field[] atributos = classe.getDeclaredFields();

		for (Field f : atributos)
		{
			Annotation a[] = f.getAnnotations();
			for (Annotation a2 : a)
			{
				if (nomeAnotacao.equalsIgnoreCase(a2.annotationType().getSimpleName()))
					resultado.add(f.getName());
			}
		}
		return resultado.toArray();
	}

	public Object[] recuperaParametrosComAnotacaoColumn(Class classe)
	{
		ArrayList atributos = new ArrayList();

		ArrayList resultado = new ArrayList();

		Field[] campos = classe.getDeclaredFields();

		for (Field f : campos)

		{
			if (f.getType().getSuperclass() != null)
			{
				System.out.println(f.getType().getSuperclass().getName());
			}

			Column col = f.getAnnotation(Column.class);

			if (col != null)
			{
				resultado.add(f.getName());
			}
		}
		return resultado.toArray();
	}

	public static List<Field> recuperarListaAtributoPersistente(GenericModelIGN objeto)
	{
		ArrayList<Field> listaAtributo = new ArrayList<Field>();

		Field[] listaCampoDeclarado = objeto.getClass().getDeclaredFields();

		for (Field campoDeclarado : listaCampoDeclarado)
		{
			Column coluna = campoDeclarado.getAnnotation(Column.class);

			if (coluna != null)
			{
				listaAtributo.add(campoDeclarado);
			}
			else
			{
				JoinColumn juncaoColuna = campoDeclarado.getAnnotation(JoinColumn.class);
				
				if (juncaoColuna != null)
				{
					listaAtributo.add(campoDeclarado);
				}
			}
			
			
			
		}
		return listaAtributo;
	}
	
	public static SearchParameter criarParametroPesquisa(Field campo, GenericModelIGN objeto) throws Exception
	{
		String nomeCampo = campo.getName();
		String operador = "=";

		Method metodoGet = objeto.getClass().getMethod(UtilAnotationIGN.toMethodGet(campo.getName()), null);
		Object valor = metodoGet.invoke(objeto, null);

		if (valor == null)
		{
			return null;
		}
		
		if(campo.getType() == Double.class)
		{
			if(((Double) valor).isNaN())
				return null;
		}
		
		if (campo.getType().getSuperclass() ==  GenericModelIGN.class)
		{
			nomeCampo = campo.getName() + ".id";
			GenericModelIGN valorObjeto = (GenericModelIGN) valor;
			
			valor = valorObjeto.getId();
		}	
		
		if (campo.getType() ==  String.class)
		{
			operador = "like";
			valor = "" + valor.toString() + "";
		}
		else if(campo.getType() == Date.class)
		{
			nomeCampo = campo.getName() + "Min;" + campo.getName() + "Max";
			operador = "BETWEEN";
			
			Calendar calendarHoraPesquisa = new GregorianCalendar();
			calendarHoraPesquisa.setTime((Date) valor);
			calendarHoraPesquisa.set(Calendar.HOUR_OF_DAY, 23);
			calendarHoraPesquisa.set(Calendar.MINUTE, 59);
			calendarHoraPesquisa.set(Calendar.SECOND, 59);
			
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			String dataPesquisa = new SimpleDateFormat("yyyy-MM-dd").format((Date) valor) + " 00:00:00";
			String dataFinalPesquisa = simpleDateFormat.format(calendarHoraPesquisa.getTime());
			
			valor = "" + dataPesquisa + ";" + dataFinalPesquisa + "";
		}
		
		return new SearchParameter(nomeCampo, operador, valor);
	}
	
	public static String toMethodGet(String prop) {
		return "get" + prop.substring(0, 1).toUpperCase() + prop.substring(1);
	}

	public static String toMethodSet(String prop) {
		return "set" + prop.substring(0, 1).toUpperCase() + prop.substring(1);
	}
}
