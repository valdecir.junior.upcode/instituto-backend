package br.com.desenv.frameworkignorante;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao;
import br.com.desenv.nepalign.model.Acao;
import br.com.desenv.nepalign.model.Acaogrupo;
import br.com.desenv.nepalign.model.Grupo;
import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.nepalign.model.Usuarioacao;
import br.com.desenv.nepalign.model.Usuariogrupo;
import br.com.desenv.nepalign.relatorios.IgnRelatorio;
import br.com.desenv.nepalign.service.AcaoService;
import br.com.desenv.nepalign.service.AcaogrupoService;
import br.com.desenv.nepalign.service.GrupoService;
import br.com.desenv.nepalign.service.UsuarioService;
import br.com.desenv.nepalign.service.UsuarioacaoService;
import br.com.desenv.nepalign.service.UsuariogrupoService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;
import flex.messaging.MessageException;
import flex.messaging.log.Log;

public class IgnSegurancaService 
{
	private static final Logger logger = Logger.getLogger("NEPAL.SECURITY");
	
	private static final UsuarioService usuarioService = new UsuarioService();
	private static final UsuarioacaoService usuarioAcaoService = new UsuarioacaoService();
	private static final UsuariogrupoService usuarioGrupoService = new UsuariogrupoService();
	private static final AcaogrupoService acaoGrupoService = new AcaogrupoService();
	private static final GrupoService grupoService = new GrupoService();
	private static final AcaoService acaoService = new AcaoService();
	
	private static final ArrayList<Usuario> listaUsuarios = new ArrayList<Usuario>();
	
	public IgnSegurancaService() { }
	
	public static synchronized void atualizaListaUsuarios() throws Exception
	{
		synchronized (listaUsuarios) 
		{
			final EntityManager manager = ConexaoUtil.getEntityManager();

			try
			{
				Log.getLogger("INICIALIZACAO_NEPAL").info("Atualizacao de credenciais de acesso");
				
				listaUsuarios.clear();
				
				Usuarioacao criterioUsuarioAcao = new Usuarioacao();
				Usuariogrupo criterioUsuarioGrupo = new Usuariogrupo();
				Acaogrupo criterioAcaoGrupo;
				
				final List<Usuario> listaUsuariosAtu = usuarioService.listar(manager, null, null, null);
				
				
				final HashMap<Integer, List<Acao>> listaAcoesPorGrupo = new HashMap<Integer, List<Acao>>(); 
				
				for (final Grupo meuGrupo : grupoService.listar()) 
				{
					final List<Acao> listaAcao = new ArrayList<Acao>();
					
					criterioAcaoGrupo = new Acaogrupo();
					criterioAcaoGrupo.setGrupo(meuGrupo);
					
					for (final Acaogrupo ag : acaoGrupoService.listarPorObjetoFiltro(manager, criterioAcaoGrupo, null, null, null)) 
						listaAcao.add(ag.getAcao());	
					
					listaAcoesPorGrupo.put(meuGrupo.getId(), listaAcao);
				}
				
				for (final Usuario usuario : listaUsuariosAtu) 
				{
					final Usuario usuarioAcesso = new Usuario();
					usuarioAcesso.setId(usuario.getId());
					usuarioAcesso.setNome(usuario.getNome());
					usuarioAcesso.setLogin(usuario.getLogin());
					usuarioAcesso.setEmpresaPadrao(usuario.getEmpresaPadrao());
			
					criterioUsuarioAcao = new Usuarioacao();
					criterioUsuarioAcao.setUsuario(usuario);
					
					for (final Usuarioacao usuarioAcao : usuarioAcaoService.listarPorObjetoFiltro(manager, criterioUsuarioAcao, null, null, null)) 
						usuarioAcesso.getAcoesLiberadas().add(usuarioAcao.getAcao());
					
					criterioUsuarioGrupo = new Usuariogrupo();
					criterioUsuarioGrupo.setUsuario(usuario);
					
					for (final Usuariogrupo usuariogrupo : usuarioGrupoService.listarPorObjetoFiltro(manager, criterioUsuarioGrupo, null, null, null)) 
					{
						if (listaAcoesPorGrupo.containsKey(usuariogrupo.getGrupo().getId()))
							usuarioAcesso.getAcoesLiberadas().addAll(listaAcoesPorGrupo.get(usuariogrupo.getGrupo().getId()));	
					}
					
					listaUsuarios.add(usuarioAcesso);
				}	
			}
			finally
			{
				if(manager.isOpen())
					manager.close();
			}	
		}
	}
	
	public static synchronized ArrayList<Usuario> getListaUsuarios()
	{
		synchronized (listaUsuarios) {
			return listaUsuarios;	
		}
	}

	public static final Method getMethod(final String method, final Class<?> service, boolean fromSuper)
	{
		for(final Method _method : (fromSuper ? service.getMethods() : service.getDeclaredMethods()))
			if(_method.getName().equalsIgnoreCase(method.toLowerCase()))
				return _method;
		return null;
	}
	
	public static final Class<?> getServiceClass(String service)
	{
		try 
		{
			return Class.forName(getPackage(service).concat(".").concat(IgnUtil.firstCharUpperCase(service)));
		}
		catch (NoClassDefFoundError e)
		{
			return null;
		}
		catch (ClassNotFoundException e) 
		{
			return null;
		}
	}
	
	private static String getPackage(String className)
	{
		if(className.equals("BackupService"))
			return "br.com.desenv.nepalign.framework";
		else if(className.contains("CockPit"))
			return "br.com.desenv.nepalign.service.cockpits";
		else if(className.contains("Relatorio"))
			return "br.com.desenv.nepalign.relatorios";
		else 
			return "br.com.desenv.nepalign.service";
	}
	
	/**
	 * Verifica se o método da operação está marcado pela annotation @IgnSegurancaAcao
	 * @param operation nome do método
	 * @param destination nome do service
	 * @return true se estiver marcado com a annotation, false se não
	 * @throws MessageException
	 */
	public static boolean isMethodSigned(final String operation, final String destination) throws MessageException
	{
		try
		{
			Method method = IgnSegurancaService.getMethod(operation, IgnSegurancaService.getServiceClass(destination), false);
			
			if(method == null)
			{
				method = IgnSegurancaService.getMethod(operation, IgnSegurancaService.getServiceClass(destination), true);	
				
				if(method != null) {} // generic ign method
				else 
					throw new MessageException("Não foi possível encontrar o método " + operation + " na classe " + destination);
			}
			else
			{
				if (!method.isAnnotationPresent(IgnSegurancaAcao.class))
					throw new MessageException("Não é possível acessar um método desprotegido!\n"+"CLASSE: "+destination+"\n"+"METODO: "+ method.getName());	
			}
			return true;
		}
		catch(MessageException ex)
		{
			throw ex;
		}
	}
	
	/**
	 * Faz uma varredura os métodos dos services para verificar se existe algum método sem o cadastro de acao
	 */
	public static void verifySignedMethods() throws Exception
	{
		EntityManager manager = null;
		try
		{
			manager = ConexaoUtil.getEntityManager();
			manager.getTransaction().begin();
			
			List<Class<?>> classes = new ArrayList<Class<?>>();
			classes.addAll(getServiceClasses());
			classes.addAll(getReportClasses());
			classes.add(BackupService.class);
			
			for(final Class<?> serviceClass : classes)
			{
				/** recupera somente os métodos declarados na classe, desprezando os métodos herdados do generic ign **/
				for(final Method method : serviceClass.getDeclaredMethods())
				{
					/** Caso o método não estiver marcado com a annotation despreza a verificação **/
					if(method.isAnnotationPresent(IgnSegurancaAcao.class) && !method.isAnnotationPresent(Deprecated.class))
					{
						final IgnSegurancaAcao segurancaAcao = method.getAnnotation(IgnSegurancaAcao.class);

						if(!acaoService.acaoCadastrada(manager, method.getName()))
						{
							if(segurancaAcao.gruposAutorizados().length > 0x00)
							{
								final Acao acao = acaoService.cadastrarAcao(manager, segurancaAcao, method);
								
								for(final IgnSegurancaAcao.Grupo grupoAutorizado : segurancaAcao.gruposAutorizados())
								{
									final Acaogrupo acaoGrupo = new Acaogrupo();
									acaoGrupo.setAcao(acao);
									acaoGrupo.setGrupo(grupoAutorizado.toGrupo(manager));
									manager.persist(acaoGrupo);
								}
							}
							else
								logger.warn("Ação " + serviceClass.getSimpleName() + "." + method.getName() + " não cadastrada e está marcada com @IgnSegurancaAcao. Não existem Perfis de acesso definidos!");
						}	
					}
				}
			}
			
			manager.getTransaction().commit();
		}
		catch(Exception ex)
		{
			if(manager.getTransaction().isActive())
				manager.getTransaction().rollback();
			
			throw ex;
		}
		finally
		{
			manager.close();
		}
	}

	/**
	 * Lista as classes que pertencem ao pacote br.com.desenv.nepalign.service
	 * @return Lista de Class<? extends GenericServiceIGN>
	 */
	private static List<Class<?>> getServiceClasses()
	{
		final List<Class<?>> classes = new ArrayList<Class<?>>();
		
	    for (final URL url : ((URLClassLoader) GenericServiceIGN.class.getClassLoader()).getURLs()) 
	    {
	        final File subdir = new File(new File(url.getFile()), "br/com/desenv/nepalign/service/");
	        
	        if (!subdir.exists())
	            continue;

	        for (final File file : subdir.listFiles()) 
	        {
	            if (!file.isFile())
	                continue;
	            
	            try 
	            {
					classes.add(Class.forName("br.com.desenv.nepalign.service.".concat(file.getName().replace(".class", ""))));
				}
	            catch (ClassNotFoundException e) { }
	        }
	    }
	    return classes;
	}
	
	private static List<Class<?>> getReportClasses()
	{
		final List<Class<?>> classes = new ArrayList<Class<?>>();
		
	    for (final URL url : ((URLClassLoader) IgnRelatorio.class.getClassLoader()).getURLs()) 
	    {
	        final File subdir = new File(new File(url.getFile()), "br/com/desenv/nepalign/relatorios/");
	        
	        if (!subdir.exists())
	            continue;

	        for (final File file : subdir.listFiles()) 
	        {
	            if (!file.isFile())
	                continue;
	            
	            try 
	            {
					classes.add(Class.forName("br.com.desenv.nepalign.relatorios.".concat(file.getName().replace(".class", ""))));
				}
	            catch (ClassNotFoundException e) { }
	        }
	    }
	    return classes;
	}
}