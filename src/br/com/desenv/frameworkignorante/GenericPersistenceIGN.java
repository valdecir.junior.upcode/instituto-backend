package br.com.desenv.frameworkignorante;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import br.com.desenv.nepalign.model.Atendido;
import br.com.desenv.nepalign.service.OperacaoService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class GenericPersistenceIGN<T extends GenericModelIGN>
{
	public GenericPersistenceIGN()
	{
		try 
		{
			ListaEmpresaEntidadeChave.getInstance();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	private Class<T> getT()
	{
		return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	 
	protected void executeUpdate(String sql) throws Exception 
	{
		try
		{
			Connection con = ConexaoUtil.getConexaoPadrao();
			PreparedStatement ps = con.prepareStatement(sql);
			ps.executeUpdate();
			ps.close();
		}
		catch(Exception ex)
		{
			System.out.println(sql);
			throw ex;
		}
	}

	public T salvar(T entidade) throws DsvException
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		
		

		T entidadeSalva = null;

		try
		{
			transaction.begin();
			entidadeSalva = salvar(manager, transaction, entidade);
			transaction.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			transaction.rollback();
			throw new DsvException(e);
		}
		finally
		{
			manager.close();
		}
		
		return entidadeSalva;
	}
	
	public T salvar(T entidade, boolean logar) throws DsvException
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		
		T entidadeSalva = null;

		try
		{
			transaction.begin();
			entidadeSalva = salvar(manager, transaction, entidade, logar);
			transaction.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			transaction.rollback();
			throw new DsvException(e);
		}
		finally
		{
			manager.close();
		}
		
		return entidadeSalva;
	}


	public T salvar(EntityManager manager, EntityTransaction transaction, T entidade) throws Exception
	{
		OperacaoService op = new OperacaoService();
		
		if (transaction == null || manager == null)
		{
			throw new Exception("Variáveis manager e transaction devem ser passadas");
		}

		if (transaction.isActive() == false)
		{
			throw new Exception("Transação deve ser iniciada fora deste método");
		}

		if(this.getT().getDeclaredField("id").getAnnotation(javax.persistence.GeneratedValue.class) != null &&
				this.getT().getDeclaredField("id").getAnnotation(javax.persistence.Id.class) != null && this.getT().getDeclaredField("id").getAnnotation(org.hibernate.annotations.GenericGenerator.class) == null)
			entidade.setId(null);

		
		if (this.getT().getDeclaredField("id").getAnnotation(org.hibernate.annotations.GenericGenerator.class) == null || entidade.getId() == null)
		{
		    manager.persist(entidade);
		}
		else if(this.getT().getDeclaredField("id").getAnnotation(org.hibernate.annotations.GenericGenerator.class).strategy().equals("br.com.desenv.frameworkignorante.BypassGenerator"))
		{
			manager.persist(entidade);	
		}
		else
		{
		    manager.merge(entidade);
		}
		
		op.logaAE(manager, transaction, this.getClass().getName(), "salvar", new Date(), entidade );
		
		return entidade;
	}
	

	public T salvar(EntityManager manager, EntityTransaction transaction, T entidade, boolean logar) throws Exception
	{
		OperacaoService op = new OperacaoService();
		
		if (transaction == null || manager == null)
		{
			throw new Exception("Variáveis manager e transaction devem ser passadas");
		}

		if (transaction.isActive() == false)
		{
			throw new Exception("Transação deve ser iniciada fora deste método");
		}

		if(this.getT().getDeclaredField("id").getAnnotation(javax.persistence.GeneratedValue.class) != null &&
				this.getT().getDeclaredField("id").getAnnotation(javax.persistence.Id.class) != null && this.getT().getDeclaredField("id").getAnnotation(org.hibernate.annotations.GenericGenerator.class) == null)
			entidade.setId(null);

		
		if (this.getT().getDeclaredField("id").getAnnotation(org.hibernate.annotations.GenericGenerator.class) == null || entidade.getId() == null)
		{
		    manager.persist(entidade);
		}
		else if(this.getT().getDeclaredField("id").getAnnotation(org.hibernate.annotations.GenericGenerator.class).strategy().equals("br.com.desenv.frameworkignorante.BypassGenerator"))
		{
			manager.persist(entidade);	
		}
		else
		{
		    manager.merge(entidade);
		}
		
		if (logar)
		    op.logaAE(manager, transaction, this.getClass().getName(), "salvar", new Date(), entidade);
		
		return entidade;
	}

	

	public T atualizar(T entidade) throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		T entidadeAtualizada = null;
		try
		{
			transaction.begin();
			entidadeAtualizada = atualizar(manager, transaction, entidade);
			transaction.commit();
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			transaction.rollback();
			throw e;
		}
		finally
		{
			manager.close();
		}
		
		return entidadeAtualizada;
	}
	
	public T atualizar(T entidade, boolean logar) throws Exception
	{
		
		
		EntityManager manager = ConexaoUtil.getEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		T entidadeAtualizada = null;
		try
		{
			transaction.begin();
			entidadeAtualizada = atualizar(manager, transaction, entidade, logar);
			transaction.commit();
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			transaction.rollback();
			throw e;
		}
		finally
		{
			manager.close();
		}
		
		return entidadeAtualizada;
	}

	public T atualizar(EntityManager manager, EntityTransaction transaction, T entidade) throws Exception
	{		
		if (transaction == null || manager == null)
		{
			throw new Exception("Variáveis manager e transaction devem ser passadas");
		}

		if (transaction.isActive() == false)
		{
			throw new Exception("Transação deve ser iniciada fora deste método");
		}

		OperacaoService op = new OperacaoService();
		op.logaAE(manager, transaction, this.getClass().getName(), "Alterar", new Date(), entidade );
		op.logaAEPersonalizadoAtualizar(manager,transaction,entidade);
		manager.merge(entidade);
		
		
		return entidade;
	}
	
	public T atualizar(EntityManager manager, EntityTransaction transaction, T entidade, boolean logar) throws Exception
	{
		if (transaction == null || manager == null)
		{
			throw new Exception("Variáveis manager e transaction devem ser passadas");
		}
		if (transaction.isActive() == false)
		{
			throw new Exception("Transação deve ser iniciada fora deste método");
		}

		manager.merge(entidade);
		
		if (logar)
		{
    		OperacaoService op = new OperacaoService();
    		op.logaAE(manager, transaction, this.getClass().getName(), "Alterar", new Date(), entidade );
    		op.logaAEPersonalizadoAtualizar(manager,transaction,entidade);
		}	
		
		return entidade;
	}

	public void excluir(T entidade) throws Exception
	{
	
		EntityManager manager = ConexaoUtil.getEntityManager();
		EntityTransaction transaction = manager.getTransaction();

		try
		{
			transaction.begin();
			excluir(manager, transaction, entidade);
			transaction.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			
			try
			{
				transaction.rollback();
			}
			catch(Exception illegalException)
			{
				throw new DsvException(e);
			}
			throw new DsvException(e);
		}
		finally
		{
			manager.close();
		}
	}
	
	public void excluir(T entidade, boolean logar) throws Exception
	{
	
		EntityManager manager = ConexaoUtil.getEntityManager();
		EntityTransaction transaction = manager.getTransaction();

		try
		{
			transaction.begin();
			excluir(manager, transaction, entidade, logar);
			transaction.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			
			try
			{
				transaction.rollback();
			}
			catch(Exception illegalException)
			{
				throw new DsvException(e);
			}
			throw new DsvException(e);
		}
		finally
		{
			manager.close();
		}
	}

	public void excluir(EntityManager manager, EntityTransaction transaction, T entidade) throws Exception
	{
		if (transaction == null || manager == null)
		{
			throw new Exception("Variáveis manager e transaction devem ser passadas");
		}
		if (transaction.isActive() == false)
		{
			throw new Exception("Transação deve ser iniciada fora deste método");
		}

		manager.remove(manager.getReference(getT(), entidade.getId()));
		
		OperacaoService op = new OperacaoService();
		op.logaAE(manager, transaction, this.getClass().getName(), "excluir", new Date(), entidade );
		op.logaAEPersonalizadoExcluir(manager, transaction, entidade);
	}
	
	public void excluir(EntityManager manager, EntityTransaction transaction, T entidade, boolean logar) throws Exception
	{	
		if (transaction == null || manager == null)
		{
			throw new Exception("Variáveis manager e transaction devem ser passadas");
		}
		if (transaction.isActive() == false)
		{
			throw new Exception("Transação deve ser iniciada fora deste método");
		}

		manager.remove(manager.getReference(getT(), entidade.getId()));
		
		if (logar)
		{
    		OperacaoService op = new OperacaoService();
    		op.logaAE(manager, transaction, this.getClass().getName(), "excluir", new Date(), entidade );
    		op.logaAEPersonalizadoExcluir(manager, transaction, entidade);
		}
	}
	

	public T recuperarPorId(Integer id) throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		T objeto = recuperarPorId(manager, id);
		manager.close();
		return objeto;
	}
	
	public T recuperarPorId(EntityManager manager, Integer id) throws Exception
	{
		T objeto = manager.find(getT(), id);
		return objeto;
	}
	
	public List<T> listar() throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		List<T> entityList = listar(manager, null, null, null);
		manager.close();
		return entityList;
	}

	public List<T> listar(String ordemPesquisa, String tipoOrdenacao) throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		List<T> entityList = listar(manager, ordemPesquisa, tipoOrdenacao, null);
		manager.close();
		return entityList;
	}
	
	public List<T> listar(String ordemPesquisa, String tipoOrdenacao, Boolean campoNumerico)throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		List<T> entityList = listar(manager, ordemPesquisa, tipoOrdenacao,campoNumerico, null);
		manager.close();
		return entityList;
	}

	public List<T> listar(String ordemPesquisa, String tipoOrdenacao, Integer numeroMaximoRegistro) throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		List<T> entityList = listar(manager, ordemPesquisa, tipoOrdenacao, numeroMaximoRegistro);
		manager.close();
		return entityList;
	}

	public List<T> listar(EntityManager manager, String ordemPesquisa, String tipoOrdenacao, Integer numeroMaximoRegistro) throws Exception
	{

		StringBuffer sql = new StringBuffer("from " + getT().getSimpleName() + " ");

		if (ordemPesquisa != null && !"".equals(ordemPesquisa.trim()) && tipoOrdenacao != null)
		{
			sql.append(" order by " + ordemPesquisa + " " + tipoOrdenacao);
		}

		Query query = manager.createQuery(sql.toString());

		if (numeroMaximoRegistro != null)
		{
			query.setMaxResults(numeroMaximoRegistro);
		}

		List<T> entityList = (List<T>) query.getResultList();

		return entityList;
	}
	
	public List<T> listar(EntityManager manager, String ordemPesquisa, String tipoOrdenacao,  Boolean campoNumerico,Integer numeroMaximoRegistro) throws Exception
	{

		StringBuffer sql = new StringBuffer("from " + getT().getSimpleName() + " ");

		if (ordemPesquisa != null && !"".equals(ordemPesquisa.trim()) && tipoOrdenacao != null)
		{
			if(campoNumerico)
				sql.append(" order by " + ordemPesquisa + "+0 " + tipoOrdenacao);
			else
				sql.append(" order by " + ordemPesquisa + " " + tipoOrdenacao);
		}

		Query query = manager.createQuery(sql.toString());

		if (numeroMaximoRegistro != null)
		{
			query.setMaxResults(numeroMaximoRegistro);
			
		}

		List<T> entityList = (List<T>) query.getResultList();

		return entityList;
	}

	public Page<T> listarPaginadoGenerico(int tamanhoPagina, int numeroPagina, List<SearchParameter> listaParametroPesquisa)
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		Page<T> page = listarPaginadoGenerico(manager, tamanhoPagina, numeroPagina, listaParametroPesquisa, null, null);
		manager.close();
		return page;
	}

	public Page<T> listarPaginadoGenerico(int tamanhoPagina, int numeroPagina, List<SearchParameter> listaParametroPesquisa, String ordemPesquisa, String tipoOrdenacao)
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		Page<T> page = listarPaginadoGenerico(manager, tamanhoPagina, numeroPagina, listaParametroPesquisa, ordemPesquisa, tipoOrdenacao);
		manager.close();
		return page;
	}

	public Page<T> listarPaginadoGenerico(EntityManager manager, int tamanhoPagina, int numeroPagina, List<SearchParameter> listaParametroPesquisa, String ordemPesquisa, String tipoOrdenacao)
	{
		StringBuffer sql = new StringBuffer();
		List<String> listaCriterio = new ArrayList<String>();
		final Map<String, Object> listaParametro = new HashMap<String, Object>();
		sql.append("from " + getT().getSimpleName() + " ");

		if (listaParametroPesquisa != null && listaParametroPesquisa.size() > 0)
		{
			for (SearchParameter parametroPesquisa : listaParametroPesquisa)
			{
				if(parametroPesquisa.getOperator().equals("BETWEEN"))
				{
					try
					{
						String attributeNameMin = parametroPesquisa.getAttributeName().split(";")[0];
						String attributeNameMax = parametroPesquisa.getAttributeName().split(";")[1];
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						
						String attributeValueMin = ((String) parametroPesquisa.getValue()).split(";")[0];
						String attributeValueMax = ((String) parametroPesquisa.getValue()).split(";")[1];
						
						listaCriterio.add(attributeNameMin.replace("Min", "") + " " + parametroPesquisa.getOperator() + " :" + attributeNameMin + " AND :" + attributeNameMax + " ");
						listaParametro.put(attributeNameMin, simpleDateFormat.parse(attributeValueMin));
						listaParametro.put(attributeNameMax, simpleDateFormat.parse(attributeValueMax));
					}
					catch(Exception parseEx)
					{
						//SUPRESS
					}
				}
				else
				{
					listaCriterio.add(parametroPesquisa.getAttributeName() + " " + parametroPesquisa.getOperator() + ":" + parametroPesquisa.getAttributeName().replaceAll("\\.", "_"));
					listaParametro.put(parametroPesquisa.getAttributeName().replaceAll("\\.", "_"), parametroPesquisa.getValue());
				}
			}
			for (int i = 0; i < listaCriterio.size(); i++)
			{
				if (i == 0)
				{
					sql.append("where " + listaCriterio.get(i) + " ");
				}
				else
				{
					sql.append(" and " + listaCriterio.get(i));
				}
			}
		}

		if (ordemPesquisa != null && !"".equals(ordemPesquisa.trim()) && tipoOrdenacao != null)
		{
			sql.append(" order by " + ordemPesquisa + " " + tipoOrdenacao);
		}

		return getPage(tamanhoPagina, numeroPagina, sql, listaParametro, manager);
	}
	
	public Page<T> listarPaginadoGenerico(EntityManager manager, int tamanhoPagina, int numeroPagina, List<SearchParameter> listaParametroPesquisa, String ordemPesquisa, String tipoOrdenacao, Boolean campoNumerico)
	{
		StringBuffer sql = new StringBuffer();
		List<String> listaCriterio = new ArrayList<String>();
		final Map<String, Object> listaParametro = new HashMap<String, Object>();
		sql.append("from " + getT().getSimpleName() + " ");

		if (listaParametroPesquisa != null && listaParametroPesquisa.size() > 0)
		{
			for (SearchParameter parametroPesquisa : listaParametroPesquisa)
			{
				if(parametroPesquisa.getOperator().equals("BETWEEN"))
				{
					try
					{
						String attributeNameMin = parametroPesquisa.getAttributeName().split(";")[0];
						String attributeNameMax = parametroPesquisa.getAttributeName().split(";")[1];
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						
						String attributeValueMin = ((String) parametroPesquisa.getValue()).split(";")[0];
						String attributeValueMax = ((String) parametroPesquisa.getValue()).split(";")[1];
						
						listaCriterio.add(attributeNameMin.replace("Min", "") + " " + parametroPesquisa.getOperator() + " :" + attributeNameMin + " AND :" + attributeNameMax + " ");
						listaParametro.put(attributeNameMin, simpleDateFormat.parse(attributeValueMin));
						listaParametro.put(attributeNameMax, simpleDateFormat.parse(attributeValueMax));
					}
					catch(Exception parseEx)
					{
						//SUPRESS
					}
				}
				else
				{
					listaCriterio.add(parametroPesquisa.getAttributeName() + " " + parametroPesquisa.getOperator() + ":" + parametroPesquisa.getAttributeName().replaceAll("\\.", "_"));
					listaParametro.put(parametroPesquisa.getAttributeName().replaceAll("\\.", "_"), parametroPesquisa.getValue());
				}
			}
			for (int i = 0; i < listaCriterio.size(); i++)
			{
				if (i == 0)
				{
					sql.append("where " + listaCriterio.get(i) + " ");
				}
				else
				{
					sql.append(" and " + listaCriterio.get(i));
				}
			}
		}

		if (ordemPesquisa != null && !"".equals(ordemPesquisa.trim()) && tipoOrdenacao != null)
		{
			if(campoNumerico)
				sql.append(" order by " + ordemPesquisa + "+0 " + tipoOrdenacao);
			else
				sql.append(" order by " + ordemPesquisa + " " + tipoOrdenacao);
		}

		return getPage(tamanhoPagina, numeroPagina, sql, listaParametro, manager);
	}
	
	public Page<T> listarPaginadoGenerico(EntityManager manager, int tamanhoPagina, int numeroPagina, List<SearchParameter> listaParametroPesquisa, String ordemPesquisa, String tipoOrdenacao, String whereAdicional)
	{
		StringBuffer sql = new StringBuffer();
		List<String> listaCriterio = new ArrayList<String>();
		final Map<String, Object> listaParametro = new HashMap<String, Object>();
		sql.append("from " + getT().getSimpleName() + " ");

		if (listaParametroPesquisa != null && listaParametroPesquisa.size() > 0)
		{
			for (SearchParameter parametroPesquisa : listaParametroPesquisa)
			{
				if(parametroPesquisa.getOperator().equals("BETWEEN"))
				{
					try
					{
						String attributeNameMin = parametroPesquisa.getAttributeName().split(";")[0];
						String attributeNameMax = parametroPesquisa.getAttributeName().split(";")[1];
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						
						String attributeValueMin = ((String) parametroPesquisa.getValue()).split(";")[0];
						String attributeValueMax = ((String) parametroPesquisa.getValue()).split(";")[1];
						
						listaCriterio.add(attributeNameMin.replace("Min", "") + " " + parametroPesquisa.getOperator() + " :" + attributeNameMin + " AND :" + attributeNameMax + " ");
						listaParametro.put(attributeNameMin, simpleDateFormat.parse(attributeValueMin));
						listaParametro.put(attributeNameMax, simpleDateFormat.parse(attributeValueMax));
					}
					catch(Exception parseEx)
					{
						//SUPRESS
					}
				}
				else
				{
					listaCriterio.add(parametroPesquisa.getAttributeName() + " " + parametroPesquisa.getOperator() + ":" + parametroPesquisa.getAttributeName().replaceAll("\\.", "_"));
					listaParametro.put(parametroPesquisa.getAttributeName().replaceAll("\\.", "_"), parametroPesquisa.getValue());
				}
			}
			
			for (int i = 0; i < listaCriterio.size(); i++)
			{
				if (i == 0)
				{
					sql.append("where " + listaCriterio.get(i) + " ");
				}
				else
				{
					sql.append(" and " + listaCriterio.get(i));
				}
			}
		}
		
		if(whereAdicional != null)
		{
			if(listaCriterio.size() > 0)
				sql.append(" and " + whereAdicional + " ");
			else
				sql.append(" where " + whereAdicional + " ");	
		}

		if (ordemPesquisa != null && !"".equals(ordemPesquisa.trim()) && tipoOrdenacao != null)
		{
			sql.append(" order by " + ordemPesquisa + " " + tipoOrdenacao);
		}

		return getPage(tamanhoPagina, numeroPagina, sql, listaParametro, manager);
	}

	
	public Page<T> listarPaginadoPorObjetoFiltro(int tamanhoPagina, int numeroPagina, T entidadeFiltro) throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		Page<T> pagina = listarPaginadoPorObjetoFiltro(manager, tamanhoPagina, numeroPagina, entidadeFiltro, null, null);
		manager.close();
		return pagina;
	}
	
	public Page<T> listarPaginadoPorObjetoFiltro(int tamanhoPagina, int numeroPagina, T entidadeFiltro, String ordemPesquisa, String tipoOrdenacao) throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		Page<T> pagina = listarPaginadoPorObjetoFiltro(manager, tamanhoPagina, numeroPagina, entidadeFiltro, ordemPesquisa, tipoOrdenacao);
		manager.close();
		return pagina;
	}
	
	public Page<T> listarPaginadoPorObjetoFiltro(int tamanhoPagina, int numeroPagina, T entidadeFiltro, String ordemPesquisa, String tipoOrdenacao, Boolean campoNumerico) throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		Page<T> pagina = listarPaginadoPorObjetoFiltro(manager, tamanhoPagina, numeroPagina, entidadeFiltro, ordemPesquisa, tipoOrdenacao, campoNumerico);
		manager.close();
		return pagina;
	}
	
	public Page<T> listarPaginadoPorObjetoFiltro(int tamanhoPagina, int numeroPagina, T entidadeFiltro, String ordemPesquisa, String tipoOrdenacao, String whereAdicional) throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		Page<T> pagina = listarPaginadoPorObjetoFiltro(manager, tamanhoPagina, numeroPagina, entidadeFiltro, ordemPesquisa, tipoOrdenacao, whereAdicional);
		manager.close();
		return pagina;
	}
	
	public Page<T> listarPaginadoPorObjetoFiltro(EntityManager manager, int tamanhoPagina, int numeroPagina, T entidadeFiltro, String ordemPesquisa, String tipoOrdenacao) throws Exception
	{
		List<SearchParameter> listaParametroPesquisa = new ArrayList<SearchParameter>();
		
		List<Field> listaAtributoPersistente = UtilAnotationIGN.recuperarListaAtributoPersistente(entidadeFiltro);
		
		for (Field campoPersistente: listaAtributoPersistente)
		{
			SearchParameter parametroPesquisa = UtilAnotationIGN.criarParametroPesquisa(campoPersistente, entidadeFiltro);
			
			if (parametroPesquisa != null)
			{
				listaParametroPesquisa.add(parametroPesquisa);
			}
		}
		
		return listarPaginadoGenerico(manager, tamanhoPagina, numeroPagina, listaParametroPesquisa, ordemPesquisa, tipoOrdenacao);
	}

	public Page<T> listarPaginadoPorObjetoFiltro(EntityManager manager, int tamanhoPagina, int numeroPagina, T entidadeFiltro, String ordemPesquisa, String tipoOrdenacao, Boolean campoNumerico) throws Exception
	{
		List<SearchParameter> listaParametroPesquisa = new ArrayList<SearchParameter>();
		
		List<Field> listaAtributoPersistente = UtilAnotationIGN.recuperarListaAtributoPersistente(entidadeFiltro);
		
		for (Field campoPersistente: listaAtributoPersistente)
		{
			SearchParameter parametroPesquisa = UtilAnotationIGN.criarParametroPesquisa(campoPersistente, entidadeFiltro);
			
			if (parametroPesquisa != null)
			{
				listaParametroPesquisa.add(parametroPesquisa);
			}
		}
		
		return listarPaginadoGenerico(manager, tamanhoPagina, numeroPagina, listaParametroPesquisa, ordemPesquisa, tipoOrdenacao, campoNumerico);
	}
	
	public Page<T> listarPaginadoPorObjetoFiltro(EntityManager manager, int tamanhoPagina, int numeroPagina, T entidadeFiltro, String ordemPesquisa, String tipoOrdenacao, String whereAdicional) throws Exception
	{
		List<SearchParameter> listaParametroPesquisa = new ArrayList<SearchParameter>();
		
		List<Field> listaAtributoPersistente = UtilAnotationIGN.recuperarListaAtributoPersistente(entidadeFiltro);
		
		for (Field campoPersistente: listaAtributoPersistente)
		{
			SearchParameter parametroPesquisa = UtilAnotationIGN.criarParametroPesquisa(campoPersistente, entidadeFiltro);
			
			if (parametroPesquisa != null)
			{
				listaParametroPesquisa.add(parametroPesquisa);
			}
		}
		
		return listarPaginadoGenerico(manager, tamanhoPagina, numeroPagina, listaParametroPesquisa, ordemPesquisa, tipoOrdenacao, whereAdicional);
	}
	
	public List<T> listarPorObjetoFiltro(T entidadeFiltro) throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		List<T> listaEntidade = listarPorObjetoFiltro(manager, entidadeFiltro, null, null, null);
		manager.close();
		return listaEntidade;
	}
	
	public List<T> listarPorObjetoFiltro(T entidadeFiltro, String ordemPesquisa, String tipoOrdenacao) throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		List<T> listaEntidade = listarPorObjetoFiltro(manager, entidadeFiltro, ordemPesquisa, tipoOrdenacao, null);
		manager.close();
		return listaEntidade;
	}
	
	public List<T> listarPorObjetoFiltro(T entidadeFiltro, String ordemPesquisa, String tipoOrdenacao, Integer numeroMaximoRegistro) throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		List<T> listaEntidade = listarPorObjetoFiltro(manager, entidadeFiltro, ordemPesquisa, tipoOrdenacao, numeroMaximoRegistro);
		manager.close();
		return listaEntidade;
	}
	
	public List<T> listarPorObjetoFiltro(EntityManager manager, T entidadeFiltro, String ordemPesquisa, String tipoOrdenacao, Integer numeroMaximoRegistro) throws Exception
	{
		List<SearchParameter> listaParametroPesquisa = new ArrayList<SearchParameter>();
		
		List<Field> listaAtributoPersistente = UtilAnotationIGN.recuperarListaAtributoPersistente(entidadeFiltro);
		
		for (Field campoPersistente: listaAtributoPersistente)
		{
			SearchParameter parametroPesquisa = UtilAnotationIGN.criarParametroPesquisa(campoPersistente, entidadeFiltro);
			
			if (parametroPesquisa != null)
			{
				listaParametroPesquisa.add(parametroPesquisa);
			}
		}
		
		return listarGenerico(manager, listaParametroPesquisa, ordemPesquisa, tipoOrdenacao, numeroMaximoRegistro);
	}

	
	
	public List<T> listarGenerico(List<SearchParameter> listaParametroPesquisa)
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		List<T> listaEntidade = listarGenerico(manager, listaParametroPesquisa, null, null, null);
		manager.close();
		return listaEntidade;
	}

	public List<T> listarGenerico(List<SearchParameter> listaParametroPesquisa, String ordemPesquisa, String tipoOrdenacao)
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		List<T> listaEntidade = listarGenerico(manager, listaParametroPesquisa, ordemPesquisa, tipoOrdenacao, null);
		manager.close();
		return listaEntidade;
	}

	public List<T> listarGenerico(List<SearchParameter> listaParametroPesquisa, String ordemPesquisa, String tipoOrdenacao, Integer numeroMaximoRegistro)
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		List<T> listaEntidade = listarGenerico(manager, listaParametroPesquisa, ordemPesquisa, tipoOrdenacao, numeroMaximoRegistro);
		manager.close();
		return listaEntidade;
	}

	public List<T> listarGenerico(EntityManager manager, List<SearchParameter> listaParametroPesquisa, String ordemPesquisa, String tipoOrdenacao, Integer numeroMaximoRegistro)
	{
		StringBuffer sql = new StringBuffer();
		List<String> listaCriterio = new ArrayList<String>();
		final Map<String, Object> listaParametro = new HashMap<String, Object>();
		sql.append("from " + getT().getSimpleName() + " ");

		if (listaParametroPesquisa != null && listaParametroPesquisa.size() > 0)
		{
			for (SearchParameter parametroPesquisa : listaParametroPesquisa)
			{
				if(parametroPesquisa.getOperator().equals("BETWEEN"))
				{
					try
					{
						String attributeNameMin = parametroPesquisa.getAttributeName().split(";")[0];
						String attributeNameMax = parametroPesquisa.getAttributeName().split(";")[1];
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						
						String attributeValueMin = ((String) parametroPesquisa.getValue()).split(";")[0];
						String attributeValueMax = ((String) parametroPesquisa.getValue()).split(";")[1];
						
						listaCriterio.add(attributeNameMin.replace("Min", "") + " " + parametroPesquisa.getOperator() + " :" + attributeNameMin + " AND :" + attributeNameMax + " ");
						listaParametro.put(attributeNameMin, simpleDateFormat.parse(attributeValueMin));
						listaParametro.put(attributeNameMax, simpleDateFormat.parse(attributeValueMax));
					}
					catch(Exception parseEx)
					{
						//SUPRESS
					}
				}
				else
				{
					listaCriterio.add(parametroPesquisa.getAttributeName() + " " + parametroPesquisa.getOperator() + ":" + parametroPesquisa.getAttributeName().replaceAll("\\.", "_"));
					listaParametro.put(parametroPesquisa.getAttributeName().replaceAll("\\.", "_"), parametroPesquisa.getValue());
				}
			}

			for (int i = 0; i < listaCriterio.size(); i++)
			{
				if (i == 0)
				{
					sql.append("where " + listaCriterio.get(i) + " ");
				}
				else
				{
					sql.append(" and " + listaCriterio.get(i));
				}
			}

		}

		if (ordemPesquisa != null && !"".equals(ordemPesquisa.trim()) && tipoOrdenacao != null)
		{
			sql.append(" order by " + ordemPesquisa + " " + tipoOrdenacao);
		}

		
		Query query = manager.createQuery(sql.toString());
		setParameters(query, listaParametro);

		if (numeroMaximoRegistro != null)
		{
			query.setMaxResults(numeroMaximoRegistro);
		}

		return (List<T>) query.getResultList();
	}

	public List<T> listarGenerico(EntityManager manager, List<SearchParameter> listaParametroPesquisa, String ordemPesquisa, String tipoOrdenacao, Integer numeroMaximoRegistro, Boolean campoNumerico)
	{
		StringBuffer sql = new StringBuffer();
		List<String> listaCriterio = new ArrayList<String>();
		final Map<String, Object> listaParametro = new HashMap<String, Object>();
		sql.append("from " + getT().getSimpleName() + " ");

		if (listaParametroPesquisa != null && listaParametroPesquisa.size() > 0)
		{
			for (SearchParameter parametroPesquisa : listaParametroPesquisa)
			{
				if(parametroPesquisa.getOperator().equals("BETWEEN"))
				{
					try
					{
						String attributeNameMin = parametroPesquisa.getAttributeName().split(";")[0];
						String attributeNameMax = parametroPesquisa.getAttributeName().split(";")[1];
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						
						String attributeValueMin = ((String) parametroPesquisa.getValue()).split(";")[0];
						String attributeValueMax = ((String) parametroPesquisa.getValue()).split(";")[1];
						
						listaCriterio.add(attributeNameMin.replace("Min", "") + " " + parametroPesquisa.getOperator() + " :" + attributeNameMin + " AND :" + attributeNameMax + " ");
						listaParametro.put(attributeNameMin, simpleDateFormat.parse(attributeValueMin));
						listaParametro.put(attributeNameMax, simpleDateFormat.parse(attributeValueMax));
					}
					catch(Exception parseEx)
					{
						//SUPRESS
					}
				}
				else
				{
					listaCriterio.add(parametroPesquisa.getAttributeName() + " " + parametroPesquisa.getOperator() + ":" + parametroPesquisa.getAttributeName().replaceAll("\\.", "_"));
					listaParametro.put(parametroPesquisa.getAttributeName().replaceAll("\\.", "_"), parametroPesquisa.getValue());
				}
			}

			for (int i = 0; i < listaCriterio.size(); i++)
			{
				if (i == 0)
				{
					sql.append("where " + listaCriterio.get(i) + " ");
				}
				else
				{
					sql.append(" and " + listaCriterio.get(i));
				}
			}

		}

		if (ordemPesquisa != null && !"".equals(ordemPesquisa.trim()) && tipoOrdenacao != null)
		{
			if(campoNumerico == true)
			{
				sql.append(" order by " + ordemPesquisa + "+0" + " " + tipoOrdenacao);
			}
			sql.append(" order by " + ordemPesquisa + " " + tipoOrdenacao);
		}

		
		Query query = manager.createQuery(sql.toString());
		setParameters(query, listaParametro);

		if (numeroMaximoRegistro != null)
		{
			query.setMaxResults(numeroMaximoRegistro);
		}

		return (List<T>) query.getResultList();
	}

	public Page<T> getPage(int tamanhoPagina, int numeroPagina, StringBuffer sql, final Map<String, Object> listaParametro, EntityManager manager)
	{
		Query query = manager.createQuery(sql.toString());
		query.setFirstResult((numeroPagina - 1) * tamanhoPagina);
		query.setMaxResults(tamanhoPagina);
		setParameters(query, listaParametro);
		Page pagina = new Page();
		pagina.setPageSize(tamanhoPagina);
		pagina.setPageNumber(numeroPagina);
		pagina.setRecordList((List) query.getResultList());
		Query queryCount = manager.createQuery("select count(*) " + sql.toString());
		setParameters(queryCount, listaParametro);
		pagina.setTotalRecords((Long) queryCount.getSingleResult());
		return pagina;
	}

	public void setParameters(Query query, Map parameters)
	{
		if (query != null && parameters != null)
		{
			for (Iterator<java.util.Map.Entry> iterator = parameters.entrySet().iterator(); iterator.hasNext();)
			{
				java.util.Map.Entry entry = (java.util.Map.Entry) iterator.next();
				query.setParameter((String) entry.getKey(), entry.getValue());
			}

		}
	}
	
	public Page<T> listarPaginadoSqlLivreGenerico(EntityManager manager, int tamanhoPagina, int numeroPagina, String where, String ordemPesquisa, String tipoOrdenacao)
	{
		StringBuffer sql = new StringBuffer();
		List<String> listaCriterio = new ArrayList<String>();
		final Map<String, Object> listaParametro = new HashMap<String, Object>();
		sql.append("from " + getT().getSimpleName() + " ");

		sql.append(" where " + where);

		if (ordemPesquisa != null && !"".equals(ordemPesquisa.trim()) && tipoOrdenacao != null)
		{
			sql.append(" order by " + ordemPesquisa + " " + tipoOrdenacao);
		}

		return getPage(tamanhoPagina, numeroPagina, sql, listaParametro, manager);
	}	
	

	public Page<T> listarPaginadoSqlLivreGenerico(int tamanhoPagina, int numeroPagina, String where)
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		Page<T> page = listarPaginadoSqlLivreGenerico(manager, tamanhoPagina, numeroPagina, where, null, null);
		manager.close();
		return page;
	}

	public Page<T> listarPaginadoSqlLivreGenerico(int tamanhoPagina, int numeroPagina, String where, String ordemPesquisa, String tipoOrdenacao)
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		Page<T> page = listarPaginadoSqlLivreGenerico(manager, tamanhoPagina, numeroPagina, where, ordemPesquisa, tipoOrdenacao);
		manager.close();
		return page;
	}
		
}
