package br.com.desenv.padraoignorante.conexao;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import br.com.desenv.nepalign.util.DsvConstante;

import com.kscbrasil.lib.database.ConstanteDataBase;

public class ConexaoUtil
{
	private static EntityManagerFactory factory = null;
	private static HashMap<Integer, EntityManagerFactory> factorys = new HashMap<Integer, EntityManagerFactory>();
	
	private static Logger hibernateLogger = Logger.getLogger("org.hibernate");
	private static boolean supressUpdateGlobal = true;

	private static Properties getProperties(final String nomeGrupo, Integer idEmpresaFisica)
	{
		EntityManagerFactory defaultFactory = Persistence.createEntityManagerFactory("padraoNepalIgnPersistenceUnit");
		
		Properties properties = new Properties();
		properties.put("hibernate.hbm2ddl.auto", "update");
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		properties.put("javax.persistence.jdbc.driver", "com.mysql.jdbc.Driver");
		
		properties.put("hibernate.show_sql", "false");
		properties.put("hibernate.format_sql", "true");
		properties.put("hibernate.use_sql_comments", "false");
		properties.put("hibernate.current_session_context_class", "thread");
		
		String propertieName = nomeGrupo != null ? 
					"javax.persistence.jdbc.url.".concat(nomeGrupo.toLowerCase()).concat(".loja".concat(String.format("%02d", idEmpresaFisica))) :
					"javax.persistence.jdbc.url.loja".concat(String.format("%02d", idEmpresaFisica));
		
		properties.put("javax.persistence.jdbc.url", defaultFactory.getProperties().get(propertieName));
		properties.put("javax.persistence.jdbc.user", defaultFactory.getProperties().get("javax.persistence.jdbc.user"));
		properties.put("javax.persistence.jdbc.password", defaultFactory.getProperties().get("javax.persistence.jdbc.password"));
		
		Logger.getRootLogger().info("Using URL from ".concat(propertieName).concat("; URL: ").concat(defaultFactory.getProperties().get(propertieName).toString()));
		
		return properties;
	}
	
	public static EntityManagerFactory getFactory(Integer idEmpresaFisica)
	{
		if(factorys.containsKey(idEmpresaFisica.intValue()))
			return factorys.get(idEmpresaFisica.intValue());
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("padraoNepalIgnPersistenceUnit", getProperties(System.getenv("NOME_GRUPO"), idEmpresaFisica));
		
		factorys.put(idEmpresaFisica.intValue(), factory);
		
		return factory;
	}
	
	public static EntityManagerFactory getFactory(final String username, final String password)
	{
		Properties properties = new Properties();
		
		for(final Entry<String,Object> key : Persistence.createEntityManagerFactory("padraoNepalIgnPersistenceUnit").getProperties().entrySet())
			properties.put(key.getKey(), key.getValue());
		
		properties.put("javax.persistence.jdbc.user", username);
		properties.put("javax.persistence.jdbc.password", password);
		properties.put("hibernate.connection.username", username);
		properties.put("hibernate.connection.password", password);
		
	
		return Persistence.createEntityManagerFactory("padraoNepalIgnPersistenceUnit", properties);
	}

	public static EntityManagerFactory getFactory()
	{
		hibernateLogger.setLevel(Level.ERROR);
		
		if (factory == null)
		{
			// não utilizado !DsvConstante.isLoja() para não causar stack overflow caso o System.getenv retorne null 
			if(System.getenv("LOJA")==null || System.getenv("LOJA").equals("") || System.getenv("LOJA").equals("ESC"))
				factory = Persistence.createEntityManagerFactory("padraoNepalIgnPersistenceUnit");
			else
				factory = Persistence.createEntityManagerFactory("padraoNepalIgnPersistenceUnit", getProperties(System.getenv("NOME_GRUPO"), Integer.parseInt(System.getenv("LOJA"))));
		}
	
		try
		{
			if(ConstanteDataBase.DRIVER == null)
			{
				ConstanteDataBase.setDatabaseProperties(factory.getProperties().get("javax.persistence.jdbc.url").toString(), 
						factory.getProperties().get("javax.persistence.jdbc.user").toString(), factory.getProperties().get("javax.persistence.jdbc.password").toString(), factory.getProperties().get("javax.persistence.jdbc.driver").toString());
			}
		}
		catch(Exception e)
		{
			Logger.getRootLogger().warn("Não foi possível configurar as propriedades de conexão com o banco de dados da DsvLib", e);
		}	
		

		try 
		{
			if(!supressUpdateGlobal)
			{
				Class.forName(factory.getProperties().get("javax.persistence.jdbc.driver").toString());
				Connection conexao = DriverManager.getConnection(factory.getProperties().get("javax.persistence.jdbc.url").toString(),factory.getProperties().get("javax.persistence.jdbc.user").toString(),factory.getProperties().get("javax.persistence.jdbc.password").toString());
				
				String globalShow = "show variables where Variable_name = 'log_bin_trust_function_creators';";
				
				ResultSet resultSetGlobal = null;

				try
				{
					resultSetGlobal = conexao.createStatement().executeQuery(globalShow);	
				}
				catch(Exception ex)
				{
					Logger.getRootLogger().warn("Não foi possível recuperar as informações sobre a GLOBAL log_bin_trust_function_creators...");
					Logger.getRootLogger().warn("Command Query usada : " + globalShow);
					Logger.getRootLogger().warn("Exibindo stacktrace do MySQL", ex);
				}
				
				if(resultSetGlobal == null || !resultSetGlobal.next())
				{
					Logger.getRootLogger().warn("Não foi possível encontrar um valor para a GLOBAL log_bin_trust_function_creators. ResultSet vazio.");
					supressUpdateGlobal = true;	
				}
				else
				{
					String global = resultSetGlobal.getString("Value");
					
					if(global == null)
					{
						Logger.getRootLogger().info("Não foi possível encontrar um valor para a GLOBAL log_bin_trust_function_creators. Coluna Value não foi encontrada. Mostrando colunas disponíveis...");
						
						ResultSetMetaData rsmd = resultSetGlobal.getMetaData();
						
						int columnIndex = 1;
						
						while(columnIndex < rsmd.getColumnCount())
						{
							try
							{
								Logger.getRootLogger().info(rsmd.getTableName(columnIndex));	
							}
							catch(Exception ex)
							{
								//SUPRESS
							}
							columnIndex++;
						}
						Logger.getRootLogger().warn("AJUSTE NECESSÁRIO!!!");	
						supressUpdateGlobal = true;
					}
					else
					{
						if(global.equals("OFF"))
						{
							Logger.getRootLogger().warn("GLOBAL log_bin_trust_function_creators está desativada. Resolvendo...");
							
							String globalUpdate = "SET GLOBAL log_bin_trust_function_creators = 1;";
							
							try
							{
								conexao.createStatement().execute(globalUpdate);
								
								Logger.getRootLogger().info("GLOBAL log_bin_trust_function_creators atualizada!!!");
							}
							catch(Exception ex)
							{
								Logger.getRootLogger().warn("Não foi possível atualizar as informações sobre a GLOBAL log_bin_trust_function_creators...");
								Logger.getRootLogger().warn("Command Query usada : " + globalUpdate);
								Logger.getRootLogger().warn("Exibindo stacktrace do MySQL", ex);
								
								supressUpdateGlobal = true;
							}
						}		
					}
				}
				
				conexao.close();
				conexao = null; 	
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

		return factory;
	}

	public static EntityManager getEntityManager()
	{
		return ConexaoUtil.getFactory().createEntityManager();
	}
	
	public static EntityManager getEntityManager(Integer idEmpresaFisica)
	{
		return ConexaoUtil.getFactory(idEmpresaFisica).createEntityManager();
	}
	
	public static Connection getConexaoPadrao()
	{
		Connection conexao = null;
		try 
		{
			Class.forName(getFactory().getProperties().get("javax.persistence.jdbc.driver").toString());
			conexao = DriverManager.getConnection(getFactory().getProperties().get("javax.persistence.jdbc.url").toString(),getFactory().getProperties().get("javax.persistence.jdbc.user").toString(),getFactory().getProperties().get("javax.persistence.jdbc.password").toString());
			return conexao;
		}
		catch (ClassNotFoundException e) 
		{
			throw new RuntimeException(e);
		}		
		catch (SQLException e) 
		{
			throw new RuntimeException(e);
		}
	}

	public static Connection getConexaoPadrao(Integer idEmpresaFisica)
	{
		Connection conexao = null;
		try 
		{
			Class.forName(getFactory().getProperties().get("javax.persistence.jdbc.driver").toString());
			conexao = DriverManager.getConnection(getFactory().getProperties().get("javax.persistence.jdbc.url.loja".concat(String.format("%02d", idEmpresaFisica))).toString(),getFactory().getProperties().get("javax.persistence.jdbc.user").toString(),getFactory().getProperties().get("javax.persistence.jdbc.password").toString());
			return conexao;
		}
		catch (ClassNotFoundException e) 
		{
			throw new RuntimeException(e);
		}		
		catch (SQLException e) 
		{
			throw new RuntimeException(e);
		}
	}
	
	public static Connection getConexaoPadrao(final String username, final String password)
	{
		Connection conexao = null;
		try 
		{
			Class.forName(getFactory().getProperties().get("javax.persistence.jdbc.driver").toString());
			conexao = DriverManager.getConnection(getFactory().getProperties().get("javax.persistence.jdbc.url").toString(), username, password);
			return conexao;
		}
		catch (ClassNotFoundException e) 
		{
			throw new RuntimeException(e);
		}		
		catch (SQLException e) 
		{
			throw new RuntimeException(e);
		}
	}
	
	public static Connection getConexaoPadraoCACHE() throws Exception
	{
		Connection conCache = null;

		try 
		{
			Class.forName("com.intersys.jdbc.CacheDriver");
			conCache = DriverManager.getConnection(DsvConstante.getParametrosSistema().get("strConexaoCache"), DsvConstante.getParametrosSistema().get("usuarioCache"), DsvConstante.getParametrosSistema().get("senhaCache"));
	 
			return conCache;
		} 
		catch (Exception chEx) 
		{
			chEx.printStackTrace();
			throw new Exception("Falha na conexão com o caché." + chEx.getMessage());
		}
	}
	
	public static Connection getConnection(final String url, final String username, final String password)
	{
		Connection conexao = null;
		try 
		{
			Class.forName(getFactory().getProperties().get("javax.persistence.jdbc.driver").toString());
			conexao = DriverManager.getConnection(url, username, password);
			return conexao;
		}
		catch (ClassNotFoundException e) 
		{
			throw new RuntimeException(e);
		}		
		catch (SQLException e) 
		{
			throw new RuntimeException(e);
		}
	}
}