import java.io.File;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;


import br.com.desenv.frameworkignorante.GenericServiceIGN;
import br.com.desenv.nepalign.model.Acao;
import br.com.desenv.nepalign.model.Acaogrupo;
import br.com.desenv.nepalign.model.Funcionalidade;
import br.com.desenv.nepalign.model.Grupo;
import br.com.desenv.nepalign.model.Modulo;
import br.com.desenv.nepalign.service.AcaoService;
import br.com.desenv.nepalign.service.AcaogrupoService;
import br.com.desenv.nepalign.service.CidadeService;
import br.com.desenv.nepalign.service.FuncionalidadeService;


public class GeraSeguranca 
{

	public static void main(String[] args) 
	{
		try
		{

			/*Class classe = CidadeService.class;
			Method[] metodos = classe.getMethods();
			for (Method method : metodos) 
			{
				System.out.println(method.getName());
			}*/
			new GeraSeguranca().init();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}
	
	private void init() throws Exception
	{
		Set<String> metodosBasicos = new TreeSet<String>();
		Set<String> metodosRestritos = new TreeSet<String>();
		Set<String> metodosServico = new TreeSet<String>();
		try
		{
			//recuperar os métodos basicos
			Class classe = GenericServiceIGN.class;
			Method[] metodosGeneric = classe.getDeclaredMethods();
			for (Method method : metodosGeneric) 
			{
				if (method.getName().toUpperCase().contains("GETP".toUpperCase()) || method.getName().toUpperCase().contains("GETPERSISTENCE".toUpperCase()) || 
						method.getName().toUpperCase().contains("GETT".toUpperCase()))
					System.out.println("Função desconsiderada:" + method.getName());
				else if (method.getName().toLowerCase().contains("salvar") || method.getName().toLowerCase().contains("atualizar") || method.getName().toLowerCase().contains("excluir"))
					metodosRestritos.add(method.getName().toUpperCase());
				else
					metodosBasicos.add(method.getName().toUpperCase());
			}
			System.out.println("Basicos");
			for (String metodoTemp : metodosBasicos) 
			{
				System.out.println(metodoTemp);
			}
			System.out.println("");
			System.out.println("Restritos");
			for (String metodoTemp : metodosRestritos) 
			{
				System.out.println(metodoTemp);
			}			
			
			
			File arquivos = new File("C:/Desenv/Workspace/nepalign/src/br/com/desenv/nepalign/service");
			
			String[] services = arquivos.list();
			String arquivo;
			ClasseMetodo classeMetodo;
			ClasseMetodo classeMetodoPublica;
			ArrayList<ClasseMetodo> classesSeguranca = new ArrayList<ClasseMetodo>();
			ArrayList<ClasseMetodo> classesSegurancaPublica = new ArrayList<ClasseMetodo>();
			
			for (int f=0;f<services.length;f++)
			{
				arquivo = services[f].replace(".java", "");
				System.out.println("");
				System.out.println(arquivo);
				Class serviceClass = Class.forName("br.com.desenv.nepalign.service."+arquivo);
				classeMetodo = new ClasseMetodo();
				classeMetodo.nomeClasse=arquivo;
				classeMetodoPublica = new ClasseMetodo();
				classeMetodoPublica.nomeClasse=arquivo;
				
				Method[] metodosService = serviceClass.getDeclaredMethods();
				metodosServico = new TreeSet<>();
				for (Method method : metodosService) 
				{
					int modifiers = method.getModifiers();
					if (Modifier.isPublic(modifiers)==true && Modifier.isStatic(modifiers)==false)
					{
						metodosServico.add(method.getName().toUpperCase());
					}
				}
				metodosServico.addAll(metodosBasicos);
				metodosServico.addAll(metodosRestritos);
				//esse aqui é pra criar o cadastro de acao
				classeMetodo.listaMetodos = metodosServico;
				classesSeguranca.add(classeMetodo);
				
				//esse aqui é pra criar o cadastro de GrupoAcao
				//onde o grupo vai ser o Generico e a acao vai ser recuperada do cadastro de acao
				classeMetodoPublica.listaMetodos = metodosBasicos;
				classesSegurancaPublica.add(classeMetodoPublica);
			}
			
			//criarFuncionalidadesAcoes(classesSeguranca);
			criaGrupoPublico(classesSegurancaPublica);
			criaGrupoAdministrador(classesSeguranca);
			
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
	
	public void criarFuncionalidadesAcoes(ArrayList<ClasseMetodo> classesMetodos) throws Exception
	{
		FuncionalidadeService fService = new FuncionalidadeService();
		AcaoService aService = new AcaoService();
		Acao acao;
		Funcionalidade func;
		Modulo modulo = new Modulo();
		modulo.setId(1);
		
		for (ClasseMetodo classeMetodo : classesMetodos) 
		{
			//criar funcionalidade
			func = new Funcionalidade();
			func.setDescricao(classeMetodo.nomeClasse);
			func.setModulo(modulo);
			func.setNomeServico(classeMetodo.nomeClasse);
			func.setSituacao("A");
			func = fService.salvar(func);
			
			Set<String> metodos = classeMetodo.listaMetodos;
			
			for (String nomeMetodo : metodos)
			{
				acao = new Acao();
				acao.setDescricao(nomeMetodo+" "+func.getNomeServico().replace("SERVICE", ""));
				acao.setFuncionalidade(func);
				acao.setLogarAcao("S");
				acao.setNomeMetodo(nomeMetodo);
				acao.setSituacao("A");
				aService.salvar(acao);
			}
		}
		
	}
	
	public void criaGrupoPublico(ArrayList<ClasseMetodo> classesPublicas) throws Exception
	{
		FuncionalidadeService fService = new FuncionalidadeService();
		AcaoService aService = new AcaoService();
		Acao acao;
		Funcionalidade func;
		Modulo modulo = new Modulo();
		modulo.setId(1);
		Acaogrupo acaoGrupo;
		Grupo grupo = new Grupo();
		grupo.setId(3);
		AcaogrupoService agService = new AcaogrupoService();
		
		for (ClasseMetodo classeMetodo : classesPublicas) 
		{
			//criar funcionalidade
			func = new Funcionalidade();
			func.setNomeServico(classeMetodo.nomeClasse);
			func = fService.listarPorObjetoFiltro(func).get(0);
			
			Set<String> metodos = classeMetodo.listaMetodos;
			
			for (String nomeMetodo : metodos)
			{
				acao = new Acao();
				acao.setNomeMetodo(nomeMetodo);
				acao.setFuncionalidade(func);
				acao = aService.listarPorObjetoFiltro(acao).get(0);
				acaoGrupo = new Acaogrupo();
				acaoGrupo.setAcao(acao);
				acaoGrupo.setGrupo(grupo);
				agService.salvar(acaoGrupo);
			}
		}
		
	} 
	public void criaGrupoAdministrador(ArrayList<ClasseMetodo> classesPublicas) throws Exception
	{
		FuncionalidadeService fService = new FuncionalidadeService();
		AcaoService aService = new AcaoService();
		Acao acao;
		Funcionalidade func;
		Modulo modulo = new Modulo();
		modulo.setId(1);
		Acaogrupo acaoGrupo;
		Grupo grupo = new Grupo();
		grupo.setId(1);
		AcaogrupoService agService = new AcaogrupoService();
		
		for (ClasseMetodo classeMetodo : classesPublicas) 
		{
			//criar funcionalidade
			func = new Funcionalidade();
			func.setNomeServico(classeMetodo.nomeClasse);
			func = fService.listarPorObjetoFiltro(func).get(0);
			
			Set<String> metodos = classeMetodo.listaMetodos;
			
			for (String nomeMetodo : metodos)
			{
				acao = new Acao();
				acao.setNomeMetodo(nomeMetodo);
				acao.setFuncionalidade(func);
				acao = aService.listarPorObjetoFiltro(acao).get(0);
				acaoGrupo = new Acaogrupo();
				acaoGrupo.setAcao(acao);
				acaoGrupo.setGrupo(grupo);
				agService.salvar(acaoGrupo);
			}
		}
		
	}	
	
	private class ClasseMetodo
	{
		public String nomeClasse;
		public Set<String> listaMetodos;
	}	
	
}

