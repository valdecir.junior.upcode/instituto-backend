import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.Cliente;
import br.com.desenv.nepalign.model.Duplicata;
import br.com.desenv.nepalign.model.DuplicataParcela;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.service.DuplicataParcelaService;
import br.com.desenv.nepalign.util.DsvConstante;
import br.com.desenv.nepalign.util.DsvImpressoraMatricial;
import br.com.desenv.nepalign.util.DsvNumeroExtenso;




public class ImpressaoDuplicataMatricial {

	public static void main(String[] args) {
		DsvImpressoraMatricial impressora = null;
		try {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			List<DuplicataParcela> lista = montarParcelas();
			Duplicata duplicata = lista.get(0).getDuplicata();
			EmpresaFisica empresa = duplicata.getEmpresaFisica();
			impressora = new DsvImpressoraMatricial("LPT1:", false); 

			if (impressora.inicializar()) {
				impressora.setCharacterSet(DsvImpressoraMatricial.BRAZIL);
				impressora.seleciona15CPI();
				impressora.modoProporcional(true);
				//Imprimir dados Empresa
				impressora.avancoHorizontal(new Float(11.4));
				String razaoSocial = empresa.getRazaoSocial().toUpperCase();
				impressora.print(razaoSocial);
				impressora.println(empresa.getEndereco().toUpperCase()+","+empresa.getNumero(),new Float(11.4));
				impressora.println(empresa.getCidade().getNome().toUpperCase()+" - "+empresa.getCidade().getUf().getSigla().toUpperCase()+" FONE:"+empresa.getTelefone(),new Float(11.4));
				impressora.println("C.G.C: "+IgnUtil.fomatadorCpfCnpj(empresa.getCnpj()),new Float(11.4));
				impressora.println("INSCRIÇÃO: "+empresa.getInscricaoEstadual(),new Float(11.4));
				impressora.println("EMISSAO: "+df.format(duplicata.getDataCompra()),new Float(11.4));

				//Imprimir dados Duplicata
				impressora.avancoVertical(new Float(2.1));
				impressora.selecionaPosicaoAbsolutaHorizontal(new Float(0.0));
				//Valor Fatura
				impressora.print(new BigDecimal(duplicata.getValorTotalCompra()).setScale(2, BigDecimal.ROUND_HALF_EVEN).toString(),new Float(0.5));
				//Numero Fatura
				impressora.print(duplicata.getNumeroFatura().toString(),new Float(2.5));
				//Valor Parcela
				impressora.print(new BigDecimal(duplicata.getValorTotalCompra()).setScale(2, BigDecimal.ROUND_HALF_EVEN).toString(),new Float(2.5));
				//Numero Duplicata
				impressora.print(duplicata.getNumeroDuplicata().toString(),new Float(2));
				//Data Primeiro Vencimento
				impressora.print(df.format(duplicata.getDataVencimentoPrimeiraPrestacao()),new Float(2));

				//Imprimir Primeira Frase, frase fica nos parametro de sistema "PRIMEIRA_FRASE_DUPLICATA_MATRICIAL" as linhas são divididas pelo ";"
				String parametroFrase = DsvConstante.getParametrosSistema().get("PRIMEIRA_FRASE_DUPLICATA_MATRICIAL")==null
						?""
						:DsvConstante.getParametrosSistema().get("PRIMEIRA_FRASE_DUPLICATA_MATRICIAL");
				
				impressora.avancoVertical(new Float(1));
				impressora.selecionaPosicaoAbsolutaHorizontal(new Float(4));
				if(parametroFrase.equals(""))
				{
					impressora.print("");
				}
				else
				{
					String[] linhas = parametroFrase.split(";");
					for (int i = 0; i < linhas.length; i++) 
					{
						if(i==0){
							impressora.print(linhas[i]);
						}
						else
						{
							impressora.println(linhas[i], new Float(4));
						}
					}
				}

				///Imprimir dados Cliente
				Cliente cliente = duplicata.getCliente();
				impressora.avancoVertical(new Float(0.75));
				impressora.selecionaPosicaoAbsolutaHorizontal(new Float(0));
				impressora.print(cliente.getNome(),new Float(5.5));//NOME
				impressora.selecionaPosicaoAbsolutaHorizontal(new Float(0));
				impressora.print(cliente.getId().toString(),new Float(18));//ID
				impressora.println(cliente.getLogradouro()+","+cliente.getNumeroEndereco(),new Float(5.5));//ENDERECO
				impressora.println(cliente.getBairro(),new Float(5.5));//BAIRRO
				impressora.println(cliente.getCidadeEndereco().getNome(),new Float(5.5));//CIDADE
				impressora.seleciona10CPI();
				impressora.selecionaPosicaoAbsolutaHorizontal(new Float(0));
				impressora.print(cliente.getCep(),new Float(15.7));//CEP
				impressora.seleciona15CPI();
				impressora.print(cliente.getCidadeEndereco().getUf().getSigla(),new Float(1));//UF CLIENTE
				if(cliente.getCidadeComercial()!=null){
					impressora.println(cliente.getCidadeComercial().getNome(),new Float(5.5));//COBRANCA CIDADE
					impressora.selecionaPosicaoAbsolutaHorizontal(new Float(0));
					impressora.print(cliente.getCepComercial(),new Float(17.5));//CEP COBRANCA
				}
				else
				{
					impressora.print("");
				}
				impressora.println(IgnUtil.fomatadorCpfCnpj(cliente.getCpfCnpj()),new Float(5.5));//CPF CNPJ
				impressora.print(cliente.getRgInscricaoEstadual(),new Float(6.4));//RG

				//Imprimi valor por extenso
				boolean parcelasMaiorQueCinco = lista.size()>5; 
				String extenso = new DsvNumeroExtenso().valorDinheiroPorExtenso(58723.99).toUpperCase();
				impressora.avancoVertical(new Float(1));
				Float quantoAvancarParaParcela = 2.1f;
				if(extenso.length()>59)
				{
					ArrayList<String> linhas = IgnUtil.quebrarEmLinhas(extenso, 59);
					for (int i = 0; i < linhas.size(); i++)
					{
						quantoAvancarParaParcela -= 0.7f;
						String linha = linhas.get(i);
						impressora.selecionaPosicaoAbsolutaHorizontal(new Float(0));
						if(i==0)
							impressora.print(linha, new Float(5.3));
						else
							impressora.println(linha, new Float(5.3));
					}
					if(linhas.size()>1){
						if(!parcelasMaiorQueCinco)
							quantoAvancarParaParcela = 1.45f;
						else
							quantoAvancarParaParcela = 1.27f;
					}
				}
				else
				{
					if(!parcelasMaiorQueCinco)
						quantoAvancarParaParcela = 2.1f;
					else
						quantoAvancarParaParcela = 1.9f;	

					impressora.selecionaPosicaoAbsolutaHorizontal(new Float(0));
					impressora.print(extenso, new Float(5.3));
				}
				//Imprimir dados parcelas
				if(!parcelasMaiorQueCinco)
				{
					impressora.selecionaPosicaoAbsolutaHorizontal(new Float(0));
					impressora.avancoVertical(new Float(quantoAvancarParaParcela));
					for (int i =0;i<lista.size();i++) 
					{
						DuplicataParcela parcela = lista.get(i);
						String numeroParcela  = (parcela.getNumeroParcela().toString().length()<=1?"0"+parcela.getNumeroParcela():parcela.getNumeroParcela().toString());
						impressora.print(numeroParcela);//NUMERO PARCELA
						//VALOR PARCELA
						impressora.selecionaPosicaoAbsolutaHorizontal(new Float(0));
						impressora.print(new BigDecimal(parcela.getValorParcela()).setScale(2, BigDecimal.ROUND_HALF_EVEN).toString(),new Float(1.5));

						impressora.selecionaPosicaoAbsolutaHorizontal(new Float(0));
						impressora.print(df.format(parcela.getDataVencimento()),new Float(4.0));//DATA VENCIMENTO
						if(parcela.getNumeroParcela().intValue()==0)
						{
							impressora.selecionaPosicaoAbsolutaHorizontal(new Float(0));
							impressora.print("ENTRADA", new Float(6.5));//ENTRADA
						}
						if(i==0)
						{
							impressora.selecionaPosicaoAbsolutaHorizontal(new Float(0));
							impressora.print(razaoSocial,new Float(12));// RAZAO NA ASSINATURA
						}
						impressora.proximaLinha();
					}	
				}
				else // quando tem mais de 5 parcelas
				{
					impressora.selecionaPosicaoAbsolutaHorizontal(new Float(0));
					impressora.modoProporcional(false);
					impressora.avancoVertical(quantoAvancarParaParcela);
					impressora.seleciona10CPI();
					for (int i =0;i<lista.size();i++) 
					{
						DuplicataParcela parcela = lista.get(i);
						String numeroParcela  = (parcela.getNumeroParcela().toString().length()<=1?"0"+parcela.getNumeroParcela():parcela.getNumeroParcela().toString());
						impressora.print(numeroParcela);//NUMERO PARCELA
						//VALOR PARCELA
						impressora.print(new BigDecimal(parcela.getValorParcela()).setScale(2, BigDecimal.ROUND_HALF_EVEN).toString(),new Float(1));

						impressora.print(df.format(parcela.getDataVencimento()),new Float(1));//DATA VENCIMENTO
						if(parcela.getNumeroParcela().intValue()==0)
						{
							impressora.print("ENTRADA", new Float(1.3));//ENTRADA
						}
						if(i==1)
						{
							impressora.selecionaPosicaoAbsolutaHorizontal(new Float(0));
							impressora.modoProporcional(true);
							impressora.print(razaoSocial,new Float(12));// RAZAO NA ASSINATURA
							impressora.modoProporcional(false);
						}
						impressora.avancoVertical(new Float(0.3));
						impressora.selecionaPosicaoAbsolutaHorizontal(new Float(0));
					}	
				}


				impressora.terminaPagina(); //termina pagina
				impressora.close(); //finaliza  o processo de impressão
			}
			else 
				System.out.println("Falha ao se conectar a impressora!s");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		finally{
			if(impressora!=null)
				impressora.resetaConfiguracoesPadrao();
		}

	}

	public static List<DuplicataParcela> montarParcelas() throws Exception
	{
		DuplicataParcela dpc = new DuplicataParcela();
		dpc.setNumeroDuplicata(new Long(9633));
		return new DuplicataParcelaService().listarPorObjetoFiltro(dpc);

	}
}