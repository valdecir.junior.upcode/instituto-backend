import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;


public class GerarPlanos {

	public static void gerarPlanoGrupoEmpresa1(List<EmpresaFisica> lista) throws SQLException
	{
		EntityManager manager = null;
		EntityTransaction transaction = null;
		Boolean transacaoIndependente = false;
		   try
		   {
			   if(manager == null || transaction == null)
			   {
				   manager = ConexaoUtil.getEntityManager();
				   transaction = manager.getTransaction();
				   transaction.begin();
				   transacaoIndependente = true;
			   }
		for (int i = 0; i < lista.size(); i++)
		{
			EmpresaFisica empresa = lista.get(i);
			
			String str = "";
			
			str = str + " INSERT INTO `planopagamento` VALUES "; 
			str = str + " (null,1,'A VISTA','-',10,NULL,1,NULL,NULL,"+empresa.getId()+",'N','A Vista','1'), "; 
			str = str + " (null,4,'CRED (1 A 15 DIAS) ','-',10,NULL,10,NULL,NULL,"+empresa.getId()+",'S','A Prazo','2'), "; 
			str = str + " (null,4,'CRED (1+1)','-',10,NULL,10,NULL,NULL,"+empresa.getId()+",'S','A Prazo','3'), "; 
			str = str + " (null,4,'CRED 30 DIAS','-',10,NULL,10,NULL,NULL,"+empresa.getId()+",'S','A Prazo','4'), "; 
			str = str + " (null,4,'CRED 30/60 OU (45DIAS)','-',10,NULL,10,NULL,NULL,"+empresa.getId()+",'S','A Prazo','5'), "; 
			str = str + " (null,4,'CRED 30/60/90 OU (1+4)','-',10,NULL,8,NULL,NULL,"+empresa.getId()+",'S','A Prazo','6'), "; 
			str = str + " (null,4,'CRED 30/60/90/120 OU (1+5)','-',10,NULL,5,NULL,NULL,"+empresa.getId()+",'S','A Prazo','7'), "; 
			str = str + " (null,4,'CRED 30/60/90/120/150','-',4,NULL,5,NULL,NULL,"+empresa.getId()+",'S','A Prazo','8'), "; 
			str = str + " (null,4,'CRED 06 VEZES','-',2,NULL,2,NULL,NULL,"+empresa.getId()+",'S','A Prazo','9'), "; 
			str = str + " (null,4,'CRED 07 VEZES','-',0,NULL,0,NULL,NULL,"+empresa.getId()+",'S','A Prazo','10'), "; 
			str = str + " (null,8,'CH (1+1)','-',10,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cheque','11'), "; 
			str = str + " (null,8,'CH (1+2)','-',10,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cheque','12'), "; 
			str = str + " (null,8,'CH (1 A 15)','-',10,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cheque','13'), "; 
			str = str + " (null,8,'CH 30 DIAS','-',10,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cheque','14'), "; 
			str = str + " (null,8,'CH 30/60 OU (45 DIAS)','-',10,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cheque','15'), "; 
			str = str + " (null,8,'CH 60 DIAS','-',10,NULL,1,NULL,NULL,"+empresa.getId()+",'N','Cheque','16'), "; 
			str = str + " (null,8,'CH 30/60/90','-',10,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cheque','17'), "; 
			str = str + " (null,8,'CH 90 DIAS','-',10,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cheque','18'), "; 
			str = str + " (null,8,'CH 30/60/90/120 OU (75 DIAS)','-',8,NULL,8,NULL,NULL,"+empresa.getId()+",'N','Cheque','19'), "; 
			str = str + " (null,8,'CH 120 DIAS','-',8,NULL,8,NULL,NULL,"+empresa.getId()+",'N','Cheque','20'), "; 
			str = str + " (null,8,'CH 150 DIAS','-',6,NULL,6,NULL,NULL,"+empresa.getId()+",'N','Cheque','21'), "; 
			str = str + " (null,8,'CH 210 DIAS','-',0,NULL,0,NULL,NULL,"+empresa.getId()+",'N','Cheque','22'), "; 
			str = str + " (null,3,'CARTÃO VISA 5X','-',10,NULL,5,NULL,NULL,"+empresa.getId()+",'N','Cartao','23'), "; 
			str = str + " (null,5,'VALE CALCADO OU CONVENIO','-',10,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Vale Calcado','24'), "; 
			str = str + " (null,8,'CH 180 DIAS','-',0,NULL,6,NULL,NULL,"+empresa.getId()+",'N','Cheque','25'), "; 
			str = str + " (null,2,'CARTAO MASTER 5X','-',10,NULL,5,NULL,NULL,"+empresa.getId()+",'N','Cartao','26'), "; 
			str = str + " (null,3,'CARTAO VISA 10X','-',0,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cartao','27'), "; 
			str = str + " (null,2,'CARTAO MASTER 10X','-',0,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cartao','28'), "; 
			str = str + " (null,8,'CH 10X','-',0,NULL,0,NULL,NULL,"+empresa.getId()+",'N','Cheque','29'), "; 
			str = str + " (null,7,'CARTÃO SICREDI','-',10,NULL,6,NULL,NULL,"+empresa.getId()+",'N','Cartao SICREDI','30'), "; 
			str = str + " (null,7,'CARTAO HIPERCARD','-',10,NULL,0,NULL,NULL,"+empresa.getId()+",'N','Cartao HIPERCARD','31'), "; 
			str = str + " (null,7,'CARTAO SENFF','+',0,NULL,0,NULL,NULL,"+empresa.getId()+",'N','Cartao SENFF','32'), "; 
			str = str + " (null,1,'EM CHEQUE',NULL,NULL,NULL,0,NULL,NULL,"+empresa.getId()+",'N','CHEQUE','33'), "; 
			str = str + " (null,NULL,'PRODUTOS',NULL,NULL,NULL,0,NULL,NULL,"+empresa.getId()+",'N',NULL,'34'), "; 
			str = str + " (null,NULL,'APLICACAO DE CRED.BOLETO',NULL,NULL,NULL,0,NULL,NULL,"+empresa.getId()+",'N',NULL,'35'), "; 
			str = str + " (null,NULL,'ACERTO FORN.',NULL,NULL,NULL,0,NULL,NULL,"+empresa.getId()+",'N',NULL,'36'), "; 
			str = str + " (null,NULL,'DEP. C/C.',NULL,NULL,NULL,0,NULL,NULL,"+empresa.getId()+",'N',NULL,'37'), "; 
			str = str + " (null,NULL,'BOLETO BANCARIO',NULL,NULL,NULL,0,NULL,NULL,"+empresa.getId()+",'N',NULL,'38'); "; 
			str = str + "  "; 
			 Query query = manager.createNativeQuery(str); 
			 query.executeUpdate();
		}
		 if(transacaoIndependente)
			   transaction.commit();
	   }
	   catch(Exception ex)
	   {
		   if(transacaoIndependente)
			   transaction.rollback();
		   
		   
		   ex.printStackTrace();
		   throw ex;
	   }
	   finally
	   {
		   if(transacaoIndependente)
			   manager.close();
	   }
	}
	public static void  gerarPlanoGrupoEmpresa2(List<EmpresaFisica> lista) throws SQLException
	{
		EntityManager manager = null;
		EntityTransaction transaction = null;
		Boolean transacaoIndependente = false;
		   try
		   {
			   if(manager == null || transaction == null)
			   {
				   manager = ConexaoUtil.getEntityManager();
				   transaction = manager.getTransaction();
				   transaction.begin();
				   transacaoIndependente = true;
			   }
		for (int i = 0; i < lista.size(); i++)
		{
			EmpresaFisica empresa = lista.get(i);
			
			String str = "";
			
			str = str + " INSERT INTO `planopagamento` VALUES "; 
			str = str + " (null,1,'A VISTA','-',5,NULL,1,NULL,NULL,"+empresa.getId()+",'N','A Vista','1'), "; 
			str = str + " (null,4,'CRED (1 A 15 DIAS) ','-',5,NULL,10,NULL,NULL,"+empresa.getId()+",'S','A Prazo','2'), "; 
			str = str + " (null,4,'CRED (1+1)','-',5,NULL,10,NULL,NULL,"+empresa.getId()+",'S','A Prazo','3'), "; 
			str = str + " (null,4,'CRED 30 DIAS','-',5,NULL,10,NULL,NULL,"+empresa.getId()+",'S','A Prazo','4'), "; 
			str = str + " (null,4,'CRED 30/60 OU (45DIAS)','-',5,NULL,10,NULL,NULL,"+empresa.getId()+",'S','A Prazo','5'), "; 
			
			str = str + " (null,4,'CRED 30/60/90 OU (1+4)','-',0,NULL,8,NULL,NULL,"+empresa.getId()+",'S','A Prazo','6'), "; 
			str = str + " (null,4,'CRED 30/60/90/120 OU (1+5)','-',0,NULL,5,NULL,NULL,"+empresa.getId()+",'S','A Prazo','7'), "; 
			str = str + " (null,4,'CRED 30/60/90/120/150','-',0,NULL,5,NULL,NULL,"+empresa.getId()+",'S','A Prazo','8'), "; 
			str = str + " (null,4,'CRED 06 VEZES (NAO AUTORIZADO)','-',0,NULL,2,NULL,NULL,"+empresa.getId()+",'S','A Prazo','9'), "; 
			str = str + " (null,4,'CRED 07 VEZES (NAO AUTORIZADO)','-',0,NULL,0,NULL,NULL,"+empresa.getId()+",'S','A Prazo','10'), ";
			
			str = str + " (null,8,'CH (1+1)','-',5,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cheque','11'), "; 
			str = str + " (null,8,'CH (1+2)','-',5,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cheque','12'), "; 
			str = str + " (null,8,'CH (1 A 15)','-',5,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cheque','13'), "; 
			str = str + " (null,8,'CH 30 DIAS','-',5,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cheque','14'), "; 
			str = str + " (null,8,'CH 30/60 OU (45 DIAS)','-',5,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cheque','15'), ";
			
			str = str + " (null,8,'CH 60 DIAS','-',0,NULL,1,NULL,NULL,"+empresa.getId()+",'N','Cheque','16'), "; 
			str = str + " (null,8,'CH 30/60/90','-',0,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cheque','17'), "; 
			str = str + " (null,8,'CH 90 DIAS','-',0,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cheque','18'), "; 
			str = str + " (null,8,'CH 30/60/90/120 OU (75 DIAS)','-',0,NULL,8,NULL,NULL,"+empresa.getId()+",'N','Cheque','19'), "; 
			str = str + " (null,8,'CH 120 DIAS','-',0,NULL,8,NULL,NULL,"+empresa.getId()+",'N','Cheque','20'), "; 
			str = str + " (null,8,'CH 150 DIAS','-',0,NULL,6,NULL,NULL,"+empresa.getId()+",'N','Cheque','21'), "; 
			str = str + " (null,8,'CH 210 DIAS (NAO AUTORIZADO)','-',0,NULL,0,NULL,NULL,"+empresa.getId()+",'N','Cheque','22'), "; 
			
			str = str + " (null,3,'CARTÃO VISA 5X','-',5,NULL,5,NULL,NULL,"+empresa.getId()+",'N','Cartao','23'), "; 
			str = str + " (null,5,'VALE CALCADO OU CONVENIO','-',10,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Vale Calcado','24'), "; 
			str = str + " (null,8,'CH 180 DIAS','-',0,NULL,6,NULL,NULL,"+empresa.getId()+",'N','Cheque','25'), "; 
			str = str + " (null,2,'CARTAO MASTER 5X','-',5,NULL,5,NULL,NULL,"+empresa.getId()+",'N','Cartao','26'), "; 
			
			str = str + " (null,3,'CARTAO VISA 10X','-',0,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cartao','27'), "; 
			str = str + " (null,2,'CARTAO MASTER 10X','-',0,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cartao','28'), "; 
			str = str + " (null,8,'CH 10X (NAO AUTORIZADO)','-',0,NULL,0,NULL,NULL,"+empresa.getId()+",'N','Cheque','29'), "; 
			
			str = str + " (null,7,'CARTÃO SICREDI','-',5,NULL,6,NULL,NULL,"+empresa.getId()+",'N','Cartao SICREDI','30'), "; 
			str = str + " (null,7,'CARTAO HIPERCARD','-',5,NULL,0,NULL,NULL,"+empresa.getId()+",'N','Cartao HIPERCARD','31'), "; 
			str = str + " (null,7,'CARTAO SENFF','-',5,NULL,0,NULL,NULL,"+empresa.getId()+",'N','Cartao SENFF','32'), "; 
			
			str = str + " (null,1,'EM CHEQUE',NULL,NULL,NULL,0,NULL,NULL,"+empresa.getId()+",'N','CHEQUE','33'), "; 
			str = str + " (null,NULL,'PRODUTOS',NULL,NULL,NULL,0,NULL,NULL,"+empresa.getId()+",'N',NULL,'34'), "; 
			str = str + " (null,NULL,'APLICACAO DE CRED.BOLETO',NULL,NULL,NULL,0,NULL,NULL,"+empresa.getId()+",'N',NULL,'35'), "; 
			str = str + " (null,NULL,'ACERTO FORN.',NULL,NULL,NULL,0,NULL,NULL,"+empresa.getId()+",'N',NULL,'36'), "; 
			str = str + " (null,NULL,'DEP. C/C.',NULL,NULL,NULL,0,NULL,NULL,"+empresa.getId()+",'N',NULL,'37'), "; 
			str = str + " (null,NULL,'BOLETO BANCARIO',NULL,NULL,NULL,0,NULL,NULL,"+empresa.getId()+",'N',NULL,'38'); "; 
			str = str + "  "; 
			 Query query = manager.createNativeQuery(str); 
			 query.executeUpdate();
		}
		 if(transacaoIndependente)
			   transaction.commit();
	   }
	   catch(Exception ex)
	   {
		   if(transacaoIndependente)
			   transaction.rollback();
		   
		   
		   ex.printStackTrace();
		   throw ex;
	   }
	   finally
	   {
		   if(transacaoIndependente)
			   manager.close();
	   }
	}
	public static void  gerarPlanoGrupoEmpresa3(List<EmpresaFisica> lista) throws SQLException
	{
		EntityManager manager = null;
		EntityTransaction transaction = null;
		Boolean transacaoIndependente = false;
		   try
		   {
			   if(manager == null || transaction == null)
			   {
				   manager = ConexaoUtil.getEntityManager();
				   transaction = manager.getTransaction();
				   transaction.begin();
				   transacaoIndependente = true;
			   }
		for (int i = 0; i < lista.size(); i++)
		{
			EmpresaFisica empresa = lista.get(i);
			
			String str = "";
			
			str = str + " INSERT INTO `planopagamento` VALUES "; 
			str = str + " (null,1,'A VISTA','-',15,NULL,1,NULL,NULL,"+empresa.getId()+",'N','A Vista','1'), "; 
			str = str + " (null,4,'CRED (1 A 15 DIAS) ','-',15,NULL,10,NULL,NULL,"+empresa.getId()+",'S','A Prazo','2'), "; 
			str = str + " (null,4,'CRED (1 A 4X)','-',10,NULL,10,NULL,NULL,"+empresa.getId()+",'S','A Prazo','3'), "; 
			str = str + " (null,4,'CRED 5X A 7X (1+8) CX ','-',0,NULL,10,NULL,NULL,"+empresa.getId()+",'S','A Prazo','4'), "; 
			str = str + " (null,4,'CH ATE 30 DIAS','-',15,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cheque','5'), "; 			
			str = str + " (null,4,'CH 30/60/90/120 OU (75 DIAS)','-',10,NULL,8,NULL,NULL,"+empresa.getId()+",'N','Cheque','6'), "; 
			str = str + " (null,4,'CH 5X E 6X (90 A 180 DIAS)','-',0,NULL,5,NULL,NULL,"+empresa.getId()+",'N','Cheque','7'), "; 
			str = str + " (null,4,'CARTAO ATE 2X','-',15,NULL,5,NULL,NULL,"+empresa.getId()+",'N','Cartao','8'), "; 
			str = str + " (null,4,'CARTAO ATE 3X ATE 6X','-',10,NULL,2,NULL,NULL,"+empresa.getId()+",'N','Cartao','9'), "; 
			str = str + " (null,4,'CARTAO ATE 7X ATE 10X','-',0,NULL,0,NULL,NULL,"+empresa.getId()+",'N','Cartao','10'), ";
			
			str = str + " (null,8,'VALE CALC OU CONVENIO','-',15,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Vale Calcado','11'), "; 
			str = str + " (null,8,'CARTAO SICREDI 6X','-',10,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cartao SICREDI','12'), "; 
			str = str + " (null,8,'CARTAO HIPERCARD 6X','-',10,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cartao HIPERCARD','13'), "; 
			str = str + " (null,8,'CARTAO SENFF 6X','-',10,NULL,10,NULL,NULL,"+empresa.getId()+",'N','Cartao SENFF','14') ";
			
			 Query query = manager.createNativeQuery(str); 
			 query.executeUpdate();
		}
		 if(transacaoIndependente)
			   transaction.commit();
	   }
	   catch(Exception ex)
	   {
		   if(transacaoIndependente)
			   transaction.rollback();
		   
		   
		   ex.printStackTrace();
		   throw ex;
	   }
	   finally
	   {
		   if(transacaoIndependente)
			   manager.close();
	   }
	}
 public static void main(String args[]) throws Exception
 {
	 List<EmpresaFisica> listaGrupo1 = new ArrayList<>();
	 List<EmpresaFisica> listaGrupo2 = new ArrayList<>();
	 List<EmpresaFisica> listaGrupo3 = new ArrayList<>();
	 
	 listaGrupo1.add(new EmpresaFisicaService().recuperarPorId(2));
	 listaGrupo1.add(new EmpresaFisicaService().recuperarPorId(4));
	 listaGrupo1.add(new EmpresaFisicaService().recuperarPorId(9));
	 
	 listaGrupo2.add(new EmpresaFisicaService().recuperarPorId(13));
	 
	 listaGrupo3.add(new EmpresaFisicaService().recuperarPorId(1));
	 listaGrupo3.add(new EmpresaFisicaService().recuperarPorId(3));
	 listaGrupo3.add(new EmpresaFisicaService().recuperarPorId(5));
	 listaGrupo3.add(new EmpresaFisicaService().recuperarPorId(7));
	 listaGrupo3.add(new EmpresaFisicaService().recuperarPorId(8));
	 
	 System.out.println("Adicionando planos no grupo 1...");
     gerarPlanoGrupoEmpresa1(listaGrupo1);
	 System.out.println("Planos no grupo 1 adicionados.");
	 System.out.println("Adicionando planos no grupo 2...");
	 gerarPlanoGrupoEmpresa2(listaGrupo2);
	 System.out.println("Planos no grupo 2 adicionados.");
	 System.out.println("Adicionando planos no grupo 3...");
	 gerarPlanoGrupoEmpresa3(listaGrupo3);
	 System.out.println("Planos no grupo 3 adicionados.");
 }
}
