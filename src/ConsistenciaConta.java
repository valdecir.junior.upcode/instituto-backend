import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.desenv.nepalign.integracao.Util;
import br.com.desenv.nepalign.snapshot.Snapshot;


public class ConsistenciaConta {


	public static void main(String[] args) throws Exception
	{
		Class.forName("com.mysql.jdbc.Driver");

		String queryContaGerencial = "select * from contagerencial";
		
		Connection conLocal = DriverManager.getConnection("jdbc:mysql://localhost:3306/nepalignloja08", "root", "1234");
		ResultSet rsContaGerencial = conLocal.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(queryContaGerencial);
		
		
	}

	public static void atualizarCreditoDebitoContaGerencial() throws Exception
	{
		String queryTipoConta = "";
		String queryContaGerencial = "";
		String queryDePara = "";

		queryTipoConta += "select * from tipoconta";
		queryContaGerencial += "select * from contagerencial";
		queryDePara += "select * from tipocontacontager";
		Class.forName("com.mysql.jdbc.Driver");

		Connection conEsc = DriverManager.getConnection("jdbc:mysql://200.150.73.67:33307/nepalign", "desenv", "desenv@3377");
		ResultSet rsTipoConta = conEsc.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(queryTipoConta);

		ResultSet rsDePara = conEsc.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(queryDePara);

		while(rsTipoConta.next())
		{
			Integer idTipoConta = rsTipoConta.getInt("idTipoConta");
			String descricaoTipoConta = rsTipoConta.getString("descricao");
			String creditoDebitoTC = rsTipoConta.getString("credito").equals("0")?"D":"C";

			while(rsDePara.next())
			{
				Integer idDeParaTipoConta = rsDePara.getInt("idTipoConta");


				if(idDeParaTipoConta.intValue() == idTipoConta.intValue())
				{
					Integer idContaGerencial = rsDePara.getInt("idContaGerencial");

					queryContaGerencial = " select * from contagerencial where idContaGerencial = "+idContaGerencial+" and creditoDebito is null";


					ResultSet rsContaGerencial = conEsc.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(queryContaGerencial);
					if(rsContaGerencial.next())
					{
						System.out.println(queryContaGerencial);
						rsContaGerencial.updateString("creditoDebito", creditoDebitoTC);
						rsContaGerencial.updateRow();
					}
				}
			}
			rsDePara.beforeFirst();
		}
		rsTipoConta.beforeFirst();
	}

	public static void atualizarCreditoDebitoTipoConta() throws Exception
	{
		Class.forName("com.mysql.jdbc.Driver");

		Connection con1 = DriverManager.getConnection(Util.getDatabaseUrl(1), "desenv", "desenv@3377");
		Connection con2 = DriverManager.getConnection(Util.getDatabaseUrl(2), "desenv", "desenv@3377");
		Connection con3 = DriverManager.getConnection(Util.getDatabaseUrl(3), "desenv", "desenv@3377");
		Connection con4 = DriverManager.getConnection(Util.getDatabaseUrl(4), "desenv", "desenv@3377");
		Connection con5 = DriverManager.getConnection(Util.getDatabaseUrl(5), "desenv", "desenv@3377");
		Connection con6 = DriverManager.getConnection(Util.getDatabaseUrl(6), "desenv", "desenv@3377");
		//Connection con7 = DriverManager.getConnection(Util.getDatabaseUrl(7), "desenv", "desenv@3377");
		Connection con8 = DriverManager.getConnection(Util.getDatabaseUrl(8), "desenv", "desenv@3377");
		//Connection con9 = DriverManager.getConnection(Util.getDatabaseUrl(9), "desenv", "desenv@3377");
		Connection con10 = DriverManager.getConnection(Util.getDatabaseUrl(10), "desenv", "desenv@3377");
		Connection con13 = DriverManager.getConnection(Util.getDatabaseUrl(13), "desenv", "desenv@3377");
		Connection conEsc = DriverManager.getConnection("jdbc:mysql://200.150.73.67:33307/nepalign", "desenv", "desenv@3377");


		String query = "";

		query += "select * from tipoconta";

		ResultSet rs1 = con1.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);
		ResultSet rs2 = con2.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);
		ResultSet rs3 = con3.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);
		ResultSet rs4 = con4.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);
		ResultSet rs5 = con5.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);
		ResultSet rs6 = con6.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);
		//ResultSet rs7 = con7.createStatement().executeQuery(query);
		ResultSet rs8 = con8.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);
		//ResultSet rs9 = con9.createStatement().executeQuery(query);
		ResultSet rs10 = con10.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);
		ResultSet rs13 = con13.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);
		ResultSet rsEsc = conEsc.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);


		while(rsEsc.next())
		{
			Integer idEscritorio = rsEsc.getInt("idTipoConta");
			String cdEscritorio = rsEsc.getString("credito");
			while(rs1.next())
			{
				Integer idLoja = rs1.getInt("idTipoConta");
				String cdLoja = rs1.getString("credito");

				if(idEscritorio.intValue() == idLoja.intValue())
				{

					if(!cdEscritorio.equalsIgnoreCase(cdLoja))
					{
						System.out.println("Loja 1: "+idLoja+" Descricao E-L: "+cdEscritorio+" - "+cdLoja);
						//rs1.updateString("descricao", descricaoEscritorio);
						//rs1.updateRow();
					}
					//else
					//System.out.println("Loja:1 "+idLoja1+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja1);
				}
			}
			rs1.beforeFirst();


			while(rs2.next())
			{
				Integer idLoja = rs2.getInt("idTipoConta");
				String cdLoja = rs2.getString("credito");

				if(idEscritorio.intValue() == idLoja.intValue())
				{

					if(!cdEscritorio.equalsIgnoreCase(cdLoja))
					{
						System.out.println("Loja 2: "+idLoja+" Descricao E-L: "+cdEscritorio+" - "+cdLoja);
						//rs2.updateString("descricao", cd);
						//rs2.updateRow();

					}
					//else
					//System.out.println("Loja:1 "+idLoja1+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja1);
				}
			}
			rs2.beforeFirst();


			while(rs3.next())
			{
				Integer idLoja = rs3.getInt("idTipoConta");
				String cdLoja = rs3.getString("credito");

				if(idEscritorio.intValue() == idLoja.intValue())
				{

					if(!cdEscritorio.equalsIgnoreCase(cdLoja))
					{
						System.out.println("Loja 3: "+idLoja+" Descricao E-L: "+cdEscritorio+" - "+cdLoja);
						//rs3.updateString("descricao", cdEscritorio);
						//rs3.updateRow();
					}
					//else
					//System.out.println("Loja:1 "+idLoja1+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja1);
				}
			}
			rs3.beforeFirst();


			while(rs4.next())
			{
				Integer idLoja = rs4.getInt("idTipoConta");
				String descricaoloja = rs4.getString("credito");

				if(idEscritorio.intValue() == idLoja.intValue())
				{

					if(!cdEscritorio.equalsIgnoreCase(descricaoloja))
					{
						System.out.println("Loja 4: "+idLoja+" Descricao E-L: "+cdEscritorio+" - "+descricaoloja);
						//rs4.updateString("descricao", cdEscritorio);
						//rs4.updateRow();
					}
					//else
					//System.out.println("Loja:1 "+idLoja1+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja1);
				}
			}
			rs4.beforeFirst();


			while(rs5.next())
			{
				Integer idLoja = rs5.getInt("idTipoConta");
				String descricaoloja = rs5.getString("credito");

				if(idEscritorio.intValue() == idLoja.intValue())
				{

					if(!cdEscritorio.equalsIgnoreCase(descricaoloja))
					{
						System.out.println("Loja 5: "+idLoja+" Descricao E-L: "+cdEscritorio+" - "+descricaoloja);
						//rs5.updateString("descricao", cdEscritorio);
						//rs5.updateRow();
					}
					//else
					//System.out.println("Loja:1 "+idLoja1+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja1);
				}
			}
			rs5.beforeFirst();


			while(rs6.next())
			{
				Integer idLoja = rs6.getInt("idTipoConta");
				String descricaoloja = rs6.getString("credito");

				if(idEscritorio.intValue() == idLoja.intValue())
				{

					if(!cdEscritorio.equalsIgnoreCase(descricaoloja))
					{
						System.out.println("Loja 6: "+idLoja+" Descricao E-L: "+cdEscritorio+" - "+descricaoloja);
						//rs6.updateString("descricao", cdEscritorio);
						//rs6.updateRow();
					}
					//else
					//System.out.println("Loja:1 "+idLoja1+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja1);
				}
			}
			rs6.beforeFirst();


			while(rs8.next())
			{
				Integer idLoja = rs8.getInt("idTipoConta");
				String descricaoloja = rs8.getString("credito");

				if(idEscritorio.intValue() == idLoja.intValue())
				{
					if(!cdEscritorio.equalsIgnoreCase(descricaoloja))
					{
						System.out.println("Loja 8: "+idLoja+" Descricao E-L: "+cdEscritorio+" - "+descricaoloja);
						//rs8.updateString("descricao", cdEscritorio);
						//rs8.updateRow();
					}
					//else
					//System.out.println("Loja:1 "+idLoja1+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja1);
				}
			}
			rs8.beforeFirst();


			while(rs10.next())
			{
				Integer idLoja = rs10.getInt("idTipoConta");
				String descricaoloja = rs10.getString("credito");

				if(idEscritorio.intValue() == idLoja.intValue())
				{
					if(!cdEscritorio.equalsIgnoreCase(descricaoloja))
					{
						System.out.println("Loja 10: "+idLoja+" Descricao E-L: "+cdEscritorio+" - "+descricaoloja);
						//rs10.updateString("descricao", cdEscritorio);
						//rs10.updateRow();
					}
					//else
					//System.out.println("Loja:1 "+idLoja1+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja1);
				}
			}
			rs10.beforeFirst();


			while(rs13.next())
			{
				Integer idLoja = rs13.getInt("idTipoConta");
				String descricaoloja = rs13.getString("credito");

				if(idEscritorio.intValue() == idLoja.intValue())
				{
					if(!cdEscritorio.equalsIgnoreCase(descricaoloja))
					{
						System.out.println("Loja 13: "+idLoja+" Descricao E-L: "+cdEscritorio+" - "+descricaoloja);
						//rs13.updateString("descricao", cdEscritorio);
						//rs13.updateRow();
					}
					//else
					//System.out.println("Loja:1 "+idLoja1+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja1);
				}
			}
			rs13.beforeFirst();

		}
		rsEsc.beforeFirst();
		rs1.close();
		rs2.close();
		rs3.close();
		rs4.close();
		rs5.close();
		rs6.close();
		rs8.close();
		rs10.close();
		rs13.close();

	}

	public static void atualizarDescricaoTipoConta() throws SQLException, ClassNotFoundException
	{
		int[] emps = new int[] { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0D };

		System.out.println("================ SNAPSHOT'S VENDA ================");
		System.out.println();

		Snapshot snap = new Snapshot(1);
		Class.forName("com.mysql.jdbc.Driver");

		Connection con1 = DriverManager.getConnection(Util.getDatabaseUrl(1), "desenv", "desenv@3377");
		Connection con2 = DriverManager.getConnection(Util.getDatabaseUrl(2), "desenv", "desenv@3377");
		Connection con3 = DriverManager.getConnection(Util.getDatabaseUrl(3), "desenv", "desenv@3377");
		Connection con4 = DriverManager.getConnection(Util.getDatabaseUrl(4), "desenv", "desenv@3377");
		Connection con5 = DriverManager.getConnection(Util.getDatabaseUrl(5), "desenv", "desenv@3377");
		Connection con6 = DriverManager.getConnection(Util.getDatabaseUrl(6), "desenv", "desenv@3377");
		//Connection con7 = DriverManager.getConnection(Util.getDatabaseUrl(7), "desenv", "desenv@3377");
		Connection con8 = DriverManager.getConnection(Util.getDatabaseUrl(8), "desenv", "desenv@3377");
		//Connection con9 = DriverManager.getConnection(Util.getDatabaseUrl(9), "desenv", "desenv@3377");
		Connection con10 = DriverManager.getConnection(Util.getDatabaseUrl(10), "desenv", "desenv@3377");
		Connection con13 = DriverManager.getConnection(Util.getDatabaseUrl(13), "desenv", "desenv@3377");
		Connection conEsc = DriverManager.getConnection("jdbc:mysql://200.150.73.67:33307/nepalign", "desenv", "desenv@3377");


		String query = "";

		query += "select * from tipoconta";

		ResultSet rs1 = con1.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);
		ResultSet rs2 = con2.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);
		ResultSet rs3 = con3.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);
		ResultSet rs4 = con4.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);
		ResultSet rs5 = con5.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);
		ResultSet rs6 = con6.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);
		//ResultSet rs7 = con7.createStatement().executeQuery(query);
		ResultSet rs8 = con8.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);
		//ResultSet rs9 = con9.createStatement().executeQuery(query);
		ResultSet rs10 = con10.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);
		ResultSet rs13 = con13.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);
		ResultSet rsEsc = conEsc.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(query);


		while(rsEsc.next())
		{
			Integer idEscritorio = rsEsc.getInt("idTipoConta");
			String descricaoEscritorio = rsEsc.getString("descricao");
			while(rs1.next())
			{
				Integer idLoja = rs1.getInt("idTipoConta");
				String descricaoloja = rs1.getString("descricao");

				if(idEscritorio.intValue() == idLoja.intValue())
				{

					if(!descricaoEscritorio.equalsIgnoreCase(descricaoloja))
					{
						System.out.println("Loja 1: "+idLoja+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja);
						rs1.updateString("descricao", descricaoEscritorio);
						rs1.updateRow();
					}
					//else
					//System.out.println("Loja:1 "+idLoja1+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja1);
				}
			}
			rs1.beforeFirst();


			while(rs2.next())
			{
				Integer idLoja = rs2.getInt("idTipoConta");
				String descricaoloja = rs2.getString("descricao");

				if(idEscritorio.intValue() == idLoja.intValue())
				{

					if(!descricaoEscritorio.equalsIgnoreCase(descricaoloja))
					{
						System.out.println("Loja 2: "+idLoja+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja);
						rs2.updateString("descricao", descricaoEscritorio);
						rs2.updateRow();

					}
					//else
					//System.out.println("Loja:1 "+idLoja1+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja1);
				}
			}
			rs2.beforeFirst();


			while(rs3.next())
			{
				Integer idLoja = rs3.getInt("idTipoConta");
				String descricaoloja = rs3.getString("descricao");

				if(idEscritorio.intValue() == idLoja.intValue())
				{

					if(!descricaoEscritorio.equalsIgnoreCase(descricaoloja))
					{
						System.out.println("Loja 3: "+idLoja+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja);
						rs3.updateString("descricao", descricaoEscritorio);
						rs3.updateRow();
					}
					//else
					//System.out.println("Loja:1 "+idLoja1+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja1);
				}
			}
			rs3.beforeFirst();


			while(rs4.next())
			{
				Integer idLoja = rs4.getInt("idTipoConta");
				String descricaoloja = rs4.getString("descricao");

				if(idEscritorio.intValue() == idLoja.intValue())
				{

					if(!descricaoEscritorio.equalsIgnoreCase(descricaoloja))
					{
						System.out.println("Loja 4: "+idLoja+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja);
						rs4.updateString("descricao", descricaoEscritorio);
						rs4.updateRow();
					}
					//else
					//System.out.println("Loja:1 "+idLoja1+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja1);
				}
			}
			rs4.beforeFirst();


			while(rs5.next())
			{
				Integer idLoja = rs5.getInt("idTipoConta");
				String descricaoloja = rs5.getString("descricao");

				if(idEscritorio.intValue() == idLoja.intValue())
				{

					if(!descricaoEscritorio.equalsIgnoreCase(descricaoloja))
					{
						System.out.println("Loja 5: "+idLoja+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja);
						rs5.updateString("descricao", descricaoEscritorio);
						rs5.updateRow();
					}
					//else
					//System.out.println("Loja:1 "+idLoja1+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja1);
				}
			}
			rs5.beforeFirst();


			while(rs6.next())
			{
				Integer idLoja = rs6.getInt("idTipoConta");
				String descricaoloja = rs6.getString("descricao");

				if(idEscritorio.intValue() == idLoja.intValue())
				{

					if(!descricaoEscritorio.equalsIgnoreCase(descricaoloja))
					{
						System.out.println("Loja 6: "+idLoja+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja);
						rs6.updateString("descricao", descricaoEscritorio);
						rs6.updateRow();
					}
					//else
					//System.out.println("Loja:1 "+idLoja1+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja1);
				}
			}
			rs6.beforeFirst();


			while(rs8.next())
			{
				Integer idLoja = rs8.getInt("idTipoConta");
				String descricaoloja = rs8.getString("descricao");

				if(idEscritorio.intValue() == idLoja.intValue())
				{
					if(!descricaoEscritorio.equalsIgnoreCase(descricaoloja))
					{
						System.out.println("Loja 8: "+idLoja+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja);
						rs8.updateString("descricao", descricaoEscritorio);
						rs8.updateRow();
					}
					//else
					//System.out.println("Loja:1 "+idLoja1+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja1);
				}
			}
			rs8.beforeFirst();


			while(rs10.next())
			{
				Integer idLoja = rs10.getInt("idTipoConta");
				String descricaoloja = rs10.getString("descricao");

				if(idEscritorio.intValue() == idLoja.intValue())
				{
					if(!descricaoEscritorio.equalsIgnoreCase(descricaoloja))
					{
						System.out.println("Loja 10: "+idLoja+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja);
						rs10.updateString("descricao", descricaoEscritorio);
						rs10.updateRow();
					}
					//else
					//System.out.println("Loja:1 "+idLoja1+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja1);
				}
			}
			rs10.beforeFirst();


			while(rs13.next())
			{
				Integer idLoja = rs13.getInt("idTipoConta");
				String descricaoloja = rs13.getString("descricao");

				if(idEscritorio.intValue() == idLoja.intValue())
				{
					if(!descricaoEscritorio.equalsIgnoreCase(descricaoloja))
					{
						System.out.println("Loja 13: "+idLoja+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja);
						rs13.updateString("descricao", descricaoEscritorio);
						rs13.updateRow();
					}
					//else
					//System.out.println("Loja:1 "+idLoja1+" Descricao E-L: "+descricaoEscritorio+" - "+descricaoloja1);
				}
			}
			rs13.beforeFirst();

		}
		rsEsc.beforeFirst();
		rs1.close();
		rs2.close();
		rs3.close();
		rs4.close();
		rs5.close();
		rs6.close();
		rs8.close();
		rs10.close();
		rs13.close();


	}

}
