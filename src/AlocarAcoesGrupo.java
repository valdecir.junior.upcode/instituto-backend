import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import br.com.desenv.nepalign.model.Acao;
import br.com.desenv.nepalign.model.Acaogrupo;
import br.com.desenv.nepalign.model.Funcionalidade;
import br.com.desenv.nepalign.model.Grupo;
import br.com.desenv.nepalign.service.AcaoService;
import br.com.desenv.nepalign.service.AcaogrupoService;
import br.com.desenv.nepalign.service.FuncionalidadeService;
import br.com.desenv.nepalign.service.GrupoService;
import br.com.desenv.nepalign.service.ModuloService;


public class AlocarAcoesGrupo {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
 
		Acaogrupo acaoGrupo  = new Acaogrupo();
		Acaogrupo acaoGrupoCriterio = new Acaogrupo();
		Grupo grupoCriterio = new Grupo();
		grupoCriterio.setDescricao("CREDIARIO");
		List<Grupo> listaGrupo = new GrupoService().listarPorObjetoFiltro(grupoCriterio);
		List<Acao> acoesRecuperadas = new AlocarAcoesGrupo().buscarArquivoAcoes();
		
		for(int x=0;x<acoesRecuperadas.size();x++)
		{
			acaoGrupo.setAcao(acoesRecuperadas.get(x));
			acaoGrupo.setGrupo(listaGrupo.get(0));
			
			List<Acaogrupo> listaAcaoGrupo = new AcaogrupoService().listarPorObjetoFiltro(acaoGrupo);
			
			
			try{
				if(listaAcaoGrupo.size() < 1)
					new AcaogrupoService().salvar(acaoGrupo);	
			}
			catch(Exception ex)
			{
				System.out.println(acaoGrupo.getAcao().getDescricao());
			}
		}
	}
	
	 public List<Acao> buscarArquivoAcoes() throws Exception
	 {
		   String[] infoAcao = null;
	  try
	  {
			   
	   ArrayList<Acao> data = new ArrayList<Acao>();
	   
	   System.out.println("Começando escaneamento do arquivo...");
	   
	   if(!new File("C:/TRACE/TRACE_CREDIARIO").exists())
	   {
	    System.out.println("Arquivo [C:/TRACE/TRACE_CREDIARIO] não encontrado.");
	   }
	   
	   BufferedReader bufferedReader = new BufferedReader(new FileReader("C:/TRACE/TRACE_CREDIARIO"));
	   
	   System.out.println("Arquivo encontrado...");
	   
	   String acao = "";
	   
	   System.out.println("Lendo arquivo....");
	   

	   
	   while((acao = bufferedReader.readLine()) != null)
	   {
	    if(!acao.equals("FIM"))
	    {
	    	infoAcao = acao.split("\\|");
		     
		     Funcionalidade funcionalidadeCriterio = new Funcionalidade();
		     funcionalidadeCriterio.setDescricao(infoAcao[0].toUpperCase());
		     
		     List<Funcionalidade> listaFuncionalidade = new FuncionalidadeService().listarPorObjetoFiltro(funcionalidadeCriterio);
		     
		     
		     String descricao = infoAcao[1].toUpperCase() + " " + infoAcao[0].toUpperCase();
		     String nomeMetodo = infoAcao[1].toUpperCase();
		     String logarAcao = "S";
		     String situacao = "A";
		      
		     Acao bufferAcao = new Acao();
		     bufferAcao.setDescricao(descricao);
		     if(listaFuncionalidade.size()>0)
		     {
		    	 bufferAcao.setFuncionalidade(listaFuncionalidade.get(0));
			     	 
		     }
		     else
		     {
		    	 Funcionalidade novaFuncionalidade = new Funcionalidade();
		    	 funcionalidadeCriterio.setNomeServico(infoAcao[0]);
		    	 funcionalidadeCriterio.setModulo(new ModuloService().listar().get(0));
		    	 funcionalidadeCriterio.setSituacao("A");
		    	 novaFuncionalidade = new FuncionalidadeService().salvar(funcionalidadeCriterio);
		    	 bufferAcao.setFuncionalidade(novaFuncionalidade);
		     }
		     bufferAcao.setLogarAcao(logarAcao);
		     bufferAcao.setNomeMetodo(nomeMetodo);
		     bufferAcao.setSituacao(situacao);
		     Acao acaoRecuperada;
		     List<Acao> listaAcaoRecuperada = new AcaoService().listarPorObjetoFiltro(bufferAcao);
		     if(listaAcaoRecuperada.size()>0)
		     {
		    	 acaoRecuperada  = listaAcaoRecuperada.get(0);
		    	 data.add(acaoRecuperada);
		     }
		     else
		     {
		    	 Acao novaAcao = new Acao();
		    	 novaAcao = new AcaoService().salvar(bufferAcao);
		    	 data.add(novaAcao);
		     }
	    }
	    else
	     break;
	   }
	   
	   System.out.println(data.size() + " Ações encontrados...");
	   
	   return data;
	   
	  }
	  catch(Exception ex)
	  {
		  System.out.println(infoAcao);
	   throw new Exception(ex);
	  }
	 }

}
