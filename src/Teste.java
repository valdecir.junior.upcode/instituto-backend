import java.awt.Point;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import javax.persistence.EntityManager;

import br.com.desenv.frameworkignorante.IgnSegurancaService;
import br.com.desenv.nepalign.model.Bancos;
import br.com.desenv.nepalign.model.Caixa;
import br.com.desenv.nepalign.model.LancamentoBanco;
import br.com.desenv.nepalign.model.LancamentoCaixa;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;

public class Teste 
{
	static HashMap<String, List<String>> calls = new HashMap<String, List<String>>();
	
	private static void recontroiSaldoBancos() throws SQLException
	{
		/**
		 * CORRIGE SALDO BANCOS, N APAGA N COMÉDIA
		 */
        EntityManager manager = ConexaoUtil.getEntityManager();
        Connection conn = ConexaoUtil.getConexaoPadrao();
        
        for(Bancos bancos : (List<Bancos>) manager.createNativeQuery("SELECT * FROM bancos", Bancos.class).getResultList())
        {
    		StringBuilder strSaldoMesAnterior = new StringBuilder();
    		strSaldoMesAnterior.append("SELECT sb.saldo FROM saldobancos sb");
            strSaldoMesAnterior.append(" INNER JOIN bancos b2 ON b2.idBancos = sb.idBancos ");
            strSaldoMesAnterior.append(" WHERE sb.ano = 2017 AND sb.mes = 5 AND b2.idBancos in(" + bancos.getId().toString() + ")");
            
            List<LancamentoBanco> listaLancamentoBanco = manager.createNativeQuery("SELECT * FROM lancamentoBanco WHERE dataLancamento between '2017-06-01 00:00:00' and '2017-06-30 23:59:59' and" +
    				" idBancos = " + bancos.getId().toString(), LancamentoBanco.class).getResultList();

            ResultSet query = conn.createStatement().executeQuery(strSaldoMesAnterior.toString());
            
            Double saldoAnterior = 0.0;
            
            if(query.first())
            	saldoAnterior = query.getDouble(1);
            else
            	continue;
            
            for(LancamentoBanco lancamentoBanco : listaLancamentoBanco)
            {
            	if(lancamentoBanco.getTipoMovimentoBancos().getCreditoDebito() == 2)
            		saldoAnterior -= lancamentoBanco.getValor();
            	else
            		saldoAnterior += lancamentoBanco.getValor();
            }
            
            System.out.println("update saldoBancos set saldo = " + saldoAnterior + " where idBancos = " + bancos.getId() + " and mes = 6 and ano = 2017;");
        }
		

        
        conn.close();
        manager.close();
	}
	
	private static void reconstroiSaldoCaixa() throws SQLException
	{
		/**
		 * CORRIGE SALDO BANCOS, N APAGA N COMÉDIA
		 */
        EntityManager manager = ConexaoUtil.getEntityManager();
        Connection conn = ConexaoUtil.getConexaoPadrao();
        
        for(Caixa caixa : (List<Caixa>) manager.createNativeQuery("SELECT * FROM caixa", Caixa.class).getResultList())
        {
    		StringBuilder strSaldoMesAnterior = new StringBuilder();
    		strSaldoMesAnterior.append("SELECT saldoCaixa.saldo FROM saldoCaixa");
            strSaldoMesAnterior.append(" INNER JOIN caixa ON caixa.idCaixa = saldoCaixa.idCaixa ");
            strSaldoMesAnterior.append(" WHERE saldoCaixa.ano = 2017 AND saldoCaixa.mes = 5 AND caixa.idCaixa in(" + caixa.getId().toString() + ")");
            
            List<LancamentoCaixa> listaLancamentoCaixa = manager.createNativeQuery("SELECT * FROM lancamentoCaixa WHERE dataLancamento between '2017-06-01 00:00:00' and '2017-06-30 23:59:59' and" +
    				" idCaixa = " + caixa.getId().toString(), LancamentoCaixa.class).getResultList();

            ResultSet query = conn.createStatement().executeQuery(strSaldoMesAnterior.toString());
            
            Double saldoAnterior = 0.0;
            
            if(query.first())
            	saldoAnterior = query.getDouble(1);
            else
            	continue;
            
            for(LancamentoCaixa lancamentoCaixa : listaLancamentoCaixa)
            {
            	if(lancamentoCaixa.getTipoMovimentoCaixa().getCreditoDebito() == 2)
            		saldoAnterior -= lancamentoCaixa.getValor();
            	else
            		saldoAnterior += lancamentoCaixa.getValor();
            }
            
            System.out.println("update saldoCaixa set saldo = " + saldoAnterior + " where idCaixa = " + caixa.getId() + " and mes = 6 and ano = 2017;");
        }
		

        
        conn.close();
        manager.close();
	}
	
	public static void main(String[] args) throws Exception 
	{	
		reconstroiSaldoCaixa();
        
		
		/*File file = new File("D:/Desenv/Workspace/nepalignair/src/br/com/desenv/nepalign/view/component");
		
		if(file.isDirectory())
		{
			System.out.println("path is a directory");
			System.out.println("reading files");
			
			for(File _file : file.listFiles())
			{
				if (_file.getName().endsWith(".mxml"))
				{
					BufferedReader reader = new BufferedReader(new FileReader(_file));
					
					String tmp;
					
					String tmpService = null;
					String reportMethod = null;
					
					while((tmp = reader.readLine()) != null)
					{
						try
						{
							
							if(tmp.contains("prepararObjetoRemoto") || tmp.contains("getOperation") || tmp.contains("const REPORT_CLASS_NAME") || tmp.contains("const REPORT_METHOD_NAME"))
							{
								final String value = tmp.split("\"")[1];
								
								if(value.trim().equals(""))
									continue;
								
								if(tmp.contains("Service") && tmp.contains("getOperation"))
								{
									String service = tmp.split("\"")[1];
									String operation = tmp.split("\"")[3];
									
									if(!calls.containsKey(service))
										calls.put(service, null);
									tmpService = service;
									
									List<String> _calls = calls.get(tmpService);
									
									if(_calls == null)
										_calls = new ArrayList<String>();
									
									if(!_calls.contains(operation))
										_calls.add(operation);
									
									calls.put(tmpService, _calls);
								}
								if(value.contains("Service"))
								{
									if(!calls.containsKey(value))
										calls.put(value, null);
									tmpService = value;
								}
								else if(tmp.contains("const REPORT_CLASS_NAME"))
								{
									if(!calls.containsKey(value))
										calls.put(value, null);
									tmpService = value;
									
									if(reportMethod != null)
									{
										List<String> methods = calls.get(value);
										
										if(methods == null)
											methods = new ArrayList<String>();
										
										methods.add(reportMethod);
										
										calls.put(value, methods);
										
										reportMethod = null;
									}
								}
								else if(tmp.contains("const REPORT_METHOD_NAME"))
								{
									reportMethod = value;
								}
								else
								{
									if(tmpService == null)
										throw new Exception();
									
									List<String> _calls = calls.get(tmpService);
									
									if(_calls == null)
										_calls = new ArrayList<String>();
	 								
									if(!_calls.contains(value))
										_calls.add(value);
									
									calls.put(tmpService, _calls);
									
									//tmpService = null;
								}
							}
						}
						catch(Exception ex)
						{
							System.out.println(tmp);
						}
					}
					reader.close();
				}
			}
		}
		
		int methods = 0;
		
		for(Iterator<Entry<String, List<String>>> it = calls.entrySet().iterator(); it.hasNext(); )
		{
			Entry<String, List<String>> _calls = it.next();

			if(_calls.getValue() == null || _calls.getValue().size() == 0x00)
				it.remove();
			else
			{
				final Iterator<String> ummodifiedIterator = _calls.getValue().iterator();
				
				while(ummodifiedIterator.hasNext())
				{
					try
					{
						final String method = ummodifiedIterator.next();
						final Method methodSignature = IgnSegurancaService.getMethod(method, IgnSegurancaService.getServiceClass(_calls.getKey().split("\\.")[_calls.getKey().split("\\.").length - 1]), false);
						
						if(methodSignature == null)
							ummodifiedIterator.remove();
					} 
					catch(Exception ex)
					{
						
					}
				}
				
				if(_calls.getValue().size() == 0x00)
					it.remove();
			}
		}
		
		for(Entry<String, List<String>> _calls : calls.entrySet())
		{
			System.out.println("Service ".concat(_calls.getKey()));
			for(final String method : _calls.getValue())
			{
				System.out.println(" " + method);
				methods++;
			}
		}
		
		System.out.println("-----------------");
		System.out.println("Services : " + calls.size());
		System.out.println("Methods  : " + methods);
		
		searchAndReplace();*/
		//searchAndReplaceReports();
	}
	
	static void searchAndReplace() throws IOException
	{
		File file = new File("F:/relatorios/");
		
		if(file.isDirectory())
		{
			for(Entry<String, List<String>> _calls : calls.entrySet())
			{
				File serviceFile = null;
				StringBuffer newContent = null;
				
				if(_calls.getKey().contains("Service"))
					continue;
				
				for(File _file : file.listFiles())
				{
					if (_file.getName().equalsIgnoreCase(_calls.getKey().replace("br.com.desenv.nepalign.relatorios.", "").concat(".java")))
					{
						serviceFile = _file;
						newContent = new StringBuffer();
						final BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(_file), "UTF8"));
						
						String tmp;
						
						while((tmp = reader.readLine()) != null)
						{
							String line = tmp;
							
							for (final String method : _calls.getValue())
							{ 
								if(tmp.contains("public") && tmp.contains(method.concat("(")))
									line = "	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao\r\n".concat(tmp);
							}
							
							if(newContent.length() > 0x00)
								newContent.append("\r\n");
							newContent.append(line);
						}
						reader.close();
					}
				}
				
				if(serviceFile == null)
					System.out.println("File not found to " + _calls.getKey());
				else
				{
					//Files.copy(serviceFile.toPath(), new File(serviceFile.getPath() + "/" + serviceFile.getName().concat(".bkp")).toPath());
					
					File newFile = new File("F:/relatoriosAtualizados/" + serviceFile.getName());
					newFile.createNewFile();
					
					BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newFile), "UTF-8"));
					for(String line : newContent.toString().split("\n"))
						writer.write(line);
					writer.close();
				}
			}
		}
	}

	
	static void searchAndReplaceReports() throws Exception
	{
		File file = new File("F:/relatorios/");
		
		File serviceFile = null;
		StringBuffer newContent = null;
		
		for(File _file : file.listFiles())
		{
			serviceFile = _file;
			newContent = new StringBuffer();
			final BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(_file), "UTF8"));
			
			String tmp;
			
			while((tmp = reader.readLine()) != null)
			{
				String line = tmp;
				
				if(tmp.contains("public void"))
					line = "	@br.com.desenv.frameworkignorante.annotations.IgnSegurancaAcao\r\n".concat(tmp);
				
				if(newContent.length() > 0x00)
					newContent.append("\r\n");
				newContent.append(line);
			}
			
			reader.close();

			File newFile = new File("F:/relatoriosAtualizados/" + serviceFile.getName());
			newFile.createNewFile();
			
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newFile), "UTF-8"));
			for(String line : newContent.toString().split("\n"))
				writer.write(line);
			writer.close();
		}
	}
} 