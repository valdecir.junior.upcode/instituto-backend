import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.nepalign.model.Feriado;
import br.com.desenv.nepalign.service.FeriadoService;
import br.com.desenv.nepalign.util.DsvConstante;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;


public class persistirFeriado {

	/**
	 * @param args
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws IOException, BiffException, ParseException {
		
		Workbook workbook = Workbook.getWorkbook(new File("C:/Desenv/feriados_nacionais.xls"));
		
		Date valorData=null;
		String valorDiaSemana = "";
		String valorDescricao = "";

		Feriado feriado = new Feriado();
		FeriadoService feriadoService = new FeriadoService();
		
		Sheet sheet = workbook.getSheet(0);
		int linhas = sheet.getRows();
		System.out.println("Iniciando a leitura da planilha XLS:");
		for(int i=1;i<linhas;i++)
		{
			
			Cell data = sheet.getCell(0,i);
			Cell diaSemana = sheet.getCell(1,i);
			Cell descricao = sheet.getCell(2,i);
			DateFormat df = new SimpleDateFormat("dd/MM/yy");
			valorData = df.parse(data.getContents());
			valorDiaSemana = diaSemana.getContents();
			valorDescricao = descricao.getContents();
			
			System.out.println("Coluna 1: "+valorData.toString());
			System.out.println("Coluna 2: "+valorDiaSemana);
			System.out.println("Coluna 3: "+valorDescricao);
			
			feriado.setDataFeriado(valorData);
			feriado.setDescricao(valorDescricao);
			feriado.setDiaSemana(valorDiaSemana);
			
			try {
				feriadoService.salvar(feriado);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		workbook.close();

	}

}
