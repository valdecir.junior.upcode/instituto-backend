import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.com.desenv.frameworkignorante.IgnUtil;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;


public class ConversaoVendaSeralle 
{
	private static Connection getConexaoSeralle()
	{
		Connection conexao = null;
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
			conexao = DriverManager.getConnection("jdbc:mysql://192.168.25.141:3306/nepalign", "desenv", "desenv@3377");
			return conexao;
		}
		catch (ClassNotFoundException e) 
		{
			throw new RuntimeException(e);
		}		
		catch (SQLException e) 
		{
			throw new RuntimeException(e);
		}
	}
	
	private static int getNewId(int oldId)
	{
		switch(oldId)
		{
		case 0x01:
			return 0x0A;
		case 0x03:
			return 0x04;
		case 0x07:
			return 0x08;
		case 0x09:
			return 0x09;
		case 33:
			return 44;
		case 57:
			return 58;
		case 59:
			return 59;
		case 11:
		case 101:
			return 110;
			default:
				return oldId + 5000;
		}
	}
	
	public static void main(String[] args) throws Exception
	{
		final Connection connection = getConexaoSeralle();
		final IgnUtil ignUtil = new IgnUtil();
		
		connection.setAutoCommit(false);
		
		final Statement st = connection.createStatement();
		try
		{
			
			st.execute("/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */");
			st.execute("/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;");
			st.execute("/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;");
			st.execute("/*!40101 SET NAMES utf8 */;");
			st.execute("/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;");
			st.execute("/*!40103 SET TIME_ZONE='+00:00' */;");
			st.execute("/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;");
			st.execute("/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;");
			st.execute("/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;");
			st.execute("/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;");

			st.execute("update PedidoVenda set idEmpresaFisica = 5010 where idEmpresaFisica = 10;");
			
			st.execute("update PedidoVenda set idEmpresaFisica = 5004 where idEmpresaFisica = 4;");
			
			st.execute("update PedidoVenda set idEmpresaFisica = 5008 where idEmpresaFisica = 8;");
			
			st.execute("update PedidoVenda set idEmpresaFisica = 10 where idEmpresaFisica = 1;");
			
			st.execute("update PedidoVenda set idEmpresaFisica = 4 where idEmpresaFisica = 3;");
			
			st.execute("update PedidoVenda set idEmpresaFisica = 8 where idEmpresaFisica = 7;");
			
			st.execute("/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;");

			st.execute("/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;");
			st.execute("/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;");
			st.execute("/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;");
			st.execute("/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;");
			st.execute("/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;");
			st.execute("/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;");
			st.execute("/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;");
			
			connection.commit();
			connection.setAutoCommit(true);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			
			connection.rollback();
		}
		finally
		{
			connection.close();
		}
	}
}