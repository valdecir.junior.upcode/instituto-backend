import java.io.BufferedReader;
import java.io.FileReader;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import org.infinispan.util.SysPropertyActions;

import br.com.desenv.nepalign.model.Atendido;
import br.com.desenv.nepalign.model.AtividadeOferecida;
import br.com.desenv.nepalign.model.Turma;
import br.com.desenv.nepalign.model.TurmaAtendido;
import br.com.desenv.nepalign.persistence.TurmaAtendidoPersistence;
import br.com.desenv.nepalign.service.AtendidoService;
import br.com.desenv.nepalign.service.AtividadeOferecidaService;
import br.com.desenv.nepalign.service.TurmaAtendidoService;
import br.com.desenv.nepalign.service.TurmaService;


public class ImportaPlanilhaAtendidoTurma02 
{

	public static void main(String[] args)
	{
		ArrayList<String> arquivos = new ArrayList<String>();
		//arquivos.add("atedidosSemHorario.csv");
		//arquivos.add("atedidosSemHorario02.csv");
		arquivos.add("atedidosSemHorarioUnificado.csv");
		
		for (String arquivo : arquivos) 
		{
			System.out.println(arquivo);
			ImportaPlanilhaAtendidoTurma02 pl = new ImportaPlanilhaAtendidoTurma02();
			pl.init("C:/upcode/projetos/instituto/planilhas/ajustadas/"+arquivo);			
		}

	}
	public void init(String planilha)
	{
		BufferedReader br = null;
		int cont=0;
		String conteudo[];
		String conteudoNome[];
		boolean isAtendido=false;
		ArrayList<String> linhasNaoUtilizadas = new ArrayList<String>();
		String nomeAtendido;
		int codigoAtendido;
		int codigoTurma;
		String nomeAtividade;
		int idTurma;
		Atendido atendido = null;
		AtendidoService atendidoService = new AtendidoService();
		AtividadeOferecida atividade = null;
		AtividadeOferecidaService atividadeService = new AtividadeOferecidaService();
		List<TurmaAtendido> listaTurmasAtendido = new ArrayList<TurmaAtendido>();
		Turma turma = null;
		TurmaService turmaService = new TurmaService();
		String nomeTurma = null;
		String listaAtendidos = "";
		int codigoAnterior= 0;

		try
		{
			br = new BufferedReader(new FileReader(planilha));
			
			String linha;
			while ((linha = br.readLine()) != null) 
			{
				//System.out.println(linha);
				cont++;
				if (!(linha.trim().equals("") || linha.trim().indexOf("Codigo")>-1 || linha.trim().indexOf("CODIGO")>-1  || linha.trim().indexOf("NOME")>-1|| linha.trim().indexOf("go;Nome;")>-1))
				{
					conteudo =  removerCaracteresEspeciais(linha).split(";");
					if (conteudo.length==10)
					{
						try
						{
							codigoAtendido = Integer.parseInt(conteudo[0]);
							atendido = atendidoService.recuperarPorId(codigoAtendido);
							if (atendido!=null)
							{
								try
								{
									if (atendido.getId()!=codigoAnterior)
									{
										TurmaAtendido criterioT = new TurmaAtendido();
										criterioT.setAtendido(atendido);
										listaTurmasAtendido = new TurmaAtendidoService().listarPorObjetoFiltro(criterioT); 
										if (listaTurmasAtendido.size()>0)
										{
											
											System.out.println("Atendido j� possui ensalamento: " + atendido.getId()+" "+atendido.getNomeCompleto());
											for (TurmaAtendido turmaAtendido2 : listaTurmasAtendido) 
											{
												new TurmaAtendidoService().excluir(turmaAtendido2);
											}
										}
										codigoAnterior = atendido.getId();
									}
									codigoTurma = Integer.parseInt(conteudo[9]);
									nomeTurma = normalizaNomeAtividade(conteudo[6])+" "+codigoTurma;
									Turma criterio = new Turma();
									criterio.setDescricao(nomeTurma);
									turma = turmaService.listarPorObjetoFiltro(criterio).get(0);
																		
									TurmaAtendido turmaAtendido = new TurmaAtendido();
									turmaAtendido.setAtendido(atendido);
									turmaAtendido.setTurma(turma);
									new TurmaAtendidoService().salvar(turmaAtendido);
									listaAtendidos = listaAtendidos+ "," + codigoAtendido;
								}
								catch(Exception ex2)
								{
									System.out.println("Campo com codigo de turma est� invalido ou n�o foi encontrado na base. " + cont + " Linha: " + linha);
									linhasNaoUtilizadas.add(linha);
								}
							}
							else
							{
								System.out.println("Atendido n�o foi encontrado. " + cont + " Linha: " + linha);
								linhasNaoUtilizadas.add(linha);
							}							
						}
						catch(Exception ex1)
						{
							System.out.println("Campo do esalamento n�o preenchido corretamente. " + cont + " Linha: " + linha);
							linhasNaoUtilizadas.add(linha);
						}
						

					}
					else
					{
						//System.out.println("Linha n�o tem a quantidade de campos suficiente. " + cont + " Linha: " + linha);
						linhasNaoUtilizadas.add(linha);
					}
					
				}
				else
				{
					linhasNaoUtilizadas.add(linha);
				}
			}
			System.out.println(listaAtendidos);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	
	}
	
	private String normalizaNomeAtividade(String nome)
	{
		nome = nome.toUpperCase().trim();
		if (nome.equals("HIDROGINµSTICA") || nome.equals("HIDROGINÁSTICA") || nome.equals("HIDROGIN�STICA") || nome.indexOf("HIDROGIN")>-1)
		{
			return "HIDROGINASTICA";
		}		
		else if (nome.equals("GINµSTICA") || nome.equals("GINÁSTICA") || nome.equals("GIN�STICA") || nome.indexOf("GIN")>-1)
		{
			return "GINASTICA";
		}
		if (nome.equals("COMPUTA€ÇO") || nome.equals("COMPUTAÇÃO") || nome.equals("COMPUTA��O")  || nome.indexOf("COMPUTA")>-1)
		{
			return "COMPUTACAO";
		}		
		if (nome.equals("CULINµRIA") || nome.equals("CULINÁRIA") || nome.equals("CULIN�RIA") || nome.indexOf("CULIN")>-1)
		{
			return "CULINARIA";
		}			
		if (nome.equals("DAN�A") || nome.equals("DANÇA") || nome.indexOf("DAN A")>-1  || nome.indexOf("DAN")>-1)
		{
			return "DANCA";
		}
		if (nome.equals("MUSCULA��O") || nome.equals("MUSCULAÇÃO") || nome.indexOf("MUSCULA")>-1)
		{
			return "MUSCULACAO";
		}		
		if (nome.equals("VOLEY") || nome.equals("V�LEI") || nome.indexOf("VOL")>-1 || nome.equals("VALEI")  || nome.equals("VALEY"))
		{
			return "VOLEI";
		}	
		if (nome.indexOf("ARTES")>-1)
		{
			return "ARTESANATO";
		}
		if (nome.indexOf("CAMINH")>-1)
		{
			return "CAMINHADA";
		}			
		else
		{
			return nome;
		}
	
	}

	public String removerCaracteresEspeciais(String string) {
        string = Normalizer.normalize(string, Normalizer.Form.NFD);
        string = string.replaceAll("[^\\p{ASCII}]", "");
        return string;
    }	
}
