import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.desenv.nepalign.model.ItemPedidoVenda;
import br.com.desenv.nepalign.model.PedidoVenda;
import br.com.desenv.nepalign.model.PlanoPagamento;
import br.com.desenv.nepalign.model.TipoMovimentacaoEstoque;
import br.com.desenv.nepalign.model.Vendedor;
import br.com.desenv.nepalign.service.ClienteService;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.EstoqueProdutoService;
import br.com.desenv.nepalign.service.PedidoVendaService;
import br.com.desenv.nepalign.service.PlanoPagamentoService;
import br.com.desenv.nepalign.service.SituacaoPedidoVendaService;
import br.com.desenv.nepalign.service.StatusItemPedidoService;
import br.com.desenv.nepalign.service.TipoMovimentacaoEstoqueService;
import br.com.desenv.nepalign.service.VendedorService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;


public class ImportarVendaArquivoCache 
{
	public static void main(String[] args) throws Exception
	{
		EntityManager manager = ConexaoUtil.getEntityManager();
		manager.getTransaction().begin();
		
		try
		{
			final ImportarVendaArquivoCache importar = new ImportarVendaArquivoCache();
			
			final String[] data = importar.readFile("C:/VENDASFEIRA");
			
			for(String d : data)
			{
				final String[] venda = d.split("\\^", -1);
				
				if(venda[0x01].equals("0"))
					manager.persist(importar.gerarPedidoVenda(manager, venda));
				else
					manager.persist(importar.gerarItemPedidoVenda(manager, venda));
			}	
			
			manager.getTransaction().commit();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			
			manager.getTransaction().rollback();
		}
		finally
		{
			manager.close();
		}
	}
	
	public String[] readFile(String fileName) throws IOException
	{
		List<String> result = new ArrayList<String>();
		
		BufferedReader br = null;

		try 
		{
			String sCurrentLine;

			br = new BufferedReader(new FileReader(fileName));

			while ((sCurrentLine = br.readLine()) != null) 
			{
				if(sCurrentLine.equals("FIM")) break;
				
				result.add(sCurrentLine);	
			}
		} 
		catch (IOException e) 
		{
			throw e;
		}
		finally 
		{
			try 
			{
				if (br != null) br.close();
			}
			catch (IOException ex) 
			{
				ex.printStackTrace();
			}
		}
		return result.toArray(new String[result.size()]);
	} 
	
	public PedidoVenda gerarPedidoVenda(EntityManager manager, String[] data) throws Exception
	{
		//NUMERO DO CONTROLE ^ 0= VENDA CAB^ DATA 64067=29/05/16 ^ VENDEDOR ^ 0 NC               ^ VR.TOTAL VENDA         ^ TIPO 1-VENDA 2 - TROCA          ^PLANO ^ CLIENTE TUDO 0 CONSUMIDOR ^ PEDACO 33 EMPRESA - NESTE CASO 9
		//NUMERO DO CONTROLE ^ NN=ITENS VEN^ COD BARRAS          ^ QUANTIDE ^ VR TABELA UNITARIO ^ VR.VENDA TOTAL DO ITEM ^ VR.CUSTO UNITARIO (PEGAR NEPAL) ^ TIPO - SAIDA + ENTRADA (USADO TROCA)


		//16990275^0^64067^990^0^20^1^1^0^^^^^^0^1^CONSUMIDOR^^^^^^^^^^^^^^^^^^9
		//16990275^1^159064^1^17.91^20^^^^^^^^^20.00
		
		Vendedor vendedor = new VendedorService().recuperarPorId(manager, ((Integer) manager.createNativeQuery("select idVendedor from vendedor where codigoVendedorCache = " + data[0x03] + " and idEmpresaFisica = 5 ").getSingleResult()));
		PlanoPagamento plano = new PlanoPagamentoService().recuperarPorId(manager, ((Integer) manager.createNativeQuery("select idPlanoPagamento from planoPagamento where codigoPlano = " + data[0x07] + " and idEmpresaFisica = 5").getSingleResult()));
		boolean troca = data[0x06].equals("2");
		
		TipoMovimentacaoEstoque tipoMovimentacaoEstoque = new TipoMovimentacaoEstoqueService().recuperarPorId(manager, troca ? 5 : 2);
		
		final PedidoVenda pedidoVenda = new PedidoVenda();
		pedidoVenda.setEmpresaFisica(new EmpresaFisicaService().recuperarPorId(manager, 0x09));
		pedidoVenda.setNumeroControle(data[0x00]);
		pedidoVenda.setVendedor(vendedor);
		pedidoVenda.setUsuario(vendedor.getUsuario());
		pedidoVenda.setTipoMovimentacaoEstoque(tipoMovimentacaoEstoque);
		pedidoVenda.setValorTotalPedido(Double.parseDouble(data[0x05]));
		pedidoVenda.setCliente(new ClienteService().recuperarPorId(manager, 0x00));
		pedidoVenda.setPlanoPagamento(plano);
		pedidoVenda.setDataVenda(new SimpleDateFormat("dd/MM/yyyy").parse("10/09/2016"));
		pedidoVenda.setSituacaoPedidoVenda(new SituacaoPedidoVendaService().recuperarPorId(manager, 3));
		
		System.out.println("Gerando Venda " + pedidoVenda.getNumeroControle() + " VALOR " + pedidoVenda.getValorTotalPedido() + " TROCA ? " + (troca ? "S" : "N"));
		
		return pedidoVenda;
	}
	
	public ItemPedidoVenda gerarItemPedidoVenda(EntityManager manager, String[] data) throws Exception
	{
		final PedidoVenda pedidoVenda = new PedidoVendaService().recuperarPorId(manager, ((Integer) manager.createNativeQuery("select idPedidoVenda from pedidoVenda where numeroControle = '" + data[0x00] + "' and dataVenda between '2016-09-10 00:00:00' and '2016-09-10 23:59:59'").getSingleResult()));
		final ItemPedidoVenda itemPedidoVenda = new ItemPedidoVenda();
		try
		{
			itemPedidoVenda.setPedidoVenda(pedidoVenda);
			itemPedidoVenda.setEstoqueProduto(new EstoqueProdutoService().recuperarPorId(manager, Integer.parseInt(data[0x02])));
			itemPedidoVenda.setProduto(itemPedidoVenda.getEstoqueProduto().getProduto());
			itemPedidoVenda.setItemSequencial(Integer.parseInt(data[0x01]));
			itemPedidoVenda.setQuantidade(Double.parseDouble(data[0x03]));
			itemPedidoVenda.setPrecoTabela(Double.parseDouble(data[0x04]));  
			itemPedidoVenda.setPrecoVenda(Double.parseDouble(data[0x05]) / Double.parseDouble(data[0x03]));
			itemPedidoVenda.setStatusItemPedido(new StatusItemPedidoService().recuperarPorId(manager, 3));
			
			if(pedidoVenda.getTipoMovimentacaoEstoque().getId().intValue() == 0x05)
				itemPedidoVenda.setEntradaSaida(data[0x07].equals("+") ? "E" : "S");
			else
				itemPedidoVenda.setEntradaSaida("S");	
		}
		catch(Exception ex)
		{
			System.out.println(data[0x02]);
			throw ex;
		}
		
		return itemPedidoVenda;
	}
}
