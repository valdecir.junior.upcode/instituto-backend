import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import br.com.desenv.nepalign.util.DsvConstante;


public class ScriptMudancaEmpresa 
{
	public ScriptMudancaEmpresa() { }
	
	public static void main(String[] args)
	{
		new ScriptMudancaEmpresa().run();
	}
	
	public void run()
	{
		String idNovo = "10";
		String idAntigo = "1";
		
		System.out.println("SET FOREIGN_KEY_CHECKS = 0;");
		
		BufferedReader br = null;

		try  
		{
			String sCurrentLine;
			
			br = new BufferedReader(new FileReader(getClass().getResource("TABELAS_EMPRESA").getPath()));

			while ((sCurrentLine = br.readLine()) != null) 
			{
				if(sCurrentLine.equals("FIM")) break;
				
				System.out.println("UPDATE " + sCurrentLine.split(";")[0x01] + " SET " + sCurrentLine.split(";")[0x00] + " = " + idNovo + " WHERE " + sCurrentLine.split(";")[0x00] + " = " + idAntigo + ";");
				//System.out.println("DELETE FROM " + sCurrentLine.split(";")[0x01] + " WHERE " + sCurrentLine.split(";")[0x00] + " not in (1,3,7,9, 101, 33, 57, 59);");
			}
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			try 
			{
				if (br != null) br.close();
			}
			catch (IOException ex) 
			{
				ex.printStackTrace();
			}
			
			System.out.println("SET FOREIGN_KEY_CHECKS = 1;");
		}
	}
}