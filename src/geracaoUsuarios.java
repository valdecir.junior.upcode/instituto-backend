import javax.net.ssl.ManagerFactoryParameters;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;

import antlr.collections.List;
import br.com.desenv.nepalign.model.Empresa;
import br.com.desenv.nepalign.model.EmpresaFisica;
import br.com.desenv.nepalign.model.EmpresaUsuario;
import br.com.desenv.nepalign.model.Grupo;
import br.com.desenv.nepalign.model.Usuario;
import br.com.desenv.nepalign.model.Usuariogrupo;
import br.com.desenv.nepalign.model.Vendedor;
import br.com.desenv.nepalign.service.EmpresaFisicaService;
import br.com.desenv.nepalign.service.EmpresaUsuarioService;
import br.com.desenv.nepalign.service.GrupoService;
import br.com.desenv.nepalign.service.UsuarioService;
import br.com.desenv.nepalign.service.UsuarioacaoService;
import br.com.desenv.nepalign.service.UsuariogrupoService;
import br.com.desenv.nepalign.service.VendedorService;
import br.com.desenv.padraoignorante.conexao.ConexaoUtil;


public class geracaoUsuarios
{
	protected UsuarioService usuarioService;
	protected UsuariogrupoService usuarioGrupoService;
	protected EmpresaUsuarioService empresaUsuarioService;
	protected GrupoService grupoService;
	protected EmpresaFisicaService empresaFisicaService;
	
	protected Integer minimumValue = 0;
	protected Integer maximumValue = 0;
	
	protected Integer idEmpresaPadrao = 3;
	protected Integer idGrupoPadrao = 2;
	
	protected Grupo grupo;
	protected EmpresaFisica empresaFisica;
	
	protected String listaUsuario = "Lilian;881390|Barbara;090387|Poliana;280391|Priscila;060593|Katiuscia;160612|Paloma;130395";
	
	
	public geracaoUsuarios(Boolean autoStart) throws Exception
	{
		System.out.println("Rodando script...");
		
		this.usuarioGrupoService = new UsuariogrupoService();
		this.usuarioService = new UsuarioService();
		this.empresaUsuarioService = new EmpresaUsuarioService();
		this.grupoService = new GrupoService();
		this.empresaFisicaService = new EmpresaFisicaService();
		
		this.grupo = this.grupoService.recuperarPorId( this.idGrupoPadrao);
		this.empresaFisica = this.empresaFisicaService.recuperarPorId( this.idEmpresaPadrao);
		
		if(autoStart)
		{
			EntityManager manager = ConexaoUtil.getEntityManager();
			EntityTransaction transaction = manager.getTransaction();
			transaction.begin();
			
			this.minimumValue = this.idEmpresaPadrao * 100;
			this.maximumValue = this.minimumValue + 100;
			
			try
			{	
				for(int i = this.minimumValue.intValue(); i < this.maximumValue.intValue(); i++)
				{
					Usuario usuario;
					Usuario usuarioPesquisa = new Usuario();
					usuarioPesquisa.setLogin(new Integer(i).toString());
					
					java.util.List<Usuario> usuarioResultList = this.usuarioService.listarPorObjetoFiltro(manager, usuarioPesquisa, null, null, null);
					
					if(usuarioResultList.size() < 1)
					{
						usuario = new Usuario();
						Empresa empresa = new Empresa();
						empresa.setId(this.idEmpresaPadrao);
						usuario.setEmpresaPadrao(empresa);
						usuario.setLogin(new Integer(i).toString());
						usuario.setNome(new Integer(i).toString());
						//usuario.setSenha(UsuarioService.criptografarMd5(new Integer(i).toString()));
						usuario.setUsuarioGlobal("N");
						usuario.setUsuarioMaster("N");
						usuario.setUsuarioCaixa("N");
						
						this.usuarioService.salvar(manager, transaction, usuario);
						
						Usuariogrupo usuarioGrupo = new Usuariogrupo();
						usuarioGrupo.setUsuario(usuario);
						usuarioGrupo.setGrupo(grupo);
						usuarioGrupo.setEmpresa(empresa);
						this.usuarioGrupoService.salvar(manager, transaction, usuarioGrupo);
						
						EmpresaUsuario empresaUsuario = new EmpresaUsuario();
						empresaUsuario.setEmpresa(empresa);
						empresaUsuario.setUsuario(usuario);
						this.empresaUsuarioService.salvar(manager, transaction, empresaUsuario);
						
						Vendedor vendedor = new Vendedor();
						vendedor.setCodigoVendedorCache(new Integer(i));
						vendedor.setEmpresaFisica(this.empresaFisica);
						vendedor.setNome(new Integer(i).toString());
						vendedor.setUsuario(usuario);
						
						new VendedorService().salvar(manager, transaction, vendedor);
						
						System.out.println("Usuario  : " + i + " criado com sucesso!");
					}
					else
						System.out.println("Usuario " + i + " existente... processo abortado para esse usuário");
				}
				transaction.commit();
			}
			catch(Exception ex)
			{
				transaction.rollback();
			}
			finally
			{
				manager.close();
			}
		}
	}
	
	public void gerarUsuarioPorLista() throws Exception
	{	
		System.out.println("Rodando script...");
		
		EntityManager manager = ConexaoUtil.getEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		transaction.begin();
		
		try
		{
			String[] listaUsuarioFormatada = this.listaUsuario.split("|");
			for(String userInfo:listaUsuarioFormatada)
			{
				String login = userInfo.split(";")[0];
				String senha = userInfo.split(";")[1];
				Usuario usuarioPesquisa = new Usuario();
				
				usuarioPesquisa.setLogin(login);
				java.util.List<Usuario> listaUsuarioPesquisa = new UsuarioService().listarPorObjetoFiltro(usuarioPesquisa);
				
				if(listaUsuarioPesquisa.size() < 1)
				{
					Usuario usuario = new Usuario();
					Empresa empresa = new Empresa();
					empresa.setId(this.idEmpresaPadrao);
					usuario.setEmpresaPadrao(empresa);
					usuario.setLogin(login);
					usuario.setNome(login);
					//usuario.setSenha(UsuarioService.criptografarMd5(senha));
					usuario.setUsuarioGlobal("N");
					usuario.setUsuarioMaster("N");
					usuario.setUsuarioCaixa("N");
					
					this.usuarioService.salvar(manager, transaction, usuario);
					
					Usuariogrupo usuarioGrupo = new Usuariogrupo();
					usuarioGrupo.setUsuario(usuario);
					usuarioGrupo.setGrupo(grupo);
					usuarioGrupo.setEmpresa(empresa);
					this.usuarioGrupoService.salvar(manager, transaction, usuarioGrupo);
					
					EmpresaUsuario empresaUsuario = new EmpresaUsuario();
					empresaUsuario.setEmpresa(empresa);
					empresaUsuario.setUsuario(usuario);
					this.empresaUsuarioService.salvar(manager, transaction, empresaUsuario);
					System.out.println("Usuário " + login + " criado com sucesso!");
				}
				else
					System.out.println("Usuario login : " + login + " já existente. Processo abortado para esse usuário");
			}

			transaction.commit();
		}
		catch(Exception ex)
		{
			transaction.rollback();
		}
		finally
		{
			manager.close();
		}
	}

	public static void main(String[] args) throws Exception
	{
		geracaoUsuarios geracao = new geracaoUsuarios(false);
		geracao.gerarUsuarioPorLista();
	}
}
