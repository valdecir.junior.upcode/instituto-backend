import java.io.BufferedReader;
import java.io.FileReader;
import java.text.Normalizer;
import java.util.ArrayList;

import org.infinispan.util.SysPropertyActions;

import br.com.desenv.nepalign.model.Atendido;
import br.com.desenv.nepalign.model.AtividadeOferecida;
import br.com.desenv.nepalign.model.Turma;
import br.com.desenv.nepalign.model.TurmaAtendido;
import br.com.desenv.nepalign.persistence.TurmaAtendidoPersistence;
import br.com.desenv.nepalign.service.AtendidoService;
import br.com.desenv.nepalign.service.AtividadeOferecidaService;
import br.com.desenv.nepalign.service.TurmaAtendidoService;
import br.com.desenv.nepalign.service.TurmaService;


public class ImportaPlanilhaAtendidoTurma 
{

	public static void main(String[] args)
	{
		ArrayList<String> arquivos = new ArrayList<String>();
		/*arquivos.add("anderson.csv");
		arquivos.add("anderson01.csv");
		arquivos.add("anderson02.csv");
		arquivos.add("C�pia de CRONOGRAMA HORARIOS ATENDIDOS.csv");
		arquivos.add("Cronograma IMG 1.csv");
		arquivos.add("Tabela IMG atualizada.csv");
		arquivos.add("Tabela IMG com codigo (Salvo automaticamente).csv");
		arquivos.add("Tabela IMG com codigo.csv");
		arquivos.add("Tabela IMG ROSANA.csv");*/
		arquivos.add("x_unificada.csv");
		
		for (String arquivo : arquivos) 
		{
			System.out.println(arquivo);
			ImportaPlanilhaAtendidoTurma pl = new ImportaPlanilhaAtendidoTurma();
			pl.init("C:/upcode/projetos/instituto/planilhas/ajustadas/"+arquivo);			
		}

	}
	public void init(String planilha)
	{
		BufferedReader br = null;
		int cont=0;
		String conteudo[];
		String conteudoNome[];
		boolean isAtendido=false;
		ArrayList<String> linhasNaoUtilizadas = new ArrayList<String>();
		String nomeAtendido;
		int codigoAtendido;
		int codigoTurma;
		String nomeAtividade;
		int idTurma;
		Atendido atendido = null;
		AtendidoService atendidoService = new AtendidoService();
		AtividadeOferecida atividade = null;
		AtividadeOferecidaService atividadeService = new AtividadeOferecidaService();
		ArrayList<TurmaAtendido> listaTurmasAtendido = new ArrayList<TurmaAtendido>();
		Turma turma = null;
		TurmaService turmaService = new TurmaService();
		String nomeTurma = null;
		int qtAtendidos=0;
		int qtAtividades=0;
		boolean existeAnterior=false;

		try
		{
			br = new BufferedReader(new FileReader(planilha));
			
			String linha;
			while ((linha = br.readLine()) != null) 
			{
				cont++;
				if (!(linha.trim().equals(";;;;;;;;") || linha.trim().indexOf("RIOS DE AULAS")>-1 || linha.trim().indexOf("ATIVIDADE")>-1  || linha.trim().indexOf("NOME:;;;")>-1
						|| linha.trim().indexOf("DAS VIRGEM RODRIGUES")>-1 || linha.trim().indexOf("Carlos Ricci")>-1))
				{
					conteudo =  removerCaracteresEspeciais(linha).split(";");
					if (conteudo.length>0)
					{
						//System.out.println(cont + " " + linha + " " + conteudo.length);
						if (!(conteudo[0].trim().equals("")&&conteudo[1].trim().equals("")&&conteudo[2].trim().equals("")))
						{ 
							
							if (conteudo[1].indexOf("NOME")>-1 && conteudo[1].split(":").length>1 && conteudo[1].split(":")[1].trim().equals(""))
							{
								linhasNaoUtilizadas.add(linha);
							}
								
							else
							{
								//System.out.println("linha: " + cont + " " + linha);
								if (conteudo[1].indexOf("NOME")>-1)
								{
									conteudoNome = conteudo[1].split(":");
									try{
										nomeAtendido = conteudoNome[1];
									}
									catch(Exception ex2)
									{
										nomeAtendido="";
										System.out.println("Nome do Atendido n�o foi informado: " + linha);
									}									
									if (conteudo[3].length()>0)
									{
										try
										{
											codigoAtendido = Integer.parseInt(conteudo[3]);
										}
										catch(Exception ex1)
										{
											System.out.println("Código do Atendido não é numérico. Linha: " + linha);
											throw new Exception("Código do Atendido não é numérico. Linha: " + linha);
										}
										if (atendido==null) existeAnterior=false;
										else existeAnterior=true;
										atendido = atendidoService.recuperarPorId(codigoAtendido);
										if (atendido==null || atendido.equals(null))
										{
											System.out.println("Código do Atendido não foi encontrado na base de dados. Linha: " + linha);
											//throw new Exception("Código do Atendido não foi encontrado na base de dados. Linha: " + linha);
										}
										else
										{
											//System.out.println(atendido.getNomeCompleto()+ " " + nomeAtendido);
											qtAtendidos++;
											
											//existe uma lista de atividades anterior para ser salva
											if (listaTurmasAtendido.size()>0)
											{
												if (existeAnterior==true)
												{
													TurmaAtendido criterioT = new TurmaAtendido();
													criterioT.setAtendido(atendido);
													if (new TurmaAtendidoService().listarPorObjetoFiltro(criterioT).size()>0)
													{
														System.out.println("Atendido j� possui ensalamento: " + atendido.getId()+" "+atendido.getNomeCompleto());
													}
													else
													{
														for (TurmaAtendido turmaAtendido : listaTurmasAtendido) 
														{
															new TurmaAtendidoService().salvar(turmaAtendido);
														}
													}
												}
												else
												{
													System.out.println("Existe atendido sem atividades identificadas.");
													throw new Exception("Atendido sem atividade.");
												}				
												
											}
											listaTurmasAtendido = new ArrayList<TurmaAtendido>();

										}										
									}
									else
									{
										System.out.println("Código do Atendido não foi informado. Linha: " + linha);
									}
								}
								else if (conteudo[0].length()>0)
								{
									try
									{
										codigoTurma = Integer.parseInt(conteudo[0]);
									}
									catch(Exception ex2)
									{
										System.out.println("Valor informado não é um código. Linha: " + linha);
										throw new Exception("Valor informado não é um código");
									}
									nomeAtividade = normalizaNomeAtividade(conteudo[1]);
									nomeTurma = nomeAtividade+ " " + codigoTurma;
									Turma criterioTurma = new Turma();
									criterioTurma.setDescricao(nomeTurma);
									try
									{
										turma = turmaService.listarPorObjetoFiltro(criterioTurma).get(0);
										TurmaAtendido turmaAtendido = new TurmaAtendido();
										Atendido atendidoNovo = new Atendido();
										atendidoNovo.setId(atendido.getId());
										turmaAtendido.setAtendido(atendidoNovo);
										turmaAtendido.setTurma(turma);
										listaTurmasAtendido.add(turmaAtendido);
										qtAtividades++;
										//System.out.println(turma.getId()+" "+turma.getDescricao());
									}
									catch(Exception ex)
									{
										System.out.println("Turma não foi encontrada. " + " " + cont + " "  + nomeTurma + " Linha: " + linha);
										ex.printStackTrace();
										throw new Exception("Turma náo foi encontrada.");
									}
								}
							}
						}
						else
						{
							linhasNaoUtilizadas.add(linha);
						}
					}
					else
					{
						linhasNaoUtilizadas.add(linha);
					}
					
				}
				else
				{
					//linhasNaoUtilizadas.add(linha);
				}
			}
			if (linhasNaoUtilizadas.size()>0)
			{
				for (String linhaNaoUtilizada : linhasNaoUtilizadas)
				{
						System.out.println(linhaNaoUtilizada);
				}
			}
			if (listaTurmasAtendido.size()>0)
			{
				TurmaAtendido criterioT = new TurmaAtendido();
				criterioT.setAtendido(atendido);
				if (new TurmaAtendidoService().listarPorObjetoFiltro(criterioT).size()>0)
				{
					System.out.println("Atendido j� possui ensalamento: " + atendido.getId()+" "+atendido.getNomeCompleto());
				}
				else
				{
					for (TurmaAtendido turmaAtendido : listaTurmasAtendido) 
					{
						new TurmaAtendidoService().salvar(turmaAtendido);
					}
				}
			}			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

		System.out.println("FINAL: Linhas Lidas " + cont);
		System.out.println("FINAL: Qt. Atendidos " + qtAtendidos);
		System.out.println("FINAL: Qt. Atividades " + qtAtividades);
	}
	
	private String normalizaNomeAtividade(String nome)
	{
		nome = nome.toUpperCase().trim();
		if (nome.equals("HIDROGINµSTICA") || nome.equals("HIDROGINÁSTICA") || nome.equals("HIDROGIN�STICA") || nome.indexOf("HIDROGIN")>-1)
		{
			return "HIDROGINASTICA";
		}		
		else if (nome.equals("GINµSTICA") || nome.equals("GINÁSTICA") || nome.equals("GIN�STICA") || nome.indexOf("GIN")>-1)
		{
			return "GINASTICA";
		}
		if (nome.equals("COMPUTA€ÇO") || nome.equals("COMPUTAÇÃO") || nome.equals("COMPUTA��O")  || nome.indexOf("COMPUTA")>-1)
		{
			return "COMPUTACAO";
		}		
		if (nome.equals("CULINµRIA") || nome.equals("CULINÁRIA") || nome.equals("CULIN�RIA") || nome.indexOf("CULIN")>-1)
		{
			return "CULINARIA";
		}			
		if (nome.equals("DAN�A") || nome.equals("DANÇA") || nome.indexOf("DAN A")>-1  || nome.indexOf("DAN")>-1)
		{
			return "DANCA";
		}
		if (nome.equals("MUSCULA��O") || nome.equals("MUSCULAÇÃO") || nome.indexOf("MUSCULA")>-1)
		{
			return "MUSCULACAO";
		}		
		if (nome.equals("VOLEY") || nome.equals("V�LEI") || nome.indexOf("VOL")>-1 || nome.equals("VALEI")  || nome.equals("VALEY"))
		{
			return "VOLEI";
		}	
		if (nome.indexOf("ARTES")>-1)
		{
			return "ARTESANATO";
		}			
		else
		{
			return nome;
		}
	
	}

	public String removerCaracteresEspeciais(String string) {
        string = Normalizer.normalize(string, Normalizer.Form.NFD);
        string = string.replaceAll("[^\\p{ASCII}]", "");
        return string;
    }	
}
